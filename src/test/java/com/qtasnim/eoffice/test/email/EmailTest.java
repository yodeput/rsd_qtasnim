package com.qtasnim.eoffice.test.email;

import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.IOException;

@RunWith(Arquillian.class)
public class EmailTest extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() throws IOException {
        return createDeployment(EmailTest.class);
    }

    @Inject
    private NotificationService notificationService;

    @Test
    public void test() {
        MasterUser seno = new MasterUser();
        seno.setEmail("seno@qtasnim.com");

        notificationService.sendEmail("Test", "Body", seno);
    }
}
