package com.qtasnim.eoffice.test.autonumber;

import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.services.MasterAutoNumberService;
import com.qtasnim.eoffice.services.SuratService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.test.alfresco.AlfrescoTest;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
public class AutoNumberTest extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() throws IOException {
        return AlfrescoTest.createDeployment(war -> {
            war.addClasses(AutoNumberTest.class);
            war.delete("WEB-INF/classes/com/qtasnim/eoffice/servlets");
        });
    }

    @Inject
    private MasterAutoNumberService autoNumberService;

    @Inject
    private SuratService suratService;

    @Test
    public void test() {
        Surat surat = suratService.find(1607L);
        String next = autoNumberService.from(Surat.class).next(t -> t.getNoDokumen(), surat);

        assertNotNull(next);
    }

    @Test
    public void testAgenda() {
        Surat surat = suratService.find(1648L);
        String next = autoNumberService.from(Surat.class).next(t -> t.getNoAgenda(), surat);

        assertNotNull(next);
    }
}
