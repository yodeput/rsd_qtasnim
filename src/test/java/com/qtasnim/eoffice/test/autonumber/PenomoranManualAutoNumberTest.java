package com.qtasnim.eoffice.test.autonumber;

import com.qtasnim.eoffice.db.PenomoranManual;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.services.MasterAutoNumberService;
import com.qtasnim.eoffice.services.PenomoranManualService;
import com.qtasnim.eoffice.services.SuratService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.test.alfresco.AlfrescoTest;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
public class PenomoranManualAutoNumberTest extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() throws IOException {
        return AlfrescoTest.createDeployment(war -> {
            war.addClasses(PenomoranManualAutoNumberTest.class);
            war.delete("WEB-INF/classes/com/qtasnim/eoffice/servlets");
        });
    }

    @Inject
    private MasterAutoNumberService autoNumberService;

    @Inject
    private PenomoranManualService manualService;

    @Test
    public void test() {
        PenomoranManual obj = manualService.find(1L);
        String next = autoNumberService.from(PenomoranManual.class).next(PenomoranManual::getNoDokumen, obj);

        assertNotNull(next);
    }
}
