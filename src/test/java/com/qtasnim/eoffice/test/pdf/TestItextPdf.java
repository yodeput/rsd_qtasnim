package com.qtasnim.eoffice.test.pdf;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.qtasnim.eoffice.util.ImageProcessing;
import com.qtasnim.eoffice.util.TextExtractionProcess;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.List;
import java.util.UUID;

public class TestItextPdf {
    @Test
    public void testPageContent() throws IOException {
        PdfReader reader = new PdfReader("D:\\Local Disk\\Download\\20181105 SOP Permohonan Cuti.pdf");

        int n = reader.getNumberOfPages();

        for (int i = 1; i <= n; i++) {
            byte[] bytes = reader.getPageContent(i);

            ImageIO.write(new ImageProcessing().byteToImage(bytes), "bmp", new File("D://", UUID.randomUUID()+".bmp"));
        }

        reader.close();
    }

    @Test
    public void testPageContent2() {
        try (final PDDocument document = PDDocument.load(new File("D:\\Local Disk\\Download\\20181105 SOP Permohonan Cuti.pdf"))){
            PDFRenderer pdfRenderer = new PDFRenderer(document);
            for (int page = 0; page < document.getNumberOfPages(); ++page)
            {
                BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
                String fileName = "D:\\image-" + page + ".png";
                ImageIOUtil.writeImage(bim, fileName, 300);
            }
            document.close();
        } catch (IOException e){
            System.err.println("Exception while trying to create pdf document - " + e);
        }
    }

    @Test
    public void testPageContent3() throws IOException {
        File file = new File("D:\\Local Disk\\Download\\20181105 SOP Permohonan Cuti.pdf");

        byte[] bytes = IOUtils.toByteArray(new FileInputStream(file));

        PDDocument document = PDDocument.load(bytes);

        PDFTextStripper pdfTextStripper = new PDFTextStripper();

        System.out.println(document.getNumberOfPages());

//        String extractedText = pdfTextStripper.getText(document);
//        System.out.println(extractedText);

        pdfTextStripper.setStartPage(4);
        pdfTextStripper.setEndPage(4);
        String extractedText = pdfTextStripper.getText(document);
        System.out.println(extractedText);

        System.out.println();
    }

    @Test
    public void testPageContent4() throws IOException {
        File file = new File("D:\\Local Disk\\Download\\20181105 SOP Permohonan Cuti.pdf");

        byte[] bytes = IOUtils.toByteArray(new FileInputStream(file));

        TextExtractionProcess textExtractionProcess = new TextExtractionProcess();

//        String text = textExtractionProcess.extractAllText(bytes);
//
//        System.out.println(text);

        List<String> texts = textExtractionProcess.extractTextV2WithoutOCR(bytes);

        System.out.println(texts);
    }

    @Test
    public void testTiffToPdf() throws Exception {
        File fileTiff = new File("D:\\Local Disk\\Download\\multipage_tiff_example.tif");

        byte[] bytes = IOUtils.toByteArray(new FileInputStream(fileTiff));

        ImageProcessing imageProcessing = new ImageProcessing();

        List<BufferedImage> images = imageProcessing.imagesFromTiff(new FileInputStream(fileTiff));

        byte[] pdfBytes = imageProcessing.multiImagesToPdf(images);

        File filepdf = new File("D:\\"+UUID.randomUUID()+".pdf");

        OutputStream os = new FileOutputStream(filepdf);

        os.write(pdfBytes);
    }
}
