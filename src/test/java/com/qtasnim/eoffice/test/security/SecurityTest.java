package com.qtasnim.eoffice.test.security;

import static org.junit.Assert.*;
import static org.valid4j.matchers.http.HttpResponseMatchers.hasHeader;
import static org.valid4j.matchers.http.HttpResponseMatchers.hasStatus;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qtasnim.eoffice.db.UserSession;
import com.qtasnim.eoffice.security.PasswordHash;
import com.qtasnim.eoffice.services.UserSessionService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;

import com.qtasnim.eoffice.ws.dto.LoginParam;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;

@RunWith(Arquillian.class)
public class SecurityTest extends EjbUnitTestBase {
    private static final long serialVersionUID = 1L;

    @Deployment
    public static Archive<?> deploy() throws IOException {
        return SecurityTest.createDeployment(SecurityTest.class);
    }

    @Inject
    private PasswordHash passwordHash;

    @Inject
    private UserSessionService userSessionService;

    @Inject
    private Logger logger;

    @Test
    public void passwordHashTest() throws NoSuchAlgorithmException {
        String password = "pass@word1";

        for (int i = 0; i < 5; i++) {
            List<String> keys = passwordHash.getSaltPassword(password);
            String hash = keys.get(1);
            String salt = keys.get(0);

            assertEquals(passwordHash.getHash(password, salt), hash);

            logger.info(String.format("hash: %s | salt: %s", hash, salt));
        }
    }

    @Test
    public void loginTest() {
        Client client = ClientBuilder.newClient();
        LoginParam loginParam = new LoginParam();
        loginParam.setUsername("superuser");
        loginParam.setPassword("pass@word1");

        //Login success test
        Response response = client
                .target("http://localhost:8181/api/login")
                .request("application/json")
                .post(Entity.entity(loginParam, MediaType.APPLICATION_JSON_TYPE));

        assertThat(response, hasStatus(Response.Status.OK));

        String token = response.readEntity(String.class);

        //Count any active sessions test
        List<UserSession> activeSession = userSessionService.getActiveSession();
        assertTrue(activeSession.size() > 0);

        //Check authorized test
        response = client.target("http://localhost:8181/api/user/me")
                .request("application/json")
                .header("Authorization", "Bearer " + token)
                .get();

        assertThat(response, hasStatus(Response.Status.OK));

        //Logout success test
        response = client.target("http://localhost:8181/api/logout")
                .request("application/json")
                .header("Authorization", "Bearer " + token)
                .get();

        assertThat(response, hasStatus(Response.Status.OK));

        //Check unauthorized test
        try {
            response = client.target("http://localhost:8181/api/user/me").request("application/json").get();
        } catch (WebApplicationException | ResponseProcessingException e) {
            response = Optional.of(e).map(t -> {
                if (t instanceof WebApplicationException) return ((WebApplicationException) t).getResponse();

                return ((ResponseProcessingException) t).getResponse();
            }).orElse(null);
        }

        assertNotNull(response);
        assertThat(response, hasStatus(Response.Status.FORBIDDEN));
        assertThat(response, hasHeader("X-Reason"));

        //Check login error test
        try {
            loginParam = new LoginParam();
            loginParam.setUsername("superuser");
            loginParam.setPassword("pass@word11");

            response = client
                    .target("http://localhost:8181/api/login")
                    .request("application/json")
                    .post(Entity.entity(loginParam, MediaType.APPLICATION_JSON_TYPE));
        } catch (WebApplicationException | ResponseProcessingException e) {
            response = Optional.of(e).map(t -> {
                if (t instanceof WebApplicationException) return ((WebApplicationException) t).getResponse();

                return ((ResponseProcessingException) t).getResponse();
            }).orElse(null);
        }

        assertNotNull(response);
        assertThat(response, hasStatus(Response.Status.UNAUTHORIZED));
        assertThat(response, hasHeader("X-Reason"));

        //Check empty active sessions test
        activeSession = userSessionService.getActiveSession();
        assertEquals(0, activeSession.size());
    }
}