package com.qtasnim.eoffice.test.camunda;


import com.qtasnim.eoffice.ws.ProcessTaskResource;
import com.qtasnim.eoffice.ws.WorkflowResource;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@LocalBean
@Stateless
public class ProcessTaskResourceMock extends ProcessTaskResource {
}
