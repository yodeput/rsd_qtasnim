package com.qtasnim.eoffice.test.camunda;

import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.ws.dto.SuratDto;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
public class SuratTaskTest extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.addClasses(
                            SuratTaskTest.class,
                            ProcessTaskResourceMock.class);
                });
    }

    @Inject
    private ProcessTaskResourceMock processTaskResourceMock;

    @Test
    public void test() {
        Response filter = processTaskResourceMock.filter(1, 1, "executionStatus=='PENDING'", "", true);
        List<SuratDto> entity = (List<SuratDto>) filter.getEntity();

        assertNotNull(entity);
    }
}
