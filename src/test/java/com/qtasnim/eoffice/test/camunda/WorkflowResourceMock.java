package com.qtasnim.eoffice.test.camunda;

import com.qtasnim.eoffice.ws.WorkflowResource;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class WorkflowResourceMock extends WorkflowResource {
}
