package com.qtasnim.eoffice.test.camunda;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.DistribusiDokumen;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.MasterWorkflowProvider;
import com.qtasnim.eoffice.db.ProcessInstance;
import com.qtasnim.eoffice.db.ProcessTask;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.db.UserSession;
import com.qtasnim.eoffice.services.CompanyCodeService;
import com.qtasnim.eoffice.services.DistribusiDokumenDetailService;
import com.qtasnim.eoffice.services.DistribusiDokumenService;
import com.qtasnim.eoffice.services.MasterStrukturOrganisasiService;
import com.qtasnim.eoffice.services.MasterWorkflowProviderService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.services.PemeriksaDistribusiService;
import com.qtasnim.eoffice.services.PenandatanganDistribusiService;
import com.qtasnim.eoffice.services.PenerimaDistribusiService;
import com.qtasnim.eoffice.services.ProcessInstanceService;
import com.qtasnim.eoffice.services.ProcessTaskService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.ProcessStatus;
import com.qtasnim.eoffice.ws.WorkflowResource;
import com.qtasnim.eoffice.ws.dto.CompanyCodeDto;
import com.qtasnim.eoffice.ws.dto.DistribusiDokumenDetailDto;
import com.qtasnim.eoffice.ws.dto.DistribusiDokumenDto;
import com.qtasnim.eoffice.ws.dto.PemeriksaDistribusiDto;
import com.qtasnim.eoffice.ws.dto.PenandatanganDistribusiDto;
import com.qtasnim.eoffice.ws.dto.PenerimaDistribusiDto;
import com.qtasnim.eoffice.ws.dto.TaskActionDto;
import com.qtasnim.eoffice.ws.dto.TaskActionRequestDto;
import com.qtasnim.eoffice.ws.dto.TaskActionResponseDto;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
@Singleton
public class DistribusiTest extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.delete("WEB-INF/classes/com/qtasnim/eoffice/security/AuthenticatedUserProducer.class"); //delete default user userSession producers
                    war.delete("WEB-INF/classes/com/qtasnim/eoffice/services/NotificationService.class"); //disable notification
                    war.addClasses(
                            DistribusiTest.class,
                            NotificationService.class,
                            SessionDistribusiProducerMock.class);
                });
    }

    @Inject
    private WorkflowResource workflowResource;

    @Inject
    private DistribusiDokumenService distribusiDokumenService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private MasterStrukturOrganisasiService masterOrganizationEntityService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private DistribusiTest engineTest;

    @Inject
    private PemeriksaDistribusiService pemeriksaDistribusiService;

    @Inject
    private PenandatanganDistribusiService penandatanganDistribusiService;

    @Inject
    private PenerimaDistribusiService penerimaDistribusiService;

    @Inject
    private DistribusiDokumenDetailService distribusiDokumenDetailService;

    @Inject
    private ProcessTaskService processTaskService;

    @Inject
    private ProcessInstanceService processInstanceService;

    @PersistenceContext(unitName = "eofficePU")
    private EntityManager em;

    @Test
    public void workflowTest() throws Exception {
        engineTest.testWorkflow();
    }

    public void testWorkflow() throws Exception {
        MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
        IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();
        DistribusiDokumen model = null;

        try {
            MasterStrukturOrganisasi konseptor = masterOrganizationEntityService.getAllValid().where(t -> t.getUser() != null).skip(0).limit(1).findFirst().orElse(null);
            MasterStrukturOrganisasi pemeriksa = masterOrganizationEntityService.getAllValid().where(t -> t.getUser() != null).skip(10).limit(1).findFirst().orElse(null);
            MasterStrukturOrganisasi penandatangan = masterOrganizationEntityService.getAllValid().where(t -> t.getUser() != null).skip(20).limit(1).findFirst().orElse(null);
            MasterStrukturOrganisasi penerima = masterOrganizationEntityService.getAllValid().where(t -> t.getUser() != null).skip(30).limit(1).findFirst().orElse(null);

            setUser(konseptor);
            Calendar calendar = Calendar.getInstance();
            DateUtil dateUtil = new DateUtil();
            CompanyCode companyCode = companyCodeService.getByCode("I100");
            DistribusiDokumenDto dao = new DistribusiDokumenDto();
            dao.setStatus("SUBMITTED");
            dao.setCompanyCode(new CompanyCodeDto(companyCode));
            calendar.set(Calendar.YEAR, 1900);
            dao.setStart(dateUtil.getCurrentISODate(calendar.getTime()));
            calendar.set(Calendar.YEAR, 2200);
            dao.setEnd(dateUtil.getCurrentISODate(calendar.getTime()));
            dao.setPerihal("Test Perihal");
            dao.setJenis("RDS");
            dao.setIsDeleted(false);
            dao.getDetail().add(new DistribusiDokumenDetailDto() {
                {
                    setReferenceTable("t_folder_kegiatan_detail");
                    setIdReference(44L);
                }
            });
            dao.getPemeriksa().add(new PemeriksaDistribusiDto(){
                {
                    setIdPemeriksa(pemeriksa.getIdOrganization());
                    setSeq(0);
                }
            });
            dao.getPenandatangan().add(new PenandatanganDistribusiDto(){
                {
                    setIdPenandatangan(penandatangan.getIdOrganization());
                    setSeq(0);
                }
            });
            dao.getPenerima().add(new PenerimaDistribusiDto(){
                {
                    setIdPenerima(penerima.getIdOrganization());
                    setSeq(0);
                    setIsDownload(false);
                    setIsRead(false);
                }
            });

            String password = "pass@word";
            model = distribusiDokumenService.saveOrEditTransaction(null, dao, password, null, null, null, null);
            TaskActionRequestDto requestDto = new TaskActionRequestDto();
            requestDto.setRecordRefId(model.getId().toString());

            setUser(pemeriksa);
            TaskActionResponseDto entity = getTaskActions("Distribusi Dokumen", model);
            approve(entity, "Setujui", "pemeriksa ok");

            setUser(penandatangan);
            entity = getTaskActions("Distribusi Dokumen", model);
            approve(entity, "Setujui", "penandatangan ok");

            ProcessStatus processStatus = workflowProvider.getProcessStatus(model);
            assertNotNull(processStatus);
        } catch (Exception e) {
            throw e;
        } finally {
            if (model != null && model.getId() != null) {
                em.refresh(model);

                ProcessInstance processInstance = processInstanceService.getByRecordRefId("Distribusi Dokumen", model.getRelatedId());

                if (processInstance != null) {
                    try {
                        workflowProvider.terminateProcess(processInstance);
                    } catch (Exception e) {

                    }

                    processInstance.getTasks().stream().sorted(Comparator.comparing(ProcessTask::getId).reversed()).forEach(row -> processTaskService.remove(row));
                    processInstance.getTasks().clear();

                    processInstanceService.remove(processInstance);
                }

                model.getPenandatangan().forEach(row -> penandatanganDistribusiService.remove(row));
                model.getPemeriksa().forEach(row -> pemeriksaDistribusiService.remove(row));
                model.getPenerima().forEach(row -> penerimaDistribusiService.remove(row));
                model.getDetail().forEach(row -> distribusiDokumenDetailService.remove(row));

                model.getPenandatangan().clear();
                model.getPemeriksa().clear();
                model.getPenerima().clear();
                model.getDetail().clear();

                distribusiDokumenService.remove(model);
            }
        }
    }

    private TaskActionResponseDto getTaskActions(String formName, final DistribusiDokumen model) {
        return (TaskActionResponseDto) workflowResource.getTaskActionsByFormName(formName, null, new TaskActionRequestDto(){{setRecordRefId(model.getId().toString());}}).getEntity();
    }

    private void approve(final TaskActionResponseDto entity, String approval, String comment) {
        workflowResource.taskAction(new TaskActionDto(){
            {
                setTaskId(entity.getTaskId());
                setForms(Arrays.asList(
                        new FormFieldDto(){{
                            setName(entity.getForms().stream().filter(t -> t.getLabel().equals("Approval")).map(FormFieldDto::getName).findFirst().orElse(null));
                            setValue(approval);
                        }},
                        new FormFieldDto(){{
                            setName("Comment");
                            setValue(comment);
                        }}));
            }
        }).getEntity();
    }

    private void disposisi(final TaskActionResponseDto entity, String disposisi) {
        workflowResource.taskAction(new TaskActionDto(){
            {
                setTaskId(entity.getTaskId());
                setForms(Arrays.asList(
                        new FormFieldDto(){{
                            setName(entity.getForms().stream().filter(t -> t.getLabel().equals("Disposisi")).map(FormFieldDto::getName).findFirst().orElse(null));
                            setValue(disposisi);
                        }}));
            }
        }).getEntity();
    }

    private void setUser(MasterStrukturOrganisasi masterStrukturOrganisasi) {
        userSession.setUserSession(new UserSession());
        userSession.getUserSession().setUser(masterStrukturOrganisasi.getUser());
    }

    private Session userSession = new Session() {};

    public Session getUserSession() {
        return userSession;
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
