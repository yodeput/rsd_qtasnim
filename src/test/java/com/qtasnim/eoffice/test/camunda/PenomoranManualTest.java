package com.qtasnim.eoffice.test.camunda;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.ProcessStatus;
import com.qtasnim.eoffice.ws.WorkflowResource;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;

import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
@Singleton
public class PenomoranManualTest extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.delete("WEB-INF/classes/com/qtasnim/eoffice/security/AuthenticatedUserProducer.class"); //delete default user userSession producers
                    war.delete("WEB-INF/classes/com/qtasnim/eoffice/services/NotificationService.class"); //disable notification
                    war.addClasses(
                            PenomoranManualTest.class,
                            NotificationService.class,
                            SessionManualProducerMock.class);
                });
    }

    @Inject
    private WorkflowResource workflowResource;

    @Inject
    private PenomoranManualService manualService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private PenomoranManualTest engineTest;

    @Inject
    private ProcessTaskService processTaskService;

    @Inject
    private ProcessInstanceService processInstanceService;

    @PersistenceContext(unitName = "eofficePU")
    private EntityManager em;

    @Test
    public void workflowTest() throws Exception {
        engineTest.testWorkflow();
    }

    public void testWorkflow() throws Exception {
        MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
        IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();
        PenomoranManual model = manualService.find(1L);

        try {
            manualService.startWorkflow(model);
            TaskActionRequestDto requestDto = new TaskActionRequestDto();
            requestDto.setRecordRefId(model.getId().toString());

            ProcessStatus processStatus = workflowProvider.getProcessStatus(model);
            assertNotNull(processStatus);
        } catch (Exception e) {
            throw e;
        } finally {
            if (model != null && model.getId() != null) {
                em.refresh(model);

                ProcessInstance processInstance = processInstanceService.getByRecordRefId("Distribusi Dokumen", model.getRelatedId());

                if (processInstance != null) {
                    try {
                        workflowProvider.terminateProcess(processInstance);
                    } catch (Exception e) {

                    }

                    processInstance.getTasks().stream().sorted(Comparator.comparing(ProcessTask::getId).reversed()).forEach(row -> processTaskService.remove(row));
                    processInstance.getTasks().clear();

                    processInstanceService.remove(processInstance);
                }
            }
        }
    }

    private TaskActionResponseDto getTaskActions(String formName, final DistribusiDokumen model) {
        return (TaskActionResponseDto) workflowResource.getTaskActionsByFormName(formName, null, new TaskActionRequestDto(){{setRecordRefId(model.getId().toString());}}).getEntity();
    }

    private void approve(final TaskActionResponseDto entity, String approval, String comment) {
        workflowResource.taskAction(new TaskActionDto(){
            {
                setTaskId(entity.getTaskId());
                setForms(Arrays.asList(
                        new FormFieldDto(){{
                            setName(entity.getForms().stream().filter(t -> t.getLabel().equals("Approval")).map(FormFieldDto::getName).findFirst().orElse(null));
                            setValue(approval);
                        }},
                        new FormFieldDto(){{
                            setName("Comment");
                            setValue(comment);
                        }}));
            }
        }).getEntity();
    }

    private void disposisi(final TaskActionResponseDto entity, String disposisi) {
        workflowResource.taskAction(new TaskActionDto(){
            {
                setTaskId(entity.getTaskId());
                setForms(Arrays.asList(
                        new FormFieldDto(){{
                            setName(entity.getForms().stream().filter(t -> t.getLabel().equals("Disposisi")).map(FormFieldDto::getName).findFirst().orElse(null));
                            setValue(disposisi);
                        }}));
            }
        }).getEntity();
    }

    private void setUser(MasterStrukturOrganisasi masterStrukturOrganisasi) {
        userSession.setUserSession(new UserSession());
        userSession.getUserSession().setUser(masterStrukturOrganisasi.getUser());
    }

    private Session userSession = new Session() {};

    public Session getUserSession() {
        return userSession;
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
