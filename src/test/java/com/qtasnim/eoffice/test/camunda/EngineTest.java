package com.qtasnim.eoffice.test.camunda;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.ProcessEventHandler;
import com.qtasnim.eoffice.workflows.ProcessStatus;
import com.qtasnim.eoffice.workflows.resolvers.SuratKonseptorEqPenandatanganResolver;
import com.qtasnim.eoffice.workflows.resolvers.SuratPemeriksaResolver;
import com.qtasnim.eoffice.workflows.resolvers.SuratPenandatanganResolver;
import com.qtasnim.eoffice.ws.WorkflowResource;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
@Singleton
public class EngineTest extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.delete("WEB-INF/classes/com/qtasnim/eoffice/security/AuthenticatedUserProducer.class"); //delete default user userSession producers
                    war.delete("WEB-INF/classes/com/qtasnim/eoffice/services/NotificationService.class"); //disable notification
                    war.addClasses(
                            EngineTest.class,
                            NotificationService.class,
                            SessionEngineProducerMock.class);
                });
    }

    @Inject
    private WorkflowResource workflowResource;

    @Inject
    private ProcessDefinitionService processDefinitionService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private ProcessDefinitionFormVarResolverService processDefinitionFormVarResolverService;

    @Inject
    private MasterStrukturOrganisasiService masterOrganizationEntityService;

    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private SuratDisposisiService suratDisposisiService;

    @Inject
    private SuratService suratService;

    @Inject
    private FormDefinitionService formDefinitionService;

    @Inject
    private MasterJenisDokumenService masterJenisDokumenService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private EngineTest engineTest;

    @Inject
    private PemeriksaSuratService pemeriksaSuratService;

    @Inject
    private PenandatanganSuratService penandatanganSuratService;

    @Inject
    private PemeriksaPelakharPymtSuratService pemeriksaPelakharPymtSuratService;

    @Inject
    private PenandatanganPelakharPymtSuratService penandatanganPelakharPymtSuratService;

    @Inject
    private ProcessTaskService processTaskService;

    @PersistenceContext(unitName = "eofficePU")
    private EntityManager em;

    @Test
    public void workflowTest() throws InternalServerErrorException, URISyntaxException, IOException {
        engineTest.testWorkflow();
    }

    public void testDisposisi() throws InternalServerErrorException, URISyntaxException, IOException {
        String deploymentId = null;
        MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
        IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();
        Surat surat = null;
        FormDefinition formDefinition = null;

        try {
            String konseptor = "90002366";
            String penerima1 = "90003500";
            String penerima2 = "90003501";
            String penerima3 = "90002366";
            String penerima4 = "90013912";
            String penerima5 = "90002375";

            MasterStrukturOrganisasi _konseptor = masterOrganizationEntityService.getByCode(konseptor);
            MasterStrukturOrganisasi _penerima2 = masterOrganizationEntityService.getByCode(penerima2);
            MasterStrukturOrganisasi _penerima1 = masterOrganizationEntityService.getByCode(penerima1);
            MasterStrukturOrganisasi _penerima3 = masterOrganizationEntityService.getByCode(penerima3);
            MasterStrukturOrganisasi _penerima4 = masterOrganizationEntityService.getByCode(penerima4);
            MasterStrukturOrganisasi _penerima5 = masterOrganizationEntityService.getByCode(penerima5);
            CompanyCode i100 = companyCodeService.getByCode("I100");
            MasterJenisDokumen rahasia = masterJenisDokumenService.getAll(true).findFirst().orElse(null);
            DateUtil dateUtil = new DateUtil();

            formDefinition = new FormDefinition();
            formDefinition.setDescription("-");
            formDefinition.setName("Demo");
            formDefinition.setStart(new Date());
            formDefinition.setEnd(dateUtil.getDefValue());
            formDefinition.setVersion(1.0);
            formDefinition.setJenisDokumen(rahasia);
            formDefinitionService.create(formDefinition);

            surat = new Surat();
            surat.setKonseptor(_konseptor);
            surat.getPenerimaSurat().stream().map(q->q.getOrganization()).collect(Collectors.toList()).add(_penerima1);
            surat.getPenerimaSurat().stream().map(q->q.getOrganization()).collect(Collectors.toList()).add(_penerima2);
            surat.setFormDefinition(formDefinition);
            surat.setStatus("APPROVED");
            surat.setCompanyCode(i100);
            suratService.create(surat);

            suratDisposisiService.create(surat.getPenerimaSurat().stream().map(q->q.getOrganization()).collect(Collectors.toList()), surat);

            ProcessDefinitionDto result;
            ProcessDefinitionDto request = new ProcessDefinitionDto();
            request.setActivationDate(dateUtil.getCurrentISODate(new Date()));
            File fileToUpload = new File(this.getClass().getResource("/processmodels/disposisi.bpmn").toURI());

            try (InputStream targetStream = new FileInputStream(fileToUpload)) {
                Response response = workflowResource.submit(null, request, targetStream, null);
                result = (ProcessDefinitionDto) response.getEntity();
            }

            assertNotNull(result);

            deploymentId = result.getDeploymentId();

            ProcessDefinition activated = processDefinitionService.getActivated(WorkflowResource.DEFAULT_FORM_NAME);

            ProcessDefinitionFormVarResolver processDefinitionFormVarResolver = new ProcessDefinitionFormVarResolver();
            processDefinitionFormVarResolver.setProcessDefinition(activated);
            processDefinitionFormVarResolver.setVarName("FormField_Disposisi");
            processDefinitionFormVarResolver.setValueResolver("com.qtasnim.eoffice.workflows.resolvers.SuratDisposisiResolver");
            processDefinitionFormVarResolverService.create(processDefinitionFormVarResolver);

            activated.getFormVars().add(processDefinitionFormVarResolver);

            Long suratId = surat.getId();
            workflowProvider.startWorkflow(surat, activated, ProcessEventHandler.DEFAULT.onProcessCreated(pi -> {
                Surat s = suratService.find(suratId);
                pi.setSurat(s);
            }),  new HashMap<>());

            setUser(_penerima1);
            TaskActionResponseDto entity = getTaskActions(null, surat);
            disposisi(entity, getObjectMapper().writeValueAsString(Arrays.asList(penerima3, penerima4)));

            setUser(_penerima3);
            entity = getTaskActions(null, surat);
            disposisi(entity, null);

            setUser(_penerima2);
            entity = getTaskActions(null, surat);
            disposisi(entity, getObjectMapper().writeValueAsString(Arrays.asList(penerima1, penerima5)));

            setUser(_penerima4);
            entity = getTaskActions(null, surat);
            disposisi(entity, null);

            setUser(_penerima5);
            entity = getTaskActions(null, surat);
            disposisi(entity, null);

            setUser(_penerima1);
            entity = getTaskActions(null, surat);
            disposisi(entity, null);

            assertEquals(surat.getStatus(), EntityStatus.CLOSED.toString());
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (deploymentId != null) {
                    workflowProvider.undeploy(deploymentId);
                }
            } catch (Exception ignored) {

            }

            List<SuratDisposisi> disposisis = suratDisposisiService.findAll();
            disposisis.forEach(t -> {
                suratDisposisiService.remove(t);
            });

            ProcessDefinition activated = processDefinitionService.getActivated(WorkflowResource.DEFAULT_FORM_NAME);

            if (activated != null) {
                em.refresh(activated);

                activated.getInstances().forEach(t -> {
                    em.refresh(t);
                });

                processDefinitionService.remove(activated);
            }

            List<MasterDelegasi> masterDelegasis = masterDelegasiService.getAll().toList();

            masterDelegasis.forEach(t -> {
                masterDelegasiService.delete(t.getId());
            });

            if (surat != null && surat.getId() != null) {
                em.refresh(surat);
                suratService.remove(surat);
            }

            if (formDefinition != null && formDefinition.getId() != null) {
                em.refresh(formDefinition);
                formDefinitionService.remove(formDefinition);
            }
        }
    }

    @Test
    public void workflowTestWorkflow() throws InternalServerErrorException, URISyntaxException, IOException {
        engineTest.testWorkflow();
    }

    public void testWorkflow() throws InternalServerErrorException, URISyntaxException, IOException {
        String deploymentId = null;
        MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
        IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();
        Surat surat = null;
        FormDefinition formDefinition = null;

        try {
//            String pelakhar = "90009780";
//            String pymt = "90013321";

            MasterStrukturOrganisasi _konseptor = masterOrganizationEntityService.getAllValid().where(t -> t.getUser() != null).skip(0).limit(1).findFirst().orElse(null);
            MasterStrukturOrganisasi _pelakhar = masterOrganizationEntityService.getAllValid().where(t -> t.getUser() != null).skip(40).limit(1).findFirst().orElse(null);
//            MasterStrukturOrganisasi _pymt = masterOrganizationEntityService.getByCode(pymt);
            MasterJenisDokumen rahasia = masterJenisDokumenService.getAll(true).findFirst().orElse(null);
            CompanyCode i100 = companyCodeService.getByCode("I100");

            DateUtil dateUtil = new DateUtil();
//            MasterDelegasi masterDelegasi = new MasterDelegasi();
//            masterDelegasi.setFrom(_penandatangan1);
//            masterDelegasi.setTo(_pelakhar);
//            masterDelegasi.setTipe("Pelakhar");
//            masterDelegasi.setAlasan("Cuti");
//            masterDelegasi.setMulai(new Date());
//            masterDelegasi.setSelesai(dateUtil.getDefValue());
//            masterDelegasi.setCompanyCode(null);
//            masterDelegasiService.create(masterDelegasi);
//
//            masterDelegasi = new MasterDelegasi();
//            masterDelegasi.setFrom(_penandatangan1);
//            masterDelegasi.setTo(_pymt);
//            masterDelegasi.setTipe("PYMT");
//            masterDelegasi.setAlasan("Cuti");
//            masterDelegasi.setMulai(new Date());
//            masterDelegasi.setSelesai(dateUtil.getDefValue());
//            masterDelegasi.setCompanyCode(null);
//            masterDelegasiService.create(masterDelegasi);

            formDefinition = new FormDefinition();
            formDefinition.setDescription("-");
            formDefinition.setName("Demo");
            formDefinition.setStart(new Date());
            formDefinition.setEnd(dateUtil.getDefValue());
            formDefinition.setVersion(1.0);
            formDefinition.setJenisDokumen(rahasia);
            formDefinitionService.create(formDefinition);

            surat = new Surat();
            surat.setKonseptor(_konseptor);
            surat.setFormDefinition(formDefinition);
            surat.setStatus("DRAFT");
            surat.setIsPemeriksaParalel(false);
            surat.setCompanyCode(i100);
            suratService.create(surat);

            PenandatanganSurat _penandatangan1 = new PenandatanganSurat();
            _penandatangan1.setSurat(surat);
            _penandatangan1.setOrganization(masterOrganizationEntityService.getAllValid().where(t -> t.getUser() != null).skip(10).limit(1).findFirst().orElse(null));
            penandatanganSuratService.create(_penandatangan1);

            PemeriksaSurat _pemeriksa1 = new PemeriksaSurat();
            _pemeriksa1.setSurat(surat);
            _pemeriksa1.setSeq(0);
            _pemeriksa1.setOrganization(masterOrganizationEntityService.getAllValid().where(t -> t.getUser() != null).skip(20).limit(1).findFirst().orElse(null));
            pemeriksaSuratService.create(_pemeriksa1);

            PemeriksaSurat _pemeriksa2 = new PemeriksaSurat();
            _pemeriksa2.setSurat(surat);
            _pemeriksa2.setSeq(1);
            _pemeriksa2.setOrganization(masterOrganizationEntityService.getAllValid().where(t -> t.getUser() != null).skip(30).limit(1).findFirst().orElse(null));
            pemeriksaSuratService.create(_pemeriksa2);

            surat.getPemeriksaSurat().add(_pemeriksa1);
            surat.getPemeriksaSurat().add(_pemeriksa2);
            surat.getPenandatanganSurat().add(_penandatangan1);

            ProcessDefinitionDto result;
            ProcessDefinitionDto request = new ProcessDefinitionDto();
            request.setActivationDate(dateUtil.getCurrentISODate(new Date()));
            File fileToUpload = new File(this.getClass().getResource("/processmodels/nota-dinas.bpmn").toURI());

            setUser(_konseptor);

            try (InputStream targetStream = new FileInputStream(fileToUpload)) {
                Response response = workflowResource.submit(rahasia.getIdJenisDokumen(), request, targetStream, null);
                result = (ProcessDefinitionDto) response.getEntity();
            }

            assertNotNull(result);

            deploymentId = result.getDeploymentId();

            ProcessDefinition activated = processDefinitionService.getActivated(formDefinition.getJenisDokumen().getIdJenisDokumen());

            ProcessDefinitionFormVarResolver processDefinitionFormVarResolver = new ProcessDefinitionFormVarResolver();
            processDefinitionFormVarResolver.setProcessDefinition(activated);
            processDefinitionFormVarResolver.setVarName("FormField_Pemeriksa");
            processDefinitionFormVarResolver.setValueResolver(SuratPemeriksaResolver.class.getName());
            processDefinitionFormVarResolverService.create(processDefinitionFormVarResolver);

            activated.getFormVars().add(processDefinitionFormVarResolver);

            processDefinitionFormVarResolver = new ProcessDefinitionFormVarResolver();
            processDefinitionFormVarResolver.setProcessDefinition(activated);
            processDefinitionFormVarResolver.setVarName("FormField_Penandatangan");
            processDefinitionFormVarResolver.setValueResolver(SuratPenandatanganResolver.class.getName());
            processDefinitionFormVarResolverService.create(processDefinitionFormVarResolver);

            activated.getFormVars().add(processDefinitionFormVarResolver);

            processDefinitionFormVarResolver = new ProcessDefinitionFormVarResolver();
            processDefinitionFormVarResolver.setProcessDefinition(activated);
            processDefinitionFormVarResolver.setVarName("FormField_KonseptorEqPenandatangan");
            processDefinitionFormVarResolver.setValueResolver(SuratKonseptorEqPenandatanganResolver.class.getName());
            processDefinitionFormVarResolverService.create(processDefinitionFormVarResolver);

            activated.getFormVars().add(processDefinitionFormVarResolver);
            AtomicReference<ProcessInstance> processInstance = new AtomicReference<>();

            Long suratId = surat.getId();
            workflowProvider.startWorkflow(surat, activated, ProcessEventHandler.DEFAULT.onProcessCreated(pi -> {
                Surat s = suratService.find(suratId);
                pi.setSurat(s);
                processInstance.set(pi);
            }), new HashMap<>());

//            workflowProvider.terminateProcess(processInstance.get());

            TaskActionRequestDto requestDto = new TaskActionRequestDto();
            requestDto.setRecordRefId(surat.getId().toString());

            setUser(_pemeriksa1.getOrganization());
            TaskActionResponseDto entity = getTaskActions(rahasia.getIdJenisDokumen(), surat);

            MasterDelegasi masterDelegasi = new MasterDelegasi();
            masterDelegasi.setFrom(_pemeriksa1.getOrganization());
            masterDelegasi.setTo(_pelakhar);
            masterDelegasi.setTipe("Pelakhar");
            masterDelegasi.setAlasan("Cuti");
            masterDelegasi.setMulai(new Date());
            masterDelegasi.setSelesai(dateUtil.getDefValue());
            masterDelegasi.setCompanyCode(null);
            masterDelegasi.setIsDeleted(false);
            masterDelegasiService.create(masterDelegasi);

            approve(entity, "Setujui", "[SKIP]");

            setUser(_pelakhar);
            entity = getTaskActions(rahasia.getIdJenisDokumen(), surat);

            masterDelegasiService.remove(masterDelegasi);

            approve(entity, "Setujui", "[SKIP]");

            setUser(_pemeriksa1.getOrganization());
            entity = getTaskActions(rahasia.getIdJenisDokumen(), surat);
            approve(entity, "Setujui", "pemeriksa2 OK");
//
//            setUser(_pemeriksa2.getOrganization());
//            entity = getTaskActions(rahasia.getIdJenisDokumen(), surat);
//            approve(entity, "Setujui", "pemeriksa2 OK");

//            setUser(_pelakhar);
//            entity = getTaskActions(rahasia.getIdJenisDokumen(), surat);
//            approve(entity, "TO PYMT", "pelakhar TO PYMT");
//
//            setUser(_pymt);
//            entity = getTaskActions(rahasia.getIdJenisDokumen(), surat);
//            approve(entity, "APPROVE", "pymt OK");

//            setUser(_penandatangan1.getOrganization());
//            entity = getTaskActions(rahasia.getIdJenisDokumen(), surat);
//            approve(entity, "Kembalikan ke Pemeriksa|1", "penandatangan1 OK");

//            setUser(_pemeriksa1.getOrganization());
//            entity = getTaskActions(rahasia.getIdJenisDokumen(), surat);
//            approve(entity, "Setujui", "pemeriksa1 OK");

            ProcessStatus processStatus = workflowProvider.getProcessStatus(surat);
            assertNotNull(processStatus);
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (deploymentId != null) {
                    workflowProvider.undeploy(deploymentId);
                }
            } catch (Exception ignored) {

            }


            if (formDefinition != null) {
                ProcessDefinition activated = processDefinitionService.getActivated(formDefinition.getJenisDokumen().getIdJenisDokumen());

                if (activated != null) {
                    em.refresh(activated);

                    activated.getInstances().forEach(t -> {
                        em.refresh(t);
                    });

                    processDefinitionService.remove(activated);
                }
            }

//            List<MasterDelegasi> masterDelegasis = masterDelegasiService.getAll().toList();
//
//            masterDelegasis.forEach(t -> {
//                masterDelegasiService.delete(t.getId());
//            });

            if (surat != null && surat.getId() != null) {
                em.refresh(surat);
                surat.getPemeriksaPelakharPymtSurats().forEach(pemeriksaPelakharPymtSurat -> pemeriksaPelakharPymtSuratService.remove(pemeriksaPelakharPymtSurat));
                surat.getPenandatanganPelakharPymtSurats().forEach(penandatanganPelakharPymtSurat -> penandatanganPelakharPymtSuratService.remove(penandatanganPelakharPymtSurat));
                surat.getPenandatanganSurat().forEach(penandatanganSurat -> penandatanganSuratService.remove(penandatanganSurat));
                surat.getPemeriksaSurat().forEach(pemeriksaSurat -> pemeriksaSuratService.remove(pemeriksaSurat));
                surat.getPenandatanganSurat().clear();
                surat.getPemeriksaSurat().clear();

                suratService.remove(surat);
            }

            if (formDefinition != null && formDefinition.getId() != null) {
                em.refresh(formDefinition);
                formDefinitionService.remove(formDefinition);
            }
        }
    }

    private TaskActionResponseDto getTaskActions(Long idJenisDokumen, final Surat surat) {
        return (TaskActionResponseDto) workflowResource.getTaskActions(idJenisDokumen, new TaskActionRequestDto(){{setRecordRefId(surat.getId().toString());}}).getEntity();
    }

    private void approve(final TaskActionResponseDto entity, String approval, String comment) {
        workflowResource.taskAction(new TaskActionDto(){
            {
                setTaskId(entity.getTaskId());
                setForms(Arrays.asList(
                        new FormFieldDto(){{
                            setName(entity.getForms().stream().filter(t -> t.getLabel().equals("Approval")).map(FormFieldDto::getName).findFirst().orElse(null));
                            setValue(approval);
                        }},
                        new FormFieldDto(){{
                            setName("Comment");
                            setValue(comment);
                        }}));
            }
        }).getEntity();
    }

    private void disposisi(final TaskActionResponseDto entity, String disposisi) {
        workflowResource.taskAction(new TaskActionDto(){
            {
                setTaskId(entity.getTaskId());
                setForms(Arrays.asList(
                        new FormFieldDto(){{
                            setName(entity.getForms().stream().filter(t -> t.getLabel().equals("Disposisi")).map(FormFieldDto::getName).findFirst().orElse(null));
                            setValue(disposisi);
                        }}));
            }
        }).getEntity();
    }

    private void setUser(MasterStrukturOrganisasi masterStrukturOrganisasi) {
        userSession.setUserSession(new UserSession());
        userSession.getUserSession().setUser(new MasterUser(){
            {
                setOrganizationEntity(masterStrukturOrganisasi);
            }
        });
    }

    private Session userSession = new Session() {};

    public Session getUserSession() {
        return userSession;
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
