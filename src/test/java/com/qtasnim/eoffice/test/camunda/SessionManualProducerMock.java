package com.qtasnim.eoffice.test.camunda;

import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.IRequestContext;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Transaction;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

public class SessionManualProducerMock {
    @Inject
    private PenomoranManualTest engineTest;

    /**
     * Mock UserSession
     * @return
     */
    @Produces
    @ISessionContext
    private Session produceSession() {
        return engineTest.getUserSession();
    }

    /**
     * Mock Transaction
     * @return
     */
    @Produces
    @IRequestContext
    private Transaction produceLogger() {
        return new Transaction();
    }
}
