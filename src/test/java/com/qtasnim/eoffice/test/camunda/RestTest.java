package com.qtasnim.eoffice.test.camunda;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.workflows.camunda.dto.ProcessEngineDto;
import com.qtasnim.workflows.camunda.dto.VariableValueDto;
import com.qtasnim.workflows.camunda.dto.history.HistoricVariableInstanceDto;
import com.qtasnim.workflows.camunda.dto.repository.DeploymentWithDefinitionsDto;
import com.qtasnim.workflows.camunda.dto.runtime.ProcessInstanceDto;
import com.qtasnim.workflows.camunda.dto.runtime.StartProcessInstanceDto;
import com.qtasnim.workflows.camunda.dto.task.CompleteTaskDto;
import com.qtasnim.workflows.camunda.dto.task.TaskDto;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.ExtensionElements;
import org.camunda.bpm.model.bpmn.instance.SubProcess;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaFormData;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaFormField;
import org.camunda.spin.json.SpinJsonNode;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertNotNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.camunda.spin.Spin.JSON;

public class RestTest {
    public static final String BASE_ADDRESS = "http://camunda.qtasnim.com/engine-rest";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void korespondensiTest() throws IOException, URISyntaxException {
        String name = "korespondensi+tandatangan";
        String tenant = "eoffice";
        String source = "com.qtasnim.eoffice";

        //CHECK DEPLOYMENT LIST
        List<DeploymentWithDefinitionsDto> deploymentList = deploymentList();
        assertThat(deploymentList).isNotEmpty();

        //CREATE DEPLOYMENT
        DeploymentWithDefinitionsDto newDeployment = deploymentCreate(name, tenant, source);
        assertThat(newDeployment.getName()).isEqualTo(name);

        //SUBMITTED PROCESS
        ProcessInstanceDto processInstanceDto = startProcess(
                newDeployment.getDeployedProcessDefinitions().entrySet().stream().map(t -> t.getValue().getId()).findFirst().orElse(""), vars -> {
                    vars.put("FormField_Pemeriksa", new VariableValueDto(){{
                        setType("json");
                        setValue("[{\"assignee\": \"111\", \"pelakhar\": null, \"pymt\": \"333\"}, {\"assignee\": \"777\", \"pelakhar\": \"999\", \"pymt\": \"888\"}]");
                        setValueInfo(new HashMap<>());
                    }});
                    vars.put("FormField_Penandatangan", new VariableValueDto(){{
                        setType("json");
                        setValue("[{\"assignee\": \"444\", \"pelakhar\": null, \"pymt\": \"666\"}]");
                        setValueInfo(new HashMap<>());
                    }});
                    vars.put("FormField_KonseptorEqPenandatangan", new VariableValueDto(){{
                        setType("Boolean");
                        setValue(false);
                        setValueInfo(new HashMap<>());
                    }});
                });

        //DELETE DEPLOYMENT
        deleteDeployment(newDeployment.getId());
    }

    @Test
    public void engineTest() throws IOException {
        Client client = getClient();

        WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS).path("engine").toString());
        Response response = webResource.request(MediaType.MULTIPART_FORM_DATA_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).get();
        List<ProcessEngineDto> list = getObjectMapper().readValue(response.readEntity(String.class), new TypeReference<List<ProcessEngineDto>>(){});

        assertThat(list).hasSize(1);
        assertThat((list.get(0)).getName()).isEqualTo("default");
    }

    @Test
    public void processInstanceTest() throws IOException {
        Client client = getClient();

        WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS).path("process-instance").path("6ea4d969-3bb4-11e9-b37a-0242ac11000b").toString());
        Response response = webResource.request(MediaType.MULTIPART_FORM_DATA_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).get();
        ProcessInstanceDto instance = getObjectMapper().readValue(response.readEntity(String.class), new TypeReference<ProcessInstanceDto>(){});

        assertNotNull(instance);
    }

    @Test
    public void deleteDeploymentTest() throws IOException {
        Client client = getClient();

        WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS).path("deployment")
                .path("59e41a91-3e14-11e9-b37a-0242ac11000b")
                .queryParam("cascade", true).toString());
        Response response = webResource.request(MediaType.MULTIPART_FORM_DATA_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).delete();
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    public void deploymentTest() throws IOException, URISyntaxException {
        String name = "surat-keluar";
        String tenant = "eoffice";
        String source = "com.qtasnim.eoffice";
        String[] pemeriksaAssignees = {"demo1", "demo2"};
        String[] penandatanganAssignees = {"demo3", "demo4"};
        String publisherAssignee = "demo5";

        //CHECK DEPLOYMENT LIST
        List<DeploymentWithDefinitionsDto> deploymentList = deploymentList();
        assertThat(deploymentList).isNotEmpty();

        //CREATE DEPLOYMENT
        DeploymentWithDefinitionsDto newDeployment = deploymentCreate(name, tenant, source);
        assertThat(newDeployment.getName()).isEqualTo(name);

        //SUBMITTED PROCESS
        ProcessInstanceDto processInstanceDto = startProcess(
                newDeployment.getDeployedProcessDefinitions().entrySet().stream().map(t -> t.getValue().getId()).findFirst().orElse(""),
                pemeriksaAssignees,
                penandatanganAssignees,
                publisherAssignee);

        //CHECK ACTIVE TASKS: [demo1, demo2]
        List<TaskDto> activeTasks = getActiveTasks(processInstanceDto);

        //DELETE DEPLOYMENT
        deleteDeployment(newDeployment.getId());
    }

    @Test
    public void approveAllTest() throws IOException, URISyntaxException {
        String name = "surat-keluar";
        String tenant = "eoffice";
        String source = "com.qtasnim.eoffice";
        String[] pemeriksaAssignees = {"demo1", "demo2"};
        String[] penandatanganAssignees = {"demo3", "demo4"};
        String publisherAssignee = "demo5";

        //CREATE DEPLOYMENT
        DeploymentWithDefinitionsDto newDeployment = deploymentCreate(name, tenant, source);
        assertThat(newDeployment.getName()).isEqualTo(name);

        //SUBMITTED PROCESS
        ProcessInstanceDto processInstanceDto = startProcess(
                newDeployment.getDeployedProcessDefinitions().entrySet().stream().map(t -> t.getValue().getId()).findFirst().orElse(""),
                pemeriksaAssignees,
                penandatanganAssignees,
                publisherAssignee);

        //CHECK ACTIVE TASKS: [demo1, demo2]
        List<TaskDto> activeTasks = getActiveTasks(processInstanceDto);

        //APPROVE ALL TASKS
        while (activeTasks.size() > 0) {
            for (TaskDto task : activeTasks) {
                CompleteTaskDto submission = new CompleteTaskDto();
                Map<String, VariableValueDto> vars = new HashMap<>();

                if (task.getTaskDefinitionKey().equals("UserTask_Pemeriksa")) {
                    vars.put("FormField_PemeriksaApproved", new VariableValueDto(){{
                        setType("Boolean");
                        setValue(true);
                        setValueInfo(new HashMap<>());
                    }});
                } else if (task.getTaskDefinitionKey().equals("UserTask_Penandatangan")) {
                    vars.put("FormField_PenandatanganApproved", new VariableValueDto(){{
                        setType("Boolean");
                        setValue(true);
                        setValueInfo(new HashMap<>());
                    }});
                }

                submission.setVariables(vars);

                approval(task, submission);
            }

            activeTasks = getActiveTasks(processInstanceDto);
        }

        //CHECK PROCESS STATUS VAR
        WorkflowStatus processStatus = getProcessStatus(processInstanceDto);

        //DELETE DEPLOYMENT
        deleteDeployment(newDeployment.getId());
    }

    @Test
    public void rejectTest() throws IOException, URISyntaxException {
        String name = "surat-keluar";
        String tenant = "eoffice";
        String source = "com.qtasnim.eoffice";
        String[] pemeriksaAssignees = {"demo1", "demo2"};
        String[] penandatanganAssignees = {"demo3", "demo4"};
        String publisherAssignee = "demo5";

        //CHECK DEPLOYMENT LIST
        List<DeploymentWithDefinitionsDto> deploymentList = deploymentList();
        assertThat(deploymentList).isNotEmpty();

        //CREATE DEPLOYMENT
        DeploymentWithDefinitionsDto newDeployment = deploymentCreate(name, tenant, source);
        assertThat(newDeployment.getName()).isEqualTo(name);

        //SUBMITTED PROCESS
        ProcessInstanceDto processInstanceDto = startProcess(
                newDeployment.getDeployedProcessDefinitions().entrySet().stream().map(t -> t.getValue().getId()).findFirst().orElse(""),
                pemeriksaAssignees,
                penandatanganAssignees,
                publisherAssignee);

        //CHECK ACTIVE TASKS: [demo1, demo2]
        List<TaskDto> activeTasks = getActiveTasks(processInstanceDto);

        //REJECT A TASK [demo1]
        CompleteTaskDto submission = new CompleteTaskDto();
        Map<String, VariableValueDto> vars = new HashMap<>();
        vars.put("FormField_PemeriksaApproved", new VariableValueDto(){{
            setType("Boolean");
            setValue(false);
            setValueInfo(new HashMap<>());
        }});
        submission.setVariables(vars);

        approval(activeTasks.get(0), submission);

        //CHECK ACTIVE TASK AFTER REJECTION
        activeTasks = getActiveTasks(processInstanceDto);

        //CHECK PROCESS STATUS VAR
        WorkflowStatus processStatus = getProcessStatus(processInstanceDto);

        //DELETE DEPLOYMENT
        deleteDeployment(newDeployment.getId());
    }

    private ProcessInstanceDto startProcess(String id, Consumer<Map<String, VariableValueDto>> mapSetup) {
        try {
            Client client = getClient();
            WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS)
                    .path("process-definition")
                    .path(id)
                    .path("submit-form")
                    .toString());

            StartProcessInstanceDto submission = new StartProcessInstanceDto();
            submission.setBusinessKey(UUID.randomUUID().toString());
            Map<String, VariableValueDto> vars = new HashMap<>();
            mapSetup.accept(vars);
            submission.setVariables(vars);

            Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(submission, MediaType.APPLICATION_JSON_TYPE));
            String content = response.readEntity(String.class);
            System.out.println(content);
            return getObjectMapper().readValue(content, ProcessInstanceDto.class) ;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private ProcessInstanceDto startProcess(String id, String[] pemeriksaAssignees, String[] penandatanganAssignees, String publisherAssignee) {
        try {
            Client client = getClient();
            WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS)
                    .path("process-definition")
                    .path(id)
                    .path("submit-form")
                    .toString());

            StartProcessInstanceDto submission = new StartProcessInstanceDto();
            submission.setBusinessKey(UUID.randomUUID().toString());
            Map<String, VariableValueDto> vars = new HashMap<>();
            vars.put("FormField_Pemeriksa", new VariableValueDto(){{
                setType("json");
                setValue(getObjectMapper().writeValueAsString(pemeriksaAssignees));
                setValueInfo(new HashMap<>());
            }});
            vars.put("FormField_Penandatangan", new VariableValueDto(){{
                setType("json");
                setValue(getObjectMapper().writeValueAsString(penandatanganAssignees));
                setValueInfo(new HashMap<>());
            }});
            vars.put("FormField_Publisher", new VariableValueDto(){{
                setType("String");
                setValue(getObjectMapper().writeValueAsString(publisherAssignee));
                setValueInfo(new HashMap<>());
            }});
            submission.setVariables(vars);

            Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(submission, MediaType.APPLICATION_JSON_TYPE));
            String content = response.readEntity(String.class);
            System.out.println(content);
            return getObjectMapper().readValue(content, ProcessInstanceDto.class) ;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<TaskDto> getActiveTasks(ProcessInstanceDto newProcess) {
        try {
            Client client = getClient();
            WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS)
                    .path("task")
                    .queryParam("processInstanceId", newProcess.getId())
                    .queryParam("assigned", true)
                    .toString());

            Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).get();
            String content = response.readEntity(String.class);
            System.out.println(content);
            return getObjectMapper().readValue(content, new TypeReference<List<TaskDto>>(){}) ;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    private void approval(TaskDto task, CompleteTaskDto submission) {
        try {
            Client client = getClient();
            WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS)
                    .path("task")
                    .path(task.getId())
                    .path("submit-form")
                    .toString());

            Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(submission, MediaType.APPLICATION_JSON_TYPE));
            String content = response.readEntity(String.class);
            System.out.println(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private WorkflowStatus getProcessStatus(ProcessInstanceDto newProcess) {
        try {
            Client client = getClient();
            WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS)
                    .path("history")
                    .path("variable-instance")
                    .queryParam("processInstanceId", newProcess.getId())
                    .queryParam("variableName", "Status")
                    .toString());

            Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).get();
            String content = response.readEntity(String.class);
            System.out.println(content);
            List<HistoricVariableInstanceDto>  vars = getObjectMapper().readValue(content, new TypeReference<List<HistoricVariableInstanceDto>>() {
            });

            switch (vars.stream().map(t -> t.getValue().toString()).findFirst().orElse("")) {
                case "Pending": {
                    return WorkflowStatus.PENDING;
                }
                case "Approved": {
                    return WorkflowStatus.APPROVED;
                }
                case "Rejected": {
                    return WorkflowStatus.REJECTED;
                }
                default:
                    return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    enum WorkflowStatus {
        PENDING, APPROVED, REJECTED
    }

    private List<DeploymentWithDefinitionsDto> deploymentList() throws IOException {
        Client client = getClient();
        WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS).path("deployment").toString());
        Response response = webResource.request(MediaType.MULTIPART_FORM_DATA_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).get();
        return getObjectMapper().readValue(response.readEntity(String.class), new TypeReference<List<DeploymentWithDefinitionsDto>>(){});
    }

    private DeploymentWithDefinitionsDto deploymentCreate(String name, String tenant, String source) throws URISyntaxException, IOException {
        Client client = getClient();
        File fileToUpload = new File(this.getClass().getResource("/processmodels/" + name + ".bpmn").toURI());
        FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("*", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE);
        FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
        fileDataBodyPart.setContentDisposition(FormDataContentDisposition.name("file").fileName(fileToUpload.getName()).build());
        FormDataMultiPart multipart = (FormDataMultiPart) formDataMultiPart
                .field("deployment-name", name)
                .field("enable-duplicate-filtering", "true")
                .field("deploy-changed-only", "false")
                .field("deployment-source", source)
                .field("tenant-id", tenant)
                .bodyPart(fileDataBodyPart);

        WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS).path("deployment").path("create").toString());
        Response response = webResource.request(MediaType.MULTIPART_FORM_DATA_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(multipart, multipart.getMediaType()));
        String content = response.readEntity(String.class);
        System.out.println(content);
        DeploymentWithDefinitionsDto newDeployment = getObjectMapper().readValue(content, DeploymentWithDefinitionsDto.class) ;
        formDataMultiPart.close();
        multipart.close();

        assertThat(response.getStatus()).isEqualTo(200);
        return newDeployment;
    }

    private void deleteDeployment(String id) {
        Client client = getClient();
        WebTarget webResource = client.target(UriBuilder.fromUri(BASE_ADDRESS).path("deployment")
                .path(id)
                .queryParam("cascade", true).toString());
        Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).delete();
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    public void spinTest() {
        String input = "[[{\"assignee\": \"123\", \"pelakhar\": null, \"pymt\": null}]]";
        SpinJsonNode json = JSON(new String(input));
    }

    @Test
    public void spinTest2() throws JsonProcessingException {
        String input = getObjectMapper().writeValueAsString(Arrays.asList("a", "b", "c"));
        SpinJsonNode json = JSON(new String(input));
    }

    @Test
    public void subprocessIdentifyTest() throws URISyntaxException, IOException {
        File fileToUpload = new File(this.getClass().getResource("/processmodels/korespondensi+tandatangan.bpmn").toURI());

        try (InputStream inputStream = new FileInputStream(fileToUpload)) {
            BpmnModelInstance modelInstance = Bpmn.readModelFromStream(inputStream);

            List<SubProcess> list = modelInstance
                    .getModelElementsByType(modelInstance.getModel().getType(ExtensionElements.class))
                    .stream()
                    .filter(t -> t.getParentElement() instanceof UserTask && ((UserTask) t.getParentElement()).getId().equals("Task_Pemeriksa_Approval"))
                    .map(t -> (UserTask) t.getParentElement())
                    .filter(t -> t.getParentElement() instanceof SubProcess)
                    .map(t -> (SubProcess) t.getParentElement())
                    .collect(Collectors.toList());

            list.size();
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    private Client getClient() {
        final ClientConfig clientConfig = new ClientConfig()
                .register(MultiPartFeature.class);

        return ClientBuilder.newClient(clientConfig);
    }
}
