package com.qtasnim.eoffice.test.sign;

import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.qtasnim.eoffice.services.DigiSignService;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.util.UUID;

import static org.junit.Assert.assertNotNull;

public class SignTest {
    @Test
    public void test() throws Exception {
        DigiSignService digiSignService = new DigiSignService();
        String keyStoreInstance = "JKS";
        String password = "pass@word1";
        String alias = "kai";
        String signedFileName = String.format("%s.pdf", UUID.randomUUID().toString());
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        byte[] keyStoreByte = digiSignService.getKeyStore(
                keyStoreInstance,
                alias,
                "R Seno Anggoro",
                "PT Kereta Api Indonesia",
                "Sistem Informasi",
                "Bandung", //organization.persa.kota
                "Jawa Barat", //organization.persa.kota.propinsi
                password
        );

        KeyStore ks = KeyStore.getInstance(keyStoreInstance);

        try (InputStream keyStream = new ByteArrayInputStream(keyStoreByte)) {
            ks.load(keyStream, password.toCharArray());
        }

        PrivateKey privateKey = (PrivateKey) ks.getKey(alias, password.toCharArray());
        ExternalSignature privateKeySignature = new PrivateKeySignature(privateKey, DigestAlgorithms.SHA256, "BC");

        byte[] fileInput = FileUtils.readFileToByteArray(new File(this.getClass().getResource("/dummyfiles/SuratMasuk/1.pdf").toURI()));

        assertNotNull(fileInput);

        byte[] signed = digiSignService.sign(privateKeySignature, ks.getCertificateChain(alias), fileInput);

        FileUtils.writeByteArrayToFile(new File(signedFileName), signed);
        File signedFile = new File(signedFileName);

        if (signedFile.exists()) {
            signedFile.delete();
        }
    }
}
