package com.qtasnim.eoffice.test.sign;

import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.MasterDigisign;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.services.IDigiSignService;
import com.qtasnim.eoffice.services.MasterDigisignService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.io.File;
import java.io.InputStream;

@RunWith(Arquillian.class)
@Singleton
public class BssnTest
        extends EjbUnitTestBase
{
    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.addClasses(BssnTest.class);
                });
    }

    @Inject
    private MasterDigisignService masterDigisignService;

    @Test
    public void testEjb() throws Exception {
        MasterDigisign masterDigisign = masterDigisignService.getProvider(ApplicationConfig.defaultConfig().getDigisignProvider());
        IDigiSignService digiSignService = masterDigisign.getWorkflowProvider();
        InputStream file = this.getClass().getResourceAsStream("/dummyfiles/SuratMasuk/1.pdf");
        byte[] fileContent = IOUtils.toByteArray(file);
        MasterStrukturOrganisasi strukturOrganisasi = new MasterStrukturOrganisasi(){
            {
                setUser(new MasterUser(){
                    {
                        setLoginUserName("30122019");
                    }
                });
            }
        };

        byte[] result = digiSignService.onSigning(map -> {
            map.put("passphrase", "#4321qwer*");
            map.put("tampilan", "invisible");
        }).sign(strukturOrganisasi, fileContent);

        if (result != null) {
            File fileResult = new File("signed.pdf");
            FileUtils.writeByteArrayToFile(fileResult, result);

            if (fileResult.exists()) {
                fileResult.delete();
            }
        }
    }

}
