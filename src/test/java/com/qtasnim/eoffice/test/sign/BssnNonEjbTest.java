package com.qtasnim.eoffice.test.sign;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.bssn.BSSNSignFailException;
import com.qtasnim.eoffice.bssn.CommonResultDto;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.Boundary;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class BssnNonEjbTest {
    @Test
    public void test() throws Exception {
        InputStream file = this.getClass().getResourceAsStream("/dummyfiles/SuratMasuk/1.pdf");
        byte[] fileContent = IOUtils.toByteArray(file);
        byte[] result = null;

        Client client = getClient();
        WebTarget webTarget = client.target(UriBuilder.fromUri("http://172.16.12.243")
                .path("/api/sign/pdf")
                .toString());

        StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart();
        streamDataBodyPart.setName("file");

        try (InputStream streamEntity = new ByteArrayInputStream(fileContent)) {
            streamDataBodyPart.setStreamEntity(streamEntity, MediaType.valueOf("application/pdf"));
        }

        try (FormDataMultiPart formDataMultiPart = new FormDataMultiPart()) {
            try (FormDataMultiPart part = (FormDataMultiPart) formDataMultiPart
                    .field("nik", "30122019")
                    .field("passphrase", "#4321qwer*")
                    .field("tampilan", "invisible")
                    .bodyPart(streamDataBodyPart)) {
                MediaType contentType = part.getMediaType();
                contentType = Boundary.addBoundary(contentType);

                Response response = webTarget
                        .request(MediaType.WILDCARD)
                        .header("Authorization", String.format("Basic %s", "YWRtaW46cXdlcnR5"))
                        .post(Entity.entity(part, contentType));

                if (response.getStatus() != 200) {
                    ObjectMapper objectMapper = getObjectMapper();
                    String stringEntity = response.readEntity(String.class);
                    String errorMessage = "Unknown";

                    if (StringUtils.isNotEmpty(stringEntity)) {
                        CommonResultDto commonResultDto = objectMapper.readValue(stringEntity, CommonResultDto.class);
                        errorMessage = getMessage(commonResultDto);
                    }

                    throw new BSSNSignFailException(errorMessage);
                } else if (response.getStatus() == 200) {
                    try (InputStream is = response.readEntity(InputStream.class)) {
                        result = IOUtils.toByteArray(is);
                    }
                }
            }
        }

        if (result != null) {
            File fileResult = new File("signed.pdf");
            FileUtils.writeByteArrayToFile(fileResult, result);

            if (fileResult.exists()) {
                fileResult.delete();
            }
        }
    }

    private Client getClient() {
        final ClientConfig clientConfig = new ClientConfig()
                .register(MultiPartFeature.class)
                .register(new LoggingFeature(Logger.getLogger(getClass().getName()), Level.OFF, LoggingFeature.Verbosity.PAYLOAD_TEXT, 8192));

        return ClientBuilder.newClient(clientConfig);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    private String getMessage(CommonResultDto commonResultDto) {
        return Optional.ofNullable(commonResultDto.getError())
                .filter(StringUtils::isNotEmpty)
                .map(t -> Stream.of(t.split(":")).reduce((first, second) -> second).map(String::trim).orElse(commonResultDto.getMessage()))
                .orElse(commonResultDto.getMessage());
    }
}
