package com.qtasnim.eoffice.test.jms;

import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;
import java.io.IOException;

@RunWith(Arquillian.class)
public class QueueTest extends EjbUnitTestBase {
    @Resource(mappedName = Constants.JNDI_MAIL_QUEUE)
    private Queue queue;

    @Inject
    private JMSContext jmsContext;

    @Inject
    private Logger logger;

    @Deployment
    public static Archive<?> deploy() throws IOException {
        return createDeployment(QueueTest.class);
    }

    @Test
    public void test() {
        logger.info("test");
        System.out.println("test");

        JMSProducer producer = jmsContext.createProducer();

        for (int i = 0; i < 2; i++) {
            producer.send(queue, String.valueOf(i));
        }
    }
}
