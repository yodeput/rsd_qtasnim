package com.qtasnim.eoffice.test.konversi;

import com.qtasnim.eoffice.services.KonversiOnlyOfficeService;
import com.qtasnim.eoffice.services.OCRService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.test.tess4j.OcrTest;
import org.apache.commons.io.IOUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.*;
import java.net.URISyntaxException;

import static junit.framework.TestCase.assertNotNull;

@RunWith(Arquillian.class)
public class toPdf extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() throws IOException {
        return createDeployment(
                OcrTest.class,
                OCRService.class);
    }

    @Inject
    KonversiOnlyOfficeService convService;
//
//    @Test
//    public void test1() throws Exception {
//        File fileToUpload = new File(this.getClass().getResource("/convert/1.docx").toURI());
//
//        byte[] content = null;
//
//        try (FileInputStream inputStream = new FileInputStream(fileToUpload)) {
//            content = IOUtils.toByteArray(inputStream);
//        }
//
//        assertNotNull(content);
//
//        byte[] result = convService.convert(content, "docx", "pdf");
//
//        assertNotNull(result);
//
//        OutputStream out = new FileOutputStream("out.pdf");
//        out.write(result);
//        out.close();
//    }
//}

//public class toPdf {



    @Test
    public void test1() throws Exception {
//        KonversiOnlyOfficeService convService = new KonversiOnlyOfficeService();

        File fileToUpload = new File(this.getClass().getResource("/temp_doc/1.docx").toURI());

        byte[] content;
        try (FileInputStream fileInputStream = new FileInputStream(fileToUpload)) {
            content = new byte[fileInputStream.available()];
            fileInputStream.read(content);
        }

//        byte[] content = null;
//
//        try (FileInputStream inputStream = new FileInputStream(fileToUpload)) {
//            content = IOUtils.toByteArray(inputStream);
//        }

        assertNotNull(content);

        byte[] result = convService.convert(content, ".docx", ".pdf");

        assertNotNull(result);
    }
}

