package com.qtasnim.eoffice.test.konversi;

import com.google.zxing.WriterException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.*;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.util.ImageProcessing;
import com.qtasnim.eoffice.util.PdfModify;
import com.qtasnim.eoffice.util.QRCodeGenerateProcess;
import org.junit.Test;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;

public class PdfMod {

    @Test
    public void test1() throws IOException, DocumentException, URISyntaxException {
        String pdfSrc = "D://addqr1.pdf";
        String pdfDest = "D://qr-added.pdf";
        String qrcode = this.getClass().getResource("/convert/qrcode.png").toURI().toString();

        manipulatePdf(pdfSrc, pdfDest, qrcode);

        System.out.println("yey");
    }

    @Test
    public void test1_5() throws IOException, DocumentException, URISyntaxException, WriterException {
        String pdfSrc = "D://addqr1.pdf";
        String pdfDest = "D://qr-added.pdf";

        manipulate3(pdfSrc, pdfDest);

        System.out.println("yey");
    }

    @Test
    public void test2() throws URISyntaxException {
        String pdfSrc = "D://addqr2.pdf";
        String qrcode = "D://qrcode.png";

        manipulate2(pdfSrc, qrcode);
    }

    @Test
    public void test3() throws URISyntaxException, IOException, WriterException {
        QRCodeGenerateProcess qrCodeGenerateProcess = new QRCodeGenerateProcess(ApplicationConfig.defaultConfig());
        PdfModify pdfModify = new PdfModify();

        byte[] pdf = null;

        File fileToUpload = new File("D://addqr1.pdf");

        try (FileInputStream fileInputStream = new FileInputStream(fileToUpload)) {
            pdf = new byte[fileInputStream.available()];
            fileInputStream.read(pdf);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] qrcode = qrCodeGenerateProcess.generateQRCodeWithLogo("qwertyuiopasdfghjkl", 80);

        byte[] pdfModed = pdfModify.addQRCode(pdf, qrcode);

        System.out.println("yey");
    }

    private void manipulatePdf(String pdfSrc, String pdfRes, String imageToAdd) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(pdfSrc);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(pdfRes));
        Image image = Image.getInstance(imageToAdd);
        PdfImage stream = new PdfImage(image, "", null);
        stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
        PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
        image.setDirectReference(ref.getIndirectReference());
        image.setAbsolutePosition(reader.getPageSize(1).getWidth()-image.getWidth(), reader.getPageSize(1).getHeight()-image.getHeight());

        for(int i=1; i<=reader.getNumberOfPages(); i++) {
            PdfContentByte over = stamper.getOverContent(i);
            over.addImage(image);
        }

        stamper.close();
        reader.close();
    }

    private void manipulate2(String pdfSrc, String imageToAdd) {
        Document doc = new Document();
        try {
            PdfWriter.getInstance(doc, new FileOutputStream(pdfSrc));
            doc.open();

            // Sets the absolute position of the image.
            Image image = Image.getInstance(imageToAdd);
            image.setAbsolutePosition(0f, 0f);
//            doc.add(image);
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        } finally {
            doc.close();
        }
    }

    private void manipulate3(String pdfSrc, String pdfRes) throws IOException, DocumentException, WriterException {
        QRCodeGenerateProcess qrCodeGenerateProcess = new QRCodeGenerateProcess(ApplicationConfig.defaultConfig());

        File temp = new File("D://temp.png");

        ImageIO.write(new ImageProcessing().byteToImage(qrCodeGenerateProcess.generateQRCodeWithLogo()), "png", temp);

        PdfReader reader = new PdfReader(pdfSrc);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(pdfRes));
        Image image = Image.getInstance(temp.getPath());
        PdfImage stream = new PdfImage(image, "", null);
        stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
        PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
        image.setDirectReference(ref.getIndirectReference());
        image.setAbsolutePosition(reader.getPageSize(1).getWidth()-image.getWidth(), reader.getPageSize(1).getHeight()-image.getHeight());

        for(int i=1; i<=reader.getNumberOfPages(); i++) {
            PdfContentByte over = stamper.getOverContent(i);
            over.addImage(image);
        }

        stamper.close();
        reader.close();

        Files.deleteIfExists(temp.toPath());
    }

    @Test
    public void testImagesToTiff() throws IOException {
        String pdfFile = "D:\\Local Disk\\Download\\99023942-275b-4f20-9e61-537ed8ca9fdf.pdf";
        File pdf = new File(pdfFile);

        List<BufferedImage> images = new ImageProcessing().pdfToImages(pdf);

        imagesToTiff(images, new File("D:/t.tif"));
    }

    private void imagesToTiff(List<BufferedImage> images, File outputFile) {

        // Obtain a TIFF writer
        ImageWriter writer = new ImageProcessing().getTiffImageWriter();

        try (ImageOutputStream output = ImageIO.createImageOutputStream(outputFile)) {
            writer.setOutput(output);

            ImageWriteParam params = writer.getDefaultWriteParam();
            params.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);

            // Compression: None, PackBits, ZLib, Deflate, LZW, JPEG and CCITT variants allowed
            // (different plugins may use a different set of compression type names)
            params.setCompressionType("JPEG");

            writer.prepareWriteSequence(null);

            for (BufferedImage image : images) {
                writer.writeToSequence(new IIOImage(image, null, null), params);
            }

            // We're done
            writer.endWriteSequence();
        } catch (IOException e) {
            e.printStackTrace();
        }

        writer.dispose();
    }
}
