package com.qtasnim.eoffice.test.date;

import com.qtasnim.eoffice.util.DateUtil;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DateTest {
    @Test
    public void test() {
        DateUtil dateUtil = new DateUtil();
        String currentISODate = dateUtil.getCurrentISODate();
        Date dateFromISOString = dateUtil.getDateFromISOString(currentISODate);
        String currentISODate2 = dateUtil.getCurrentISODate(dateFromISOString);

        assertEquals(currentISODate, currentISODate2);
    }

    @Test
    public void test2() {
        DateUtil dateUtil = new DateUtil();
        Date date = dateUtil.getDateFromString("1984-01-17", "yyyy-MM-dd");

        assertNotNull(date);
    }
}
