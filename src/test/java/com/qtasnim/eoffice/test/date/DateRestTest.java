package com.qtasnim.eoffice.test.date;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.test.alfresco.AlfrescoTest;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.valid4j.matchers.http.HttpResponseMatchers.hasStatus;

@RunWith(Arquillian.class)
public class DateRestTest extends EjbUnitTestBase {
    @Inject
    private TestModelService testModelService;

    @Deployment
    public static Archive<?> deploy() throws IOException {
        return AlfrescoTest.createDeployment(
                DateDto.class,
                DateResource.class,
                DateRestTest.class,
                DateTest.class,
                TestModel.class,
                TestModelService.class
        );
    }

    @Test
    public void testDateSerialize() throws IOException {
        String isoDate = "2019-07-05T11:23:14.496+07";
        Client client = getClient();

        Response response = client
                .target("http://localhost:8181/api/date")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(new DateDto(isoDate), MediaType.APPLICATION_JSON_TYPE));
        assertThat(response, hasStatus(Response.Status.OK));

        String result = response.readEntity(String.class);

        assertEquals(isoDate, result);





        response = client
                .target("http://localhost:8181/api/date")
                .request(MediaType.APPLICATION_JSON)
                .get();
        assertThat(response, hasStatus(Response.Status.OK));

        result = response.readEntity(String.class);

        DateDto dateDto = getObjectMapper().readValue(result, DateDto.class);

        isoDate = dateDto.getDate();





        response = client
                .target("http://localhost:8181/api/date")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(new DateDto(isoDate), MediaType.APPLICATION_JSON_TYPE));
        assertThat(response, hasStatus(Response.Status.OK));

        result = response.readEntity(String.class);

        assertEquals(isoDate, result);
    }

    @Test
    public void testDbTimezone() throws IOException {
        String isoDate = "2019-07-05T11:23:14.496+07";

        testModelService.createTable();

        Client client = getClient();

        Response response = client
                .target("http://localhost:8181/api/date/save")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(new DateDto(isoDate), MediaType.APPLICATION_JSON_TYPE));
        assertThat(response, hasStatus(Response.Status.OK));
        String result = response.readEntity(String.class);
        DateDto dateDto = getObjectMapper().readValue(result, DateDto.class);
        assertEquals(isoDate, dateDto.getDate());

        testModelService.dropTable();
    }

    private Client getClient() {
        final ClientConfig clientConfig = new ClientConfig()
                .register(MultiPartFeature.class);

        return ClientBuilder.newClient(clientConfig);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }


}
