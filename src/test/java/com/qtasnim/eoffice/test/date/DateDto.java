package com.qtasnim.eoffice.test.date;

import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

@Data
public class DateDto {
    private Long id;
    private String date;

    public DateDto() {

    }

    public DateDto(String date) {
        this.date = date;
    }

    public DateDto(TestModel testModel) {
        if (testModel != null) {
            DateUtil dateUtil = new DateUtil();

            this.id = testModel.getId();
            this.date = dateUtil.getCurrentISODate(testModel.getDateField());
        }
    }
}
