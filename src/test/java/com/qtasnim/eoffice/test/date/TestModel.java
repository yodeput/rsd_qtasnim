package com.qtasnim.eoffice.test.date;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@Entity
@Table(name = "test")
@XmlRootElement
public class TestModel {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name="date_field")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateField;
}
