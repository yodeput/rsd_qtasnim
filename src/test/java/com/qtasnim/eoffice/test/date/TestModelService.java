package com.qtasnim.eoffice.test.date;

import com.qtasnim.eoffice.services.AbstractFacade;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class TestModelService extends AbstractFacade<TestModel> {
    @Override
    protected Class<TestModel> getEntityClass() {
        return TestModel.class;
    }

    public void createTable() {
        getEntityManager().createNativeQuery("CREATE TABLE test (id int NOT NULL PRIMARY KEY AUTO_INCREMENT, date_field TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)").executeUpdate();
    }

    public void dropTable() {
        getEntityManager().createNativeQuery("DROP TABLE test").executeUpdate();
    }
}
