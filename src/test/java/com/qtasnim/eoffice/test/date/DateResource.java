package com.qtasnim.eoffice.test.date;

import com.qtasnim.eoffice.util.DateUtil;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

/**
 * @author seno@qtasnim.com
 */
@Path("date")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class DateResource {
    @Inject
    private Logger logger;

    @Inject
    private TestModelService testModelService;

    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response get() {
        DateUtil dateUtil = new DateUtil();
        String currentISODate = dateUtil.getCurrentISODate();

        DateDto dateDto = new DateDto();
        dateDto.setDate(currentISODate);

        return Response
                .ok(dateDto)
                .build();
    }

    @POST
    @Produces(value = MediaType.TEXT_PLAIN)
    public Response post(DateDto dateDto) {
        if (dateDto != null) {
            DateUtil dateUtil = new DateUtil();
            Date dateFromISOString = dateUtil.getDateFromISOString(dateDto.getDate());

            logger.info(String.format("date iso: %s", dateFromISOString));

            return Response
                    .ok(dateUtil.getCurrentISODate(dateFromISOString))
                    .build();
        }

        return Response
                .status(500)
                .build();
    }

    @POST
    @Path("save")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response save(DateDto dateDto) {
        if (dateDto != null) {
            DateUtil dateUtil = new DateUtil();
            Date dateFromISOString = dateUtil.getDateFromISOString(dateDto.getDate());

            TestModel testModel = new TestModel();
            testModel.setDateField(dateFromISOString);

            testModelService.create(testModel);

            return Response
                    .ok(new DateDto(testModel))
                    .build();
        }

        return Response
                .status(500)
                .build();
    }
}
