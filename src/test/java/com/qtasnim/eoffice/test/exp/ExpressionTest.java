package com.qtasnim.eoffice.test.exp;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.util.ExpressionUtil;
import com.qtasnim.eoffice.util.StringTemplatingUtil;
import org.junit.Test;

import java.io.Serializable;

import static org.junit.Assert.assertNotNull;

public class ExpressionTest {
    @Test
    public void test() {
        MasterUser user = new MasterUser();
        user.setNameFront("A");
        user.setNameMiddleLast("B");

        MasterStrukturOrganisasi smasterStrukturOrganisasirat = new MasterStrukturOrganisasi();
        smasterStrukturOrganisasirat.setOrganizationName("TEST");
        smasterStrukturOrganisasirat.setOrganizationCode("T");
        smasterStrukturOrganisasirat.setUser(user);

        ExpressionUtil expressionUtil = new ExpressionUtil();
        String resolve = (String) expressionUtil.resolve(smasterStrukturOrganisasirat, "$.organizationName | $.organizationCode | $.user.nameFront | $.user.nameMiddleLast");

        assertNotNull(resolve);
    }

    private class Location {
        String country;
        String province;
        String city;
        String address;
    }

    private class Publisher {
        String name;
        String id;
        Location location;
    }

    private class Book implements Serializable {
        String title;
        String author;
        String issn;
        Publisher publisher;
    }

    @Test
    public void test2(){
//        String formatSrc = "${title} | ${author} | ${issn} | ${publisher.name} | ${publisher.location.country},${publisher.location.city}";
//
//        Location location = new Location();
//        location.country = "Wwkwk Land";
//        location.   province = "wkwk01";
//        location.        city = "wkwk01A";
//        location.        address = "St. wkwk01A01";
//
//        Publisher publisher = new Publisher();
//        publisher.        name = "62 publisher";
//        publisher.id = "62kwkwk";
//        publisher.location = location;
//
//        Book book = new Book() ;
//        book.        title = "Hisotry of wkwk";
//        book.    author = "wekaweka";
//        book.    issn = "6262626";
//        book.publisher = publisher;

//        StringTemplatingUtil stutil = new StringTemplatingUtil();
//
//        String resolved = stutil.templating(formatSrc, book);

        String formatSrc = "${organizationName} | ${organizationCode} | ${user.nameFront} | ${user.nameMiddleLast}";

        MasterUser user = new MasterUser();
        user.setNameFront("A");
        user.setNameMiddleLast("B");

        MasterStrukturOrganisasi smasterStrukturOrganisasirat = new MasterStrukturOrganisasi();
        smasterStrukturOrganisasirat.setOrganizationName("TEST");
        smasterStrukturOrganisasirat.setOrganizationCode("T");
        smasterStrukturOrganisasirat.setUser(user);

        StringTemplatingUtil stutil = new StringTemplatingUtil();

        String resolved = stutil.templating(formatSrc, smasterStrukturOrganisasirat);

        assertNotNull(resolved);
    }
}
