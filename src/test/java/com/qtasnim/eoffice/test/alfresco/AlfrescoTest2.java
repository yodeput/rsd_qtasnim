package com.qtasnim.eoffice.test.alfresco;

import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.services.FileService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import org.apache.commons.io.IOUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
public class AlfrescoTest2 extends EjbUnitTestBase {
    @Inject
    private FileService fileService;

    @Deployment
    public static Archive<?> deploy() throws IOException {
        return AlfrescoTest.createDeployment(AlfrescoTest2.class);
    }

    @Test
    public void test() throws CMISProviderException, IOException {
        String fileName = "/dummyfiles/SuratKeluar/old.docx";
        InputStream file = this.getClass().getResourceAsStream(fileName);
        byte[] fileContent = IOUtils.toByteArray(file);
        file.close();

        String id = fileService.upload("old.docx", fileContent);

        byte[] download = fileService.download(id);

        fileName = "/dummyfiles/SuratKeluar/new.docx";
        file = this.getClass().getResourceAsStream(fileName);
        fileContent = IOUtils.toByteArray(file);
        file.close();

        fileService.update(id, fileContent);

        download = fileService.download(id);

        fileService.delete(id);
    }

    @Test
    public void testOnlyOfficeLink() {
        String id = "workspace://SpacesStore/f17bedad-fbc0-48c6-bce9-f8f60a1133b5";

        String editLink = fileService.getEditLink(id);

        assertNotNull(id);
    }
}
