package com.qtasnim.eoffice.test.alfresco;

import com.qtasnim.eoffice.cmis.*;
import com.qtasnim.eoffice.cmis.alfresco.AlfrescoProvider;
import com.qtasnim.eoffice.helpers.FileUtils;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import org.apache.commons.io.IOUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class AlfrescoTest extends EjbUnitTestBase {
    @Inject
    private AlfrescoProvider alfrescoProvider;

    @Inject
    private Logger logger;

    @Deployment
    public static Archive<?> deploy() throws IOException {
        return AlfrescoTest.createDeployment(AlfrescoTest.class);
    }

    @Before
    public void before() throws CMISProviderException {
        getCmisProvider().openConnection();
    }

    @After
    public void after() {
        getCmisProvider().closeConnection();
    }

    @Test
    public void testGetContents() throws CMISProviderException {
        CMISFolder folder = getCmisProvider().getFolder("/Test");

        assertNotNull(folder);

        List<CMISItem> contents = getCmisProvider().getContents(folder);

        assertTrue(contents.size() > 0);
    }

    @Test
    public void testGetDocument() throws CMISProviderException {
        String objectId = "workspace://SpacesStore/f17bedad-fbc0-48c6-bce9-f8f60a1133b5";

        CMISDocument document = getCmisProvider().getDocument(objectId);

        assertNotNull(document);
    }

    @Test
    public void testGetVersions() throws CMISProviderException {
        String objectId = "workspace://SpacesStore/ff57cb76-15c5-4260-be89-af7b3080aed4";

        List<CMISDocument> document = getCmisProvider().getVersions(objectId);

        assertNotNull(document);
    }

    @Test
    public void testUploadDocument() throws CMISProviderException, IOException {
        CMISFolder folder = getCmisProvider().getRoot();

        assertNotNull(folder);

        String fileName = "/dummyfiles/SuratKeluar/1.docx";
        String generatedFileName = FileUtils.generateRandomFileName(fileName);
        InputStream file = this.getClass().getResourceAsStream(fileName);

        byte[] fileContent = IOUtils.toByteArray(file);
        file.close();

        CMISDocument document = getCmisProvider().uploadDocument(folder, fileContent, generatedFileName);

        assertNotNull(document);

        byte[] uploadedContents = getCmisProvider().getDocumentContent(document.getId());

        assertNotNull(uploadedContents);

        getCmisProvider().delete(document);
    }

    @Test
    public void testCreateFolder() throws CMISProviderException {
        CMISFolder folder = getCmisProvider().getRoot();

        assertNotNull(folder);

        String folderName = "create-from-junit";

        CMISFolder newFolder = getCmisProvider().createFolder(folder, folderName);

        assertNotNull(newFolder);

        assertEquals(newFolder.getName(), folderName);

        getCmisProvider().delete(newFolder);
    }

    @Test
    public void uploadDummyDocument() throws CMISProviderException {
        CMISFolder folder = getCmisProvider().getRoot();

        List<String> uploads = Arrays.asList(
                "/dummyfiles/SuratKeluar/1.docx"
                ,"/dummyfiles/SuratKeluar/2.doc"
                ,"/dummyfiles/SuratMasuk/1.pdf"
                ,"/dummyfiles/SuratMasuk/2.pdf"
                ,"/dummyfiles/SuratMasuk/3.pdf"
                );

        uploads.forEach(fileName -> {
            try {
                String generatedFileName = FileUtils.generateRandomFileName(fileName);
                InputStream file = this.getClass().getResourceAsStream(fileName);

                byte[] fileContent = IOUtils.toByteArray(file);
                file.close();

                CMISDocument document = getCmisProvider().uploadDocument(folder, fileContent, generatedFileName);

                assertNotNull(document);
            } catch (IOException | CMISProviderException e) {
                logger.error(null, e);
            }
        });
    }

    private ICMISProvider getCmisProvider() {
        return alfrescoProvider;
    }
}
