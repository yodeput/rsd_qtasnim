package com.qtasnim.eoffice.test.masterdata;

import com.qtasnim.eoffice.services.DataMasterService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.util.BeanUtil;
import com.qtasnim.eoffice.util.ClassUtil;
import com.qtasnim.eoffice.ws.dto.DataMasterValueRetrieve;
import com.qtasnim.eoffice.ws.dto.DataMasterValueRetrieveParam;
import com.qtasnim.eoffice.ws.dto.ListStringDto;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.lang.reflect.Method;
import java.util.*;

@RunWith(Arquillian.class)
public class MasterDataTest extends EjbUnitTestBase {

    static Object obj;
    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.addClasses(
                            MasterDataTest.class);
                });
    }

    @Inject
    DataMasterService dataMasterService;

    @Test
    public void testListNick() {
        ListStringDto listNick = dataMasterService.getAllDataMaster();

        Map<String, ListStringDto> classesFields = new HashMap<>();

        listNick.getValues().forEach((e) -> {
            classesFields.put(e, dataMasterService.getAllFieldOfMasterData(e));
        });

        System.out.println("yey");
    }

    @Test
    public void test2() {
        String className = "com.qtasnim.eoffice.services.MasterDivisionService";

        Class<?> claz = ClassUtil.getClassByName(className);
        obj = BeanUtil.getBean(claz);

        for(Method m : claz.getMethods()) {
            System.out.println(m.getName());
        }

        System.out.println("yey");
    }

    @Test
    public void test3() {
        DataMasterValueRetrieveParam param = new DataMasterValueRetrieveParam("MasterDivision", Arrays.asList("kode", "nama"), "id");

        DataMasterValueRetrieve values = dataMasterService.getValueOfSelectedMasterDataFields(param);

        System.out.println("yey");
    }

    @Test
    public void test4() {
        DataMasterValueRetrieveParam param = new DataMasterValueRetrieveParam("MasterDivision", Arrays.asList("kode", "nama"), "id");

        DataMasterValueRetrieve values = dataMasterService.getValueOfSelectedMasterDataFields(param, null, null);

        System.out.println("yey");
    }
}
