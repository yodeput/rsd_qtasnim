package com.qtasnim.eoffice.test.masterdata;

import com.qtasnim.eoffice.db.FormDefinition;
import com.qtasnim.eoffice.services.FormDefinitionService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

@RunWith(Arquillian.class)
public class Formdefinition extends EjbUnitTestBase {

    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.addClasses(
                            Formdefinition.class);
                });
    }

    @Inject
    FormDefinitionService formDefinitionService;

    @Test
    public void test1() {
        List<FormDefinition> fds = formDefinitionService.getByIdJenisDokumen(new Long(1));

        Arrays.toString(fds.toArray());
    }
}
