package com.qtasnim.eoffice.test;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.function.Consumer;

public abstract class EjbUnitTestBase implements Serializable {
    public static WebArchive createDeployment(Class... clazz) {
        WebArchive war = createDeployment(w -> {});
        war.addClasses(clazz);

        return war;
    }


    public static WebArchive createDeployment(Consumer<WebArchive> onPrepare) {
        if (onPrepare == null) {
            onPrepare = war -> {};
        }

        WebArchive war = ShrinkWrap.createFromZipFile(JavaArchive.class, new File("target/eoffice.war")).as(WebArchive.class);
        File[] libs = Maven.resolver()
                .loadPomFromFile(new File("pom.xml"))
                .resolve(
                        "junit:junit",
                        "mysql:mysql-connector-java",
                        "org.jboss.arquillian.junit:arquillian-junit-container",
                        "org.jboss.shrinkwrap.resolver:shrinkwrap-resolver-impl-maven-archive"
                )
                .withoutTransitivity()
                .asFile();
        war.delete("WEB-INF/classes/com/qtasnim/eoffice/InitializerBean.class"); //skip db migration update
        war.delete("WEB-INF/web.xml");
        war.addAsWebInfResource(new File("src/test/resources/web.xml"));
        war.addAsResource(new File("src/test/resources/dummyfiles"));
        war.addClass(EjbUnitTestBase.class);
        war.addAsLibraries(libs);

        onPrepare.accept(war);

        return war;
    }
}

