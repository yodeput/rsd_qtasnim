package com.qtasnim.eoffice.test;

import org.junit.Test;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class TestTest {
    @Test
    public void test() {
        RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
        List<String> arguments = runtimeMxBean.getInputArguments();

        assertNotNull(arguments);
    }
}
