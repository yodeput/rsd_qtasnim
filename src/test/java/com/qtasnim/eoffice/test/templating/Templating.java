package com.qtasnim.eoffice.test.templating;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.helpers.TemplatingHelper;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.util.LayoutingEntity;
import com.qtasnim.eoffice.ws.dto.MetadataValueDto;
import com.qtasnim.eoffice.ws.dto.TemplatingDto;
import org.apache.commons.io.IOUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@RunWith(Arquillian.class)
public class Templating extends EjbUnitTestBase {

    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.addClasses(
                            Templating.class);
                });
    }

    @Inject
    TemplatingService templatingService;

    @Inject
    SuratService suratService;

    @Inject
    FileService fileService;

    @Inject
    MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    FormDefinitionService formDefinitionService;

    @Inject
    JenisDokumenDocLibService jenisDokumenDocLibService;

    @Test
    public void getLayoutingKeys() {
        Surat surat = suratService.getByIdSurat(new Long(327L));

        Long idJenisDokumen = formDefinitionService.find(surat.getFormDefinition().getId()).getJenisDokumen().getIdJenisDokumen();

        List<LayoutingEntity> metas = templatingService.getLayoutingEntityJenisDokumen(new Long(idJenisDokumen));

        System.out.println("yo");
    }

    @Test
    public void test2() throws Exception {
        File fileToUpload = new File(this.getClass().getResource("/temp_doc/templating.docx").toURI());

        byte[] content;
        try (FileInputStream fileInputStream = new FileInputStream(fileToUpload)) {
            content = new byte[fileInputStream.available()];
            fileInputStream.read(content);
        }

        Surat surat = suratService.find(new Long(18));

        byte[] contentRes = templatingService.dbBasedTemplating(content, surat);

        OutputStream out = new FileOutputStream(new File("D:\\templated.docx"));
        out.write(contentRes);
        out.close();

        System.out.println("yey");
    }

    @Test
    public void test3() {
        int a = 9+2;

        System.out.println(a);
    }

    @Test
    public void directTemplating() throws Exception {
        TemplatingDto templatingDto = new TemplatingDto();

        List<MetadataValueDto> metadataValueDtos = Arrays.asList(new MetadataValueDto[] {
                new MetadataValueDto("Tanggal Dokumen",
                "",
                "2019-07-25T00:00:00.000+07"),
                new MetadataValueDto("Perihal",
                "Ini Perihal",
                ""),
                new MetadataValueDto("No Dokumen",
                        "9900999",
                        ""),
                new MetadataValueDto("Kepada Internal",
                "Direktur Utama-EDI SUKMORO,Anggota Komisaris-RIZA PRIMADI",
                "5651,5635")
        });

        templatingDto.setIdFormDefinition(new Long(9));
        templatingDto.setMetadata(metadataValueDtos);
        templatingDto.setDocId("6f4eff05-e066-4f57-8a40-5b9420e742f5");

        String docid = templatingService.directTemplating(templatingDto);

        byte[] contentRes = fileService.download(docid);

        OutputStream out = new FileOutputStream(new File("D:\\templated.docx"));
        out.write(contentRes);
        out.close();

        System.out.println("yey");
    }

    @Test
    public void dbBasedTemplating() throws Exception {
//
//        surat.setFormValue(new LinkedList<FormValue>(){
//                {
//                    add(new FormValue(){
//                        {
//                            setValue("123");
//                            setFormField(new FormField(){
//                                {
//                                    setMetadata(new MasterMetadata(){
//                                        {
//                                            setNama("lampiran");
//                                        }
//                                    });
//                                }
//                            });
//                        }
//                    });
//                    add(new FormValue(){
//                        {
//                            setValue("123");
//                            setFormField(new FormField(){
//                                {
//                                    setMetadata(new MasterMetadata(){
//                                        {
//                                            setNama("perihal");
//                                        }
//                                    });
//                                }
//                            });
//                        }
//                    });
//                }
//        });
//
//        surat.setPenandatanganSurat(new LinkedList<PenandatanganSurat>(){
//            {
//                add(new PenandatanganSurat() {
//                    {
//                        setSeq(1);
//                        setUser(new MasterUser(){
//                            {
//                                setNameFront("Budi");
//                                setNameMiddleLast("Sutejo");
//                                setOrganization(new MasterStrukturOrganisasi() {
//                                    {
//                                        setOrganizationName("org1");
//                                        setOrganizationCode("123123123");
//                                    }
//                                });
//                            }
//                        });
//                    }
//                });
//            }
//        });

        Surat surat = suratService.getByIdSurat(171L);

        Long idJenisDokumen = formDefinitionService.find(surat.getFormDefinition().getId()).getJenisDokumen().getIdJenisDokumen();
        String documentId = jenisDokumenDocLibService.getDocIdByJenisDokumenId(idJenisDokumen);

//        byte[] contentOri = fileService.download(documentId);
        byte[] contentOri = IOUtils.toByteArray(new FileInputStream(new File("D:/template.docx")));
//        byte[] contentOri = IOUtils.toByteArray(new URL("http://office.qtasnim.com:2001/Download?id="+documentId));

        byte[] contentRes = templatingService.dbBasedTemplating(contentOri, surat);

        OutputStream out = new FileOutputStream(new File("D:\\templated.docx"));
        out.write(contentRes);
        out.close();

        System.out.println("yey");
    }
}
