package com.qtasnim.eoffice.test.templating;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.oned.Code39Writer;
import com.google.zxing.oned.EAN13Writer;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.SimpleValue;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.junit.Test;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;

//@RunWith(Arquillian.class)
public class QrCodeTest
//        extends EjbUnitTestBase
{
//    @Deployment
//    public static Archive<?> deploy() {
//        return createDeployment(
//                war -> {
//                    war.addClasses(
//                            QrCodeTest.class);
//                });
//    }

    private final int TYPE_SDT_RUN = 1;
    private final int TYPE_SDT_BLOCK = 2;

    @Test
    public void test() throws IOException, XmlException {
        String file = "/dummyfiles/test2.docx";
//        String file = "/dummyfiles/69bc79fd-78b6-480f-b423-4499bb485c40.docx";
        XWPFDocument docx = new XWPFDocument(this.getClass().getResourceAsStream(file));

        assertNotNull(docx);

        Map<Integer, List> fields = extractSDTs(docx);

        assertNotNull(fields);

        List<CTSdtRun> sdtRuns = (List<CTSdtRun>) fields.get(TYPE_SDT_RUN);
        List<CTSdtBlock> sdtBlocks = (List<CTSdtBlock>) fields.get(TYPE_SDT_BLOCK);

        sdtRuns.stream().findFirst().ifPresent(ctSdtRun -> {
            try {
                templatingImage(docx, ctSdtRun, this.getClass().getResourceAsStream("/dummyfiles/qrcode2.png"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        try (FileOutputStream out = new FileOutputStream(new File("test.docx"))) {
            docx.write(out);
            docx.close();
        }
    }

    private Map<Integer, List> extractSDTs(XWPFDocument document) throws XmlException {
        List<CTSdtRun> sdtRuns = new LinkedList<>();
        List<CTSdtBlock> sdtBlocks = new LinkedList<>();

        XmlCursor xmlcursor = document.getDocument().getBody().newCursor();

        while (xmlcursor.hasNextToken()) {
            XmlCursor.TokenType tokentype = xmlcursor.toNextToken();
            if (tokentype.isStart()) {
                if (xmlcursor.getObject() instanceof CTSdtRun)
                    sdtRuns.add((CTSdtRun) xmlcursor.getObject());
                else if (xmlcursor.getObject() instanceof CTSdtBlock)
                    sdtBlocks.add((CTSdtBlock) xmlcursor.getObject());
            }
        }

        Map<Integer, List> sdts = new HashMap<>();

        sdts.put(TYPE_SDT_RUN, sdtRuns);
        sdts.put(TYPE_SDT_BLOCK, sdtBlocks);

        return sdts;
    }

    private void templatingImage(XWPFDocument document, CTSdtRun stdRun, InputStream image) throws Exception {
        List<String> before = extractBlipId(stdRun);
        String blipId = document.addPictureData(image, Document.PICTURE_TYPE_PNG);
        CTSdtContentRun sdtContentRun = stdRun.getSdtContent();
        List<CTR> ctrs = sdtContentRun.getRList();

        if (ctrs != null && !ctrs.isEmpty()) {
            for (int p = 0; p < ctrs.size(); p++) {
                CTR ctr = ctrs.get(p);

                if (ctr != null) {
                    for (int i = 0; i < ctr.getDrawingList().size(); i++) {
                        ctr.removeDrawing(i);
                    }

                    CTDrawing ctDrawing = ctr.addNewDrawing();
                    ctDrawing.set(getInlineWithGraphic(blipId, document.getNextPicNameNumber(Document.PICTURE_TYPE_PNG), 704850, 704850));

                    List<String> after = extractBlipId(stdRun);
                    List<String> deletedBlib = before.stream().filter(t -> after.stream().noneMatch(v -> v.equals(t))).collect(Collectors.toList());
                    List<CTText> texts = ctr.getTList();
                    if (texts != null && !texts.isEmpty()) {
                        CTText text = texts.get(0);

                        if (text != null) {
                            text.setStringValue("");
                            sdtContentRun.setRArray(new CTR[]{ctr});

                            //modify content control lock
                            CTLock lock = stdRun.getSdtPr().addNewLock();
                            lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                            stdRun.getSdtPr().setLockArray(new CTLock[]{lock});
                        }
                    }
                }
            }
        }
    }

    private static XmlObject getInlineWithGraphic(String drawingDescr, int id, int width, int height) throws Exception {

        String xml =
                "<wp:inline distT=\"0\" distB=\"0\" distL=\"0\" distR=\"0\" xmlns:wpc=\"http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas\" xmlns:cx=\"http://schemas.microsoft.com/office/drawing/2014/chartex\" xmlns:cx1=\"http://schemas.microsoft.com/office/drawing/2015/9/8/chartex\" xmlns:cx2=\"http://schemas.microsoft.com/office/drawing/2015/10/21/chartex\" xmlns:cx3=\"http://schemas.microsoft.com/office/drawing/2016/5/9/chartex\" xmlns:cx4=\"http://schemas.microsoft.com/office/drawing/2016/5/10/chartex\" xmlns:cx5=\"http://schemas.microsoft.com/office/drawing/2016/5/11/chartex\" xmlns:cx6=\"http://schemas.microsoft.com/office/drawing/2016/5/12/chartex\" xmlns:cx7=\"http://schemas.microsoft.com/office/drawing/2016/5/13/chartex\" xmlns:cx8=\"http://schemas.microsoft.com/office/drawing/2016/5/14/chartex\" xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:aink=\"http://schemas.microsoft.com/office/drawing/2016/ink\" xmlns:am3d=\"http://schemas.microsoft.com/office/drawing/2017/model3d\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" xmlns:m=\"http://schemas.openxmlformats.org/officeDocument/2006/math\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:wp14=\"http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing\" xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" xmlns:w14=\"http://schemas.microsoft.com/office/word/2010/wordml\" xmlns:w15=\"http://schemas.microsoft.com/office/word/2012/wordml\" xmlns:w16cid=\"http://schemas.microsoft.com/office/word/2016/wordml/cid\" xmlns:w16se=\"http://schemas.microsoft.com/office/word/2015/wordml/symex\" xmlns:wpg=\"http://schemas.microsoft.com/office/word/2010/wordprocessingGroup\" xmlns:wpi=\"http://schemas.microsoft.com/office/word/2010/wordprocessingInk\" xmlns:wne=\"http://schemas.microsoft.com/office/word/2006/wordml\" xmlns:wps=\"http://schemas.microsoft.com/office/word/2010/wordprocessingShape\">\n" +
                        "  <wp:extent cx=\""+width+"\" cy=\""+height+"\"/>\n" +
                        "  <wp:effectExtent l=\"0\" t=\"0\" r=\"0\" b=\"0\"/>\n" +
                        "  <wp:docPr id=\""+id+"\" name=\"Picture "+id+"\" descr=\"A picture containing drawing\n" +
                        "\n" +
                        "Description automatically generated\"/>\n" +
                        "  <wp:cNvGraphicFramePr>\n" +
                        "    <a:graphicFrameLocks noChangeAspect=\"1\" xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\"/>\n" +
                        "  </wp:cNvGraphicFramePr>\n" +
                        "  <a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">\n" +
                        "    <a:graphicData uri=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">\n" +
                        "      <pic:pic xmlns:pic=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">\n" +
                        "        <pic:nvPicPr>\n" +
                        "          <pic:cNvPr id=\""+id+"\" name=\""+drawingDescr+"\"/>\n" +
                        "          <pic:cNvPicPr/>\n" +
                        "        </pic:nvPicPr>\n" +
                        "        <pic:blipFill>\n" +
                        "          <a:blip r:embed=\""+drawingDescr+"\" cstate=\"print\">\n" +
                        "            <a:extLst>\n" +
                        "              <a:ext uri=\"{28A0092B-C50C-407E-A947-70E740481C1C}\">\n" +
                        "                <a14:useLocalDpi val=\"0\" xmlns:a14=\"http://schemas.microsoft.com/office/drawing/2010/main\"/>\n" +
                        "              </a:ext>\n" +
                        "            </a:extLst>\n" +
                        "          </a:blip>\n" +
                        "          <a:stretch>\n" +
                        "            <a:fillRect/>\n" +
                        "          </a:stretch>\n" +
                        "        </pic:blipFill>\n" +
                        "        <pic:spPr>\n" +
                        "          <a:xfrm>\n" +
                        "            <a:off x=\"0\" y=\"0\"/>\n" +
                        "            <a:ext cx=\""+width+"\" cy=\""+height+"\"/>\n" +
                        "          </a:xfrm>\n" +
                        "          <a:prstGeom prst=\"rect\">\n" +
                        "            <a:avLst/>\n" +
                        "          </a:prstGeom>\n" +
                        "        </pic:spPr>\n" +
                        "      </pic:pic>\n" +
                        "    </a:graphicData>\n" +
                        "  </a:graphic>\n" +
                        "</wp:inline>";

        return XmlObject.Factory.parse(xml);
    }

    private List<String> extractBlipId(CTSdtRun sdtRun) {
        return sdtRun.getSdtContent().getRList().stream().flatMap(t -> t.getDrawingList().stream()).flatMap(t -> t.getInlineList().stream()).filter(t -> t.getGraphic() != null && t.getGraphic().getGraphicData() != null).map(t -> {
            try {
                XmlObject drawing = t.getGraphic().getGraphicData().copy();

                String blipPath = "declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' " +
                        ".//a:blip";
                XmlObject[] blips = drawing.selectPath(blipPath);
                if(blips.length > 0) {

                    XmlObject blip = blips[0];
                    XmlObject _blipId =
                            blip.selectAttribute("http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                                    , "embed");
                    if(_blipId != null) {
                        return ((SimpleValue)_blipId).getStringValue();
                    }
                }
            } catch (Exception e) {

            }

            return "null";
        }).filter(t -> !"null".equals(t)).collect(Collectors.toList());
    }

    @Test
    public void testBarcode() throws WriterException, IOException {
        //width 280px
        //height 22px
        Code128Writer barcodeWriter = new Code128Writer();
        BitMatrix bitMatrix = barcodeWriter.encode("16/KA.102/V/CCC/KA/2020", BarcodeFormat.CODE_128, 280, 22);

        BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
        ImageIO.write(bufferedImage, "png", new File("apanih.png"));
    }
}
