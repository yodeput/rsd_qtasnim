package com.qtasnim.eoffice.test.templating;

import com.qtasnim.eoffice.helpers.TemplatingHelper;
import com.qtasnim.eoffice.util.DateUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.junit.Assert;
import org.junit.Test;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TemplatingProcess {
    //---------------------------------------------------------------------
    //https://stackoverflow.com/a/46465717/11863006
    @Test
    public void listBookmarkNames() throws URISyntaxException, IOException {
        File file = new File(this.getClass().getResource("/temp_doc/templating 2.docx").toURI());

        XWPFDocument document = new XWPFDocument(new FileInputStream(file.getPath()));

        List<XWPFParagraph> paragraphs = collectParagraphs(document);

        List<String> bookmarks = getBookmarkNames(paragraphs);

        Assert.assertNotNull(paragraphs);
        Assert.assertNotNull(bookmarks);
    }

    private List<XWPFParagraph> collectParagraphs(XWPFDocument document) {
        List<XWPFParagraph> paragraphs = new ArrayList<>();
        paragraphs.addAll(document.getParagraphs());

//        for (XWPFTable table : document.getTables())
//        {
//            for (XWPFTableRow row : table.getRows())
//            {
//                for (XWPFTableCell cell : row.getTableCells())
//                    paragraphs.addAll(cell.getParagraphs());
//            }
//        }
        return paragraphs;
    }

    private List<String> getBookmarkNames(List<XWPFParagraph> paragraphs) {
        List<String> bookmarkNames = new ArrayList<>();
        Iterator<XWPFParagraph> paraIter = null;
        XWPFParagraph para = null;
        List<CTBookmark> bookmarkList = null;
        Iterator<CTBookmark> bookmarkIter = null;
        CTBookmark bookmark = null;
        XWPFRun run = null;

        // Get an Iterator for the XWPFParagraph object and step through them
        // one at a time.
        paraIter = paragraphs.iterator();
        while (paraIter.hasNext())
        {
            para = paraIter.next();

            // Get a List of the CTBookmark object sthat the paragraph
            // 'contains' and step through these one at a time.
            bookmarkList = para.getCTP().getBookmarkStartList();
            bookmarkIter = bookmarkList.iterator();
            while (bookmarkIter.hasNext())
            {
                bookmark = bookmarkIter.next();
                bookmarkNames.add(bookmark.getName());
            }
        }
        return bookmarkNames;
    }
    //--------------------------------------------------------------------
    //https://stackoverflow.com/a/54017209/11863006
    @Test
    public void alterBookmarkContent() throws Exception {
        File file = new File(this.getClass().getResource("/temp_doc/templating.docx").toURI());

        XWPFDocument document = new XWPFDocument(new FileInputStream(file.getPath()));

        String bookmarkName = "body";

        String toReplaceText = "ndjfksdfjdakfjdskf kdnfksdf ldjfkd";

        replaceBookmark(document, bookmarkName, toReplaceText);
    }

    private String replaceBookmarkedPara(String input, String bookmarkTxt) {
        char[] tmp = input.toCharArray();
        StringBuilder sb = new StringBuilder();
        int bookmarkedCharCount = 0;
        for (int i = 0 ; i < tmp.length ; i++) {
            int asciiCode = tmp[i];
            if (asciiCode == 8194) {
                bookmarkedCharCount ++;
                if (bookmarkedCharCount == 5) {
                    sb.append(bookmarkTxt);
                }
            }
            else {
                sb.append(tmp[i]);
            }
        }
        return sb.toString();
    }

    private void removeAllRuns(XWPFParagraph paragraph) {
        int size = paragraph.getRuns().size();
        for (int i = 0; i < size; i++) {
            paragraph.removeRun(0);
        }
    }

    private void insertReplacementRuns(XWPFParagraph paragraph, String replacedText) {
        String[] replacementTextSplitOnCarriageReturn = StringUtils.split(replacedText, "\n");

        for (int j = 0; j < replacementTextSplitOnCarriageReturn.length; j++) {
            String part = replacementTextSplitOnCarriageReturn[j];

            XWPFRun newRun = paragraph.insertNewRun(j);
            newRun.setText(part);

            if (j+1 < replacementTextSplitOnCarriageReturn.length) {
                newRun.addCarriageReturn();
            }
        }
    }

    public void replaceBookmark (XWPFDocument document, String bookmarkName, String toReplace) throws Exception {
        List<XWPFParagraph> paragraphs = document.getParagraphs();
        for (XWPFParagraph paragraph : paragraphs) {
            //Here you have your paragraph;
            CTP ctp = paragraph.getCTP();
            // Get all bookmarks and loop through them
            List<CTBookmark> bookmarks = ctp.getBookmarkStartList();
            for(CTBookmark bookmark : bookmarks) {
                if(bookmark.getName().equals(bookmarkName)) {

                    String paraText = paragraph.getText();
                    System.out.println("paraText = "+paraText +" for bookmark name "+bookmark.getName());
                    String replacementText = replaceBookmarkedPara(paraText, toReplace);
                    removeAllRuns(paragraph);
                    insertReplacementRuns(paragraph, replacementText);

                }
            }
        }
        OutputStream out = new FileOutputStream("D:\\output.docx");
        document.write(out);
        document.close();
        out.close();
    }

    //--------------------------------------

    @Test
    public void populateBookmarkContent() throws IOException, URISyntaxException {
        File file = new File(this.getClass().getResource("/temp_doc/templating.docx").toURI());

        XWPFDocument document = new XWPFDocument(new FileInputStream(file.getPath()));

        String bookmarkName = "body";

        String content = populateContentOnBookmark(document, bookmarkName);

        System.out.println(content);
    }

    // two int members. 1st index : start bookmark marker, 2nd index : end bookmark marker.
    private int[] paragraphPositionOnBookmark(List<XWPFParagraph> paragraphs, String bookmarkName) {
        int[] position = new int[]{-1,-1};

        for(int i=0; i<paragraphs.size(); i++) {
            XWPFParagraph paragraph = paragraphs.get(i);

            if(position[0] == -1) {
                for (CTBookmark bookmark : paragraph.getCTP().getBookmarkStartList()) {
                    if (bookmark.getName().equals(bookmarkName)) {
                        position[0] = i;

                        for(int j=i; j<paragraphs.size(); j++) {
                            if(paragraph.getCTP().getBookmarkEndList().size() > 0) {
                                position[1] = j;
                            }
                        }
                        break;
                    }
                }
            }
        }

        return position;
    }

    private String populateContentOnBookmark(XWPFDocument document, String bookmarkName) {
        String content = "";

        List<XWPFParagraph> paragraphs = document.getParagraphs();

        for(XWPFParagraph paragraph : paragraphs) {
            System.out.println("paragraph: "+paragraph.getText());

            List<CTBookmark> bookmarks = paragraph.getCTP().getBookmarkStartList();
            for(CTBookmark bookmark : bookmarks) {
                if(bookmark.getName().equals(bookmarkName)) {
                    content.concat(paragraph.getText());
                    break;
                }
            }
        }

        return content;
    }

    //--------------------------------
    @Test
    public void populateStdList() throws IOException {
        XWPFDocument document = new XWPFDocument(new FileInputStream(new File("C:\\Users\\Lenovo\\Desktop\\compare docx\\ye3.docx").getPath()));

        List<CTSdtRun> stds = listStdts(document);

        System.out.println("yey");
    }

    @Test
    public void populateStdList2() throws IOException {
        XWPFDocument document = new XWPFDocument(new FileInputStream(new File("C:\\Users\\Lenovo\\Desktop\\compare docx\\ye3.docx").getPath()));

        List<XWPFAbstractSDT> sdts = extractAllSDTs(document);

        System.out.println("yey");
    }

    @Test
    public void populateStdList3() throws IOException, XmlException {
        XWPFDocument document = new XWPFDocument(new FileInputStream(new File("C:\\Users\\Lenovo\\Desktop\\compare docx\\ye3.docx").getPath()));

        Map sdts = extractSDTs(document);

        List<CTSdtRun> sdtRuns = (List<CTSdtRun>) sdts.get("run");
        List<CTSdtBlock> sdtBlocks = (List<CTSdtBlock>) sdts.get("block");

        System.out.println("yey");
    }

    @Test
    public void templatingStdBased() throws IOException {
        XWPFDocument document = new XWPFDocument(new FileInputStream(new File("C:\\Users\\Lenovo\\Desktop\\compare docx\\ye3.docx").getPath()));
//        XWPFDocument document = new XWPFDocument(new FileInputStream(new File("D:\\p2.docx").getPath()));

        Map<String,String> pairKeyValue = new HashMap<>();

        pairKeyValue.put("{number}", "0987654321");
        pairKeyValue.put("{classification}", "buka");
        pairKeyValue.put("{appendix}", "yoyy");
        pairKeyValue.put("{penerima#1}", "yui\ndsd");

        templatingViaControlContent(document, pairKeyValue);
    }

    @Test
    public void templatingStdBased2() throws IOException, XmlException {
        XWPFDocument document = new XWPFDocument(new FileInputStream(new File("C:\\Users\\Lenovo\\Desktop\\compare docx\\ye3.docx").getPath()));
//        XWPFDocument document = new XWPFDocument(new FileInputStream(new File("D:\\p2.docx").getPath()));

        Map<String,String> pairKeyValue = new HashMap<>();

        pairKeyValue.put("{number}", "09876\n54321");
        pairKeyValue.put("{classification}", "buka");
        pairKeyValue.put("{appendix}", "yoyy");
        pairKeyValue.put("{penerima#1}", "dsd,yui");
        pairKeyValue.put("yoy", "yui,dsd");

        templatingViaControlContent2(document, pairKeyValue);
    }

    @Test
    public void testTemplatingPenerimaConditional() throws IOException {
        XWPFDocument document = new XWPFDocument(new FileInputStream(new File("C:\\Users\\Lenovo\\Desktop\\compare docx\\ye3.docx").getPath()));

        Map<String,String> pairKeyValue = new HashMap<>();

        pairKeyValue.put("{penerima#1}", "\tPara penerima\nTerlampir");
        pairKeyValue.put("{penerima#2}", "Penerima Surat:\n");
    }

    private List<CTSdtRun> listStdts(XWPFDocument document) {
        List<CTSdtRun> stds = new LinkedList<>();

        for(XWPFParagraph paragraph : document.getParagraphs()) {
            stds.addAll(paragraph.getCTP().getSdtList());
        }

        return stds;
    }

//    private List<CTSdtRun> listSdts(XWPFDocument document) {
//        List<CTSdtRun> stds = new LinkedList<>();
//
//        for(IBodyElement e : document.getBodyElements()) {
//            if(e instanceof XWPFSDT) {
//                XWPFSDT xwpfsdt = (XWPFSDT) e;
//
//            }
//            if(e instanceof XWPFParagraph) {
//
//            }
//        }
//    }

    @Test
    public void convertStringDate() throws ParseException {
        String dateString = "2019-07-25T00:00:00.000+07";

        Calendar calendar = DatatypeConverter.parseDateTime(dateString.split("\\+")[0]);

        System.out.println(calendar.getTime());

        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        Date result2 = df2.parse(dateString);
        System.out.println(result2);

        System.out.println(new DateUtil().getDateFromISOString(dateString));

        df2 = new SimpleDateFormat("dd MMMMMMMMMMMM yyyy", Locale.forLanguageTag("id-ID"));
        System.out.println(df2.format(result2));
    }

    private List<XWPFAbstractSDT> extractAllSDTs(XWPFDocument doc) {
        List<XWPFAbstractSDT> sdts = new ArrayList<>();

        List<XWPFHeader> headers = doc.getHeaderList();
        for (XWPFHeader header : headers) {
            sdts.addAll(extractSDTsFromBodyElements(header.getBodyElements()));
        }
        sdts.addAll(extractSDTsFromBodyElements(doc.getBodyElements()));

        List<XWPFFooter> footers = doc.getFooterList();
        for (XWPFFooter footer : footers) {
            sdts.addAll(extractSDTsFromBodyElements(footer.getBodyElements()));
        }

        for (XWPFFootnote footnote : doc.getFootnotes()) {
            sdts.addAll(extractSDTsFromBodyElements(footnote.getBodyElements()));
        }
        for (XWPFEndnote footnote : doc.getEndnotes()) {
            sdts.addAll(extractSDTsFromBodyElements(footnote.getBodyElements()));
        }
        return sdts;
    }

    private List<XWPFAbstractSDT> extractSDTsFromBodyElements(List<IBodyElement> elements) {
        List<XWPFAbstractSDT> sdts = new ArrayList<>();
        for (IBodyElement e : elements) {
            if (e instanceof XWPFSDT) {
                XWPFSDT sdt = (XWPFSDT) e;
                sdts.add(sdt);
            } else if (e instanceof XWPFParagraph) {

                XWPFParagraph p = (XWPFParagraph) e;
                for (IRunElement e2 : p.getIRuns()) {
                    if (e2 instanceof XWPFSDT) {
                        XWPFSDT sdt = (XWPFSDT) e2;
                        sdts.add(sdt);
                    }
                }
            } else if (e instanceof XWPFTable) {
                XWPFTable table = (XWPFTable) e;
                sdts.addAll(extractSDTsFromTable(table));
            }
        }
        return sdts;
    }

    private List<XWPFAbstractSDT> extractSDTsFromTable(XWPFTable table) {

        List<XWPFAbstractSDT> sdts = new ArrayList<>();
        for (XWPFTableRow r : table.getRows()) {
            for (ICell c : r.getTableICells()) {
                if (c instanceof XWPFSDTCell) {
                    sdts.add((XWPFSDTCell) c);
                } else if (c instanceof XWPFTableCell) {
                    sdts.addAll(extractSDTsFromBodyElements(((XWPFTableCell) c).getBodyElements()));
                }
            }
        }
        return sdts;
    }

    //http://apache-poi.1045710.n5.nabble.com/Unable-to-replace-title-of-docx-using-apache-poi-in-java-tp5733971p5733976.html
    private Map<String, List> extractSDTs(XWPFDocument document)
            throws XmlException {
        List<CTSdtRun> sdtRuns = new LinkedList<>();
        List<CTSdtBlock> sdtBlocks = new LinkedList<>();

        XmlCursor xmlcursor = document.getDocument().getBody().newCursor();

        while (xmlcursor.hasNextToken()) {
            XmlCursor.TokenType tokentype = xmlcursor.toNextToken();
            if (tokentype.isStart()) {
                if (xmlcursor.getObject() instanceof CTSdtRun)
                    sdtRuns.add((CTSdtRun) xmlcursor.getObject());
                else if (xmlcursor.getObject() instanceof CTSdtBlock)
                    sdtBlocks.add((CTSdtBlock) xmlcursor.getObject());
            }
        }

        Map<String, List> sdts = new HashMap<>();

        sdts.put("run", sdtRuns);
        sdts.put("block", sdtBlocks);

        return sdts;
    }

//    private void templatingViaControlContent(XWPFDocument document) {
//        List<XWPFAbstractSDT> sdts = extractAllSDTs(document);
//
//        XWPFSDTContent xwpfsdtContent = (XWPFSDTContent) sdts.get(1).getContent();
//        XWPFRun run = (XWPFRun) xwpfsdtContent.bodyElements.get(0);
//        CTText text = run.getCTR().addNewT();
//        text.setStringValue("yeyyyy");
//        run.getCTR().setTArray(new CTText[]{text});
//    }

    private void templatingViaControlContent(XWPFDocument document, Map<String,String> pairKeyValue) throws IOException {
        List<CTSdtRun> stds = listStdts(document);

        for(CTSdtRun stdRun : stds) {
            // replace values
            String key = stdRun.getSdtPr().getTagList().get(0).getVal();
            String value = pairKeyValue.get(key);

            CTR ctr = stdRun.getSdtContent().addNewR();

            CTText text = ctr.addNewT();
            text.setStringValue(value);

            ctr.setTArray(new CTText[]{text});

            stdRun.getSdtContent().setRArray(new CTR[]{ctr});

            //modify content control lock
            CTLock lock = stdRun.getSdtPr().addNewLock();
            lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

            stdRun.getSdtPr().setLockArray(new CTLock[]{lock});
        }

        OutputStream out = new FileOutputStream("D:/p3.docx");
        document.write(out);
        out.close();
    }

    private void templatingViaControlContent2(XWPFDocument document, Map<String,String> pairKeyValue) throws IOException, XmlException {
        Map sdts = extractSDTs(document);

        List<CTSdtRun> sdtRuns = (List<CTSdtRun>) sdts.get("run");
        List<CTSdtBlock> sdtBlocks = (List<CTSdtBlock>) sdts.get("block");

        for(CTSdtRun stdRun : sdtRuns) {
            // replace values
            String key = stdRun.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
                String value = pairKeyValue.get(key);

                CTR ctr = stdRun.getSdtContent().addNewR();

                CTText text = ctr.addNewT();
                text.setStringValue(value);

                ctr.setTArray(new CTText[]{text});

                stdRun.getSdtContent().setRArray(new CTR[]{ctr});

                //modify content control lock
                CTLock lock = stdRun.getSdtPr().addNewLock();
                lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                stdRun.getSdtPr().setLockArray(new CTLock[]{lock});
            }
        }

        for(CTSdtBlock sdtBlock : sdtBlocks) {
            // replace values
            String key = sdtBlock.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
                String value = pairKeyValue.get(key);

                String[] values = value.split(",");

                CTSdtContentBlock contentBlock = sdtBlock.getSdtContent();

                CTP[] ctps = new CTP[values.length];

                String xmlCtp = "<xml-fragment xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"><w:pPr><w:pStyle w:val=\"255\" /><w:numPr><w:ilvl w:val=\"0\" /><w:numId w:val=\"4\" /></w:numPr><w:contextualSpacing w:val=\"true\" /><w:ind w:left=\"283\" w:right=\"0\" w:hanging=\"283\" /><w:jc w:val=\"both\" /><w:spacing w:lineRule=\"auto\" w:line=\"240\" w:after=\"0\" /><w:tabs><w:tab w:val=\"left\" w:pos=\"1418\" w:leader=\"none\" /><w:tab w:val=\"left\" w:pos=\"1701\" w:leader=\"none\" /><w:tab w:val=\"left\" w:pos=\"1843\" w:leader=\"none\" /><w:tab w:val=\"left\" w:pos=\"5103\" w:leader=\"none\" /></w:tabs><w:rPr><w:lang w:val=\"en-US\" /></w:rPr></w:pPr><w:r><w:rPr><w:lang w:val=\"en-US\" /></w:rPr><w:t xml:space=\"preserve\">%s</w:t></w:r><w:r><w:rPr><w:lang w:val=\"en-US\" /></w:rPr></w:r><w:r /></xml-fragment>";

                for(int i=0; i<values.length; i++) {
//                    CTP ctp = contentBlock.addNewP();
//                    text.setStringValue(i+1+".  "+values[i]);
//
//                    ctr.setTArray(new CTText[]{text});
//
//                    ctp.setRArray(new CTR[]{ctr});
//
//                    CTPPr ctpPr = ctp.addNewPPr();
//
//                    XmlObject xmlObject = XmlObject.Factory.parse(pprXml);
//
//                    ctpPr.set(xmlObject);
//
//                    ctp.setPPr(ctpPr);

//                    XmlObject xmlObject = XmlObject.Factory.parse(String.format(xmlCtp, values[i]));
                    ctps[i] = CTP.Factory.parse(String.format(xmlCtp, i+1+".  "+values[i]));
                }

                contentBlock.setPArray(ctps);

                //modify content control lock
                CTLock lock = sdtBlock.getSdtPr().addNewLock();
                lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                sdtBlock.getSdtPr().setLockArray(new CTLock[]{lock});
            }
        }

        OutputStream out = new FileOutputStream("D:/p3.docx");
        document.write(out);
        out.close();
    }

    @Test
    public void iterateHeader() {
        try {
            XWPFDocument document = new XWPFDocument(new FileInputStream(new File("C:\\Users\\Lenovo\\Desktop\\compare docx\\watermark.docx").getPath()));

            List<XWPFHeader> headers = document.getHeaderList();

            System.out.println("yey");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteWatermark() {
        try {
            com.qtasnim.eoffice.util.TemplatingProcess templatingProcess = new com.qtasnim.eoffice.util.TemplatingProcess(new HashMap<>(), "");
            byte[] noWatermark = templatingProcess.deleteWatermark("D:\\7590ffb0-48f3-47a3-9af2-13ad8e64afbf.docx");
            File file = new File("D:\\7590ffb0-48f3-47a3-9af2-13ad8e64afbf_remove.docx");
            FileUtils.writeByteArrayToFile(file, noWatermark);

            if (file.exists()) {
                file.delete();
            }

            System.out.println("yey");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void templatingProcess() {
        try {
//            byte[] docOri = IOUtils.toByteArray(new FileInputStream(new File("C:\\Users\\Lenovo\\Desktop\\compare docx\\nd wrmk\\wtmked\\SD-Template.docx")));
            byte[] docOri = IOUtils.toByteArray(new FileInputStream(new File("C:\\Users\\Lenovo\\Desktop\\compare docx\\ea5c65f6-cd08-41df-ae42-4a30a5e7b975.docx")));

            String pathTempTemplateRaw = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID() + ".docx";

            byte[] docTemplated;

            com.qtasnim.eoffice.util.TemplatingProcess templatingProcess;

            FileOutputStream fos = new FileOutputStream(pathTempTemplateRaw);

            fos.write(docOri);
            fos.close();

            Map<String, String> pairMetaNameValue = new HashMap();

            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Tempat"), "Tanjung Enim");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Tanggal"), "13-06-2020");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("klasifikasi_keamanan"), "0x00ffffff");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Undangan Hari"), "0x00ffffff");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Undangan Tanggal"), "Bismillah test");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Penerima Surat#1"), "Di bawah");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Penerima Surat#2"), "budi,tono");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Penandatangan#1"), "penerima");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Penandatangan#2"), "abcdef,qwerty");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Konseptor#1"), "IT Specialist Level 1 Development");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Konseptor#2"), "Nita Marta Asih,NIPP : 61616");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Agenda"), "abcdsdsdsd,NIP : 122331");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Lampiran"), "lampiran tyiyuyu");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Perihal"), "perihal");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("Tembusan"), "tembusan");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("NoDokumen"), "0129039120");
            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName("UndanganHari"), "senin");

            templatingProcess = new com.qtasnim.eoffice.util.TemplatingProcess(pairMetaNameValue, pathTempTemplateRaw);

            docTemplated = templatingProcess.process();

            OutputStream out = new FileOutputStream(new File("D:\\templated.docx"));
            out.write(docTemplated);
            out.close();

            Files.deleteIfExists(new File(pathTempTemplateRaw).toPath());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
