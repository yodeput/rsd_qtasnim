package com.qtasnim.eoffice.test;

import com.qtasnim.eoffice.db.IWorkflowEntity;
import com.qtasnim.eoffice.services.NgSuratService;
import com.qtasnim.eoffice.services.ProcessTaskService;
import com.qtasnim.eoffice.services.SuratService;
import com.qtasnim.eoffice.util.ClassUtil;
import com.qtasnim.eoffice.workflows.IWorkflowService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
public class CdiTest extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(CdiTest.class);
    }

    @Inject
    private ProcessTaskService processTaskService;

    @Test
    public void test() {
        processTaskService.getAll().limit(1).findFirst().ifPresent(processTask -> {
            IWorkflowService workflowService = null;

            try {
                workflowService = ((IWorkflowEntity) ClassUtil.getClassByName(processTask.getProcessInstance().getRelatedEntity()).newInstance()).getWorkflowService();
            } catch (Exception e) {
                e.printStackTrace();
            }

            assertNotNull(workflowService);
        });
    }
}
