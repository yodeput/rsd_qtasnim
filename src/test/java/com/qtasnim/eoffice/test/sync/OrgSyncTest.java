package com.qtasnim.eoffice.test.sync;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.JerseyClient;
import com.qtasnim.eoffice.db.MasterArea;
import com.qtasnim.eoffice.db.OrganizationWrapper;
import com.qtasnim.eoffice.db.UserWrapper;
import com.qtasnim.eoffice.services.MasterStrukturOrganisasiService;
import com.qtasnim.eoffice.services.MasterUserService;
import com.qtasnim.eoffice.services.SyncService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.util.DateUtil;
import id.kai.ws.dto.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.stream.Collectors;

@RunWith(Arquillian.class)
public class OrgSyncTest
        extends EjbUnitTestBase
{
    @Deployment
    public static Archive<?> deploy() {
        return OrgSyncTest.createDeployment(OrgSyncTest.class);
    }

    private static final String BASE_ADDRESS_ORG = "https://ws-int-alpha.kai.id/v2/hris/get-all-struktur-organisasi-kai";
    private static final String BASE_ADDRESS_POS = "https://ws-int-alpha.kai.id/v2/hris/get-all-posisi-kai";
    private static final String BASE_ADDRESS_EMP = "https://ws-int-alpha.kai.id/v3/hris/service_employee";

    @Inject
    private SyncService syncService;

    @Inject
    @JerseyClient
    private Client client;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private MasterUserService masterUserService;

    @Test
    public void org() throws Exception {
        List<HrisOrgDto> result = new ArrayList<>();
        List<HrisEmployeeDto> result2 = new ArrayList<>();
        downloadOrg(result);
        downloadPosisi(result);
        downloadEmp(result, result2);

        List<OrganizationWrapper> api = result
                .stream()
                .map(t -> new OrganizationWrapper(null, t, t.getOrgeh().trim(), t.getOutxt().trim(), Optional.ofNullable(t.getOuabv()).map(String::trim).orElse(null), t.getParnt().trim(),t.isPosisi(),t.isChief(),Optional.ofNullable(t.getPersa()).map(String::trim).orElse(null),"I100",Optional.ofNullable(t.getClazz()).orElse(null),Optional.ofNullable(t.getGrade()).orElse(null),Optional.ofNullable(t.getKorsa()).orElse(null)))
                .distinct()
                .collect(Collectors.toList());

        try {
            masterStrukturOrganisasiService.sync(api);
        } catch (Exception e) {
            throw e;
        }
    }

    @Test
    public void user() throws Exception {
        List<HrisOrgDto> result = new ArrayList<>();
        List<HrisEmployeeDto> result2 = new ArrayList<>();
        downloadEmp(result, result2);
        DateUtil dateUtil = new DateUtil();

        List<UserWrapper> api2 = getUserWrappers(result2, dateUtil);
        try {
            masterUserService.sync(api2);
        } catch (Exception e) {
            throw e;
        }
    }

    @Test
    public void test() throws Exception {
        List<HrisOrgDto> result = new ArrayList<>();
        List<HrisEmployeeDto> result2 = new ArrayList<>();
        downloadOrg(result);
        downloadPosisi(result);
        downloadEmp(result, result2);
//        DateUtil dateUtil = new DateUtil();
//        digDummy(result);
//        digDummy2(result);
//        digDummy3(result, result2);
//        List<OrganizationWrapper> api = result
//                .stream()
//                .map(t -> new OrganizationWrapper(null, t, t.getOrgeh().trim(), t.getOutxt().trim(), Optional.ofNullable(t.getOuabv()).map(String::trim).orElse(null), t.getParnt().trim(),t.isPosisi(),t.isChief(),Optional.ofNullable(t.getPersa()).map(String::trim).orElse(null),""))
//                .distinct()
//                .collect(Collectors.toList());
//        List<UserWrapper> api2 = getUserWrappers(result2, dateUtil);
//
//        try {
//            syncService.syncOrgAndEmployee(api, api2);
//        } catch (Exception e) {
//            throw e;
//        }
    }

    private List<UserWrapper> getUserWrappers(List<HrisEmployeeDto> result2, DateUtil dateUtil) {
        return result2
                .stream()
                .map(t -> {
                    UserWrapper userWrapper = new UserWrapper();
                    List<String> nameParts = new ArrayList<>(Arrays.asList(t.getName().split(" ")));
                    String nameFront = nameParts.get(0);
                    nameParts.remove(0);
                    String nameMiddleLast = String.join(" ", nameParts).trim();

                    userWrapper.setMasterUser(null);
                    userWrapper.setEmployeeDto(t);
                    userWrapper.setEmployeeId(Optional.ofNullable(t.getPernr()).map(String::trim).orElse(null));
                    userWrapper.setFullName(t.getName());
                    userWrapper.setNameFront(nameFront);
                    userWrapper.setNameMiddleLast(nameMiddleLast);
                    userWrapper.setEmail(Optional.ofNullable(t.getEmailKorporate()).filter(e -> EmailValidator.getInstance().isValid(e)).map(String::trim).orElse("-"));
                    userWrapper.setSalutation(t.getGender().toLowerCase().equals("perempuan") ? "Mrs." : "Mr.");
                    userWrapper.setKelamin(t.getGender());
                    userWrapper.setUsername(userWrapper.getEmployeeId());
                    userWrapper.setBusinessArea(t.getBusArea());
                    userWrapper.setKedudukan(t.getPangkatText());
                    userWrapper.setPersonalArea(t.getPersaText());
                    userWrapper.setPersonalAreaCode(t.getPersa());
                    userWrapper.setKorsa(t.getTextKorsa());
                    userWrapper.setKorsaId(t.getIdKorsa());
                    userWrapper.setKorsaAbbr(t.getAbbrKorsa());
                    userWrapper.setGrade(t.getGrade());
                    userWrapper.setOrganizationCode(t.getPlans());
                    userWrapper.setOrganizationName(t.getPosText());
                    userWrapper.setCompanyCode(t.getIdBukrs());
                    userWrapper.setCompanyName(t.getTextBukrs());
                    userWrapper.setTglLahir(Optional.ofNullable(dateUtil.getDateFromString(t.getBirthdate(), "yyyy-MM-dd")).orElse(new Date(Long.MIN_VALUE)));
                    userWrapper.setTempatLahir(t.getBirthplace());
                    userWrapper.setAgama(t.getAgama());
                    userWrapper.setKedudukan(t.getPosText());
                    userWrapper.setJabatan(t.getJobText());
                    userWrapper.setMobilePhone(t.getHpPribadi());

                    return userWrapper;
                })
                .collect(Collectors.toList());
    }

    private void downloadOrg(List<HrisOrgDto> result) throws IOException {
        ObjectMapper objectMapper = getObjectMapper();
        String url = UriBuilder.fromUri(BASE_ADDRESS_ORG)
                .toString();
        WebTarget webResource = getClient().target(url);
        Response response = webResource
                .request()
                .header("Authorization", "Bearer un3P-Y3T_1563327096")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get();
        String stringEntity = response.readEntity(String.class);
        HrisOrgSearchDto hrisOrgSearchDto = objectMapper.readValue(stringEntity, HrisOrgSearchDto.class);
        for (HrisOrgDto dto: hrisOrgSearchDto.getData()) {
            HrisOrgDto organizationDto = new HrisOrgDto();
            organizationDto.setOrgeh(dto.getOrgeh());
            organizationDto.setParnt(dto.getParnt());
            organizationDto.setOutxt(dto.getOutxt());
            organizationDto.setOuabv(dto.getOuabv());
            organizationDto.setLevel(dto.getLevel() + 1);
            organizationDto.setPosisi(false);
            organizationDto.setChief(false);
            organizationDto.setPersa(dto.getPersa());
            result.add(organizationDto);

            try(PrintWriter output = new PrintWriter(new FileWriter("org.txt",true))) {
                output.printf("%s\r\n", objectMapper.writeValueAsString(dto));
            }
            catch (Exception e) {}
        }

//        HrisOrgSearchDto hrisOrgSearchDto = objectMapper.readValue(stringEntity, HrisOrgSearchDto.class);
//        result.addAll(hrisOrgSearchDto.getData());
    }

    private Long countEmp() throws IOException {
        ObjectMapper objectMapper = getObjectMapper();
        String url = UriBuilder.fromUri(BASE_ADDRESS_EMP)
                .queryParam("count", "1")
                .toString();
        WebTarget webResource = getClient().target(url);
        Response response = webResource
                .request()
                .header("Authorization", "Bearer un3P-Y3T_1563327096")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get();
        String stringEntity = response.readEntity(String.class);
        HrisEmployeeCountDto employeeCountDto = objectMapper.readValue(stringEntity, HrisEmployeeCountDto.class);

        return Long.parseLong(employeeCountDto.getData());
    }

    private Long countPosisi() throws IOException {
        ObjectMapper objectMapper = getObjectMapper();
        String url = UriBuilder.fromUri(BASE_ADDRESS_POS)
                .queryParam("count", "1")
                .toString();
        WebTarget webResource = getClient().target(url);
        Response response = webResource
                .request()
                .header("Authorization", "Bearer un3P-Y3T_1563327096")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get();
        String stringEntity = response.readEntity(String.class);
        HrisPosisiCountDto employeeCountDto = objectMapper.readValue(stringEntity, HrisPosisiCountDto.class);

        return Long.parseLong(employeeCountDto.getCount());
    }

    private void downloadEmp(List<HrisOrgDto> result, List<HrisEmployeeDto> result2) throws IOException {
        Long count = countEmp();
        ObjectMapper objectMapper = getObjectMapper();
        int limit = 1000;
        int page = 0;

        while (count > 0) {
            String url = UriBuilder.fromUri(BASE_ADDRESS_EMP)
                    .queryParam("page", page)
                    .queryParam("limit", limit)
                    .toString();
            WebTarget webResource = getClient().target(url);
            Response response = webResource
                    .request()
                    .header("Authorization", "Bearer un3P-Y3T_1563327096")
                    .accept(MediaType.APPLICATION_JSON_TYPE)
                    .get();
            String stringEntity = response.readEntity(String.class);
            HrisEmployeeSearchDto employeeSearchDto = objectMapper.readValue(stringEntity, HrisEmployeeSearchDto.class);

            for (HrisEmployeeDto dto: employeeSearchDto.getData()) {
                HrisOrgDto org = result.stream()
                        .filter(t -> t.getOrgeh().equals(dto.getOrgeh()) && StringUtils.isNotEmpty(dto.getOrgeh()))
                        .findFirst().orElse(null);
                if (org != null) {
                    HrisOrgDto organizationDto = new HrisOrgDto();
                    organizationDto.setOrgeh(dto.getPlans());
                    organizationDto.setParnt(dto.getOrgeh());
                    organizationDto.setOutxt(dto.getPosText());
                    organizationDto.setLevel(org.getLevel() + 1);

                    result.add(organizationDto);
                }

                result2.add(dto);

                try(PrintWriter output = new PrintWriter(new FileWriter("user.txt",true)))
                {
                    output.printf("%s\r\n", objectMapper.writeValueAsString(dto));
                }
                catch (Exception e) {}
            }

            count = count - employeeSearchDto.getData().size();
            page++;
        }
    }

    private void downloadPosisi(List<HrisOrgDto> result) throws IOException {
        Long count = countPosisi();
        ObjectMapper objectMapper = getObjectMapper();
        int limit = 10000;
        int page = 0;

        while (count > 0) {
            String url = UriBuilder.fromUri(BASE_ADDRESS_POS)
                    .queryParam("page", page)
                    .queryParam("limit", limit)
                    .toString();
            WebTarget webResource = getClient().target(url);
            Response response = webResource
                    .request()
                    .header("Authorization", "Bearer un3P-Y3T_1563327096")
                    .accept(MediaType.APPLICATION_JSON_TYPE)
                    .get();
            String stringEntity = response.readEntity(String.class);
            HrisPosisiSearchDto employeeSearchDto = objectMapper.readValue(stringEntity, HrisPosisiSearchDto.class);

            for (HrisPosisiDto dto: employeeSearchDto.getData()) {
                HrisOrgDto org = result.stream()
                        .filter(t -> t.getOrgeh().equals(dto.getOrgeh()) && StringUtils.isNotEmpty(dto.getOrgeh()))
                        .findFirst().orElse(null);
                if (org != null) {
                    HrisOrgDto organizationDto = new HrisOrgDto();
                    organizationDto.setOrgeh(dto.getPlans());
                    organizationDto.setParnt(dto.getOrgeh());
                    organizationDto.setOutxt(dto.getPstxt());
                    organizationDto.setOuabv(dto.getPsabv());
                    organizationDto.setLevel(org.getLevel() + 1);
                    organizationDto.setPosisi(true);
                    organizationDto.setChief(dto.getChief() !=null ? dto.getChief().toLowerCase().equals("x") ? true : false : false);
                    result.add(organizationDto);
                }

                try(PrintWriter output = new PrintWriter(new FileWriter("posisi.txt",true)))
                {
                    output.printf("%s\r\n", objectMapper.writeValueAsString(dto));
                }
                catch (Exception e) {}
            }

            count = count - employeeSearchDto.getData().size();
            page++;
        }
    }

    private void digDummy(List<HrisOrgDto> result) throws IOException {
        ObjectMapper objectMapper = getObjectMapper();

        try {
            File f = new File("org.txt");

            HrisOrgSearchDto hrisOrgSearchDto = objectMapper.readValue(FileUtils.readFileToString(f, "UTF-8"), HrisOrgSearchDto.class);
            for (HrisOrgDto dto: hrisOrgSearchDto.getData()) {
                HrisOrgDto organizationDto = new HrisOrgDto();
                organizationDto.setOrgeh(dto.getOrgeh());
                organizationDto.setParnt(dto.getParnt());
                organizationDto.setOutxt(dto.getOutxt());
                organizationDto.setOuabv(dto.getOuabv());
                organizationDto.setLevel(dto.getLevel() + 1);
                organizationDto.setPosisi(false);
                organizationDto.setChief(false);
                organizationDto.setPersa(dto.getPersa());
                result.add(organizationDto);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void digDummy2(List<HrisOrgDto> result) throws IOException {
        ObjectMapper objectMapper = getObjectMapper();

        try {
            File f = new File("posisi.txt");
            List<String> lines = FileUtils.readLines(f, "UTF-8");

            for (String line : lines) {
                if (StringUtils.isNotEmpty(line)) {
                    HrisPosisiDto dto = objectMapper.readValue(line, HrisPosisiDto.class);
                    HrisOrgDto org = result.stream()
                            .filter(t -> t.getOrgeh().equals(dto.getOrgeh()) && StringUtils.isNotEmpty(dto.getOrgeh()))
                            .findFirst()
                            .orElse(null);

                    if (org != null) {
                        HrisOrgDto organizationDto = new HrisOrgDto();
                        organizationDto.setOrgeh(dto.getPlans());
                        organizationDto.setParnt(dto.getOrgeh());
                        organizationDto.setOutxt(dto.getPstxt());
                        organizationDto.setOuabv(dto.getPsabv());
                        organizationDto.setLevel(org.getLevel() + 1);
                        organizationDto.setPosisi(true);
                        organizationDto.setChief(dto.getChief() !=null ? dto.getChief().toLowerCase().equals("x") ? true : false : false);

                        result.add(organizationDto);
                    } else {
                        try(PrintWriter output = new PrintWriter(new FileWriter("posisi-invalid.txt",true)))
                        {
                            output.printf("%s\r\n", line);
                        }
                        catch (Exception e) {}
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void digDummy3(List<HrisOrgDto> result, List<HrisEmployeeDto> result2) throws IOException {
        ObjectMapper objectMapper = getObjectMapper();

        try {
            File f = new File("user.txt");
            List<String> lines = FileUtils.readLines(f, "UTF-8");

            for (String line : lines) {
                if (StringUtils.isNotEmpty(line)) {
                    HrisEmployeeDto dto = objectMapper.readValue(line, HrisEmployeeDto.class);
                    HrisOrgDto org = result.stream()
                            .filter(t -> t.getOrgeh().equals(dto.getOrgeh()) && StringUtils.isNotEmpty(dto.getOrgeh()))
                            .findFirst().orElse(null);

                    if (org != null) {
                        HrisOrgDto organizationDto = new HrisOrgDto();
                        organizationDto.setOrgeh(dto.getPlans());
                        organizationDto.setParnt(dto.getOrgeh());
                        organizationDto.setOutxt(dto.getPosText());
                        organizationDto.setLevel(org.getLevel() + 1);

                        result.add(organizationDto);
                    } else {
                        try(PrintWriter output = new PrintWriter(new FileWriter("user-invalid.txt",true)))
                        {
                            output.printf("%s\r\n", line);
                        }
                        catch (Exception e) {}
                    }

                    result2.add(dto);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

        return objectMapper;
    }

    public Client getClient() {
        try {
            SSLContext sslcontext = SSLContext.getInstance("TLS");
            sslcontext.init(null, new TrustManager[]{new X509TrustManager()
            {
                public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException{}
                public X509Certificate[] getAcceptedIssuers()
                {
                    return new X509Certificate[0];
                }

            }}, new java.security.SecureRandom());

            HostnameVerifier allowAll = (hostname, session) -> true;

            final ClientConfig clientConfig = new ClientConfig()
                    .register(MultiPartFeature.class);

            return ClientBuilder
                    .newBuilder()
                    .sslContext(sslcontext)
                    .hostnameVerifier(allowAll)
                    .withConfig(clientConfig)
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw new InternalServerErrorException(null, e);
        }
    }
}
