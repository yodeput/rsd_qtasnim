package com.qtasnim.eoffice.test;

import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;

import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

@Singleton
public class UserSessionProducerMock {
    private Session user;

    public void setUser(Session user) {
        this.user = user;
    }

    /**
     * Mock UserSession
     * @return
     */
    @Produces
    @ISessionContext
    private Session produceLogger() {
        return user;
    }
}
