package com.qtasnim.eoffice.test.generator;

import org.apache.commons.text.RandomStringGenerator;
import org.junit.Test;

import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;

public class RandomStringGeneratorTest {
    @Test
    public void test() {
        RandomStringGenerator pwdGenerator = new RandomStringGenerator.Builder().withinRange('0', 'z')
                .filteredBy(LETTERS, DIGITS)
                .build();
        String username = pwdGenerator.generate(8);
        String password = pwdGenerator.generate(8);

    }
}
