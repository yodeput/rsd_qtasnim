package com.qtasnim.eoffice.test.scanner;

import com.qtasnim.eoffice.util.ClassUtil;
import com.qtasnim.eoffice.workflows.resolvers.ITaskValueResolver;
import com.qtasnim.eoffice.ws.dto.OptionDto;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassScannerTest {
    @Test
    public void test() throws IOException, ClassNotFoundException {
        List<OptionDto> optionDtos = new ArrayList<>();

        Arrays.stream(ClassUtil.getClasses("com.qtasnim.eoffice.workflows.resolvers")).filter(c -> Arrays.asList(c.getInterfaces()).contains(ITaskValueResolver.class)).forEach(clazz -> {
            optionDtos.add(new OptionDto(clazz));
        });

        optionDtos.size();
    }
}
