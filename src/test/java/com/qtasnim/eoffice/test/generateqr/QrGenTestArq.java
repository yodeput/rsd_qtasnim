package com.qtasnim.eoffice.test.generateqr;

import com.google.zxing.WriterException;
import com.qtasnim.eoffice.services.QRGenerateService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.awt.*;
import java.io.*;

@RunWith(Arquillian.class)
public class QrGenTestArq extends EjbUnitTestBase {
    @Inject
    QRGenerateService qrGenerateService;

    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.addClasses(
                            QrGenTestArq.class);
                });
    }

    @Test
    public void testS1() throws IOException, WriterException {
        String content = "E-Office-V2";

        byte[] res1 = qrGenerateService.getQRCode(content);
        byte[] res2 = qrGenerateService.getQRCode(content, 400);
        byte[] res3 = qrGenerateService.getQRCode(content, 200);
        byte[] res4 = qrGenerateService.getQRCode(content, 100);
        byte[] res5 = qrGenerateService.getQRCode(content, 80);

        //ImageIO.write(new ImageProcessing().byteToImage(res5), "png", new File("D://qrcode.png"));

        System.out.println("yey");
    }

    @Test
    public void testEmbedQRPdf() throws IOException, WriterException {
        String content = "E-Office-V2";

        byte[] qrCode = qrGenerateService.getQRCode(content, 80);

        byte[] pdf = null;

        File fileToUpload = new File("D://addqr1.pdf");

        try (FileInputStream fileInputStream = new FileInputStream(fileToUpload)) {
            pdf = new byte[fileInputStream.available()];
            fileInputStream.read(pdf);

//            byte[] pdfModed = qrGenerateService.embedQRCodeToPdf(pdf, qrCode);
            byte[] pdfModed = qrGenerateService.embedQRCodeToPdf(pdf, qrCode, new Point(460, 650));

            OutputStream out = new FileOutputStream("D://qr-added.pdf");
            out.write(pdfModed);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("yey");
    }
}
