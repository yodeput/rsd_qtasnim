package com.qtasnim.eoffice.test.generateqr;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.qtasnim.eoffice.util.QRCodeGenerateProcess;
import org.junit.Test;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// copied from https://skrymerdev.wordpress.com/2012/09/22/qr-code-generation-with-zxing/

public class QrGenTest {


    @Test
    public void test1() throws WriterException, IOException {
        int QRCODE_IMAGE_HEIGHT = 250;
        int QRCODE_IMAGE_WIDTH = 250;
        String IMAGE_PATH = "D://";

        QRCodeWriter qrWriter = new QRCodeWriter();
        BitMatrix matrix = qrWriter.encode("https://en.wikipedia.org/wiki/QR_code",
                BarcodeFormat.QR_CODE,
                QRCODE_IMAGE_WIDTH,
                QRCODE_IMAGE_HEIGHT);

        BufferedImage image = MatrixToImageWriter.toBufferedImage(matrix);
        File imageFile = new File(IMAGE_PATH, "qrcode.png");
        ImageIO.write(image, "PNG", imageFile);
    }

    @Test
    public void test2() throws WriterException, IOException {
        String pathQRResult = "D://qrcode.png";

        String pathOverlayImage= "D://kai.png";

        String content = "https://en.wikipedia.org/wiki/QR_code";

        int QRCODE_IMAGE_HEIGHT = 120;
        int QRCODE_IMAGE_WIDTH = 120;

        Map hints = new HashMap();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.MARGIN, 0);

        QRCodeWriter qrWriter = new  QRCodeWriter();
        BitMatrix matrix = qrWriter.encode(content,
                BarcodeFormat.QR_CODE,
                QRCODE_IMAGE_WIDTH,
                QRCODE_IMAGE_HEIGHT,
                hints);

        BufferedImage image = MatrixToImageWriter.toBufferedImage(matrix);
        BufferedImage overlay = scaleOverlay(image, ImageIO.read(new File(pathOverlayImage)));

        //Calculate the delta height and width
        int deltaHeight = image.getHeight() - overlay.getHeight();
        int deltaWidth  = image.getWidth()  - overlay.getWidth();

        //Draw the new image
        BufferedImage combined = new BufferedImage(QRCODE_IMAGE_HEIGHT, QRCODE_IMAGE_WIDTH, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D)combined.getGraphics();
        g.drawImage(image, 0, 0, null);
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        g.drawImage(overlay, (int)Math.round(deltaWidth/2), (int)Math.round(deltaHeight/2), null);

        File imageFile = new File(pathQRResult);
        ImageIO.write(combined, "PNG", imageFile);
    }

    private BufferedImage scaleOverlay(BufferedImage qrcode, BufferedImage overlay)
    {
        float ratio = overlay.getWidth()/overlay.getHeight();

        int scaledHeight = Math.round(qrcode.getHeight() * .15f);
        int scaledWidth = Math.round(scaledHeight * ratio);

        BufferedImage imageBuff = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics g = imageBuff.createGraphics();
        g.drawImage(overlay.getScaledInstance(scaledWidth, scaledHeight, BufferedImage.SCALE_SMOOTH), 0, 0, new Color(0,0,0), null);
        g.dispose();
        return imageBuff;
    }
}
