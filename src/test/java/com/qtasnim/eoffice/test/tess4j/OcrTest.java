package com.qtasnim.eoffice.test.tess4j;

import com.qtasnim.eoffice.services.OCRService;
import com.qtasnim.eoffice.services.TextExtractionService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.test.camunda.EngineTest;
import com.qtasnim.eoffice.test.camunda.ProcessDefinitionResourceMock;
import com.qtasnim.eoffice.test.templating.Templating;
import com.qtasnim.eoffice.util.OCRProcess;
import net.sourceforge.tess4j.TesseractException;
import org.apache.commons.io.IOUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;

@RunWith(Arquillian.class)
public class OcrTest extends EjbUnitTestBase {

    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.addClasses(
                            Templating.class);
                });
    }

    @Inject
    OCRService ocrService;

    @Inject
    TextExtractionService textExtractionService;

    @Test
    public void test() throws Exception {
        File fileToUpload = new File(this.getClass().getResource("/tess4j/multipage_tiff_example.tif").toURI());
        byte[] content = null;

        try (FileInputStream inputStream = new FileInputStream(fileToUpload)) {
            content = IOUtils.toByteArray(inputStream);
        }

        assertNotNull(content);

        String text;
        text = ocrService.extractText(content);

        assertNotNull(text);
    }

    @Test
    public void test2() throws Exception {
        String filename = "multipage_tiff_example.tif";
        File fileToUpload = new File(this.getClass().getResource("/tess4j/"+filename).toURI());

        List<String> text = ocrService.extractText(new FileInputStream(fileToUpload), filename);

        System.out.println("yey");
    }
}
