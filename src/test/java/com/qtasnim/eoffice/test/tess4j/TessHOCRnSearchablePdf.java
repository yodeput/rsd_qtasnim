package com.qtasnim.eoffice.test.tess4j;

import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.util.ImageProcessing;
import com.qtasnim.eoffice.util.TesseractDataInitializer;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.leptonica.PIX;
import org.bytedeco.tesseract.TessBaseAPI;
import org.bytedeco.tesseract.TessResultRenderer;
import org.bytedeco.tesseract.global.tesseract;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

import static org.bytedeco.leptonica.global.lept.pixRead;

@RunWith(Arquillian.class)
public class TessHOCRnSearchablePdf extends EjbUnitTestBase {

    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.addClasses(
                            TessHOCRnSearchablePdf.class);
                });
    }

    @Test
    public void testHOCR() throws Exception {
        TessBaseAPI api = new TessBaseAPI();

        BytePointer outText;

        if (api.Init(TesseractDataInitializer.GetTessDataTempPath(), "eng") != 0) {
            throw new Exception("Could not initialize tesseract.");
        }

        File tiff = new File(this.getClass().getResource("/tess4j/multipage_tiff_example.tif").toURI());

        PIX image = pixRead(tiff.getPath());
        api.SetImage(image);

        outText = api.GetHOCRText(3);
        String result = outText.getString();

        System.out.println(result);

        Assert.assertNotNull(result);
    }

    @Test
    public void testSearchablePdf() throws Exception {
        TessBaseAPI api = new TessBaseAPI();

        BytePointer outText;

        if (api.Init(TesseractDataInitializer.GetTessDataTempPath(), "eng") != 0) {
            throw new Exception("Could not initialize tesseract.");
        }

        File tiff = new File(this.getClass().getResource("/tess4j/multipage_tiff_example.tif").toURI());

        String singleImage = "C:\\Users\\Lenovo\\Pictures\\gambar-artikel-16-a.jpg";

        String pdfFile = "D:\\Local Disk\\Download\\99023942-275b-4f20-9e61-537ed8ca9fdf.pdf";
        File pdf = new File(pdfFile);
        File tiffOutput = new File("D:/t.tif");

        ImageProcessing impro = new ImageProcessing();
        List<BufferedImage> images = impro.pdfToImages(pdf);
        impro.writeImagesToTiff(images, tiffOutput);

//        PIX image = pixRead(tiff.getPath());
//        api.SetImage(image);

        TessResultRenderer renderer = tesseract.TessPDFRendererCreate("D:/sa.pdf", TesseractDataInitializer.GetTessDataTempPath(), false);

//        api.ProcessPages(tiff.getPath(), null, 0, renderer);
//        api.ProcessPages(singleImage, null, 0, renderer);
//        api.ProcessPages(pdfFile, null, 0, renderer); //failed
        api.ProcessPages(tiffOutput.getPath(), null, 0, renderer);

        api.End();
        renderer.deallocate();

        tesseract.TessDeleteResultRenderer(renderer);

        System.out.println("yey");
    }


}
