package com.qtasnim.eoffice.test.docIdGet;

import com.qtasnim.eoffice.services.JenisDokumenDocLibService;
import com.qtasnim.eoffice.services.SuratDocLibService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.test.masterdata.Formdefinition;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(Arquillian.class)
public class DocIdRetrieveTest extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.addClasses(
                            DocIdRetrieveTest.class);
                });
    }

    @Inject
    private JenisDokumenDocLibService jenisDokumenDocLibService;

    @Inject
    private SuratDocLibService suratDocLibService;

    @Test
    public void testJenisDokumenGetDocId() {

    }

    @Test
    public void testSuratGetDocId() {

    }
}
