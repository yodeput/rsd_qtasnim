package com.qtasnim.eoffice.test.surat;

import com.qtasnim.eoffice.db.UserSession;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.db.SuratInteraction;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.services.MasterUserService;
import com.qtasnim.eoffice.services.SuratInteractionService;
import com.qtasnim.eoffice.services.SuratService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import com.qtasnim.eoffice.test.UserSessionProducerMock;
import com.qtasnim.eoffice.ws.dto.RiwayatDto;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Arquillian.class)
public class SuratInterationTest extends EjbUnitTestBase {
    @Deployment
    public static Archive<?> deploy() {
        return createDeployment(
                war -> {
                    war.delete("WEB-INF/classes/com/qtasnim/eoffice/security/AuthenticatedUserProducer.class"); //delete default user userSession producers
                    war.addClasses(
                            SuratInterationTest.class,
                            SuratInteractionResourceMock.class,
                            UserSessionProducerMock.class);
                });
    }

    @Inject
    private SuratInteractionResourceMock suratInteractionResourceMock;

    @Inject
    private UserSessionProducerMock userSessionProducerMock;

    @Inject
    private SuratInteractionService suratInteractionService;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    private SuratService suratService;

    @Test
    public void testRead() {
        userSessionProducerMock.setUser(new Session() {
            {
                setUserSession(new UserSession());
                getUserSession().setUser(masterUserService.getAllValid().skip(0).limit(1).findFirst().orElse(null));
            }
        });

        Surat surat = suratService.getAll().skip(0).limit(1).findFirst().orElse(null);

        if (surat != null) {
            suratInteractionResourceMock.setIsRead(surat.getId());
            Response riwayat = suratInteractionResourceMock.riwayat(surat.getId(), 0, 10);

            List<RiwayatDto> result = (List<RiwayatDto>)riwayat.getEntity();
            assertNotEquals(result.size(), 0);

            List<SuratInteraction> all = suratInteractionService.findAll();
            assertNotEquals(all.size(), 0);
            all.forEach(t -> suratInteractionService.remove(t));
        }
    }
}
