package com.qtasnim.eoffice.test.surat;

import com.qtasnim.eoffice.ws.SuratInteractionResource;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class SuratInteractionResourceMock extends SuratInteractionResource {
}
