package com.qtasnim.eoffice.test.echo;

import com.qtasnim.eoffice.services.EchoService;
import com.qtasnim.eoffice.test.EjbUnitTestBase;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

@RunWith(Arquillian.class)
public class EchoTest extends EjbUnitTestBase {
    @Inject
    private EchoService echoService;

    @Deployment
    public static Archive<?> deploy() throws IOException {
        return EchoTest.createDeployment(EchoTest.class);
    }

    @Test
    public void test() {
        String message = "test";
        String echo = echoService.echo(message);

        assertEquals(message, echo);
    }
}
