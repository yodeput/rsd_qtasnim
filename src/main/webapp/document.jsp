<!--*
*
* (c) Copyright Ascensio System SIA 2019
*
* The MIT License (MIT)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*-->

<%@page import="com.qtasnim.eoffice.office.FileModelFactory"%>
<%@ page import="java.util.UUID" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ONLYOFFICE</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="css/editor.css" />

    <% FileModelFactory fileModelFactory = (FileModelFactory) request.getAttribute("fileModelFactory"); %>

    <script type="text/javascript" src="${docserviceApiUrl}"></script>

    <script type="text/javascript" language="javascript">

        var docEditor;

        var innerAlert = function (message) {
            if (console && console.log)
                console.log(message);
        };

        var onAppReady = function () {
            innerAlert("Document editor ready");
        };

        var onDocumentStateChange = function (event) {
            var title = document.title.replace(/\*$/g, "");
            document.title = title + (event.data ? "*" : "");
        };

        var onRequestEditRights = function () {
            location.href = location.href.replace(RegExp("mode=view\&?", "i"), "");
        };

        var onError = function (event) {
            if (event)
                innerAlert(event.data);
        };

        var onOutdatedVersion = function (event) {
            location.reload(true);
        };

        var сonnectEditor = function () {
            var config = <%= fileModelFactory.build() %>;
            config.width = "100%";
            config.height = "100%";
            config.events = {
                "onAppReady": onAppReady,
                "onDocumentStateChange": onDocumentStateChange,
                'onRequestEditRights': onRequestEditRights,
                "onError": onError,
                "onOutdatedVersion": onOutdatedVersion,
            };
            config.plugins = {
                "autostart": [
                    ${autoStartPlugins}
                ],
            }

            docEditor = new DocsAPI.DocEditor("iframeEditor", config);
        };

        var configPluginReqResp = {
            originOnlyOffice: "${originOnlyoffice}",
            reqKey: "hesoyam-aezakmi-bringiton-ripazha",
            reqContent: "reqIdJenisDokumen",
            respKey: "kmzway87aa",
            respType: "respIdJenisDokumen",
            apiPath: "${apiPath}"
        }

        function listenerRequestIdJenisDokumen(event) {
            if(event.origin === configPluginReqResp.originOnlyOffice) {
                if(event.data.key != null) {
                    if(event.data.key === configPluginReqResp.reqKey && event.data.content === configPluginReqResp.reqContent) {console.log("listenerRequestIdJenisDokumen")
                        var data = {
                            content : "${idJenisDokumen}",
                            type : configPluginReqResp.respType,
                            key : configPluginReqResp.respKey,
                            apiPath : configPluginReqResp.apiPath
                        };

                        var _iframe = document.getElementsByName("frameEditor")[0];
                        if (_iframe) {
                            _iframe.contentWindow.postMessage(data, configPluginReqResp.originOnlyOffice);
                        }
                    }
                }
            }
        }

        if (window.addEventListener) {
            window.addEventListener("load", сonnectEditor);
        } else if (window.attachEvent) {
            window.attachEvent("load", сonnectEditor);
        }

        // listener for onlyoffice api request idJenisDokumen
        window.addEventListener("message", listenerRequestIdJenisDokumen);
    </script>

</head>
<body>
<div class="form">
    <div id="iframeEditor"></div>
</div>
</body>
</html>
