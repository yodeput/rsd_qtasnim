package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_reset_password")
@XmlRootElement
public class ForgotPassword {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name="u_id")
    private String uuid;

    @Basic(optional = false)
    @Column(name="id_user")
    private Long userId;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_valid")
    private Boolean isValid;
}
