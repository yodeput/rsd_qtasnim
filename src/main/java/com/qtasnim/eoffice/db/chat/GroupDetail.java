package com.qtasnim.eoffice.db.chat;

import com.qtasnim.eoffice.db.MasterUser;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@Table(name = "t_chat_group_detail")
@Entity
@XmlRootElement
public class GroupDetail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "gid", referencedColumnName = "id")
    @ManyToOne
    private Group group;

    @JoinColumn(name = "uid", referencedColumnName = "id")
    @ManyToOne
    private MasterUser user;

    @Basic()
    @Column(name="add_time")
    private Date addTime;
}
