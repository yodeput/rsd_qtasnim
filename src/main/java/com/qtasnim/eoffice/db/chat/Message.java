package com.qtasnim.eoffice.db.chat;

import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.listeners.ChatMessageListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@Table(name = "t_chat_message")
@Entity
@EntityListeners(ChatMessageListener.class)
@XmlRootElement
public class Message {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "uid", referencedColumnName = "id")
    @ManyToOne
    private MasterUser to;

    @JoinColumn(name = "fromuser", referencedColumnName = "id")
    @ManyToOne
    private MasterUser from;

    @JoinColumn(name = "gid", referencedColumnName = "id")
    @ManyToOne
    private Group group;

    @Basic()
    @Column(name="msg")
    private String msg;

    @Basic()
    @Column(name="chat_id")
    private String chatId;

    @Basic()
    @Column(name="add_time")
    private Date addTime;
}
