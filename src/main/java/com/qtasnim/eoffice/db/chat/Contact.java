package com.qtasnim.eoffice.db.chat;

import com.qtasnim.eoffice.db.MasterUser;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@Table(name = "t_chat_contact")
@Entity
@XmlRootElement
public class Contact {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "fromUid", referencedColumnName = "id")
    @ManyToOne
    private MasterUser from;

    @JoinColumn(name = "toUid", referencedColumnName = "id")
    @ManyToOne
    private MasterUser to;

    @Basic(optional = false)
    @NotNull
    @Column(name = "unread")
    private Long unread;
}
