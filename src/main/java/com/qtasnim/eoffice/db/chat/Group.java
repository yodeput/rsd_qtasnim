package com.qtasnim.eoffice.db.chat;

import com.qtasnim.eoffice.db.MasterUser;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Table(name = "t_chat_group")
@Entity
@XmlRootElement
public class Group {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name="name")
    private String name;

    @Basic()
    @Column(name="headphoto")
    private byte[] headphoto;

    @Basic()
    @Column(name="desc")
    private String desc;

    @JoinColumn(name = "owner_uid", referencedColumnName = "id")
    @ManyToOne
    private MasterUser owner;

    @Basic()
    @Column(name="add_time")
    private Date addTime;

    @OneToMany(mappedBy = "group")
    private List<GroupDetail> details;
}
