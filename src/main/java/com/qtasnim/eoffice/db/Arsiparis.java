package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_arsiparis")
@XmlRootElement
public class Arsiparis implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "pejabat_id", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi pejabat;

    @JoinColumn(name = "id_arsiparis", referencedColumnName = "id_arsiparis", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private MasterArsiparis arsiparis;
}
