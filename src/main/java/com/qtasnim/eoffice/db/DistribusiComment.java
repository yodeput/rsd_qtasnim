package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import com.qtasnim.eoffice.services.DistribusiDokumenService;
import com.qtasnim.eoffice.util.BeanUtil;
import com.qtasnim.eoffice.workflows.IWorkflowService;
import com.qtasnim.eoffice.workflows.WorkflowException;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_distribusi_comment")
@XmlRootElement
public class DistribusiComment implements Serializable, IAuditTrail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @NotNull
    @Column(name = "komentar", columnDefinition = "text")
    private String komentar;

    @JoinColumn(name = "id_distribusi", referencedColumnName = "id")
    @ManyToOne
    private DistribusiDokumen distribusi;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser user;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_deleted")
    private Boolean isDeleted = false;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date",columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

}
