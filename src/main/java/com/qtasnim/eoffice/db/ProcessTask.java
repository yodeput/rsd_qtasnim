package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.db.converters.ExecutionStatusConverter;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "process_task")
@XmlRootElement
public class ProcessTask implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name="task_id")
    private String taskId;

    @Basic(optional = false)
    @NotNull
    @Column(name="task_name")
    private String taskName;

    @Basic(optional = false)
    @NotNull
    @Column(name="task_type")
    private String taskType;

    @Basic()
    @Column(name="related_assignee_record_id")
    private String relatedAssigneeRecordId;

    @Basic(optional = false)
    @NotNull
    @Column(name="execution_status")
    @Convert(converter = ExecutionStatusConverter.class)
    private ExecutionStatus executionStatus = ExecutionStatus.PENDING;

    @Basic()
    @Column(name="response_var")
    private String responseVar;

    @Basic()
    @Column(name="response")
    private String response;

    @JoinColumn(name = "m_struktur_organisasi_id_organisasi", referencedColumnName = "id_organization", nullable = false)
    @ManyToOne
    @NotNull
    private MasterStrukturOrganisasi assignee;

    @JoinColumn(name = "process_instance_id", referencedColumnName = "id", nullable = false)
    @ManyToOne
    @NotNull
    private ProcessInstance processInstance;

    @Basic(optional = false)
    @NotNull
    @Column(name = "assigned_date")
    private Date assignedDate;

    @Basic()
    @Column(name = "executed_date")
    private Date executedDate;

    @Basic()
    @Column(name="note")
    private String note;

    @JoinColumn(name = "previous_id", referencedColumnName = "id")
    @ManyToOne
    private ProcessTask previous;

    @Basic()
    @Column(name = "seq",columnDefinition = "TINYINT")
    private Integer seq;

    @Basic()
    @Column(name = "is_hadir")
    private Boolean isHadir;
}
