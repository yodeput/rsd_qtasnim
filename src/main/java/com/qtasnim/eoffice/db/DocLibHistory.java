package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "doc_lib_history")
@XmlRootElement
public class DocLibHistory implements ICreationAudit {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="version")
    private String version;

    @Basic(optional = false)
    @NotNull
    @Column(name="doc_size")
    private Long size = 0L;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="doc_id")
    private String docId;

    @JoinColumn(name = "m_user_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private MasterUser user;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;
}
