package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_vendor")
@XmlRootElement
public class MasterVendor implements Serializable, IAuditTrail {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id_vendor")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="nama_vendor")
    private String nama;

    @Basic()
    @Column(name="alamat")
    private String alamat;

    @Basic()
    @Column(name="phone", length = 50)
    private String phone;

    @Basic()
    @Column(name="email", length = 100)
    private String email;

    @Basic()
    @Column(name="pic")
    private String pic;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_active")
    private Boolean isActive=true;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @OneToMany(mappedBy = "vendor")
    @JsonIgnore
    private List<MasterUser> user;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @JoinColumn(name = "id_kategori_kontak",referencedColumnName = "id")
    @OneToOne
    private MasterKategoriEksternal kategoriEksternal;

    @Basic()
    @Column(name="nama_unit", length = 255)
    private String namaUnit;

    @Basic()
    @Column(name="website", length = 255)
    private String website;

    @Basic()
    @Column(name="kode_pos", length = 255)
    private String kodePos;

    @Basic()
    @Column(name="fax", length = 255)
    private String fax;

    @JoinColumn(name = "id_propinsi",referencedColumnName = "id")
    @OneToOne
    private MasterPropinsi propinsi;

    @JoinColumn(name = "id_kota",referencedColumnName = "id_kota")
    @OneToOne
    private MasterKota kota;

    public MasterVendor() {
        user = new ArrayList<>();
    }
}
