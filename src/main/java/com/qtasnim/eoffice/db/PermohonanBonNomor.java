package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.DocListener;
import com.qtasnim.eoffice.util.FieldMetadata;
import com.qtasnim.eoffice.util.MetadataValueType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(DocListener.class)
@Table(name = "t_permohonan_bon_nomor")
@XmlRootElement
public class PermohonanBonNomor implements Serializable,IDocTrail {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name="no_registrasi")
    private String noRegistrasi;

    @JoinColumn(name = "id_jenis_dokumen", referencedColumnName = "id_jenis_dokumen")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterJenisDokumen jenisDokumen;

    @Basic()
    @Column(name="tgl_target",columnDefinition = "TIMESTAMP")
    private Date tglTarget;


    @JoinColumn(name = "id_organization_konseptor", referencedColumnName = "id_organization")
    @OneToOne
    private MasterStrukturOrganisasi orgKonseptor;

    @JoinColumn(name = "id_user_konseptor", referencedColumnName = "id")
    @OneToOne
    private MasterUser userKonseptor;

    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @NotNull
    @Column(name="perihal")
    private String perihal;

    @Basic()
    @Column(name="status",columnDefinition = "TINYINT")
    private Integer status;

    @JoinColumn(name = "company_id", referencedColumnName = "id", nullable = false)
    @OneToOne
    private CompanyCode companyCode;

    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date createdDate;

    @Basic(optional = true)
    @Size(min = 1, max = 50)
    @Column(name="modified_by", nullable = true)
    private String modifiedBy;

    @Basic(optional = true)
    @Column(name="modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @Basic()
    @Column(name="is_deleted")
    private Boolean isDeleted=false;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "t_pemeriksa_bon_nomor",
            joinColumns = @JoinColumn(name = "id_permohonan"),
            inverseJoinColumns = @JoinColumn(name = "id_organization")
    )
    @FieldMetadata(nama="Pemeriksa_List", type= MetadataValueType.MULTI_VALUE, format = "${organizationName} | ${organizationCode} | ${user.nameFront} ${user.nameMiddleLast}")
    private List<MasterStrukturOrganisasi> pemeriksaList;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "t_penandatangan_bon_nomor",
            joinColumns = @JoinColumn(name = "id_permohonan"),
            inverseJoinColumns = @JoinColumn(name = "id_organization")
    )
    @FieldMetadata(nama="Penandatangan_List", type= MetadataValueType.MULTI_VALUE, format = "${organizationName} | ${organizationCode} | ${user.nameFront} ${user.nameMiddleLast}")
    private List<MasterStrukturOrganisasi> penandatanganList;


    public PermohonanBonNomor(){}
}
