package com.qtasnim.eoffice.db;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@Embeddable
public class RoleUserPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_role")
    private Long roleId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_organization")
    private Long organizationId;

    public RoleUserPK() {
    }

    public RoleUserPK(Long roleId, Long organizationId) {
        this.roleId = roleId;
        this.organizationId = organizationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int)(long)roleId;
        hash += (int)(long)organizationId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof RoleUserPK)) {
            return false;
        }

        RoleUserPK other = (RoleUserPK) object;

        if (this.roleId != other.roleId) {
            return false;
        }

        if (this.organizationId != other.organizationId) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "com.qtasnim.eoffice.db.RoleUserPK[ roleId=" + roleId + ", organizationId=" + organizationId + "]";
    }
}
