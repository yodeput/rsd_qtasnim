package com.qtasnim.eoffice.db;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Immutable
@Table(name = "v_list_inbox_need_att")
@Getter
@Setter
public class ViewListInboxNeedAtt {
    @Id
    @Column(name="id_surat")
    private Long idSurat;

    @Column(name="tgl_dokumen")
    private Date tglDokumen;

    @Column(name="id_unit")
    private String idUnit;

    @Column(name="pemerima_assignee")
    private Long pemerimaAssignee;

    @Column(name="disposisi_assignee")
    private Long disposisiAssignee;

    @Column(name="task_assignee")
    private Long taskAssignee;
}
