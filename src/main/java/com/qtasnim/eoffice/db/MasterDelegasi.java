package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_delegasi")
@XmlRootElement
public class MasterDelegasi implements Serializable, IAuditTrail {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="id_delegasi")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "delegasi_from", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterStrukturOrganisasi from;

    @JoinColumn(name = "delegasi_to", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterStrukturOrganisasi to;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 10)
    @Column(name="tipe")
    private String tipe;

    @Basic(optional = false)
    @NotNull
    @Column(name="tgl_mulai")
    private Date mulai;

    @Basic(optional = false)
    @NotNull
    @Column(name="tgl_selesai")
    private Date selesai;

    @Basic(optional = false)
    @NotNull
    @Column(name="alasan", columnDefinition="TEXT")
    private String alasan;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_deleted")
    private Boolean isDeleted;
}
