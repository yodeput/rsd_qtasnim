package com.qtasnim.eoffice.db;

import id.kai.ws.dto.HrisOrgDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationWrapper {
    private MasterStrukturOrganisasi masterStrukturOrganisasi;
    private HrisOrgDto organizationDto;
    private String code;
    private String name;
    private String singkatan;
    private String parent;
    private boolean isPosisi;
    private boolean isChief;
    private String persa;
    private String company;
    private String clazz;
    private String grade;
    private String korsa;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrganizationWrapper that = (OrganizationWrapper) o;
        return Objects.equals(code, that.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
