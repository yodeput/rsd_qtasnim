package com.qtasnim.eoffice.db;

public enum Media {
    VHS("VHS"), BETAMAX("BETAMAX"), CD("CD");

    String toString;

    private Media(String toString) {
        this.toString = toString;
    }

    private Media() {
    }

    public String toString() {
        return this.toString != null?this.toString:super.toString();
    }
}
