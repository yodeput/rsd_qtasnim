package com.qtasnim.eoffice.db;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@Embeddable
public class SuratFormPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_surat")
    private Long surat;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_form_value")
    private Long formValue;

    public SuratFormPK() {
    }

    public SuratFormPK(Long surat, Long formValue) {
        this.surat = surat;
        this.formValue = formValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int)(long)surat;
        hash += (int)(long)formValue;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SuratFormPK)) {
            return false;
        }

        SuratFormPK other = (SuratFormPK) object;

        if (this.surat != other.surat) {
            return false;
        }

        if (this.formValue != other.formValue) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "com.qtasnim.eoffice.db.SuratFormPK[ surat=" + surat + ", formValue=" + formValue + "]";
    }
}
