package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_penandatangan_surat_sign_passphrase")
@XmlRootElement
public class PenandatanganSuratSignPassphrase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_surat", referencedColumnName = "id", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Surat surat;

    @JoinColumn(name = "id_signer", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private MasterUser masterUser;

    @Basic(optional = false)
    @NotNull
    @Column(name="passphrase")
    private String passphrase;
}
