package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "doc_lib_item")
@XmlRootElement
public class DocLibItem {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 1000)
    @Column(name="doc_name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 1000)
    @Column(name="doc_generated_name")
    private String generatedName;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 1000)
    @Column(name="doc_path")
    private String path;

    @Basic(optional = false)
    @NotNull
    @Column(name="doc_size")
    private Long size = 0L;

    @JoinColumn(name = "m_mime_mapping_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private MasterMimeMapping extension;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name="doc_id")
    private String docLibId;

    @JoinColumn(name = "m_doclib_provider_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private MasterDocLibProvider docLibProvider;

    @Version
    @Column(name="version", nullable = false)
    @NotNull
    private long version = 0L;
}
