package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.DocListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(DocListener.class)
@Table(name = "t_penandatangan_permohonan")
@XmlRootElement
public class PenandatanganPermohonan {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_user", referencedColumnName = "id")
    @OneToOne
    private MasterUser user;

    @JoinColumn(name = "id_permohonan", referencedColumnName = "id")
    @OneToOne
    private PermohonanDokumen permohonan;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne
    private MasterStrukturOrganisasi organisasi;


    public PenandatanganPermohonan(){}
}
