package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_surat_undangan")
@XmlRootElement
public class SuratUndangan implements Serializable, IAuditTrail {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_surat", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    private Surat surat;

    @Basic(optional = false)
    @Column(name="start_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date start;

    @Basic(optional = false)
    @Column(name="end_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date end;

    @Basic(optional = false)
    @Column(name="zona")
    @NotNull
    private String zona;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date createdDate;

    @Basic()
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic(optional = true)
    @Column(name="modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;
}
