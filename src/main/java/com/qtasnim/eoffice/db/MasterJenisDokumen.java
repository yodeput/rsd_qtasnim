package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_jenis_dokumen")
@XmlRootElement
public class MasterJenisDokumen implements Serializable, IAuditTrail {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id_jenis_dokumen")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idJenisDokumen;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 10)
    @Column(name="kode_jenis_dokumen")
    private String kodeJenisDokumen;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name="nama_jenis_dokumen")
    private String namaJenisDokumen;

    @Basic()
    @Column(name="is_active")
    private Boolean isActive;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic()
    @Column(name="is_korespondensi")
    private Boolean isKorespondensi;

    @JoinColumn(name = "grup_dokumen", referencedColumnName = "id_jenis_dokumen", nullable = true)
    @OneToOne
    private MasterJenisDokumen group;

    @Basic(optional = false)
    @Column(name="base_code")
    private String baseCode;

    @JoinColumn(name = "id_parent", referencedColumnName = "id_jenis_dokumen")
    @ManyToOne
    private MasterJenisDokumen parent;

    @Basic(optional = false)
    @Column(name="is_hidden")
    private Boolean isHidden = false;

    @Basic(optional = false)
    @Column(name="is_deletable")
    private Boolean isDeletable = true;

    @Basic(optional = false)
    @Column(name="is_multi_pemeriksa")
    private Boolean isMultiPemeriksa = true;

    @Basic(optional = false)
    @Column(name="is_pemeriksa_mandatory")
    private Boolean isPemeriksaMandatory = false;

    @Basic(optional = false)
    @Column(name="is_include_pejabat_mengetahui")
    private Boolean isIncludePejabatMengetahui = false;

    @Basic(optional = false)
    @Column(name="is_regulasi")
    @NotNull
    private Boolean isRegulasi = false;

    @Basic(optional = false)
    @NotNull
    @Column(name = "jumlah_penandatangan")
    private Integer jumlahPenandatangan = 1;
}
