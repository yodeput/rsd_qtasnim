package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_role_m_module")
@XmlRootElement
public class RoleModule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_role", referencedColumnName = "id_role", nullable = false)
    @ManyToOne()
    private MasterRole roles;

    @JoinColumn(name = "id_modul", referencedColumnName = "id_modul", nullable = false)
    @ManyToOne()
    private MasterModul modules;

    @Basic()
    @Column(name="allow_create")
    private Boolean allowCreate;

    @Basic()
    @Column(name="allow_delete")
    private Boolean allowDelete;

    @Basic()
    @Column(name="allow_print")
    private Boolean allowPrint;

    @Basic()
    @Column(name="allow_read")
    private Boolean allowRead;

    @Basic()
    @Column(name="allow_update")
    private Boolean allowUpdate;
}
