package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.workflows.IWorkflowService;
import com.qtasnim.eoffice.workflows.WorkflowException;

import java.util.List;

public interface IWorkflowEntity {
    String getRelatedId() throws WorkflowException;
    EntityStatus getCompletionStatus();
    void setCompletionStatus(EntityStatus completionStatus);
    String getCreatedBy();
    FormDefinition getFormDefinition();
    String getModuleName();
    String getNoDokumen();
    String getPageLink();
    String getClassName();
    List<ProcessInstance> getProcessInstances();
    void setProcessInstances(List<ProcessInstance> processInstances);

    // < Mail Props >
    List<FormValue> getFormValue();

    String getJenisDokumen();
    void setJenisDokumen(String param);

    String getFormattedTanggal();
    void setFormattedTanggal(String param);

    String getFormattedSender();
    void setFormattedSender(String param);

    String getNomor();
    void setNomor(String param);

    String getPerihal();
    void setPerihal(String param);

    String getTaskExecutor();
    void setTaskExecutor(String param);

    String getTaskResponse();
    void setTaskResponse(String param);

    String getKlasifikasiKeamanan();
    void setKlasifikasiKeamanan(String param);

    IWorkflowService getWorkflowService();

    // </ Mail Props >
}
