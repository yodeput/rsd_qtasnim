/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author THINKPAD-PC
 */
@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_regulasi")
@XmlRootElement
public class Regulasi implements Serializable, IAuditTrail{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @NotNull
    @Size(max = 255)
    @Column(name = "nomor_regulasi", length = 255)
    private String nomorRegulasi;
    
    @Basic()
    @NotNull
    @Size(max = 255)
    @Column(name = "nama_regulasi", length = 255)
    private String namaRegulasi;
    
    @JoinColumn(name = "regulasi_pengganti_id", referencedColumnName = "id", nullable = true)
    @OneToOne
    @JsonIgnore
    private Regulasi regulasiPengganti;
    
    @JoinColumn(name = "jenis_regulasi_id", referencedColumnName = "id_jenis_regulasi")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterJenisRegulasi jenisRegulasi;
    
    @Basic()
    @Column(name = "deskripsi", columnDefinition = "text")
    private String deskripsi;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_regulasi_perusahaan")
    private Boolean isRegulasiPerusahaan = false;
    
    @Basic()
    @Column(name="tanggal_mulai_berlaku")
    private Date tanggalMulaiBerlaku;

    @Basic()
    @Column(name = "tanggal_selesai_berlaku")
    private Date tanggalSelesaiBerlaku;
    
    @Basic()
    @Column(name = "tanggal_terbit")
    private Date tanggalTerbit;
    
    @JoinColumn(name = "id_klasifikasi_dokumen", referencedColumnName = "id_klasifikasi_masalah")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterKlasifikasiMasalah klasifikasiDokumen;
    
    @Basic()
    @Column(name = "status", columnDefinition = "ENUM")
    private String status;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_published")
    private Boolean isPublished = false;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;
    
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;
    
    @JoinColumn(name = "unit_konseptor_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterKorsa unitKonseptor;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by", length = 255)
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date", columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "modified_by", length = 255)
    private String modifiedBy;

    @Basic()
    @Column(name = "modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @Basic(optional = false)
    @Column(name="is_starred")
    private Boolean isStarred = false;
    
    @OneToMany(
            mappedBy = "regulasi",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<RegulasiTerkait> regulasiIndukTerkaitList;
    
    @OneToMany(
            mappedBy = "regulasi",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<RegulasiTerkait> regulasiTerkaitList;
    
    @OneToMany(
            mappedBy = "regulasi",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<RegulasiPengganti> regulasiIndukPenggantiList;
    
    @OneToMany(
            mappedBy = "regulasi",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<RegulasiPengganti> regulasiPenggantiList;
    
    @OneToMany(
            mappedBy = "regulasi",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<RegulasiHistory> regulasiHistoryList;
    
    public Regulasi() {
        regulasiIndukTerkaitList = new ArrayList<>();        
        regulasiTerkaitList = new ArrayList<>();        
        regulasiIndukPenggantiList = new ArrayList<>();
        regulasiPenggantiList = new ArrayList<>();
        regulasiHistoryList = new ArrayList<>();
    }
}
