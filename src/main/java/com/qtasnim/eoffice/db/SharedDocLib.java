package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_shared_doclib")
@XmlRootElement
public class SharedDocLib {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name="document_id")
    private String docId;

    @Basic(optional = false)
    @NotNull
    @Column(name="token")
    private String token;

    @Basic(optional = false)
    @NotNull
    @Column(name="expired_date")
    private Date expiredDate;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="username")
    private String username;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="password")
    private String password;

    @OneToOne(mappedBy = "sharedDocLib")
    private SharedDocLibVendor sharedDocLibVendor;
}
