package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_tindakan_disposisi")
@XmlRootElement
public class TindakanDisposisi implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_surat_disposisi", referencedColumnName = "id", nullable = false)
    @ManyToOne()
    private SuratDisposisi suratDisposisi;

    @JoinColumn(name = "id_tindakan", referencedColumnName = "id_tindakan")
    @OneToOne
    private MasterTindakan tindakan;

    public TindakanDisposisi() {}
}
