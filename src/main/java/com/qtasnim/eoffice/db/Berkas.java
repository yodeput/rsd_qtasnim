package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.hpsf.Decimal;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_berkas")
@XmlRootElement
public class Berkas implements Serializable, IAuditTrail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_unit", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterKorsa unit;

    @JoinColumn(name = "id_klasifikasi_masalah", referencedColumnName = "id_klasifikasi_masalah")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterKlasifikasiMasalah klasifikasiMasalah;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nomor_berkas")
    private String nomor;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "judul_berkas")
    private String judul;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "jenis_kegiatan")
    private String jenis;

    @Basic()
    @Column(name = "jra_aktif_date")
    private Date jraAktif;

    @Basic()
    @Column(name = "jra_inaktif_date")
    private Date jraInaktif;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "lokasi_fisik_berkas")
    private String lokasi;

    @Basic(optional = false)
    @NotNull
    @Column(name = "deskripsi_berkas")
    private String deskripsi;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic()
    @Column(name = "start_kurun_waktu")
    private Date startKurun;

    @Basic()
    @Column(name = "end_kurun_waktu")
    private Date endKurun;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_arsip_aktif")
    private Boolean isAktif =true;

    @JoinColumn(name = "id_penyusutan_akhir", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterStatusAkhirArsip statusAkhirArsip;

    @Basic()
    @Column(name = "closed_file_date")
    private Date closedDate;

    @Basic()
    @Column(name = "tgl_rencana_musnah")
    private Date rencanaMusnahDate;

    @Basic()
    @Column(name = "tgl_terakhir_akses")
    private Date terakhirAksesDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private String status;

    @Basic()
    @Column(name = "inaktif_date")
    private Date inaktifDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date")
    private Date createdDate;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "modified_by")
    private String modifiedBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_date")
    private Date modifiedDate;

    @Basic()
    @Column(name = "biaya", precision = 10, scale = 0)
    private BigDecimal biaya;

    @Basic()
    @Column(name = "progress", precision = 10, scale = 0)
    private BigDecimal progress;

    @JoinColumn(name = "id_folder", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterFolderKegiatan folderKegiatan;

    @Basic()
    @Column(name = "folder_name")
    private String folderName;

    @JoinColumn(name = "id_ruang_arsip", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterRuangArsip ruangArsip;

    @JoinColumn(name = "id_lemari_arsip", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterLemariArsip lemariArsip;

    @JoinColumn(name = "id_trap_arsip", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterTrapArsip trapArsip;

    @JoinColumn(name = "id_boks_arsip", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterBoksArsip boksArsip;

    @OneToMany(mappedBy = "berkas", cascade = CascadeType.REMOVE)
    private List<Arsip> arsipList;

    public Berkas() {
        arsipList = new ArrayList<>();
    }
}
