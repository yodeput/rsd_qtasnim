package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_user")
@XmlRootElement
public class MasterUser implements Serializable, IAuditTrail {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @Column(name="user_uuid")
    private UUID clientId = UUID.randomUUID();

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 15)
    @Column(name="user_login_username")
    private String loginUserName;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name="password_hash")
    private String passwordHash;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name="password_salt")
    private String passwordSalt;

    @Basic()
    @Size(max = 30)
    @Column(name="user_employee_id")
    private String employeeId;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 20)
    @Column(name="user_name_front")
    private String nameFront;

    @Basic()
    @Column(name="user_name_middle_last")
    private String nameMiddleLast;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 10)
    @Column(name="user_salutation")
    private String salutation;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 50)
    @Column(name="user_email")
    private String email;

    @Basic()
    @Size(max = 100)
    @Column(name="user_other_contact_information")
    private String otherContactInformation;

    @Basic()
    @Size(max = 50)
    @Column(name="mobile_phone",length = 50)
    private String mobilePhone;

    @Basic()
    @Size(max = 1000)
    @Column(name="user_description")
    private String description;

    @Basic()
    @Size(max = 30)
    @Column(name="user_status")
    private String status;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_deleted")
    private Boolean isDeleted;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_active")
    private Boolean isActive;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="modified_date")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_internal")
    private Boolean isInternal = false;

    @JoinColumn(name = "m_struktur_organisasi_id_organisasi", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi organizationEntity;

    @JoinColumn(name = "id_vendor", referencedColumnName = "id_vendor")
    @ManyToOne(fetch = FetchType.LAZY)
    private MasterVendor vendor;

    @Basic()
    @Size(max = 255)
    @Column(name="business_area")
    private String businessArea;

    @Basic()
    @Size(max = 255)
    @Column(name="jabatan")
    private String jabatan;

    @Basic()
    @Size(max = 255)
    @Column(name="kedudukan")
    private String kedudukan;

    @JoinColumn(name = "personal_area", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterArea area;

    @Lob()
    @Column(name="photo", columnDefinition="LONGBLOB")
    private byte[] photo;

    @JoinColumn(name = "file_repository_id", referencedColumnName = "id")
    @ManyToOne
    private ImageRepository image;

    @Version
    @Column(name="version", nullable = false)
    @NotNull
    private long version = 0L;

    @Basic()
    @Size(max = 1000)
    @Column(name="key_pattern")
    private String keyPattern;

    @Basic()
    @Size(max = 1000)
    @Column(name="finger_print")
    private String fingerprint;

    @Basic()
    @Column(name="tempat_lahir")
    private String tempatLahir;

    @Basic(optional = false)
    @Column(name="tanggal_lahir")
    private Date tanggalLahir;

    @Basic()
    @Column(name="agama")
    private String agama;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="jenis_kelamin")
    private String kelamin;

    @JoinColumn(name = "korsa", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterKorsa korsa;

    @JoinColumn(name = "grade", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterGrade grade;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private CompanyCode companyCode;

    @Transient
    public String getFormattedName() {
        return String.format("%s %s", Optional.ofNullable(nameFront).orElse(""), Optional.ofNullable(nameMiddleLast).orElse("")).trim();
    }
}
