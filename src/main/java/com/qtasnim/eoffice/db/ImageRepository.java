package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "image_repository")
@XmlRootElement
public class ImageRepository implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name="file_name")
    private String fileName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name="generated_file_name")
    private String generatedFileName;

    @Basic(optional = false)
    @NotNull
    @Column(name="image_original",columnDefinition="LONGBLOB")
    @Lob
    private byte[] imageOriginal;

    @Basic(optional = false)
    @NotNull
    @Column(name="image_large",columnDefinition="LONGBLOB")
    @Lob
    private byte[] imageLarge;

    @Basic(optional = false)
    @NotNull
    @Column(name="image_medium",columnDefinition="LONGBLOB")
    @Lob
    private byte[] imageMedium;

    @Basic(optional = false)
    @NotNull
    @Column(name="image_small",columnDefinition="LONGBLOB")
    @Lob
    private byte[] imageSmall;

    @JoinColumn(name = "m_mime_mapping_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private MasterMimeMapping extension;

    @JoinColumn(name = "m_user_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private MasterUser ownerUser;

    @JoinColumn(name = "m_group_id", referencedColumnName = "id")
    @ManyToOne
    private MasterGroup ownerGroup;

    @JoinColumn(name = "m_struktur_organisasi_id_organisasi", referencedColumnName = "id_organization")
    @ManyToOne
    @NotNull
    private MasterStrukturOrganisasi ownerOrganization;
}
