package com.qtasnim.eoffice.db;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "v_surat_masuk_need_att")
@Getter
@Setter
public class ViewSuratMasukNeedAtt {
    @Id
    @Column(name="id_surat")
    private Long idSurat;

    @Column(name="pemerima_assignee")
    private Long pemerimaAssignee;

    @Column(name="disposisi_assignee")
    private Long disposisiAssignee;

    @Column(name="task_assignee")
    private Long taskAssignee;
}
