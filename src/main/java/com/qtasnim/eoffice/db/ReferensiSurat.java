package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_referensi_surat")
@XmlRootElement
public class ReferensiSurat implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_surat", referencedColumnName = "id", nullable = true)
    @OneToOne
    private Surat surat;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne
    private MasterStrukturOrganisasi organisasi;

    @OneToMany(mappedBy = "referensiSurat")
    private List<ReferensiSuratDetail> suratDetail;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser user;

    public ReferensiSurat() {}
}
