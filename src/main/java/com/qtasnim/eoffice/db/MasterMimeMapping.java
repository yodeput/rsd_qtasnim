package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_mime_mapping")
@XmlRootElement
public class MasterMimeMapping {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 10)
    @Column(name="file_extension")
    private String extension;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 200)
    @Column(name="file_mime")
    private String mime;

    @Version
    @Column(name="version", nullable = false)
    @NotNull
    private long version = 0L;
}
