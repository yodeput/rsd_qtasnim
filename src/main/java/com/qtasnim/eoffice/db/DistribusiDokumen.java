package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import com.qtasnim.eoffice.services.DistribusiDokumenService;
import com.qtasnim.eoffice.services.NgSuratService;
import com.qtasnim.eoffice.util.BeanUtil;
import com.qtasnim.eoffice.workflows.IWorkflowService;
import com.qtasnim.eoffice.workflows.WorkflowException;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_distribusi_dokumen")
@XmlRootElement
public class DistribusiDokumen implements Serializable, IAuditTrail, IWorkflowEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @NotNull
    @Column(name = "perihal", columnDefinition = "text")
    private String perihal;

    @Basic()
    @Column(name = "jenis_distribusi", columnDefinition = "ENUM")
    private String jenis;

    @Basic()
    @Size(max = 255)
    @Column(name = "password_hash", length = 255)
    private String passwordHash;

    @Basic()
    @Size(max = 255)
    @Column(name = "password_salt", length = 255)
    private String passwordSalt;

    @Basic()
    @Column(name = "approved_date", columnDefinition = "TIMESTAMP")
    private Date approvedDate;

    @Basic()
    @Column(name="status", columnDefinition = "ENUM")
    private String status;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @JoinColumn(name = "konseptor", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi konseptor;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by", length = 255)
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date", columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "modified_by", length = 255)
    private String modifiedBy;

    @Basic()
    @Column(name = "modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "distribusi")
    @JsonIgnore
    private List<DistribusiDokumenDetail> detail;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "distribusi")
    @JsonIgnore
    private List<PemeriksaDistribusi> pemeriksa;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "distribusi")
    @JsonIgnore
    private List<PenandatanganDistribusi> penandatangan;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "distribusi")
    @JsonIgnore
    private List<PenerimaDistribusi> penerima;

    public DistribusiDokumen() {
        detail = new ArrayList<>();
        pemeriksa = new ArrayList<>();
        penandatangan = new ArrayList<>();
        penerima = new ArrayList<>();
    }

    @Transient
    @Override
    public String getRelatedId() throws WorkflowException {
        return Optional.ofNullable(id).map(Object::toString).orElseThrow(() -> new WorkflowException("Invalid workflow entity, no related id supplied.", "QT-ERR-WF-03"));
    }

    @Transient
    @Override
    public String getModuleName() {
        return "Distribusi Dokumen";
    }

    @Override
    public String getNoDokumen() {
        return null;
    }

    @Transient
    @Override
    public String getPageLink() {
        return "";
    }

    @Transient
    @Override
    public String getClassName() {
        return Optional.of(getClass().getName()).filter(t -> t.contains("_")).map(t -> Stream.of(t.split("_")).findFirst().orElse(getClass().getName())).orElse(getClass().getName());
    }

    @Override
    public List<ProcessInstance> getProcessInstances() {
        return null;
    }

    @Override
    public void setProcessInstances(List<ProcessInstance> processInstances) {

    }

    @Override
    public List<FormValue> getFormValue() {
        return null;
    }

    @Override
    public IWorkflowService getWorkflowService() {
        return (IWorkflowService) BeanUtil.getBean(DistribusiDokumenService.class);
    }

    @Transient
    @Override
    public EntityStatus getCompletionStatus() {
        return EntityStatus.valueOf(status);
    }

    @Transient
    @Override
    public void setCompletionStatus(EntityStatus completionStatus) {
        status = completionStatus.toString();
    }

    @Override
    public FormDefinition getFormDefinition() {
        return null;
    }

    @Transient
    private String jenisDokumen;

    @Transient
    private String taskExecutor;

    @Transient
    private String taskResponse;

    @Transient
    private String nomor;

    @Transient
    private String formattedTanggal;

    @Transient
    private String formattedSender;

    @Transient
    private String klasifikasiKeamanan;
}
