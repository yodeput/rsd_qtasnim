package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_company_code")
@XmlRootElement
public class CompanyCode implements Serializable, IAuditTrail {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="company_code")
    private String code;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="company_name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="application_url")
    private String url;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="address")
    private String address;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="email")
    private String email;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="description")
    private String description;

    @Lob()
    @Column(name="bg_image", columnDefinition="LONGBLOB")
    private byte[] bg;

    @Lob()
    @Column(name="logo_image", columnDefinition="LONGBLOB")
    private byte[] logo;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_active")
    private Boolean isActive = true;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    public CompanyCode() {

    }

    public CompanyCode(Long id) {
        this.id = id;
    }
}
