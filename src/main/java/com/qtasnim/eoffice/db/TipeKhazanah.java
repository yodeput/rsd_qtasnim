package com.qtasnim.eoffice.db;

public enum TipeKhazanah {
    BUKU("BUKU"), VIDEO("VIDEO"), FOTO("FOTO"), PRESENTASI("PRESENTASI");

    String toString;

    private TipeKhazanah(String toString) {
        this.toString = toString;
    }

    private TipeKhazanah() {
    }

    public String toString() {
        return this.toString != null?this.toString:super.toString();
    }
}
