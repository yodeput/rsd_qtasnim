package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_card")
@XmlRootElement
public class Card implements Serializable, IAuditTrail{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name="kode")
    private String kode;

    @Basic()
    @Column(name="title")
    private String title;

    @Basic()
    @Column(name="description",columnDefinition = "TEXT")
    private String description;

    @JoinColumn(name = "bucket_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Bucket bucket;

    @Basic()
    @Column(name="progress", columnDefinition = "ENUM")
    private String progress;

    @JoinColumn(name = "priority_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterPrioritas prioritas;

    @Basic()
    @Column(name="is_deleted")
    private Boolean isDeleted;

    @JoinColumn(name = "card_owner", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterStrukturOrganisasi cardOwner;

    @Basic()
    @Column(name="start_date")
    private Date start;

    @Basic()
    @Column(name="end_date")
    private Date end;

    @JoinColumn(name = "company_id", referencedColumnName = "id",nullable=false)
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;

    @OneToMany(
            mappedBy = "card",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CardMember> memberList;

    @OneToMany(
            mappedBy = "card",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CardChecklist> checkList;

    public Card (){
        checkList = new ArrayList<>();
        memberList = new ArrayList<>();
    }
}
