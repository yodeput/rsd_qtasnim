package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_khazanah")
@XmlRootElement
public class Khazanah implements Serializable, IAuditTrail{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
//    @Enumerated(EnumType.STRING)
    @Column(name="tipe_khazanah",columnDefinition = "ENUM")
    private String tipeKhazanah;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name="judul")
    private String judul;

    @Basic()
    @Column(name="deskripsi",columnDefinition = "TEXT")
    private String deskripsi;

    @JoinColumn(name = "id_bahasa", referencedColumnName = "id_bahasa")
    @OneToOne()
    private MasterBahasa bahasa;

    @JoinColumn(name = "id_negara", referencedColumnName = "id")
    @OneToOne()
    private MasterNegara negara;

    @JoinColumn(name = "id_klasifikasi_khazanah", referencedColumnName = "id")
    @OneToOne()
    private MasterKlasifikasiKhazanah klasifikasiKhazanah;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name="penerbit")
    private String penerbit;

    @Basic()
    @Column(name="tahun_terbit",columnDefinition = "YEAR")
    private Short tahunTerbit;

    @Basic()
    @Size(max = 255)
    @Column(name="penulis")
    private String penulis;

    @Basic()
    @Column(name = "jml_halaman",columnDefinition = "TINYINT")
    private Integer jmlHalaman;

    @Basic()
//    @Enumerated(EnumType.STRING)
    @Column(name="jenis_buku",columnDefinition = "ENUM")
    private String jenisBuku;

    @Basic()
    @Size(max = 255)
    @Column(name="lokasi_simpan")
    private String lokasiSimpan;

    @Basic()
    @Size(max = 255)
    @Column(name="penulis_skenario")
    private String penulisSkenario;

    @Basic()
    @Column(name="durasi",columnDefinition = "TIME")
    private Date durasi;

    @Basic()
//    @Enumerated(EnumType.STRING)
    @Column(name="media",columnDefinition = "ENUM")
    private String media;

    @Basic()
    @Size(max = 255)
    @Column(name="draft")
    private String draft;

    @Basic()
    @Size(max = 255)
    @Column(name="status")
    private String status;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;
}
