package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "form_field")
@XmlRootElement
public class FormField implements Serializable, IAuditTrail {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "form_definition_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    @JsonbTransient // avoid stackoverflow error
    private FormDefinition definition;

    @JoinColumn(name = "metadata_id", referencedColumnName = "id_metadata", nullable = false)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @NotNull
    @JsonIgnore
    private MasterMetadata metadata;

    @Basic()
    // @NotNull
    @Column(name="field_data",columnDefinition = "TEXT")
    private String data;

    @Basic()
    // @NotNull
    @Column(name="field_default_value")
    private String defaultValue;

    @Basic(optional = false)
    @NotNull
    @Column(name="field_type")
    private String type;

    @Basic()
    // @NotNull
    @Column(name="placeholder")
    private String placeholder;

    @Basic()
    // @NotNull
    @Column(name="table_source_name")
    private String tableSourceName;

    @Basic()
    // @NotNull
    @Column(name="selected_source_field")
    private String selectedSourceField;

    @Basic()
    // @NotNull
    @Column(name="selected_source_value")
    private String selectedSourceValue;

    @Basic(optional = false)
    @NotNull
    @Column(name="seq", columnDefinition = "TINYINT", length = 1)
    private Integer seq;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_required")
    private Boolean isRequired = true;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_active")
    private Boolean isActive = true;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_disabled")
    private Boolean isDisabled = false;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="modified_date")
    private Date modifiedDate;
}
