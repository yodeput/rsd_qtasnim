package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.DocListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(DocListener.class)
@Table(name = "t_penerima_para")
@XmlRootElement
public class PenerimaPara implements Serializable, IDocTrail {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_penerima", referencedColumnName = "id")
    @OneToOne
    @NotNull
    private PenerimaSurat penerimaSurat;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;


    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne
    @NotNull
    private MasterStrukturOrganisasi organisasi;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne
    private MasterUser user;
}
