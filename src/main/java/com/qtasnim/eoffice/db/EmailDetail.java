package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@Table(name = "t_email_detail")
@XmlRootElement
public class EmailDetail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "email_header_id", referencedColumnName = "id")
    @OneToOne
    @NotNull
    private EmailHeader header;

    @Column(name = "body",columnDefinition = "TEXT")
    @Basic(optional = false)
    @NotNull
    private String body;
}
