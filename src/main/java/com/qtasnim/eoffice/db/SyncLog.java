package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.db.converters.SyncLogStatusConverter;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_sync_log")
@XmlRootElement
public class SyncLog implements Serializable, ICreationAudit {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name="duration")
    private Long duration;

    @Basic(optional = false)
    @NotNull
    @Column(name="type")
    private String type;

    @Basic(optional = false)
    @NotNull
    @Column(name="status")
    @Convert(converter = SyncLogStatusConverter.class)
    private SyncLogStatus status;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Column(name="completed_date")
    private Date completedDate;

    @OneToMany(mappedBy = "syncLog")
    private List<SyncLogDetail> syncLogDetails;
}
