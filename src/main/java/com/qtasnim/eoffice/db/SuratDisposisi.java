package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import com.qtasnim.eoffice.util.FieldMetadata;
import com.qtasnim.eoffice.util.MetadataValueType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_surat_disposisi")
@XmlRootElement
public class SuratDisposisi implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_parent", referencedColumnName = "id", nullable = true)
    @ManyToOne
    private SuratDisposisi parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private List<SuratDisposisi> disposisi;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "suratDisposisi")
    @JsonIgnore
    @FieldMetadata(nama="Tindakan", type= MetadataValueType.MULTI_VALUE, format = "${tindakan.id} | ${tindakan.nama}")
    private List<TindakanDisposisi> tindakanDisposisi;

    @JoinColumn(name = "id_surat", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    private Surat surat;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterStrukturOrganisasi organization;

    @Basic()
    @Column(name="is_secretary_executed")
    private Boolean isSecretaryExecuted = false;

    @Basic(optional = true)
    @Column(name = "comment", columnDefinition = "TEXT")
    private String comment;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_read")
    private Boolean isRead = false;

    @Basic(optional = false)
    @Column(name = "assigned_date", nullable = true)
    private Date assignedDate;

    @Basic(optional = true)
    @Column(name = "executed_date", nullable = true)
    private Date executedDate;

    @Basic(optional = true)
    @Column(name = "target_date", nullable = true)
    private Date targetDate;

    @Basic(optional = true)
    @Column(name = "execution_status", nullable = true)
    private String status;

    @Basic()
    @Column(name = "is_hadir")
    private Boolean isHadir;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser user;

    public SuratDisposisi() {
        tindakanDisposisi = new ArrayList<>();
        disposisi = new ArrayList<>();
    }
}
