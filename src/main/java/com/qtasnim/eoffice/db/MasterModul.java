package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_modul")
@XmlRootElement

public class MasterModul implements IAuditTrail, Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id_modul")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idModul;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name="nama_modul")
    private String namaModul;

    @JoinColumn(name = "id_parent", referencedColumnName = "id_modul")
    @OneToOne
    private MasterModul parent;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name="translate")
    private String translate;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 20)
    @Column(name="tipe")
    private String tipe;

    @Basic()
    @Column(name="url")
    private String url;

    @Basic()
    @Column(name="description",columnDefinition = "text")
    private String description;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_active")
    private Boolean isActive = false;

    @Basic()
    @Column(name="key_modul")
    private String keyModul;

    @Basic()
    @Column(name="icon_class")
    private String iconClass;

    @Basic()
    @Column(name = "seq",columnDefinition = "TINYINT")
    private Integer seq;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date",columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "modules")
    @JsonIgnore
    private List<RoleModule> roleModuleList;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;
}
