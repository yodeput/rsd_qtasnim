package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_auto_number")
@XmlRootElement
public class MasterAutoNumber implements IAuditTrail {
    @Id
    @Column(name="auto_number_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="title")
    private String title;

    @Basic(optional = false)
    @NotNull
    @Column(name="value")
    private Integer value;

    // redy to delete
    @Basic(optional = false)
    @NotNull
    @Column(name = "format", nullable = false, columnDefinition = "TEXT")
    private String format;
    // redy to delete

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="reset_period", length = 5)
    private String resetPeriod;

    @Basic(optional = false)
    @NotNull
    @Column(name="next_reset")
    private Date nextReset;

    // redy to delete
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic(optional = false)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic(optional = false)
    @Column(name="modified_date")
    private Date modifiedDate;
    // redy to delete
}
