package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "process_definition")
@XmlRootElement
public class ProcessDefinition implements Serializable, ICreationAudit {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name="definition_id")
    private String definitionId;

    @Basic()
    @Column(name="deployment_id")
    private String deploymentId;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name="definition_name")
    private String definitionName;

    @Basic()
    @Column(name="definition_type")
    private String definitionType;

    @Basic(optional = false)
    @NotNull
    @Column(name="form_name")
    private String formName;

    @Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name="definition")
    @NotNull
    @Size(min = 1)
    private String definition;

    @Basic(optional = false)
    @NotNull
    @Column(name="definition_version")
    private Double version;

    @Basic()
    @Column(name="activation_date")
    private Date activationDate;

    @JoinColumn(name = "m_workflow_provider_id", referencedColumnName = "id", nullable = false)
    @ManyToOne
    @NotNull
    private MasterWorkflowProvider workflowProvider;

    @OneToMany(mappedBy = "processDefinition", cascade = CascadeType.REMOVE)
    private List<ProcessDefinitionVar> vars;

    @OneToMany(mappedBy = "processDefinition", cascade = CascadeType.REMOVE)
    private List<ProcessDefinitionFormVarResolver> formVars;

    @OneToMany(mappedBy = "processDefinition", cascade = CascadeType.REMOVE)
    private List<ProcessInstance> instances;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @JoinColumn(name = "id_jenis_dokumen", referencedColumnName = "id_jenis_dokumen")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterJenisDokumen jenisDokumen;

    public ProcessDefinition() {
        vars = new ArrayList<>();
        formVars = new ArrayList<>();
    }
}
