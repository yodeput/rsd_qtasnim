package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.listeners.EntityListener;
import com.qtasnim.eoffice.services.IDigiSignService;
import com.qtasnim.eoffice.util.BeanUtil;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_digisign")
@XmlRootElement
public class MasterDigisign {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name="provider_name")
    private String name;

    @OneToMany(mappedBy = "provider")
    private List<MasterDigisignConfig> config;

    public IDigiSignService getWorkflowProvider() throws InternalServerErrorException {
        try {
            return (IDigiSignService) BeanUtil.getBean(Class.forName(name));
        } catch (ClassNotFoundException ex) {
            throw new InternalServerErrorException("Invalid Digisign class", ex);
        }
    }
}
