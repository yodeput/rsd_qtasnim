package com.qtasnim.eoffice.db;

public enum EntityStatus {
    DRAFT("DRAFT"), SUBMITTED("SUBMITTED"), APPROVED("APPROVED"), REJECTED("REJECTED"), CLOSED("CLOSED"), CANCEL("CANCEL");

    String toString;

    private EntityStatus(String toString) {
        this.toString = toString;
    }

    private EntityStatus() {
    }

    public String toString() {
        return this.toString != null?this.toString:super.toString();
    }
}
