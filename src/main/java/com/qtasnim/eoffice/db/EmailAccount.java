package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_email_account")
@XmlRootElement
public class EmailAccount implements IAuditTrail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "email_user_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private EmailUser emailUser;

    @Column(name = "server")
    @Basic(optional = false)
    @NotNull
    private String server;

    @Column(name = "port")
    @Basic(optional = false)
    @NotNull
    private Integer port;

    @Column(name = "protocol")
    @Basic(optional = false)
    @NotNull
    private String protocol;

    @Column(name = "username")
    @Basic()
    private String username;

    @Column(name = "password")
    @Basic()
    private String password;

    @Column(name = "use_ssl")
    @Basic(optional = false)
    @NotNull
    private Boolean useSsl;

    @Column(name = "use_proxy")
    @Basic(optional = false)
    @NotNull
    private Boolean useProxy;

    @Column(name = "proxy_url")
    @Basic()
    private String proxyUrl;

    @Column(name = "proxy_username")
    @Basic()
    private String proxyUsername;

    @Column(name = "proxy_password")
    @Basic()
    private String proxyPassword;

    @Column(name = "syncing")
    @Basic(optional = false)
    @NotNull
    private Boolean syncing;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date",columnDefinition = "TIMESTAMP")
    private Date modifiedDate;
}
