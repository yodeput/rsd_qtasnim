package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_penandatangan_bon_nomor")
@XmlRootElement
public class PenandatanganBonNomor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_permohonan", referencedColumnName = "id", nullable = false)
    @OneToOne
    private PermohonanBonNomor permohonan;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne
    private MasterStrukturOrganisasi organisasi;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne
    private MasterUser user;

    public PenandatanganBonNomor() {}
}
