package com.qtasnim.eoffice.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.qtasnim.eoffice.listeners.EntityListener;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "user_session")
@XmlRootElement
public class UserSession implements Serializable {
    private static final long serialVersionUID = 346852118121647662L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Basic(optional = false)
    @NotNull
    @Column(name="user_token", columnDefinition = "text")
    private String token;

    @Basic()
    @Column(name="refresh_token", columnDefinition = "text")
    private String refreshToken;

    @Basic(optional = false)
    @NotNull
    @Column(name="token_valid_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tokenValidFrom;

    @Basic(optional = false)
    @NotNull
    @Column(name="token_valid_until")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tokenValidUntil;

    @Basic()
    @Column(name="rt_valid_until")
    @Temporal(TemporalType.TIMESTAMP)
    private Date refreshTokenValidUntil;

    @Basic()
    @Size(max = 100)
    @Column(name="user_ip_address")
    private String ipAddress;

    @Basic()
    @Size(max = 1000)
    @Column(name="user_client")
    private String client;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private MasterUser user;
}