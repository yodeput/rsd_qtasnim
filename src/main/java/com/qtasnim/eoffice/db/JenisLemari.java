package com.qtasnim.eoffice.db;

public enum JenisLemari {
    LEMARI("Lemari"), RAK("Rak"),
    FILLINGCABINET("Filling Cabinet"), ROLLOPACK("Roll O Pack");

    String toString;

    private JenisLemari(String toString) {
        this.toString = toString;
    }

    private JenisLemari() {
    }

    public String toString() {
        return this.toString != null?this.toString:super.toString();
    }
}
