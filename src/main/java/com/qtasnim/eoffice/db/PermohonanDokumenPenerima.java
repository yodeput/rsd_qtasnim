package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_permohonan_dokumen_penerima")
@XmlRootElement
public class PermohonanDokumenPenerima implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_permohonan_dokumen", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private PermohonanDokumen permohonanDokumen;

    @Basic()
    @Column(name = "seq", columnDefinition = "TINYINT")
    private Integer seq;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi organization;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser userApprover;
}
