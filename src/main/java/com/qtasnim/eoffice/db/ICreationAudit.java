package com.qtasnim.eoffice.db;

import java.util.Date;

public interface ICreationAudit {
    public String getCreatedBy();
    public void setCreatedBy(String createdBy);
    public Date getCreatedDate();
    public void setCreatedDate(Date createdDate);
}
