package com.qtasnim.eoffice.db;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@Embeddable
public class ReferensiSuratPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_surat")
    private Long surat;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_dokumen_eksternal")
    private Long dokumenEksternal;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_organization")
    private Long organization;

    public ReferensiSuratPK() {
    }

    public ReferensiSuratPK(Long surat, Long dokumenEksternal, Long organization) {
        this.surat = surat;
        this.dokumenEksternal = dokumenEksternal;
        this.organization = organization;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int)(long)surat;
        hash += (int)(long)dokumenEksternal;
        hash += (int)(long)organization;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ReferensiSuratPK)) {
            return false;
        }

        ReferensiSuratPK other = (ReferensiSuratPK) object;

        if (this.surat != other.surat) {
            return false;
        }

        if (this.dokumenEksternal != other.dokumenEksternal) {
            return false;
        }

        if (this.organization != other.organization) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "com.qtasnim.eoffice.db.ReferensiSuratPK[ surat=" + surat + ", dokumenEksternal=" + dokumenEksternal + ", organization=" + organization + "]";
    }
}
