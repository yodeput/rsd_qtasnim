package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import com.qtasnim.eoffice.ws.dto.MasterRuangArsipDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_lemari_arsip")
@XmlRootElement
public class MasterLemariArsip implements Serializable, IAuditTrail {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_ruang_arsip", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterRuangArsip ruangArsip;

    @Size(min = 1, max = 255)
    @Column(name="kode")
    private String kode;

    @Basic()
    @Column(name="jenis_lemari", columnDefinition = "ENUM")
    private String jenisLemari;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;
}
