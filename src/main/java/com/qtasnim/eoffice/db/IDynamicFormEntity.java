package com.qtasnim.eoffice.db;

public interface IDynamicFormEntity {
    FormDefinition getFormDefinition();

    java.util.List<FormValue> getFormValue();

    void setFormDefinition(FormDefinition formDefinition);

    void setFormValue(java.util.List<FormValue> formValue);
}
