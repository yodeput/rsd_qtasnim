package com.qtasnim.eoffice.db;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "v_surat_setuju_need_att")
@Getter
@Setter
public class ViewSuratSetujuNeedAtt {
    @Id
    @Column(name="id_task")
    private Long idTask;

    @Column(name="id_surat")
    private Long idSurat;

    @Column(name="id_assignee")
    private Long idAssignee;
}
