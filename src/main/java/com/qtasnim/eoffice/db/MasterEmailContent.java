package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_email_content")
@XmlRootElement
public class MasterEmailContent {
    @Id
    @Column(name="email_content_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 50)
    @Column(name="email_type", length = 50)
    private String type;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="email_body", columnDefinition = "text")
    private String body;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="subject", columnDefinition = "text")
    private String subject;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic
    @Column(name="modified_date")
    private Date modifiedDate;
}
