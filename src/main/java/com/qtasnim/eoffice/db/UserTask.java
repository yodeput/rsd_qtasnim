package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.db.converters.ExecutionStatusConverter;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_user_task")
@XmlRootElement
public class UserTask implements Serializable, IAuditTrail {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @NotNull
    @Column(name="task_id")
    private String taskId; // camunda task id

    @Basic()
    @NotNull
    @Column(name="task_name")
    private String taskName; // camunda task name

    @Basic(optional = false)
    @NotNull
    @Column(name="execution_status")
    @Convert(converter = ExecutionStatusConverter.class)
    private ExecutionStatus executionStatus = ExecutionStatus.PENDING;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_task_date")
    private Date createdTaskDate;

    @Basic()
    @Column(name="approved_date")
    private Date approvedDate;

    @Basic()
    @NotNull
    @Column(name="related_entity")
    private String relatedEntity; // nama kelas entity

    @Basic()
    @NotNull
    @Column(name="record_ref_id")
    private String recordRefId; // id relatedEntity

    @Basic()
    @Column(name="nomor_dokumen")
    private String nomorDokumen;

    @Basic()
    @NotNull
    @Column(name="perihal")
    private String perihal;

    @Basic()
    @Column(name="id_jenis_dokumen")
    private Long idJenisDokumen;

    @Basic()
    @Column(name="jenis_dokumen")
    private String jenisDokumen;

    @Basic()
    @Column(name="klasifikasi_keamanan")
    private String klasifikasiKeamanan;

    @Basic()
    @Column(name="status")
    private String status;

    @Basic()
    @Column(name="note")
    private String note;

    @JoinColumn(name = "id_konseptor", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi konseptor;

    @JoinColumn(name = "id_pengirim", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi pengirim;

    @JoinColumn(name = "id_assignee", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi assignee;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private CompanyCode companyCode;

    @Basic()
    @ColumnDefault("false")
    @Column(name="is_paralel_assignee")
    private Boolean isParalelAssignee = false;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;
}
