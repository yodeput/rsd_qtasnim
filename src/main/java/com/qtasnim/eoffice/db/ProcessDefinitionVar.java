package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "process_definition_var")
@XmlRootElement
public class ProcessDefinitionVar {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @Column(name = "var_name")
    @NotEmpty
    @NotNull
    @Size(min= 1, max = 100)
    private String name;

    @Basic()
    @Column(name = "var_value")
    private String value;

    @Basic(optional = false)
    @Column(name = "var_type")
    @NotEmpty
    @NotNull
    @Size(max = 100)
    private String type;

    @JoinColumn(name = "process_definition_id", referencedColumnName = "id", nullable = false)
    @ManyToOne
    @NotNull
    private ProcessDefinition processDefinition;

//    @JoinColumn(name = "resolver_id", referencedColumnName = "id", nullable = false)
//    @OneToOne
//    @NotNull
//    private ProcessDefinitionFormVarResolver resolver;
}
