package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.listeners.EntityListener;
import com.qtasnim.eoffice.util.BeanUtil;
import com.qtasnim.eoffice.workflows.IRdsWorkflowProvider;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_workflow_provider")
@XmlRootElement
public class MasterWorkflowProvider {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name="provider_name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1)
    @Column(name="class_name")
    private String className;

    @OneToMany(mappedBy = "provider")
    private List<MasterWorkflowProviderConfig> config;

    @Version
    @Column(name="version", nullable = false)
    @NotNull
    private long version = 0L;

    public MasterWorkflowProvider() {
        config = new ArrayList<>();
    }

    @Transient
    public IWorkflowProvider getWorkflowProvider() throws InternalServerErrorException {
        try {
            return (IWorkflowProvider) BeanUtil.getBean(Class.forName(className));
        } catch (ClassNotFoundException ex) {
            throw new InternalServerErrorException("Invalid Workflow Definition class","QT-ERR-WF-02", ex);
        }
    }

    @Transient
    public IRdsWorkflowProvider getRdsWorkflowProvider() throws InternalServerErrorException {
        try {
            return (IRdsWorkflowProvider) BeanUtil.getBean(Class.forName(className));
        } catch (ClassNotFoundException ex) {
            throw new InternalServerErrorException("Invalid Workflow Definition class","QT-ERR-WF-02", ex);
        }
    }
}
