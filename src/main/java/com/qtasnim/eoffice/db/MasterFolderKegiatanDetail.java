package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_folder_kegiatan_detail")
@XmlRootElement
public class MasterFolderKegiatanDetail implements Serializable,IAuditTrail {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name="folder_name")
    @Size(min = 1, max = 255)
    private String folderName;

    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    @OneToOne
    private MasterFolderKegiatanDetail parent;


    @JoinColumn(name = "personal_id", referencedColumnName = "id")
    @OneToOne
    private MasterFolderKegiatan personal;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @JoinColumn(name = "id_user", referencedColumnName = "id")
    @OneToOne()
    private MasterUser user;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date",columnDefinition = "TIMESTAMP")
    private Date modifiedDate;
}
