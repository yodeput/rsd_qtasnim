package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.DocListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(DocListener.class)
@Table(name = "m_jenis_dokumen_doc_lib")
@XmlRootElement
public class JenisDokumenDocLib implements Serializable,IDocTrail{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="doc_id")
    private String docId;

    @JoinColumn(name = "company_id", referencedColumnName = "id",nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @JoinColumn(name = "id_jenis_dokumen", referencedColumnName = "id_jenis_dokumen", nullable = false)
    @ManyToOne
    private MasterJenisDokumen jenisDokumen;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="doc_generated_name")
    private String docGeneratedName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="mime_type")
    private String mimeType;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="doc_name")
    private String docName;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name="no_id_doc")
    private String number;

    @Basic(optional = false)
    @NotNull
    @Column(name="doc_size")
    private Long size = 0L;

    @Basic(optional = false)
    @NotNull
    @Column(name="doc_version")
    private Double version;

    @Basic(optional = false)
    @NotNull
    @Column(name="cmis_version")
    private String cmisVersion;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date createdDate;

    @Basic(optional = true)
    @Column(name="modified_by", nullable = true)
    private String modifiedBy;

    @Basic(optional = true)
    @Column(name="modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_active")
    private Boolean isActive = true;

    @JoinColumn(name = "m_user_id", referencedColumnName = "id")
    @ManyToOne
    private MasterUser author;

    public JenisDokumenDocLib(){}
}
