package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_surat_form_value")
@XmlRootElement
public class SuratForm implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected SuratFormPK SuratFormPK;

    public SuratForm() {
    }

    public SuratForm(SuratFormPK suratFormPK) {
        this.SuratFormPK = suratFormPK;
    }

    public SuratForm(Long surat, Long formValue) {
        this.SuratFormPK = new SuratFormPK(surat, formValue);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (SuratFormPK != null ? SuratFormPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SuratForm)) {
            return false;
        }
        SuratForm other = (SuratForm) object;
        if ((this.SuratFormPK == null && other.SuratFormPK != null) || (this.SuratFormPK != null && !this.SuratFormPK.equals(other.SuratFormPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.qtasnim.eoffice.db.SuratForm[ masterSuratFormPK=" + SuratFormPK + " ]";
    }
}
