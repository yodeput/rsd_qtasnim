package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "sys_change_log")
@XmlRootElement
public class SysChangeLog implements Serializable, IAuditTrail {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @Column(name = "version")
    private String version;

    @Basic(optional = false)
    @NotNull
    @Column(name = "change_log_date")
    private Date changeLogDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_web")
    private Boolean isWeb = true;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name = "modified_date")
    private Date modifiedDate;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "changeLog")
    @JsonIgnore
    private List<SysChangeLogDetail> changeLogDetailList;

    public SysChangeLog() {
        changeLogDetailList = new ArrayList<>();
    }
}
