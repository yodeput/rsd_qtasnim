package com.qtasnim.eoffice.db;

public enum SyncLogStatus {
    RUNNING("RUNNING"), SUCCESSFUL("SUCCESSFUL"), ERROR("ERROR");

    String toString;

    private SyncLogStatus(String toString) {
        this.toString = toString;
    }

    private SyncLogStatus() {
    }

    public String toString() {
        return this.toString != null?this.toString:super.toString();
    }
}
