package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.helpers.TemplatingHelper;
import com.qtasnim.eoffice.listeners.DocListener;
import com.qtasnim.eoffice.services.NgSuratService;
import com.qtasnim.eoffice.util.BeanUtil;
import com.qtasnim.eoffice.util.FieldMetadata;
import com.qtasnim.eoffice.util.MetadataValueType;
import com.qtasnim.eoffice.workflows.IWorkflowService;
import com.qtasnim.eoffice.workflows.WorkflowException;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Getter
@Setter
@Entity
@EntityListeners(DocListener.class)
@Table(name = "t_surat")
@XmlRootElement
public class Surat implements Serializable, IDocTrail, IDynamicFormEntity, IWorkflowEntity {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "company_id", referencedColumnName = "id",nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private CompanyCode companyCode;

    @JoinColumn(name = "id_form_definition", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private FormDefinition formDefinition;

    @JoinColumn(name = "id_parent", referencedColumnName = "id", nullable = true)
    @OneToOne
    private Surat parent;

    @FieldMetadata(nama= TemplatingHelper.NO_DOKUMEN_KEY)
    @Basic()
    @Column(name="no_dokumen")
    private String noDokumen;

    @FieldMetadata(nama=TemplatingHelper.NO_AGENDA_KEY)
    @Basic()
    @Column(name="no_agenda")
    private String noAgenda;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 10)
    @Column(name="status")
    private String status;

    @JoinColumn(name = "konseptor", referencedColumnName = "id_organization", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi konseptor;

    @JoinColumn(name = "konseptor_user", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser userKonseptor;

    @Basic()
    @Column(name="submitted_date",columnDefinition = "TIMESTAMP")
    private Date submittedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_deleted")
    private Boolean isDeleted = false;

    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date createdDate;

    @Basic(optional = true)
    @Size(min = 1, max = 255)
    @Column(name="modified_by", nullable = true)
    private String modifiedBy;

    @Basic(optional = true)
    @Column(name="modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "t_surat_form_value",
            joinColumns = @JoinColumn(name = "id_surat"),
            inverseJoinColumns = @JoinColumn(name = "id_form_value")
    )
    @JsonIgnore
    private List<FormValue> formValue;

    @Basic()
    @Column(name="is_pemeriksa_pararel")
    private Boolean isPemeriksaParalel=false;

    @Basic(optional = false)
    @Column(name="is_need_publish_by_unit_dokumen")
    @NotNull
    private Boolean isNeedPublishByUnitDokumen = false;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    private List<ProcessInstance> processInstances;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    private List<SuratDisposisi> suratDisposisi;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    @FieldMetadata(nama=TemplatingHelper.PENANDATANGAN_KEY, type= MetadataValueType.MULTI_VALUE, format = "${organization.organizationName} | ${organization.user.nameFront} ${organization.user.nameMiddleLast} | ${organization.user.employeeId}")
    private List<PenandatanganSurat> penandatanganSurat;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    private List<PenandatanganPelakharPymtSurat> penandatanganPelakharPymtSurats;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    @FieldMetadata(nama=TemplatingHelper.PEMERIKSA_SURAT_KEY, type= MetadataValueType.MULTI_VALUE, format = "${organization.organizationName} | ${organization.user.nameFront} ${organization.user.nameMiddleLast} | ${organization.user.employeeId}")
    private List<PemeriksaSurat> pemeriksaSurat;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    private List<PemeriksaPelakharPymtSurat> pemeriksaPelakharPymtSurats;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    private List<PublisherSurat> publisherSurats;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    @FieldMetadata(nama=TemplatingHelper.PENERIMA_SURAT_KEY, type= MetadataValueType.MULTI_VALUE, format = "${organization.organizationName} | ${organization.user.nameFront} ${organization.user.nameMiddleLast} | ${organization.user.employeeId}")
    private List<PenerimaSurat> penerimaSurat;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    @FieldMetadata(nama=TemplatingHelper.TEMBUSAN_KEY, type= MetadataValueType.MULTI_VALUE, format = "${organization.organizationName} | ${organization.user.nameFront} ${organization.user.nameMiddleLast} | ${organization.user.employeeId}")
    private List<TembusanSurat> tembusanSurat;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    @FieldMetadata(nama=TemplatingHelper.MENGETAHUI_KEY, type= MetadataValueType.MULTI_VALUE, format = "${organization.organizationName} | ${organization.user.nameFront} ${organization.user.nameMiddleLast} | ${organization.user.employeeId}")
    private List<MengetahuiSurat> mengetahuiSurat;

    @Basic()
    @Column(name = "folder_arsip")
    private String folderArsip;

    @Basic()
    @Column(name= "id_arsip")
    private Long idArsip;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    private List<SuratInteraction> suratInteractions;

    @JoinColumn(name = "area", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterArea area;

    @JoinColumn(name = "unit", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterKorsa korsa;

    @Basic(optional = false)
    @NotNull
    @Column(name= "is_konsep_edited")
    private Boolean isKonsepEdited = false;

    @Basic()
    @Column(name="approved_date",columnDefinition = "TIMESTAMP")
    private Date approvedDate;

    @OneToMany(mappedBy = "surat", fetch = FetchType.LAZY)
    private List<SuratUndangan> undangans;

    @Basic()
    @Column(name = "status_navigasi")
    private String statusNavigasi = Constants.STATUS_NAVIGASI_SIMPAN_OLEH_KONSEPTOR;

    @Basic()
    @Column(name = "is_pinjam")
    private Boolean isPinjam = false;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "surat")
    @JsonIgnore
    private List<SuratCatatanSekretaris> catatanSekretarisList;

    @Basic(optional = false)
    @Column(name="is_multi_pemeriksa")
    private Boolean isMultiPemeriksa = true;

    @Basic(optional = false)
    @Column(name="is_pemeriksa_mandatory")
    private Boolean isPemeriksaMandatory = false;

    @Basic(optional = false)
    @Column(name="is_include_pejabat_mengetahui")
    private Boolean isIncludePejabatMengetahui = false;

    @Basic(optional = false)
    @Column(name="is_regulasi")
    @NotNull
    private Boolean isRegulasi = false;

    @Basic(optional = false)
    @NotNull
    @Column(name = "jumlah_penandatangan")
    private Integer jumlahPenandatangan = 1;

    @Basic()
    @Column(name="delegasi_type")
    private String delegasiType;

    @Basic()
    @Column(name = "bon_nomor")
    private Boolean bonNomor = false;

    public Surat(){
        formValue = new ArrayList<>();
        pemeriksaSurat = new ArrayList<>();
        penandatanganSurat = new ArrayList<>();
        tembusanSurat = new ArrayList<>();
        mengetahuiSurat = new ArrayList<>();
        penerimaSurat = new ArrayList<>();
        processInstances = new ArrayList<>();
        catatanSekretarisList = new ArrayList<>();
        undangans = new ArrayList<>();
        publisherSurats = new ArrayList<>();
    }

    @Transient
    @Override
    public String getRelatedId() throws WorkflowException {
        return Optional.ofNullable(id).map(Object::toString).orElseThrow(() -> new WorkflowException("Invalid workflow entity, no related id supplied.", "QT-ERR-WF-03"));
    }

    @Transient
    @Override
    public String getModuleName() {
        return "Surat";
    }

    @Transient
    @Override
    public String getPageLink() {
        return "/apps/dokumen";
    }

    @Transient
    @Override
    public String getClassName() {
        return Optional.of(getClass().getName()).filter(t -> t.contains("_")).map(t -> Stream.of(t.split("_")).findFirst().orElse(getClass().getName())).orElse(getClass().getName());
    }

    @Override
    public IWorkflowService getWorkflowService() {
        return (IWorkflowService) BeanUtil.getBean(NgSuratService.class);
    }

    @Transient
    @Override
    public EntityStatus getCompletionStatus() {
        return EntityStatus.valueOf(status);
    }

    @Transient
    @Override
    public void setCompletionStatus(EntityStatus completionStatus) {
        status = completionStatus.toString();
    }

    @Transient
    private String jenisDokumen;

    @Transient
    private String perihal;

    @Transient
    private String taskExecutor;

    @Transient
    private String taskResponse;

    @Transient
    private String nomor;

    @Transient
    private String formattedTanggal;

    @Transient
    private String formattedSender;

    @Transient
    private String klasifikasiKeamanan;

    @Transient
    private String klasifikasiMasalah;
}
