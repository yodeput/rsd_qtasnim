package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "process_instance")
@XmlRootElement
public class ProcessInstance {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="instance_id")
    private String instanceId;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="related_entity")
    private String relatedEntity;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="record_ref_id")
    private String recordRefId;

    @JoinColumn(name = "process_definition_id", referencedColumnName = "id", nullable = false)
    @ManyToOne
    @NotNull
    private ProcessDefinition processDefinition;

    @OneToMany(mappedBy = "processInstance", cascade = CascadeType.REMOVE)
    private List<ProcessTask> tasks;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "t_surat_process_instance",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_surat")
    )
    private Surat surat;

    public ProcessInstance() {
        tasks = new ArrayList<>();
    }
}
