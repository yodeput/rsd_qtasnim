package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_penerima_nomor_manual")
@XmlRootElement
public class PenerimaNomorManual implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name = "seq",columnDefinition = "TINYINT")
    private Integer seq;

    @Basic()
    @Column(name="delegasi_type")
    private String delegasiType;

    @JoinColumn(name = "id_permohonan", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private PenomoranManual permohonan;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi organization;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser user;

    @JoinColumn(name = "id_kontak_eksternal", referencedColumnName = "id_vendor")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterVendor vendor;

    public PenerimaNomorManual() {}
}
