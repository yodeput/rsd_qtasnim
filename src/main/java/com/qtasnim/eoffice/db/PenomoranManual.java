package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.helpers.TemplatingHelper;
import com.qtasnim.eoffice.listeners.DocListener;
import com.qtasnim.eoffice.services.DistribusiDokumenService;
import com.qtasnim.eoffice.services.PenomoranManualService;
import com.qtasnim.eoffice.util.BeanUtil;
import com.qtasnim.eoffice.util.FieldMetadata;
import com.qtasnim.eoffice.util.MetadataValueType;
import com.qtasnim.eoffice.workflows.IWorkflowService;
import com.qtasnim.eoffice.workflows.WorkflowException;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Getter
@Setter
@Entity
@EntityListeners(DocListener.class)
@Table(name = "t_penomoran_manual")
@XmlRootElement
public class PenomoranManual implements Serializable, IDocTrail , IWorkflowEntity{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @FieldMetadata(nama= TemplatingHelper.NO_DOKUMEN_KEY)
    @Basic()
    @Column(name="no_permohonan")
    private String noPermohonan;

    @FieldMetadata(nama= TemplatingHelper.NO_DOKUMEN_KEY)
    @Basic()
    @Column(name="no_dokumen")
    private String noDokumen;

    @Basic()
    @Column(name="tgl_dokumen",columnDefinition = "TIMESTAMP")
    private Date tglDokumen;

    @Basic()
    @Column(name="lampiran")
    private String lampiran;

    @Basic()
    @Column(name="perihal")
    private String perihal;

    @Basic()
    @Column(name="keterangan", columnDefinition="TEXT")
    private String keterangan;

    @Basic()
    @Column(name="tempat_diterima")
    private String tempatDiterima;

    @Basic()
    @Column(name="tgl_diterima",columnDefinition = "TIMESTAMP")
    private Date tglDiterima;

    @Basic()
    @Column(name="status")
    private String status;

    @JoinColumn(name = "id_jenis_dokumen", referencedColumnName = "id_jenis_dokumen")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterJenisDokumen jenisDok;

    @JoinColumn(name = "id_klasifikasi_dokumen", referencedColumnName = "id_klasifikasi_masalah")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterKlasifikasiMasalah klasDokumen;

    @JoinColumn(name = "id_klasifikasi_keamanan", referencedColumnName = "id_klasifikasi_keamanan")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterKlasifikasiKeamanan klasKeamanan;

    @JoinColumn(name = "id_organization_konseptor", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi konseptor;

    @JoinColumn(name = "id_company", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private CompanyCode companyCode;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_deleted")
    private Boolean isDeleted = false;

    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date createdDate;

    @Basic(optional = true)
    @Size(min = 1, max = 255)
    @Column(name="modified_by", nullable = true)
    private String modifiedBy;

    @Basic(optional = true)
    @Column(name="modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "permohonan")
    @JsonIgnore
    @FieldMetadata(nama=TemplatingHelper.PENANDATANGAN_KEY, type= MetadataValueType.MULTI_VALUE)
    private List<PenandatanganNomorManual> penandatangan;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "permohonan")
    @JsonIgnore
    @FieldMetadata(nama=TemplatingHelper.PEMERIKSA_SURAT_KEY, type= MetadataValueType.MULTI_VALUE)
    private List<PemeriksaNomorManual> pemeriksa;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "permohonan")
    @JsonIgnore
    @FieldMetadata(nama=TemplatingHelper.PEMERIKSA_SURAT_KEY, type= MetadataValueType.MULTI_VALUE)
    private List<PenerimaNomorManual> penerima;

    @Transient
    @Override
    public String getRelatedId() throws WorkflowException {
        return Optional.ofNullable(id).map(Object::toString).orElseThrow(() -> new WorkflowException("Invalid workflow entity, no related id supplied.", "QT-ERR-WF-03"));
    }

    @Transient
    @Override
    public String getModuleName() {
        return "Penomoran Manual";
    }

    @Override
    public String getNoDokumen() {
        return null;
    }

    @Transient
    @Override
    public String getPageLink() {
        return "";
    }

    @Transient
    @Override
    public String getClassName() {
        return Optional.of(getClass().getName()).filter(t -> t.contains("_")).map(t -> Stream.of(t.split("_")).findFirst().orElse(getClass().getName())).orElse(getClass().getName());
    }

    @Override
    public List<ProcessInstance> getProcessInstances() {
        return null;
    }

    @Override
    public void setProcessInstances(List<ProcessInstance> processInstances) {

    }

    @Override
    public List<FormValue> getFormValue() {
        return null;
    }

    @Override
    public IWorkflowService getWorkflowService() {
        return (IWorkflowService) BeanUtil.getBean(PenomoranManualService.class);
    }

    @Transient
    @Override
    public EntityStatus getCompletionStatus() {
        return EntityStatus.valueOf(status);
    }

    @Transient
    @Override
    public void setCompletionStatus(EntityStatus completionStatus) {
        status = completionStatus.toString();
    }

    @Override
    public FormDefinition getFormDefinition() {
        return null;
    }

    @Transient
    private String jenisDokumen;

    @Transient
    private String taskExecutor;

    @Transient
    private String taskResponse;

    @Transient
    private String nomor;

    @Transient
    private String formattedTanggal;

    @Transient
    private String formattedSender;

    @Transient
    private String klasifikasiKeamanan;
}
