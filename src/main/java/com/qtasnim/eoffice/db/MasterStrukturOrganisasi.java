package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_struktur_organisasi")
@XmlRootElement
@EqualsAndHashCode(exclude = "user")
public class MasterStrukturOrganisasi implements Serializable,IAuditTrail {
    @Id
    @Column(name="id_organization")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOrganization;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 50)
    @Column(name="organization_code")
    private String organizationCode;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name="organization_name")
    private String organizationName;

    @JoinColumn(name = "organization_parent", referencedColumnName = "id_organization", nullable = true)
    @OneToOne
    @JsonIgnore
    private MasterStrukturOrganisasi parent;

    @Basic(optional = false)
    @NotNull
    @Column(name="level")
    private Integer level;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_deleted")
    private Boolean isDeleted;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_active")
    private Boolean isActive;

    @Basic()
    @NotNull
    @Column(name="is_hris")
    private Boolean isHris = true;

    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Column(name="created_date")
    private Date createdDate;

    @Basic(optional = true)
    @Size(min = 1, max = 50)
    @Column(name="modified_by", nullable = true)
    private String modifiedBy;

    @Basic(optional = true)
    @Column(name="modified_date", nullable = true)
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @OneToOne(mappedBy = "organizationEntity", fetch = FetchType.LAZY)
    @JsonIgnore
    private MasterUser user;

    @ManyToMany(cascade = {
        CascadeType.PERSIST,
        CascadeType.MERGE
    })
    @JoinTable(name = "m_user_m_role",
            joinColumns = @JoinColumn(name = "id_organization"),
            inverseJoinColumns = @JoinColumn(name = "id_role")
    )
    @JsonIgnore
    private List<MasterRole> roleList;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @JoinColumn(name = "id_persa", referencedColumnName = "id")
    @OneToOne()
    private MasterArea area;

    @Basic()
    @Column(name="singkatan",length = 20)
    private String singkatan;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_chief")
    private Boolean isChief;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_posisi")
    private Boolean isPosisi;

    @JoinColumn(name = "id_korsa", referencedColumnName = "id")
    @OneToOne()
    private MasterKorsa unit;

    @Basic()
    @Column(name="class")
    private String clazz;

    @JoinColumn(name = "id_grade", referencedColumnName = "id")
    @OneToOne()
    private MasterGrade grade;

    public MasterStrukturOrganisasi(){
    }
}
