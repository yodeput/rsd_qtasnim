/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author THINKPAD-PC
 */
@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_regulasi_terkait")
@XmlRootElement
public class RegulasiTerkait implements Serializable, IAuditTrail{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @JoinColumn(name = "regulasi_id", referencedColumnName = "id", nullable = true)
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Regulasi regulasi;
    
    @JoinColumn(name = "regulasi_terkait_id", referencedColumnName = "id", nullable = true)
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Regulasi regulasiTerkait;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;
    
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by", length = 255)
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date", columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "modified_by", length = 255)
    private String modifiedBy;

    @Basic()
    @Column(name = "modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;
    
    public RegulasiTerkait() {
        
    }
}
