package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_distribusi_dokumen_detail")
@XmlRootElement
public class DistribusiDokumenDetail implements Serializable, IAuditTrail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_distribusi", referencedColumnName = "id")
    @OneToOne
    private DistribusiDokumen distribusi;    

    @Basic()
    @Column(name = "reference_id")
    private Long referenceId;

    @Basic()
    @Column(name = "reference_table")
    private String referenceTable;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by", length = 255)
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date", columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "modified_by", length = 255)
    private String modifiedBy;

    @Basic()
    @Column(name = "modified_date", columnDefinition = "TIMESTAMP")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedDate;

    public DistribusiDokumenDetail() {
    }
}
