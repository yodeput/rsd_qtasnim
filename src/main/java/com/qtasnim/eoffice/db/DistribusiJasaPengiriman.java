package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_distribusi_jasa_pengiriman")
@XmlRootElement
public class DistribusiJasaPengiriman implements Serializable, IAuditTrail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name = "perihal", columnDefinition = "text")
    private String perihal;

    @Basic()
    @Column(name = "nomor_dokumen")
    private String nomorDokumen;

    @Basic()
    @Column(name = "tanggal_dokumen", columnDefinition = "TIMESTAMP")
    private Date tglDokumen;

    @Basic()
    @Column(name = "tanggal_pengiriman", columnDefinition = "TIMESTAMP")
    private Date tglPengiriman;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_deleted", nullable = false)
    private Boolean isDeleted = false;

    @Basic(optional = false)
    @NotNull
    @Column(name = "nomor_pengiriman", nullable = false)
    private String nomorPengiriman;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_internal", nullable = false)
    private Boolean isInternal;

    @Basic()
    @Column(name = "nomor_resi")
    private String nomorResi;

    @Basic()
    @Column(name = "status_penerimaan", columnDefinition = "ENUM")
    private String status;

    @Basic()
    @Column(name = "tanggal_diterima", columnDefinition = "TIMESTAMP")
    private Date tglDiterima;

    @Basic()
    @Column(name = "alasan_retur")
    private String alasanRetur;

    @Basic()
    @Column(name = "tanggal_retur", columnDefinition = "TIMESTAMP")
    private Date tglRetur;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @JoinColumn(name = "surat_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private Surat surat;

    @JoinColumn(name = "klasifikasi_keamanan", referencedColumnName = "id_klasifikasi_keamanan")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterKlasifikasiKeamanan klasifikasiKeamanan;

    @JoinColumn(name = "tingkat_urgensi", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterTingkatUrgensi tingkatUrgensi;

    @JoinColumn(name = "keaslian_dokumen", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterTingkatPerkembangan tingkatPerkembangan;

    @JoinColumn(name = "perusahaan_pengiriman", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterPerusahaanPengiriman perusahaanPengiriman;

    @JoinColumn(name = "pengirim", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser pengirim;

    @JoinColumn(name = "tujuan_internal", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterUser tujuanInternal;

    @JoinColumn(name = "tujuan_eksternal", referencedColumnName = "id_vendor")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterVendor tujuanEksternal;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date", columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name = "modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;
}
