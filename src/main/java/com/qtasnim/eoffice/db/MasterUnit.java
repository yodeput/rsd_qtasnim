package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_unit")
@XmlRootElement
public class MasterUnit implements Serializable, IAuditTrail {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="id")
    private String id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name="nama")
    private String nama;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @JoinColumn(name = "id_company", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;
}
