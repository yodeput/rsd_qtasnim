package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.management.relation.Role;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_user_m_role")
@XmlRootElement
public class RoleUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected RoleUserPK roleUserPK;

    public RoleUser() {
    }

    public RoleUser(RoleUserPK masterRoleUserPK) {
        this.roleUserPK = masterRoleUserPK;
    }

    public RoleUser(Long roleId, Long organizationId) {
        this.roleUserPK = new RoleUserPK(roleId, organizationId);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleUserPK != null ? roleUserPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoleUser)) {
            return false;
        }
        RoleUser other = (RoleUser) object;
        if ((this.roleUserPK == null && other.roleUserPK != null) || (this.roleUserPK != null && !this.roleUserPK.equals(other.roleUserPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.qtasnim.eoffice.db.RoleUser[ masterRoleUserPK=" + roleUserPK + " ]";
    }
}
