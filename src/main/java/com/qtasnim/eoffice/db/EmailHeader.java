package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_email_header")
@XmlRootElement
public class EmailHeader {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "email_user_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private EmailUser user;

    @Column(name = "uid")
    @Basic(optional = false)
    @NotNull
    private String uid;

    @Column(name = "subject")
    @Basic(optional = false)
    @NotNull
    private String subject;

    @Column(name = "from_email")
    @Basic(optional = false)
    @NotNull
    private String from;

    @Column(name = "from_name")
    @Basic()
    private String fromName;

    @Column(name = "sent_date", columnDefinition = "TIMESTAMP")
    @Basic(optional = false)
    @NotNull
    private Date sentDate;

    @Column(name = "size")
    @Basic(optional = false)
    @NotNull
    private Long size;

    @Column(name = "folder")
    @Basic(optional = false)
    @NotNull
    private String folder;

    @Column(name = "is_important")
    @Basic(optional = false)
    @NotNull
    private Boolean important;

    @Column(name = "is_starred")
    @Basic(optional = false)
    @NotNull
    private Boolean starred;

    @OneToMany(mappedBy = "header", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<EmailDetailAttachment> attachments;

    @OneToOne(mappedBy = "header", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private EmailDetail detail;

    @Column(name = "has_attachment")
    @Basic(optional = false)
    @NotNull
    private Boolean hasAttachment;
}
