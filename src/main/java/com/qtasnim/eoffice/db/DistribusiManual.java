package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_distribusi_manual")
@XmlRootElement
public class DistribusiManual implements Serializable, IAuditTrail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name = "tanggal_dokumen", columnDefinition = "TIMESTAMP")
    private Date tglDokumen;

    @Basic()
    @Column(name = "tanggal_diterima", columnDefinition = "TIMESTAMP")
    private Date tglDiterima;

    @Basic()
    @Column(name = "perihal", columnDefinition = "text")
    private String perihal;

    @Basic()
    @Column(name = "no_dokumen")
    private String nomorDokumen;

    @Basic()
    @Column(name = "jabatan_penerima")
    private String jabatanPenerima;

    @Basic()
    @Column(name = "nama_penerima")
    private String namaPenerima;

    @Basic()
    @Column(name = "nipp_penerima")
    private String nippPenerima;

    @Basic()
    @Column(name = "nama_kurir")
    private String kurir;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_deleted")
    private Boolean isDeleted = false;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @JoinColumn(name = "surat_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private Surat surat;

    @JoinColumn(name = "klasifikasi_keamanan", referencedColumnName = "id_klasifikasi_keamanan")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterKlasifikasiKeamanan klasifikasiKeamanan;

    @JoinColumn(name = "tingkat_urgensi", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterTingkatUrgensi tingkatUrgensi;

    @JoinColumn(name = "keaslian_dokumen", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterTingkatPerkembangan tingkatPerkembangan;

    @JoinColumn(name = "petugas_menyerahkan", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser pengirim;

    @JoinColumn(name = "user_id_penerima", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterUser penerima;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by", length = 255)
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date", columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "modified_by", length = 255)
    private String modifiedBy;

    @Basic()
    @Column(name = "modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "distribusi")
    @JsonIgnore
    private List<TujuanDistribusiManual> tujuan;

    public DistribusiManual() {
        tujuan = new ArrayList<>();
    }
}
