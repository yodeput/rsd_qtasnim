package com.qtasnim.eoffice.db.constraints;


import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

// @Constraint(validatedBy = { CompanyIdValidator.class })
@Target({ ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CompanyId {
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String serviceQualifier() default "";

    @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        CompanyId[] value();
    }
}
