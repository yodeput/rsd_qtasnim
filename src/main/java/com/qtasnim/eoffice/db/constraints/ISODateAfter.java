package com.qtasnim.eoffice.db.constraints;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ISODateAfter {

    String field();

    boolean orEquals() default false;

    String message() default "";
}
