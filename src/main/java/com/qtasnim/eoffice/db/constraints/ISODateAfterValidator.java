package com.qtasnim.eoffice.db.constraints;

import com.qtasnim.eoffice.util.DateUtil;
import org.apache.commons.beanutils.BeanUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.util.Date;

public class ISODateAfterValidator implements ConstraintValidator<EnableISODateAfterValidator, Object> {
    @Override
    public void initialize(EnableISODateAfterValidator ageValue) {

    }

    @Override
    public boolean isValid(final Object o, final ConstraintValidatorContext context) {
        boolean valid = true;
        try {
            String mainField, secondField, message;
            Object firstObj, secondObj;

            final Class<?> clazz = o.getClass();
            final Field[] fields = clazz.getDeclaredFields();

            for (Field field : fields) {
                if (field.isAnnotationPresent(ISODateAfter.class)) {
                    mainField = field.getName();
                    secondField = field.getAnnotation(ISODateAfter.class).field();
                    message = field.getAnnotation(ISODateAfter.class).message();

                    if (message == null || "".equals(message))
                        message = "Field " + mainField + " must be after " + secondField;

                    firstObj = BeanUtils.getProperty(o, mainField);
                    secondObj = BeanUtils.getProperty(o, secondField);

                    valid = firstObj == null || secondObj == null;

                    if (firstObj != null && secondObj != null) {
                        DateUtil dateUtil = new DateUtil();
                        Date firstDate = dateUtil.getDateFromISOString(firstObj.toString());
                        Date secondDate = dateUtil.getDateFromISOString(secondObj.toString());

                        valid = firstDate.after(secondDate) || (field.getAnnotation(ISODateAfter.class).orEquals() && firstDate.compareTo(secondDate) == 0);
                    }

                    if (!valid) {
                        context.disableDefaultConstraintViolation();
                        context.buildConstraintViolationWithTemplate(message).addPropertyNode(mainField).addConstraintViolation();
                        break;
                    }
                }
            }
        } catch (final Exception e) {
            // ignore
            //e.printStackTrace();
        }
        return valid;
    }
}
