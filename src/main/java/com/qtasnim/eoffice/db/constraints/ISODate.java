package com.qtasnim.eoffice.db.constraints;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = { ISODateValidator.class })
@Target({ ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ISODate {
    String message() default "should be valid format: {format}";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
    String format() default "yyyy-MM-dd'T'HH:mm:ss.SSSX";

    @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        ISODate[] value();
    }
}
