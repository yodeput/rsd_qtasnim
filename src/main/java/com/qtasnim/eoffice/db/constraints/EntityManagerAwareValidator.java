package com.qtasnim.eoffice.db.constraints;

import javax.persistence.EntityManager;

public interface EntityManagerAwareValidator {
    void setEntityManager(EntityManager entityManager);
}
