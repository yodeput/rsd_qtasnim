package com.qtasnim.eoffice.db.constraints;

import lombok.Setter;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class UniqueValidator implements ConstraintValidator<Unique, String>, EntityManagerAwareValidator  {
    @Setter
    private EntityManager entityManager;
    private String fieldName;
    private String entityName;

    @Override
    public void initialize(Unique constraintAnnotation) {
        this.fieldName = constraintAnnotation.fieldName();
        this.entityName = "com.qtasnim.eoffice.db." + constraintAnnotation.entityName();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (entityManager != null) {
            try {
                Class entityClass = Class.forName(this.entityName);

                CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                CriteriaQuery q = cb.createQuery();

                Root<?> root = q.from(entityClass);
                q.select(root);

                String propertyName = fieldName;

                List<Predicate> predicates = new ArrayList<Predicate>();
                Predicate predicate = cb.equal(cb.lower(root.get(propertyName)), value.toLowerCase());
                predicates.add(predicate);

                q.where(predicates.toArray(new Predicate[predicates.size()]));
                TypedQuery<Object> typedQuery = entityManager.createQuery(q);
                List<Object> resultSet = typedQuery.getResultList();
                return resultSet.size() == 0;

            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        return true;
    }
}
