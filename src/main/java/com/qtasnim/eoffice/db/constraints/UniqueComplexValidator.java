package com.qtasnim.eoffice.db.constraints;

import com.google.common.base.Strings;
import lombok.Setter;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UniqueComplexValidator implements ConstraintValidator<EnableUniqueValidator, Object>, EntityManagerAwareValidator  {
    @Setter
    private EntityManager entityManager;
    private String[] fieldNames;
    private String entityName;
    private Object result;
    private Object result1;

    @Override
    public void initialize(EnableUniqueValidator val) {
        fieldNames = val.fieldNames();
        entityName = "com.qtasnim.eoffice.db." + val.entityName();
    }

    @Override
    public boolean isValid(final Object o, final ConstraintValidatorContext context) {
        boolean valid = true;

        if (entityManager != null) {
            try {
                String idField = "", companyIdField = "", message;
                Object idObj, companyIdObj;

                final Class<?> clazz = o.getClass();
                final Field[] fields = clazz.getDeclaredFields();

                for (Field field : fields) {
                    if (field.isAnnotationPresent(Id.class)) {
                        idField = field.getName();
                        if (!Strings.isNullOrEmpty(companyIdField)) break;
                    }
                    if (field.isAnnotationPresent(CompanyId.class)) {
                        companyIdField = field.getName();
                        if (!Strings.isNullOrEmpty(idField)) break;
                    }
                }

                List<Predicate> predicates = new ArrayList<Predicate>();

                for (String field : fieldNames) {
                    // Checking to DB
                    Class entityClass = Class.forName(this.entityName);

                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery q = cb.createQuery();

                    Root<?> root = q.from(entityClass);
                    q.select(root);

                    Object fieldObj = BeanUtils.getProperty(o, field);
                    String uniqueValue = String.valueOf(fieldObj);
                    Predicate predicate = cb.equal(cb.lower(root.get(field)), uniqueValue.toLowerCase());

                    predicates.clear();
                    predicates.add(predicate);

                    if (!Strings.isNullOrEmpty(companyIdField)) {
                        companyIdObj = BeanUtils.getProperty(o, companyIdField);
                        Predicate companyCodePredicate = cb.equal(root.join("companyCode").get("id"), companyIdObj);
                        predicates.add(companyCodePredicate);
                    }

                    q.where(predicates.toArray(new Predicate[predicates.size()]));
                    TypedQuery<Object> typedQuery = entityManager.createQuery(q);
                    List<Object> resultSet = typedQuery.getResultList();
                    valid = resultSet.size() == 0;

                    if (resultSet.size() == 0) {
                        valid = true;
                    } else {
                        idObj = BeanUtils.getProperty(o, idField);

                        for (Object result : resultSet) {
                            Class<?> entityClazz = result.getClass();
                            for (Field f : entityClazz.getDeclaredFields()){
                                f.setAccessible(true);
                                String name = f.getName();
                                Object value = f.get(result);
                                if (name.equals(idField)) {
                                    if (Objects.equals(String.valueOf(value), (String.valueOf(idObj)))) {
                                        valid = true;
                                        break;
                                    }
                                }

                                if (name.equals(field)) {
                                    if (Objects.equals(String.valueOf(value).toLowerCase(), uniqueValue.toLowerCase())) {
                                        valid = false;
                                    } else {
                                        valid = true;
                                    }
                                    break;
                                }
                            }
                            if (!valid) break;
                        }
                    }

                    if (!valid) {
                        // split string if founded Uppercase
//                    s.split("(?=\\p{Upper})")
                        field = StringUtils.capitalize(field);
                        String[] nameParts = field.split("(?=\\p{Lu})");
                        message = String.join(" ", nameParts) + " sudah ada";
                        context.disableDefaultConstraintViolation();
                        context.buildConstraintViolationWithTemplate(message).addPropertyNode(field)
                                .addConstraintViolation();
                        break;
                    }

                }

            } catch (final Exception e) {
                // ignore
                e.printStackTrace();
                valid = false;
            }
        }

        return valid;
    }
}
