package com.qtasnim.eoffice.db.constraints;

import com.qtasnim.eoffice.util.DateUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ISODateValidator implements ConstraintValidator<ISODate, String> {
    protected String format;

    @Override
    public void initialize(ISODate ageValue) {
        this.format = ageValue.format();
    }

    @Override
    public boolean isValid(String date, ConstraintValidatorContext constraintValidatorContext) {
        if (date == null) {
            return true;
        }

        try {
            DateUtil dateUtil = new DateUtil();
            dateUtil.getDateFromISOString(date);

            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
