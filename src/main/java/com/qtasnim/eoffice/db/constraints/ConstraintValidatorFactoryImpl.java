package com.qtasnim.eoffice.db.constraints;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorFactory;

public class ConstraintValidatorFactoryImpl implements ConstraintValidatorFactory {
    @PersistenceContext(unitName = "eofficePU")
    private EntityManager em;

    @Override
    public <T extends ConstraintValidator<?, ?>> T getInstance(Class<T> key) {
        T instance = null;

        try {
            instance = key.newInstance();
        } catch (Exception e) {
            // could not instantiate class
            e.printStackTrace();
        }

        try {
            if (EntityManagerAwareValidator.class.isAssignableFrom(key)) {
                EntityManagerAwareValidator validator = (EntityManagerAwareValidator) instance;

                if (validator != null) {
                    validator.setEntityManager(em);
                }
            }
        } catch (Exception e) {
            // could not setup em
            e.printStackTrace();
        }

        return instance;
    }

    @Override
    public void releaseInstance(ConstraintValidator<?, ?> instance) {

    }
}