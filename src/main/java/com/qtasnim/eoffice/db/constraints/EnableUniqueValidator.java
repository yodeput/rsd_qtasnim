package com.qtasnim.eoffice.db.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueComplexValidator.class)
@Documented
public @interface EnableUniqueValidator {
    String message() default "Unique Complex Constrains Validation";
    String entityName();
    String[] fieldNames() default {};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
