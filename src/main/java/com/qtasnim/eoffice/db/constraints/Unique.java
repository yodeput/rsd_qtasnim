package com.qtasnim.eoffice.db.constraints;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

// @Constraint(validatedBy = { UniqueValidator.class })
@Target({ ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Unique {
    String message() default "should be unique field";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String serviceQualifier() default "";
    String fieldName();
    String entityName();
    // String[] columnNames();

    @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        Unique[] value();
    }
}
