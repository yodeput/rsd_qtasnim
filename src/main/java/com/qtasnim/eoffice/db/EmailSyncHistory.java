package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "t_email_sync_history")
@XmlRootElement
public class EmailSyncHistory {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "email_account_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private EmailAccount account;

    @Column(name = "sync_date", columnDefinition = "TIMESTAMP")
    @Basic(optional = false)
    @NotNull
    private Date syncDate;

    @Column(name = "status")
    @Basic(optional = false)
    @NotNull
    private String status;

    @Column(name = "message")
    @Basic(optional = false)
    @NotNull
    private String message;
}
