package com.qtasnim.eoffice.db;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "v_surat_setuju_v2")
@Getter
@Setter
public class ViewSuratSetujuV2 {
    @Id
    @Column(name="id_task")
    private Long idTask;

    @Column(name="id_surat")
    private Long idSurat;

    @Column(name="status_navigasi")
    private String statusNavigasi;

    @Column(name="id_penandatangan")
    private Long idPenandatangan;

    @Column(name="id_delegasi_penandatangan")
    private Long idDelegasiPenandatangan;

    @Column(name="id_pemeriksa")
    private Long idPemeriksa;

    @Column(name="id_delegasi_pemeriksa")
    private Long idDelegasiPemeriksa;
}
