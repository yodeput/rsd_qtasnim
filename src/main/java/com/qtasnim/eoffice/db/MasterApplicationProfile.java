package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_application_profile")
@XmlRootElement
public class MasterApplicationProfile {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name="profile_name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_active")
    private Boolean isActive = false;

    @OneToMany(mappedBy = "profile")
    private List<MasterApplicationConfig> config;

    @Version
    @Column(name="version", nullable = false)
    @NotNull
    private long version = 0L;
}
