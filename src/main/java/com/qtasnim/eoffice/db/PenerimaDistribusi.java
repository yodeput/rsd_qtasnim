package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_penerima_distribusi")
@XmlRootElement
public class PenerimaDistribusi implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name = "seq",columnDefinition = "TINYINT")
    private Integer seq;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi organization;

    @JoinColumn(name = "id_distribusi", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private DistribusiDokumen distribusi;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser user;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_download")
    private Boolean isDownload = false;

    @Basic()
    @Column(name = "download_date", columnDefinition = "TIMESTAMP")
    private Date downloadDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_read")
    private Boolean isRead = false;

    @Basic()
    @Column(name = "read_date", columnDefinition = "TIMESTAMP")
    private Date readDate;
    
    public PenerimaDistribusi() {}
}
