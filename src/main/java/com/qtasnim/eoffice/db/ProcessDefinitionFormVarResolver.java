package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "process_definition_form_var_resolver")
@XmlRootElement
public class ProcessDefinitionFormVarResolver {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotEmpty
    @Basic(optional = false)
    @Column(name = "var_name")
    @Size(min = 1, max = 100)
    private String varName;

    @NotNull
    @NotEmpty
    @Basic(optional = false)
    @Column(name = "value_resolver")
    @Size(min = 1)
    private String valueResolver;

    @JoinColumn(name = "process_definition_id", referencedColumnName = "id", nullable = false)
    @ManyToOne
    @NotNull
    private ProcessDefinition processDefinition;
}
