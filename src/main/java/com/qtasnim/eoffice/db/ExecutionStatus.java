package com.qtasnim.eoffice.db;

public enum ExecutionStatus {
    PENDING("PENDING"), EXECUTED("EXECUTED"), SKIP("SKIP");

    String toString;

    private ExecutionStatus(String toString) {
        this.toString = toString;
    }

    private ExecutionStatus() {
    }

    public String toString() {
        return this.toString != null?this.toString:super.toString();
    }
}
