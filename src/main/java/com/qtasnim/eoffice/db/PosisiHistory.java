package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_posisi_history")
@XmlRootElement
public class PosisiHistory implements Serializable, IAuditTrail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "plans_ex", length = 50)
    private String plans_ex;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "pstxt_ex", length = 255)
    private String pstxt_ex;

    @Basic()
    @Column(name = "begda")
    private Date begda;

    @Basic()
    @Column(name = "endda")
    private Date endda;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "plans_pr", length = 50)
    private String plans_pr;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "pstxt_pr", length = 255)
    private String pstxt_pr;

    @Basic()
    @NotNull
    @Column(name = "begda_prev")
    private Date begda_prev;

    @Basic()
    @Column(name = "endda_prev")
    private Date endda_prev;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date")
    private Date createdDate;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "modified_by")
    private String modifiedBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_date")
    private Date modifiedDate;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;
}

