package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_shared_doclib_vendor")
@XmlRootElement
public class SharedDocLibVendor implements Serializable, ICreationAudit {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "m_vendor_id", referencedColumnName = "id_vendor")
    @ManyToOne
    @NotNull
    private MasterVendor vendor;

    @JoinColumn(name="t_shared_doclib_id", referencedColumnName="id")
    @OneToOne
    @NotNull
    private SharedDocLib sharedDocLib;

    @JoinColumn(name="t_surat_id", referencedColumnName="id")
    @ManyToOne
    @NotNull
    private Surat surat;

    @Basic()
    @Column(name="readDate")
    private Date readDate;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by", length = 255)
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date", columnDefinition = "TIMESTAMP")
    private Date createdDate;
}
