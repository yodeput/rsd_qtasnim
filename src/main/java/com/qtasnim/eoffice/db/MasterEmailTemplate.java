package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_email_template")
@XmlRootElement
public class MasterEmailTemplate {
    @Id
    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="id")
    private String id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="description")
    private String description;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="subject")
    private String subject;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Column(name="template")
    private String template;
}
