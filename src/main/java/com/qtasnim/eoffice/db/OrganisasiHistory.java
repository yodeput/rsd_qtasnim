package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_organisasi_history")
@XmlRootElement
public class OrganisasiHistory implements Serializable, IAuditTrail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name = "level", columnDefinition = "TINYINT", length = 4)
    private Integer level;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "parnt", length = 50)
    private String parnt;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "orgeh", length = 50)
    private String orgeh;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "ouabv", length = 50)
    private String ouabv;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "outxt")
    private String outxt;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "persa", length = 50)
    private String persa;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "btrtl", length = 50)
    private String btrtl;

    @Basic()
    @Column(name = "begda_o")
    private Date begda_o;

    @Basic()
    @Column(name = "endda_o")
    private Date endda_o;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "plans", length = 50)
    private String plans;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "psabv", length = 50)
    private String psabv;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "pstxt", length = 50)
    private String pstxt;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "trfgb", length = 50)
    private String trfgb;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "trfg1", length = 50)
    private String trfg1;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "trgs1", length = 50)
    private String trgs1;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "chief", length = 50)
    private String chief;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "sttus", length = 50)
    private String sttus;

    @Basic()
    @NotNull
    @Column(name = "begda_s")
    private Date begda_s;

    @Basic()
    @Column(name = "endda_s")
    private Date endda_s;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "pernr", length = 50)
    private String pernr;
    @Basic()
    @Size(min = 1, max = 100)
    @Column(name = "cname", length = 100)
    private String cname;

    @Basic()
    @NotNull
    @Column(name = "begda_p")
    private Date begda_p;

    @Basic()
    @Column(name = "endda_p")
    private Date endda_p;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date")
    private Date createdDate;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "modified_by")
    private String modifiedBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_date")
    private Date modifiedDate;
}

