package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_dokumen_history")
@XmlRootElement
public class DokumenHistory implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name="reference_table")
    private String referenceTable;

    @Basic(optional = false)
    @NotNull
    @Column(name="reference_id",length = 20)
    private Long referenceId;

    @Basic(optional = false)
    @Column(name = "status", columnDefinition = "ENUM")
    private String status;

    @Basic()
    @Column(name = "action_date", columnDefinition = "TIMESTAMP")
    private Date actionDate;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterStrukturOrganisasi organization;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterUser user;

    @Basic()
    @Column(name="delegasi_type")
    private String delegasiType;
}
