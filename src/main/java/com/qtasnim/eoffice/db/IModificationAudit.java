package com.qtasnim.eoffice.db;

import java.util.Date;

public interface IModificationAudit {
    public String getModifiedBy();
    public void setModifiedBy(String modifiedBy);
    public Date getModifiedDate();
    public void setModifiedDate(Date modifiedDate);
}
