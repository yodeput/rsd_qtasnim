package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_penyerah_disposisi")
@XmlRootElement
public class PenyerahDisposisi implements Serializable  {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_parent", referencedColumnName = "id", nullable = true)
    @OneToOne
    private PenyerahDisposisi parent;

    @JoinColumn(name = "id_permohonan", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private PermohonanDokumen permohonan;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterStrukturOrganisasi organization;

    @JoinColumn(name = "id_tindakan", referencedColumnName = "id_tindakan")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterTindakan tindakan;

    @Basic(optional = false)
    @NotNull
    @Column(name="comment", columnDefinition="TEXT")
    private String comment;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_read")
    private Boolean isRead = false;

    @Basic(optional = false)
    @Column(name="assigned_date", nullable= true)
    private Date assignedDate;

    @Basic(optional = false)
    @Column(name="executed_date", nullable = true)
    private Date executedDate;

    @Basic(optional = false)
    @Column(name="execution_status", nullable = true)
    private String status;
}
