package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.helpers.TemplatingHelper;
import com.qtasnim.eoffice.listeners.DocListener;
import com.qtasnim.eoffice.services.DistribusiDokumenService;
import com.qtasnim.eoffice.services.NgSuratService;
import com.qtasnim.eoffice.services.PermohonanDokumenService;
import com.qtasnim.eoffice.util.BeanUtil;
import com.qtasnim.eoffice.util.FieldMetadata;
import com.qtasnim.eoffice.util.MetadataValueType;
import com.qtasnim.eoffice.workflows.IWorkflowService;
import com.qtasnim.eoffice.workflows.WorkflowException;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Getter
@Setter
@Entity
@EntityListeners(DocListener.class)
@Table(name = "t_permohonan_dokumen")
@XmlRootElement
public class PermohonanDokumen implements Serializable, IDocTrail, IWorkflowEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name="tipe_layanan", columnDefinition = "ENUM")
    private String tipeLayanan;

    @JoinColumn(name = "id_arsip", referencedColumnName = "id")
    @OneToOne
    private Arsip arsip;

    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @NotNull
    @Column(name="no_registrasi")
    private String noRegistrasi;

    @Basic()
    @Column(name="tgl_pinjam",columnDefinition = "TIMESTAMP")
    private Date tglPinjam;

    @Basic()
    @Column(name="tgl_kembali",columnDefinition = "TIMESTAMP")
    private Date tglKembali;

    @Basic()
    @Column(name="tgl_target",columnDefinition = "TIMESTAMP")
    private Date tglTarget;

    @Basic()
    @Column(name="lama_pinjam",columnDefinition = "INTEGER")
    private Integer lamaPinjam;

    @JoinColumn(name = "id_user_peminjam", referencedColumnName = "id")
    @OneToOne()
    private MasterUser UserPeminjam;

    @JoinColumn(name = "id_user_tujuan", referencedColumnName = "id")
    @OneToOne()
    private MasterUser userTujuan;

    @JoinColumn(name = "id_unit_peminjam", referencedColumnName = "id")
    @OneToOne()
    private MasterKorsa unitPeminjam;

    @JoinColumn(name = "id_unit_tujuan", referencedColumnName = "id")
    @OneToOne()
    private MasterKorsa unitTujuan;

    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @NotNull
    @Column(name="perihal")
    private String perihal;

    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @NotNull
    @Column(name="keperluan")
    private String keperluan;

    @Basic()
    @Column(name="status", columnDefinition = "ENUM")
    private String status;

    @Basic()
    @Column(name="perpanjangan",columnDefinition = "TINYINT")
    private Integer perpanjangan;

    @JoinColumn(name = "company_id", referencedColumnName = "id", nullable = false)
    @OneToOne
    private CompanyCode companyCode;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date createdDate;

    @Basic(optional = true)
    @Column(name="modified_by", nullable = true)
    private String modifiedBy;

    @Basic(optional = true)
    @Column(name="modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @Basic()
    @Column(name="is_deleted")
    private Boolean isDeleted=false;

    @JoinColumn(name = "id_surat", referencedColumnName = "id")
    @OneToOne
    private Surat surat;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "permohonanDokumen")
    @JsonIgnore
    // @FieldMetadata(nama=TemplatingHelper.PENANDATANGAN_KEY, type= MetadataValueType.MULTI_VALUE, format = "${organization.organizationName} | ${organization.user.nameFront} ${organization.user.nameMiddleLast} | ${organization.user.employeeId}")
    private List<PermohonanDokumenPenerima> penerima;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "permohonanDokumen")
    @JsonIgnore
    // @FieldMetadata(nama=TemplatingHelper.TEMBUSAN_KEY, type= MetadataValueType.MULTI_VALUE, format = "${organization.organizationName} | ${organization.user.nameFront} ${organization.user.nameMiddleLast} | ${organization.user.employeeId}")
    private List<PermohonanDokumenTembusan> tembusan;

    public PermohonanDokumen(){}


    @Transient
    @Override
    public String getRelatedId() throws WorkflowException {
        return Optional.ofNullable(id).map(Object::toString).orElseThrow(() -> new WorkflowException("Invalid workflow entity, no related id supplied.", "QT-ERR-WF-03"));
    }

    @Transient
    @Override
    public String getModuleName() {
        return "Layanan Dokumen";
    }

    @Override
    public String getNoDokumen() {
        return null;
    }

    @Transient
    @Override
    public String getPageLink() {
        return "/apps/dokumen";
    }

    @Transient
    @Override
    public String getClassName() {
        return Optional.of(getClass().getName()).filter(t -> t.contains("_")).map(t -> Stream.of(t.split("_")).findFirst().orElse(getClass().getName())).orElse(getClass().getName());
    }

    @Override
    public List<ProcessInstance> getProcessInstances() {
        return null;
    }

    @Override
    public void setProcessInstances(List<ProcessInstance> processInstances) {

    }

    @Override
    public List<FormValue> getFormValue() {
        return null;
    }

    @Transient
    @Override
    public EntityStatus getCompletionStatus() {
        return EntityStatus.valueOf(status);
    }

    @Transient
    @Override
    public void setCompletionStatus(EntityStatus completionStatus) {
        status = completionStatus.toString();
    }

    @Override
    public FormDefinition getFormDefinition() {
        return null;
    }

    @Transient
    private String jenisDokumen;

    @Transient
    private String taskExecutor;

    @Transient
    private String taskResponse;

    @Transient
    private String formattedTanggal;

    @Transient
    private String formattedSender;

    @Transient
    private String klasifikasiKeamanan;

    @Transient
    @Override
    public String getNomor() { return noRegistrasi; }

    @Transient
    @Override
    public void setNomor(String param) { noRegistrasi = param; }

    @Override
    public IWorkflowService getWorkflowService() {
        return (IWorkflowService) BeanUtil.getBean(PermohonanDokumenService.class);
    }
}
