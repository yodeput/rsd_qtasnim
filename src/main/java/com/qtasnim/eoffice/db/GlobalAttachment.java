package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.DocListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(DocListener.class)
@Table(name = "t_global_attachment")
@XmlRootElement
public class GlobalAttachment implements Serializable, IDocTrail{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="doc_generated_name")
    private String docGeneratedName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="doc_id")
    private String docId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="doc_name")
    private String docName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="reference_table")
    private String referenceTable;

    @Basic(optional = false)
    @NotNull
//    @Size(min = 1, max = 20)
    @Column(name="reference_id",length = 20)
    private Long referenceId;

    @Basic()
    @Column(name="extracted_text",columnDefinition = "TEXT")
    private String extractedText;

    @Basic(optional=false)
    @NotNull
    @Column(name="is_deleted")
    private Boolean isDeleted = false;

    @Basic(optional=false)
    @NotNull
    @Column(name="is_konsep")
    private Boolean isKonsep = false;

    @Basic(optional=false)
    @NotNull
    @Column(name="is_published")
    private Boolean isPublished = false;

    @Basic(optional = false)
    @NotNull
    @Column(name="doc_size")
    private Long size = 0L;

    @Version
    @Column(name="version", nullable = true)
    private Long version = 0L;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="mime_type")
    private String mimeType;

    @Basic()
    @Column(name="no_id_doc")
    private String number;

    @JoinColumn(name = "company_id", referencedColumnName = "id",nullable = false)
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic(optional = false)
    @Size(min = 1)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date createdDate;

    @Basic(optional = true)
    @Size(min = 1)
    @Column(name="modified_by", nullable = true)
    private String modifiedBy;

    @Basic(optional = true)
    @Column(name="modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @Basic()
    @Column(name="id_document")
    private String idDocument;

    public GlobalAttachment(){}
}
