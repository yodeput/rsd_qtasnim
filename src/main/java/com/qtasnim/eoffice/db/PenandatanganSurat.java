package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_penandatangan_surat")
@XmlRootElement
public class PenandatanganSurat implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi organization;

    @JoinColumn(name = "id_surat", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Surat surat;

    @Basic()
    @Column(name = "seq",columnDefinition = "TINYINT")
    private Integer seq;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser user;

    @JoinColumn(name = "org_an_id", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi orgAN;

    @JoinColumn(name = "user_an_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser userAN;

    @JoinColumn(name = "id_vendor", referencedColumnName = "id_vendor")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterVendor vendor;

    @Basic()
    @Column(name="delegasi_type")
    private String delegasiType;
}
