package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_sekretaris")
@XmlRootElement
public class MasterSekretaris implements Serializable, IAuditTrail  {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="id_sekretaris")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_pejabat", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterStrukturOrganisasi pejabat;

    @JoinColumn(name = "sekretaris", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterStrukturOrganisasi sekretaris;

    @Basic(optional = false)
    @NotNull
    @Column(name="tipe_penerimaan", length = 255)
    private String tipePenerimaan;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_active")
    private Boolean isActive = true;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_deleted")
    private Boolean isDeleted;
}
