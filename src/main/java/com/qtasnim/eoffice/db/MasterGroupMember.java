package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_group_member")
@XmlRootElement
public class MasterGroupMember {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "m_user_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private MasterUser member;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_owner")
    private Boolean isOwner = false;

    @JoinColumn(name = "m_group_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private MasterGroup group;
}
