package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString(exclude = "formFields")
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "form_definition")
@XmlRootElement
public class FormDefinition implements Serializable, IAuditTrail {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name="form_version")
    private Double version;

    @JoinColumn(name = "id_jenis_dokumen", referencedColumnName = "id_jenis_dokumen")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterJenisDokumen jenisDokumen;

    @OneToMany(mappedBy="definition", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FormField> formFields;

    @Basic(optional = false)
    @NotNull
    @Column(name="form_name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Column(name="description", columnDefinition="TEXT")
    private String description;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_active")
    private Boolean isActive = true;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;


    public FormDefinition() {
        formFields = new ArrayList<>();
    }
}
