package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_digisign_config")
@XmlRootElement
public class MasterDigisignConfig {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name="config_name")
    private String name;

    @Basic()
    @Size(max = 1000)
    @Column(name="config_value")
    private String value;

    @JoinColumn(name = "m_bssn_digisign_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private MasterDigisign provider;
}
