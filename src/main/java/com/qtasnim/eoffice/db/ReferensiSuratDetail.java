package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_referensi_surat_detail")
@XmlRootElement
public class ReferensiSuratDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_surat", referencedColumnName = "id", nullable = false)
    @OneToOne
    private Surat surat;

    @JoinColumn(name = "id_referensi", referencedColumnName = "id", nullable = false)
    @ManyToOne
    private ReferensiSurat referensiSurat;

    public ReferensiSuratDetail() {}
}
