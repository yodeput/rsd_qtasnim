package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_para_detail")
@XmlRootElement
public class ParaDetail implements Serializable, IAuditTrail {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="id_para_detail")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idParaDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_para")
    private Para para;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date",columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne
    private MasterStrukturOrganisasi organisasi;

    @JoinColumn(name = "id_user", referencedColumnName = "id")
    @OneToOne
    private MasterUser user;
}
