package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_app_token")
@XmlRootElement
public class AppSession implements Serializable {
    private static final long serialVersionUID = 346852118121647662L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Basic()
    @Column(name="app_name")
    private String appName;

    @Basic(optional = false)
    @NotNull
    @Column(name="token", columnDefinition = "text")
    private String token;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

}