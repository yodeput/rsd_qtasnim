package com.qtasnim.eoffice.db.converters;

import com.qtasnim.eoffice.db.SyncLogStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class SyncLogStatusConverter implements AttributeConverter<SyncLogStatus, String> {
    @Override
    public String convertToDatabaseColumn(SyncLogStatus syncLogStatus) {
        return syncLogStatus.toString();
    }

    @Override
    public SyncLogStatus convertToEntityAttribute(String syncLogStatus) {
        return SyncLogStatus.valueOf(syncLogStatus);
    }
}
