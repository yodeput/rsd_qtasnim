package com.qtasnim.eoffice.db.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Base64;

@Converter
public class ByteConverter implements AttributeConverter<byte[], String> {
    @Override
    public String convertToDatabaseColumn(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    @Override
    public byte[] convertToEntityAttribute(String dbData) {
        return Base64.getDecoder().decode(dbData);
    }
}
