package com.qtasnim.eoffice.db.converters;

import com.qtasnim.eoffice.db.EntityStatus;

import javax.persistence.AttributeConverter;

public class EntityStatusConverter implements AttributeConverter<EntityStatus, String> {
    @Override
    public String convertToDatabaseColumn(EntityStatus entityStatus) {
        return entityStatus.toString();
    }

    @Override
    public EntityStatus convertToEntityAttribute(String entityStatus) {
        return EntityStatus.valueOf(entityStatus);
    }
}
