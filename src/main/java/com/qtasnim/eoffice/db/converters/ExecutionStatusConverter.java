package com.qtasnim.eoffice.db.converters;

import com.qtasnim.eoffice.db.ExecutionStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ExecutionStatusConverter implements AttributeConverter<ExecutionStatus, String> {
    @Override
    public String convertToDatabaseColumn(ExecutionStatus executionStatus) {
        return executionStatus.toString();
    }

    @Override
    public ExecutionStatus convertToEntityAttribute(String executionStatus) {
        return ExecutionStatus.valueOf(executionStatus);
    }
}
