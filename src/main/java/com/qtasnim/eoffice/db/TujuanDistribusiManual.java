package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_tujuan_distribusi_manual")
@XmlRootElement
public class TujuanDistribusiManual implements Serializable, IAuditTrail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_distribusi", referencedColumnName = "id", nullable = false)
    @OneToOne
    private DistribusiManual distribusi;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi organization;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by", length = 255)
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date", columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name = "modified_by", length = 255)
    private String modifiedBy;

    @Basic()
    @Column(name = "modified_date", columnDefinition = "TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;

    public TujuanDistribusiManual() {
    }
}
