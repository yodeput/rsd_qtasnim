package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.DocListener;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(DocListener.class)
@Table(name = "t_surat_doc_lib")
@XmlRootElement
public class SuratDocLib implements Serializable,IDocTrail{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="doc_id")
    private String docId;

    @JoinColumn(name = "company_id", referencedColumnName = "id",nullable = false)
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @JoinColumn(name = "id_surat", referencedColumnName = "id", nullable = false)
    @OneToOne
    private Surat surat;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="doc_generated_name")
    private String docGeneratedName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="mime_type")
    private String mimeType;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name="doc_name")
    private String docName;

    @Basic()
    @Size(min = 1, max = 255)
    @Column(name="no_id_doc")
    private String number;

    @Basic(optional = false)
    @NotNull
    @Column(name="doc_size")
    private Long size = 0L;

    @Version
    @Column(name="version", nullable = true)
    private Long version = 0L;

    @Basic(optional=false)
    @NotNull
    @Column(name="is_konsep")
    private Boolean isKonsep = false;

    @Basic(optional=false)
    @NotNull
    @Column(name="is_published")
    private Boolean isPublished = false;

    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date createdDate;

    @Basic(optional = true)
    @Size(min = 1, max = 50)
    @Column(name="modified_by", nullable = true)
    private String modifiedBy;

    @Basic(optional = true)
    @Column(name="modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    public SuratDocLib(){}
}
