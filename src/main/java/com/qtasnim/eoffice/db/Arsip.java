package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_arsip")
@XmlRootElement
public class Arsip implements Serializable, IAuditTrail {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_berkas", referencedColumnName = "id", nullable = false)
    @ManyToOne
    @NotNull
    private Berkas berkas;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterStrukturOrganisasi organisasi;

    @JoinColumn(name = "id_unit", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterKorsa unit;

    @JoinColumn(name = "`id_jenis_dokumen`", referencedColumnName = "id_jenis_dokumen")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterJenisDokumen jenisDokumen;

    @JoinColumn(name = "id_bahasa", referencedColumnName = "id_bahasa",nullable = true)
    @OneToOne(fetch = FetchType.EAGER)
    private MasterBahasa bahasa;

    @JoinColumn(name = "id_klasifikasi_keamanan", referencedColumnName = "id_klasifikasi_keamanan")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterKlasifikasiKeamanan klasifikasiKeamanan;

    @JoinColumn(name = "id_klasifikasi_masalah", referencedColumnName = "id_klasifikasi_masalah")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterKlasifikasiMasalah klasifikasiMasalah;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @JoinColumn(name = "id_satuan", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterSatuan satuan;

    @JoinColumn(name = "id_tingkat_perkembangan", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterTingkatPerkembangan tingkatPerkembangan;

    @JoinColumn(name = "id_tingkat_urgensi", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterTingkatUrgensi tingkatUrgensi;

    @JoinColumn(name = "id_kategori_arsip", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterKategoriArsip kategoriArsip;

    @JoinColumn(name = "id_tingkat_akses_publik", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterTingkatAksesPublik tingkatAkses;

    @JoinColumn(name = "id_media_arsip", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterMediaArsip mediaArsip;

    @JoinColumn(name = "id_pencipta_arsip", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterPenciptaArsip penciptaArsip;

    @Basic()
    @Column(name = "jra_aktif_date")
    private Date jraAktif;

    @Basic()
    @Column(name = "jra_inaktif_date")
    private Date jraInaktif;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "status")
    private String status;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "perihal")
    private String perihal;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "deskripsi_arsip")
    private String deskripsi;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nomor_arsip")
    private String nomor;

    @Basic()
    @Column(name = "`tgl_naskah`")
    private Date tglNaskah;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_arsip_asset")
    private Boolean isAsset = true;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_arsip_vital")
    private Boolean isVital = true;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;

    @Basic(optional = false)
    @NotNull
    @Column(name = "jumlah_arsip")
    private Integer jumlah;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date")
    private Date createdDate;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "modified_by")
    private String modifiedBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_date")
    private Date modifiedDate;

    public Arsip() {
    }
}
