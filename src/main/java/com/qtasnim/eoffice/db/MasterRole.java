package com.qtasnim.eoffice.db;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_role")
@XmlRootElement
public class MasterRole implements Serializable, IAuditTrail {
    @Id
    @Column(name="id_role")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRole;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 10)
    @Column(name="role_code")
    private String roleCode;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name="role_name")
    private String roleName;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 1000)
    @Column(name="description",columnDefinition = "TEXT",length = 1000)
    private String description;

    @Basic()
    @Column(name="is_active")
    private Boolean isActive;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date",columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @ManyToMany(mappedBy = "roleList")
    @JsonIgnore
    private List<MasterStrukturOrganisasi> organisasiList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "roles")
    @JsonIgnore
    private List<RoleModule> roleModuleList;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic()
    @Column(name="role_level")
    private Integer level;

    public MasterRole(){
        organisasiList = new ArrayList<>();
        roleModuleList = new ArrayList();
    }
}