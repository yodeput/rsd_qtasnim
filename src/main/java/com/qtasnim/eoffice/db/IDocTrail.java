package com.qtasnim.eoffice.db;

public interface IDocTrail extends ICreationDoc, IModificationDoc {
}
