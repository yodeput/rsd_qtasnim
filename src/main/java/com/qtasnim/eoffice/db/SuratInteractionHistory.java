package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_surat_interaction_history")
@XmlRootElement
public class SuratInteractionHistory implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_surat", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private Surat surat;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterStrukturOrganisasi organization;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterUser user;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic(optional = false)
    @Column(name="is_read")
    private Boolean isRead;

    @Basic(optional = false)
    @Column(name="is_starred")
    private Boolean isStarred;

    @Basic(optional = false)
    @Column(name="is_important")
    private Boolean isImportant;

    @Basic(optional = false)
    @Column(name="is_finished")
    private Boolean isFinished;

    @Basic(optional = false)
    @Column(name="finished_date")
    private Date finishedDate;

    @Basic(optional = false)
    @NotNull
    @Column(name="modified_date")
    private Date modifiedDate;
}
