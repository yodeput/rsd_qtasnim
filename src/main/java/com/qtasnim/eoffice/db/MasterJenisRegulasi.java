package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_jenis_regulasi")
@XmlRootElement

public class MasterJenisRegulasi implements Serializable, IAuditTrail {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_jenis_regulasi")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idJenisRegulasi;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name = "nama_jenis_regulasi")
    private String namaJenisRegulasi;

//    @Basic(optional = false)
//    @NotNull
//    @NotEmpty
//    @Size
//    @Column(name = "deskripsi_jenis_regulasi")
//    private String deskripsiJenisRegulasi;

    @Basic(optional = false)
    @NotNull
    @Column(name = "deskripsi_jenis_regulasi", nullable = false, columnDefinition = "TEXT")
    private String deskripsiJenisRegulasi;

    @Basic(optional = false)
    @NotNull
    @Column(name = "seq_jenis_regulasi")
    private Integer seq;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name = "end_date")
    private Date end;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name = "modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name = "modified_date")
    private Date modifiedDate;
}
