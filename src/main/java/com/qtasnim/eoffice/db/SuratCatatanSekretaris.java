package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_surat_catatan_sekretaris")
@XmlRootElement
public class SuratCatatanSekretaris implements IAuditTrail {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_surat", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Surat surat;

    @JoinColumn(name = "id_pejabat", referencedColumnName = "id_organization", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi pejabat;

    @JoinColumn(name = "id_sekretaris", referencedColumnName = "id_organization", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private MasterStrukturOrganisasi sekretaris;

    @Basic(optional = false)
    @NotNull
    @Column(name="note")
    private String note;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    @NotNull
    private Date createdDate;

    @Basic()
    @Column(name="modified_by", nullable = true)
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date", columnDefinition = "TIMESTAMP")
    private Date modifiedDate;
}
