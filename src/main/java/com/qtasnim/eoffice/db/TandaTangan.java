package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "m_tandatangan")
@XmlRootElement
public class TandaTangan implements Serializable, IAuditTrail {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic(optional = false)
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name="kode")
    private String kode;

    @Lob()
    @Column(name="tandatangan", columnDefinition="LONGBLOB")
    private byte[] tandatangan;

    @Basic(optional = false)
    @NotNull
    @Column(name="tipe")
    private String tipe;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterUser user;

    @Basic(optional = false)
    @NotNull
    @Column(name="start_date")
    private Date start;

    @Basic(optional = false)
    @NotNull
    @Column(name="end_date")
    private Date end;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modified_date")
    private Date modifiedDate;

    @Basic
    @Column(name="riwayat",columnDefinition = "TEXT",length = 1000)
    private String riwayat;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_deleted")
    private Boolean isDeleted;
}
