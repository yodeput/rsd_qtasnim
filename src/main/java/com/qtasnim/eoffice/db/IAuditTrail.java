package com.qtasnim.eoffice.db;

public interface IAuditTrail extends IModificationAudit, ICreationAudit {
}
