package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_surat_interaction")
@XmlRootElement
public class SuratInteraction implements Serializable, ICreationAudit{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_surat", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private Surat surat;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private MasterStrukturOrganisasi organization;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CompanyCode companyCode;

    @Basic(optional = false)
    @Column(name="is_read")
    private Boolean isRead = false;

    @Basic(optional = false)
    @Column(name="is_starred")
    private Boolean isStarred = false;

    @Basic(optional = false)
    @Column(name="is_important")
    private Boolean isImportant = false;

    @Basic(optional = false)
    @Column(name="is_finished")
    private Boolean isFinished = false;

    @Basic()
    @Column(name="finished_date")
    private Date finishedDate;

    @Basic()
    @Column(name="readDate")
    private Date readDate;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MasterUser user;
}
