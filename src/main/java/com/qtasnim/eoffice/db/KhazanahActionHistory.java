package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_khazanah_action_history")
@XmlRootElement
public class KhazanahActionHistory implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_khazanah", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private Khazanah khazanah;

    @JoinColumn(name = "id_organization", referencedColumnName = "id_organization")
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private MasterStrukturOrganisasi organization;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private MasterUser user;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_viewed")
    private Boolean isViewed;

    @Basic(optional = false)
    @NotNull
    @Column(name="is_downloaded")
    private Boolean isDownloaded;

    @Basic()
    @Column(name="viewed_date")
    private Date viewedDate;

    @Basic()
    @Column(name="downloaded_date")
    private Date downloadedDate;
}
