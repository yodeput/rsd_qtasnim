package com.qtasnim.eoffice.db;

import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_email_detail_attachment")
@XmlRootElement
public class EmailDetailAttachment {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "email_header_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private EmailHeader header;

    @Basic(optional = false)
    @NotNull
    @Column(name="size")
    private Long size = 0L;

    @Basic(optional = false)
    @NotNull
    @Column(name="type")
    private String type; //attachment | raw

    @Basic(optional = false)
    @NotNull
    @Column(name="mime_type")
    private String mimeType;

    @Basic(optional = false)
    @NotNull
    @Column(name="file_extension")
    private String fileExtension;

    @Basic(optional = false)
    @NotNull
    @Column(name="doc_id")
    private String docId;

    @Basic(optional = false)
    @NotNull
    @Column(name="file_name")
    private String fileName;

    @Basic(optional = false)
    @NotNull
    @Column(name="generated_fileName")
    private String generatedFileName;

    @Transient
    private byte[] content;
}
