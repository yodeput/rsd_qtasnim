package com.qtasnim.eoffice.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.listeners.EntityListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(EntityListener.class)
@Table(name = "t_card_checklist")
public class CardChecklist implements Serializable, IAuditTrail{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic()
    @Column(name="title")
    private String title;

    @Basic()
    @Column(name="description",columnDefinition = "TEXT")
    private String description;

    @JoinColumn(name = "card_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Card card;

    @JoinColumn(name = "company_id", referencedColumnName = "id",nullable=false)
    @OneToOne(fetch = FetchType.EAGER)
    private CompanyCode companyCode;

    @Basic()
    @Column(name="is_deleted")
    private Boolean isDeleted;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name="created_by")
    private String createdBy;

    @Basic(optional = false)
    @NotNull
    @Column(name="created_date",columnDefinition = "TIMESTAMP")
    private Date createdDate;

    @Basic()
    @Size(min = 1, max = 50)
    @Column(name="modified_by")
    private String modifiedBy;

    @Basic()
    @Column(name="modified_date",columnDefinition = "TIMESTAMP")
    private Date modifiedDate;

    @OneToMany(
            mappedBy = "checklistCard",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CardChecklistDetail> checkListDetail;

    public CardChecklist(){
        checkListDetail = new ArrayList<>();
    }
}
