package com.qtasnim.eoffice.db;

public enum JenisBuku {
    TEKSTUAL("TEKSTUAL"), ELEKTRONIK("ELEKTRONIK");

    String toString;

    private JenisBuku(String toString) {
        this.toString = toString;
    }

    private JenisBuku() {
    }

    public String toString() {
        return this.toString != null?this.toString:super.toString();
    }
}
