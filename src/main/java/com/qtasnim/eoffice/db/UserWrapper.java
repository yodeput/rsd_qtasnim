package com.qtasnim.eoffice.db;

import id.kai.ws.dto.HrisEmployeeDto;
import id.kai.ws.dto.ITWSEmployeeDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserWrapper {
    private MasterUser masterUser;
    private HrisEmployeeDto employeeDto;
    private String employeeId; //pernr
    private String fullName; //name
    private String nameFront;
    private String nameMiddleLast;
    private String email; //email
    private String salutation; //gender_key == '1' ? 'Mrs.' : 'Mr.'
    private String username; //nip
    private String businessArea; //bus_area
    private String organizationCode; //plans
    private String organizationName; //org_text
    private String companyCode; //id_bukrs
    private String companyName; //text_bukrs
    private Date tglLahir; //birthdate
    private String tempatLahir; //birthplace
    private String personalAreaCode; //personalArea
    private String personalArea; //personalArea
    private String korsaId; //id_korsa
    private String korsaAbbr; //abbr_korsa
    private String korsa; //text_korsa
    private String grade; //grade
    private String kelamin; //gender
    private String agama; //agama
    private String jabatan;
    private String kedudukan;
    private String mobilePhone;
    private Boolean isActive;
}
