package com.qtasnim.eoffice;

public class Constants {
    public static final String PERSISTENCE_CONTEXT_NAME = "eofficePU";

    public static final String APPLICATIONCONFIG_CMISPROVIDER = "APPLICATIONCONFIG_CMISPROVIDER";
    public static final String APPLICATIONCONFIG_DIGISIGNPROVIDER = "APPLICATIONCONFIG_DIGISIGNPROVIDER";
    public static final String APPLICATIONCONFIG_JWTKEY = "APPLICATIONCONFIG_JWTKEY";
    public static final String APPLICATIONCONFIG_APPLICATIONDESCRIPTION = "APPLICATIONCONFIG_APPLICATIONDESCRIPTION";
    public static final String APPLICATIONCONFIG_APPLICATIONNAME = "APPLICATIONCONFIG_APPLICATIONNAME";
    public static final String APPLICATIONCONFIG_APPLICATIONLOGO = "APPLICATIONCONFIG_APPLICATIONLOGO";
    public static final String APPLICATIONCONFIG_TIMEOUT = "APPLICATIONCONFIG_TIMEOUT";
    public static final String APPLICATIONCONFIG_BASEURL = "APPLICATIONCONFIG_BASEURL";
    public static final String APPLICATIONCONFIG_FRONTEND_URL = "APPLICATIONCONFIG_FRONTEND_URL";
    public static final String APPLICATIONCONFIG_TEMPDIR = "APPLICATIONCONFIG_TEMPDIR";

    public static final String SHARED_DOCLIB_EXPIRATION_DAYS = "SHARED_DOCLIB_EXPIRATION_DAYS";

    public static final String URL_RESET_PASSWORD = "URL_RESET_PASSWORD";
    public static final String URL_VALIDATE_CODE = "URL_VALIDATE_CODE";
    public static final String URL_CHANGE_PASSWORD = "URL_CHANGE_PASSWORD";
    public static final String URL_FORGOT_PASSWORD = "URL_FORGOT_PASSWORD";

    public static final String INTEGRATION_HRISURL = "INTEGRATION_HRISURL";
    public static final String INTEGRATION_HRISAUTHBEARER = "INTEGRATION_HRISAUTHBEARER";
    public static final String INTEGRATION_LOGINURL = "INTEGRATION_LOGINURL";
    public static final String INTEGRATION_SKIPKAILOGIN = "INTEGRATION_SKIPKAILOGIN";

    public static final String EMAIL_ENABLED = "EMAIL_ENABLED";
    public static final String EMAIL_FROMADDRESS = "EMAIL_FROMADDRESS";
    public static final String EMAIL_USERNAME = "EMAIL_USERNAME";
    public static final String EMAIL_PASSWORD = "EMAIL_PASSWORD";
    public static final String EMAIL_HOST = "EMAIL_HOST";
    public static final String EMAIL_PORT = "EMAIL_PORT";
    public static final String EMAIL_TLS = "EMAIL_TLS";
    public static final String IS_EMAIL_TEST = "IS_EMAIL_TEST";
    public static final String EMAIL_TEST = "EMAIL_TEST";

    public static final String BACKGROUND_SERVICE_WORKER = "BACKGROUND_SERVICE_WORKER";
    public static final String AMQP_HOST = "AMQP_HOST";
    public static final String AMQP_USERNAME = "AMQP_USERNAME";
    public static final String AMQP_PASSWORD = "AMQP_PASSWORD";
    public static final String AMQP_SECURE = "AMQP_SECURE";
    public static final String MESSAGE_SERVICE_PROVIDER = "MESSAGE_SERVICE_PROVIDER";

    public static final String TEMPLATE_NEW_TASK_ASSIGNMENT = "NEW_TASK_ASSIGNMENT";
    public static final String TEMPLATE_SEND_TO_EXTERNAL_INFO = "SEND_TO_EXTERNAL_INFO";
    public static final String TEMPLATE_WORKFLOW_APPROVED = "WORKFLOW_APPROVED";
    public static final String TEMPLATE_WORKFLOW_REJECTED = "WORKFLOW_REJECTED";
    public static final String TEMPLATE_WORKFLOW_INFO = "WORKFLOW_INFO";
    public static final String TEMPLATE_WORKFLOW_DISPOSISI = "WORKFLOW_DISPOSISI";
    public static final String TEMPLATE_WORKFLOW_TEMBUSAN = "WORKFLOW_TEMBUSAN";
    public static final String TEMPLATE_WORKFLOW_TEMBUSAN_EXT = "WORKFLOW_TEMBUSAN_EXT";
    public static final String TEMPLATE_WORKFLOW_SIGN = "WORKFLOW_SIGN";
    public static final String TEMPLATE_WORKFLOW_SIGN_EXT = "WORKFLOW_SIGN_EXT";
    public static final String TEMPLATE_RESET_PASSWORD = "RESET_PASSWORD";
    public static final String LAYANAN_NEED_APPROVAL = "LAYANAN_NEED_APPROVAL";
    public static final String INFO_PEMOHON_LAYANAN = "INFO_PEMOHON_LAYANAN";
    public static final String TEMBUSAN_LAYANAN = "TEMBUSAN_LAYANAN";
    public static final String TEMPLATE_NEW_OTHER_TUGAS_ASSIGNMENT = "OTHER_TUGAS_ASSIGMENT";

    public static final String JNDI_MAIL_QUEUE = "jms/MailQueue";
    public static final String JNDI_NOTIFICATION_QUEUE = "jms/myQueue";
    public static final String JNDI_DOCUMENT_ACTIVITY_QUEUE = "jms/docAct";
    public static final String JNDI_LOGGER_QUEUE = "jms/loggerAct";

    public static final String ONLYOFFICE_URLDOCUMENTCONVERTER  = "ONLYOFFICE_URLDOCUMENTCONVERTER";
    public static final String ONLYOFFICE_URLTEMPSTORAGE  = "ONLYOFFICE_URLTEMPSTORAGE";
    public static final String ONLYOFFICE_URLAPI  = "ONLYOFFICE_URLAPI";
    public static final String ONLYOFFICE_URLPRELOADER  = "ONLYOFFICE_URLPRELOADER";
    public static final String ONLYOFFICE_SECRET  = "ONLYOFFICE_SECRET";
    public static final String ONLYOFFICE_CONTAINERORIGIN  = "ONLYOFFICE_CONTAINERORIGIN";
    public static final String ONLYOFFICE_ORIGIN  = "ONLYOFFICE_ORIGIN";
    public static final String ONLYOFFICE_PLUGINLAYOUTINGKEYAPIPATH  = "ONLYOFFICE_PLUGINLAYOUTINGKEYAPIPATH";
    public static final String ONLYOFFICE_AUTOSTARTPLUGINS  = "ONLYOFFICE_AUTOSTARTPLUGINS";

    public static final String SYNC_ORGHOST  = "SYNC_ORGHOST";
    public static final String SYNC_EMPLOYEEHOST  = "SYNC_EMPLOYEEHOST";
    public static final String SYNC_POSISIHOST  = "SYNC_POSISIHOST";
    public static final String SYNC_AUTHBEARER  = "SYNC_AUTHBEARER";

    public static final String QRCODE_CONTENTDEFAULT  = "QRCODE_CONTENTDEFAULT";
    public static final String QRCODE_SIZEDEFAULT  = "QRCODE_SIZEDEFAULT";
    public static final String QRCODE_SIZEMINIMAL  = "QRCODE_SIZEMINIMAL";
    public static final String QRCODE_SIZELOGOHEIGHTPERCENTAGE   = "QRCODE_SIZELOGOHEIGHTPERCENTAGE";

    public static final String OCR_ALLOWEDIMAGEFORMAT  = "OCR_ALLOWEDIMAGEFORMAT";
    public static final String OCR_PREPROCESSISBINARYZE  = "OCR_PREPROCESSISBINARYZE";
    public static final String OCR_PREPROCESSISDESKEW  = "OCR_PREPROCESSISDESKEW";
    public static final String OCR_PREPROCESSISDENOISE  = "OCR_PREPROCESSISDENOISE";
    public static final String OCR_PREPROCESSISENHANCE  = "OCR_PREPROCESSISENHANCE";

    public static final String DATAMASTER_LOCATIONDBSPACKAGE  = "DATAMASTER_LOCATIONDBSPACKAGE";
    public static final String DATAMASTER_LOCATIONSERVICESPACKAGE  = "DATAMASTER_LOCATIONSERVICESPACKAGE";
    public static final String DATAMASTER_LOCATIONDTOPACKAGE  = "DATAMASTER_LOCATIONDTOPACKAGE";
    public static final String DATAMASTER_LISTTOIGNORETRAVERS  = "DATAMASTER_LISTTOIGNORETRAVERS";
    public static final String DATAMASTER_LISTTOPARENTHANDLE  = "DATAMASTER_LISTTOPARENTHANDLE";

    public static final String CONVERT_FILESDOCSERVICEVIEWEDDOCS  = "CONVERT_FILESDOCSERVICEVIEWEDDOCS";
    public static final String CONVERT_FILESDOCSERVICEEDITEDDOCS  = "CONVERT_FILESDOCSERVICEEDITEDDOCS";
    public static final String CONVERT_FILESDOCSERVICECONVERTDOCS  = "CONVERT_FILESDOCSERVICECONVERTDOCS";
    public static final String CONVERT_FILESDOCSERVICETIMEOUT  = "CONVERT_FILESDOCSERVICETIMEOUT";

    public static final String PLUGIN_SCANNING_TWAIN_KEY  = "PLUGIN_SCANNING_TWAIN_KEY";
    public static final String PLUGIN_SCANNING_TWAIN_IS_TRIAL  = "PLUGIN_SCANNING_TWAIN_IS_TRIAL";

    public static final String KODE_SURAT_DINAS  = "SD";
    public static final String KODE_NOTA_DINAS  = "ND";
    public static final String KODE_REGISTRASI_SURAT  = "SM";
    public static final String KODE_UNDANGAN  = "UD";

    public static final String STATUS_NAVIGASI_SIMPAN_OLEH_KONSEPTOR  = "SIMPAN_OLEH_KONSEPTOR";
    public static final String STATUS_NAVIGASI_KIRIM_OLEH_KONSEPTOR  = "KIRIM_OLEH_KONSEPTOR";
    public static final String STATUS_NAVIGASI_SIMPAN_OLEH_PEMERIKSA  = "SIMPAN_OLEH_PEMERIKSA";
    public static final String STATUS_NAVIGASI_KIRIM_OLEH_PEMERIKSA  = "KIRIM_OLEH_PEMERIKSA";
    public static final String STATUS_NAVIGASI_SIMPAN_OLEH_PENANDATANGAN  = "SIMPAN_OLEH_PENANDATANGAN";
    public static final String STATUS_NAVIGASI_KIRIM_OLEH_PENANDATANGAN  = "KIRIM_OLEH_PENANDATANGAN";
    public static final String STATUS_NAVIGASI_BATAL_OLEH_PEMERIKSA  = "BATAL_OLEH_PEMERIKSA";
    public static final String STATUS_NAVIGASI_BATAL_OLEH_PENANDATANGAN  = "BATAL_OLEH_PENANDATANGAN";
}
