package com.qtasnim.eoffice.bssn;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResultDto {
    private Long statusCode;
    private String status;
    private String message;
    private String error;
}
