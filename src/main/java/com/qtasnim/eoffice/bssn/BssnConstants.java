package com.qtasnim.eoffice.bssn;

public class BssnConstants {
    public static final String BSSN_BASE_URL = "BSSN_BASE_URL";
    public static final String BSSN_SIGN_URL = "BSSN_SIGN_URL";
    public static final String BSSN_DOWNLOAD_URL = "BSSN_DOWNLOAD_URL";
    public static final String BSSN_VERIFY_URL = "BSSN_VERIFY_URL";
    public static final String BSSN_USER_STATUS_URL = "BSSN_USER_STATUS_URL";
    public static final String BSSN_USER_REG_URL = "BSSN_USER_REG_URL";
    public static final String BSSN_AUTH_BEARER = "BSSN_AUTH_BEARER";
    public static final String BSSN_DEFAULT_NIK = "BSSN_DEFAULT_NIK";
    public static final String BSSN_DEFAULT_PASSPHRASE = "BSSN_DEFAULT_PASSPHRASE";
    public static final String BSSN_AUTH_USERNAME = "BSSN_AUTH_USERNAME";
    public static final String BSSN_AUTH_PASSWORD = "BSSN_AUTH_PASSWORD";
}
