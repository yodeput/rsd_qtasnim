package com.qtasnim.eoffice.bssn;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BeforeSigningEvent {
    private byte[] qrCode;
}
