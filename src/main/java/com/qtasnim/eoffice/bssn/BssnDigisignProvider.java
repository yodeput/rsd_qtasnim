package com.qtasnim.eoffice.bssn;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.MasterDigisign;
import com.qtasnim.eoffice.db.MasterDigisignConfig;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.ExceptionUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.media.multipart.Boundary;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

@Stateless
@LocalBean
public class BssnDigisignProvider implements IDigiSignService {
    @Inject
    private Logger logger;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private MasterDigisignService masterDigisignService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private QRGenerateService qrGenerateService;

    private String baseUrl = "http://172.16.12.243";
    private String signUrl = "/api/sign/pdf";
    private String downloadUrl = "/api/sign/download";
    private String verifyUrl = "/api/sign/verify";
    private String userStatusUrl = "/api/user/status";
    private String userRegUrl = "/api/user/registrasi";
    private String authBearer = "YWRtaW46cXdlcnR5";
    private String defaultNik;
    private String defaultPassphrase;
    private String username;
    private String password;
    private Consumer<Map<String, String>> onSigning = map -> {};
    private Consumer<BeforeSigningEvent> onBeforeSigning = event -> {};

    @PostConstruct
    private void init() {
        try {
            MasterDigisign masterDocLibProvider = masterDigisignService.getProvider(applicationContext.getApplicationConfig().getDigisignProvider());

            for (MasterDigisignConfig config : masterDocLibProvider.getConfig()) {
                switch (config.getName()) {
                    case BssnConstants.BSSN_BASE_URL: {
                        this.baseUrl = config.getValue();
                        break;
                    }
                    case BssnConstants.BSSN_SIGN_URL: {
                        this.signUrl = config.getValue();
                        break;
                    }
                    case BssnConstants.BSSN_DOWNLOAD_URL: {
                        this.downloadUrl = config.getValue();
                        break;
                    }
                    case BssnConstants.BSSN_VERIFY_URL: {
                        this.verifyUrl = config.getValue();
                        break;
                    }
                    case BssnConstants.BSSN_USER_STATUS_URL: {
                        this.userStatusUrl = config.getValue();
                        break;
                    }
                    case BssnConstants.BSSN_USER_REG_URL: {
                        this.userRegUrl = config.getValue();
                        break;
                    }
                    case BssnConstants.BSSN_AUTH_BEARER: {
                        this.authBearer = config.getValue();
                        break;
                    }
                    case BssnConstants.BSSN_DEFAULT_NIK: {
                        this.defaultNik = config.getValue();
                        break;
                    }
                    case BssnConstants.BSSN_DEFAULT_PASSPHRASE: {
                        this.defaultPassphrase = config.getValue();
                        break;
                    }
                    case BssnConstants.BSSN_AUTH_USERNAME: {
                        this.username = config.getValue();
                        break;
                    }
                    case BssnConstants.BSSN_AUTH_PASSWORD: {
                        this.password = config.getValue();
                        break;
                    }
                }
            }
        } catch (BSSNProviderNotFoundException e) {
            logger.error(null, e);
        }
    }

    @Override
    public byte[] sign(MasterStrukturOrganisasi strukturOrganisasi, byte[] pdfByte) {
        byte[] result = pdfByte;
        String nik = "";
        String passphrase = "";

        try {
            BeforeSigningEvent event = new BeforeSigningEvent();
            onBeforeSigning.accept(event);

            if (event.getQrCode() != null) {
                pdfByte = qrGenerateService.embedQRCodeToPdf(pdfByte, event.getQrCode(), new Point(450, 660));
            }

            HttpAuthenticationFeature auth = HttpAuthenticationFeature.basic(this.username, this.password);
            Client client = getClient();
            client.property(ClientProperties.CONNECT_TIMEOUT, 20000);
            client.property(ClientProperties.READ_TIMEOUT,    20000);
            client.register(auth);
            WebTarget webTarget = client.target(UriBuilder.fromUri(baseUrl)
                    .path(signUrl)
                    .toString());

            StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart();
            streamDataBodyPart.setName("file");

            try (InputStream streamEntity = new ByteArrayInputStream(pdfByte)) {
                streamDataBodyPart.setStreamEntity(streamEntity, MediaType.valueOf("application/pdf"));
            }

            Map<String, String> vars = new HashMap<>();
            onSigning.accept(vars);

            nik = Optional
                    .ofNullable(defaultNik)
                    .filter(StringUtils::isNotEmpty)
                    .orElse(vars.entrySet()
                            .stream()
                            .filter(t -> t.getKey().equals("nik"))
                            .map(Map.Entry::getValue)
                            .filter(Objects::nonNull)
                            .findFirst()
                            .orElse(""));
            passphrase = Optional
                    .ofNullable(defaultPassphrase)
                    .filter(StringUtils::isNotEmpty)
                    .orElse(vars.entrySet()
                            .stream()
                            .filter(t -> t.getKey().equals("passphrase"))
                            .map(Map.Entry::getValue)
                            .filter(Objects::nonNull)
                            .findFirst()
                            .orElse(""));

            try (FormDataMultiPart formDataMultiPart = new FormDataMultiPart()) {
                try (FormDataMultiPart part = (FormDataMultiPart) formDataMultiPart
                        .field("nik", nik)
                        .field("passphrase", passphrase)
                        .bodyPart(streamDataBodyPart)) {

                    vars.forEach((key, value) -> {
                        if (part.getFields().entrySet().stream().noneMatch(t -> t.getKey().equals(key))) {
                            part.field(key, value);
                        }
                    });

                    MediaType contentType = part.getMediaType();
                    contentType = Boundary.addBoundary(contentType);

                    Response response = webTarget
                            .request(MediaType.WILDCARD)
//                            .header("Authorization", String.format("Basic %s", authBearer))
                            .post(Entity.entity(part, contentType));

                    if (response.getStatus() != 200) {
                        ObjectMapper objectMapper = getObjectMapper();
                        String stringEntity = response.readEntity(String.class);
                        String errorMessage = "Unknown";

                        if (StringUtils.isNotEmpty(stringEntity)) {
                            CommonResultDto commonResultDto = objectMapper.readValue(stringEntity, CommonResultDto.class);
                            errorMessage = getMessage(commonResultDto);
                        }

                        throw new BSSNSignFailException(errorMessage);
                    } else if (response.getStatus() == 200) {
                        try (InputStream is = response.readEntity(InputStream.class)) {
                            result = IOUtils.toByteArray(is);
                        }
                    }
                }
            }

            return result;
        } catch (Exception e) {
            Throwable realCause = ExceptionUtil.getRealCause(e);
            logger.error(realCause.getMessage(), realCause);
            throw new BSSNSignFailException(String.format("BSSN Failed pdf sign: nik = %s, message: %s", nik, realCause.getMessage()), null);
        }
    }

    @Override
    public void validate(MasterStrukturOrganisasi strukturOrganisasi, String passphrase) {
        try {
            try (InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("minimal.pdf")) {
                onSigning(map -> {
                    map.put("tampilan", "invisible");
                    map.put("passphrase", passphrase);
                    map.put("nik", strukturOrganisasi.getUser().getLoginUserName());
                }).sign(strukturOrganisasi, IOUtils.toByteArray(stream));
            }
        } catch (IOException e) {
            Throwable realCause = ExceptionUtil.getRealCause(e);
            logger.error(realCause.getMessage(), realCause);
            throw new BSSNSignFailException(realCause.getMessage());
        } catch (Exception e) {
            throw new BSSNSignFailException(null, e);
        }
    }

    @Override
    public BssnDigisignProvider onSigning(Consumer<Map<String, String>> onSigning) {
        this.onSigning = onSigning;
        return this;
    }

    @Override
    public IDigiSignService onBeforeSigning(Consumer<BeforeSigningEvent> onBeforeSigning) {
        this.onBeforeSigning = onBeforeSigning;
        return this;
    }

    @Override
    public Boolean isRequirePassphrase() {
        return Optional
                .ofNullable(defaultPassphrase)
                .map(StringUtils::isEmpty)
                .orElse(true);
    }

    public boolean checkUserStatus(String userId) {
        try {
            ObjectMapper objectMapper = getObjectMapper();
            Client client = getClient();
            HttpAuthenticationFeature auth = HttpAuthenticationFeature.basic(this.username, this.password);
            client.register(auth);
            WebTarget webResource = client.target(UriBuilder.fromUri(baseUrl)
                    .path(userStatusUrl)
                    .path(userId)
                    .toString());
            Response response = webResource
                    .request()
//                    .header("Authorization", String.format("Bearer %s", authBearer))
                    .accept(MediaType.APPLICATION_JSON_TYPE)
                    .get();

            String stringEntity = response.readEntity(String.class);
            CommonResultDto commonResultDto = objectMapper.readValue(stringEntity, CommonResultDto.class);
            Long resultCode = null;
            String message = null;
            boolean isValid = false;

            if (commonResultDto != null) {
                resultCode = commonResultDto.getStatusCode();
                message = getMessage(commonResultDto);
                isValid = new Long(1111L).equals(resultCode);
            }

            if (resultCode != null && !isValid && StringUtils.isNotEmpty(message)) {
                notificationService.sendError("BSSN Error", message);
            }

            return isValid;
        } catch (Exception e) {
            Throwable realCause = ExceptionUtil.getRealCause(e);
            logger.error(realCause.getMessage(), realCause);
            notificationService.sendError("BSSN Error", e.getMessage());
        }

        return false;
    }

    private Client getClient() {
        final ClientConfig clientConfig = new ClientConfig()
                .register(MultiPartFeature.class);

        return ClientBuilder.newClient(clientConfig);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    private String getMessage(CommonResultDto commonResultDto) {
        return Optional.ofNullable(commonResultDto.getError())
                .filter(StringUtils::isNotEmpty)
                .map(t -> Optional.of(t).filter(s -> s.contains(":")).map(s -> s.substring(s.indexOf(":"))).orElse(commonResultDto.getMessage()))
                .orElse(commonResultDto.getMessage());
    }
}
