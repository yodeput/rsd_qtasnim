package com.qtasnim.eoffice.bssn;

import com.qtasnim.eoffice.InternalServerErrorException;

public class BSSNProviderNotFoundException extends InternalServerErrorException {
    public BSSNProviderNotFoundException() {
        super("BSSNProviderNotFoundException");
    }

    public BSSNProviderNotFoundException(String message, Throwable ex) {
        super(message, ex);
    }
}
