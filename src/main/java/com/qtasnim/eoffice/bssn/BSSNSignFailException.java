package com.qtasnim.eoffice.bssn;

import com.qtasnim.eoffice.InternalServerErrorException;

public class BSSNSignFailException extends InternalServerErrorException {
    public BSSNSignFailException(String message) {
        super(message);
    }

    public BSSNSignFailException(String message, Throwable ex) {
        super(message, ex);
    }
}
