package com.qtasnim.eoffice.servlets;

import com.qtasnim.eoffice.caching.FileCache;
import com.qtasnim.eoffice.cmis.CMISDocument;
import com.qtasnim.eoffice.cmis.ICMISProvider;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.context.RequestContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.IRequestContext;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.JenisDokumenDocLibDto;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.qtasnim.eoffice.services.Logger;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.util.Optional;
import java.util.Scanner;

@WebServlet(name = "UploadServlet", urlPatterns = {"/Upload"})
public class UploadServlet extends HttpServlet {
    @Inject
    private ICMISProvider icmisProvider;

    @Inject
    private Logger logger;

    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private UserSessionService userSessionService;

    @Inject
    private DocLibHistoryService docLibHistoryService;

    @Inject
    private JenisDokumenDocLibService jenisDokumenDocLibService;

    @Inject
    private JWTService jwtService;

    @Inject
    private DocActivityNotificationService docActivityNotificationService;

    @Inject
    private FileCache fileCache;

    @Inject
    @ISessionContext
    private Event<UserSession> userAuthenticatedEvent;

    @Inject
    @IRequestContext
    private Event<RequestContext> requestContextEvent;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        PrintWriter writer = response.getWriter();
        String id = request.getParameter("id");
        String rdsToken = request.getParameter("token");
        String idJenisDokumenString = request.getParameter("idJenisDokumen");

        if (id == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        String body;

        UserSession session = userSessionService.findByToken(rdsToken);
        if (session == null) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        userAuthenticatedEvent.fire(session);
        requestContextEvent.fire(new RequestContext(null, request.getRemoteAddr()));

        try
        {
            Scanner scanner = new Scanner(request.getInputStream());
            scanner.useDelimiter("\\A");
            body = scanner.hasNext() ? scanner.next() : "";
            scanner.close();
        }
        catch (Exception ex)
        {
            logger.error("get request.getInputStream error:" + ex.getMessage(), ex);
            writer.write("get request.getInputStream error:" + ex.getMessage());
            return;
        }

        if (body.isEmpty())
        {
            logger.error("empty request.getInputStream");
            writer.write("empty request.getInputStream");
            return;
        }

        System.out.println("########");
        System.out.println(body);

        JSONParser parser = new JSONParser();
        JSONObject jsonObj;

        try
        {
            Object obj = parser.parse(body);
            jsonObj = (JSONObject) obj;
        }
        catch (Exception ex)
        {
            logger.error("JSONParser.parse error:" + ex.getMessage(), ex);
            writer.write("JSONParser.parse error:" + ex.getMessage());
            return;
        }

        int status;
        String downloadUri;

        if (StringUtils.isNotEmpty(applicationConfig.getOnlyOfficeSecret()))
        {
            String token = (String) jsonObj.get("token");

            Claims jwtClaims = jwtService.readToken(applicationConfig.getOnlyOfficeSecret(), token);
            if (jwtClaims == null)
            {
                logger.error("JWT.parse error");
                writer.write("JWT.parse error");
                return;
            }

            status = jwtClaims.get("status", Integer.class);
            downloadUri = jwtClaims.get("url", String.class);
        }
        else
        {
            status = Integer.parseInt(jsonObj.get("status").toString());
            downloadUri = (String) jsonObj.get("url");
        }

        int saved = 0;
        if (status == 2 || status == 3 || status == 6 || status == 7) //MustSave, Corrupted, ForceSave
        {
            try
            {
                URL url = new URL(downloadUri);
                java.net.HttpURLConnection connection = (java.net.HttpURLConnection) url.openConnection();
                InputStream stream = connection.getInputStream();

                if (stream == null)
                {
                    throw new Exception("Stream is null");
                }

                byte[] content;

                try (ByteArrayOutputStream out = new ByteArrayOutputStream())
                {
                    int read;
                    final byte[] bytes = new byte[1024];
                    while ((read = stream.read(bytes)) != -1)
                    {
                        out.write(bytes, 0, read);
                    }

                    content = out.toByteArray();
                    out.flush();
                }

                connection.disconnect();

                icmisProvider.openConnection();
                CMISDocument cmisDocument = icmisProvider.updateDocument(id, content);
                JenisDokumenDocLib first = jenisDokumenDocLibService.findFirst(id);

                if (StringUtils.isNotEmpty(idJenisDokumenString) && StringUtils.isNotEmpty(rdsToken) && first != null) {
                    long idJenisDokumen = Long.parseLong(idJenisDokumenString);
                    JenisDokumenDocLib activated = jenisDokumenDocLibService.getLatest(idJenisDokumen);
                    JenisDokumenDocLibDto dto = new JenisDokumenDocLibDto();
                    dto.setDocId(id);
                    dto.setSize(cmisDocument.getSize());
                    dto.setCmisVersion(cmisDocument.getVersion());
                    dto.setIdJenisDokumen(idJenisDokumen);
                    dto.setDocGeneratedName(first.getDocGeneratedName());
                    dto.setDocName(first.getDocName());
                    dto.setMimeType(first.getMimeType());
                    dto.setIdCompany(first.getCompanyCode().getId());
                    dto.setNumber(first.getNumber());

                    double nextVersion = 0.1;

                    if (activated != null) {
                        String versionString = activated.getVersion().toString();
                        Integer leftPart = Integer.valueOf(versionString.substring(0, versionString.lastIndexOf(".")));
                        Integer rightPart = Integer.valueOf(versionString.substring(versionString.lastIndexOf(".") + 1));

                        rightPart++;

                        nextVersion = Double.valueOf(String.format("%s.%s", leftPart, rightPart));
                    }

                    JenisDokumenDocLib jenisDokumenDocLib = jenisDokumenDocLibService.saveOrSubmit(idJenisDokumen, dto, nextVersion);

                    docActivityNotificationService.jenisDokumentDocLibSaved(idJenisDokumenString, jenisDokumenDocLib);
                } else if (StringUtils.isEmpty(idJenisDokumenString) && StringUtils.isNotEmpty(rdsToken)) {
                    DocLibHistory docLibHistory = new DocLibHistory();
                    docLibHistory.setDocId(id);
                    docLibHistory.setSize(Optional.ofNullable(cmisDocument.getSize()).orElse(-1L));
                    docLibHistory.setVersion(cmisDocument.getVersion());
                    docLibHistory.setUser(session.getUser());

                    docLibHistoryService.create(docLibHistory);
                }
            }
            catch (Exception ex)
            {
                logger.error("Error save file", ex);
                saved = 1;
            } finally {
                icmisProvider.closeConnection();
            }
        }

        writer.write("{\"error\":" + saved + "}");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String getServletInfo()
    {
        return "Download page";
    }
}
