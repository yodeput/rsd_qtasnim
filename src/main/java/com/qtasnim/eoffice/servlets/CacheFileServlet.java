package com.qtasnim.eoffice.servlets;

import com.qtasnim.eoffice.caching.FileCache;
import com.qtasnim.eoffice.cmis.CMISDocument;
import com.qtasnim.eoffice.cmis.ICMISProvider;
import com.qtasnim.eoffice.services.Logger;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "FileCacheServlet", urlPatterns = {"/FileCache"})
public class CacheFileServlet extends HttpServlet {
    @Inject
    private Logger logger;

    @Inject
    private FileCache fileCache;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String id = request.getParameter("id");
        String name = request.getParameter("name");

        if (StringUtils.isEmpty(id) || StringUtils.isEmpty(name)) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        try {
            response.reset();
            response.setHeader("Content-Disposition", String.format("attachment;filename=\"%s\"", name));

            byte[] documentContent = fileCache.get(id);

            ServletOutputStream out = response.getOutputStream();
            out.write(documentContent);
            out.flush();
        } catch (Exception e) {
            logger.error(null, e);
            response.resetBuffer();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo()
    {
        return "Download page";
    }
}
