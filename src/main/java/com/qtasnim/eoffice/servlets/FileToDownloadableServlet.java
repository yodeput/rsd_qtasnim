/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.servlets;

import com.qtasnim.eoffice.config.ApplicationConfig;

import java.io.*;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author acdwisu
 */
@WebServlet("/filedownload")
public class FileToDownloadableServlet extends HttpServlet {
    private final int ARBITARY_SIZE = 1048;

    @Inject
    private ApplicationConfig applicationConfig;
 
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String fileName = req.getParameter("fileName");
        String fileType = fileName.split("\\.")[1].toLowerCase();
        
        String contentType;
        
        switch(fileType) {
            case "docx":
                contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                break;
            case "pdf":
                contentType = "application/pdf";
                break;
            case "txt":
                contentType = "text/plain";
                break;
            case "png":
                contentType = "image/png";
                break;
            case "jpg":
                contentType = "image/jpg";
                break;
            default:
                contentType = "text/plain";
                break;
        }
        
        resp.setContentType(contentType);
        resp.setHeader("Content-disposition", "attachment; filename="+fileName);

        try(InputStream in = new FileInputStream(new File(applicationConfig.getTempDir(), fileName));
            OutputStream out = resp.getOutputStream()) {

            byte[] buffer = new byte[ARBITARY_SIZE];

            int numBytesRead;
            while ((numBytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, numBytesRead);
            }
        }
    }
}
