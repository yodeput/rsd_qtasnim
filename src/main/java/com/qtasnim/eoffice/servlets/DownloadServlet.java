package com.qtasnim.eoffice.servlets;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.cmis.CMISDocument;
import com.qtasnim.eoffice.cmis.ICMISProvider;
import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.services.GlobalAttachmentService;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.services.MasterMimeMappingService;
import com.qtasnim.eoffice.ws.dto.GlobalAttachmentDto;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DownloadServlet", urlPatterns = {"/Download"})
public class DownloadServlet extends HttpServlet {
    @Inject
    private ICMISProvider icmisProvider;

    @Inject
    private Logger logger;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private MasterMimeMappingService mimeMappingService;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String id = request.getParameter("id");
        String version = request.getParameter("version");

        if (id == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        try {
            icmisProvider.openConnection();

            CMISDocument document = icmisProvider.getDocument(icmisProvider.resolveDocId(id, version));

            response.reset();
            response.setContentType(document.getMime());
            response.setHeader("Content-Disposition", String.format("attachment;filename=\"%s\"", document.getName()));

            GlobalAttachmentDto file = globalAttachmentService.findByDocId(id);
            if(file != null && !Strings.isNullOrEmpty(file.getIdDocument())) {
                String name = file.getIdDocument() + "." + mimeMappingService.getExtensionBy(document.getMime());
                response.setHeader("Content-Disposition", String.format("attachment;filename=\"%s\"", name));
            }

            byte[] documentContent = icmisProvider.getDocumentContent(id, version);
            ServletOutputStream out = response.getOutputStream();
            out.write(documentContent);
            out.flush();
        } catch (Exception e) {
            logger.error(null, e);
            response.resetBuffer();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } finally {
            icmisProvider.closeConnection();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo()
    {
        return "Download page";
    }
}
