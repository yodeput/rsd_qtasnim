package com.qtasnim.eoffice.servlets;

import com.qtasnim.eoffice.cmis.CMISDocument;
import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.cmis.ICMISProvider;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.JenisDokumenDocLib;
import com.qtasnim.eoffice.db.UserSession;
import com.qtasnim.eoffice.office.FileModelFactory;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.office.OnlyOfficeService;
import com.qtasnim.eoffice.services.JenisDokumenDocLibService;
import com.qtasnim.eoffice.services.UserSessionService;
import com.qtasnim.eoffice.servlets.demo.helpers.DocumentManager;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LayoutingServlet", urlPatterns = {"/Layouting"})
public class LayoutingServlet extends HttpServlet {
    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private OnlyOfficeService onlyOfficeService;

    @Inject
    private JenisDokumenDocLibService jenisDokumenDocLibService;

    @Inject
    private UserSessionService userSessionService;

    @Inject
    private ICMISProvider icmisProvider;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DocumentManager.Init(request, response);

        String mode = request.getParameter("mode");
        String idJenisDokumen = request.getParameter("idJenisDokumen");
        String token = request.getParameter("token");
        String version = request.getParameter("version");
        String id;

        if (StringUtils.isNotBlank(idJenisDokumen)) {
            JenisDokumenDocLib lastest = jenisDokumenDocLibService.getLatest(Long.parseLong(idJenisDokumen));
            id = lastest.getDocId();
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        if ("edit".equals(mode)) {
            UserSession session = userSessionService.findByToken(token);

            if (session == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }

        try {
            icmisProvider.openConnection();
            CMISDocument document = icmisProvider.getDocument(id);
            id = id + FileUtility.GetFileExtension(document.getName());

            if (StringUtils.isEmpty(version)) {
                version = document.getVersion();
            }
        } catch (CMISProviderException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        } finally {
            icmisProvider.closeConnection();
        }

        FileModelFactory fileModelFactory = onlyOfficeService
                .getConfigFactory()
                .init(id, version, mode, applicationConfig, token, idJenisDokumen);
        if ("embedded".equals(mode))
            fileModelFactory.initDesktop();
        if ("view".equals(mode))
            fileModelFactory.setMode("view");

        request.setAttribute("fileModelFactory", fileModelFactory);
        request.setAttribute("docserviceApiUrl", applicationConfig.getOnlyOfficeUrlApi());
        request.setAttribute("originOnlyoffice", applicationConfig.getOnlyofficeOrigin());
        request.setAttribute("apiPath", applicationConfig.getOnlyOfficePluginLayoutingKeyApiPath());
        request.setAttribute("autoStartPlugins", applicationConfig.getOnlyOfficeAutoStartPlugins());
        if(StringUtils.isNotBlank(idJenisDokumen)) request.setAttribute("idJenisDokumen", idJenisDokumen);

        request.getRequestDispatcher("document.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo()
    {
        return "Document page";
    }
}
