/*
 *
 * (c) Copyright Ascensio System SIA 2019
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
*/


package com.qtasnim.eoffice.servlets.demo;

import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.servlets.demo.helpers.ConfigManager;
import com.qtasnim.eoffice.servlets.demo.helpers.DocumentManager;
import com.qtasnim.eoffice.servlets.demo.dao.FileModel;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "EditorServlet", urlPatterns = {"/viewers/EditorServlet"})
public class EditorServlet extends HttpServlet
{

    @Inject
    private ApplicationConfig applicationConfig;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        DocumentManager.Init(request, response);

        String fileName = request.getParameter("fileName");
        String fileExt = request.getParameter("fileExt");
        String idJenisDokumen = request.getParameter("idJenisDokumen");

        if (fileExt != null)
        {
            try
            {
                fileName = DocumentManager.CreateDemo(fileExt);
            }
            catch (Exception ex)
            {
                response.getWriter().write("Error: " + ex.getMessage());    
            }
        }

        FileModel file = new FileModel(fileName);
        if ("embedded".equals(request.getParameter("mode")))
            file.InitDesktop();
        if ("view".equals(request.getParameter("mode")))
            file.editorConfig.mode = "view";

        if (DocumentManager.TokenEnabled())
        {
            file.BuildToken();
        }

        request.setAttribute("file", file);
        request.setAttribute("docserviceApiUrl", applicationConfig.getOnlyOfficeUrlApi());
        request.setAttribute("originOnlyoffice", applicationConfig.getOnlyofficeOrigin());// ConfigManager.GetProperty("onlyoffice.origin"));
        if(idJenisDokumen!=null) request.setAttribute("idJenisDokumen", idJenisDokumen);

        request.getRequestDispatcher("editor.jsp").forward(request, response);

        response.setHeader("Access-Control-Allow-Origin", applicationConfig.getOnlyofficeOrigin());//ConfigManager.GetProperty("onlyoffice.container.origin"));
        response.setHeader("Access-Control-Allow-Methods", "GET");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo()
    {
        return "Editor page";
    }
}
