package com.qtasnim.eoffice;

public class RequirePassphraseException extends InternalServerErrorException {
    public RequirePassphraseException() {
        super(null);
    }

    public RequirePassphraseException(String message, Throwable ex) {
        super(message, ex);
    }
}