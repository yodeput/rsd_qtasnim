package com.qtasnim.eoffice.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.qtasnim.eoffice.db.MasterEmailContent;

import org.jinq.jpa.JPAJinqStream;

@Stateless
@LocalBean
public class MasterEmailContentService extends AbstractFacade<MasterEmailContent> {
    private static final long serialVersionUID = 1L;

    @Override
    protected Class<MasterEmailContent> getEntityClass() {
        return MasterEmailContent.class;
    }

    public JPAJinqStream<MasterEmailContent> getAll() {return db();
    }

}