package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterModul;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterModulDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class MasterModulService extends AbstractFacade<MasterModul> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterModul> getEntityClass() {
        return MasterModul.class;
    }

    public JPAJinqStream<MasterModul> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterModul>getFiltered(String tipe,String nama,Boolean isActive,String url,String desc,String
            translate,Long parent){
        JPAJinqStream<MasterModul> query = db();

        if(!Strings.isNullOrEmpty(tipe)){
            query = query.where(q ->q.getTipe().toLowerCase().contains(tipe.toLowerCase()));
        }

        if(!Strings.isNullOrEmpty(nama)){
            query = query.where(q ->q.getNamaModul().toLowerCase().contains(nama.toLowerCase()));
        }

        if (isActive!=null) {
            if(isActive) {
                query = query.where(q -> q.getIsActive());
            }else{
                query = query.where(q -> !q.getIsActive());
            }
        }

        if(!Strings.isNullOrEmpty(url)){
            query = query.where(q ->q.getUrl().toLowerCase().contains(url.toLowerCase()));
        }

        if(!Strings.isNullOrEmpty(desc)){
            query = query.where(q ->q.getDescription().toLowerCase().contains(desc.toLowerCase()));
        }

        if(!Strings.isNullOrEmpty(translate)){
            query = query.where(q ->q.getTranslate().toLowerCase().contains(translate.toLowerCase()));
        }

        if(parent!=null){
            query = query.where(q->q.getParent().getIdModul()==parent);
        }

        return query;
    }

    public void saveOrEdit(MasterModulDto dao, Long id){
        boolean isNew =id==null;
        MasterModul model = new MasterModul();
        DateUtil dateUtil = new DateUtil();

        if(isNew) {
            model.setNamaModul(dao.getNamaModul());
            model.setDescription(dao.getDescription());
            model.setUrl(dao.getUrl());
            model.setTranslate(dao.getTranslate());
            model.setTipe(dao.getTipe());
            model.setIsActive(dao.getIsActive());
            if(dao.getIdParent()!=null) {
                MasterModul parent = this.find(dao.getIdParent());
                model.setParent(parent);
            }else {
                model.setParent(null);
            }

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setKeyModul(dao.getKeyModul());
            model.setIconClass(dao.getIconClass());
            model.setSeq(dao.getSeq());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.create(model);
        }else{
            model = this.find(id);
            model.setNamaModul(dao.getNamaModul());
            model.setDescription(dao.getDescription());
            model.setUrl(dao.getUrl());
            model.setTranslate(dao.getTranslate());
            model.setTipe(dao.getTipe());
            model.setIsActive(dao.getIsActive());

            if(dao.getIdParent()!=null) {
                MasterModul parent = this.find(dao.getIdParent());
                model.setParent(parent);
            }else {
                model.setParent(null);
            }

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            if(!Strings.isNullOrEmpty(dao.getKeyModul())) {
                model.setKeyModul(dao.getKeyModul());
            }
            if(!Strings.isNullOrEmpty(dao.getKeyModul())) {
                model.setIconClass(dao.getIconClass());
            }
            if(dao.getSeq() != null) {
                model.setSeq(dao.getSeq());
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }

    }

    public JPAJinqStream<MasterModul> getByParent(Long parent,String sort,Boolean descending){
        Date n = new Date();
        JPAJinqStream<MasterModul> query = db().where(q->n.after(q.getStart()) && n.before(q.getEnd()));
        if(parent==null){
            query = query.where(q->q.getParent().equals(null));
        }else{
            query = query.where(q->q.getParent().getIdModul().equals(parent));
        }

        if (!Strings.isNullOrEmpty(sort)) {
            if (descending) {
                if (sort.toLowerCase().equals("namamodul")) {
                    query = query.sortedDescendingBy(q->q.getNamaModul());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedDescendingBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedDescendingBy(q -> q.getEnd());
                }
            } else {
                if (sort.toLowerCase().equals("namamodul")) {
                    query = query.sortedBy(q -> q.getNamaModul());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedBy(q -> q.getEnd());
                }
            }
        }
        return query;
    }
}
