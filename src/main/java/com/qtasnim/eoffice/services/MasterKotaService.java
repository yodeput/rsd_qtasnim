package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterArea;
import com.qtasnim.eoffice.db.MasterKota;
import com.qtasnim.eoffice.db.MasterPropinsi;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterKotaDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class MasterKotaService extends AbstractFacade<MasterKota> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterPropinsiService propinsiService;

    @Override
    protected Class<MasterKota> getEntityClass() {
        return MasterKota.class;
    }

    public JPAJinqStream<MasterKota> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterKota>getFiltered(String kode,String nama,Boolean isActive){
        JPAJinqStream<MasterKota> query = db();

        if(!Strings.isNullOrEmpty(kode)){
            query = query.where(q ->q.getKodeKota().toLowerCase().contains(kode.toLowerCase()));
        }

        if(!Strings.isNullOrEmpty(nama)){
            query = query.where(q ->q.getNamaKota().toLowerCase().contains(nama.toLowerCase()));
        }

        if (isActive!=null) {
            if(isActive) {
                query = query.where(q -> q.getIsActive());
            }else{
                query = query.where(q -> !q.getIsActive());
            }
        }

        return query;
    }

    public void saveOrEdit(MasterKotaDto dao, Long id){
        boolean isNew =id==null;
        MasterKota model = new MasterKota();
        DateUtil dateUtil = new DateUtil();

        if(isNew){
            model.setKodeKota(dao.getKodeKota());
            model.setNamaKota(dao.getNamaKota());
            model.setIsActive(dao.getIsActive());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            if(dao.getIdPropinsi()!=null){
                MasterPropinsi prov = propinsiService.find(dao.getIdPropinsi());
                model.setPropinsi(prov);
            }else{
                model.setPropinsi(null);
            }
            this.create(model);
        }else{
            model = this.find(id);
            model.setKodeKota(dao.getKodeKota());
            model.setNamaKota(dao.getNamaKota());
            model.setIsActive(dao.getIsActive());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }

            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            if(dao.getIdPropinsi()!=null){
                MasterPropinsi prov = propinsiService.find(dao.getIdPropinsi());
                model.setPropinsi(prov);
            }else{
                model.setPropinsi(null);
            }
            this.edit(model);
        }
    }
}
