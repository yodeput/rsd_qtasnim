package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.SharedDocLibVendor;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@LocalBean
public class SharedDocLibVendorService extends AbstractFacade<SharedDocLibVendor> {
    @Override
    protected Class<SharedDocLibVendor> getEntityClass() {
        return SharedDocLibVendor.class;
    }

    public List<SharedDocLibVendor> getBySuratAndReaded(Long idSurat) {
        return db().where(t -> t.getSurat().getId().equals(idSurat) && t.getReadDate() != null).toList();
    }
}
