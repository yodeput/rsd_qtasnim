package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Stateless
@LocalBean
public class RegulasiService extends AbstractFacade<Regulasi> {
    private static final long serialVersionUID = 1L;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    CompanyCodeService companyCodeService;

    @Inject
    private MasterKlasifikasiMasalahService klasifikasiMasalahService;


    @Inject
    private MasterKorsaService korsaService;

    @Inject
    private FileService fileService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private MasterJenisRegulasiService jenisRegulasiService;

    @Inject
    private RegulasiPenggantiService penggantiService;

    @Inject
    private RegulasiHistoryService historyService;

    @Override
    protected Class<Regulasi> getEntityClass() {
        return Regulasi.class;
    }

    public JPAJinqStream<Regulasi> getFiltered(String name) {
        return db().where(q -> q.getNamaRegulasi().toLowerCase().contains(name));
    }

    public List<Regulasi> getByDate(String start, String end, String filter, String sort, boolean descending, int skip, int limit){
        DateUtil dateUtil = new DateUtil();
        Date startKurun = dateUtil.getDateFromISOString(start);
        Date endKurun = dateUtil.getDateFromISOString(end);
        List<Regulasi> listFilter = this.getResultList(filter, sort, descending, skip, limit);
        List<Regulasi> listFilterByDate = db().where(q -> !q.getIsDeleted() && q.getTanggalMulaiBerlaku().after(startKurun) && q.getTanggalSelesaiBerlaku().before(endKurun)).toList();
        listFilter.retainAll(listFilterByDate);
        return listFilter;
    }

    public Regulasi save(Long id, RegulasiDto dao){
        Regulasi model = new Regulasi();
        if (id==null){
            model = data(model, dao);
            this.create(model);
            getEntityManager().flush();
            historyService.save(new RegulasiHistoryDto("CREATE", new RegulasiDto(model), new CompanyCodeDto(model.getCompanyCode())));
        } else {
            model = this.find(id);
            model = data(model, dao);
            this.edit(model);
            createHistory(model);
        }

        return  model;
    }

    private Regulasi data(Regulasi regulasi, RegulasiDto dao){
        Regulasi model = regulasi;
        DateUtil dateUtil = new DateUtil();

        model.setNamaRegulasi(dao.getNamaRegulasi());
        model.setNomorRegulasi(dao.getNomorRegulasi());
        model.setDeskripsi(dao.getDeskripsi());
        model.setStatus(dao.getStatus());
        model.setIsDeleted(dao.getIsDeleted());
        model.setIsPublished(dao.getIsPublished());
        model.setIsRegulasiPerusahaan(dao.getIsRegulasiPerusahaan());
        model.setTanggalTerbit(dateUtil.getDateTimeStart(dao.getTanggalTerbit()));
        model.setTanggalSelesaiBerlaku(dateUtil.getDateTimeEnd(dao.getTanggalTerbit()));
        model.setTanggalMulaiBerlaku(dateUtil.getDateTimeStart(dao.getTanggalMulaiBerlaku()));
        model.setIsStarred(dao.getIsStarred());

//        if(dao.getJenisRegulasiId()!=null){
//            MasterJenisRegulasi object = jenisRegulasiService.find(dao.getJenisRegulasiId());
//            model.setJenisRegulasi(object);
//        } else {
//            model.setJenisRegulasi(null);
//        }

        if(dao.getJenisRegulasi()!=null){
            MasterUser user = userSession.getUserSession().getUser();
            MasterJenisRegulasi object = jenisRegulasiService.find(dao.getJenisRegulasi().getIdJenisRegulasi());
            model.setJenisRegulasi(object);
            model.setCompanyCode(object.getCompanyCode()); //cara 1 buat ngambil company code dari master jenis regulasi
//            model.setCompanyCode(user.getCompanyCode()); //cara 2 buat ngambil company code dari master jenis regulasi
        } else {
            model.setJenisRegulasi(null);
        }

//        if(dao.getCompanyId()!=null){
//            CompanyCode object = companyCodeService.find(dao.getCompanyId());
//            model.setCompanyCode(object);
//        } else {
//            model.setCompanyCode(null);
//        }

        if(dao.getKlasifikasiDokumenId()!=null){
//          if(dao.getKlasifikasiDokumen()!=null){
//            MasterKlasifikasiMasalah object = klasifikasiMasalahService.find(dao.getKlasifikasiDokumen().getIdKlasifikasiMasalah());
            MasterKlasifikasiMasalah object = klasifikasiMasalahService.find(dao.getKlasifikasiDokumenId());
            model.setKlasifikasiDokumen(object);
        } else {
            model.setKlasifikasiDokumen(null);
        }

        if(dao.getRegulasiPenggantiId()!=null){
            Regulasi object = this.find(dao.getRegulasiPenggantiId());
            model.setRegulasiPengganti(object);
            if(model.getId()!=null){
                penggantiService.createOrEdit(model.getId(),dao.getRegulasiPenggantiId(), dao.getCompanyId());
            }
        } else {
            model.setRegulasiPengganti(null);
        }

        if(dao.getUnitKonseptorId()!=null){
            MasterKorsa object = korsaService.find(dao.getUnitKonseptorId());
            model.setUnitKonseptor(object);
        } else {
            model.setUnitKonseptor(null);
        }

        return model;
    }

    private void createHistory(Regulasi obj){
        String status = obj.getStatus();
        historyService.save(new RegulasiHistoryDto("EDIT", new RegulasiDto(obj), new CompanyCodeDto(obj.getCompanyCode())));
        switch (status){
            case "BERLAKU":
                historyService.save(new RegulasiHistoryDto("PUBLISH", new RegulasiDto(obj), new CompanyCodeDto(obj.getCompanyCode())));
                break;
            case "TIDAK BERLAKU":
                historyService.save(new RegulasiHistoryDto("UNPUBLISH", new RegulasiDto(obj), new CompanyCodeDto(obj.getCompanyCode())));
                break;
            case "TELAH DIUBAH":
                historyService.save(new RegulasiHistoryDto("EDIT", new RegulasiDto(obj), new CompanyCodeDto(obj.getCompanyCode())));
                break;
        }
    }

    public Regulasi delete(Long id){
        Regulasi obj = this.find(id);
        obj.setIsDeleted(true);
        this.edit(obj);
        historyService.save(new RegulasiHistoryDto("DELETE", new RegulasiDto(obj), new CompanyCodeDto(obj.getCompanyCode())));
        return obj;
    }

    public void uploadAttachment(Long idRegulasi, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";
        try {
            String filename = formDataContentDisposition.getFileName();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is2 = new ByteArrayInputStream(baos.toByteArray());

            byte[] documentContent = IOUtils.toByteArray(is2);

            Regulasi regulasi = this.find(idRegulasi);

//            String number = autoNumberService.from(SuratDocLib.class).next(SuratDocLib::getNumber);
            List<String> extractedText = textExtractionService.parsingFile(is1, formDataContentDisposition, body).getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            docId = fileService.upload(filename, documentContent);
            docId = docId.replace("workspace://SpacesStore/", "");

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString().concat(FileUtility.GetFileExtension(filename)));
            obj.setDocName(filename);
            obj.setCompanyCode(regulasi.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setSize(new Long(documentContent.length));
            obj.setIsDeleted(false);
            obj.setReferenceId(idRegulasi);
            obj.setReferenceTable("t_regulasi");
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);
        } catch (Exception ex) {
            if (!StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }
            throw ex;
        }
    }

    public GlobalAttachment deleteFile(Long idFile) {
        GlobalAttachment global = globalAttachmentService.find(idFile);

        if (global.getReferenceTable().toLowerCase().equals("t_regulasi")) {
            global.setIsDeleted(true);
            globalAttachmentService.edit(global);
        }

        return global;
    }




//************************** DARI ERROR REPORT **************************************
    public void saveOrEdit(Long id, RegulasiDto dao, List<FormDataBodyPart> body, List<String> deletedIds) throws InternalServerErrorException, IOException, Exception {
        boolean isNew = id == null;
        Regulasi model;
        DateUtil dateUtil = new DateUtil();
//        model = data(model, dao);

        if (isNew) {
            model = new Regulasi();
            model.setNamaRegulasi(dao.getNamaRegulasi());
            model.setNomorRegulasi(dao.getNomorRegulasi());
            model.setDeskripsi(dao.getDeskripsi());
            model.setStatus(dao.getStatus());
            model.setIsDeleted(dao.getIsDeleted());
            model.setIsPublished(dao.getIsPublished());
            model.setIsRegulasiPerusahaan(dao.getIsRegulasiPerusahaan());
            model.setTanggalTerbit(dateUtil.getDateTimeStart(dao.getTanggalTerbit()));
            model.setTanggalSelesaiBerlaku(dateUtil.getDateTimeEnd(dao.getTanggalSelesaiBerlaku()));
            model.setTanggalMulaiBerlaku(dateUtil.getDateTimeStart(dao.getTanggalMulaiBerlaku()));
//            model.setKategori(dao.getKategori());
//            model.setUraian(dao.getUraian());
//            model.setCompanyCode(userSession.getUserSession().getUser().getCompanyCode());
            if(dao.getJenisRegulasiId()!=null){
                MasterUser user = userSession.getUserSession().getUser();
                MasterJenisRegulasi object = jenisRegulasiService.find(dao.getJenisRegulasiId());
                model.setJenisRegulasi(object);
                model.setCompanyCode(object.getCompanyCode()); //cara 1 buat ngambil company code dari master jenis regulasi
//            model.setCompanyCode(user.getCompanyCode()); //cara 2 buat ngambil company code dari master jenis regulasi
            }

            if(dao.getKlasifikasiDokumenId()!=null){
//          if(dao.getKlasifikasiDokumen()!=null){
//            MasterKlasifikasiMasalah object = klasifikasiMasalahService.find(dao.getKlasifikasiDokumen().getIdKlasifikasiMasalah());
                MasterKlasifikasiMasalah object = klasifikasiMasalahService.find(dao.getKlasifikasiDokumenId());
                model.setKlasifikasiDokumen(object);
            }
//            else {
//                model.setKlasifikasiDokumen(null);
//            }

            if(dao.getRegulasiPenggantiId()!=null){
                Regulasi object = this.find(dao.getRegulasiPenggantiId());
                model.setRegulasiPengganti(object);
                if(model.getId()!=null){
                    penggantiService.createOrEdit(model.getId(),dao.getRegulasiPenggantiId(), dao.getCompanyId());
//                    penggantiService.save(null,dao);
                }
            }
//            else {
//                model.setRegulasiPengganti(null);
//            }

            if(dao.getUnitKonseptorId()!=null){
                MasterKorsa object = korsaService.find(dao.getUnitKonseptorId());
                model.setUnitKonseptor(object);
            }
//            else {
//                model.setUnitKonseptor(null);
//            }
            this.create(model);
            getEntityManager().flush();
            getEntityManager().refresh(model);
            historyService.save(new RegulasiHistoryDto("CREATE", new RegulasiDto(model), new CompanyCodeDto(model.getCompanyCode())));
        } else {
            model = this.find(id);
//            model = data(model, dao);
            model.setIsDeleted(false);
            model.setIsPublished(false);

            //******************** DARI REGULASI ********************
            if(dao.getJenisRegulasiId()!=null){
                MasterUser user = userSession.getUserSession().getUser();
                MasterJenisRegulasi object = jenisRegulasiService.find(dao.getJenisRegulasiId());
                model.setJenisRegulasi(object);
                model.setCompanyCode(object.getCompanyCode()); //cara 1 buat ngambil company code dari master jenis regulasi
//            model.setCompanyCode(user.getCompanyCode()); //cara 2 buat ngambil company code dari master jenis regulasi
            } else {
                model.setJenisRegulasi(null);
            }
            if(dao.getCompanyId()!=null){
                CompanyCode object = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(object);
            } else {
                model.setCompanyCode(null);
            }
            if(dao.getKlasifikasiDokumenId()!=null){
//          if(dao.getKlasifikasiDokumen()!=null){
//            MasterKlasifikasiMasalah object = klasifikasiMasalahService.find(dao.getKlasifikasiDokumen().getIdKlasifikasiMasalah());
                MasterKlasifikasiMasalah object = klasifikasiMasalahService.find(dao.getKlasifikasiDokumenId());
                model.setKlasifikasiDokumen(object);
            } else {
                model.setKlasifikasiDokumen(null);
            }

            if(dao.getRegulasiPenggantiId()!=null){
                Regulasi object = this.find(dao.getRegulasiPenggantiId());
                model.setRegulasiPengganti(object);
                if(model.getId()!=null){
                    penggantiService.createOrEdit(model.getId(),dao.getRegulasiPenggantiId(), dao.getCompanyId());
                }
            } else {
                model.setRegulasiPengganti(null);
            }

            if(dao.getUnitKonseptorId()!=null){
                MasterKorsa object = korsaService.find(dao.getUnitKonseptorId());
                model.setUnitKonseptor(object);
            } else {
                model.setUnitKonseptor(null);
            }

//            if (deletedIds != null) {
//                this.deleteAttachment(deletedIds);
//
//                String docId = "";
//                List<String> split = Arrays.asList(model.getDocId().split(";"));
//                split = split.stream().filter(q -> !deletedIds.contains(q)).collect(Collectors.toList());
//                if (split.stream().count() > 0) {
//                    for (String strId : split) {
//                        if ("".equals(docId)) {
//                            docId = strId;
//                        } else {
//                            docId = docId + ";" + strId;
//                        }
//                    }
//                }
//
//                model.setDocId(docId);
//                this.edit(model);
//            }
            this.edit(model);
            createHistory(model);
        }

//        Long idRegulasi = model.getId();
//        if (body != null && !body.isEmpty()) {
//            this.uploadAttachment(idRegulasi, body);
//        }

    }

//    public void uploadAttachment(Long idRegulasi, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, List<FormDataBodyPart> body) throws InternalServerErrorException, Exception {
//        String docId = "";
//        try {
//            Regulasi error = this.find(idRegulasi);
//            GlobalAttachment obj = globalAttachmentService.getAttachmentDataByRef("t_regulasi", idRegulasi).findFirst().orElse(null);
//
//            if (body != null && !body.isEmpty()) {
//                if (obj != null) {
//                    globalAttachmentService.deleteAttachment("t_regulasi", idRegulasi);
//                }
//
//                for (FormDataBodyPart fd : body) {
//                    String tmpDoc = "";
//                    BodyPartEntity bodyPartEntity = (BodyPartEntity) fd.getEntity();
//                    String filename = fd.getContentDisposition().getFileName();
//
//                    byte[] documentContent = org.apache.commons.compress.utils.IOUtils.toByteArray(bodyPartEntity.getInputStream());
//
//                    tmpDoc = fileService.upload(filename, documentContent);
//                    tmpDoc = tmpDoc.replace("workspace://SpacesStore/", "");
//
//                    obj = new GlobalAttachment();
//                    obj.setMimeType(fd.getMediaType().toString());
//                    obj.setDocGeneratedName(UUID.randomUUID().toString());
//                    obj.setDocName(filename);
//                    obj.setCompanyCode(userSession.getUserSession().getUser().getCompanyCode());
//                    obj.setDocId(tmpDoc);
//                    obj.setIsKonsep(false);
//                    obj.setIsPublished(false);
//                    obj.setSize(new Long(documentContent.length));
//                    obj.setIsDeleted(false);
//                    obj.setReferenceId(idRegulasi);
//                    obj.setReferenceTable("t_regulasi");
//    //
//    //                obj.setDocGeneratedName(UUID.randomUUID().toString().concat(FileUtility.GetFileExtension(filename)));
//    //                obj.setCompanyCode(regulasi.getCompanyCode());
//    //                obj.setNumber("test 123");
//    //                obj.setExtractedText(jsonRepresentation);
//
//                    globalAttachmentService.create(obj);
//
//                    if (docId == "") {
//                        docId = tmpDoc;
//                    } else {
//                        docId = docId + ";" + tmpDoc;
//                    }
//                }
//
//                error.setDocId(docId);
//                this.edit(error);
//            }
//        } catch (Exception ex) {
//            ex.getMessage();
//        }
//    }

//    public void deleteAttachment(Long idRegulasi) throws InternalServerErrorException, Exception {
////        try {
//            Regulasi error = this.find(idRegulasi);
//            List<String> split = Arrays.asList(error.getDocId().split(";"));
//            for (String docId : split) {
//                GlobalAttachmentDto attachmentDto = globalAttachmentService.findByDocId(docId);
//                GlobalAttachment attachment = globalAttachmentService.find(attachmentDto.getId());
//
//                fileService.delete(attachment.getDocId());
//                globalAttachmentService.remove(attachment);
//            }
////        } catch (Exception ex) {
////            ex.getMessage();
////        }
//    }

//    public void deleteAttachment(List<String> deletedFilesId) throws InternalServerErrorException, IOException, Exception {
////        try {
//        if (deletedFilesId != null) {
////            Regulasi error = this.find(idRegulasi);
////            List<String> split = Arrays.asList(error.getDocId().split(";"));
////                List<String> deletedIds = Arrays.asList(deletedFilesId.split(";"));
//
//            for (String docId : deletedFilesId) {
//                GlobalAttachmentDto attachmentDto = globalAttachmentService.findByDocId(docId);
//                GlobalAttachment attachment = globalAttachmentService.find(attachmentDto.getId());
//
//                fileService.delete(docId);
//                globalAttachmentService.remove(attachment);
//
////                    split.remove(split.indexOf(docId));
////                split = split.stream().filter(q -> !q.equals(docId)).collect(Collectors.toList());
//            }
//
////            String docId = "";
////            if (!split.isEmpty()) {
////                for (String id : split) {
////                    if (docId == "") {
////                        docId = id;
////                    } else {
////                        docId = docId + ";" + id;
////                    }
////                }
////            }
////
////            error.setDocId(docId);
////            this.edit(error);
//        }
//
////        } catch (Exception ex) {
////            ex.getMessage();
////        }
//    }

    //***************** DARI SURAT SERVICE ********************
    public void deleteAttachment(Long idRegulasi, String docId) {
        GlobalAttachment attachment = globalAttachmentService.findByDocId("t_regulasi", idRegulasi, docId);

        if (attachment != null) {
            globalAttachmentService.remove(attachment);
            fileService.delete(docId);
        }
    }
    
//**************** DARI SURAT INTERACTION HISTORY SERVICE ***********************
//    public Regulasi setStared(Long idRegulasi){
//        Regulasi model = this.find(idRegulasi);
//        model.setIsStarred(true);
//        this.edit(model);
//        return model;
//    }

    public Regulasi setStared(Long idRegulasi, Boolean stared){
        Regulasi model = this.find(idRegulasi);
        model.setIsStarred(stared);
        if(stared.booleanValue()){
            model.setIsStarred(true);
        }else{
            model.setIsStarred(false);
        }
        edit(model);
        return model;
    }
}