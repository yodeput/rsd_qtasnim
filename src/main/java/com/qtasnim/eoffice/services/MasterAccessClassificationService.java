package com.qtasnim.eoffice.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.qtasnim.eoffice.db.MasterAccessClassification;

import org.jinq.jpa.JPAJinqStream;

@Stateless
@LocalBean
public class MasterAccessClassificationService extends AbstractFacade<MasterAccessClassification> {
    private static final long serialVersionUID = 1L;

    @Override
    protected Class<MasterAccessClassification> getEntityClass() {
        return MasterAccessClassification.class;
    }

    public JPAJinqStream<MasterAccessClassification> getAll() {
        return db();
    }

}