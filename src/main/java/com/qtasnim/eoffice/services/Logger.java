package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.context.RequestContext;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.db.SysLog;
import com.qtasnim.eoffice.security.IRequestContext;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Transaction;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.*;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

@Stateless
@LocalBean
public class Logger {
    @Inject
    private SysLogService logService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    @IRequestContext
    private Transaction transaction;

    @Inject
    private Event<SysLog> logEvent;

    @Inject
    private org.slf4j.Logger logger;

    public void logEventFired(@Observes SysLog log) {
        saveLog(log);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void info(String log) {
        logger.info(log);

        logging(LogType.INFO, log);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void error(String log) {
        logger.error(log);

        logging(LogType.ERROR, log);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void error(String log, Throwable e) {
        logger.error(log, e);

        StringWriter writer = new StringWriter();
        PrintWriter printWriter= new PrintWriter(writer);
        e.printStackTrace(printWriter);

        logging(LogType.ERROR, String.format("[%s] %s", log, writer.toString()));
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void log(LogType logType, String message) {
        logging(logType, message);
    }

    private void logging(LogType logType, String message) {
        SysLog log = new SysLog();
        log.setCategory(logType.toString());
        log.setExecutorIp(Optional.ofNullable(transaction.getRequestContext()).map(RequestContext::getIpAddress).orElse("-"));
        log.setContent(Optional.ofNullable(message).orElse("-"));
        log.setExecutorId("System");
        log.setExecutorIdType("System");

        Optional.ofNullable(userSession).filter(t -> t.getUserSession() != null && t.getUserSession().getUser() != null && StringUtils.isNotEmpty(t.getUserSession().getUser().getLoginUserName())).ifPresent(us -> {
            log.setExecutorId(us.getUserSession().getUser().getLoginUserName());
            log.setExecutorIdType("KAI User");
        });

        logEvent.fire(log);
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void saveLog(SysLog log) {
        logService.create(log);
    }
}
