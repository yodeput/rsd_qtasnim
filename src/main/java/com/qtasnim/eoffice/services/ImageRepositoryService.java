package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.JerseyClient;
import com.qtasnim.eoffice.db.ImageRepository;
import com.qtasnim.eoffice.db.MasterMimeMapping;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;
import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.UUID;

@Stateless
@LocalBean
public class ImageRepositoryService extends AbstractFacade<ImageRepository>{
    @Inject
    private MasterMimeMappingService masterMimeMappingService;

    @Inject
    private MasterUserService userService;

    @Inject
    @JerseyClient
    private Client client;

    @Override
    protected Class<ImageRepository> getEntityClass() {
        return ImageRepository.class;
    }

    public ImageRepository getByGeneratedName(String name) {
        return db().where(t -> t.getGeneratedFileName().equals(name)).findFirst().orElse(null);
    }

    public ImageRepository upload(String fileName, URL url) throws URISyntaxException, IOException {
        WebTarget webResource = client.target(url.toURI());
        Response response = webResource
                .request()
                .get();

        InputStream result = response.readEntity(InputStream.class);

        return upload(fileName, result);
    }

    public ImageRepository upload(String fileName, InputStream imageInputStream) throws IOException {
        int large = 1280;
        int medium = 1024;
        int small = 600;

        String extension = FilenameUtils.getExtension(fileName);
        String generatedFileName = String.format("%s.%s", UUID.randomUUID().toString(), extension);

        BufferedImage imgOriginal = ImageIO.read(imageInputStream); // load image

        if (imgOriginal == null) {
            throw new InternalServerErrorException("Profile image import error");
        }

        BufferedImage imgLarge = imgOriginal;
        BufferedImage imgMedium = imgOriginal;
        BufferedImage imgSmall = imgOriginal;

        MasterMimeMapping mimeMapping = masterMimeMappingService.getByExtension(extension.toLowerCase());

        int max = Math.max(imgOriginal.getWidth(), imgOriginal.getHeight());

        if (max > large) {
            imgLarge = Scalr.resize(imgOriginal, Scalr.Method.QUALITY, large, large, Scalr.OP_ANTIALIAS);
        }

        if (max > medium) {
            imgMedium = Scalr.resize(imgOriginal, Scalr.Method.QUALITY, medium, medium, Scalr.OP_ANTIALIAS);
        }

        if (max > small) {
            imgSmall = Scalr.resize(imgOriginal, Scalr.Method.QUALITY, small, small, Scalr.OP_ANTIALIAS);
        }

        ImageRepository imageRepository = new ImageRepository();
        imageRepository.setExtension(mimeMapping);
        imageRepository.setFileName(fileName);
        imageRepository.setGeneratedFileName(generatedFileName);

        MasterUser objUser = userService.findUserByUsername("superuser");
        imageRepository.setOwnerOrganization(objUser.getOrganizationEntity());
        imageRepository.setOwnerUser(objUser);

        try{
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write( imgOriginal, "png", baos );
            baos.flush();
            byte[] imageIn = baos.toByteArray();
            imageRepository.setImageOriginal(imageIn);
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write( imgLarge, "png", baos );
            baos.flush();
            byte[] imageIn = baos.toByteArray();
            imageRepository.setImageLarge(imageIn);
            baos.close();
        }catch (IOException e){
            e.printStackTrace();
        }

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write( imgMedium, "png", baos );
            baos.flush();
            byte[] imageIn = baos.toByteArray();
            imageRepository.setImageMedium(imageIn);
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write( imgSmall, "png", baos );
            baos.flush();
            byte[] imageIn = baos.toByteArray();
            imageRepository.setImageSmall(imageIn);
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.create(imageRepository);

        return imageRepository;
    }
}
