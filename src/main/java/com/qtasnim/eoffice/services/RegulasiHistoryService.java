package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.RegulasiHistoryDto;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class RegulasiHistoryService extends AbstractFacade<RegulasiHistory> {
    private static final long serialVersionUID = 1L;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    CompanyCodeService companyCodeService;

    @Inject
    RegulasiService regulasiService;

    @Inject
    private FileService fileService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;


    @Override
    protected Class<RegulasiHistory> getEntityClass() {
        return RegulasiHistory.class;
    }


    public RegulasiHistory save(RegulasiHistoryDto dao) {
        RegulasiHistory model = new RegulasiHistory();
        model.setStatus(dao.getStatus());
        Regulasi regulasi = regulasiService.find(dao.getRegulasi().getId());
        model.setRegulasi(regulasi);

        CompanyCode companyCode = companyCodeService.find(dao.getCompany().getId());
        model.setCompanyCode(companyCode);

        this.create(model);
        return model;
    }

    public void delete(Long id) {
        this.delete(id);

    }
}