package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.CardCommentDto;
import com.qtasnim.eoffice.ws.dto.CardMemberDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class CardCommentService  extends AbstractFacade<CardComment>{

    @Override
    protected Class<CardComment> getEntityClass() {
        return CardComment.class;
    }

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyService;

    @Inject
    private BoardService boardService;

    @Inject
    private CardService cardService;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    @Inject
    private MasterUserService userService;

    public JPAJinqStream<CardComment> getAll(){
        Long idCompany = userSession.getUserSession().getUser().getCompanyCode().getId();
        CompanyCode company = companyService.find(idCompany);
        return db().where(a->a.getIsDeleted().booleanValue()==false && a.getCompanyCode().equals(company));
    }

    public CardComment saveOrEdit(CardCommentDto dao, Long id) throws Exception {
        try{
            CardComment obj = new CardComment();
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = companyService.getByCode(user.getCompanyCode().getCode());
            if(id==null){
                obj.setIsDeleted(false);
                obj.setComment(dao.getComment());
                if(dao.getCardId()!=null){
                    Card card = cardService.find(dao.getCardId());
                    obj.setCard(card);
                }
                obj.setCompanyCode(company);
                if(dao.getOrganizationId()!=null){
                    MasterStrukturOrganisasi org = organisasiService.find(dao.getOrganizationId());
                    MasterUser usr = userService.find(org.getUser().getId());
                    obj.setOrganization(org);
                    obj.setUser(usr);
                }
                create(obj);
            }else{
                obj = find(id);
                obj.setIsDeleted(false);
                obj.setComment(dao.getComment());
                if(dao.getCardId()!=null){
                    Card card = cardService.find(dao.getCardId());
                    obj.setCard(card);
                }
                obj.setCompanyCode(company);
                if(dao.getOrganizationId()!=null){
                    MasterStrukturOrganisasi org = organisasiService.find(dao.getOrganizationId());
                    MasterUser usr = userService.find(org.getUser().getId());
                    obj.setOrganization(org);
                    obj.setUser(usr);
                }
                edit(obj);
            }
            getEntityManager().flush();
            getEntityManager().refresh(obj);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public CardComment deleteComment(Long id) throws Exception {
        try{
            CardComment obj = find(id);
            obj.setIsDeleted(true);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
}
