package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterUnit;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterUnitDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterUnitService extends AbstractFacade<MasterUnit> {
    private static final long serialVersionUID = 1L;
    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterUnit> getEntityClass() {
        return MasterUnit.class;
    }

    public JPAJinqStream<MasterUnit> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }


    public JPAJinqStream<MasterUnit> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name));
    }


    public MasterUnit save(String id, MasterUnitDto dao) {
        MasterUnit model = new MasterUnit();
        if (id == null) {
            this.create(data(model, dao));
        } else {
            model = this.find(id);
            this.edit(data(model, dao));
        }
        return model;
    }

    public MasterUnit data(MasterUnit masterUnit, MasterUnitDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterUnit model = masterUnit;
        model.setId(dao.getId());
        model.setNama(dao.getNama());
        if (dao.getCompanyId() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }
        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        return model;
    }

    public MasterUnit delete(String id) {
        MasterUnit model = this.find(id);
        this.remove(model);
        return model;
    }

}