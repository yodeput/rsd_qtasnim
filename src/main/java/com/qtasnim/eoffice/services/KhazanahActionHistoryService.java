package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.KhazanahActionHistoryDto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class KhazanahActionHistoryService extends AbstractFacade<KhazanahActionHistory> {

    
    @Inject
    private KhazanahService khazanahService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterUserService masterUserService;


    public List<KhazanahActionHistory> getByKhazanah(Long idKhazanah){
        return db().where(c -> c.getKhazanah().getId().equals(idKhazanah))
                .sortedDescendingBy(KhazanahActionHistory::getViewedDate)
                .sortedDescendingBy(KhazanahActionHistory::getDownloadedDate)
                .toList();
    }

    @Override
    protected Class<KhazanahActionHistory> getEntityClass() {
        return KhazanahActionHistory.class;
    }

    public KhazanahActionHistory getByKhazanahAndUser(Long khazanahId, Long userId) {
        return db().where(q -> q.getKhazanah().getId().equals(khazanahId) && q.getUser().getId().equals(userId)).findFirst().orElse(null);
    }

    public KhazanahActionHistory saveOrEdit(Long id, KhazanahActionHistoryDto dao){
        Boolean isNew = id == null;
        KhazanahActionHistory model = isNew ? new KhazanahActionHistory() : find(id);
        DateUtil dateUtil = new DateUtil();
        MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getOrganizationId());
        model.setOrganization(org);
        Khazanah khazanah = khazanahService.find(dao.getKhazanahId());
        model.setKhazanah(khazanah);
        CompanyCode companyCode = companyCodeService.find(dao.getCompanyId());
        model.setCompanyCode(companyCode);
        MasterUser masterUser = masterUserService.find(dao.getUserId());
        model.setUser(masterUser);
        model.setIsViewed(dao.getIsViewed());
        model.setIsDownloaded(dao.getIsDownloaded());
        if (dao.getIsViewed()) {
            model.setViewedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
        }
        if (dao.getIsDownloaded()) {
            model.setDownloadedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
        }

        if (id == null) {
            this.create(model);
        } else {
            this.edit(model);
        }

        return model;
    }

    public KhazanahActionHistory setViewed(Long id){
        KhazanahActionHistory model = this.find(id);
        DateUtil dateUtil = new DateUtil();
        model.setIsViewed(true);
        model.setViewedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
        this.edit(model);
        return model;
    }

    public KhazanahActionHistory setDownloaded(Long id){
        KhazanahActionHistory model = this.find(id);
        DateUtil dateUtil = new DateUtil();
        model.setIsDownloaded(true);
        model.setDownloadedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
        this.edit(model);
        return model;
    }
}
