package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterKlasifikasiMasalah;
import com.qtasnim.eoffice.db.MasterStatusAkhirArsip;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterKlasifikasiMasalahDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class MasterKlasifikasiMasalahService extends AbstractFacade<MasterKlasifikasiMasalah> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterStatusAkhirArsipService masterStatusAkhirArsipService;

    @Override
    protected Class<MasterKlasifikasiMasalah> getEntityClass() {
        return MasterKlasifikasiMasalah.class;
    }

    public JPAJinqStream<MasterKlasifikasiMasalah> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterKlasifikasiMasalah>getFiltered(String kode, String nama, Date dateParam, Boolean isInBetween
            , Boolean isBefore, Boolean isAfter, Boolean isActive){
        JPAJinqStream<MasterKlasifikasiMasalah> query = db();

        if(!Strings.isNullOrEmpty(kode)){
            query = query.where(q ->q.getKodeKlasifikasiMasalah().toLowerCase().contains(kode.toLowerCase()));
        }

        if(!Strings.isNullOrEmpty(nama)){
            query = query.where(q ->q.getNamaKlasifikasiMasalah().toLowerCase().contains(nama.toLowerCase()));
        }

        if(dateParam != null){
            if(isInBetween!=null && isInBetween){
                query = query.where(q -> dateParam.after(q.getTglRetensiAktif()) && dateParam.before(q.getTglRetensiInaktif()));
            }

            if(isBefore!=null && isBefore){
                query = query.where(q-> dateParam.before(q.getTglRetensiAktif()));
            }

            if(isAfter!=null && isAfter){
                query = query.where(q->dateParam.after(q.getTglRetensiInaktif()));
            }
        }

        if(isActive!=null){
            if(isActive.booleanValue()==true){
                query = query.where(q -> q.getIsActive());
            }else{
                query = query.where(q -> !q.getIsActive());
            }
        }

        return query;
    }

    public void saveOrEdit(MasterKlasifikasiMasalahDto dao,Long id) throws InternalServerErrorException {
        boolean isNew = id==null;
        DateUtil dateUtil = new DateUtil();
        MasterKlasifikasiMasalah model = new MasterKlasifikasiMasalah();
        if(isNew){
            model.setKodeKlasifikasiMasalah(dao.getKodeKlasifikasiMasalah());
            model.setNamaKlasifikasiMasalah(dao.getNamaKlasifikasiMasalah());
            Date n = new Date();
            model.setTglRetensiAktif(n);
            model.setTglRetensiInaktif(n);
//            model.setTglRetensiAktif(dateUtil.getDateFromISOString(dao.getTglRetensiAktif()));
//            model.setTglRetensiInaktif(dateUtil.getDateFromISOString(dao.getTglRetensiInaktif()));

            if(dao.getIdParent()!=null){
                MasterKlasifikasiMasalah parent = this.find(dao.getIdParent());
                model.setParent(parent);
            }else{
                model.setParent(null);
            }

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }

            model.setIsActive(dao.getIsActive());
//            model.setCreatedBy(dao.getCreatedBy());
//            model.setCreatedDate(new Date());
//            model.setModifiedBy("-");
//            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            if(dao.getIdStatusAkhirArsip()!=null){
                MasterStatusAkhirArsip statusAkhir = masterStatusAkhirArsipService.find(dao.getIdStatusAkhirArsip());
                model.setStatusAkhir(statusAkhir);
            }else{
                model.setStatusAkhir(null);
            }

            model.setRetensiAktifYear(dao.getRetensiAktifYear());
            model.setRetensiInaktifYear(dao.getRetensiInaktifYear());

            this.create(model);
        }else{
            model = this.find(id);
            model.setKodeKlasifikasiMasalah(dao.getKodeKlasifikasiMasalah());
            model.setNamaKlasifikasiMasalah(dao.getNamaKlasifikasiMasalah());
            Date n = new Date();
            model.setTglRetensiAktif(n);
            model.setTglRetensiInaktif(n);
//            model.setTglRetensiAktif(dateUtil.getDateFromISOString(dao.getTglRetensiAktif()));
//            model.setTglRetensiInaktif(dateUtil.getDateFromISOString(dao.getTglRetensiInaktif()));

            if(dao.getIdParent()!=null){
                MasterKlasifikasiMasalah parent = this.find(dao.getIdParent());
                model.setParent(parent);
            }else{
                model.setParent(null);
            }

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }

            model.setIsActive(dao.getIsActive());
//            model.setModifiedBy(dao.getModifiedBy());
//            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            if(dao.getIdStatusAkhirArsip()!=null){
                MasterStatusAkhirArsip statusAkhir = masterStatusAkhirArsipService.find(dao.getIdStatusAkhirArsip());
                model.setStatusAkhir(statusAkhir);
            }else{
                model.setStatusAkhir(null);
            }

            model.setRetensiAktifYear(dao.getRetensiAktifYear());
            model.setRetensiInaktifYear(dao.getRetensiInaktifYear());
            this.edit(model);
        }
    }

    public JPAJinqStream<MasterKlasifikasiMasalah> getByParentId(Long parent,String sort,Boolean descending){
        Date n = new Date();
        JPAJinqStream<MasterKlasifikasiMasalah> query = db().where(q->n.after(q.getStart()) && n.before(q.getEnd()));
        if(parent==null){
            query = query.where(q->q.getParent().equals(null));
        }else{
           query = query.where(q->q.getParent().getIdKlasifikasiMasalah().equals(parent));
        }

        if (!Strings.isNullOrEmpty(sort)) {
            if (descending) {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedDescendingBy(q->q.getNamaKlasifikasiMasalah());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedDescendingBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedDescendingBy(q -> q.getEnd());
                }
            } else {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedBy(q -> q.getNamaKlasifikasiMasalah());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedBy(q -> q.getEnd());
                }
            }
        }
        return query;
    }

    public List<MasterKlasifikasiMasalahDto> getListByParent(Long parent, String sort, Boolean descending) {
        return getByParentId(parent, sort, descending).map(MasterKlasifikasiMasalahDto::new).collect(Collectors.toList());
    }

    public List<MasterKlasifikasiMasalahDto> getListByParent(Long parent) {
        return getByParentId(parent, null, null).map(MasterKlasifikasiMasalahDto::new).collect(Collectors.toList());
    }

    public void deleteData(MasterKlasifikasiMasalah klasifikasi) throws Exception {
        try {
            List<MasterKlasifikasiMasalah> childs = getByParentId(klasifikasi.getIdKlasifikasiMasalah(), "", null).collect(Collectors.toList());
            if (childs.isEmpty()) {
                this.remove(klasifikasi);
            } else {
                throw new Exception("Data memiliki child");
            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterKlasifikasiMasalah findByNamaKlasifikasiMasalah(String value) {
        return db().where(t -> t.getNamaKlasifikasiMasalah().equals(value)).findFirst().orElse(null);
    }
    
    public MasterKlasifikasiMasalah findByKode(String value) {
        return db().where(t -> t.getKodeKlasifikasiMasalah().equals(value)).findFirst().orElse(null);
    }

    public boolean checkChilds(Long idParent){
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && q.getParent()!=null).anyMatch(w->w.getParent().getIdKlasifikasiMasalah().equals(idParent));
    }
}
