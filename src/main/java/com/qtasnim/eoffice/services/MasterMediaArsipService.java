package com.qtasnim.eoffice.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterMediaArsip;

import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterMediaArsipDto;
import org.jinq.jpa.JPAJinqStream;

import java.util.Date;

@Stateless
@LocalBean
public class MasterMediaArsipService extends AbstractFacade<MasterMediaArsip> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterMediaArsip> getEntityClass() {
        return MasterMediaArsip.class;
    }

    public JPAJinqStream<MasterMediaArsip> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public MasterMediaArsipDto saveOrEdit(Long id, MasterMediaArsipDto dao) throws Exception{
        try {
            MasterMediaArsipDto result;
            boolean isNew = id == null;
            MasterMediaArsip model = new MasterMediaArsip();
            DateUtil dateUtil = new DateUtil();
            if (isNew) {
                model.setName(dao.getName());
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null){
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.create(model);
            } else {
                model = this.find(id);
                model.setName(dao.getName());
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null){
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.edit(model);
            }

            result = new MasterMediaArsipDto(model);
            return result;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterMediaArsip findByNama(String value) {
        return db().where(t -> t.getName().equals(value)).findFirst().orElse(null);
    }
}