package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.IWorkflowEntity;
import com.qtasnim.eoffice.db.ProcessDefinition;
import com.qtasnim.eoffice.db.ProcessInstance;
import com.qtasnim.eoffice.ws.dto.SuratDto;
import com.qtasnim.workflows.camunda.dto.runtime.ProcessInstanceDto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;


@Stateless
@LocalBean
public class ProcessInstanceService extends AbstractFacade<ProcessInstance> {
    @Override
    protected Class<ProcessInstance> getEntityClass() {
        return ProcessInstance.class;
    }

    public ProcessInstance getByRelatedEntity(String entityName, String recordRefId) {
        return db()
                .where(t -> t.getRelatedEntity().equals(entityName)
                        && t.getRecordRefId().equals(recordRefId))
                .sortedDescendingBy(ProcessInstance::getId)
                .findFirst()
                .orElse(null);
    }

    public ProcessInstance getByRecordRefId(String formName, String recordRefId) {
        return db()
                .where(t -> t.getProcessDefinition().getFormName().equals(formName)
                        && t.getRecordRefId().equals(recordRefId))
                .findFirst()
                .orElse(null);
    }

    public String getProcessDefinitionByInstanceId(String instanceId) {
        return db()
                .where(t -> t.getInstanceId().equals(instanceId))
                .select(t -> t.getProcessDefinition().getDefinition())
                .findFirst()
                .orElse(null);
    }

    public ProcessInstance saveInstances(SuratDto entity, ProcessInstanceDto processInstanceDto, ProcessDefinition processDefinition, IWorkflowEntity obj){
        try {
            ProcessInstance processInstance = new ProcessInstance();
            processInstance.setRecordRefId(entity.getId().toString());
            processInstance.setInstanceId(processInstanceDto.getId());
            processInstance.setProcessDefinition(processDefinition);
//                processInstance.setRelatedEntity(String.valueOf(idJenisDokumen));  // dari jenis dokumen
            processInstance.setRelatedEntity(obj.getClassName());

            this.create(processInstance);
            getEntityManager().flush();
            getEntityManager().refresh(processInstance);

            return processInstance;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
}
