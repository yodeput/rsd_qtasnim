package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class TindakanDisposisiService extends AbstractFacade<TindakanDisposisi>{

    @Override
    protected Class<TindakanDisposisi> getEntityClass() {
        return TindakanDisposisi.class;
    }

    @Inject
    private SuratDisposisiService suratDisposisiService;

    @Inject
    @ISessionContext
    private Session userSession;

    public List<TindakanDisposisi> getAllBy(Long idDisposisi){
        SuratDisposisi surat = suratDisposisiService.find(idDisposisi);
        return db().where(q->q.getSuratDisposisi().equals(surat)).toList();
    }

}
