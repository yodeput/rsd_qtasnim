package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.DistribusiManualDto;
import com.qtasnim.eoffice.ws.dto.TujuanDistribusiManualDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class DistribusiManualService extends AbstractFacade<DistribusiManual> {
    @Inject private CompanyCodeService companyService;
    @Inject private MasterUserService userService;
    @Inject private SuratService suratService;
    @Inject private MasterStrukturOrganisasiService organisasiService;
    @Inject private MasterKlasifikasiKeamananService klasifikasiKeamananService;
    @Inject private MasterTingkatUrgensiService tingkatUrgensiService;
    @Inject private MasterTingkatPerkembanganService tingkatPerkembanganService;
    @Inject private TujuanDistribusiManualService tujuanService;

    @Override protected Class<DistribusiManual> getEntityClass() { return DistribusiManual.class; }

    public JPAJinqStream<DistribusiManual> getAll() { return db(); }

    public DistribusiManual saveOrEdit(Long id, DistribusiManualDto dto, List<Long> deletedTujuanIds) {
        boolean isNew = id == null;
        DistribusiManual model = this.getModel(isNew ? new DistribusiManual() : this.find(id), dto);

        if (isNew) {
            model.setIsDeleted(false);
            this.create(model);
            getEntityManager().flush();
            this.saveTujuanDistribusi(model, dto);
        } else {
            this.edit(model);
            getEntityManager().flush();

            if (deletedTujuanIds != null && !deletedTujuanIds.isEmpty()) {
                for (Long deletedId : deletedTujuanIds) {
                    this.deletedTujuan(deletedId);
                }
            }

            this.saveTujuanDistribusi(model, dto);
        }

        return model;
    }

    public DistribusiManual delete(Long id) {
        DistribusiManual model = this.find(id);
        model.setIsDeleted(true);
        this.edit(model);
        return model;
    }

    private void saveTujuanDistribusi(DistribusiManual model, DistribusiManualDto dto) {
        if (!dto.getTujuan().isEmpty()) {
            for (TujuanDistribusiManualDto dao : dto.getTujuan()) {
                TujuanDistribusiManual obj;
                if (dao.getId() == null) {
                    obj = new TujuanDistribusiManual();
                    if (dao.getOrganizationId() != null) {
                        MasterStrukturOrganisasi org = organisasiService.find(dao.getOrganizationId());
                        obj.setOrganization(org);
                    }
                    obj.setDistribusi(model);
                    tujuanService.create(obj);
                } else {
                    obj = tujuanService.find(dao.getId());
                    if (dao.getOrganizationId() != null) {
                        MasterStrukturOrganisasi org = organisasiService.find(dao.getOrganizationId());
                        obj.setOrganization(org);
                    }
                    tujuanService.edit(obj);
                }
            }
        }
    }

    private void deletedTujuan(Long idTujuan) {
        TujuanDistribusiManual tujuan = tujuanService.find(idTujuan);
        tujuanService.remove(tujuan);
    }

    private DistribusiManual getModel(DistribusiManual model, DistribusiManualDto dto) {
        DateUtil dateUtil = new DateUtil();

        CompanyCode company = companyService.find(dto.getIdCompany());
        model.setCompanyCode(company);

        MasterUser pengirim = userService.find(dto.getIdPengirim());
        model.setPengirim(pengirim);

        model.setKurir(dto.getKurir());

        if (dto.getIdPenerima() != null) {
            MasterUser penerima = userService.find(dto.getIdPenerima());
            model.setPenerima(penerima);
        } else {
            if (dto.getNippPenerima() != null) model.setNippPenerima(dto.getNippPenerima());
            if (dto.getNamaPenerima() != null) model.setNamaPenerima(dto.getNamaPenerima());
            if (dto.getJabatanPenerima() != null) model.setJabatanPenerima(dto.getJabatanPenerima());
        }

        if (!Strings.isNullOrEmpty(dto.getTglDiterima())) {
            model.setTglDiterima(dateUtil.getDateFromISOString(dto.getTglDiterima()));
        }

        if (dto.getIdSurat() != null) {
            Surat surat = suratService.find(dto.getIdSurat());
            model.setSurat(surat);
        }

        model.setNomorDokumen(dto.getNomorDokumen());
        model.setTglDokumen(dateUtil.getDateFromISOString(dto.getTglDokumen()));
        model.setPerihal(dto.getPerihal());

        if (dto.getIdKlasifikasiKeamanan() != null) {
            MasterKlasifikasiKeamanan klasifikasi = klasifikasiKeamananService.find(dto.getIdKlasifikasiKeamanan());
            model.setKlasifikasiKeamanan(klasifikasi);
        }

        if (dto.getIdTingkatUrgensi() != null) {
            MasterTingkatUrgensi urgensi = tingkatUrgensiService.find(dto.getIdTingkatUrgensi());
            model.setTingkatUrgensi(urgensi);
        }

        if (dto.getIdTingkatPerkembangan() != null) {
            MasterTingkatPerkembangan perkembangan = tingkatPerkembanganService.find(dto.getIdTingkatPerkembangan());
            model.setTingkatPerkembangan(perkembangan);
        }


        return model;
    }
}
