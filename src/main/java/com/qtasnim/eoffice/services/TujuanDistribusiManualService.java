package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.TujuanDistribusiManual;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@LocalBean
public class TujuanDistribusiManualService extends AbstractFacade<TujuanDistribusiManual> {

    @Override
    protected Class<TujuanDistribusiManual> getEntityClass() {
        return TujuanDistribusiManual.class;
    }

    public JPAJinqStream<TujuanDistribusiManual> getAll() {
        return db();
    }

    public List<TujuanDistribusiManual> getTujuans(Long idDistribusi) {
        return db().where(a -> a.getDistribusi().getId().equals(idDistribusi)).toList();
    }
}
