package com.qtasnim.eoffice.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.FormDefinitionDto;
import com.qtasnim.eoffice.ws.dto.FormFieldDto;
import com.qtasnim.eoffice.ws.dto.IDynamicFormDto;
import com.qtasnim.eoffice.ws.dto.MetadataValueDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.print.attribute.standard.Media;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service untuk fungsi CRUD table m_user
 * 
 * @author seno@qtasnim.com
 */


 @Stateless
 @LocalBean
public class FormDefinitionService extends AbstractFacade<FormDefinition>{

    private static final long serialVersionUID = -4184209743573831159L;

    @Inject
    MasterJenisDokumenService masterJenisDokumenService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private FormValueService formValueService;
    
    @Inject
    private MasterJenisDokumenService jenisDokumenService;
    
    @Inject
    private MasterMetadataService masterMetaDataService;

    @Inject
    private FormFieldService formFieldService;

    @Override
    protected Class<FormDefinition> getEntityClass() {
        return FormDefinition.class;
    }

    public JPAJinqStream<FormDefinition> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }
    
    public FormDefinition save(Long id, FormDefinitionDto dao){
        FormDefinition model = new FormDefinition();
        DateUtil dateUtil = new DateUtil();

        if(id==null) {
            model.setVersion(dao.getVersion());
            MasterJenisDokumen jenis = masterJenisDokumenService.find(dao.getJenisDokumenId());
            model.setJenisDokumen(jenis);

            model.setName(dao.getName());
            model.setDescription(dao.getDescription());
            model.setIsActive(dao.getIsActive());
            model.setCreatedBy(dao.getCreatedBy());
            model.setCreatedDate(new Date());
            model.setModifiedBy("-");
            model.setModifiedDate(new Date());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.create(model);
        } else {

            model = this.find(id);
            model.setVersion(dao.getVersion());
            MasterJenisDokumen jenis = masterJenisDokumenService.find(dao.getJenisDokumenId());
            model.setJenisDokumen(jenis);
            model.setName(dao.getName());
            model.setDescription(dao.getDescription());
            model.setIsActive(dao.getIsActive());
            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);

        }
        getEntityManager().flush();
      return model;
    }

    public FormDefinition saveAsNewDefnition(FormDefinition sourceDefinition, Long newDocumentTypeId, Double version){
        FormDefinition model = sourceDefinition;

        model.setId(null);
        model.setVersion(version);
        MasterJenisDokumen jenis = masterJenisDokumenService.find(newDocumentTypeId);
        model.setJenisDokumen(jenis);

        this.create(model);

        return model;
    }


    public FormDefinition delete(Long id){
        FormDefinition model = this.find(id);
        this.remove(model);
        
        return model;
    }

    public List<FormDefinition> getByIdJenisDokumen (Long idJenisDokumen){
        MasterJenisDokumen jenisDokumen = jenisDokumenService.find(idJenisDokumen);
        Date n = new Date();
        return db().where(q->q.getJenisDokumen().equals(jenisDokumen) && n.after(q.getStart()) && n.before(q.getEnd())).toList();
    }

    public void saveDynamicForm(IDynamicFormDto dto, IDynamicFormEntity dynamicForm, Boolean isNew, AbstractFacade abstractFacade) throws Exception {
        FormDefinition activated;

        DateUtil dateUtil = new DateUtil();

        if (isNew) {
            Long idFormDef = dto.getIdFormDefinition();

            if (idFormDef == null) {
                return;
            }

            activated = find(idFormDef);

            List<FormField> required = activated.getFormFields().stream().filter(q->q.getIsRequired()).collect(Collectors.toList());
            if(!required.isEmpty()){
                Map<String,String> dynamicMap = new HashMap();
                for(FormField ff : required) {
                    if (!dto.getMetadata().stream().anyMatch(a->a.getName().equals(ff.getMetadata().getNama()))){
                        dynamicMap.put(ff.getMetadata().getLabel(),"Null/Empty Field");
                    }
                }
                Gson gsonBuilder = new GsonBuilder().create();
                String notify = gsonBuilder.toJson(dynamicMap);

                if (dynamicMap.entrySet().size() > 0)
                    throw new Exception(notify);
            }

            if (activated != null) {
                List<FormValue> result = activated.getFormFields().stream().map(t -> {
                    FormValue formValue = new FormValue();
                    formValue.setFormField(t);
                    formValue.setStart(new Date());
                    formValue.setEnd(dateUtil.getDefValue());
                    formValue.setValue(dto.getMetadata().stream().filter(m -> m.getName().toLowerCase().equals(t.getMetadata().getNama().toLowerCase()) && m.getValue() != null).map(MetadataValueDto::getValue).findFirst().orElse(null));
                    formValueService.create(formValue);
                    return formValue;
                }).collect(Collectors.toList());

                dynamicForm.setFormValue(result);

                abstractFacade.edit(dynamicForm);
            }
        } else {
            activated = dynamicForm.getFormDefinition();

            if (activated != null) {
                dynamicForm.getFormValue().forEach(t -> {
                    t.setValue(dto.getMetadata().stream().filter(m -> m.getName().equals(t.getFormField().getMetadata().getNama()) && m.getValue() != null).map(MetadataValueDto::getValue).findFirst().orElse(null));
                    formValueService.edit(t);
                });

                abstractFacade.edit(dynamicForm);
            }
        }
    }

    public void saveDynamicForm(String json, IDynamicFormEntity dynamicForm, Boolean isNew, AbstractFacade abstractFacade) throws Exception {
        Map<String,Object> values = getObjectMapper().readValue(json, new TypeReference<HashMap<String,Object>>() {});
        FormDefinition activated;

        DateUtil dateUtil = new DateUtil();

        if (isNew) {
            Object workflowFormName = values.get("workflowFormName");

            if (workflowFormName == null) {
                return;
            }

            activated = getActivated(workflowFormName.toString());

            List<FormField> required = activated.getFormFields().stream().filter(q->q.getIsRequired()).collect(Collectors.toList());
            if(!required.isEmpty()){
                Map<String,String> dynamicMap = new HashMap();
                for(FormField ff : required) {
                    if (!values.containsKey(ff)){
                        dynamicMap.put(ff.getMetadata().getLabel(),"Null/Empty Field");
                    }
                }
                Gson gsonBuilder = new GsonBuilder().create();
                String notify = gsonBuilder.toJson(dynamicMap);
                throw new Exception(notify);
            }

            if (activated != null) {
                List<FormValue> result = activated.getFormFields().stream().map(t -> {
                    FormValue formValue = new FormValue();
                    formValue.setFormField(t);
                    formValue.setStart(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate()));
                    formValue.setEnd(dateUtil.getDefValue());
                    formValueService.create(formValue);
                    return formValue;
                }).collect(Collectors.toList());

                dynamicForm.setFormValue(result);

                abstractFacade.edit(dynamicForm);
            }
        } else {
            activated = dynamicForm.getFormDefinition();

            if (activated != null) {
                dynamicForm.getFormValue().forEach(t -> {
                    t.setValue(Optional.ofNullable(values.get(t.getFormField().getMetadata().getNama())).map(Object::toString).orElse(null));
                    formValueService.edit(t);
                });

                abstractFacade.edit(dynamicForm);
            }
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    public FormDefinition getActivated(String name) {
        Date now = new Date();

        return db()
                .where(t ->
                        t.getName().equals(name)
                                && (now.after(t.getStart()) && now.before(t.getEnd()))
                )
                .filter(t -> t.getVersion() % 1 == 0)
                .max(Comparator.comparing(FormDefinition::getId))
                .orElse(null);

    }

    public JPAJinqStream<FormDefinition> getPublished() {
        Date now = new Date();

        return db()
                .where(t -> t.getIsActive() && (now.after(t.getStart()) && now.before(t.getEnd())));

    }

    public JPAJinqStream<FormDefinition> getPublished(Long jenisDokumenId) {
        Date now = new Date();

        return db()
                .where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(jenisDokumenId) && t.getIsActive() && (now.after(t.getStart()) && now.before(t.getEnd())));

    }

    public JPAJinqStream<FormDefinition> findByJenisDokumenId(Long jenisDokumenId) {
        return db()
                .where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(jenisDokumenId));

    }

    public JPAJinqStream<FormDefinition> getHistory(Long jenisDokumenId) {
        return db()
                .where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(jenisDokumenId))
                .sortedDescendingBy(FormDefinition::getCreatedDate);
    }

    public FormDefinition getByVersion(Long jenisDokumenId, Double version) {
        Date now = new Date();

        return db()
                .where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(jenisDokumenId) && t.getVersion().equals(version))
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public FormDefinition getActivated(Long jenisDokumenId) {
        Date now = new Date();

        return db()
                .where(t ->
                        t.getJenisDokumen().getIdJenisDokumen().equals(jenisDokumenId)
                                && (now.after(t.getStart()) && now.before(t.getEnd()))
                                && t.getIsActive()
                )
                .sortedDescendingBy(FormDefinition::getVersion)
                .limit(1)
                .findFirst()
                .orElse(null);

    }

    public FormDefinition getLatest(Long jenisDokumenId) {
        Date now = new Date();

        return db()
                .where(t ->
                        t.getJenisDokumen().getIdJenisDokumen().equals(jenisDokumenId)
                                && (now.after(t.getStart()) && now.before(t.getEnd()))
                )
                .sortedDescendingBy(FormDefinition::getVersion)
                .limit(1)
                .findFirst()
                .orElse(null);

    }

    public FormDefinition saveOrSubmit(Long jenisDokumenId, FormDefinitionDto dao, List<FormFieldDto> dtos, double nextVersion) throws Exception {
        
        try {
            FormDefinition newForm = this.save(dao.getId(), dao);

            for (FormFieldDto dto : dtos){
                if(dto.getIsDeleted()) {
                     FormField field = formFieldService.find(dto.getId());
                     if (field != null) {
                         formFieldService.remove(field);
                         getEntityManager().flush();
                     }
                } else {
                    formFieldService.save(dto.getId(), dto, newForm);
                }
            }

            getEntityManager().refresh(newForm);
    
            return newForm;
        } catch(Exception ex) {
            throw new Exception(ex.getMessage());
        }
        
    }

    public FormDefinition saveAsNewTemplate(FormDefinition sourceDocument, Long jenisDokumenId, double nextVersion) throws Exception {
        
        try {
            FormDefinition newForm = this.saveAsNewDefnition(sourceDocument, jenisDokumenId, nextVersion);
            formFieldService.saveAsNewField(sourceDocument, newForm);

            getEntityManager().refresh(newForm);
    
            return newForm;
        } catch(Exception ex) {
            throw new Exception(ex.getMessage());
        }
        
    }

}