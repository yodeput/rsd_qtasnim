package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.TembusanPara;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class TembusanParaService extends AbstractFacade<TembusanPara>{

    @Override
    protected Class<TembusanPara> getEntityClass() {
        return TembusanPara.class;
    }

    public JPAJinqStream<TembusanPara> getAll() {return db();}

    public List<TembusanPara> getListTembusanPara(Long idTembusanSurat){
        return db().where(a->a.getTembusanSurat().getId().equals(idTembusanSurat)).collect(Collectors.toList());
    }
}
