package com.qtasnim.eoffice.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.PasswordHash;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterStrukturOrganisasiDto;
import com.qtasnim.eoffice.ws.dto.MasterUserDto;
import com.qtasnim.eoffice.ws.dto.ProfileUserDto;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.jinq.jpa.JPAJinqStream;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service untuk fungsi CRUD table m_user
 *
 * @author seno@qtasnim.com
 */


@Stateless
@LocalBean
public class MasterUserService extends AbstractFacade<MasterUser> {

    private static final long serialVersionUID = -4184209743573831159L;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private MasterVendorService masterVendorService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterAreaService areaService;

    @Inject
    private MasterKorsaService korsaService;

    @Inject
    private MasterGradeService gradeService;

    @Inject
    private PasswordHash passwordHash;

    @Inject
    private BackgroundServiceLoggerService loggerService;

    @Inject
    private OrganisationSyncScheduler organisationSyncScheduler;

    @Inject
    private MasterRoleService roleService;

    @Inject
    private Logger logger;

    @Override
    protected Class<MasterUser> getEntityClass() {
        return MasterUser.class;
    }

    public JPAJinqStream<MasterUser> getAll() {
        return db();
    }

    public JPAJinqStream<MasterUser> getAllValid() {
        Date n = new Date();
        return db().where(q -> q.getIsActive() && !q.getIsDeleted() && n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterUser> findAllInternal() {
        return db().where(MasterUser::getIsInternal);
    }

    public MasterUser findUser(String username) {
        return db().where(q -> q.getLoginUserName().equals(username) && q.getIsInternal() &&!q.getIsDeleted()).findFirst().orElse(null);
    }

    public MasterUser findUserByUsername(String username) {
        return db().where(t -> t.getLoginUserName().equals(username) && t.getIsActive() && !t.getIsDeleted()).findFirst().orElse(null);
    }

    public void saveEksternal(Long id, MasterUserDto dao, String password) throws Exception {
        MasterUser model = new MasterUser();
        if (id == null){
            List<String> keys = passwordHash.getSaltPassword(password);
            String hash = keys.get(1);
            String salt = keys.get(0);

            model.setPasswordHash(hash);
            model.setPasswordSalt(salt);
        }else{
            model = this.find(id);
            if(!Strings.isNullOrEmpty(password)) {
                List<String> keys = passwordHash.getSaltPassword(password);
                String hash = keys.get(1);
                String salt = keys.get(0);

                model.setPasswordHash(hash);
                model.setPasswordSalt(salt);
            }
        }
        saveUser(id, dao, model);
    }

    public void saveOrEdit(Long id, MasterUserDto dao) throws Exception {
        MasterUser model = new MasterUser();
        if (id == null){
            model.setPasswordHash("19a438e31e797e1bddef6c26491ec45");
            model.setPasswordSalt("cf4365595f6b6f29449d222d9b7157");
        }
        saveUser(id, dao, model);
    }

    public void saveOrEdit2(Long id, MasterUserDto dao) throws Exception {
        MasterUser model = new MasterUser();
        if (id == null){
            List<String> keys = passwordHash.getSaltPassword(dao.getPassHash());
            String hash = keys.get(1);
            String salt = keys.get(0);

            model.setPasswordHash(hash);
            model.setPasswordSalt(salt);
        }else{
            model = this.find(id);
        }
        saveUser(id, dao, model);
    }

    public void saveUser(Long id, MasterUserDto dao, MasterUser model) throws Exception {
        boolean isNew = id == null;
        DateUtil dateUtil = new DateUtil();
        try {
            model.setLoginUserName(dao.getUsername());
            model.setEmployeeId(dao.getEmployeeId());
            model.setNameFront(dao.getNameFront());
            model.setNameMiddleLast(dao.getNameMiddleLast());
            model.setSalutation(dao.getSalutation());
            model.setEmail(dao.getEmail());
            model.setOtherContactInformation(dao.getOtherInformation());
            model.setDescription(dao.getDescription());
            model.setStatus("");
            model.setIsActive(dao.getIsActive());
            model.setBusinessArea(dao.getBusinessArea());
            model.setIsInternal(dao.getIsInternal());
            model.setJabatan(dao.getJabatan());
            model.setKedudukan(dao.getKedudukan());
            model.setKelamin(dao.getKelamin());
            model.setTempatLahir(dao.getTempatLahir());
            model.setTanggalLahir(dateUtil.getDateFromISOString(dao.getTanggalLahir()));
            model.setMobilePhone(dao.getMobilePhone());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if (dao.getEndDate() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            if(!Strings.isNullOrEmpty(dao.getPhoto())){
                model.setPhoto(Base64.getDecoder().decode(dao.getPhoto()));
            }

            if (dao.getCompanyId() != null) {
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            }

            if (dao.getAreaId() != null) {
                MasterArea area = areaService.find(dao.getAreaId());
                model.setArea(area);
            }
            if (dao.getKorsaId() != null) {
                MasterKorsa korsa = korsaService.find(dao.getKorsaId());
                model.setKorsa(korsa);
            }

            if (dao.getGradeId() != null) {
                MasterGrade grade = gradeService.find(dao.getGradeId());
                model.setGrade(grade);
            }

            if(!Strings.isNullOrEmpty(dao.getAgama())) {
                model.setAgama(dao.getAgama());
            }


            if (isNew) {
                model.setIsDeleted(false);

                create(model);
                getEntityManager().flush();
                getEntityManager().refresh(model);

                boolean valid;
                if (dao.getOrganization() != null) {
                    if (isNew) {
                        valid = true;
                    } else {
                        valid = model.getOrganizationEntity()!=null ? !model.getOrganizationEntity().getIdOrganization().equals(dao.getOrganization()) : true;
                    }

                    if (valid) {
                        MasterUserDto dto = this.getByOrgId(dao.getOrganization());
                        if (dto != null) {
                            throw new Exception("Organisasi sudah digunakan");
                        } else {
                            MasterStrukturOrganisasi mOrg = masterStrukturOrganisasiService.find(dao.getOrganization());
                            model.setOrganizationEntity(mOrg);
                            edit(model);
                            mOrg.setUser(model);
                            masterStrukturOrganisasiService.edit(mOrg);
                        }
                    }
                }else{
                    model.setOrganizationEntity(null);
                    model.setJabatan("No Position");
                    model.setKedudukan("No Position");
                }
            } else {
                if(dao.getOrganization()==null && model.getOrganizationEntity()!=null){
                    MasterStrukturOrganisasi tempOrg = model.getOrganizationEntity();
                    tempOrg.setUser(null);
                    masterStrukturOrganisasiService.edit(tempOrg);
                    model.setOrganizationEntity(null);
                    model.setJabatan("No Position");
                    model.setKedudukan("No Position");
                }
                if(dao.getOrganization()!=null){
                    MasterStrukturOrganisasi tempOrg = masterStrukturOrganisasiService.getByIdOrganisasi(dao.getOrganization());
                    tempOrg.setUser(model);
                    masterStrukturOrganisasiService.edit(tempOrg);
                    model.setOrganizationEntity(tempOrg);
                    model.setJabatan(tempOrg.getOrganizationName());
                    model.setKedudukan(tempOrg.getOrganizationName());
                }
                edit(model);
            }

            if(model.getOrganizationEntity()!=null) {
                MasterStrukturOrganisasi organisasi = model.getOrganizationEntity();
                List<MasterRole> existing = organisasi.getRoleList();
                List<MasterRole> newList = new ArrayList<>();
                if (dao.getRoles() != null && !dao.getRoles().isEmpty()) {
                    dao.getRoles().stream().filter(r -> !r.getRoleName().toLowerCase().equals("user")).forEach(roleDto -> {
                        MasterRole role = roleService.find(roleDto.getIdRole());
                        newList.add(role);
//                    organisasi.getRoleList().add(role);
//                    masterStrukturOrganisasiService.edit(organisasi);
                    });
                }

                Collection<MasterRole> similar = new HashSet<>(existing);
                similar.retainAll(newList);

                existing.removeAll(similar);
//            newList.removeAll(similar);

                if (!existing.isEmpty()) {
                    organisasi.getRoleList().removeAll(existing);
                    masterStrukturOrganisasiService.edit(organisasi);
                }

                if (!newList.isEmpty()) {
                    organisasi.getRoleList().addAll(newList);
                    masterStrukturOrganisasiService.edit(organisasi);
                }
            }
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public MasterUser delete(Long id) {
        MasterUser model = this.find(id);

        model.setIsActive(false);
        model.setIsDeleted(true);
        model.setKedudukan("No Position");
        model.setJabatan("No Position");
        model.setOrganizationEntity(null);
        this.edit(model);
        getEntityManager().flush();
        getEntityManager().refresh(model);
        return model;
    }

    public MasterUserDto getByOrgId(long idOrganization) {
        return db().where(q -> q.getOrganizationEntity().getIdOrganization() == idOrganization).map(a -> new MasterUserDto(a)).findFirst()
                .orElse(null);
    }

    public MasterUserDto getByEmailAdd(String email) {
        return db().where(q -> q.getEmail().equals(email) && q.getIsActive() && !q.getIsDeleted()).map(q -> new MasterUserDto(q)).findFirst().orElse(null);
    }

    public void savePattern(long id, MasterUserDto dao) {
        MasterUser user = this.find(id);
        user.setKeyPattern(dao.getKeyPattern());
        this.edit(user);
    }

    public void saveFinger(long id, MasterUserDto dao) {
        MasterUser user = this.find(id);
        user.setFingerprint(dao.getFingerprint());
        this.edit(user);
    }

//    @Transactional(value = Transactional.TxType.REQUIRES_NEW)
    public void sync(List<UserWrapper> api) {
        SyncLog syncLog = organisationSyncScheduler.getSyncLog();
        sync(api, syncLog);
    }

    public void sync(List<UserWrapper> api, SyncLog syncLog) {
        Instant start = Instant.now();
        loggerService.info(syncLog, String.format("Start saving user | %s", new Date()));

        Date now = new Date();
        DateUtil dateUtil = new DateUtil();
        List<MasterStrukturOrganisasi> strukturOrganisasis = ((List<MasterStrukturOrganisasi>) getEntityManager().createQuery("SELECT A FROM MasterStrukturOrganisasi A LEFT OUTER JOIN FETCH A.user B LEFT OUTER JOIN FETCH A.parent C").getResultList());
        List<CompanyCode> companyCodes = companyCodeService.findAll();
        List<MasterGrade> masterGrades = gradeService.findAll();
        List<MasterArea> masterAreas = areaService.findAll();
        List<MasterKorsa> masterKorsas = korsaService.findAll();
        List<UserWrapper> db = ((List<MasterUser>) getEntityManager().createQuery("SELECT A FROM MasterUser A WHERE A.isInternal = TRUE").getResultList())
                .stream()
                .map(t -> {
                    UserWrapper userWrapper = new UserWrapper();

                    userWrapper.setMasterUser(t);
                    userWrapper.setEmployeeDto(null);
                    userWrapper.setEmployeeId(t.getEmployeeId());
                    userWrapper.setFullName(String.format("%s %s", t.getNameFront(), t.getNameMiddleLast()).trim());
                    userWrapper.setNameFront(t.getNameFront());
                    userWrapper.setNameMiddleLast(t.getNameMiddleLast());
                    userWrapper.setEmail(t.getEmail());
                    userWrapper.setSalutation(t.getSalutation());
                    userWrapper.setUsername(t.getLoginUserName());
                    userWrapper.setBusinessArea(t.getBusinessArea());
                    userWrapper.setKelamin(t.getKelamin());
                    userWrapper.setKedudukan(t.getKedudukan());
                    userWrapper.setTglLahir(t.getTanggalLahir());
                    userWrapper.setTempatLahir(t.getTempatLahir());
                    userWrapper.setAgama(t.getAgama());
                    userWrapper.setKedudukan(t.getKedudukan());
                    userWrapper.setJabatan(t.getJabatan());
                    userWrapper.setMobilePhone(t.getMobilePhone());

                    Optional.ofNullable(t.getArea()).ifPresent(area -> {
                        userWrapper.setPersonalArea(area.getNama());
                        userWrapper.setPersonalAreaCode(area.getId());
                    });
                    Optional.ofNullable(t.getKorsa()).ifPresent(korsa -> {
                        userWrapper.setKorsa(korsa.getNama());
                        userWrapper.setKorsaId(korsa.getId());
                    });
                    Optional.ofNullable(t.getGrade()).ifPresent(grade -> {
                        userWrapper.setGrade(grade.getId());
                    });
                    Optional.ofNullable(t.getOrganizationEntity()).ifPresent(strukturOrganisasi -> {
                        userWrapper.setOrganizationCode(strukturOrganisasi.getOrganizationCode());
                        userWrapper.setOrganizationName(strukturOrganisasi.getOrganizationName());
                    });
                    Optional.ofNullable(t.getCompanyCode()).ifPresent(companyCode -> {
                        userWrapper.setCompanyCode(companyCode.getCode());
                        userWrapper.setCompanyName(companyCode.getName());
                    });

                    userWrapper.setIsActive(t.getIsActive());

                    return userWrapper;
                })
                .collect(Collectors.toList());

        //PROP CHANGED
        EmailValidator emailValidator = EmailValidator.getInstance();
        List<UserWrapper> propChanged = db.stream()
                .filter(t -> api.stream().anyMatch(a ->
                        t.getMasterUser().getIsActive() && now.after(t.getMasterUser().getStart()) && now.before(t.getMasterUser().getEnd())
                                && a.getUsername().equals(t.getUsername())
                                && (
                                !Optional.ofNullable(a.getEmail()).filter(emailValidator::isValid).orElse("-").equals(Optional.ofNullable(t.getEmail()).filter(emailValidator::isValid).orElse("-"))
                                        || !Optional.ofNullable(a.getBusinessArea()).orElse("").equals(Optional.ofNullable(t.getBusinessArea()).orElse(""))
                                        || !Optional.ofNullable(a.getKedudukan()).orElse("").equals(Optional.ofNullable(t.getKedudukan()).orElse(""))
                                        || !Optional.ofNullable(a.getJabatan()).orElse("").equals(Optional.ofNullable(t.getJabatan()).orElse(""))
                                        || !Optional.ofNullable(a.getTempatLahir()).orElse("").equals(Optional.ofNullable(t.getTempatLahir()).orElse(""))
                                        || !Optional.ofNullable(a.getAgama()).orElse("").equals(Optional.ofNullable(t.getAgama()).orElse(""))
                                        || !Optional.ofNullable(a.getKelamin()).orElse("").equals(Optional.ofNullable(t.getKelamin()).orElse(""))
                                        || !Optional.ofNullable(a.getGrade()).orElse("").equals(Optional.ofNullable(t.getGrade()).orElse(""))
                                        || !Optional.ofNullable(a.getKorsa()).orElse("").equals(Optional.ofNullable(t.getKorsa()).orElse(""))
                                        || !Optional.ofNullable(a.getMobilePhone()).orElse("").equals(Optional.ofNullable(t.getMobilePhone()).orElse(""))
                                        || Optional.ofNullable(a.getTglLahir()).orElse(new Date(Long.MIN_VALUE)).compareTo(Optional.ofNullable(t.getTglLahir()).orElse(new Date(Long.MIN_VALUE))) == 0
                                        || !Optional.ofNullable(a.getPersonalArea()).orElse("").equals(Optional.ofNullable(t.getPersonalArea()).orElse(""))
                                        || !a.getFullName().equals(t.getFullName())
                                        || !a.getSalutation().equals(t.getSalutation())
                                        || (a.getTglLahir() != null && t.getTglLahir() != null && a.getTglLahir().compareTo(t.getTglLahir()) != 0)
                                        || !Optional.ofNullable(t.getMasterUser().getOrganizationEntity()).map(MasterStrukturOrganisasi::getOrganizationCode).orElse("-").equals(a.getOrganizationCode())
                                        || !Optional.ofNullable(t.getMasterUser().getCompanyCode()).map(CompanyCode::getCode).orElse("-").equals(a.getCompanyCode())
                        )
                ))
                .collect(Collectors.toList());
        loggerService.info(syncLog, "Updated User count: " + propChanged.size());

        propChanged
                .forEach(t -> {
                    UserWrapper updated = api.stream().filter(a -> a.getUsername().equals(t.getUsername())).findFirst().orElse(new UserWrapper());
                    CompanyCode companyCode = getOrCreateCompanyCode(companyCodes, updated.getCompanyCode(), updated.getCompanyName());
                    MasterStrukturOrganisasi masterStrukturOrganisasi = strukturOrganisasis.stream().filter(m -> m.getOrganizationCode().equals(updated.getOrganizationCode())).findFirst().orElse(null);
                    MasterArea masterArea = getOrCreateMasterArea(masterAreas, updated.getPersonalAreaCode(), updated.getPersonalArea(), companyCode);
                    MasterGrade masterGrade = getOrCreateMasterGrade(masterGrades, updated.getGrade(), updated.getGrade(), companyCode);
                    MasterKorsa masterKorsa = getOrCreateMasterKorsa(masterKorsas, updated.getKorsaId(), updated.getKorsaAbbr(), updated.getKorsa(), companyCode);

                    if (masterStrukturOrganisasi != null && !Optional.ofNullable(masterStrukturOrganisasi.getCompanyCode()).orElse(new CompanyCode(-1L)).getId().equals(Optional.ofNullable(companyCode).orElse(new CompanyCode(-1L)).getId())) {
                        masterStrukturOrganisasi.setCompanyCode(companyCode);

                        getEntityManager().merge(masterStrukturOrganisasi);
                    }

                    MasterUser masterUser = t.getMasterUser();
                    masterUser.setEmail(Optional.ofNullable(updated.getEmail()).filter(emailValidator::isValid).orElse("-"));
                    masterUser.setBusinessArea(updated.getBusinessArea());
                    masterUser.setKedudukan(updated.getKedudukan());
                    masterUser.setNameFront(updated.getNameFront());
                    masterUser.setNameMiddleLast(updated.getNameMiddleLast());
                    masterUser.setSalutation(updated.getSalutation());
                    masterUser.setOrganizationEntity(masterStrukturOrganisasi);
                    masterUser.setCompanyCode(companyCode);
                    masterUser.setTanggalLahir(updated.getTglLahir());
                    masterUser.setJabatan(updated.getJabatan());
                    masterUser.setKedudukan(updated.getKedudukan());
                    masterUser.setTempatLahir(updated.getTempatLahir());
                    masterUser.setAgama(updated.getAgama());
                    masterUser.setKelamin(updated.getKelamin());
                    masterUser.setMobilePhone(updated.getMobilePhone());
                    masterUser.setGrade(masterGrade);
                    masterUser.setArea(masterArea);
                    masterUser.setKorsa(masterKorsa);

                    edit(masterUser);
                });

        if (propChanged.size() > 0) {
            flushEntityManager();
        }

        //DELETED
        List<UserWrapper> deleted = db.stream()
                .filter(t ->
                        t.getMasterUser().getIsActive() && now.after(t.getMasterUser().getStart()) && now.before(t.getMasterUser().getEnd())
                                && (api.stream().noneMatch(a -> a.getUsername().equals(t.getUsername()))) //belum ada
                )
                .collect(Collectors.toList());
        loggerService.info(syncLog, "Deleted User count: " + deleted.size());

        deleted
                .forEach(t -> {
                    MasterUser masterUser = t.getMasterUser();
                    masterUser.setIsActive(false);
                    masterUser.setIsDeleted(true);
                    masterUser.setEnd(now);
                    masterUser.setOrganizationEntity(null);

                    edit(masterUser);
                });

        if (deleted.size() > 0) {
            flushEntityManager();
        }

        //NOT ACTIVATED
        List<UserWrapper> inactive = db
                .stream()
                .filter(t -> !(t.getMasterUser().getIsActive() && now.after(t.getMasterUser().getStart()) && now.before(t.getMasterUser().getEnd())))
                .collect(Collectors.toList());

        //CREATED
        int[] batchIndex = {0};
        List<UserWrapper> created = api.stream()
                .filter(a -> a.getIsActive() && ( (db.stream().noneMatch(t ->
                        t.getMasterUser().getIsActive()
                                && now.after(t.getMasterUser().getStart())
                                && now.before(t.getMasterUser().getEnd())
                                && a.getUsername().equals(t.getUsername())))
                 || (db.stream().anyMatch(t->t.getUsername().equals(a.getUsername()) && !t.getIsActive())) ))
                .collect(Collectors.toList());
        loggerService.info(syncLog, "Created User count: " + created.size());

        created.forEach(t -> {
            UserWrapper userWrapper = inactive.stream().filter(a -> t.getUsername().equals(a.getUsername())).findFirst().orElse(null);
            CompanyCode companyCode = getOrCreateCompanyCode(companyCodes, t.getCompanyCode(), t.getCompanyName());
            MasterStrukturOrganisasi masterStrukturOrganisasi = strukturOrganisasis.stream().filter(m -> m.getOrganizationCode().equals(t.getOrganizationCode())).findFirst().orElse(null);
            MasterArea masterArea = getOrCreateMasterArea(masterAreas, t.getPersonalAreaCode(), t.getPersonalArea(), companyCode);
            MasterGrade masterGrade = getOrCreateMasterGrade(masterGrades, t.getGrade(), t.getGrade(), companyCode);
            MasterKorsa masterKorsa = getOrCreateMasterKorsa(masterKorsas, t.getKorsaId(), t.getKorsaAbbr(), t.getKorsa(), companyCode);
            MasterUser masterUser;

            if (masterStrukturOrganisasi != null && !Optional.ofNullable(masterStrukturOrganisasi.getCompanyCode()).orElse(new CompanyCode(-1L)).getId().equals(Optional.ofNullable(companyCode).orElse(new CompanyCode(-1L)).getId())) {
                masterStrukturOrganisasi.setCompanyCode(companyCode);

                getEntityManager().merge(masterStrukturOrganisasi);
            }

            if (userWrapper != null) { //re-use inactivated org
                masterUser = userWrapper.getMasterUser();

                masterUser.setEmail(Optional.ofNullable(t.getEmail()).filter(emailValidator::isValid).orElse("-"));
                masterUser.setNameFront(t.getNameFront());
                masterUser.setNameMiddleLast(t.getNameMiddleLast());
                masterUser.setSalutation(t.getSalutation());
                masterUser.setCompanyCode(companyCode);
                masterUser.setOrganizationEntity(masterStrukturOrganisasi);
                masterUser.setKedudukan(t.getKedudukan());
                masterUser.setBusinessArea(t.getBusinessArea());
                masterUser.setEnd(dateUtil.getDefValue());
                masterUser.setTanggalLahir(t.getTglLahir());
                masterUser.setIsActive(t.getIsActive());
                masterUser.setIsDeleted(false);
                masterUser.setJabatan(t.getJabatan());
                masterUser.setKedudukan(t.getKedudukan());
                masterUser.setTempatLahir(t.getTempatLahir());
                masterUser.setAgama(t.getAgama());
                masterUser.setKelamin(t.getKelamin());
                masterUser.setGrade(masterGrade);
                masterUser.setArea(masterArea);
                masterUser.setKorsa(masterKorsa);
                masterUser.setIsInternal(true);
                masterUser.setMobilePhone(t.getMobilePhone());

                edit(masterUser);
            } else { //completely new
                masterUser = new MasterUser();
                masterUser.setEmail(Optional.ofNullable(t.getEmail()).filter(emailValidator::isValid).orElse("-"));
                masterUser.setEmployeeId(t.getEmployeeId());
                masterUser.setLoginUserName(t.getUsername());
                masterUser.setNameFront(t.getNameFront());
                masterUser.setNameMiddleLast(t.getNameMiddleLast());
                masterUser.setSalutation(t.getSalutation());
                masterUser.setPasswordSalt("cf4365595f6b6f29449d222d9b7157");
                String hash = passwordHash.getHash("pass@word1", masterUser.getPasswordSalt());
                masterUser.setPasswordHash(hash);
                masterUser.setCompanyCode(companyCode);
                masterUser.setOrganizationEntity(masterStrukturOrganisasi);
                masterUser.setKedudukan(t.getKedudukan());
                masterUser.setBusinessArea(t.getBusinessArea());
                masterUser.setStart(now);
                masterUser.setEnd(dateUtil.getDefValue());
                masterUser.setTanggalLahir(t.getTglLahir());
                masterUser.setIsActive(t.getIsActive());
                masterUser.setIsDeleted(false);
                masterUser.setJabatan(t.getJabatan());
                masterUser.setKedudukan(t.getKedudukan());
                masterUser.setTempatLahir(t.getTempatLahir());
                masterUser.setAgama(t.getAgama());
                masterUser.setKelamin(t.getKelamin());
                masterUser.setGrade(masterGrade);
                masterUser.setArea(masterArea);
                masterUser.setKorsa(masterKorsa);
                masterUser.setIsInternal(true);
                masterUser.setMobilePhone(t.getMobilePhone());

                create(masterUser);
            }

            Optional.ofNullable(masterStrukturOrganisasi).ifPresent(so -> {
                if (!Optional.ofNullable(so.getArea()).map(MasterArea::getId).orElse("").equals(Optional.ofNullable(masterArea).map(MasterArea::getId).orElse(""))) {
                    so.setArea(masterArea);
                    getEntityManager().merge(so);
                }
            });

            if ((batchIndex[0] % 30) == 0) {
                flushEntityManager();
            }

            batchIndex[0] = batchIndex[0] + 1;
        });

        if ((batchIndex[0] % 30) != 0) {
            flushEntityManager();
        }

        //Clean up 99999999 org code
        db().where(t -> t.getOrganizationEntity() != null && t.getOrganizationEntity().getOrganizationCode().equals("99999999")).toList().forEach(t -> {
            t.setOrganizationEntity(null);
            getEntityManager().merge(t);
        });

        //Clean up In active
        db().where(t -> t.getOrganizationEntity() != null && !t.getIsActive()).toList().forEach(t -> {
            t.setOrganizationEntity(null);
            getEntityManager().merge(t);
        });

        db().where(t -> !t.getIsActive() && t.getOrganizationEntity() == null && !t.getKedudukan().equals("No Position")).toList().forEach(t -> {
            t.setKedudukan("No Position");
            getEntityManager().merge(t);
        });

        Instant end = Instant.now();
        loggerService.info(syncLog, String.format("User sync completed on: %s seconds", Duration.between(start, end).getSeconds()));
        loggerService.info(syncLog, String.format("End saving user | %s", new Date()));
    }

    private void flushEntityManager() {
//        try {
//            getEntityManager().flush();
//        } catch (Exception e) {
//
//        }
    }

    private CompanyCode getOrCreateCompanyCode(List<CompanyCode> companyCodes, String code, String name) {
        CompanyCode companyCode = null;

        if (StringUtils.isNotEmpty(code) && StringUtils.isNotEmpty(name)) {
            companyCode = companyCodes.stream().filter(t -> t.getCode().equals(code)).findFirst().orElse(null);

            if (companyCode == null) {
                Date now = new Date();
                DateUtil dateUtil = new DateUtil();
                companyCode = new CompanyCode();
                companyCode.setName(name);
                companyCode.setCode(code);
                companyCode.setAddress("-");
                companyCode.setEmail("-");
                companyCode.setUrl("-");
                companyCode.setDescription("-");
                companyCode.setIsActive(true);
                companyCode.setStart(now);
                companyCode.setEnd(dateUtil.getDefValue());

                getEntityManager().persist(companyCode);

                companyCodes.add(companyCode);
            }
        }

        return companyCode;
    }

    private MasterArea getOrCreateMasterArea(List<MasterArea> masterAreas, String id, String name, CompanyCode companyCode) {
        MasterArea masterArea = null;

        if (StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(name)) {
            masterArea = masterAreas.stream().filter(t -> t.getId().equals(id)).findFirst().orElse(null);

            if (masterArea == null) {
                Date now = new Date();
                DateUtil dateUtil = new DateUtil();
                masterArea = new MasterArea();
                masterArea.setId(id);
                masterArea.setNama(name);
                masterArea.setStart(now);
                masterArea.setEnd(dateUtil.getDefValue());
                masterArea.setCompanyCode(companyCode);

                getEntityManager().persist(masterArea);

                masterAreas.add(masterArea);
            } else if (
                    !Optional.ofNullable(name).orElse("").equals(Optional.ofNullable(masterArea.getNama()).orElse(""))
                    || !Optional.ofNullable(masterArea.getCompanyCode()).orElse(new CompanyCode(-1L)).getId().equals(Optional.ofNullable(companyCode).orElse(new CompanyCode(-1L)).getId())) {
                masterArea.setCompanyCode(companyCode);
                masterArea.setNama(name);

                getEntityManager().merge(masterArea);
            }
        }

        return masterArea;
    }

    private MasterKorsa getOrCreateMasterKorsa(List<MasterKorsa> masterKorsas, String id, String kode, String name, CompanyCode companyCode) {
        MasterKorsa masterKorsa = null;

        if (StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(kode)) {
            masterKorsa = masterKorsas.stream().filter(t -> t.getId().equals(id)).findFirst().orElse(null);

            if (masterKorsa == null) {
                Date now = new Date();
                DateUtil dateUtil = new DateUtil();
                masterKorsa = new MasterKorsa();
                masterKorsa.setId(id);
                masterKorsa.setNama(name);
                masterKorsa.setKode(kode);
                masterKorsa.setStart(now);
                masterKorsa.setEnd(dateUtil.getDefValue());
                masterKorsa.setCompanyCode(companyCode);

                getEntityManager().persist(masterKorsa);

                masterKorsas.add(masterKorsa);
            } else if (
                    !Optional.ofNullable(kode).orElse("").equals(Optional.ofNullable(masterKorsa.getKode()).orElse(""))
                    || !Optional.ofNullable(name).orElse("").equals(Optional.ofNullable(masterKorsa.getNama()).orElse(""))
                    || !Optional.ofNullable(masterKorsa.getCompanyCode()).orElse(new CompanyCode(-1L)).getId().equals(Optional.ofNullable(companyCode).orElse(new CompanyCode(-1L)).getId())) {
                masterKorsa.setCompanyCode(companyCode);
                masterKorsa.setNama(name);
                masterKorsa.setKode(kode);

                getEntityManager().merge(masterKorsa);
            }
        }

        return masterKorsa;
    }

    private MasterGrade getOrCreateMasterGrade(List<MasterGrade> masterGrades, String id, String name, CompanyCode companyCode) {
        MasterGrade masterKorsa = null;

        if (StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(name)) {
            masterKorsa = masterGrades.stream().filter(t -> t.getId().equals(id)).findFirst().orElse(null);

            if (masterKorsa == null) {
                Date now = new Date();
                DateUtil dateUtil = new DateUtil();
                masterKorsa = new MasterGrade();
                masterKorsa.setId(id);
                masterKorsa.setNama(name);
                masterKorsa.setStart(now);
                masterKorsa.setEnd(dateUtil.getDefValue());
                masterKorsa.setCompanyCode(companyCode);

                getEntityManager().persist(masterKorsa);

                masterGrades.add(masterKorsa);
            } else if (
                    !Optional.ofNullable(name).orElse("").equals(Optional.ofNullable(masterKorsa.getNama()).orElse(""))
                    || !Optional.ofNullable(masterKorsa.getCompanyCode()).orElse(new CompanyCode(-1L)).getId().equals(Optional.ofNullable(companyCode).orElse(new CompanyCode(-1L)).getId())) {
                masterKorsa.setCompanyCode(companyCode);
                masterKorsa.setNama(name);

                getEntityManager().merge(masterKorsa);
            }
        }

        return masterKorsa;
    }

    public ProfileUserDto getProfile(MasterUser user) {
        ProfileUserDto profile = new ProfileUserDto();
        if (user != null) {
            if(user.getIsInternal()) {
                MasterStrukturOrganisasiDto orgDto = masterStrukturOrganisasiService.getByUserId(user.getId());
                profile.setJabatan(orgDto.getOrganizationName());
                String namaNipp = user.getNameFront() + " " + user.getNameMiddleLast() + " | " + user.getEmployeeId();
                profile.setNamaNip(namaNipp);
                if (user.getPhoto() != null) {
                    profile.setPhoto(Base64.getEncoder().encodeToString(user.getPhoto()));
                }
            }else{
                MasterVendor vendor = user.getVendor();
                profile.setJabatan(vendor.getNama());
                String namaNipp = user.getNameFront() + " " + user.getNameMiddleLast();
                profile.setNamaNip(namaNipp);
                if (user.getPhoto() != null) {
                    profile.setPhoto(Base64.getEncoder().encodeToString(user.getPhoto()));
                }
            }
        }

        return profile;
    }

    public JPAJinqStream<MasterUser> getByName(String name) {
        JPAJinqStream<MasterUser> query =  db().where(q -> q.getIsActive() && !q.getIsDeleted());
        query = query.where(q -> q.getNameFront().toLowerCase().contains(name.toLowerCase()) || q.getNameMiddleLast()
                .toLowerCase().contains(name.toLowerCase()) || (q.getNameFront().toLowerCase()+" "+q.getNameMiddleLast()
                .toLowerCase()).contains(name.toLowerCase()));
        return query;
    }

    public boolean validate(Long id, String username, String email, String phone){
        boolean valid=false;
        Date n = new Date();
        if(id==null) {
            if (!Strings.isNullOrEmpty(username)) {
                valid = db().where(q -> !q.getIsDeleted()).noneMatch(r -> r.getLoginUserName().toLowerCase().equals(username.toLowerCase()));
            }
            if (!Strings.isNullOrEmpty(email)) {
                valid = db().where(q -> !q.getIsDeleted() && q.getEmail() != null).noneMatch(r -> r.getEmail().toLowerCase().equals(email.toLowerCase()));
            }
            if (!Strings.isNullOrEmpty(phone)) {
                valid = db().where(q -> !q.getIsDeleted() && q.getMobilePhone() != null).noneMatch(r -> r.getMobilePhone().toLowerCase().equals(phone.toLowerCase()));
            }
        }else{
            if (!Strings.isNullOrEmpty(username)) {
                valid = db().where(q -> !q.getIsDeleted() && !q.getId().equals(id)).noneMatch(r -> r.getLoginUserName().toLowerCase().equals(username.toLowerCase()));
            }
            if (!Strings.isNullOrEmpty(email)) {
                valid = db().where(q -> !q.getIsDeleted() && q.getEmail() != null && !q.getId().equals(id)).noneMatch(r -> r.getEmail().toLowerCase().equals(email.toLowerCase()));
            }
            if (!Strings.isNullOrEmpty(phone)) {
                valid = db().where(q -> !q.getIsDeleted() && q.getMobilePhone() != null && !q.getId().equals(id)).noneMatch(r -> r.getMobilePhone().toLowerCase().equals(phone.toLowerCase()));
            }
        }
        return valid;
    }

    public HashMap<String,Object> getFiltered(String filter,String start, String end, int skip, int limit, Boolean descending, String sort) {
        List<MasterUserDto> result = new ArrayList<>();
        TypedQuery<MasterUser> all = getResultList(filter,start,end,skip,limit,descending,sort);
        result = MappingToDto(all);
        Long length = all.getResultStream().count();
        HashMap<String,Object> retur = new HashMap<String,Object>();
        retur.put("data",result);
        retur.put("length",length);
        return retur;
    }

    public TypedQuery<MasterUser> getResultList(String filter,String start, String end,int skip,int limit,Boolean descending,String sort) {
        DateUtil dateUtil = new DateUtil();
        int kondisi = 0;

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<MasterUser> q = cb.createQuery(MasterUser.class);
        Root<MasterUser> root = q.from(MasterUser.class);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.isFalse(root.get("isDeleted"));
        predicates.add(mainCond);

        if (!Strings.isNullOrEmpty(filter)) {
            Predicate pkeyword =
                   cb.or(
                                cb.like(root.get("employeeId"),"%"+filter+"%"),
                                    cb.like(root.get("loginUserName"), "%"+filter+"%"),
                                    cb.like(root.get("email"), "%"+filter+"%"),
                                    cb.like(root.get("nameFront"),"%"+filter+"%"),
                                    cb.like(root.get("nameMiddleLast"),"%"+filter+"%"),
                                    cb.like(cb.concat(root.get("nameFront"),cb.concat(" ",root.get("nameMiddleLast"))),"%"+filter+"%"),
                                    cb.like(root.join("area").get("nama"),"%"+filter+"%")
                   );

            predicates.add(pkeyword);
        }

        if(!Strings.isNullOrEmpty(start)){
            if(!Strings.isNullOrEmpty(end)){
                Date startDate = dateUtil.getDateFromISOString_Biasa(start + "T00:00:00.000Z");
                Date endDate = dateUtil.getDateFromISOString_Biasa(end + "T23:59:59.999Z");
                Predicate pPeriode = cb.and(
                        cb.greaterThanOrEqualTo(root.<Date>get("start"),startDate),
                        cb.lessThanOrEqualTo(root.<Date>get("end"),endDate)
                );
                predicates.add(pPeriode);
            }else{
                Date startDate = dateUtil.getDateFromISOString_Biasa(start + "T00:00:00.000Z");
                Date endDate = dateUtil.getDateFromISOString_Biasa(start + "T23:59:59.999Z");
                Predicate pStart = cb.and(
                        cb.greaterThanOrEqualTo(root.<Date>get("start"),startDate),
                        cb.lessThanOrEqualTo(root.<Date>get("end"),endDate)
                );
                predicates.add(pStart);
            }
        }else{
            if(!Strings.isNullOrEmpty(end)){
                Date startDate = dateUtil.getDateFromISOString_Biasa(end + "T00:00:00.000Z");
                Date endDate = dateUtil.getDateFromISOString_Biasa(end + "T23:59:59.999Z");
                Predicate pEnd = cb.and(
                        cb.greaterThanOrEqualTo(root.<Date>get("start"),startDate),
                        cb.lessThanOrEqualTo(root.<Date>get("end"),endDate)
                );
                predicates.add(pEnd);
            }
        }


        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);

        if (descending != null && !Strings.isNullOrEmpty(sort)) {
            if (descending) {
                q.orderBy(cb.desc(root.get(sort)));
            } else {
                q.orderBy(cb.asc(root.get(sort)));
            }
        }

        return getEntityManager().createQuery(q).setFirstResult(skip).setMaxResults(limit);
    }

    private List<MasterUserDto> MappingToDto(TypedQuery<MasterUser> all) {
        List<MasterUserDto> result = null;

        if (all != null) {
            result = all.getResultList().stream().map(MasterUserDto::new).collect(Collectors.toList());
        }
        return result;
    }

    public MasterUser findUserByNIPP(String nipp) {
        return db().where(t -> t.getEmployeeId().equals(nipp) && t.getIsActive() && !t.getIsDeleted()).findFirst().orElse(null);
    }

    public List<MasterUser> searchUserByKeyword(String keyword) {
        return db()
                .where(t -> t.getNameFront().contains(keyword) || t.getNameMiddleLast().contains(keyword))
                .collect(Collectors.toList());
    }
}
