package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.PemeriksaPelakharPymtSurat;
import com.qtasnim.eoffice.db.Surat;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@LocalBean
public class PemeriksaPelakharPymtSuratService extends AbstractFacade<PemeriksaPelakharPymtSurat> {
    @Override
    protected Class<PemeriksaPelakharPymtSurat> getEntityClass() {
        return PemeriksaPelakharPymtSurat.class;
    }

    public PemeriksaPelakharPymtSurat find(String type, Long idSurat, Long idOrganization) {
        return db()
                .where(t -> t.getStatus().equals(type) && t.getSurat().getId().equals(idSurat) && t.getOrganization().getIdOrganization().equals(idOrganization))
                .findFirst()
                .orElse(null);
    }

    public List<PemeriksaPelakharPymtSurat> findBySurat(Long idSurat) {
        return db()
                .where(t -> t.getSurat().getId().equals(idSurat))
                .toList();
    }

    public PemeriksaPelakharPymtSurat find(Surat surat, MasterStrukturOrganisasi assignee, String status) {
        Long suratId = surat.getId();
        Long orgId = assignee.getIdOrganization();

        return db()
                .where(t -> t.getSurat().getId().equals(suratId) && t.getOrganization().getIdOrganization().equals(orgId) && t.getStatus().equals(status))
                .limit(1)
                .findFirst()
                .orElse(null);
    }
}
