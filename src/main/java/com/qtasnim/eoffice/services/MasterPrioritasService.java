package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterDivision;
import com.qtasnim.eoffice.db.MasterPrioritas;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterDivisionDto;
import com.qtasnim.eoffice.ws.dto.MasterPrioritasDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Stateless
@LocalBean
public class MasterPrioritasService extends AbstractFacade<MasterPrioritas> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterPrioritas> getEntityClass() {
        return MasterPrioritas.class;
    }

    public JPAJinqStream<MasterPrioritas> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterPrioritas> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name));
    }

    public MasterPrioritas save(Long id, MasterPrioritasDto dao){
        MasterPrioritas model = new MasterPrioritas();
        DateUtil dateUtil = new DateUtil();

        if(id==null){
            model.setNama(dao.getNama());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setCreatedBy(dao.getCreatedBy());
            model.setCreatedDate(new Date());
            model.setModifiedBy("-");
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.create(model);

        }else {
            model = this.find(id);

            model.setNama(dao.getNama());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }


       return model;
    }

    public MasterPrioritas delete(Long id){
        MasterPrioritas model = this.find(id);
        this.remove(model);
        return model;
    }

}