package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterTrapArsip;
import com.qtasnim.eoffice.db.MasterBoksArsip;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterBoksArsipDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterBoksArsipService extends AbstractFacade<MasterBoksArsip> {
    private static final long serialVersionUID = 1L;
    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterTrapArsipService trapArsipService;

    @Inject
    private MasterAutoNumberService autoNumberService;

    @Override
    protected Class<MasterBoksArsip> getEntityClass() {
        return MasterBoksArsip.class;
    }

    public JPAJinqStream<MasterBoksArsip> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public MasterBoksArsip save(Long id, MasterBoksArsipDto dao) throws Exception {
        MasterBoksArsip model = new MasterBoksArsip();
        try {
            if (id == null) {
                this.create(data(model, dao));
            } else {
                model = this.find(id);
                dao.setId(id);
                this.edit(data(model, dao));
            }
            return model;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterBoksArsip data(MasterBoksArsip masterBoksArsip, MasterBoksArsipDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterBoksArsip model = masterBoksArsip;
        model.setId(dao.getId());
        if (dao.getIdTrapArsip() != null) {
            MasterTrapArsip trapArsip = trapArsipService.find(dao.getIdTrapArsip());
            model.setTrapArsip(trapArsip);
        } else {
            model.setTrapArsip(null);
        }
        if (dao.getCompanyId() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }
        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        if(dao.getId()==null) {
            String kode = autoNumberService.from(MasterBoksArsip.class).next(t -> t.getKode(), model);
            model.setKode(kode);
        }
        return model;
    }

    public MasterBoksArsip delete(Long id) {
        MasterBoksArsip model = this.find(id);
        this.remove(model);
        return model;
    }

}