package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.GlobalAttachmentDto;
import com.qtasnim.eoffice.ws.dto.MasterRuangArsipDto;
import org.apache.commons.compress.utils.IOUtils;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Stateless
@LocalBean
public class MasterRuangArsipService extends AbstractFacade<MasterRuangArsip> {
    private static final long serialVersionUID = 1L;
    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterKorsaService korsaService;

    @Inject
    private MasterAutoNumberService autoNumberService;

    @Inject
    private FileService fileService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Override
    protected Class<MasterRuangArsip> getEntityClass() {
        return MasterRuangArsip.class;
    }

    public JPAJinqStream<MasterRuangArsip> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterRuangArsip> getFiltered(String name) {
        return db().where(q -> q.getLokasi().toLowerCase().contains(name));
    }

    public MasterRuangArsipDto save(Long id, MasterRuangArsipDto dao) throws Exception {
        MasterRuangArsip model = new MasterRuangArsip();
        try {
            if (id == null) {
                this.create(data(model, dao));
            } else {
                model = this.find(id);
                dao.setId(id);
                this.edit(data(model, dao));
            }

            MasterRuangArsipDto result = new MasterRuangArsipDto(model);
            return result;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterRuangArsip data(MasterRuangArsip masterRuangArsip, MasterRuangArsipDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterRuangArsip model = masterRuangArsip;
        model.setId(dao.getId());
        model.setLokasi(dao.getLokasi());
//        model.setFoto(dao.getFoto());
        if (dao.getIdUnitKerja() != null) {
            MasterKorsa korsa = korsaService.find(dao.getIdUnitKerja());
            model.setUnitKerja(korsa);
        } else {
            model.setUnitKerja(null);
        }
        if (dao.getCompanyId() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }
        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        if(dao.getId()==null) {
            String kode = autoNumberService.from(MasterRuangArsip.class).next(t -> t.getKode(), model);
            model.setKode(kode);
        }
        return model;
    }

    public MasterRuangArsip delete(Long id) {
        MasterRuangArsip model = this.find(id);
        globalAttachmentService.deleteAttachment("m_ruang_arsip",id);
        this.remove(model);
        return model;
    }

    public void uploadAttachment(Long idRuangArsip, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";

        try {
            String filename = formDataContentDisposition.getFileName();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is2 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is3 = new ByteArrayInputStream(baos.toByteArray());

            byte[] documentContent = IOUtils.toByteArray(is1);

            docId = fileService.upload(filename, documentContent);
            docId = docId.replace("workspace://SpacesStore/", "");

            MasterRuangArsip ruangArsip = this.find(idRuangArsip);


            GlobalAttachment obj = globalAttachmentService.getAttachmentDataByRef("m_ruang_arsip",idRuangArsip).findFirst().orElse(null);
            if(obj==null) {
                obj.setMimeType(body.getMediaType().toString());
                obj.setDocGeneratedName(UUID.randomUUID().toString());
                obj.setDocName(filename);
                obj.setCompanyCode(ruangArsip.getCompanyCode());
                obj.setDocId(docId);
                obj.setIsKonsep(false);
                obj.setIsPublished(false);
                obj.setSize(new Long(documentContent.length));
                obj.setIsDeleted(false);
                obj.setReferenceId(idRuangArsip);
                obj.setReferenceTable("m_ruang_arsip");
                obj.setNumber("test 123");

                globalAttachmentService.create(obj);
            }else{
                globalAttachmentService.deleteAttachment("m_ruang_arsip",idRuangArsip);
                fileService.delete(ruangArsip.getFoto());

                obj.setMimeType(body.getMediaType().toString());
                obj.setDocGeneratedName(UUID.randomUUID().toString());
                obj.setDocName(filename);
                obj.setCompanyCode(ruangArsip.getCompanyCode());
                obj.setDocId(docId);
                obj.setIsKonsep(false);
                obj.setIsPublished(false);
                obj.setSize(new Long(documentContent.length));
                obj.setIsDeleted(false);
                obj.setReferenceId(idRuangArsip);
                obj.setReferenceTable("m_ruang_arsip");
                obj.setNumber("test 123");

                globalAttachmentService.create(obj);
            }

            ruangArsip.setFoto(docId);
            this.edit(ruangArsip);
        } catch (Exception ex) {
            if (!org.apache.commons.lang.StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }
            throw ex;
        }
    }

    public MasterRuangArsip getData(Long id){
        Date n = new Date();
        return db().where(q-> n.after(q.getStart()) && n.before(q.getEnd()) && q.getId().equals(id)).findFirst().orElse(null);
    }

    public void uploadAttachment2(Long idRuangArsip, List<FormDataBodyPart> body) throws Exception {
        String docId="";
        try {
            MasterRuangArsip ruangArsip = this.find(idRuangArsip);
            GlobalAttachment obj = globalAttachmentService.getAttachmentDataByRef("m_ruang_arsip",idRuangArsip).findFirst().orElse(null);

            if(body!=null && !body.isEmpty()) {
                if (obj != null) {
                    globalAttachmentService.deleteAttachment("m_ruang_arsip", idRuangArsip);
                }

                for (FormDataBodyPart fd : body) {
                    String tmpDoc = "";
                    BodyPartEntity bodyPartEntity = (BodyPartEntity) fd.getEntity();
                    String filename = fd.getContentDisposition().getFileName();

                    byte[] documentContent = IOUtils.toByteArray(bodyPartEntity.getInputStream());

                    tmpDoc = fileService.upload(filename, documentContent);
                    tmpDoc = tmpDoc.replace("workspace://SpacesStore/", "");


                    obj = new GlobalAttachment();
                    obj.setMimeType(fd.getMediaType().toString());
                    obj.setDocGeneratedName(UUID.randomUUID().toString());
                    obj.setDocName(filename);
                    obj.setCompanyCode(ruangArsip.getCompanyCode());
                    obj.setDocId(tmpDoc);
                    obj.setIsKonsep(false);
                    obj.setIsPublished(false);
                    obj.setSize(new Long(documentContent.length));
                    obj.setIsDeleted(false);
                    obj.setReferenceId(idRuangArsip);
                    obj.setReferenceTable("m_ruang_arsip");
                    obj.setNumber("test 123");

                    globalAttachmentService.create(obj);

                    if (docId == "") {
                        docId = tmpDoc;
                    } else {
                        docId = docId + ";" + tmpDoc;
                    }
                }

                ruangArsip.setFoto(docId);
                this.edit(ruangArsip);
            }
        } catch (Exception ex) {
           ex.getMessage();
        }
    }
}