package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.SuratCatatanSekretaris;
import com.qtasnim.eoffice.ws.dto.SuratCatatanSekretarisDto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Stateless
@LocalBean
public class SuratCatatanSekretarisService extends AbstractFacade<SuratCatatanSekretaris> {
    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private SuratService suratService;

    @Override
    protected Class<SuratCatatanSekretaris> getEntityClass() {
        return SuratCatatanSekretaris.class;
    }

    public List<SuratCatatanSekretaris> findBySuratIds(List<Long> ids) {
        List<SuratCatatanSekretaris> result = new ArrayList<>();

        if (ids != null && ids.size() > 0) {
            return db().where(t -> ids.contains(t.getSurat().getId())).toList();
        }

        return result;
    }

    public SuratCatatanSekretaris saveOrEdit(Long id, SuratCatatanSekretarisDto dao) {
        SuratCatatanSekretaris suratCatatanSekretaris;

        if (id == null) {
            suratCatatanSekretaris = new SuratCatatanSekretaris();
            bindDao(dao, suratCatatanSekretaris);
            create(suratCatatanSekretaris);
        } else {
            suratCatatanSekretaris = find(id);

            if (suratCatatanSekretaris == null) {
                throw new InternalServerErrorException("Invalid ID");
            }

            bindDao(dao, suratCatatanSekretaris);
            edit(suratCatatanSekretaris);
        }

        return suratCatatanSekretaris;
    }

    private void bindDao(SuratCatatanSekretarisDto dao, SuratCatatanSekretaris model) {
        model.setPejabat(masterStrukturOrganisasiService.find(dao.getIdPejabat()));
        model.setSekretaris(masterStrukturOrganisasiService.find(dao.getIdSekretaris()));
        model.setNote(dao.getNote());
        model.setSurat(suratService.find(dao.getIdSurat()));
    }
}
