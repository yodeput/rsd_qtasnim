/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.cmis.ICMISProvider;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.helpers.ReflectionHelper;
import com.qtasnim.eoffice.helpers.TemplatingHelper;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.*;
import com.qtasnim.eoffice.ws.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;
import org.yaml.snakeyaml.scanner.Constant;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * @author acdwisu
 */
@Stateless
@LocalBean
public class TemplatingService {
    @Inject
    private FormDefinitionService formDefinitionService;

    @Inject
    private SuratService suratService;

    @Inject
    private MasterMetadataService masterMetadataService;

    @Inject
    private JenisDokumenDocLibService jenisDokumenDocLibService;

    @Inject
    private FileService fileService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private QRGenerateService qrGenerateService;

    @Inject
    private FormFieldService fieldService;

    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private PenandatanganSuratService penandatanganSuratService;

    @Inject
    private ProcessInstanceService processInstanceService;

    @Inject
    private Logger logger;

    @Inject
    private ICMISProvider icmisProvider;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private MasterUserService userService;

//    @Inject
//    private ICMISProvider icmisProvider;

    public TemplatingService() {
    }

    private String getMetadataNameOfFormField(FormField form) {
//        return TemplatingHelper.toMetadataFormatName(form.getMetadata().getNama());
        return form.getMetadata().getNama();
    }

    private String getMetadataNameOfFormField(FormFieldDto form) {
        return TemplatingHelper.toMetadataFormatName(form.getMetadata().getNama());
    }

    private byte[] processTemplating(byte[] docOri, Map<String, String> pairMetaNameValue) {
        String pathTempTemplateRaw = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID() + ".docx";

        byte[] docTemplated;

        TemplatingProcess templatingProcess;

        try {
            FileOutputStream fos = new FileOutputStream(pathTempTemplateRaw);

            fos.write(docOri);
            fos.close();

            templatingProcess = new TemplatingProcess(pairMetaNameValue, pathTempTemplateRaw);

            docTemplated = templatingProcess.process();

            Files.deleteIfExists(new File(pathTempTemplateRaw).toPath());
        } catch (IOException ioe) {
            docTemplated = null;

            ioe.printStackTrace();
        }

        return docTemplated;
    }

    public byte[] deleteWatermarkText(byte[] docWatermarked) {
        String pathTempWatermarked = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID() + ".docx";

        byte[] docNonWatermarked;

        TemplatingProcess templatingProcess;

        try {
            FileOutputStream fos = new FileOutputStream(pathTempWatermarked);

            fos.write(docWatermarked);
            fos.close();

            templatingProcess = new TemplatingProcess(new HashMap(), "");

            docNonWatermarked = templatingProcess.deleteWatermark(pathTempWatermarked);

            Files.deleteIfExists(new File(pathTempWatermarked).toPath());
        } catch (IOException ioe) {
            docNonWatermarked = null;

            ioe.printStackTrace();
        }

        return docNonWatermarked;
    }

    public void handleMetadataPenandatangan(TemplatingDto templatingDto, MetadataValueDto metadataPenandatangan, Map<String, String> pairMetaNameValue, String key) {
        List<PenandatanganSuratDto> penandatanganSuratDtos = templatingDto.getPenandatanganSurat();

        String labelTipePenandatangan = "", labelJabatanPenandatanganOri = "";
        String labelNamaPenandatanganPengganti = "", labelNippPenandatanganPengganti = "";

        if (metadataPenandatangan.getFieldData() != null && metadataPenandatangan.getFieldId() != null) {
            Gson g = new Gson();

            FormField objF = fieldService.find(metadataPenandatangan.getFieldId());

            objF.setData(metadataPenandatangan.getFieldData());
            fieldService.edit(objF);

            String json = metadataPenandatangan.getFieldData().replace("\\", "");

            FieldDataDto field = g.fromJson(json, FieldDataDto.class);

            field.setName(metadataPenandatangan.getName());

            boolean isPenandatanganKonseptor, isPenandatanganAtasNama;

            MasterUser konseptor = userSession.getUserSession().getUser();
            MasterUser atasNamaSurat;
            if (penandatanganSuratDtos != null) {
                if (penandatanganSuratDtos.size() == 0 || penandatanganSuratDtos.get(0).getIdANOrg() == null)
                    atasNamaSurat = null;
                else {
                    MasterStrukturOrganisasi org = masterStrukturOrganisasiService.getByIdOrganisasi(penandatanganSuratDtos.get(0).getIdANOrg());

                    if (org == null) atasNamaSurat = null;
                    else atasNamaSurat = org.getUser();
                }
            } else {
                atasNamaSurat = null;
            }

            isPenandatanganAtasNama = atasNamaSurat == null ? false : true;
            isPenandatanganKonseptor = field.getVMySelf();

            if (isPenandatanganAtasNama) {
                labelTipePenandatangan = "a.n";
//                labelNamaPenandatanganPengganti = atasNamaSurat.getNameFront().concat(" ").concat(atasNamaSurat.getNameMiddleLast());
//                labelNippPenandatanganPengganti = "NIPP : ".concat(atasNamaSurat.getEmployeeId());

                if (isPenandatanganKonseptor) {
                    labelNamaPenandatanganPengganti = konseptor.getNameFront().concat(" ").concat(konseptor.getNameMiddleLast());
                    labelNippPenandatanganPengganti = "NIPP ".concat(konseptor.getEmployeeId());

                    String jabatan = StringUtils.isBlank(konseptor.getKedudukan()) ? "-" : konseptor.getKedudukan();

//                    Long idOrgAn = penandatanganSuratDtos.get(0).getIdANOrg();
                    Long idOrgAn = atasNamaSurat.getOrganizationEntity().getIdOrganization();

                    String jabatanAn;

                    if (idOrgAn == null)
                        jabatanAn = "-";
                    else
                        jabatanAn = atasNamaSurat.getKedudukan();

                    labelJabatanPenandatanganOri = jabatanAn.concat(TemplatingSeparator.SEPARATOR_NEW_ENTITY + jabatan);
                } else {
                    if (penandatanganSuratDtos.get(0).getIdPenandatangan() == null) {
                        labelJabatanPenandatanganOri = "-";
                    } else {
                        MasterUser user ;
                        if(penandatanganSuratDtos.get(0).getUserId() != null){
                            user = userService.find(penandatanganSuratDtos.get(0).getUserId());
                        }else{
                            user = masterStrukturOrganisasiService.getByIdOrganisasi(penandatanganSuratDtos.get(0).getIdPenandatangan()).getUser();
                        }

                        labelNamaPenandatanganPengganti = user.getNameFront().concat(" ").concat(user.getNameMiddleLast());
                        labelNippPenandatanganPengganti = "NIPP ".concat(user.getEmployeeId());

                        if (user == null) labelJabatanPenandatanganOri = "-";
                        else {
                            String jabatan = StringUtils.isBlank(konseptor.getKedudukan()) ? "-" : konseptor.getKedudukan();

                            Long idOrgAn = penandatanganSuratDtos.get(0).getIdANOrg();

                            String jabatanAn;

                            if (idOrgAn == null)
                                jabatanAn = "-";
                            else
                                jabatanAn = user.getKedudukan();

                            labelJabatanPenandatanganOri = jabatanAn.concat(TemplatingSeparator.SEPARATOR_NEW_ENTITY + jabatan);
                        }
                    }
                }
            } else {
                if (isPenandatanganKonseptor) {
                    boolean isHavePendelegasi;

                    MasterUser user = userSession.getUserSession().getUser();

                    MasterDelegasi delegasiPelakhar = masterDelegasiService.getByToOrgCode(konseptor.getOrganizationEntity().getOrganizationCode(), "Pelakhar"),
                            delegasiPymt = masterDelegasiService.getByToOrgCode(konseptor.getOrganizationEntity().getOrganizationCode(), "PYMT");

                    isHavePendelegasi = (delegasiPelakhar != null || delegasiPymt != null ? true : false);

                    String jabatan = user.getKedudukan();

                    if (isHavePendelegasi) {
                        MasterDelegasi delegasi = null;

                        try {
                            if (delegasiPelakhar != null) {
                                //                            List<ProcessTask> pts = processTaskService.getExecutedTaskByOrgId(delegasiPelakhar.getTo().getIdOrganization()).toList();
//
//                            ProcessTask pt = pts.get(pts.size()-1);
//
//                            if(pt.getResponse().equals("Setujui")) {
//                                delegasi = delegasiPelakhar;
//                            }

                                PenandatanganSurat penandatanganSurat = penandatanganSuratService.getByIdOrganisasi(penandatanganSuratDtos.get(0).getIdPenandatangan());

                                ProcessInstance processInstance = processInstanceService.getByRelatedEntity("com.qtasnim.eoffice.db.Surat", penandatanganSurat.getSurat().getId().toString());

                                List<ProcessTask> processTasks = processInstance.getTasks();

//                            for(ProcessTask pt : processTasks) {

                                ProcessTask pt = processTasks.get(processTasks.size() - 1);

                                if (pt.getTaskName().contains("Task_Penandatangan")) {
//                                    if(pt.getResponse() != null && pt.getResponse().equals("Setujui")) {
                                    if (userSession.getUserSession().getUser().getEmployeeId().equals(delegasiPelakhar.getTo().getUser().getEmployeeId())) {
                                        delegasi = delegasiPelakhar;
                                    } else if (userSession.getUserSession().getUser().getEmployeeId().equals(delegasiPymt.getTo().getUser().getEmployeeId())) {
                                        delegasi = delegasiPymt;
                                    }
//                                    }
                                }
//                            }
                            } else delegasi = delegasiPymt;
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        if (delegasi == null) delegasi = delegasiPymt;

                        labelTipePenandatangan = delegasi.getTipe();

                        MasterUser pendelegasi = delegasi.getTo().getUser();
                        if (pendelegasi == null) {
                            labelJabatanPenandatanganOri = "-";
                            labelNamaPenandatanganPengganti = "-";
                            labelNippPenandatanganPengganti = "NIPP -";
                        } else {
                            labelJabatanPenandatanganOri = StringUtils.isBlank(pendelegasi.getKedudukan()) ? "-" : pendelegasi.getKedudukan();
                            labelNamaPenandatanganPengganti = pendelegasi.getNameFront().concat(" ").concat(pendelegasi.getNameMiddleLast());
                            labelNippPenandatanganPengganti = "NIPP " + (StringUtils.isBlank(pendelegasi.getEmployeeId()) ? "-" : pendelegasi.getEmployeeId());
                        }
                    } else {
                        labelJabatanPenandatanganOri = StringUtils.isBlank(jabatan) ? "-" : jabatan;
                        labelTipePenandatangan = "";
                        labelNamaPenandatanganPengganti = user.getNameFront().concat(" ").concat(user.getNameMiddleLast());
                        labelNippPenandatanganPengganti = "NIPP " + user.getEmployeeId();
                    }
                } else {
                    boolean isHavePendelegasi;
                    if (penandatanganSuratDtos != null) {
                        if (penandatanganSuratDtos.size() > 0) {
                            MasterStrukturOrganisasi penandatanganSurat = masterStrukturOrganisasiService.getByIdOrganisasi(penandatanganSuratDtos.get(0).getIdPenandatangan());

                            if (penandatanganSurat == null) {
                                labelTipePenandatangan = "";
                                labelNamaPenandatanganPengganti = "-";
                                labelNippPenandatanganPengganti = "NIPP -";
                                labelJabatanPenandatanganOri = "-";
                            } else {
                                MasterDelegasi delegasiPelakhar = masterDelegasiService.getByToOrgCode(penandatanganSurat.getOrganizationCode(), "Pelakhar"),
                                        delegasiPymt = masterDelegasiService.getByToOrgCode(penandatanganSurat.getOrganizationCode(), "PYMT");

                                String jabatan = penandatanganSurat.getUser().getKedudukan();

                                labelJabatanPenandatanganOri = StringUtils.isBlank(jabatan) ? "-" : jabatan;

                                isHavePendelegasi = (delegasiPelakhar != null || delegasiPymt != null ? true : false);

                                if (isHavePendelegasi) {
                                    MasterDelegasi delegasi = null;

                                    try {
                                        if (delegasiPelakhar != null) {
                                            //                            List<ProcessTask> pts = processTaskService.getExecutedTaskByOrgId(delegasiPelakhar.getTo().getIdOrganization()).toList();
//
//                            ProcessTask pt = pts.get(pts.size()-1);
//
//                            if(pt.getResponse().equals("Setujui")) {
//                                delegasi = delegasiPelakhar;
//                            }

                                            PenandatanganSurat penandatanganSurat2 = penandatanganSuratService.getByIdOrganisasi(penandatanganSuratDtos.get(0).getIdPenandatangan());

                                            ProcessInstance processInstance = processInstanceService.getByRelatedEntity("com.qtasnim.eoffice.db.Surat", penandatanganSurat2.getSurat().getId().toString());

                                            List<ProcessTask> processTasks = processInstance.getTasks();

//                            for(ProcessTask pt : processTasks) {

                                            ProcessTask pt = processTasks.get(processTasks.size() - 1);

                                            if (pt.getTaskName().contains("Task_Penandatangan")) {
//                                    if(pt.getResponse() != null && pt.getResponse().equals("Setujui")) {
                                                if (userSession.getUserSession().getUser().getEmployeeId().equals(delegasiPelakhar.getTo().getUser().getEmployeeId())) {
                                                    delegasi = delegasiPelakhar;
                                                } else if (userSession.getUserSession().getUser().getEmployeeId().equals(delegasiPymt.getTo().getUser().getEmployeeId())) {
                                                    delegasi = delegasiPymt;
                                                }
//                                    }
                                            }
//                            }
                                        } else delegasi = delegasiPymt;
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }

                                    if (delegasi == null) delegasi = delegasiPymt;

                                    labelTipePenandatangan = delegasi.getTipe();

                                    MasterUser pendelegasi = delegasi.getTo().getUser();

                                    if (pendelegasi != null) {
                                        labelJabatanPenandatanganOri = StringUtils.isBlank(pendelegasi.getKedudukan()) ? "-" : pendelegasi.getKedudukan();
                                        labelNamaPenandatanganPengganti = pendelegasi.getNameFront().concat(" ").concat(pendelegasi.getNameMiddleLast());
                                        labelNippPenandatanganPengganti = "NIPP " + pendelegasi.getEmployeeId();
                                    } else {
                                        labelJabatanPenandatanganOri = "-";
                                        labelNamaPenandatanganPengganti = "-";
                                        labelNippPenandatanganPengganti = "NIPP -";
                                    }
                                } else {
                                    labelJabatanPenandatanganOri = StringUtils.isBlank(jabatan) ? "-" : jabatan;
                                    labelTipePenandatangan = "";
                                    labelNamaPenandatanganPengganti = penandatanganSurat.getUser().getNameFront().concat(" ").concat(penandatanganSurat.getUser().getNameMiddleLast());
                                    labelNippPenandatanganPengganti = "NIPP " + penandatanganSurat.getUser().getEmployeeId();
                                }
                            }
                        } else {
                            labelTipePenandatangan = "";
                            labelJabatanPenandatanganOri = "-";
                            labelNamaPenandatanganPengganti = "-";
                            labelNippPenandatanganPengganti = "NIPP -";
                        }
                    } else {
                        labelTipePenandatangan = "";
                        labelJabatanPenandatanganOri = "-";
                        labelNamaPenandatanganPengganti = "-";
                        labelNippPenandatanganPengganti = "NIPP -";
                    }
                }
            }
        } else {
            labelTipePenandatangan = "";
            labelJabatanPenandatanganOri = "-";
            labelNamaPenandatanganPengganti = "-";
            labelNippPenandatanganPengganti = "NIPP -";
        }

        String[] conditionalKeys = TemplatingHelper.PENANDATANGAN_CONDITIONAL_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

        String value;

        value = labelTipePenandatangan.concat(" ").concat(labelJabatanPenandatanganOri.concat(TemplatingHelper.SUBSTITUTE_COMMA));
        pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(conditionalKeys[0]), value.trim());

        value = WordUtils.capitalizeFully(labelNamaPenandatanganPengganti).concat(",").concat(labelNippPenandatanganPengganti);
        pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(conditionalKeys[1]), value.trim());
    }

    public void handleMetadataPenandatangan(Surat entity, List<PenandatanganSuratDto> penandatanganSuratDtos, Map<String, String> pairMetaNameValue, boolean isKonseptor) {
        String labelTipePenandatangan = "";
        String labelJabatanPenandatanganOri = "";
        String labelNamaPenandatanganPengganti = "";
        String labelNippPenandatanganPengganti = "";

        boolean isPenandatanganKonseptor, isPenandatanganAtasNama;

        if (penandatanganSuratDtos != null && penandatanganSuratDtos.size() > 0 && penandatanganSuratDtos.get(0).getIdPenandatangan() != null) {
            MasterUser penandatangan;

            if(penandatanganSuratDtos.get(0).getUserId() != null){
                penandatangan = userService.find(penandatanganSuratDtos.get(0).getUserId());
            }else {
                penandatangan = masterStrukturOrganisasiService.getByIdOrganisasi(penandatanganSuratDtos.get(0).getIdPenandatangan()).getUser();
            }

            PenandatanganSurat penandatanganSurat = penandatanganSuratService.getByIdOrganisasi(penandatanganSuratDtos.get(0).getIdPenandatangan());

            MasterUser atasNama;

            if (penandatanganSuratDtos.get(0).getIdANOrg() == null)
                atasNama = null;
            else {
                MasterStrukturOrganisasi org = masterStrukturOrganisasiService.getByIdOrganisasi(penandatanganSuratDtos.get(0).getIdANOrg());

                if (org == null) atasNama = null;
                else atasNama = org.getUser();
            }

            isPenandatanganAtasNama = atasNama == null ? false : true;
            isPenandatanganKonseptor = isKonseptor;

            if (isPenandatanganAtasNama) {
                labelTipePenandatangan = "a.n";
//                labelNamaPenandatanganPengganti = atasNamaSurat.getNameFront().concat(" ").concat(atasNamaSurat.getNameMiddleLast());
//                labelNamaPenandatanganPengganti = penandatangan.getNameFront().concat(" ").concat(penandatangan.getNameMiddleLast());
//                labelNippPenandatanganPengganti = "NIPP : ".concat(penandatangan.getEmployeeId());

                if (isPenandatanganKonseptor) {
                    MasterUser konseptor = userSession.getUserSession().getUser();

                    labelNamaPenandatanganPengganti = konseptor.getNameFront().concat(" ").concat(konseptor.getNameMiddleLast());
                    labelNippPenandatanganPengganti = "NIPP ".concat(konseptor.getEmployeeId());

                    String jabatan = StringUtils.isBlank(konseptor.getKedudukan()) ? "-" : konseptor.getKedudukan();

//                    Long idOrgAn = penandatanganSuratDtos.get(0).getIdANOrg();
                    Long idOrgAn = atasNama.getOrganizationEntity().getIdOrganization();

                    String jabatanAn;

                    if (idOrgAn == null)
                        jabatanAn = "-";
                    else
                        jabatanAn = atasNama.getKedudukan();

                    labelJabatanPenandatanganOri = jabatanAn.concat(TemplatingSeparator.SEPARATOR_NEW_ENTITY + jabatan);
                } else {
                    String jabatan ="";
                    if(penandatangan!=null) {
                        labelNamaPenandatanganPengganti = penandatangan.getNameFront().concat(" ").concat(penandatangan.getNameMiddleLast());
                        labelNippPenandatanganPengganti = "NIPP ".concat(penandatangan.getEmployeeId());
                        jabatan = StringUtils.isBlank(penandatangan.getKedudukan()) ? "-" : penandatangan.getKedudukan();
                    }else{
                        labelNamaPenandatanganPengganti = "__";
                        labelNippPenandatanganPengganti = "NIPP __";
                        jabatan = penandatanganSurat.getOrganization().getOrganizationName();
                    }

                    Long idOrgAn = penandatanganSuratDtos.get(0).getIdANOrg();

                    String jabatanAn;

                    if (idOrgAn == null) {
                        jabatanAn = "-";
                    } else {
                        jabatanAn = masterStrukturOrganisasiService.getByIdOrganisasi(idOrgAn).getUser().getKedudukan();
                    }
                    labelJabatanPenandatanganOri = jabatanAn.concat(TemplatingSeparator.SEPARATOR_NEW_ENTITY + jabatan);
                }
            } else {
                if(isPenandatanganKonseptor){
                    MasterUser konseptor = userSession.getUserSession().getUser();
                    String jabatan = konseptor.getKedudukan();

                    labelJabatanPenandatanganOri = StringUtils.isBlank(jabatan) ? "-" : jabatan;
                    labelTipePenandatangan = "";
                    labelNamaPenandatanganPengganti = konseptor.getNameFront().concat(" ").concat(konseptor.getNameMiddleLast());
                    labelNippPenandatanganPengganti = "NIPP " + konseptor.getEmployeeId();
                }else{
                    if(penandatangan!=null) {
                        MasterDelegasi delegasi = masterDelegasiService.getByToOrgCode(penandatangan.getOrganizationEntity().getOrganizationCode(), "PYMT");

                        if (delegasi != null) {
                            labelTipePenandatangan = delegasi.getTipe();
                            MasterUser pendelegasi = delegasi.getTo().getUser();
                            if (pendelegasi != null) {
                                labelJabatanPenandatanganOri = StringUtils.isBlank(penandatangan.getKedudukan()) ? "-" : penandatangan.getKedudukan();
                                labelNamaPenandatanganPengganti = pendelegasi.getNameFront().concat(" ").concat(pendelegasi.getNameMiddleLast());
                                labelNippPenandatanganPengganti = "NIPP " + pendelegasi.getEmployeeId();
                            } else {
                                labelJabatanPenandatanganOri = "-";
                                labelNamaPenandatanganPengganti = "-";
                                labelNippPenandatanganPengganti = "NIPP -";
                            }
                        } else {
                            labelJabatanPenandatanganOri = StringUtils.isBlank(penandatangan.getKedudukan()) ? "-" : penandatangan.getKedudukan();
                            labelTipePenandatangan = "";
                            labelNamaPenandatanganPengganti = penandatangan.getNameFront().concat(" ").concat(penandatangan.getNameMiddleLast());
                            labelNippPenandatanganPengganti = "NIPP " + penandatangan.getEmployeeId();
                        }
                    }else{
                        MasterDelegasi delegasi = masterDelegasiService.getByToOrgCode(penandatanganSurat.getOrganization().getOrganizationCode(), "PYMT");

                        if (delegasi != null) {
                            labelTipePenandatangan = delegasi.getTipe();
                            MasterUser pendelegasi = delegasi.getTo().getUser();
                            if (pendelegasi != null) {
                                labelJabatanPenandatanganOri = penandatanganSurat.getOrganization().getOrganizationName();
                                labelNamaPenandatanganPengganti = pendelegasi.getNameFront().concat(" ").concat(pendelegasi.getNameMiddleLast());
                                labelNippPenandatanganPengganti = "NIPP " + pendelegasi.getEmployeeId();
                            } else {
                                labelJabatanPenandatanganOri = "-";
                                labelNamaPenandatanganPengganti = "-";
                                labelNippPenandatanganPengganti = "NIPP -";
                            }
                        } else {
                            labelJabatanPenandatanganOri = penandatanganSurat.getOrganization().getOrganizationName();
                            labelTipePenandatangan = "";
                            labelNamaPenandatanganPengganti = "__";
                            labelNippPenandatanganPengganti = "NIPP __";
                        }
                    }
                }
            }
        } else {
            labelTipePenandatangan = "";
            labelJabatanPenandatanganOri = "-";
            labelNamaPenandatanganPengganti = "-";
            labelNippPenandatanganPengganti = "NIPP -";
        }


        String[] conditionalKeys = TemplatingHelper.PENANDATANGAN_CONDITIONAL_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

        String value;

        value = labelTipePenandatangan.concat(" ").concat(labelJabatanPenandatanganOri.concat(TemplatingHelper.SUBSTITUTE_COMMA));
        pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(conditionalKeys[0]), value.trim());

        value = WordUtils.capitalizeFully(labelNamaPenandatanganPengganti).concat(",").concat(labelNippPenandatanganPengganti);
        pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(conditionalKeys[1]), value.trim());
    }

    public String directTemplating(TemplatingDto templatingDto) {
        String documentId;
        String docId = templatingDto.getDocId();

        if (StringUtils.isBlank(docId)) {
            Long idJenisDokumen = formDefinitionService.find(templatingDto.getIdFormDefinition()).getJenisDokumen().getIdJenisDokumen();
            documentId = jenisDokumenDocLibService.getDocIdByJenisDokumenId(idJenisDokumen);
        } else {
            documentId = docId;
        }

        try {
            icmisProvider.openConnection();

            byte[] docMod = fileService.download(icmisProvider, documentId);

//        List<PemeriksaSuratDto> pemeriksaSuratDtos = templatingDto.getPemeriksaSurat();
//        List<TembusanSuratDto> tembusanSuratDtos = templatingDto.getTembusanSurat();
//        List<PenerimaSuratDto> penerimaSuratDtos = templatingDto.getPenerimaSurat();

            Map<String, String> pairMetaNameValue = new HashMap<>();

            for (MetadataValueDto mvDto : templatingDto.getMetadata()) {
                String key = TemplatingHelper.toMetadataFormatName(mvDto.getName());

                if (mvDto.getName().equals(TemplatingHelper.PENANDATANGAN_KEY)) {
                    handleMetadataPenandatangan(templatingDto, mvDto, pairMetaNameValue, key);
                } else {
                    String value = StringUtils.isBlank(mvDto.getLabel()) ? mvDto.getValue() : (StringUtils.isBlank(mvDto.getLabel()) ? " " : mvDto.getLabel());
                    pairMetaNameValue.put(key, value.trim());
                }
            }

            byte[] docTemplated = processTemplating(docMod, pairMetaNameValue);

            if (StringUtils.isBlank(docId)) {
//            try {
//                icmisProvider.openConnection();
//
//                CMISDocument document = icmisProvider.getDocument(documentId);

//                newDocId = fileService.upload("templated"+ FileUtility.GetFileExtension(document.getName()), docTemplated);
                String newDocId = fileService.upload("templated.docx", docTemplated);
                newDocId = newDocId.replace("workspace://SpacesStore/", "");
//            } catch (CMISProviderException e) {
//                e.printStackTrace();
//                newDocId = "";
//            } finally {
//                icmisProvider.closeConnection();
//            }

                return newDocId;
            } else {
                return fileService.update(docId, docTemplated);
            }
        } catch (Exception ex) {
            if (StringUtils.isNotEmpty(docId)) {
                fileService.delete(icmisProvider, docId);
            }

            throw new InternalServerErrorException(ex.getMessage(), ex);
        } finally {
            icmisProvider.closeConnection();
        }
    }

//    public byte[] dbBasedTemplating(IDynamicFormEntity entity) {
//        Class<?> form = entity.getClass();
//
//        Long idFormDefinition = entity.getFormDefinition().getId();
//
//        Long idJenisDokumen = formDefinitionService.find(idFormDefinition).getJenisDokumen().getIdJenisDokumen();
//        documentId = jenisDokumenDocLibService.getDocIdByJenisDokumenId(idJenisDokumen);
//
//        byte[] docMod = fileService.download(documentId);
//
//        TemplatingDto templatingDto = new TemplatingDto();
//
//        templatingDto.setIdFormDefinition(idFormDefinition);
//        templatingDto.setDocId(documentId);
//        templatingDto.setPemeriksaSurat();
//
//        Map<String, String> pairMetaNameValue = new HashMap<>();
//
//        for(MetadataValueDto mvDto : templatingDto.getMetadata()) {
//            String key = TemplatingHelper.toMetadataFormatName(mvDto.getName());
//
//            if(mvDto.getName().equals(TemplatingHelper.PENANDATANGAN_KEY)) {
//                handleMetadataPenandatangan(templatingDto, mvDto, pairMetaNameValue, key);
//            } else {
//                String value = StringUtils.isBlank(mvDto.getLabel())? mvDto.getValue() : (StringUtils.isBlank(mvDto.getLabel())? " " : mvDto.getLabel());
//                pairMetaNameValue.put(key, value.trim());
//            }
//        }
//
//        byte[] docTemplated = processTemplating(docMod, pairMetaNameValue);
//
//        return docTemplated;
//    }

    public byte[] dbBasedTemplating(byte[] docTemplateRaw, IDynamicFormEntity entity) {
        return dbBasedTemplating(docTemplateRaw, entity, false);
    }

    public byte[] dbBasedTemplating(byte[] docTemplateRaw, IDynamicFormEntity entity, Boolean isPublish) {
        try {
            byte[] docTemplated;

            Class<?> form = entity.getClass();

            Map<String, String> pairMetaNameValue;

            // fetch data from metadata values
            pairMetaNameValue = entity.getFormValue().stream().
                    collect(Collectors.toMap(t -> TemplatingHelper.toMetadataFormatName(getMetadataNameOfFormField(t.getFormField())), t -> Optional.ofNullable(t.getValue()).orElse("__")));

            // fetch data from form dynamic properties
            //   pair no_dokumen, penerima, penandatangan, pemerika & tembusan
            Field[] fields = form.getDeclaredFields();

            for (Field field : fields) {
                if (field.isAnnotationPresent(FieldMetadata.class)) {
                    FieldMetadata annotation = field.getAnnotation(FieldMetadata.class);

                    String key = TemplatingHelper.toMetadataFormatName(annotation.nama());
                    String value = "";

                    switch (annotation.type()) {
                        case MetadataValueType.MULTI_VALUE:
                            List<Object> objs = (List<Object>) ReflectionHelper.executeMethod(entity, TemplatingHelper.getGetterName(field.getName()));

                            StringTemplatingUtil stutil = new StringTemplatingUtil();
                            String template = annotation.format();

                            if (objs != null && objs.size() > 0) {
                                for (Object obj : objs) {
                                    String templated = stutil.templating(template, obj);

                                    if (templated == null || templated.isEmpty() || templated.replaceAll(" ", "").endsWith("||")) {
                                        try {
                                            if (obj.getClass() == PenandatanganSurat.class) {
                                                PenandatanganSurat o = ((PenandatanganSurat) obj);
                                                templated = formattedMasterOrgForTemplatingSurat(o.getOrganization(),o.getUser());
                                            } else if (obj.getClass() == PemeriksaSurat.class) {
                                                PemeriksaSurat o = ((PemeriksaSurat) obj);
                                                templated = formattedMasterOrgForTemplatingSurat(o.getOrganization(),o.getUser());
                                            } else if (obj.getClass() == PenerimaSurat.class) {
                                                PenerimaSurat o = ((PenerimaSurat) obj);
                                                if (entity.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_SURAT_DINAS)) {
                                                    templated = o.getVendor().getNama();
                                                }else {
                                                    if(o.getOrganization()!=null) {
                                                        templated = formattedMasterOrgForTemplatingSurat(o.getOrganization(),o.getUser());
                                                    }else{
                                                        templated = "";
                                                    }
                                                }
                                            } else if (obj.getClass() == TembusanSurat.class) {
                                                TembusanSurat o = ((TembusanSurat) obj);
                                                if(o.getOrganization()!=null){
                                                    templated = formattedMasterOrgForTemplatingSurat(o.getOrganization(),o.getUser());
                                                }else{
                                                    templated = o.getVendor().getNama();
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    if(templated!="") {
                                        value += templated + TemplatingSeparator.SEPARATOR_NEW_ENTITY;
                                    }
                                }
                                value.substring(0, value.length() - 2);
                            } else {
                                value = "  ";
                            }
                            break;
                        case MetadataValueType.SINGLE_VALUE:
                        default:
                            String resolved = ReflectionHelper.getFieldValue(entity, field.getName());
                            value = resolved;
                            break;
                    }
                    if (value == null || value.toLowerCase().equals("null"))
                        value = " ";

                    pairMetaNameValue.put(key, value);
                }
            }

               List<PenandatanganSuratDto> penandatanganSuratDtos = new LinkedList();

            boolean isPenandatanganKonspetor = false;

            List<PenandatanganSurat> penandatanganSurats = ((Surat) entity).getPenandatanganSurat();
            for (PenandatanganSurat i : penandatanganSurats) {
                penandatanganSuratDtos.add(new PenandatanganSuratDto(i));

                if (((Surat) entity).getKonseptor() != null) {
                    if (i.getOrganization().getIdOrganization().equals(((Surat) entity).getKonseptor().getIdOrganization()))
                        isPenandatanganKonspetor = true;
                }
            }

            //<editor-fold desc="handle tempat if exists">
            String noDok = ((Surat) entity).getNoDokumen();
            if (!StringUtils.isEmpty(noDok)) {
                pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.NO_DOKUMEN_KEY), noDok);
            } else {
                pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.NO_DOKUMEN_KEY), "__");
            }
            //</editor-fold>

            //<editor-fold desc="handle tgl if exists">
            Date approvedDate = ((Surat) entity).getApprovedDate();
            String status = ((Surat) entity).getStatus();
            if (approvedDate != null && status.equals("APPROVED")) {
                Locale locale = new Locale("in", "ID");

                String formatted = new SimpleDateFormat("dd MMMMMMMMMM yyyy", locale).format(approvedDate);

                pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TANGGAL_KEY), formatted);
            } else {
                pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TANGGAL_KEY), "__");
            }
            //</editor-fold>

            //<editor-fold desc="handle no dokumen if exists">
//            if(penandatanganSurats != null && penandatanganSurats.size() > 0 && !entity.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_UNDANGAN)) {
            if (penandatanganSurats != null && penandatanganSurats.size() > 0) {
                String tempat = Optional.ofNullable(penandatanganSurats.get(0).getOrganization().getArea().getKota()).map(MasterKota::getNamaKota).orElse("__");

                pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMPAT_KEY), tempat);
            } else {
                pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMPAT_KEY), "__");
            }
            //</editor-fold>

            //<editor-fold desc="handle tembusan">
            if(entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("NR") || entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SD")
                    || entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SDU")){
                List<MasterStrukturOrganisasi> listInt = ((Surat)entity).getTembusanSurat().stream().filter(a->a.getOrganization()!=null)
                        .map(b->b.getOrganization()).collect(Collectors.toList());

                String tembusanInt = "";

                for(MasterStrukturOrganisasi v : listInt){
                    tembusanInt += v.getOrganizationName() +" | "+v.getUser().getNameFront()+" "+v.getUser().getNameMiddleLast()+" | "+
                            v.getUser().getEmployeeId()+TemplatingSeparator.SEPARATOR_NEW_ENTITY;
                }

                if(StringUtils.isNotBlank(tembusanInt)) {
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_KEY), TemplatingSeparator.SEPARATOR_NEW_ENTITY.concat(tembusanInt));
                }else{
                    pairMetaNameValue.remove("tembusan");
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_KEY), " ");
                }
            }else {
                String tembusanVal = pairMetaNameValue.get(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_KEY));

                if (StringUtils.isNotBlank(tembusanVal)) {
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_KEY), "Tembusan: ".concat(TemplatingSeparator.SEPARATOR_NEW_ENTITY).concat(tembusanVal));
                } else {
                    pairMetaNameValue.remove("tembusan");
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_KEY), " ");
                }
            }
            //</editor-fold>

            //<editor-fold desc="handle penerima eksternal">
            List<MasterVendor> penerimaExtList = ((Surat) entity).getPenerimaSurat().stream().filter(a->a.getVendor()!=null)
                    .map(b->b.getVendor()).collect(Collectors.toList());
            if(!penerimaExtList.isEmpty()){
                String penerimaEksternalVal = "";

                for(MasterVendor v : penerimaExtList){
                    penerimaEksternalVal += v.getNama() + TemplatingSeparator.SEPARATOR_NEW_ENTITY;
                }

                if(StringUtils.isNotBlank(penerimaEksternalVal)) {
                    if (entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("NR") || entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SDU")) {
                        pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_EKSTERNAL_KEY), TemplatingSeparator.SEPARATOR_NEW_ENTITY.concat(penerimaEksternalVal));
                    }else{
                        pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_EKSTERNAL_KEY), "Penerima Eksternal: ".concat(TemplatingSeparator.SEPARATOR_NEW_ENTITY).concat(penerimaEksternalVal));
                    }
                }
            }
            //</editor-fold>

            //<editor-fold desc="handle list lampiran">
            String lampiran = pairMetaNameValue.get("lampiran");
            if(lampiran!=null) {
                if (!lampiran.toLowerCase().equals("ya") && !lampiran.equals("__")) {
                    Long idSurat = ((Surat) entity).getId();
                    String lampiranList = "";
                    if(idSurat!=null) {
                        List<GlobalAttachment> listLampiran = globalAttachmentService.getAttachmentDataByRef("t_surat"
                                , idSurat).collect(Collectors.toList());
                        if (!listLampiran.isEmpty()) {

                            for (GlobalAttachment la : listLampiran) {
                                lampiranList += la.getDocName() + TemplatingSeparator.SEPARATOR_NEW_ENTITY;
                            }
                        }
                        if (StringUtils.isNotBlank(lampiranList)) {
                            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.LAMPIRAN_LIST_KEY), TemplatingSeparator.SEPARATOR_NEW_ENTITY.concat(lampiranList));
                        } else {
                            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.LAMPIRAN_LIST_KEY), "__");
                        }
                    }
                } else {
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.LAMPIRAN_LIST_KEY), " ");
                }
            }else{
                pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.LAMPIRAN_LIST_KEY), " ");
            }
            //</editor-fold>

            //<editor-fold desc="handle penerima internal, pimpinan rapat dan pemeriksa if Notulen Rapat>
            if(entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("NR") || entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("UD") || entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SDU")){
                List<PenerimaSurat> penerimaSuratInternal = ((Surat) entity).getPenerimaSurat().stream().filter(a->a.getOrganization()!=null).collect(Collectors.toList());
                if(!penerimaSuratInternal.isEmpty()){
                    String penerimaInternal = "";
                    for(PenerimaSurat ps:penerimaSuratInternal){
                        penerimaInternal += formattedMasterOrgForTemplatingSurat(ps.getOrganization(),ps.getUser()) + TemplatingSeparator.SEPARATOR_NEW_ENTITY;
                    }

                    if(StringUtils.isNotBlank(penerimaInternal)){
                        pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_SURAT_LIST_KEY),TemplatingSeparator.SEPARATOR_NEW_ENTITY.concat(penerimaInternal));
                    }
                }else{
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_SURAT_LIST_KEY), "__");
                }

                String tmpPemimpin = pairMetaNameValue.get("pemimpin_rapat");
                if(!Strings.isNullOrEmpty(tmpPemimpin) && !tmpPemimpin.equals("__")){
                    String[] conditionalKeys = TemplatingHelper.PEMIMPIN_RAPAT_CONDITIONAL_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);
                    String [] pemimpinRapat = tmpPemimpin.split("\\|");

                    String value;
                    String labelJabatanPemimpin = pemimpinRapat[0];
                    String labelNamaPemimpin = pemimpinRapat[1];
                    String labelNippPemimpin= "NIPP "+pemimpinRapat[2];

                    value = labelJabatanPemimpin.concat(TemplatingHelper.SUBSTITUTE_COMMA);
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(conditionalKeys[0]), value);

                    value = WordUtils.capitalizeFully(labelNamaPemimpin).concat(",").concat(labelNippPemimpin);
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(conditionalKeys[1]), value.trim());
                }else{
                    String[] conditionalKeys = TemplatingHelper.PEMIMPIN_RAPAT_CONDITIONAL_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(conditionalKeys[0]), "__");
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(conditionalKeys[1]), "__");
                }

                List<MasterStrukturOrganisasi> listInt = ((Surat)entity).getPemeriksaSurat().stream().filter(a->a.getOrganization()!=null)
                        .map(b->b.getOrganization()).collect(Collectors.toList());

                String pemeriksa = "";

                for(MasterStrukturOrganisasi v : listInt){
                   /* pemeriksa += v.getOrganizationName() +" | "+v.getUser().getNameFront()+" "+v.getUser().getNameMiddleLast()+" | "+
                            v.getUser().getEmployeeId()+TemplatingSeparator.SEPARATOR_NEW_ENTITY;*/
                    pemeriksa += formattedMasterOrgForTemplatingSurat(v,v.getUser()) + TemplatingSeparator.SEPARATOR_NEW_ENTITY;
                }

                if(StringUtils.isNotBlank(pemeriksa)) {
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PEMERIKSA_SURAT_KEY), pemeriksa);
                } else {
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PEMERIKSA_SURAT_KEY), "__");
                }
            }
            //</editor-fold>

            //<editor-fold desc="handle tembusan eksternal">
            if (entity.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_SURAT_DINAS) || entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("NR")
                || entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SDU")) {
                List<TembusanSurat> tss = ((Surat) entity).getTembusanSurat();

                if (tss != null && !tss.isEmpty()) {
                    String tembusanEksternalVal = "";

                    for (TembusanSurat ts : tss) {
                        if (ts.getVendor() != null) {
                            String tsVal = ts.getVendor().getNama();

                            tembusanEksternalVal += tsVal + TemplatingSeparator.SEPARATOR_NEW_ENTITY;
                        }
                    }

                    if (StringUtils.isNotBlank(tembusanEksternalVal)) {
                        if (entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("NR") || entity.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SDU")) {
                            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_EKSTERNAL_KEY), TemplatingSeparator.SEPARATOR_NEW_ENTITY.concat(tembusanEksternalVal));
                        } else {
                            pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_EKSTERNAL_KEY), "Tembusan Eksternal: ".concat(TemplatingSeparator.SEPARATOR_NEW_ENTITY).concat(tembusanEksternalVal));
                        }
                    }else{
                        pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_EKSTERNAL_KEY), " ");
                    }
                }
            }
            //</editor-fold>

            handleMetadataPenandatangan(((Surat) entity), penandatanganSuratDtos, pairMetaNameValue, isPenandatanganKonspetor);

            if (entity.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_UNDANGAN)) {
                Surat surat = ((Surat) entity);
                List<SuratUndangan> listUndangan = surat.getUndangans();

                if (listUndangan != null && !listUndangan.isEmpty()) {
                    String tgl="";
                    String jam="";

                    Locale locale = new Locale("in", "ID");

                    for(int i=0;i<listUndangan.size();i++){
                        String tglU = new SimpleDateFormat("dd MMMMMMMMMM yyyy",locale).format(listUndangan.get(i).getStart());
                        String hari = new SimpleDateFormat("EEEE", locale).format(listUndangan.get(i).getStart());
                        String jamU = new SimpleDateFormat("HH:mm", locale).format(listUndangan.get(i).getStart())
                                + " s.d " + new SimpleDateFormat("HH:mm", locale).format(listUndangan.get(i).getEnd()) + " " + listUndangan.get(i).getZona();
                        String tglHari = hari+", "+tglU;
                        if(i==0){
                            tgl = tglHari;
                            jam = jamU;
                        }else{
                            tgl = tgl + "; " + tglHari;
                            jam = jam + "; " + jamU;
                        }
                    }

//                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_DATE_KEY), new SimpleDateFormat("dd MMMMMMMMMM yyyy", locale).format(suratUndangan.getStart()));
//                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_DAY_KEY), new SimpleDateFormat("EEEE", locale).format(suratUndangan.getStart()));
//                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_START_TIME_KEY), new SimpleDateFormat("HH:mm", locale).format(suratUndangan.getStart()));
//                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_END_TIME_KEY), new SimpleDateFormat("HH:mm", locale).format(suratUndangan.getEnd()));

                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_HARI_KEY),tgl);
                    pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_JAM_KEY),jam);
                }

//                MasterStrukturOrganisasi pimpinanRapat = penandatanganSurats.stream().filter(t -> t.getOrganization() != null).reduce((first, second) -> second).map(PenandatanganSurat::getOrganization).orElse(null);

//                if (pimpinanRapat!=null){
//                    String pimpinan = formattedMasterOrgForTemplatingSurat(pimpinanRapat);
//                    pairMetaNameValue.put("pemimpin_rapat",pimpinan);
//                }
            }

            MasterUser konseptor = ((Surat) entity).getKonseptor().getUser();
            if(konseptor!=null){
                String[] conditionalKeys = TemplatingHelper.KONSEPTOR_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

                String value;
                String labelJabatanKonseptor = konseptor.getKedudukan();
                String labelNamaKonseptor = konseptor.getNameFront()+" "+konseptor.getNameMiddleLast();
                String labelNippKonseptor = "";
                if(konseptor.getIsInternal().booleanValue()==true) {
                    labelNippKonseptor = "NIPP ".concat(konseptor.getEmployeeId());
                }
                value = labelJabatanKonseptor.concat(TemplatingHelper.SUBSTITUTE_COMMA);
                pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(conditionalKeys[0]), value);

                value = WordUtils.capitalizeFully(labelNamaKonseptor).concat(",").concat(labelNippKonseptor);
                pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(conditionalKeys[1]), value.trim());
            }

            if (entity instanceof Surat && isPublish) {
                Surat surat = (Surat) entity;

                if (StringUtils.isNotEmpty(surat.getNoDokumen())) {
                    try {
                        pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.NO_DOKUMEN_BARCODE_KEY), new String(Base64.getEncoder().encode(qrGenerateService.getBarcode(surat.getNoDokumen(), 280, 22))));
                    } catch (Exception e) {
                        logger.error(null, ExceptionUtil.getRealCause(e));
                    }

                    try {
                        pairMetaNameValue.put(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENANDATANGAN_QRCODE_KEY), new String(Base64.getEncoder().encode(qrGenerateService.getQRCode(String.format("%s/digi-sign-info/%s", applicationContext.getApplicationConfig().getFrontEndUrl(), surat.getId()), 80))));
                    } catch (Exception e) {
                        logger.error(null, ExceptionUtil.getRealCause(e));
                    }
                }
            }

            docTemplated = processTemplating(docTemplateRaw, pairMetaNameValue);

            return docTemplated;
        } catch (Exception e) {
            e.printStackTrace();

            return docTemplateRaw;
        }
    }

    private String formattedMasterOrgForTemplatingSurat(MasterStrukturOrganisasi o,MasterUser u) {
        if(u != null){
            return o.getOrganizationName() + " | "
                    + u.getNameFront() + " " + u.getNameMiddleLast() + " | "
                    + u.getEmployeeId();
        }else {
            return o.getOrganizationName() + " | "
                    + o.getUser().getNameFront() + " " + o.getUser().getNameMiddleLast() + " | "
                    + o.getUser().getEmployeeId();
        }
    }

    public List<LayoutingEntity> getLayoutingEntityJenisDokumen(Long idJenisDokumen) {
        List<LayoutingEntity> entities = new LinkedList();

        FormDefinition formDefinition = formDefinitionService.getActivated(idJenisDokumen);

        // fetch data from metadata values
        for (FormField ff : formDefinition.getFormFields()) {
            MasterMetadata mm = ff.getMetadata();

            if (mm.getNama().equals(TemplatingHelper.PENERIMA_SURAT_KEY) || mm.getNama().equals(TemplatingHelper.PENANDATANGAN_KEY))
                continue;

            String fieldName = TemplatingHelper.toMetadataFormatName(mm.getNama());
            String fieldType = ff.getType();

            entities.add(new LayoutingEntity(fieldName, fieldType));
        }

        addMustHaveLayoutingKeys(formDefinition, entities);

        if (formDefinition.getJenisDokumen().getBaseCode().equals(Constants.KODE_UNDANGAN)) {
            addUndanganKeys(entities);
        }

        if (!formDefinition.getJenisDokumen().getBaseCode().equals("ND")){
            entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_EKSTERNAL_KEY), "List"));
        }

        //Konseptor Surat
        String[] konseptorKey = TemplatingHelper.KONSEPTOR_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

        for (String key : konseptorKey) {
            entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(key), "List"));
        }

        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.NO_DOKUMEN_BARCODE_KEY), "Image") );
        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENANDATANGAN_QRCODE_KEY), "Image") );

        return entities;
    }

    public List<LayoutingEntity> getLayoutingEntitySurat(Long idSurat) {
        List<LayoutingEntity> entities = new LinkedList();

        Surat surat = suratService.getByIdSurat(idSurat);

        // fetch data from metadata values
        for (FormField ff : surat.getFormDefinition().getFormFields()) {
            MasterMetadata mm = masterMetadataService.find(ff.getId());

            String fieldName = TemplatingHelper.toMetadataFormatName(mm.getNama());
            String fieldType;

            switch (ff.getType().toLowerCase()) {
                default:
                case "string":
                    fieldType = "Text";
                    break;
                case "list":
                    fieldType = "List Text";
                    break;
            }

            entities.add(new LayoutingEntity(fieldName, fieldType));
        }

        // fetch data from surat properties
        //   pair no_dokumen, penerima, penandatangan, pemerika & tembusan
        Field[] fields = Surat.class.getDeclaredFields();

        for (Field field : fields) {
            if (field.isAnnotationPresent(FieldMetadata.class)) {
                FieldMetadata annotation = field.getAnnotation(FieldMetadata.class);

                String fieldName = TemplatingHelper.toMetadataFormatName(annotation.nama());
                String fieldType;

                switch (annotation.type()) {
                    default:
                    case MetadataValueType.SINGLE_VALUE:
                        fieldType = "Text";
                        break;
                    case MetadataValueType.MULTI_VALUE:
                        fieldType = "List Text";
                        break;
                }

                entities.add(new LayoutingEntity(fieldName, fieldType));
            }
        }

        addMustHaveLayoutingKeys(surat.getFormDefinition(), entities);

        if (surat.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_UNDANGAN)) {
            addUndanganKeys(entities);
        }

        return entities;
    }

    private void addMustHaveLayoutingKeys(FormDefinition formDefinition, List<LayoutingEntity> entities) {
        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.NO_DOKUMEN_KEY), "Text"));
        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TANGGAL_KEY), "Text"));
        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMPAT_KEY), "Text"));

        //templating key untuk list lampiran ketika metadata lampiran dipilih "TIDAK"
        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.LAMPIRAN_LIST_KEY),"List"));

//        if (!formDefinition.getJenisDokumen().getBaseCode().equals(Constants.KODE_UNDANGAN)) {
//            entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMPAT_KEY), "Text"));
//        }

        //kondisional templating (penerima surat)
        String[] penerimaSuratKondisionalKeys = TemplatingHelper.PENERIMA_SURAT_CONDITIONAL_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

        for (String key : penerimaSuratKondisionalKeys) {
            entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(key), "List"));
        }

        //templating penerima surat Notulen Rapat
        if(formDefinition.getJenisDokumen().getKodeJenisDokumen().equals("NR") || formDefinition.getJenisDokumen().getKodeJenisDokumen().equals("UD") || formDefinition.getJenisDokumen().getKodeJenisDokumen().equals("SDU")){
            entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_SURAT_LIST_KEY), "List"));
        }

        //penandatangan
        String[] penandatanganKondisionalKeys = TemplatingHelper.PENANDATANGAN_CONDITIONAL_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

        for (String key : penandatanganKondisionalKeys) {
            entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(key), "List"));
        }

        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_EKSTERNAL_KEY), "List"));

        Class<?> form = Surat.class;

        Field[] fields = form.getDeclaredFields();

        for (Field field : fields) {
            if (field.isAnnotationPresent(FieldMetadata.class)) {
                FieldMetadata annotation = field.getAnnotation(FieldMetadata.class);

                if (annotation.nama().equals(TemplatingHelper.PENANDATANGAN_KEY) || annotation.nama().equals(TemplatingHelper.PENERIMA_SURAT_KEY))
                    continue;

                String key = TemplatingHelper.toMetadataFormatName(annotation.nama());

                String fieldType;

                if (annotation.type() == MetadataValueType.MULTI_VALUE)
                    fieldType = "List";
                else
                    fieldType = "Text";

                entities.add(new LayoutingEntity(key, fieldType));
            }
        }
    }

    private void addUndanganKeys(List<LayoutingEntity> entities) {
//        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_DATE_KEY), "Text"));
//        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_DAY_KEY), "Text"));
//        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_START_TIME_KEY), "Text"));
//        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_END_TIME_KEY), "Text"));
        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_HARI_KEY), "Text"));
        entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(TemplatingHelper.UNDANGAN_JAM_KEY), "Text"));
        String[] pemimpinRapatConditionalKeys = TemplatingHelper.PEMIMPIN_RAPAT_CONDITIONAL_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

        for (String key : pemimpinRapatConditionalKeys) {
            entities.add(new LayoutingEntity(TemplatingHelper.toMetadataFormatName(key), "List"));
        }
    }

    public String generateTemporaryDocAlfresco(Long idJenisDokumen) {
        String documentId = jenisDokumenDocLibService.getDocIdByJenisDokumenId(idJenisDokumen);

        byte[] docBytes = fileService.download(documentId);

        return fileService.upload("temp.docx", docBytes);
    }

    public void deleteTemporaryGeneratedDocAlfresco(String docId) throws Exception {
        if (StringUtils.isNotBlank(docId)) {
            if (globalAttachmentService.findByDocId(docId) == null && jenisDokumenDocLibService.findByDocId(docId) == null) {
                fileService.delete(docId);
            } else throw new Exception("TemplatingService.deleteTemporaryGeneratedDocAlfresco : docId blank");
        } else throw new Exception("TemplatingService.deleteTemporaryGeneratedDocAlfresco : docId blank");
    }

    public void deleteDocAlfresco(String docId) throws Exception {
        if (StringUtils.isNotBlank(docId)) {
//            globalAttachmentService.findByDocId(docId)
        } else throw new Exception("TemplatingService.deleteTemporaryGeneratedDocAlfresco : docId blank");
    }
}
