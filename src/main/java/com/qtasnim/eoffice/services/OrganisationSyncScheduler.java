package com.qtasnim.eoffice.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.qtasnim.eoffice.JerseyClient;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.OrganizationWrapper;
import com.qtasnim.eoffice.db.SyncLog;
import com.qtasnim.eoffice.db.UserWrapper;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.util.ExceptionUtil;
import id.kai.ws.dto.*;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import javax.ejb.*;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Startup
@Singleton
public class OrganisationSyncScheduler implements IService {
    @Inject
    private SyncService syncService;

    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private BackgroundServiceLoggerService loggerService;

    @Inject
    private Logger logger;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    private SyncLogService syncLogService;

    @Inject
    @JerseyClient
    private Client client;

    private Boolean isRun = false;

    @Getter
    private SyncLog syncLog;

    @Lock(LockType.READ)
    @Schedule(second = "0", minute = "0", hour = "1", persistent = false)
    public void atSchedule() {
        if (!isRun && isRunnable()) {
            isRun = true;
            Instant start = Instant.now();
            syncLog = syncLogService.start(getClassName());
            loggerService.info(syncLog, "Organization Sync Started");

            try {
                sync();
            } catch (IOException e) {
                logger.error(null, e);
                loggerService.error(syncLog, e);
            }

            loggerService.info(syncLog, "Organization Sync Ended");
            syncLogService.stop(syncLog, start);
            syncLog = null;
            isRun = false;
        }
    }

    @Lock(LockType.READ)
    @Transactional(value = Transactional.TxType.REQUIRES_NEW)
    public void sync() throws IOException {
        List<OrganizationWrapper> api = new ArrayList<>();
        List<UserWrapper> api2 = new ArrayList<>();

        fetchAPI(api, api2);

        syncService.syncOrgAndEmployee(api, api2);
    }

    public void fetchAPI(List<OrganizationWrapper> api, List<UserWrapper> api2) throws IOException {
        DateUtil dateUtil = new DateUtil();
        List<HrisOrgDto> result = new ArrayList<>();
        List<HrisEmployeeDto> result2 = new ArrayList<>();
        downloadOrg(result);
        downloadPosisi(result);
        downloadEmp(result, result2);
        loggerService.info(syncLog, "HRIS Org & Posisi Size: " + result.size());
        loggerService.info(syncLog, "HRIS Employee Size: " + result2.size());

        List<OrganizationWrapper> _api = result
                .stream()
                .map(t -> new OrganizationWrapper(null, t, t.getOrgeh().trim(), t.getOutxt().trim(), Optional.ofNullable(t.getOuabv()).map(String::trim).orElse(null), t.getParnt().trim(),t.isPosisi(),t.isChief(),Optional.ofNullable(t.getPersa()).orElse(null),"I100",Optional.ofNullable(t.getClazz()).orElse(null),
                Optional.ofNullable(t.getGrade()).orElse(null), Optional.ofNullable(t.getKorsa()).map(String::trim).orElse(null)))
                .distinct()
                .collect(Collectors.toList());

        List<UserWrapper> _api2 = result2
                .stream()
                .filter(t -> StringUtils.isNotEmpty(t.getName()))
                .map(t -> {
                    UserWrapper userWrapper = new UserWrapper();
                    List<String> nameParts = new ArrayList<>(Arrays.asList(t.getName().split(" ")));
                    String nameFront = nameParts.get(0);
                    nameParts.remove(0);
                    String nameMiddleLast = String.join(" ", nameParts).trim();

                    userWrapper.setMasterUser(null);
                    userWrapper.setEmployeeDto(t);
                    userWrapper.setEmployeeId(Optional.ofNullable(t.getPernr()).map(String::trim).orElse(null));
                    userWrapper.setFullName(t.getName());
                    userWrapper.setNameFront(nameFront);
                    userWrapper.setNameMiddleLast(nameMiddleLast);
                    userWrapper.setEmail(Optional.ofNullable(t.getEmailKorporate()).filter(e -> EmailValidator.getInstance().isValid(e)).map(String::trim).orElse("-"));
                    userWrapper.setSalutation(t.getGender().toLowerCase().equals("perempuan") ? "Mrs." : "Mr.");
                    userWrapper.setKelamin(t.getGender());
                    userWrapper.setUsername(userWrapper.getEmployeeId());
                    userWrapper.setBusinessArea(t.getBusArea());
                    userWrapper.setKedudukan(t.getPangkatText());
                    userWrapper.setPersonalArea(t.getPersaText());
                    userWrapper.setPersonalAreaCode(t.getPersa());
                    userWrapper.setKorsa(t.getTextKorsa());
                    userWrapper.setKorsaId(t.getIdKorsa());
                    userWrapper.setKorsaAbbr(t.getAbbrKorsa());
                    userWrapper.setGrade(t.getGrade());
                    userWrapper.setOrganizationCode(t.getPlans());
                    userWrapper.setOrganizationName(t.getPosText());
                    userWrapper.setCompanyCode(t.getIdBukrs());
                    userWrapper.setCompanyName(t.getTextBukrs());
                    userWrapper.setTglLahir(Optional.ofNullable(dateUtil.getDateFromString(t.getBirthdate(), "yyyy-MM-dd")).orElse(new Date(Long.MIN_VALUE)));
                    userWrapper.setTempatLahir(t.getBirthplace());
                    userWrapper.setAgama(t.getAgama());
                    userWrapper.setKedudukan(t.getPosText());
                    userWrapper.setJabatan(t.getJobText());
                    userWrapper.setMobilePhone(t.getHpPribadi());
                    if(!t.getPersg().equals("2") || !t.getPersg().equals("5") || !t.getPersg().equals("6")) {
                        userWrapper.setIsActive(true);
                    }else{
                        userWrapper.setIsActive(false);
                    }
                    return userWrapper;
                })
                .collect(Collectors.toList());

        api.addAll(_api);
        api2.addAll(_api2);
    }

    private void downloadOrg(List<HrisOrgDto> result) throws IOException {
        ObjectMapper objectMapper = getObjectMapper();
        String url = UriBuilder.fromUri(applicationConfig.getSyncOrgHost())
                .toString();
        loggerService.info(syncLog, "Downloading: " + url);
        WebTarget webResource = client.target(url);
        Response response = webResource
                .request()
                .header("Authorization", String.format("Bearer %s", applicationConfig.getSyncAuthBearer()))
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get();
        String stringEntity = response.readEntity(String.class);

        HrisOrgSearchDto hrisOrgSearchDto = objectMapper.readValue(stringEntity, HrisOrgSearchDto.class);
        for (HrisOrgDto dto: hrisOrgSearchDto.getData()) {
            HrisOrgDto organizationDto = new HrisOrgDto();
            organizationDto.setOrgeh(dto.getOrgeh());
            organizationDto.setParnt(dto.getParnt());
            organizationDto.setOutxt(dto.getOutxt());
            organizationDto.setOuabv(dto.getOuabv());
            organizationDto.setLevel(dto.getLevel() + 1);
            organizationDto.setPosisi(false);
            organizationDto.setChief(false);
            organizationDto.setPersa(dto.getPersa());
            result.add(organizationDto);
        }
//        result.addAll(hrisOrgSearchDto.getData());
    }

    private Long countEmp() throws IOException {
        ObjectMapper objectMapper = getObjectMapper();
        String url = UriBuilder.fromUri(applicationConfig.getSyncEmployeeHost())
                .queryParam("count", "1")
                .toString();
        loggerService.info(syncLog, "Downloading: " + url);
        WebTarget webResource = client.target(url);
        Response response = webResource
                .request()
                .header("Authorization", String.format("Bearer %s", applicationConfig.getSyncAuthBearer()))
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get();
        String stringEntity = response.readEntity(String.class);
        HrisEmployeeCountDto employeeCountDto = objectMapper.readValue(stringEntity, HrisEmployeeCountDto.class);

        try {
            return Long.parseLong(employeeCountDto.getData());
        } catch (Exception e) {
            loggerService.info(syncLog, "Invalid Data: " + stringEntity);
            throw e;
        }
    }

    private void downloadEmp(List<HrisOrgDto> result, List<HrisEmployeeDto> result2) throws IOException {
        Long count = countEmp();
        ObjectMapper objectMapper = getObjectMapper();
        int limit = 1000;
        int page = 0;

        while (count > 0) {
            String url = UriBuilder.fromUri(applicationConfig.getSyncEmployeeHost())
                    .queryParam("page", page)
                    .queryParam("limit", limit)
                    .toString();
            loggerService.info(syncLog, "Downloading: " + url);
            WebTarget webResource = client.target(url);
            Response response = webResource
                    .request()
                    .header("Authorization", String.format("Bearer %s", applicationConfig.getSyncAuthBearer()))
                    .accept(MediaType.APPLICATION_JSON_TYPE)
                    .get();
            String stringEntity = response.readEntity(String.class);
            HrisEmployeeSearchDto employeeSearchDto = objectMapper.readValue(stringEntity, HrisEmployeeSearchDto.class);

            for (HrisEmployeeDto dto: employeeSearchDto.getData()) {
                if (!Optional.ofNullable(dto.getPlans()).orElse("").equals("99999999")) {
                    HrisOrgDto org = result.stream()
                            .filter(t -> t.getOrgeh().equals(dto.getOrgeh()) && StringUtils.isNotEmpty(dto.getOrgeh()))
                            .findFirst().orElse(null);
                    if (org != null) {
                        HrisOrgDto organizationDto = new HrisOrgDto();
                        organizationDto.setOrgeh(dto.getPlans());
                        organizationDto.setParnt(dto.getOrgeh());
                        organizationDto.setOutxt(dto.getPosText());
                        organizationDto.setLevel(org.getLevel() + 1);

                        result.add(organizationDto);
                    }

                    result2.add(dto);
                }
            }

            count = count - employeeSearchDto.getData().size();
            page++;
        }
    }

    private Long countPosisi() throws IOException {
        ObjectMapper objectMapper = getObjectMapper();
        String url = UriBuilder.fromUri(applicationConfig.getSyncPosisiHost())
                .queryParam("count", "1")
                .toString();
        loggerService.info(syncLog, "Downloading: " + url);
        WebTarget webResource = client.target(url);
        Response response = webResource
                .request()
                .header("Authorization", String.format("Bearer %s", applicationConfig.getSyncAuthBearer()))
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get();
        String stringEntity = response.readEntity(String.class);
        HrisPosisiCountDto employeeCountDto = objectMapper.readValue(stringEntity, HrisPosisiCountDto.class);

        try {
            return Long.parseLong(employeeCountDto.getCount());
        } catch (Exception e) {
            loggerService.info(syncLog, "Invalid Data: " + stringEntity);
            throw e;
        }
    }
    private void downloadPosisi(List<HrisOrgDto> result) throws IOException {
        Long count = countPosisi();
        ObjectMapper objectMapper = getObjectMapper();
        int limit = 1000;
        int page = 0;

        while (count > 0) {
            String url = UriBuilder.fromUri(applicationConfig.getSyncPosisiHost())
                    .queryParam("page", page)
                    .queryParam("limit", limit)
                    .toString();
            loggerService.info(syncLog, "Downloading: " + url);
            WebTarget webResource = client.target(url);
            Response response = webResource
                    .request()
                    .header("Authorization", String.format("Bearer %s", applicationConfig.getSyncAuthBearer()))
                    .accept(MediaType.APPLICATION_JSON_TYPE)
                    .get();
            String stringEntity = response.readEntity(String.class);
            HrisPosisiSearchDto employeeSearchDto = objectMapper.readValue(stringEntity, HrisPosisiSearchDto.class);

            for (HrisPosisiDto dto: employeeSearchDto.getData()) {
                HrisOrgDto org = result.stream()
                        .filter(t -> t.getOrgeh().equals(dto.getOrgeh()) && StringUtils.isNotEmpty(dto.getOrgeh()))
                        .findFirst().orElse(null);
                if (org != null) {
                    HrisOrgDto organizationDto = new HrisOrgDto();
                    organizationDto.setOrgeh(dto.getPlans());
                    organizationDto.setParnt(dto.getOrgeh());
                    organizationDto.setOutxt(dto.getPstxt());
                    organizationDto.setOuabv(dto.getPsabv());
                    organizationDto.setLevel(org.getLevel() + 1);
                    organizationDto.setPosisi(true);
                    organizationDto.setChief(dto.getChief() !=null ? dto.getChief().toLowerCase().equals("x") ? true : false : false);
                    organizationDto.setClazz(dto.getClazz());
                    organizationDto.setGrade(dto.getGrade());
                    organizationDto.setKorsa(dto.getKorsa().equals("00000000") ? null : dto.getKorsa());
                    result.add(organizationDto);
                }
            }

            count = count - employeeSearchDto.getData().size();
            page++;
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

        return objectMapper;
    }

    @Override
    public String getName() {
        return "HRIS Organization Sync";
    }

    public String getClassName() {
        return Stream.of(getClass().getName().split("\\$")).findFirst().orElse(getClass().getName());
    }

    @Override
    public Boolean isRunnable() {
        String servername = System.getProperty("rds.servername");
        String worker = applicationConfig.getBackgroundServiceWorker();

        return Optional.ofNullable(worker).map(s -> worker.equals(servername)).orElse(true);
    }

    public void setSyncLog(SyncLog syncLog) {
        this.syncLog = syncLog;
    }

    @Override
    @Asynchronous
    public void run() {
        if (!isRun) {
            isRun = true;
            Instant start = Instant.now();
            syncLog = syncLogService.start(getClassName());
            loggerService.info(syncLog, "Service starting");

            try {
                List<OrganizationWrapper> api = new ArrayList<>();
                List<UserWrapper> api2 = new ArrayList<>();
                fetchAPI(api, api2);

                masterStrukturOrganisasiService.sync(api);
                masterUserService.sync(api2);
            } catch (Exception e) {
                Throwable realCause = ExceptionUtil.getRealCause(e);
                logger.error(realCause.getMessage(), realCause);
                loggerService.error(syncLog, realCause);
            }

            loggerService.info(syncLog, "Service stopped");
            loggerService.info(syncLog, "DONE");
            syncLogService.stop(syncLog, start);
            syncLog = null;
            isRun = false;
        }
    }

    @Override
    public Boolean isRun() {
        return isRun;
    }
}
