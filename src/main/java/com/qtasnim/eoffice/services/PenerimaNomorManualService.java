package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.PemeriksaNomorManual;
import com.qtasnim.eoffice.db.PenerimaNomorManual;
import com.qtasnim.eoffice.db.PenomoranManual;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class PenerimaNomorManualService extends AbstractFacade<PenerimaNomorManual> {

    @Override
    protected Class<PenerimaNomorManual> getEntityClass() {
        return PenerimaNomorManual.class;
    }

    @Inject
    private PenomoranManualService penomoranManualService;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    public List<PenerimaNomorManual> getAllBy(Long id){
        PenomoranManual permohonan = penomoranManualService.find(id);
        return db().where(q->q.getPermohonan().equals(permohonan)).toList();
    }

    public PenerimaNomorManual getByIdPermohonanOrg(Long id, Long idOrg){
        PenomoranManual permohonan = penomoranManualService.find(id);
        MasterStrukturOrganisasi org = organisasiService.find(idOrg);
        return db().where(q->q.getOrganization().equals(org) && q.getPermohonan().equals(permohonan)).findFirst().orElse(null);
    }
}
