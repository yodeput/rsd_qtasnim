package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.PenerimaDistribusi;
import com.qtasnim.eoffice.db.DistribusiDokumen;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class PenerimaDistribusiService extends AbstractFacade<PenerimaDistribusi>{

    @Override
    protected Class<PenerimaDistribusi> getEntityClass() {
        return PenerimaDistribusi.class;
    }

    @Inject
    private DistribusiDokumenService distribusiService;

    @Inject
    @ISessionContext
    private Session userSession;

    public List<PenerimaDistribusi> getAllBy(Long idDistribusi){
        DistribusiDokumen distribusi = distribusiService.find(idDistribusi);
        return db().where(q->q.getDistribusi().equals(distribusi)).toList();
    }

    public PenerimaDistribusi getByOrgAndIdDistribusi(Long idDistribusi){
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        DistribusiDokumen distribusi = distribusiService.find(idDistribusi);
        return db().where(q->q.getDistribusi().equals(distribusi) && q.getOrganization().equals(org)).findFirst().orElse(null);
    }
}
