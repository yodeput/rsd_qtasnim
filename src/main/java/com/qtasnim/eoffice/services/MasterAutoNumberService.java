package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterAutoNumber;
import com.qtasnim.eoffice.services.numbering.AutoNumberHandler;
import com.qtasnim.eoffice.services.numbering.RecordingObject;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterAutoNumberDto;
import com.qtasnim.eoffice.services.numbering.Recorder;

import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class MasterAutoNumberService extends AbstractFacade<MasterAutoNumber> {
    private static final long serialVersionUID = 1L;
    @Inject
    private AutoNumberHandler autoNumberHandler;
    private Recorder recorder;

    @Override
    protected Class<MasterAutoNumber> getEntityClass() {
        return MasterAutoNumber.class;
    }

    public JPAJinqStream<MasterAutoNumber> getAll() {
        return db();
    }

    public <T> AutoNumberHandler<T> from(Class<T> object) {
        recorder = RecordingObject.create(object);
        autoNumberHandler.setRecorder(recorder);
        autoNumberHandler.setObject(object);

        return autoNumberHandler;
    }

    public MasterAutoNumber findByTitle(String title) {
        return db().where(t -> t.getTitle().equals(title)).findFirst().orElse(null);
    }

    public void saveOrEdit(Long id, MasterAutoNumberDto dao) {
        MasterAutoNumber obj;
        boolean isNew = id==null;
        DateUtil dateUtil = new DateUtil();

        if(isNew){
            obj = new MasterAutoNumber();
        } else {
            obj = this.find(id);
        }

        obj.setTitle(dao.getTitle());
        obj.setValue(dao.getValue());
        obj.setFormat(dao.getFormat());
        obj.setResetPeriod(dao.getResetPeriod());
        obj.setNextReset(dateUtil.getDateFromISOString(dao.getNextReset()));

        if(isNew){
            this.create(obj);
        } else {
            this.edit(obj);
        }

    }

}