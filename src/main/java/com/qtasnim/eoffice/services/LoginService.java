package com.qtasnim.eoffice.services;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.security.NotAuthorizedException;
import com.qtasnim.eoffice.security.PasswordHash;

/**
 * Service untuk menghandle operasi login
 * 
 * @author seno@qtasnim.com
 */

@Stateless
@LocalBean
public class LoginService {
    @Inject
    private PasswordHash passwordHash;

    @Inject
    private MasterUserService masterUserService;

    public void login(MasterUser user, String password) throws InternalServerErrorException {
        String salt = user.getPasswordSalt();
        String hash = user.getPasswordHash();
        String inputPassHash = passwordHash.getHash(password, salt);

        if (!hash.equals(inputPassHash)) {
            throw new NotAuthorizedException("Username/Password Salah");
        }

        refreshSalt(user, password);
        masterUserService.edit(user);
    }

    private void refreshSalt(MasterUser user, String password) throws InternalServerErrorException {
        List<String> newSalt;
        try {
            newSalt = passwordHash.getSaltPassword(password);

            user.setPasswordHash(newSalt.get(1));
            user.setPasswordSalt(newSalt.get(0));
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException("Internal error", e);
        }

    }

    public void patternAuth(MasterUser user,String pattern) throws InternalServerErrorException{
        if (!user.getKeyPattern().equals(pattern)) {
            throw new NotAuthorizedException("Pattern not recognize");
        }
    }
}