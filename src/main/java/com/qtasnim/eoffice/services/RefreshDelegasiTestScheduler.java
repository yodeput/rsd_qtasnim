package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.ws.WorkflowResource;
import liquibase.util.StringUtils;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Startup
@Singleton
public class RefreshDelegasiTestScheduler implements IService {
    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private ProcessTaskService processTaskService;

    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private WorkflowResource workflowResource;

    @Inject
    private SyncLogService syncLogService;

    @Inject
    private BackgroundServiceLoggerService loggerService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private PemeriksaSuratService pemeriksaSuratService;

    @Inject
    private PenandatanganSuratService penandatanganSuratService;

    @Inject
    private PemeriksaDistribusiService pemeriksaDistribusiService;

    @Inject
    private PenandatanganDistribusiService penandatanganDistribusiService;

    @Inject
    private PenerimaPermohonanDokumenService penerimaPermohonanDokumenService;

    @Inject
    private Logger logger;

    private SyncLog syncLog;

    private Boolean isRun = false;

    @Override
    public String getName() {
        return "Refresh Delegasi (TEST ONLY)";
    }

    @Override
    @Lock(LockType.READ)
//    @Schedule(second = "0", minute = "0", hour = "2")
    public void run() {
        if (!isRun && isRunnable()) {
            isRun = true;
            Instant start = Instant.now();
            syncLog = syncLogService.start(getClassName());
            loggerService.info(syncLog, "Service starting");

            try {
                List<ProcessTask> delegasiPendingTask = processTaskService.getDelegasiApprovalPendingTask();
                List<ProcessTask> nonDelegasiPendingTask = processTaskService.getNonDelegasiApprovalPendingTask();
                List<MasterDelegasi> activeDelegasi = masterDelegasiService.getAll().toList();
                List<ProcessTask> needRefreshProcesses = new ArrayList<>();
                List<ProcessTask> noNeedRefreshCamunda = new ArrayList<>();
                IWorkflowProvider workflowProvider = masterWorkflowProviderService.getByProviderName(applicationContext.getApplicationConfig().getWorkflowProvider()).getWorkflowProvider();

                // remove delegasi
                delegasiPendingTask.forEach(task -> {
                    String delegasiType = Optional.of(task.getTaskName()).filter(t -> t.contains("Pelakhar")).map(s -> "Pelakhar").orElse("PYMT");
                    boolean isPenandatangan = task.getTaskName().contains("Penandatangan");
                    boolean isPemeriksa = task.getTaskName().contains("Pemeriksa");
                    boolean isNeedRefresh = false;

                    if (StringUtils.isNotEmpty(task.getRelatedAssigneeRecordId())) {
                        if (task.getProcessInstance().getRelatedEntity().equals(Surat.class.getName())) {
                            if (isPemeriksa) {
                                PemeriksaSurat pemeriksaSurat = pemeriksaSuratService.find(Long.parseLong(task.getRelatedAssigneeRecordId()));

                                if (pemeriksaSurat != null) {
                                    isNeedRefresh = activeDelegasi.stream().noneMatch(t ->
                                            t.getFrom().getIdOrganization().equals(pemeriksaSurat.getOrganization().getIdOrganization())
                                                    && t.getTo().getIdOrganization().equals(task.getAssignee().getIdOrganization())
                                                    && t.getTipe().equals(delegasiType));

                                    /**
                                     * [PATCH ISSUE] TL-53 : REFRESH DELEGASI
                                     *      PYMT diganti setelah pelakhar approve
                                     */
                                    if (isNeedRefresh && delegasiType.equals("PYMT") && activeDelegasi.stream().anyMatch(t -> t.getTipe().equals("PYMT") && t.getFrom().getIdOrganization().equals(pemeriksaSurat.getOrganization().getIdOrganization()))) {
                                        noNeedRefreshCamunda.add(task);
                                    }
                                }
                            }

                            if (isPenandatangan) {
                                PenandatanganSurat penandatanganSurat = penandatanganSuratService.find(Long.parseLong(task.getRelatedAssigneeRecordId()));

                                if (penandatanganSurat != null) {
                                    isNeedRefresh = activeDelegasi.stream().noneMatch(t ->
                                            t.getFrom().getIdOrganization().equals(penandatanganSurat.getOrganization().getIdOrganization())
                                                    && t.getTo().getIdOrganization().equals(task.getAssignee().getIdOrganization())
                                                    && t.getTipe().equals(delegasiType));

                                    /**
                                     * [PATCH ISSUE] TL-53 : REFRESH DELEGASI
                                     *      PYMT diganti setelah pelakhar approve
                                     */
                                    if (isNeedRefresh && delegasiType.equals("PYMT") && activeDelegasi.stream().anyMatch(t -> t.getTipe().equals("PYMT") && t.getFrom().getIdOrganization().equals(penandatanganSurat.getOrganization().getIdOrganization()))) {
                                        noNeedRefreshCamunda.add(task);
                                    }
                                }
                            }
                        } else if (task.getProcessInstance().getRelatedEntity().equals(DistribusiDokumen.class.getName())) {
                            if (isPemeriksa) {
                                PemeriksaDistribusi pemeriksaDistribusi = pemeriksaDistribusiService.find(Long.parseLong(task.getRelatedAssigneeRecordId()));

                                if (pemeriksaDistribusi != null) {
                                    isNeedRefresh = activeDelegasi.stream().noneMatch(t ->
                                            t.getFrom().getIdOrganization().equals(pemeriksaDistribusi.getOrganization().getIdOrganization())
                                                    && t.getTo().getIdOrganization().equals(task.getAssignee().getIdOrganization())
                                                    && t.getTipe().equals(delegasiType));

                                    /**
                                     * [PATCH ISSUE] TL-53 : REFRESH DELEGASI
                                     *      PYMT diganti setelah pelakhar approve
                                     */
                                    if (isNeedRefresh && delegasiType.equals("PYMT") && activeDelegasi.stream().anyMatch(t -> t.getTipe().equals("PYMT") && t.getFrom().getIdOrganization().equals(pemeriksaDistribusi.getOrganization().getIdOrganization()))) {
                                        noNeedRefreshCamunda.add(task);
                                    }
                                }
                            }

                            if (isPenandatangan) {
                                PenandatanganDistribusi penandatanganDistribusi = penandatanganDistribusiService.find(Long.parseLong(task.getRelatedAssigneeRecordId()));

                                if (penandatanganDistribusi != null) {
                                    isNeedRefresh = activeDelegasi.stream().noneMatch(t ->
                                            t.getFrom().getIdOrganization().equals(penandatanganDistribusi.getOrganization().getIdOrganization())
                                                    && t.getTo().getIdOrganization().equals(task.getAssignee().getIdOrganization())
                                                    && t.getTipe().equals(delegasiType));

                                    /**
                                     * [PATCH ISSUE] TL-53 : REFRESH DELEGASI
                                     *      PYMT diganti setelah pelakhar approve
                                     */
                                    if (isNeedRefresh && delegasiType.equals("PYMT") && activeDelegasi.stream().anyMatch(t -> t.getTipe().equals("PYMT") && t.getFrom().getIdOrganization().equals(penandatanganDistribusi.getOrganization().getIdOrganization()))) {
                                        noNeedRefreshCamunda.add(task);
                                    }
                                }
                            }
                        } else if (task.getProcessInstance().getRelatedEntity().equals(PermohonanDokumen.class.getName())) {
                            PermohonanDokumenPenerima penerima = penerimaPermohonanDokumenService.find(Long.parseLong(task.getRelatedAssigneeRecordId()));

                            if (penerima != null) {
                                isNeedRefresh = activeDelegasi.stream().noneMatch(t ->
                                        t.getFrom().getIdOrganization().equals(penerima.getOrganization().getIdOrganization())
                                                && t.getTo().getIdOrganization().equals(task.getAssignee().getIdOrganization())
                                                && t.getTipe().equals(delegasiType));

                                /**
                                 * [PATCH ISSUE] TL-53 : REFRESH DELEGASI
                                 *      PYMT diganti setelah pelakhar approve
                                 */
                                if (isNeedRefresh && delegasiType.equals("PYMT") && activeDelegasi.stream().anyMatch(t -> t.getTipe().equals("PYMT") && t.getFrom().getIdOrganization().equals(penerima.getOrganization().getIdOrganization()))) {
                                    noNeedRefreshCamunda.add(task);
                                }
                            }
                        }
                    }

                    if (isNeedRefresh) {
                        List<ProcessTask> activeTasks = workflowProvider.getActiveTasks(task.getProcessInstance());
                        boolean isTaskValid = activeTasks.stream().anyMatch(t -> t.getTaskId().equals(task.getTaskId()));

                        if (isTaskValid) {
                            needRefreshProcesses.add(task);

                            loggerService.info(syncLog, String.format("Status Delegasi %s untuk %s pada surat id %s akan dihapus", delegasiType, task.getAssignee().getOrganizationCode(), task.getProcessInstance().getSurat().getId()));
                        }
                    }
                });

                // remove delegasi
                nonDelegasiPendingTask.forEach(task -> {
                    boolean isNeedRefresh = activeDelegasi.stream().anyMatch(t ->
                            t.getFrom().getIdOrganization().equals(task.getAssignee().getIdOrganization()));

                    if (isNeedRefresh) {
                        List<ProcessTask> activeTasks = workflowProvider.getActiveTasks(task.getProcessInstance());
                        boolean isTaskValid = activeTasks.stream().anyMatch(t -> t.getTaskId().equals(task.getTaskId()));

                        if (isTaskValid) {
                            needRefreshProcesses.add(task);

                            if (task.getProcessInstance().getRelatedEntity().equals(Surat.class.getName())) {
                                loggerService.info(syncLog, String.format("Status Delegasi untuk %s pada surat id %s akan ditambahkan", task.getAssignee().getOrganizationCode(), task.getProcessInstance().getSurat().getId()));
                            } else {
                                loggerService.info(syncLog, String.format("Status Delegasi untuk %s  akan ditambahkan", task.getAssignee().getOrganizationCode()));
                            }
                        }
                    }
                });

            } catch (Exception e) {
                Throwable realCause = ExceptionUtil.getRealCause(e);
                logger.error(realCause.getMessage(), realCause);
                loggerService.error(syncLog, realCause);
            }

            loggerService.info(syncLog, "Service stopped");
            loggerService.info(syncLog, "DONE");
            syncLogService.stop(syncLog, start);
            syncLog = null;
            isRun = false;
        }
    }

    @Override
    public Boolean isRun() {
        return isRun;
    }

    @Override
    public String getClassName() {
        return Stream.of(getClass().getName().split("\\$")).findFirst().orElse(getClass().getName());
    }

    @Override
    public Boolean isRunnable() {
        String servername = System.getProperty("rds.servername");
        String worker = applicationConfig.getBackgroundServiceWorker();

        return Optional.ofNullable(worker).map(s -> worker.equals(servername)).orElse(true);
    }
}
