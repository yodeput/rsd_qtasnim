package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.OrganizationWrapper;
import com.qtasnim.eoffice.db.UserWrapper;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@LocalBean
@Stateless
public class SyncService {
    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private MasterUserService masterUserService;

    public void syncOrgAndEmployee(List<OrganizationWrapper> apiOrg, List<UserWrapper> apiEmployee) {
        masterStrukturOrganisasiService.sync(apiOrg);
        masterUserService.sync(apiEmployee);
    }
}
