package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.PemeriksaDistribusi;
import com.qtasnim.eoffice.db.DistribusiDokumen;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class PemeriksaDistribusiService extends AbstractFacade<PemeriksaDistribusi> {

    @Override
    protected Class<PemeriksaDistribusi> getEntityClass() {
        return PemeriksaDistribusi.class;
    }

    @Inject
    private DistribusiDokumenService distribusiService;

    @Inject
    @ISessionContext
    private Session userSession;

    public List<PemeriksaDistribusi> getAllBy(Long idDistribusi) {
        DistribusiDokumen distribusi = distribusiService.find(idDistribusi);
        return db().where(q -> q.getDistribusi().equals(distribusi)).toList();
    }

    public PemeriksaDistribusi getByIdDistribusiOrg(Long idDistribusi) {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        DistribusiDokumen distribusi = distribusiService.find(idDistribusi);
        return db().where(q -> q.getDistribusi().equals(distribusi) && q.getOrganization().equals(org)).findFirst().orElse(null);
    }

    public PemeriksaDistribusi getByIdOrganisasi(Long idOrg) {
        return db().where(t -> t.getOrganization().getIdOrganization().equals(idOrg)).findFirst().orElse(null);
    }
}
