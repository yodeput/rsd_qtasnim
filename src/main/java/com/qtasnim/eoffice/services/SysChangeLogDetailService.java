package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.SysChangeLog;
import com.qtasnim.eoffice.db.SysChangeLogDetail;
import com.qtasnim.eoffice.ws.dto.SysChangeLogDetailDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;


@Stateless
@LocalBean
public class SysChangeLogDetailService extends AbstractFacade<SysChangeLogDetail> {
    private static final long serialVersionUID = 1L;

    @Inject
    CompanyCodeService companyCodeService;
    @Inject
    SysChangeLogService sysChangeLogService;

    @Override
    protected Class<SysChangeLogDetail> getEntityClass() {
        return SysChangeLogDetail.class;
    }

    public JPAJinqStream<SysChangeLogDetail> getAll() {
        return db();
    }

    public SysChangeLogDetail save(Long id, SysChangeLogDetailDto dao) {
        SysChangeLogDetail model = new SysChangeLogDetail();

        if (id == null) {
            this.create(addOrEdit(model, dao));
        } else {
            model = this.find(id);
            this.edit(addOrEdit(model, dao));
        }
        return model;
    }

    private SysChangeLogDetail addOrEdit(SysChangeLogDetail model, SysChangeLogDetailDto dao) {
        if (model.getId() == null) {
            SysChangeLog log = sysChangeLogService.find(dao.getIdChangeLog());
            model.setChangeLog(log);
        }

        model.setDescription(dao.getDescription());

        return model;
    }

    public SysChangeLogDetail delete(Long id) {
        SysChangeLogDetail model = this.find(id);
        this.remove(model);
        return model;
    }

}