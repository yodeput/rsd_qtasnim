package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.Agenda;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.AgendaDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class AgendaService extends AbstractFacade<Agenda>{

    @Override
    protected Class<Agenda> getEntityClass() {
        return Agenda.class;
    }

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    public JPAJinqStream<Agenda> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public void saveOrEdit(Long id, AgendaDto dao){
        boolean isNew =id==null;
        Agenda model = new Agenda();
        DateUtil dateUtil = new DateUtil();
        try{
            if(isNew){
                model.setPerihal(dao.getPerihal());
                if(dao.getIdCompany()!=null){
                    CompanyCode cCode = companyCodeService.find(dao.getIdCompany());
                    model.setCompanyCode(cCode);
                } else {
                    model.setCompanyCode(null);
                }
                model.setDescription(dao.getDescription());
                if(dao.getEnd()!=null){
                    model.setEnd(dateUtil.getDateFromISOString(dao.getEnd()));
                } else {
                    model.setEnd(dateUtil.getDefValue());
                }
                model.setLocation(dao.getLocation());
                if(dao.getIdOrganization()!=null){
                    MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getIdOrganization());
                    model.setOrganization(org);
                } else {
                    model.setOrganization(null);
                }
                model.setStart(dateUtil.getDateFromISOString(dao.getStart()));
                this.create(model);
            }else{
                model = this.find(id);
                model.setPerihal(dao.getPerihal());
                if(dao.getIdCompany()!=null){
                    CompanyCode cCode = companyCodeService.find(dao.getIdCompany());
                    model.setCompanyCode(cCode);
                } else {
                    model.setCompanyCode(null);
                }
                model.setDescription(dao.getDescription());
                if(dao.getEnd()!=null){
                    model.setEnd(dateUtil.getDateFromISOString(dao.getEnd()));
                } else {
                    model.setEnd(dateUtil.getDefValue());
                }
                model.setLocation(dao.getLocation());
                if(dao.getIdOrganization()!=null){
                    MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getIdOrganization());
                    model.setOrganization(org);
                } else {
                    model.setOrganization(null);
                }
                model.setStart(dateUtil.getDateFromISOString(dao.getStart()));
                this.edit(model);
            }
        }catch (Exception ex){

        }
    }
}
