package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.BoardDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class BoardService extends AbstractFacade<Board> {

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyService;

    @Override
    protected Class<Board> getEntityClass() {
        return Board.class;
    }

    public JPAJinqStream<Board> getBoards() {
        CompanyCode company = userSession.getUserSession().getUser().getCompanyCode();
        return db().where(a->a.getCompanyCode().equals(company) && a.getIsDeleted().booleanValue()==false);
    }

    public Board getBoard(Long id) {
        CompanyCode company = userSession.getUserSession().getUser().getCompanyCode();
        return db().where(a->a.getCompanyCode().equals(company) && a.getId().equals(id) && a.getIsDeleted().booleanValue()==false).findFirst().orElse(null);
    }

    public Board saveOrEdit(BoardDto dao, Long id) throws Exception {
        try{
            Board obj = new Board();
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = companyService.getByCode(user.getCompanyCode().getCode());
            if(id==null){
                obj.setTitle(dao.getTitle());
                obj.setIsDeleted(false);
                obj.setCompanyCode(company);
                create(obj);
            }else{
                obj = find(id);
                obj.setTitle(dao.getTitle());
                obj.setIsDeleted(false);
                obj.setCompanyCode(company);
                edit(obj);
            }
            getEntityManager().flush();
            getEntityManager().refresh(obj);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public Board deleteBoard(Long id) throws Exception {
        try{
            Board obj = find(id);
            obj.setIsDeleted(true);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
}
