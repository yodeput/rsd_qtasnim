package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.SyncLog;
import com.qtasnim.eoffice.db.SyncLogDetail;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.List;

@Stateless
@LocalBean
public class SyncLogDetailService extends AbstractFacade<SyncLogDetail> {
    @Override
    protected Class<SyncLogDetail> getEntityClass() {
        return SyncLogDetail.class;
    }

    public List<SyncLogDetail> getLog(Long idSyncLog) {
        return db()
                .where(t -> t.getSyncLog().getId().equals(idSyncLog))
                .sortedBy(SyncLogDetail::getId)
                .toList();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void log(SyncLog syncLog, String type, String content) {
        SyncLogDetail detail = new SyncLogDetail();
        detail.setSyncLog(syncLog);
        detail.setContent(content);
        detail.setType(type);

        create(detail);
    }
}
