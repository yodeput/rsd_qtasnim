package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterEmailTemplate;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class MasterEmailTemplateService extends AbstractFacade<MasterEmailTemplate> {
    @Override
    protected Class<MasterEmailTemplate> getEntityClass() {
        return MasterEmailTemplate.class;
    }

    public JPAJinqStream<MasterEmailTemplate> getAll() {return db();
    }
}
