package com.qtasnim.eoffice.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.MessageType;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.JenisDokumenDocLib;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Transaction;
import com.qtasnim.eoffice.security.IRequestContext;
import com.qtasnim.eoffice.ws.dto.DocActivityMessageDto;
import com.qtasnim.eoffice.ws.dto.DocActivityPayloadDto;
import com.qtasnim.eoffice.ws.dto.JenisDokumenDocLibDto;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

@LocalBean
@Stateless
public class DocActivityNotificationService {
    @Resource(mappedName = Constants.JNDI_DOCUMENT_ACTIVITY_QUEUE)
    private Queue myQueue;

    @Inject
    private JMSContext jmsContext;

    @Inject
    private Logger logger;

    @Inject
    private FileService fileService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    @IRequestContext
    private Transaction transaction;

    @Inject
    private ApplicationConfig applicationConfig;

    public void jenisDokumentDocLibSaved(String idJenisDokumen, JenisDokumenDocLib docLib) {
        DocActivityMessageDto message = new DocActivityMessageDto();
        message.setPayload(new DocActivityPayloadDto(new JenisDokumenDocLibDto(docLib, fileService)));

        try {
            IMessagingProvider messagingProvider = applicationConfig.getMessagingProvider();
            messagingProvider.send(MessageType.DOC_ACT, map -> map.put("idJenisDokumen", idJenisDokumen), getObjectMapper().writeValueAsString(message));
        } catch (JsonProcessingException e) {
            logger.error(null, e);
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
