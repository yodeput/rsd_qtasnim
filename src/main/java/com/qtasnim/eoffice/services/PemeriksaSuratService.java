package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.PemeriksaSurat;
import com.qtasnim.eoffice.db.Surat;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class PemeriksaSuratService extends AbstractFacade<PemeriksaSurat> {

    @Override
    protected Class<PemeriksaSurat> getEntityClass() {
        return PemeriksaSurat.class;
    }

    @Inject
    private SuratService suratService;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    public List<PemeriksaSurat> getAllBy(Long idSurat){
        Surat surat = suratService.find(idSurat);
        return db().where(q->q.getSurat().equals(surat)).toList();
    }

    public PemeriksaSurat getByIdSuratOrg(Long idSurat, Long idOrg){
        Surat surat = suratService.find(idSurat);
        MasterStrukturOrganisasi org = organisasiService.find(idOrg);
        return db().where(q->q.getOrganization().equals(org) && q.getSurat().equals(surat)).findFirst().orElse(null);
    }
}
