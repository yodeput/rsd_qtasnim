package com.qtasnim.eoffice.services;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.util.PdfModify;
import com.qtasnim.eoffice.util.QRCodeGenerateProcess;
import com.qtasnim.eoffice.util.QRCodePositionOnDocument;
import org.apache.commons.io.IOUtils;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

@Stateless
@LocalBean
public class QRGenerateService {
    private QRCodeGenerateProcess qrCodeGenerateProcess;
    private PdfModify pdfModify;

    @Inject
    private ApplicationConfig applicationConfig;

    public QRGenerateService() {
        pdfModify = new PdfModify();
    }

    @PostConstruct
    private void postConstruct() {
        qrCodeGenerateProcess = new QRCodeGenerateProcess(applicationConfig);
    }

    public byte[] getQRCode(String content) throws IOException, WriterException {
        return qrCodeGenerateProcess.generateQRCodeWithLogo(content);
    }

    public byte[] getQRCode(String content, Integer size) throws IOException, WriterException {
        return qrCodeGenerateProcess.generateQRCodeWithLogo(content, size);
    }

    public byte[] getBarcode(String content, Integer width, Integer height) throws IOException, WriterException {
        Code128Writer barcodeWriter = new Code128Writer();
        BitMatrix bitMatrix = barcodeWriter.encode(content, BarcodeFormat.CODE_128, width, height);

        BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            ImageIO.write(bufferedImage, "png", out);
            out.flush();
            return out.toByteArray();
        }
    }

    public byte[] embedQRCodeToPdf(byte[] pdf, byte[] qrcode) {
        return pdfModify.addQRCode(pdf, qrcode);
    }

    public byte[] embedQRCodeToPdf(byte[] pdf, byte[] qrcode, QRCodePositionOnDocument position) {
        return pdfModify.addQRCode(pdf, qrcode, position);
    }

    public byte[] embedQRCodeToPdf(byte[] pdf, byte[] qrcode, Point position) {
        return pdfModify.addQRCode(pdf, qrcode, position);
    }
}
