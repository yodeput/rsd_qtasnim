package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class UserTaskService extends AbstractFacade<UserTask>{


    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private MasterStrukturOrganisasiService orgService;
    @Inject
    private MasterUserService userService;

    @Override
    protected Class<UserTask> getEntityClass() {
        return UserTask.class;
    }

    public JPAJinqStream<UserTask> getAll() {return db();}

    public CriteriaQuery<UserTask> getAllQuery(String filterBy, String sortedField, boolean descending, String jenisDokumen) {
        try {
            Long entityId = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();

            // Exec Query
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<UserTask> q = cb.createQuery(UserTask.class);
            Root<UserTask> root = q.from(UserTask.class);

            q.select(root);

            List<Predicate> predicates = new ArrayList<>();


            Predicate pPendingTask = cb.equal(root.get("executionStatus"), ExecutionStatus.PENDING);
            Predicate pAssignee =
                    cb.equal(
                            root.get("assignee").get("idOrganization"),
                            entityId
                    );

            if (!Strings.isNullOrEmpty(filterBy)) {
                Predicate pkeyword =
                        cb.or(
                                cb.like(root.get("nomorDokumen"), "%"+filterBy+"%"),
                                cb.like(root.get("perihal"), "%"+filterBy+"%"),
                                cb.like(root.get("note"), "%"+filterBy+"%")
                        );
                predicates.add(pkeyword);
            }

            if (!Strings.isNullOrEmpty(jenisDokumen)) {
                Predicate pJenisDokumen = cb.like(root.get("jenisDokumen"), "%"+jenisDokumen+"%");
                predicates.add(pJenisDokumen);
            }

            // WHERE CONDITION
            predicates.add(pPendingTask);
            predicates.add(pAssignee);

            q.where(predicates.toArray(new Predicate[predicates.size()]));
            q.distinct(true);

            if (Strings.isNullOrEmpty(sortedField)) sortedField = "createdTaskDate";
            if (descending) {
                q.orderBy(cb.desc(root.get(sortedField)));
            } else {
                q.orderBy(cb.asc(root.get(sortedField)));
            }

            return q;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public UserTask findByTaskId(String taskId) {
        return db().where(q -> q.getTaskId().equals(taskId)).findFirst().orElse(null);
    }

    public void saveOrEdit(ProcessTask task) {
        UserTask userTask = this.findByTaskId(task.getTaskId());
        if (userTask != null) {
            userTask.setExecutionStatus(task.getExecutionStatus());
            userTask.setApprovedDate(task.getExecutedDate());
            this.edit(userTask);
        }
    }

    public UserTask saveOrEdit(Long userTaskId, ProcessTask task, IWorkflowEntity entity) {
        try {

            UserTask data;
            if (userTaskId == null) {
                data = new UserTask();
            } else {
                data = this.find(userTaskId);
            }

            //#region CAMUNDA DATA
            data.setNomorDokumen(entity.getNoDokumen());
            data.setTaskId(task.getTaskId());
            data.setTaskName(task.getTaskName());
            data.setExecutionStatus(task.getExecutionStatus());
            data.setCreatedTaskDate(task.getAssignedDate());
            data.setApprovedDate(task.getExecutedDate());
            if (task.getPrevious() != null) data.setNote(task.getPrevious().getNote());
            data.setAssignee(task.getAssignee());

            if (task.getPrevious() != null) data.setPengirim(task.getPrevious().getAssignee());
            //#endregion

            //#region ENTITY DATA
            data.setRelatedEntity(entity.getClassName());
            data.setRecordRefId(entity.getRelatedId());
            switch (entity.getClassName()) {
                case "com.qtasnim.eoffice.db.Surat": {
                    mappingSurat(data, (Surat) entity);
                    break;
                }
                case "com.qtasnim.eoffice.db.PermohonanDokumen": {
                    mappingPermohonanDokumen(data, (PermohonanDokumen) entity);
                    break;
                }
                case "com.qtasnim.eoffice.db.DistribusiDokumen": {
                    mappingDistribusiDokumen(data, (DistribusiDokumen) entity);
                    break;
                }
                case "com.qtasnim.eoffice.db.PenomoranManual": {
                    mappingPenomoranManual(data, (PenomoranManual) entity);
                    break;
                }
            }
            //#endregion

            if (userTaskId == null) {
                this.create(data);
            } else {
                this.edit(data);
            }
            return data;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private void mappingSurat(UserTask data, Surat entity) {
        FormDefinition formDefinition = entity.getFormDefinition();
        List<FormValue> formValues = entity.getFormValue();

        String jenisDokumen = formDefinition.getJenisDokumen().getNamaJenisDokumen();
        String perihal = getFormValueBy("perihal", formDefinition, formValues);
        String klasifikasiKeamanan = getFormValueBy("klasifikasi_keamanan", formDefinition, formValues);

        data.setJenisDokumen(jenisDokumen);
        data.setPerihal(perihal);
        data.setKlasifikasiKeamanan(klasifikasiKeamanan);
        data.setStatus(entity.getStatus());
        data.setKonseptor(entity.getKonseptor());

        /* Kondisi ketika bon nomor*/
        if(entity.getBonNomor() != null && entity.getBonNomor().booleanValue()==true){
            data.setApprovedDate(entity.getApprovedDate());
        }
    }

    private void mappingPermohonanDokumen(UserTask data, PermohonanDokumen entity) {
        data.setJenisDokumen(entity.getTipeLayanan());
        data.setPerihal(entity.getPerihal());
        data.setStatus(entity.getStatus());

        String[] konseptorParts = entity.getCreatedBy().split("\\|");
        MasterUser konseptor = userService.findUserByNIPP(konseptorParts[0].replaceAll("\\s",""));
        data.setKonseptor(konseptor.getOrganizationEntity());
        data.setPengirim(konseptor.getOrganizationEntity());
    }

    private void mappingDistribusiDokumen(UserTask data, DistribusiDokumen entity) {
        data.setJenisDokumen("Distribusi Dokumen");
        data.setPerihal(entity.getPerihal());
        data.setStatus(entity.getStatus());

        String[] konseptorParts = entity.getCreatedBy().split("\\|");
        MasterUser konseptor = userService.findUserByNIPP(konseptorParts[0].replaceAll("\\s",""));
        data.setKonseptor(konseptor.getOrganizationEntity());
        data.setPengirim(konseptor.getOrganizationEntity());
    }

    private void mappingPenomoranManual(UserTask data, PenomoranManual entity) {
        data.setJenisDokumen("Penomoran Manual");
        data.setPerihal(entity.getPerihal());
        data.setStatus(entity.getStatus());

        String[] konseptorParts = entity.getCreatedBy().split("\\|");
        MasterUser konseptor = userService.findUserByNIPP(konseptorParts[0].replaceAll("\\s",""));
        data.setKonseptor(konseptor.getOrganizationEntity());
        data.setPengirim(konseptor.getOrganizationEntity());
    }

    private String getFormValueBy(String fieldName, FormDefinition formDefinition, List<FormValue> formValues) {
        FormField field = formDefinition.getFormFields().stream()
                .filter(q -> q.getMetadata().getNama().equalsIgnoreCase(fieldName.toLowerCase()))
                .findFirst().orElse(null);
        if (field == null) return "";

        String value = formValues.stream()
                .filter(q -> q.getFormField().getId().equals(field.getId()))
                .map(FormValue::getValue)
                .findFirst().orElse("");

        return value;
    }

    public List<UserTask> execCriteriaQuery(CriteriaQuery<UserTask> criteriaQuery, int skip, int limit) {
        try {
            return getEntityManager().createQuery(criteriaQuery).setFirstResult(skip).setMaxResults(limit).getResultList();
        } catch (Exception ex) {
            return new ArrayList<>();
        }

    }

    public Long countCriteriaQuery(CriteriaQuery<UserTask> criteriaQuery) {
        try {
            return getEntityManager().createQuery(criteriaQuery).getResultStream().count();
        } catch (Exception ex) {
            return 0L;
        }

    }
    
    public Long tugasNeedAttCount(Long organizationId) throws Exception {
        Long id = Optional.ofNullable(organizationId)
                    .orElse(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());
        
        return db().where(q -> q.getAssignee().getIdOrganization().equals(id) && q.getExecutionStatus().equals(ExecutionStatus.PENDING)).count();
    }

    public void updatePerihal(Long idUserTask,String perihal) throws Exception {
        try {
            UserTask userTask = this.find(idUserTask);
            userTask.setPerihal(perihal);
            edit(userTask);
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public UserTask getUserTask(String recordRefId, String referenceTable, Long idOrganization){
        return db().where(a->a.getRelatedEntity().equals(referenceTable) && a.getRecordRefId().equals(recordRefId)
                && a.getAssignee().getIdOrganization().equals(idOrganization)).findFirst().orElse(null);
    }

    public List<UserTask> getListUserTask(String recordRefId, String referenceTable){
        return db().where(a->a.getRelatedEntity().equals(referenceTable) && a.getRecordRefId().equals(recordRefId)).collect(Collectors.toList());
    }

}
