package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.OrganisasiHistory;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;


@Stateless
@LocalBean
public class OrganisasiHistoryService extends AbstractFacade<OrganisasiHistory> {
    private static final long serialVersionUID = 1L;
    
    @Override
    protected Class<OrganisasiHistory> getEntityClass() {
        return OrganisasiHistory.class;
    }

    public OrganisasiHistory delete(Long id) {
        OrganisasiHistory model = this.find(id);
        this.remove(model);
        return model;
    }

}