package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.AppSession;
import com.qtasnim.eoffice.db.MasterRole;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.UserSession;
import com.qtasnim.eoffice.security.ForbiddenException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

/**
 * JWT Service untuk men-generate dan verifikasi token JWT.
 *
 * @author seno@qtasnim.com
 */
@Stateless
@LocalBean
public class JWTService {
    /**
     * Fungsi untuk men-generate Token JWT
     */
    public UserSession generateJWTToken(MasterUser user, ApplicationConfig applicationConfig) {
        UserSession userSession = new UserSession();

        long currentTimeMillis = System.currentTimeMillis();
        Date validFrom = new Date(currentTimeMillis);
        Date exp = new Date(System.currentTimeMillis() + applicationConfig.getTimeout());
        Date expRefresh = new Date(System.currentTimeMillis() + (7 * 24 * 60 * 60 * 1000));
        String token = getToken(user.getLoginUserName(), user.getOrganizationEntity().getRoleList().stream().map(MasterRole::getRoleName).toArray(), user.getEmail(), applicationConfig.getJwtKey(), new Date(System.currentTimeMillis() + applicationConfig.getTimeout()));
        String refreshToken = getToken(user.getLoginUserName(), user.getOrganizationEntity().getRoleList().stream().map(MasterRole::getRoleName).toArray(), user.getEmail(), applicationConfig.getJwtKey(), new Date(System.currentTimeMillis() + (7 * 24 * 60 * 60 * 1000)));

        userSession.setToken(token);
        userSession.setRefreshToken(refreshToken);
        userSession.setTokenValidFrom(validFrom);
        userSession.setTokenValidUntil(exp);
        userSession.setRefreshTokenValidUntil(expRefresh);
        userSession.setUser(user);
        userSession.setClient(null);
        userSession.setIpAddress(null);

        return userSession;
    }

    public UserSession refreshJWTToken(UserSession userSession, ApplicationConfig applicationConfig) {
        long currentTimeMillis = System.currentTimeMillis();
        Date exp = new Date(currentTimeMillis + applicationConfig.getTimeout());
        String token = getToken(userSession.getUser().getLoginUserName(), userSession.getUser().getOrganizationEntity().getRoleList().stream().map(MasterRole::getRoleName).toArray(), userSession.getUser().getEmail(), applicationConfig.getJwtKey(), new Date(System.currentTimeMillis() + applicationConfig.getTimeout()));

        userSession.setToken(token);
        userSession.setTokenValidUntil(exp);

        return userSession;
    }

    private String getToken(String subject, Object[] groups, String mail, String jwtKey, Date exp) {
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(jwtKey);
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        return Jwts.builder()
                .setSubject(subject)
                .claim("groups", groups)
                .claim("mail", mail)
                .signWith(signatureAlgorithm, signingKey)
                .setExpiration(exp)
                .compact();
    }

    public String validate(UserSession userSession, ApplicationConfig applicationConfig) {
        return Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(applicationConfig.getJwtKey()))
                .parseClaimsJws(userSession.getToken()).getBody().getSubject();
    }

    public String validateApp(AppSession appSession, ApplicationConfig applicationConfig) {
        return Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(applicationConfig.getJwtKey()))
                .parseClaimsJws(appSession.getToken()).getBody().getSubject();
    }

    public void refresh(UserSession userSession, ApplicationConfig applicationConfig) throws ForbiddenException {
        try {
            validate(userSession, applicationConfig);
        } catch (Exception e) {
            throw new ForbiddenException("Invalid token");
        }

        MasterUser user = userSession.getUser();
        long currentTimeMillis = System.currentTimeMillis();
        Date validFrom = new Date(currentTimeMillis);
        Date exp = new Date(System.currentTimeMillis() + applicationConfig.getTimeout());
        String token = Jwts.builder()
                .setSubject(user.getLoginUserName())
                .claim("groups", user.getOrganizationEntity().getRoleList().stream().map(MasterRole::getRoleName).toArray())
                .claim("mail", user.getEmail())
                .signWith(SignatureAlgorithm.HS512, applicationConfig.getJwtKey())
                .setExpiration(exp)
                .compact();

        userSession.setToken(token);
        userSession.setTokenValidFrom(validFrom);
        userSession.setTokenValidUntil(exp);
    }

    public Claims readToken(String secret, String token) {
        try
        {
            return Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(secret))
                    .parseClaimsJws(token).getBody();
        }
        catch (Exception exception)
        {
            return null;
        }
    }

    public UserSession generateJWTToken2(MasterUser user, ApplicationConfig applicationConfig) {
        UserSession userSession = new UserSession();

        long currentTimeMillis = System.currentTimeMillis();
        Date validFrom = new Date(currentTimeMillis);
        Date exp = new Date(System.currentTimeMillis() + applicationConfig.getTimeout());
        Date expRefresh = new Date(System.currentTimeMillis() + (7 * 24 * 60 * 60 * 1000));
        /*@TODO nanti di sesuaikan lagi untuk eksternal rolenya dari mana */
        Object[] groups= new Object[5];
        String token = getToken(user.getLoginUserName(), groups, user.getEmail(), applicationConfig.getJwtKey(), new Date(System.currentTimeMillis() + applicationConfig.getTimeout()));
        String refreshToken = getToken(user.getLoginUserName(),groups , user.getEmail(), applicationConfig.getJwtKey(), new Date(System.currentTimeMillis() + (7 * 24 * 60 * 60 * 1000)));

        userSession.setToken(token);
        userSession.setRefreshToken(refreshToken);
        userSession.setTokenValidFrom(validFrom);
        userSession.setTokenValidUntil(exp);
        userSession.setRefreshTokenValidUntil(expRefresh);
        userSession.setUser(user);
        userSession.setClient(null);
        userSession.setIpAddress(null);

        return userSession;
    }

    public String generateJWTApp(AppSession app, ApplicationConfig applicationConfig) {
        Object[] groups= new Object[5];
        String token = getToken(app.getAppName(), groups, null, applicationConfig.getJwtKey(),app.getEndDate());
        return token;
    }
}
