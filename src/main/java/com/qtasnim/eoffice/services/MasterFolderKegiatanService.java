package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterFolderKegiatanDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class

MasterFolderKegiatanService extends AbstractFacade<MasterFolderKegiatan> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private BerkasService berkasService;

    @Inject
    private PersonalArsipService personalArsipService;

    @Inject
    private MasterFolderKegiatanDetailService detailService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private FileService fileService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    private List<MasterFolderKegiatan>childArsip;
    private List<MasterFolderKegiatanDetail>childPersonal;

    @Override
    protected Class<MasterFolderKegiatan> getEntityClass() {
        return MasterFolderKegiatan.class;
    }

    public JPAJinqStream<MasterFolderKegiatan> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public void saveOrEdit(MasterFolderKegiatanDto dao, Long id) throws Exception {
        boolean isNew =id==null;
        MasterFolderKegiatanDetail obj = new MasterFolderKegiatanDetail();
        MasterUser usr = userSession.getUserSession().getUser();
        MasterFolderKegiatan personal = this.getFolderPersonal("Personal");
        try{
            if(isNew){
                if (detailService.validateName(dao.getFolderName(), usr.getId() ,null, dao.getIdParent())) {
                    obj.setFolderName(dao.getFolderName());
                }else{
                    throw new Exception("Terdapat folder dengan nama yang sama");
                }
                if(dao.getIdCompany()!=null) {
                    CompanyCode company = companyCodeService.find(dao.getIdCompany());
                    obj.setCompanyCode(company);
                }else{
                    obj.setCompanyCode(null);
                }
                obj.setUser(usr);
                if(dao.getIdParent() != null){
                    MasterFolderKegiatanDetail detailParent = detailService.find(dao.getIdParent());
                    obj.setParent(detailParent);
                    obj.setPersonal(personal);
                }
                else{
                    obj.setParent(null);
                    obj.setPersonal(personal);
                    /*MasterFolderKegiatan personal = this.getFolderPersonal("Personal");

                    if (detailService.validateName(dao.getFolderName(), usr.getId() ,null, dao.getIdParent())) {
                        obj.setFolderName(dao.getFolderName());
                    }else{
                        throw new Exception("Terdapat folder dengan nama yang sama");
                    }

                    if(dao.getIdCompany()!=null) {
                        CompanyCode company = companyCodeService.find(dao.getIdCompany());
                        obj.setCompanyCode(company);
                    }else{
                        obj.setCompanyCode(null);
                    }

                    obj.setUser(usr);
                    obj.setPersonal(personal);
                    detailService.create(obj);*/
                }
                detailService.create(obj);
            }else{
                obj = detailService.find(dao.getId());
                if(!obj.getFolderName().toLowerCase().equals(dao.getFolderName().toLowerCase())){
                    if (detailService.validateName(dao.getFolderName(), usr.getId() ,null, dao.getIdParent())) {
                        obj.setFolderName(dao.getFolderName());
                    }else{
                        throw new Exception("Terdapat folder dengan nama yang sama");
                    }
                }

                if(dao.getIdCompany()!=null) {
                    CompanyCode company = companyCodeService.find(dao.getIdCompany());
                    obj.setCompanyCode(company);
                }else{
                    obj.setCompanyCode(null);
                }
                if(dao.getIdParent()!=null){
                    MasterFolderKegiatanDetail detailParent = detailService.find(dao.getIdParent());
                    obj.setParent(detailParent);
                }else{
                    obj.setParent(null);
                }
                obj.setUser(usr);
                obj.setPersonal(personal);
                detailService.edit(obj);
            }
        }catch(Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public JPAJinqStream<MasterFolderKegiatan> getByParentId(Long parent,String sort,Boolean descending){
        Date n = new Date();
        JPAJinqStream<MasterFolderKegiatan> query = db().where(q->n.after(q.getStart()) && n.before(q.getEnd()));
        if(parent==null){
            query = query.where(q->q.getParent().equals(null));
        }else{
            query = query.where(q->q.getParent().getId().equals(parent));
        }

        if (!Strings.isNullOrEmpty(sort)) {
            if (descending) {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedDescendingBy(q->q.getFolderName());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedDescendingBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedDescendingBy(q -> q.getEnd());
                }
            } else {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedBy(q -> q.getFolderName());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedBy(q -> q.getEnd());
                }
            }
        }
        return query;
    }

    public void deleteFolder(Long idFolder,Boolean isPersonalFolder) throws Exception{
        try{
            childArsip = new ArrayList<>();
            childPersonal = new ArrayList<>();
            if(!isPersonalFolder){
                /*ketika Arsip*/
                MasterFolderKegiatan folder = this.find(idFolder);
                if(folder!=null){
                    addChildArsip(folder,childArsip);
                    List<Berkas> dataBerkas = checkBerkas(childArsip);
                    if(dataBerkas.isEmpty()){
                        for(MasterFolderKegiatan fk:childArsip){
                            this.remove(fk);
                        }
                    }else{
                        throw new Exception("Terdapat berkas yang berelasi dengan folder yang dipilih");
                    }
                    this.remove(folder);
                }
            }else{
                /*ketika Personal*/
                MasterFolderKegiatanDetail folder = detailService.find(idFolder);
                addChildPersonal(folder,childPersonal);
                if(folder!=null){
                    String result = deleteFolderPersonal(folder,folder);
                    if(!result.isEmpty()){
                        throw new Exception(result);
                    }
                    /*if(childPersonal.isEmpty()){
                        Kondisi dimana tidak punya child
                        PersonalArsip arsip = personalArsipService.getByIdFolderKegiatan(folder.getId());
                        if(arsip!=null){
                            fileService.delete(arsip.getDocId());
                            personalArsipService.remove(arsip);
                        }
                        detailService.remove(folder);
                    }else{
                        String result = deleteFolderPersonal(folder,folder);
                        if(!result.isEmpty()){
                            throw new Exception(result);
                        }
                    }*/
                }
            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    private boolean validateName(String folderName, Long companyId, Long id, Long parentId){
        boolean result = false;
        JPAJinqStream<MasterFolderKegiatan>query =  db().where(a->a.getCompanyCode().getId().equals(companyId));

        if(id==null){
            /*add*/
            if(parentId==null){
                List<MasterFolderKegiatan> tmp = query.where(b -> b.getParent() == null).collect(Collectors.toList());
                result = tmp.stream().noneMatch(c->c.getFolderName().toLowerCase().equals(folderName.toLowerCase()));
            }else{
                List<MasterFolderKegiatan> tmp = query.where(b -> b.getParent().getId().equals(parentId)).collect(Collectors.toList());
                result = tmp.stream().noneMatch(c->c.getFolderName().toLowerCase().equals(folderName.toLowerCase()));
            }
        }else{
            /*edit*/
            if(parentId==null){
                List<MasterFolderKegiatan> tmp = query.where(b -> b.getParent() == null && b.getId()!=id).collect(Collectors.toList());
                result = tmp.stream().noneMatch(c->c.getFolderName().toLowerCase().equals(folderName.toLowerCase()));
            }else{
                List<MasterFolderKegiatan> tmp = query.where(b -> b.getParent().getId().equals(parentId) && b.getId()!=id).collect(Collectors.toList());
                result = tmp.stream().noneMatch(c->c.getFolderName().toLowerCase().equals(folderName.toLowerCase()));
            }
        }
        return result;
    }


    public MasterFolderKegiatan getFolderPersonal(String personal){
        return db().where(a->a.getFolderName().toLowerCase().equals(personal.toLowerCase()) && a.getParent()==null).findOne().orElse(null);
    }

    /*Fungsi Rekursif untuk getAllChild*/
    private void addChildArsip(MasterFolderKegiatan parent,List<MasterFolderKegiatan> childArsip){
        List<MasterFolderKegiatan>childs = this.getByParentId(parent.getId(),"",false).toList();
        if(!childs.isEmpty()) {
            for (MasterFolderKegiatan c : childs) {
                childArsip.add(c);
                addChildArsip(c, childArsip);
            }
        }
    }

    private void addChildPersonal(MasterFolderKegiatanDetail parent,List<MasterFolderKegiatanDetail> childPersonal){
        List<MasterFolderKegiatanDetail>childs = detailService.getByParentId(parent.getId(),"",false, null).toList();
        if(!childs.isEmpty()) {
            for (MasterFolderKegiatanDetail c : childs) {
                childPersonal.add(c);
                addChildPersonal(c, childPersonal);
            }
        }
    }

    private List<Berkas> checkBerkas(List<MasterFolderKegiatan> folder){
        List<Berkas> berkasList = new ArrayList<>();
        for(MasterFolderKegiatan fk:folder){
            Berkas berkas = berkasService.getByIdFolderKegiatan(fk.getId());
            if(berkas!=null){
                berkasList.add(berkas);
            }
        }

        return berkasList;
    }

    public GlobalAttachment deleteFile(Long id){
        GlobalAttachment global = globalAttachmentService.find(id);

        if (global.getReferenceTable().toLowerCase().equals("t_personal_arsip")) {
            PersonalArsip personal = personalArsipService.find(global.getReferenceId());
            if(personal!=null) {
                personalArsipService.remove(personal);
            }

            fileService.delete(global.getDocId());
            globalAttachmentService.remove(global);

            return global;
        } else {
            throw new InternalServerErrorException(String.format("Unable find file %s from table t_personal_arsip", id));
        }
    }

    private String deleteFolderPersonal(MasterFolderKegiatanDetail obj,MasterFolderKegiatanDetail limit){
        String message = "";
        List<MasterFolderKegiatanDetail>childs=detailService.getChilds(obj.getId());
        if(childs.isEmpty()){
            PersonalArsip arsip = personalArsipService.getByIdFolderKegiatan(obj.getId());
            if(arsip!=null){
                /*List<GlobalAttachment> fileAttachment = globalAttachmentService.getAttachmentDataByRef("t_personal_arsip",arsip.getId()).collect(Collectors.toList());
                if(fileAttachment!=null && !fileAttachment.isEmpty()) {
                    for(GlobalAttachment att:fileAttachment) {
                        fileService.delete(att.getDocId());
                        globalAttachmentService.remove(att);
                    }
                }
                personalArsipService.remove(arsip);*/
                message="Folder sudah digunakan.";
            }
            else
                detailService.remove(obj);

        }else {
            message="Folder sudah digunakan.";
            /*for(int i=0;i<childs.size();i++){
                deleteFolderPersonal(childs.get(i),limit);
            }
            PersonalArsip arsip = personalArsipService.getByIdFolderKegiatan(obj.getParent().getId());
            if(arsip!=null){
                List<GlobalAttachment> fileAttachment = globalAttachmentService.getAttachmentDataByRef("t_personal_arsip",arsip.getId()).collect(Collectors.toList());
                if(fileAttachment!=null && !fileAttachment.isEmpty()) {
                    for(GlobalAttachment att:fileAttachment) {
                        fileService.delete(att.getDocId());
                        globalAttachmentService.remove(att);
                    }
                }
                personalArsipService.remove(arsip);
            }
            System.out.println("Dihapus di else"+obj.getId());
            detailService.remove(obj);*/
        }
        return message;
    }

    private MasterFolderKegiatan getParent (Long idParent){
        return db().where(a->a.getId().equals(idParent)).findFirst().orElse(null);
    }
}
