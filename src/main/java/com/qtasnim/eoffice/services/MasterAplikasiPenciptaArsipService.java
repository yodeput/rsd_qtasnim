package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterAplikasiPenciptaArsip;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class MasterAplikasiPenciptaArsipService extends AbstractFacade<MasterAplikasiPenciptaArsip> {
    private static final long serialVersionUID = 1L;

    @Override
    protected Class<MasterAplikasiPenciptaArsip> getEntityClass() {
        return MasterAplikasiPenciptaArsip.class;
    }

    public JPAJinqStream<MasterAplikasiPenciptaArsip> getAll() {
        return db();
    }

}