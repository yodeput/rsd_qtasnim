package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class NgSuratInteractionService extends AbstractFacade<SuratInteraction> {
    @Inject
    private SuratService suratService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private AgendaService agendaService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Override
    protected Class<SuratInteraction> getEntityClass() {
        return SuratInteraction.class;
    }


    public SuratInteraction setStared(Long idSurat, Boolean stared){
        Surat surat = suratService.find(idSurat);
        SuratInteraction model = getBySuratIdOrCreate(surat, userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());
        model.setIsStarred(stared);
        if(stared.booleanValue()){
            model.setUser(userSession.getUserSession().getUser());
        }else{
            model.setUser(null);
        }
        edit(model);
        return model;
    }

    public SuratInteraction setFinished(Long idSurat, Boolean finished){
        Surat surat = suratService.find(idSurat);
        SuratInteraction model = getBySuratIdOrCreate(surat, userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());
        DateUtil dateUtil = new DateUtil();
        model.setIsFinished(finished);
        model.setFinishedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
        edit(model);
        return model;
    }

    public SuratInteraction setImportant(Long idSurat, Boolean isImportant){
        Surat surat = suratService.find(idSurat);
        SuratInteraction model = getBySuratIdOrCreate(surat, userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());
        model.setIsImportant(isImportant);
        edit(model);
        return model;
    }

    public SuratInteraction setIsRead(Long idSurat){
        Surat surat = suratService.find(idSurat);
        SuratInteraction model = null;

        if (!surat.getKonseptor().getIdOrganization().equals(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization())) {
            model = getBySuratIdOrCreate(surat, userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());

            if (!model.getIsRead()) {
                model.setIsRead(true);
                model.setReadDate(new Date());
                edit(model);
            }
        }

        return model;
    }

    public SuratInteraction getBySuratIdOrCreate(Surat surat, Long idOrganization) {
        Long suratId = surat.getId();
        SuratInteraction suratInteraction = db()
                .where(t -> t.getSurat().getId().equals(suratId) && t.getOrganization().getIdOrganization().equals(idOrganization))
                .findFirst()
                .orElse(null);

        if (suratInteraction == null) {
            suratInteraction = new SuratInteraction();
            suratInteraction.setSurat(surat);
            suratInteraction.setCompanyCode(userSession.getUserSession().getUser().getCompanyCode());
            suratInteraction.setOrganization(userSession.getUserSession().getUser().getOrganizationEntity());

            suratService.create(surat);
        }

        return suratInteraction;
    }

    public SuratInteraction getBySuratIdOrCreateByOrg(Surat surat, Long idOrganization) {
        Long suratId = surat.getId();
        SuratInteraction suratInteraction = db()
                .where(t -> t.getSurat().getId().equals(suratId) && t.getOrganization().getIdOrganization().equals(idOrganization))
                .findFirst()
                .orElse(null);

        if (suratInteraction == null) {
            suratInteraction = new SuratInteraction();
            suratInteraction.setSurat(surat);

            MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(idOrganization);
            if(org.getUser()!=null) {
                suratInteraction.setCompanyCode(org.getUser().getCompanyCode());
            }
            suratInteraction.setOrganization(org);

            this.create(suratInteraction);
        }

        return suratInteraction;
    }
}
