package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterKontakHelpdesk;
import com.qtasnim.eoffice.db.MasterKontakHelpdesk;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterKontakHelpdeskDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterKontakHelpdeskService extends AbstractFacade<MasterKontakHelpdesk> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterKontakHelpdesk> getEntityClass() {
        return MasterKontakHelpdesk.class;
    }

    public JPAJinqStream<MasterKontakHelpdesk> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public void saveOrEdit(Long id, MasterKontakHelpdeskDto dao){
        MasterKontakHelpdesk model = new MasterKontakHelpdesk();
        DateUtil dateUtil = new DateUtil();
        boolean isNew = id==null;
        if(isNew) {
            model.setToka(dao.getToka());
            model.setEmail(dao.getEmail());
            if(dao.getCompanyId()!=null) {
                CompanyCode company = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(company);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStart()));
            if(dao.getEnd()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEnd()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.create(model);
        } else {
            model = this.find(id);
            model.setToka(dao.getToka());
            model.setEmail(dao.getEmail());
            if(dao.getCompanyId()!=null) {
                CompanyCode company = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(company);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStart()));
            if(dao.getEnd()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEnd()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }
    }

    public MasterKontakHelpdesk deleteData(Long id){
        MasterKontakHelpdesk model = this.find(id);
        this.remove(model);

        return model;
    }
}