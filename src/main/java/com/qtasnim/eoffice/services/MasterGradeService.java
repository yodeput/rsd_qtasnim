package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterGrade;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterGradeDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Stateless
@LocalBean
public class MasterGradeService extends AbstractFacade<MasterGrade> {
    private static final long serialVersionUID = 1L;
    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterGrade> getEntityClass() {
        return MasterGrade.class;
    }

    public JPAJinqStream<MasterGrade> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }


    public JPAJinqStream<MasterGrade> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name));
    }


    public MasterGrade save(String id, MasterGradeDto dao) {
        MasterGrade model = new MasterGrade();
        if (id == null) {
            this.create(data(model, dao));
        } else {
            model = this.find(id);
            this.edit(data(model, dao));
        }
        return model;
    }

    public MasterGrade data(MasterGrade masterGrade, MasterGradeDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterGrade model = masterGrade;
        model.setId(dao.getId());
        model.setNama(dao.getNama());
        if (dao.getCompanyId() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }
        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        return model;
    }

    public MasterGrade delete(String id) {
        MasterGrade model = this.find(id);
        this.remove(model);
        return model;
    }

    public List<MasterGrade> getMasterGradeListByNames(List<String> names) {
        if (names.size() == 0) {
            return new ArrayList<>();
        }

        return db().where(t -> names.contains(t.getId())).toList();
    }
}