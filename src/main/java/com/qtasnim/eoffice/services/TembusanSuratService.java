package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.PenerimaSurat;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.db.TembusanSurat;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class TembusanSuratService extends AbstractFacade<TembusanSurat>{

    @Override
    protected Class<TembusanSurat> getEntityClass() {
        return TembusanSurat.class;
    }

    @Inject
    private SuratService suratService;

    public List<TembusanSurat> getAllBy(Long idSurat){
        Surat surat = suratService.find(idSurat);
        return db().where(q->q.getSurat().equals(surat)).toList();
    }

    public TembusanSurat findTembusan(Long idOrganization,Long idSurat){
        return db().where(q->q.getOrganization().getIdOrganization().equals(idOrganization) && q.getSurat().getId().equals(idSurat)).findFirst().orElse(null);
    }
}
