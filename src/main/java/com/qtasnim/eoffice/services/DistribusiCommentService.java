package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.DistribusiComment;
import com.qtasnim.eoffice.db.DistribusiDokumen;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.DistribusiCommentDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;


@Stateless
@LocalBean
public class DistribusiCommentService extends AbstractFacade<DistribusiComment> {
    private static final long serialVersionUID = 1L;

    @Inject
    private DistribusiDokumenService distribusiService;

    @Inject
    @ISessionContext
    private Session session;

    @Override
    protected Class<DistribusiComment> getEntityClass() {
        return DistribusiComment.class;
    }

    public JPAJinqStream<DistribusiComment> getAll(Long idDistribusi) {
        return db().where(q -> !q.getIsDeleted().booleanValue() && q.getDistribusi().getId().equals(idDistribusi));
    }

    public void saveOrEdit(Long id,DistribusiCommentDto dao) throws Exception {
        boolean isNew = id == null ? true : false;
        DistribusiComment model = new DistribusiComment();
        try {
            if (isNew) {
                model.setIsDeleted(false);
                model.setKomentar(dao.getKomentar());
                if (dao.getIdDistribusi() != null) {
                    DistribusiDokumen distribusi = distribusiService.find(dao.getIdDistribusi());
                    model.setDistribusi(distribusi);
                }
                MasterUser user = session.getUserSession().getUser();
                model.setUser(user);
                create(model);
            } else {
                model = this.find(id);
                model.setKomentar(dao.getKomentar());
                edit(model);
            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public void removeComment(Long id) throws Exception {
        try {
            DistribusiComment obj = this.find(id);
            obj.setIsDeleted(true);
            edit(obj);
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
}