package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterPerusahaanPengiriman;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterPerusahaanPengirimanDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterPerusahaanPengirimanService extends AbstractFacade<MasterPerusahaanPengiriman> {
    private static final long serialVersionUID = 1L;

    @Inject
    CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterPerusahaanPengiriman> getEntityClass() {
        return MasterPerusahaanPengiriman.class;
    }

    public JPAJinqStream<MasterPerusahaanPengiriman> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterPerusahaanPengiriman> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name));
    }
     public MasterPerusahaanPengiriman save(Long id, MasterPerusahaanPengirimanDto dao){
         MasterPerusahaanPengiriman model = new MasterPerusahaanPengiriman();
         DateUtil dateUtil = new DateUtil();

         if(id==null){
             model.setNama(dao.getNama());

             if(dao.getCompanyId()!=null){
                 CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                 model.setCompanyCode(cCode);
             } else {
                 model.setCompanyCode(null);
             }
             model.setCreatedBy(dao.getCreatedBy());
             model.setCreatedDate(new Date());
             model.setModifiedBy("-");
             model.setModifiedDate(new Date());
             model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
             if(dao.getEndDate()!=null){
                 model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
             } else {
                 model.setEnd(dateUtil.getDefValue());
             }

             this.create(model);
         } else {
             model = this.find(id);
             model.setNama(dao.getNama());

             if(dao.getCompanyId()!=null){
                 CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                 model.setCompanyCode(cCode);
             } else {
                  model.setCompanyCode(null);
             }
             model.setModifiedBy(dao.getModifiedBy());
             model.setModifiedDate(new Date());
             model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

             if(dao.getEndDate()!=null){
                 model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
             } else {
                 model.setEnd(dateUtil.getDefValue());
             }

             this.edit(model);
         }
        return model;
     }

    public MasterPerusahaanPengiriman delete(Long id){
        MasterPerusahaanPengiriman model = this.find(id);
        this.remove(model);
        return model;
    }

    public MasterPerusahaanPengiriman findByNama(String value) {
        String nama;
        if (Strings.isNullOrEmpty(value)) {
            nama = "Indonesia";
        } else {
            nama = value;
        }
        
        return db().where(q -> q.getNama().equals(nama)).findFirst().orElse(null);
    }
}