package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.ConvertedPdfDto;
import com.qtasnim.eoffice.ws.dto.DetailConvertedPdfDto;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.criteria.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;


@Stateless
@LocalBean
public class RiwayatService extends AbstractFacade<Surat>{


    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private MasterStrukturOrganisasiService orgService;
    @Inject
    private SuratService suratService;
    @Inject
    private GlobalAttachmentService globalAttachmentService;
    @Inject
    private FileService fileService;

    @Override
    protected Class<Surat> getEntityClass() {
        return Surat.class;
    }

    public JPAJinqStream<Surat> getAll() {return db();}


    public CriteriaQuery<Surat> getAllQuery(Long idOrganisasi, String filterBy, String sortedField, boolean descending, String jenisDokumen) {
        try {
            Long entityId = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();

            // Exec Query
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Surat> q = cb.createQuery(Surat.class);
            Root<Surat> root = q.from(Surat.class);

            Join<Surat, PemeriksaSurat> pemeriksa = root.join("pemeriksaSurat", JoinType.LEFT);
            Join<Surat, PenandatanganSurat> penandatangan = root.join("penandatanganSurat", JoinType.LEFT);
            Join<Surat, PenerimaSurat> penerima = root.join("penerimaSurat", JoinType.LEFT);
            Join<Surat, TembusanSurat> tembusan = root.join("tembusanSurat", JoinType.LEFT);
            Join<Surat, FormValue> formValue = root.join("formValue", JoinType.LEFT);

            q.select(root);

            List<Predicate> predicates = new ArrayList<>();


            Predicate pApproved = cb.equal(root.get("status"), "APPROVED");
            Predicate pAssignee =
                    cb.or(
                            cb.equal(root.get("konseptor").get("idOrganization"), idOrganisasi),
                            cb.or(
                                pemeriksa.get("organization").get("idOrganization").in(idOrganisasi),
                                cb.or(
                                    penandatangan.get("organization").get("idOrganization").in(idOrganisasi),
                                    cb.or(
                                            penerima.get("organization").get("idOrganization").in(idOrganisasi),
                                            tembusan.get("organization").get("idOrganization").in(idOrganisasi)
                                    )
                                )
                            )
                    );

            if (!Strings.isNullOrEmpty(filterBy)) {
                Predicate pkeyword = cb.like(formValue.get("value"), "%"+filterBy+"%");
                predicates.add(pkeyword);
            }

            if (!Strings.isNullOrEmpty(jenisDokumen)) {
                Predicate pJenisDokumen = cb.like(root.get("formDefinition").get("jenisDokumen").get("namaJenisDokumen"), "%"+jenisDokumen+"%");
                predicates.add(pJenisDokumen);
            }

            // WHERE CONDITION
            predicates.add(pApproved);
            predicates.add(pAssignee);

            q.where(predicates.toArray(new Predicate[predicates.size()]));
            q.distinct(true);

            if (Strings.isNullOrEmpty(sortedField)) sortedField = "approvedDate";
            if (descending) {
                q.orderBy(cb.desc(root.get(sortedField)));
            } else {
                q.orderBy(cb.asc(root.get(sortedField)));
            }

            return q;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private String getFormValueBy(String fieldName, FormDefinition formDefinition, List<FormValue> formValues) {
        FormField field = formDefinition.getFormFields().stream()
                .filter(q -> q.getMetadata().getNama().equalsIgnoreCase(fieldName.toLowerCase()))
                .findFirst().orElse(null);
        if (field == null) return "";

        String value = formValues.stream()
                .filter(q -> q.getFormField().getId().equals(field.getId()))
                .map(FormValue::getValue)
                .findFirst().orElse("");

        return value;
    }

    public List<Surat> execCriteriaQuery(CriteriaQuery<Surat> criteriaQuery, int skip, int limit) {
        try {
            return getEntityManager().createQuery(criteriaQuery).setFirstResult(skip).setMaxResults(limit).getResultList();
        } catch (Exception ex) {
            return new ArrayList<>();
        }

    }

    public Long countCriteriaQuery(CriteriaQuery<Surat> criteriaQuery) {
        try {
            return getEntityManager().createQuery(criteriaQuery).getResultStream().count();
        } catch (Exception ex) {
            return 0L;
        }

    }

    public List<ConvertedPdfDto> convertRiwayatDokumen(Long idSurat) throws IOException {

        List<ConvertedPdfDto> convertedPdf = new ArrayList<>();
        List<DetailConvertedPdfDto> details;

        /**
         * @TODO: ready to delete.
         * it is just dummy data
         */
        if (idSurat == null) {
            details = new ArrayList<>();

            ByteArrayOutputStream out;
            try (final PDDocument document = PDDocument.load(new File("C:\\a89e86de-2575-4e51-936a-441b448fffaa.pdf"))) {   // a89e86de-2575-4e51-936a-441b448fffaa.pdf
//                try (final PDDocument document = PDDocument.load(file)){
                PDFRenderer pdfRenderer = new PDFRenderer(document);

                for (int page = 0; page < document.getNumberOfPages(); page++) {
                    out = new ByteArrayOutputStream();
                    BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
                    ImageIOUtil.writeImage(bim, "PNG", out);

                    byte[] bytes = out.toByteArray();
                    String encodedImg = Base64.getEncoder().encodeToString(bytes);

                    DetailConvertedPdfDto detail = new DetailConvertedPdfDto("sampeu", page + 1, "data:image/png;base64," + encodedImg);
                    details.add(detail);
                }
            }

            convertedPdf.add(new ConvertedPdfDto("dokumen", "sampeu", details));

            //#region Lampiran
            details = new ArrayList<>();
            out = new ByteArrayOutputStream();
            try (final PDDocument document = PDDocument.load(new File("C:\\sample.pdf"))) {   // a89e86de-2575-4e51-936a-441b448fffaa.pdf
//                try (final PDDocument document = PDDocument.load(file)){
                PDFRenderer pdfRenderer = new PDFRenderer(document);

                for (int page = 0; page < document.getNumberOfPages(); page++) {
                    BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
                    ImageIOUtil.writeImage(bim, "PNG", out);

                    byte[] bytes = out.toByteArray();
                    String encodedImg = Base64.getEncoder().encodeToString(bytes);

                    DetailConvertedPdfDto detail = new DetailConvertedPdfDto("lampiran sample", page + 1, "data:image/png;base64," + encodedImg);
                    details.add(detail);
                }
            }

            out = new ByteArrayOutputStream();
            try (final PDDocument document = PDDocument.load(new File("C:\\sample2.pdf"))) {   // a89e86de-2575-4e51-936a-441b448fffaa.pdf
//                try (final PDDocument document = PDDocument.load(file)){
                PDFRenderer pdfRenderer = new PDFRenderer(document);

                for (int page = 0; page < document.getNumberOfPages(); page++) {
                    BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
                    ImageIOUtil.writeImage(bim, "PNG", out);

                    byte[] bytes = out.toByteArray();
                    String encodedImg = Base64.getEncoder().encodeToString(bytes);

                    DetailConvertedPdfDto detail = new DetailConvertedPdfDto("lampiran sample 2", page + 1, "data:image/png;base64," + encodedImg);
                    details.add(detail);
                }
            }
            convertedPdf.add(new ConvertedPdfDto("lampiran", details));
            //#endregion

            return convertedPdf;
        }

        //#region dokumen
        GlobalAttachment doc = globalAttachmentService.getPublished("t_surat", idSurat).findFirst().orElse(null);
        String noIdDoc = doc != null ? doc.getNumber() : "";

        details = getConvertedRiwayatDokumenByType(idSurat, "dokumen");
        convertedPdf.add(new ConvertedPdfDto("dokumen", noIdDoc, details));
        //#endregion

        //#region lampiran
        details = getConvertedRiwayatDokumenByType(idSurat, "lampiran");
        convertedPdf.add(new ConvertedPdfDto("lampiran", details));
        //#endregion

        return convertedPdf;

    }

    public List<DetailConvertedPdfDto> getConvertedRiwayatDokumenByType(Long idSurat, String type) throws IOException {

        List<DetailConvertedPdfDto> details = new ArrayList<>();

        List<GlobalAttachment> attachments = new ArrayList<>();
        switch (type) {
            case "dokumen": {
                attachments = globalAttachmentService.getPublished("t_surat", idSurat).collect(Collectors.toList());
                break;
            }
            case "lampiran": {
                attachments = globalAttachmentService.getAttachmentDataByRef("t_surat", idSurat).collect(Collectors.toList());
                break;
            }
        }

        if (!attachments.isEmpty()) {

            for (GlobalAttachment attachment : attachments) {

                byte[] file = fileService.download(attachment.getDocId());
                ByteArrayOutputStream out;

                switch (attachment.getMimeType()) {

                    case "application/pdf":
                    case "application/vnd.openxmlformats-officedocument.wordprocessingml.document": {
                        //#region PDF
                        try (final PDDocument document = PDDocument.load(file)){
                            PDFRenderer pdfRenderer = new PDFRenderer(document);

                            int pages = document.getNumberOfPages();
                            for (int page = 0; page < pages; page++) {
                                BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
                                out = new ByteArrayOutputStream();
                                ImageIOUtil.writeImage(bim, "PNG", out);

                                byte[] bytes = out.toByteArray();
                                String encodedImg = Base64.getEncoder().encodeToString(bytes);

                                DetailConvertedPdfDto detail = new DetailConvertedPdfDto(attachment.getDocName(), page + 1, "data:image/png;base64," + encodedImg);
                                details.add(detail);
                            }
                        }
                        //#endregion
                        break;
                    }

                    case "image/jpg":
                    case "image/jpeg":
                    case "image/bmp":
                    case "image/png":
                    case "image/tiff": {
                        //#region IMAGE
                        String encodedImg = Base64.getEncoder().encodeToString(file);

                        DetailConvertedPdfDto detail = new DetailConvertedPdfDto(attachment.getDocName(), null, "data:image/png;base64," + encodedImg);
                        details.add(detail);
                        //#endregion
                        break;
                    }

                }
            }
        }

        return details;
    }

}
