package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.cmis.CMISDocument;
import com.qtasnim.eoffice.cmis.ICMISProvider;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.JenisDokumenDocLib;
import com.qtasnim.eoffice.db.MasterJenisDokumen;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.JenisDokumenDocLibDto;
import org.apache.commons.io.IOUtils;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.InputStream;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Stateless
@LocalBean
public class JenisDokumenDocLibService extends AbstractFacade<JenisDokumenDocLib> {
    @Inject
    private MasterJenisDokumenService masterJenisDokumenService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private ICMISProvider icmisProvider;

    @Inject
    private FileService fileService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Override
    protected Class<JenisDokumenDocLib> getEntityClass() {
        return JenisDokumenDocLib.class;
    }

    public JenisDokumenDocLibDto findByDocId(String docId){
        return db().where(a->a.getDocId().equals(docId)).map(JenisDokumenDocLibDto::new).findFirst().orElse(null);
    }

    public String getDocIdByJenisDokumenId(Long idJenisDokumen) {
        return db().where(a -> a.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen)).findFirst().map(JenisDokumenDocLib::getDocId).orElse(null);
    }

    public JenisDokumenDocLib getByJenisDokumenId(Long idJenisDokumen) {
        return db().where(a -> a.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen)).findFirst().orElse(null);
    }

    public JPAJinqStream<JenisDokumenDocLib> getHistory(Long idJenisDokumen) {
        return db()
                .where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen))
                .sortedDescendingBy(JenisDokumenDocLib::getCreatedDate);
    }

    public JPAJinqStream<JenisDokumenDocLib> getPublished(Long idJenisDokumen) {
        Date now = new Date();

        return db()
                .where(t -> t.getIsActive()
                        && t.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen)
                        && (now.after(t.getStart()) && now.before(t.getEnd())))
                .sortedDescendingBy(JenisDokumenDocLib::getVersion);
    }

    public JenisDokumenDocLib getLatest(Long idJenisDokumen) {
        return db()
                .where(t ->
                        t.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen)
                )
                .sortedDescendingBy(JenisDokumenDocLib::getVersion)
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public JenisDokumenDocLib findFirst(String docId) {
        return db().where(t -> t.getDocId().equals(docId)).findFirst().orElse(null);
    }

    public JenisDokumenDocLib saveOrSubmit(Long idJenisDokumen, JenisDokumenDocLibDto dto, double nextVersion) {
        return saveOrSubmit(idJenisDokumen, dto, nextVersion, null);
    }

    public JenisDokumenDocLib saveOrSubmit(Long idJenisDokumen, JenisDokumenDocLibDto dto, double nextVersion, Date activationDate) {
        boolean published = nextVersion % 1 == 0;

        if (published) {
            JenisDokumenDocLib activated = getLatest(idJenisDokumen);

            if (activated != null) {
                activated.setIsActive(false);
                edit(activated);
            }
        }

        JenisDokumenDocLib newForm = new JenisDokumenDocLib();
        MasterJenisDokumen masterJenisDokumen = masterJenisDokumenService.find(idJenisDokumen);
        DateUtil dateUtil = new DateUtil();
        CompanyCode companyCode = companyCodeService.find(dto.getIdCompany());

        newForm.setMimeType(dto.getMimeType());
        newForm.setDocGeneratedName(dto.getDocGeneratedName());
        newForm.setDocName(dto.getDocName());
        newForm.setCompanyCode(companyCode);
        newForm.setDocId(dto.getDocId());
        newForm.setAuthor(userSession.getUserSession().getUser());
        newForm.setSize(dto.getSize());
        newForm.setNumber(dto.getNumber());
        newForm.setJenisDokumen(masterJenisDokumen);
        newForm.setVersion(nextVersion);
        newForm.setCmisVersion(dto.getCmisVersion());
        newForm.setStart(Optional.ofNullable(activationDate).orElse(new Date()));
        newForm.setEnd(dateUtil.getDefValue());
        newForm.setIsActive(published);
        create(newForm);

        return newForm;
    }

    public JenisDokumenDocLib upload(Long idJenisDokumen, JenisDokumenDocLibDto dto, double nextVersion, InputStream stream, String fileName, String mediaType) throws Exception {
        return upload(idJenisDokumen, dto, nextVersion, null, stream, fileName, mediaType);
    }

    public JenisDokumenDocLib upload(Long idJenisDokumen, JenisDokumenDocLibDto dto, double nextVersion, Date activationDate, InputStream stream, String fileName, String mediaType) throws Exception {
        try {
            if (stream != null && fileName != null && mediaType != null) {
                try {
                    icmisProvider.openConnection();
                    String originalFileName = fileName; //body.getFormDataContentDisposition().getFileName();
                    String fileType = FileUtility.GetFileExtension(originalFileName);
                    String generatedFileName = UUID.randomUUID().toString() + fileType;
                    byte[] content = IOUtils.toByteArray(stream);
                    String docId = fileService.upload(icmisProvider, generatedFileName, content);
                    CMISDocument cmisDocument = fileService.getFile(docId);

                    dto.setMimeType(mediaType); //body.getMediaType().toString()
                    dto.setDocGeneratedName(generatedFileName);
                    dto.setDocName(originalFileName);
                    dto.setDocId(docId);
                    dto.setCmisVersion(cmisDocument.getVersion());
                    dto.setSize((long) content.length);
                    dto.setNumber("-");
                } catch (Exception e) {
                    throw new InternalServerErrorException(e.getMessage(), e);
                } finally {
                    icmisProvider.closeConnection();
                }
            }

            return saveOrSubmit(idJenisDokumen, dto, nextVersion, activationDate);
        } catch (Exception ex) {
            throw new InternalServerErrorException(ex.getMessage(), ex);
        }
    }

    public JinqStream<JenisDokumenDocLib> findByJenisDokumenId(Long idJenisDokumen) {
        return db().where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen));
    }
}
