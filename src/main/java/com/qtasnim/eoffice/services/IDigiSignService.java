package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.bssn.BeforeSigningEvent;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;

import java.util.Map;
import java.util.function.Consumer;

public interface IDigiSignService {
    byte[] sign(MasterStrukturOrganisasi strukturOrganisasi, byte[] pdfByte);

    void validate(MasterStrukturOrganisasi strukturOrganisasi, String passphrase);

    IDigiSignService onSigning(Consumer<Map<String, String>> onSigning);

    IDigiSignService onBeforeSigning(Consumer<BeforeSigningEvent> onBeforeSigning);

    Boolean isRequirePassphrase();
}
