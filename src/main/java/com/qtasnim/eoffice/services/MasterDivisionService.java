package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterDivision;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterDivisionDto;
import org.apache.commons.lang3.StringUtils;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Stateless
@LocalBean
public class MasterDivisionService extends AbstractFacade<MasterDivision> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterDivision> getEntityClass() {
        return MasterDivision.class;
    }

    public JPAJinqStream<MasterDivision> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterDivision> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name));
    }

    public MasterDivision save(Long id, MasterDivisionDto dao){
        MasterDivision model = new MasterDivision();
        DateUtil dateUtil = new DateUtil();

        if(id==null){
            model.setNama(dao.getNama());
            model.setKode(dao.getKode());
            model.setKodeHris(dao.getKodeHris());
            model.setLevel(dao.getLevel());
            model.setIsActive(dao.getIsActive());

            if(dao.getIdParent()!=null){
                MasterDivision parent = this.find(dao.getIdParent());
                model.setIdParent(parent);
            } else {
                model.setIdParent(null);
            }

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setCreatedBy(dao.getCreatedBy());
            model.setCreatedDate(new Date());
            model.setModifiedBy("-");
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.create(model);

        }else {
            model = this.find(id);

            model.setNama(dao.getNama());
            model.setKode(dao.getKode());
            model.setKodeHris(dao.getKodeHris());
            model.setLevel(dao.getLevel());
            model.setIsActive(dao.getIsActive());

            if(dao.getIdParent()!=null){
                MasterDivision parent = this.find(dao.getIdParent());
                model.setIdParent(parent);
            } else {
                model.setIdParent(null);
            }

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }


       return model;
    }

    public MasterDivision delete(Long id){
        MasterDivision model = this.find(id);
        this.remove(model);
        return model;
    }

    public JPAJinqStream<MasterDivision> getByParent(Long parent,String sort,Boolean descending){
//        List<MasterDivisionDto> result = new ArrayList<>();
        Date n = new Date();
        JPAJinqStream<MasterDivision> query = db().where(q->n.after(q.getStart()) && n.before(q.getEnd()));
//        List<MasterDivisionDto> data = query.sorted(Comparator.comparing(MasterDivision::getId))
//                .map(MasterDivisionDto::new).collect(Collectors.toList());
        if(parent==null){
//            result = data.stream().filter(q->q.getIdParent()==null).collect(Collectors.toList());
            query = query.where(q->q.getIdParent().equals(null));
        }else{
//            data = data.stream().filter(q->q.getIdParent()!=null).collect(Collectors.toList());
//            result = data.stream().filter(q->q.getIdParent()==parent).collect(Collectors.toList());
            query = query.where(q->q.getIdParent().getId().equals(parent));
        }

        if (!Strings.isNullOrEmpty(sort)) {
            if (descending) {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedDescendingBy(q->q.getNama());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedDescendingBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedDescendingBy(q -> q.getEnd());
                }
            } else {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedBy(q -> q.getNama());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedBy(q -> q.getEnd());
                }
            }
        }

        return query;
    }

    public List<MasterDivisionDto> getListByParent(Long parent, String sort, Boolean descending) {
        return getByParent(parent, sort, descending).map(MasterDivisionDto::new).collect(Collectors.toList());
    }

    public List<MasterDivisionDto> getListByParent(Long parent) {
        return getByParent(parent, null, null).map(MasterDivisionDto::new).collect(Collectors.toList());
    }
}