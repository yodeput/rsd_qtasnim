package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterKlasifikasiKeamanan;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterKlasifikasiKeamananDto;

import org.jinq.jpa.JPAJinqStream;

import java.util.Date;

@Stateless
@LocalBean
public class MasterKlasifikasiKeamananService extends AbstractFacade<MasterKlasifikasiKeamanan> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterKlasifikasiKeamanan> getEntityClass() {
        return MasterKlasifikasiKeamanan.class;
    }

    public JPAJinqStream<MasterKlasifikasiKeamanan> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterKlasifikasiKeamanan>getFiltered(String kode,String nama,Boolean isActive){
        JPAJinqStream<MasterKlasifikasiKeamanan> query = db();

        if(!Strings.isNullOrEmpty(kode)){
            query = query.where(q ->q.getKodeKlasifikasiKeamanan().toLowerCase().contains(kode.toLowerCase()));
        }

        if(!Strings.isNullOrEmpty(nama)){
            query = query.where(q ->q.getNamaKlasifikasiKeamanan().toLowerCase().contains(nama.toLowerCase()));
        }

        if (isActive!=null) {
            if(isActive) {
                query = query.where(q -> q.getIsActive());
            }else{
                query = query.where(q -> !q.getIsActive());
            }
        }

        return query;
    }

    public void saveOrEdit(Long id,MasterKlasifikasiKeamananDto dao){
        boolean isNew = id==null;
        MasterKlasifikasiKeamanan model = new MasterKlasifikasiKeamanan();
        DateUtil dateUtil = new DateUtil();

        if(isNew){
            model.setKodeKlasifikasiKeamanan(dao.getKodeKlasifikasiKeamanan());
            model.setNamaKlasifikasiKeamanan(dao.getNamaKlasifikasiKeamanan());
            model.setIsActive(dao.getIsActive());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
//            model.setCreatedBy(dao.getCreatedBy());
//            model.setCreatedDate(new Date());
//            model.setModifiedBy("-");
//            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.create(model);
        }else{
            model = this.find(id);
            model.setKodeKlasifikasiKeamanan(dao.getKodeKlasifikasiKeamanan());
            model.setNamaKlasifikasiKeamanan(dao.getNamaKlasifikasiKeamanan());
            model.setIsActive(dao.getIsActive());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
//            model.setModifiedBy(dao.getModifiedBy());
//            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }
    }

    public MasterKlasifikasiKeamanan findByNamaKlasifikasiKeamanan(String value) {
        return db().where(t -> t.getNamaKlasifikasiKeamanan().equals(value)).findFirst().orElse(null);
    }
}