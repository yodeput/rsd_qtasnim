package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.helpers.FileUtils;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.PersonalArsipDto;
import com.qtasnim.eoffice.ws.dto.ScanDocumentDto;
import com.qtasnim.eoffice.ws.dto.ScanResultDto;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class PersonalArsipService extends AbstractFacade<PersonalArsip> {

    @Inject
    private MasterFolderKegiatanDetailService folderService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private FileService fileService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Override
    protected Class<PersonalArsip> getEntityClass() {
        return PersonalArsip.class;
    }

    public JPAJinqStream<PersonalArsip> getAll() {
        return db();
    }

    public void saveOrEdit(PersonalArsipDto dao, Long id){
        boolean isNew =id==null;
        PersonalArsip model = new PersonalArsip();
        DateUtil dateUtil = new DateUtil();

        if(isNew){
            model.setDocId(dao.getDocId());

            if(dao.getIdFolder()!=null){
                MasterFolderKegiatanDetail folder = folderService.find(dao.getIdFolder());
                model.setFolder(folder);
            } else {
                model.setFolder(null);
            }
            this.create(model);
        }else{
            model = this.find(id);
            if(dao.getIdFolder()!=null){
                MasterFolderKegiatanDetail folder = folderService.find(dao.getIdFolder());
                model.setFolder(folder);
            } else {
                model.setFolder(null);
            }

            this.edit(model);
        }
    }

    public PersonalArsip getByIdFolderKegiatan(Long id){
        MasterFolderKegiatanDetail folder = folderService.find(id);
        return db().where(a->a.getFolder().equals(folder)).findFirst().orElse(null);
    }

    public void uploadAttachment(Long idFolder, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";

        try {
            String filename = formDataContentDisposition.getFileName();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1 ) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is2 = new ByteArrayInputStream(baos.toByteArray());

            byte[] documentContent = IOUtils.toByteArray(is1);

            docId = fileService.upload(filename, documentContent);
            docId = docId.replace("workspace://SpacesStore/", "");

            MasterFolderKegiatanDetail folderKegiatanDetail = folderService.find(idFolder);

//            String number = autoNumberService.from(SuratDocLib.class).next(SuratDocLib::getNumber);

            /*save personal arsip*/
            PersonalArsip pa = new PersonalArsip();
            pa.setDocId(docId);
            pa.setFolder(folderKegiatanDetail);
            this.create(pa);
            getEntityManager().flush();

            List<String> extractedText = textExtractionService.parsingFile(is2, formDataContentDisposition, body).getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString().concat(FileUtility.GetFileExtension(filename)));
            obj.setDocName(filename);
            obj.setCompanyCode(folderKegiatanDetail.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setSize(new Long(documentContent.length));
            obj.setIsDeleted(false);
            obj.setReferenceId(pa.getId());
            obj.setReferenceTable("t_personal_arsip");
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);
        } catch (Exception ex) {
            if(!StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }

            throw ex;
        }
    }

    public ScanDocumentDto scanDocument(Long idFolder, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";

        try {
            MasterFolderKegiatanDetail folderKegiatanDetail = folderService.find(idFolder);

//            String number = autoNumberService.from(SuratDocLib.class).next(SuratDocLib::getNumber);

            ScanResultDto scanned = textExtractionService.parsingFileFromScan(inputStream, formDataContentDisposition, body);

            if (scanned.getPdf() == null) throw new Exception("scanned source not tiff nor pdf / pdf unrecognized");

            byte[] pdf = scanned.getPdf();

            docId = fileService.upload("scanned.pdf", pdf);
            docId = docId.replace("workspace://SpacesStore/", "");

            /*save personal arsip*/
            PersonalArsip pa = new PersonalArsip();
            pa.setDocId(docId);
            pa.setFolder(folderKegiatanDetail);
            this.create(pa);
            getEntityManager().flush();

            List<String> extractedText = scanned.getText().getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType("application/pdf");
            obj.setDocGeneratedName(UUID.randomUUID().toString().concat(".pdf"));
            obj.setDocName(FileUtils.getFileName(formDataContentDisposition.getFileName()).concat(".pdf"));
            obj.setCompanyCode(pa.getFolder().getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setIsDeleted(false);
            obj.setSize(new Long(pdf.length));
            obj.setReferenceTable("t_personal_arsip");
            obj.setReferenceId(pa.getId());
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);

            return new ScanDocumentDto(scanned.getText(), docId);
        } catch (Exception ex) {
            if(!StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }

            throw ex;
        }
    }

    public List<Long> getByIdFolder(Long idFolderKegiatanDetail) {
        return db().where(a -> a.getFolder().getId().equals(idFolderKegiatanDetail)).map(b -> b.getId()).collect(Collectors.toList());
    }
}
