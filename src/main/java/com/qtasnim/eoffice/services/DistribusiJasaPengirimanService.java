package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.DistribusiJasaPengirimanDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class DistribusiJasaPengirimanService extends AbstractFacade<DistribusiJasaPengiriman> {
    @Inject private CompanyCodeService companyService;
    @Inject private MasterUserService userService;
    @Inject private SuratService suratService;
    @Inject private MasterVendorService vendorService;
    @Inject private MasterKlasifikasiKeamananService klasifikasiKeamananService;
    @Inject private MasterTingkatUrgensiService tingkatUrgensiService;
    @Inject private MasterTingkatPerkembanganService tingkatPerkembanganService;
    @Inject private MasterPerusahaanPengirimanService perusahaanPengirimanService;

    @Override protected Class<DistribusiJasaPengiriman> getEntityClass() { return DistribusiJasaPengiriman.class; }

    public JPAJinqStream<DistribusiJasaPengiriman> getAll() { return db(); }

    public DistribusiJasaPengiriman saveOrEdit(Long id, DistribusiJasaPengirimanDto dto) {
        boolean isNew = id == null;
        DistribusiJasaPengiriman model = this.getModel(isNew ? new DistribusiJasaPengiriman() : this.find(id), dto);

        if (isNew) {
            model.setIsDeleted(false);
            this.create(model);
            getEntityManager().flush();
        } else {
            this.edit(model);
            getEntityManager().flush();
        }

        return model;
    }

    public DistribusiJasaPengiriman delete(Long id) {
        DistribusiJasaPengiriman model = this.find(id);
        model.setIsDeleted(true);
        this.edit(model);
        return model;
    }

    private DistribusiJasaPengiriman getModel(DistribusiJasaPengiriman model, DistribusiJasaPengirimanDto dto) {
        DateUtil dateUtil = new DateUtil();

        model.setNomorDokumen(dto.getNomorDokumen());
        model.setPerihal(dto.getPerihal());
        model.setNomorPengiriman(dto.getNomorPengiriman());
        model.setNomorResi(dto.getNomorResi());
        model.setStatus(dto.getStatus());
        model.setAlasanRetur(dto.getAlasanRetur());
        model.setIsInternal(dto.getIsInternal());

        MasterUser pengirim = userService.find(dto.getIdPengirim());
        model.setPengirim(pengirim);

        if (!Strings.isNullOrEmpty(dto.getTglDokumen())) {
            model.setTglDokumen(dateUtil.getDateFromISOString(dto.getTglDokumen()));
        }

        if (!Strings.isNullOrEmpty(dto.getTglPengiriman())) {
            model.setTglPengiriman(dateUtil.getDateFromISOString(dto.getTglPengiriman()));
        }

        if (!Strings.isNullOrEmpty(dto.getTglDiterima())) {
            model.setTglDiterima(dateUtil.getDateFromISOString(dto.getTglDiterima()));
        }

        if (!Strings.isNullOrEmpty(dto.getTglRetur())) {
            model.setTglRetur(dateUtil.getDateFromISOString(dto.getTglRetur()));
        }

        CompanyCode company = companyService.find(dto.getIdCompany());
        model.setCompanyCode(company);

        if (dto.getIdSurat() != null) {
            Surat surat = suratService.find(dto.getIdSurat());
            model.setSurat(surat);
        }

        if (dto.getIdKlasifikasiKeamanan() != null) {
            MasterKlasifikasiKeamanan klasifikasi = klasifikasiKeamananService.find(dto.getIdKlasifikasiKeamanan());
            model.setKlasifikasiKeamanan(klasifikasi);
        }

        if (dto.getIdTingkatUrgensi() != null) {
            MasterTingkatUrgensi urgensi = tingkatUrgensiService.find(dto.getIdTingkatUrgensi());
            model.setTingkatUrgensi(urgensi);
        }

        if (dto.getIdTingkatPerkembangan() != null) {
            MasterTingkatPerkembangan perkembangan = tingkatPerkembanganService.find(dto.getIdTingkatPerkembangan());
            model.setTingkatPerkembangan(perkembangan);
        }

        if (dto.getPerusahaanPengiriman() != null) {
            MasterPerusahaanPengiriman perusahaan = perusahaanPengirimanService.find(dto.getIdPerusahaanPengiriman());
            model.setPerusahaanPengiriman(perusahaan);
        }

        if (dto.getTujuanInternal() != null) {
            MasterUser tujuanInternal = userService.find(dto.getIdTujuanInternal());
            model.setTujuanInternal(tujuanInternal);
        }

        if (dto.getTujuanEksternal() != null) {
            MasterVendor tujuanEksternal = vendorService.find(dto.getTujuanEksternal());
            model.setTujuanEksternal(tujuanEksternal);
        }

        return model;
    }
}
