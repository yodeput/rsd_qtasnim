package com.qtasnim.eoffice.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.MessageType;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.ws.dto.WebSocketMessage;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;
import java.util.Arrays;
import java.util.stream.Collectors;

@LocalBean
@Stateless
public class GlobalPushEventService {
    @Resource(mappedName = Constants.JNDI_NOTIFICATION_QUEUE)
    private Queue myQueue;

    @Inject
    private JMSContext jmsContext;

    @Inject
    private Logger logger;

    @Inject
    private ApplicationConfig applicationConfig;

    public void push(WebSocketMessage message, MasterUser ...users) {
        try {
            String recipients = String.join(",", Arrays.stream(users).map(MasterUser::getLoginUserName).collect(Collectors.toList()));
            IMessagingProvider messagingProvider = applicationConfig.getMessagingProvider();
            messagingProvider.send(MessageType.GLOBAL, map -> map.put("recipients", recipients), getObjectMapper().writeValueAsString(message));
        } catch (JsonProcessingException e) {
            logger.error(null, e);
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
