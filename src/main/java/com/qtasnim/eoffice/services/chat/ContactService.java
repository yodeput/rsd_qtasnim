package com.qtasnim.eoffice.services.chat;

import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.chat.Contact;
import com.qtasnim.eoffice.services.AbstractFacade;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@LocalBean
public class ContactService extends AbstractFacade<Contact> {
    @Override
    protected Class<Contact> getEntityClass() {
        return Contact.class;
    }

    public List<Contact> selectByContactRelation(Long fromUid, Long toUid) {
        return db()
                .where(t -> t.getFrom().getId().equals(fromUid) && t.getTo().getId().equals(toUid))
                .toList();
    }

    public List<Contact> getContacts(Long friendUid) {
        return db().where(t -> t.getFrom().getId().equals(friendUid)).toList();
    }

    public Boolean isFriend(Long fromUid, Long toUid) {
        return selectByContactRelation(fromUid, toUid).size() > 0;
    }

    public Contact insertContact(MasterUser from, MasterUser to) {
        List<Contact> friendGroupDetails = selectByContactRelation(from.getId(), to.getId());

        if (friendGroupDetails.size() > 0) {
            return friendGroupDetails.stream().findFirst().orElse(null);
        }

        Contact contact = new Contact();
        contact.setTo(to);
        contact.setFrom(from);
        contact.setUnread(0L);

        create(contact);

        return contact;
    }

    @Asynchronous
    public void setUnread(MasterUser from, MasterUser to) {
        getEntityManager()
                .createQuery("UPDATE Contact c SET c.unread = c.unread + 1 WHERE c.from.id = :fromId AND c.to.id = :toId")
                .setParameter("fromId", from.getId())
                .setParameter("toId", to.getId())
                .executeUpdate();
    }

    @Asynchronous
    public void openChat(MasterUser from, MasterUser to) {
        getEntityManager()
                .createQuery("UPDATE Contact c SET c.unread = 0 WHERE c.from.id = :fromId AND c.to.id = :toId")
                .setParameter("fromId", from.getId())
                .setParameter("toId", to.getId())
                .executeUpdate();
    }

    public Contact find(Long fromUid, Long toUid) {
        return db().where(t -> t.getFrom().getId().equals(fromUid) && t.getTo().getId().equals(toUid)).limit(1).findFirst().orElse(null);
    }
}
