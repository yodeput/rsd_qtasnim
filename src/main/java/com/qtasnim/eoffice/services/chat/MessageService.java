package com.qtasnim.eoffice.services.chat;

import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.chat.Contact;
import com.qtasnim.eoffice.db.chat.Message;
import com.qtasnim.eoffice.services.AbstractFacade;
import com.qtasnim.eoffice.services.GlobalPushEventService;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterUserDto;
import com.qtasnim.eoffice.ws.dto.chat.ContactDto;
import com.qtasnim.eoffice.ws.dto.chat.NewContactDto;
import com.qtasnim.eoffice.ws.dto.chat.NewContactPayloadDto;
import com.qtasnim.eoffice.ws.dto.chat.NewMessageDto;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class MessageService extends AbstractFacade<Message> {
    @Inject
    private ContactService contactService;

    @Inject
    private GlobalPushEventService globalPushEventService;

    @Override
    protected Class<Message> getEntityClass() {
        return Message.class;
    }

    public JinqStream<Message> getMessages(Long uid, Long toUid) {
        return db().where(t ->
                (t.getFrom().getId().equals(uid) && t.getTo().getId().equals(toUid))
                || (t.getTo().getId().equals(uid) && t.getFrom().getId().equals(toUid))
        );
    }

    public void insertMessage(MasterUser fromUser, MasterUser toUser, String content, String chatId) {
        Contact contact = contactService.find(toUser.getId(), fromUser.getId());

        if (contact == null) { //create contact
            contact = new Contact();
            contact.setFrom(toUser);
            contact.setTo(fromUser);
            contact.setUnread(0L);
            contactService.create(contact);
            getEntityManager().flush();

            NewContactDto newContactDto = new NewContactDto();
            newContactDto.setPayload(new NewContactPayloadDto(new ContactDto(contact)));
            globalPushEventService.push(newContactDto, toUser);
        }

        Message message = new Message();
        message.setFrom(fromUser);
        message.setTo(toUser);
        message.setMsg(content);
        message.setAddTime(new Date());
        message.setChatId(chatId);

        create(message);
    }

    public void sendMessage(MasterUser fromUser, MasterUser toUser, Date date, String content, String chatId) {
        DateUtil dateUtil = new DateUtil();

        contactService.setUnread(toUser, fromUser);

        com.qtasnim.eoffice.ws.dto.chat.MessageDto payload = new com.qtasnim.eoffice.ws.dto.chat.MessageDto();
        payload.setFrom(new MasterUserDto(fromUser));
        payload.setDate(dateUtil.getCurrentISODate(date));
        payload.setMessage(content);
        payload.setChatId(chatId);

        NewMessageDto newMessage = new NewMessageDto();
        newMessage.setPayload(payload);
        globalPushEventService.push(newMessage, toUser);
    }

    public void setUnread(MasterUser fromUser, MasterUser toUser) {
        contactService.setUnread(toUser, fromUser);
    }
}
