package com.qtasnim.eoffice.services.chat;

import com.qtasnim.eoffice.db.chat.Group;
import com.qtasnim.eoffice.db.chat.GroupDetail;
import com.qtasnim.eoffice.services.AbstractFacade;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class GroupDetailService extends AbstractFacade<GroupDetail> {
    @Override
    protected Class<GroupDetail> getEntityClass() {
        return GroupDetail.class;
    }

    public List<GroupDetail> selectByUID(Long id) {
        return db().where(t -> t.getUser().getId().equals(id)).toList();
    }

    public List<Group> findGroupsByUid(Long uid) {
        return db().where(t -> t.getUser().getId().equals(uid)).map(GroupDetail::getGroup).collect(Collectors.toList());
    }

    public List<GroupDetail> selectByGID(Long groupId) {
        return db().where(t -> t.getGroup().getId().equals(groupId)).toList();
    }
}
