package com.qtasnim.eoffice.services.chat;

import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.chat.Group;
import com.qtasnim.eoffice.db.chat.GroupDetail;
import com.qtasnim.eoffice.services.AbstractFacade;
import com.qtasnim.eoffice.ws.dto.chat.GroupDto;
import com.qtasnim.eoffice.ws.dto.chat.GroupMembersDto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class GroupService extends AbstractFacade<Group> {
    @Inject
    private GroupDetailService groupDetailService;

    @Override
    protected Class<Group> getEntityClass() {
        return Group.class;
    }

    public Group addGroup(MasterUser user, String name) {
        Group group = new Group();
        group.setAddTime(new Date());
        group.setHeadphoto(null);
        group.setName(name);
        group.setOwner(user);

        create(group);

        return group;
    }

    public Group addGroup(MasterUser user, String name, List<MasterUser> members) {
        Group group = addGroup(user, name);

        members.forEach(member -> {
            addMember(group, member);
        });

        return group;
    }

    public void addMemberToGroup(Group group, MasterUser user) {
        addMember(group, user);
    }

    public List<GroupDto> getAllGroup(MasterUser user) {
        return groupDetailService.findGroupsByUid(user.getId())
                .stream()
                .map(GroupDto::new)
                .collect(Collectors.toList());
    }

    public GroupMembersDto getGroupMembers(Group group) {
        return new GroupMembersDto(group);
    }

    public void addMember(Group group, MasterUser user) {
        GroupDetail groupDetail = new GroupDetail();
        groupDetail.setAddTime(new Date());
        groupDetail.setGroup(group);
        groupDetail.setUser(user);

        groupDetailService.create(groupDetail);
    }

    public List<Group> selectGroup(Long userId) {
        return groupDetailService.selectByUID(userId).stream().map(GroupDetail::getGroup).collect(Collectors.toList());
    }

    public List<String> getAllMemberToString(Long groupId) {
        List<GroupDetail> groupDetails = groupDetailService.selectByGID(groupId);
        List<String> result = new ArrayList<>();

        for (GroupDetail groupDetail : groupDetails) {
            result.add(String.valueOf(groupDetail.getUser().getId()));
        }

        return result;
    }
}
