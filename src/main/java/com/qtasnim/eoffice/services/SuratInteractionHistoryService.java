package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.SuratInteractionHistoryDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class SuratInteractionHistoryService extends AbstractFacade<SuratInteractionHistory> {

    
    @Inject
    private SuratService suratService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterUserService masterUserService;


    public List<SuratInteractionHistory> getByIdSurat(Long idSurat, String filter, String sort, boolean descending, int skip, int limit){
        Surat surat = suratService.find(idSurat);
        List<SuratInteractionHistory> history = this.getResultList(filter, sort, descending, skip, limit);
        List<SuratInteractionHistory> history2 = db().where(c->!c.getSurat().equals(surat)).toList();
        history.retainAll(history2);
        return history;
    }

    @Override
    protected Class<SuratInteractionHistory> getEntityClass() {
        return SuratInteractionHistory.class;
    }

    public SuratInteractionHistory save(Long id, SuratInteractionHistoryDto dao){
        SuratInteractionHistory model = new SuratInteractionHistory();
        DateUtil dateUtil = new DateUtil();

        if(id==null){
            MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getOrganzationId());
            model.setOrganization(org);
            Surat surat = suratService.find(dao.getSuratId());
            model.setSurat(surat);
            CompanyCode companyCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(companyCode);
            MasterUser masterUser = masterUserService.find(dao.getUserId());
            model.setUser(masterUser);
            model.setIsRead(dao.getIsRead());
            model.setIsImportant(dao.getIsImportant());
            model.setIsStarred(dao.getIsStarred());
            model.setIsFinished(dao.getIsFinished());
            model.setFinishedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
            model.setModifiedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
            this.create(model);
        } else {
            model = find(id);
            MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getOrganzationId());
            model.setOrganization(org);
            Surat surat = suratService.find(dao.getSuratId());
            model.setSurat(surat);
            CompanyCode companyCode = companyCodeService.find(dao.getCompanyId());
            MasterUser masterUser = masterUserService.find(dao.getUserId());
            model.setUser(masterUser);
            model.setIsRead(dao.getIsRead());
            model.setIsImportant(dao.getIsImportant());
            model.setIsStarred(dao.getIsStarred());
            model.setIsFinished(dao.getIsFinished());
            model.setFinishedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
            model.setModifiedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
            this.edit(model);
        }
        return model;
    }

    public SuratInteractionHistory setStared(Long id){
        SuratInteractionHistory model = this.find(id);
        model.setIsStarred(true);
        this.edit(model);
        return model;
    }

    public SuratInteractionHistory setFinished(Long id){
        SuratInteractionHistory model = this.find(id);
        DateUtil dateUtil = new DateUtil();
        model.setIsFinished(true);
        model.setFinishedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
        this.edit(model);
        return model;
    }

    public SuratInteractionHistory setImportant(Long id){
        SuratInteractionHistory model = this.find(id);
        model.setIsImportant(true);
        this.edit(model);
        return model;
    }

    public SuratInteractionHistory setIsRead(Long id){
        SuratInteractionHistory model = this.find(id);
        model.setIsRead(true);
        this.edit(model);
        return model;
    }


}
