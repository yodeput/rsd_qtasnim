package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.helpers.ClassFinder;
import com.qtasnim.eoffice.helpers.ReflectionHelper;
import com.qtasnim.eoffice.helpers.TemplatingHelper;
import com.qtasnim.eoffice.util.BeanUtil;
import com.qtasnim.eoffice.util.ClassUtil;
import com.qtasnim.eoffice.ws.dto.DataMasterSelectedSourceDto;
import com.qtasnim.eoffice.ws.dto.ListStringDto;
import com.qtasnim.eoffice.ws.dto.DataMasterValueRetrieve;
import com.qtasnim.eoffice.ws.dto.DataMasterValueRetrieveParam;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.Id;
import java.lang.reflect.Field;
import java.util.*;

@Stateless
@LocalBean
public class DataMasterService {
    @Inject
    private ApplicationConfig applicationConfig;

    public DataMasterService() {

    }

    @PostConstruct
    private void postConstruct() {
        classesToIgnore = Arrays.asList(applicationConfig.getDatamasterListToIgnoreTravers().split(",")); //ConfigManager.GetProperty("datamaster.list.to_ignore_travers").split(","));
        classesToParentHandle = Arrays.asList(applicationConfig.getDatamasterListToParentHandle().split(","));  //ConfigManager.GetProperty("datamaster.list.to_parent_handle").split(","));

        defaultDataMasterPackageLocation = applicationConfig.getLocationDbsPackage();//ConfigManager.GetProperty("location.dbs.package");
        defaultDataMasterServicePackageLocation = applicationConfig.getLocationServicesPackage();//ConfigManager.GetProperty("location.services.package");
        defaultDataMasterDtoPackageLocation = applicationConfig.getLocationDtoPackage();//ConfigManager.GetProperty("location.dto.package");
    }

    private List<String> classesToIgnore;
    private List<String> classesToParentHandle;

    private String defaultDataMasterPackageLocation;
    private String defaultDataMasterServicePackageLocation;
    private String defaultDataMasterDtoPackageLocation;

    public ListStringDto getAllDataMaster() {
        ListStringDto result = new ListStringDto();

        List<String> classNickNameList = new LinkedList<>();

        for(String fullname : this.iterateDataMasterServiceDirV2()) {
            String[] fullnameSplitted = fullname.split("\\.");
            String nick = fullnameSplitted[fullnameSplitted.length-1];

            nick = nick.replaceFirst("Service", "");

            if(!classesToIgnore.contains(nick)) {
                classNickNameList.add(nick);
            }
        }

        result.setValues(classNickNameList);

        return result;
    }

    private List<String> getAllDataMasterServiceClasses() {
        List<String> classServiceList = new LinkedList<>();

        for(String fullname : this.iterateDataMasterServiceDir()) {
            String[] fullnameSplitted = fullname.split("\\.");
            String nick = fullnameSplitted[fullnameSplitted.length-1];

            nick = nick.replaceFirst("Service", "");

            if(!classesToIgnore.contains(nick)) {
                classServiceList.add(fullname);
            }
        }

        return classServiceList;
    }

    public ListStringDto getAllFieldOfMasterData(String masterdata) {
        ListStringDto result = new ListStringDto();

        List<String> fieldList = new LinkedList<>();

        result.setValues(fieldList);

        if(classesToIgnore.contains(masterdata)) return result;

        try {
            Class<?> claz = Class.forName(defaultDataMasterPackageLocation+"."+masterdata);

            Field[] fields = claz.getDeclaredFields();

            for(Field field : fields) {
                if(field.isAnnotationPresent(Column.class)) {
                    fieldList.add(field.getName());
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }

    public DataMasterValueRetrieve getValueOfSelectedMasterDataFields(DataMasterValueRetrieveParam param) {
        DataMasterValueRetrieve values = new DataMasterValueRetrieve();

        String tableSourceName = param.getTableSourceName();
        List<String> selectedSourceField = param.getSelectedSourceField();
        String selectedSourceValue = param.getSelectedSourceValue();

        if(StringUtils.isBlank(tableSourceName)) return values;
        if(selectedSourceField == null || selectedSourceField.size() == 0) return values;
        if(StringUtils.isBlank(selectedSourceValue)) return values;

        String dataMasterServiceClass = defaultDataMasterServicePackageLocation+"."+param.getTableSourceName()+"Service",
                dataMasterModelClass = defaultDataMasterPackageLocation+"."+param.getTableSourceName();

        Class classService = ClassUtil.getClassByName(dataMasterServiceClass),
                classModel = ClassUtil.getClassByName(dataMasterModelClass);

        if(classService == null || classModel == null) return values;

        Object objService = BeanUtil.getBean(classService);

        if(objService == null) return values;

        List<String> fields = param.getSelectedSourceField();

        List<Object> datas = (List<Object>) ReflectionHelper.executeMethod(objService, "findAll");

        if(datas == null) return values;

        List<DataMasterSelectedSourceDto> bindedDatas = new LinkedList<>();

        if(classesToParentHandle.contains(param.getTableSourceName())) {
            for (Object data : datas) {
                DataMasterSelectedSourceDto bindedData = new DataMasterSelectedSourceDto();

                Class c1 = data.getClass();

                String fieldParentContainer = "sameParent", fieldIdParentContainer = "id";

                Map<String, String> selectedFieldValues = new HashMap<>();
                for (String field : fields) {
                    String val = ReflectionHelper.getFieldValue(data, field);

                    selectedFieldValues.put(field, val);
                }
                bindedData.setSelectedSourceField(selectedFieldValues);

                String selectedSourceValueVal = ReflectionHelper.getFieldValue(data, param.getSelectedSourceValue());

                bindedData.setSelectedSourceValue(selectedSourceValueVal);

                for(Field field : classModel.getDeclaredFields()) {
                    if(field.getType() .getName().equalsIgnoreCase(classModel.getName())) fieldParentContainer = field.getName();

                    if(field.isAnnotationPresent(Id.class)) fieldIdParentContainer = field.getName();
                }

                Object objParent = ReflectionHelper.executeMethod(data, TemplatingHelper.getGetterName(fieldParentContainer));

                Long idParent;

                if(objParent == null) idParent = null;
                else idParent = Long.parseLong(String.valueOf(ReflectionHelper.executeMethod(objParent, TemplatingHelper.getGetterName(fieldIdParentContainer))));

                Object[] args = new Object[] {idParent};
                Class[] argTypes = new Class[] {Long.class};

                List<Object> parent = (List<Object>) ReflectionHelper.executeMethod(objService, "getListByParent", args, argTypes);

                bindedData.setParent(parent==null?new LinkedList():parent);

                bindedDatas.add(bindedData);
            }
        } else {
            for (Object data : datas) {
                DataMasterSelectedSourceDto bindedData = new DataMasterSelectedSourceDto();

                Class c1 = data.getClass();

                Map<String, String> selectedFieldValues = new HashMap<>();
                for (String field : fields) {
                    String val = ReflectionHelper.getFieldValue(data, field);

                    selectedFieldValues.put(field, val);
                }

                bindedData.setSelectedSourceField(selectedFieldValues);

                String selectedSourceValueVal = ReflectionHelper.getFieldValue(data, param.getSelectedSourceValue());

                bindedData.setSelectedSourceValue(selectedSourceValueVal);

                bindedDatas.add(bindedData);
            }
        }

        values.setValues(bindedDatas);

        return values;
    }

    public DataMasterValueRetrieve getValueOfSelectedMasterDataFields(DataMasterValueRetrieveParam param, String sort, Boolean descending) {
        DataMasterValueRetrieve values = new DataMasterValueRetrieve();

        String dataMasterServiceClass = defaultDataMasterServicePackageLocation+"."+param.getTableSourceName()+"Service",
                dataMasterModelClass = defaultDataMasterPackageLocation+"."+param.getTableSourceName();

        Class classService = ClassUtil.getClassByName(dataMasterServiceClass),
                classModel = ClassUtil.getClassByName(dataMasterModelClass);

        if(classService == null || classModel == null) return values;

        Object objService = BeanUtil.getBean(classService);

        if(objService == null) return values;

        List<String> fields = param.getSelectedSourceField();

        List<Object> datas = (List<Object>) ReflectionHelper.executeMethod(objService, "findAll");

        if(datas == null) return values;

        List<DataMasterSelectedSourceDto> bindedDatas = new LinkedList<>();

        if(classesToParentHandle.contains(param.getTableSourceName())) {
            for (Object data : datas) {
                DataMasterSelectedSourceDto bindedData = new DataMasterSelectedSourceDto();

                Class c1 = data.getClass();

                String fieldParentContainer = "sameParent", fieldIdParentContainer = "id";

                Map<String, String> selectedFieldValues = new HashMap<>();
                for (String field : fields) {
                    String val = ReflectionHelper.getFieldValue(data, field);

                    selectedFieldValues.put(field, val);
                }
                bindedData.setSelectedSourceField(selectedFieldValues);

                String selectedSourceValueVal = ReflectionHelper.getFieldValue(data, param.getSelectedSourceValue());

                bindedData.setSelectedSourceValue(selectedSourceValueVal);

                for(Field field : classModel.getDeclaredFields()) {
                    if(field.getType() .getName().equalsIgnoreCase(classModel.getName())) fieldParentContainer = field.getName();

                    if(field.isAnnotationPresent(Id.class)) fieldIdParentContainer = field.getName();
                }

                Object objParent = ReflectionHelper.executeMethod(data, TemplatingHelper.getGetterName(fieldParentContainer));

                Long idParent;

                if(objParent == null) idParent = null;
                else idParent = Long.parseLong(String.valueOf(ReflectionHelper.executeMethod(objParent, TemplatingHelper.getGetterName(fieldIdParentContainer))));

                Object[] args = new Object[] {idParent, sort, descending};
                Class[] argTypes = new Class[] {Long.class, String.class, Boolean.class};

                List<Object> parent = (List<Object>) ReflectionHelper.executeMethod(objService, "getListByParent", args, argTypes);

                bindedData.setParent(parent==null?new LinkedList():parent);

                bindedDatas.add(bindedData);
            }
        } else {
            for (Object data : datas) {
                DataMasterSelectedSourceDto bindedData = new DataMasterSelectedSourceDto();

                Class c1 = data.getClass();

                Map<String, String> selectedFieldValues = new HashMap<>();
                for (String field : fields) {
                    String val = ReflectionHelper.getFieldValue(data, field);

                    selectedFieldValues.put(field, val);
                }

                bindedData.setSelectedSourceField(selectedFieldValues);

                String selectedSourceValueVal = ReflectionHelper.getFieldValue(data, param.getSelectedSourceValue());

                bindedData.setSelectedSourceValue(selectedSourceValueVal);

                bindedDatas.add(bindedData);
            }
        }

        values.setValues(bindedDatas);

        return values;
    }

    /*
    masalah ketika telah dideploy -library Reflections-
     */
    private List<String> iterateDataMasterServiceDir() {
        List<String> serviceClasses = new LinkedList<>();

        Reflections reflections = new Reflections(defaultDataMasterServicePackageLocation);

        Set<Class<? extends AbstractFacade>> allClasses =
                reflections.getSubTypesOf(AbstractFacade.class);

        allClasses.forEach((e) -> {
            String[] splitted = e.getName().split("\\.");
            String clazname = splitted[splitted.length-1];

            if (clazname.startsWith("Master")) {
                serviceClasses.add(e.getName());
            }
        });

        serviceClasses.sort(Comparator.comparing(String::toString));

        return serviceClasses;
    }

    private List<String> iterateDataMasterServiceDirV2() {
        List<String> serviceClasses = new LinkedList<>();

        List<Class<?>> classes = ClassFinder.find(defaultDataMasterServicePackageLocation);

        classes.forEach((e) -> {
            String[] splitted = e.getName().split("\\.");
            String clazname = splitted[splitted.length-1];

            if (clazname.startsWith("Master")) {
                serviceClasses.add(e.getName());
            }
        });

        return serviceClasses;
    }
}
