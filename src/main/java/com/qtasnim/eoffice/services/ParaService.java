package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.CompanyCodeDto;
import com.qtasnim.eoffice.ws.dto.MasterStrukturOrganisasiDto;
import com.qtasnim.eoffice.ws.dto.ParaDetailDto;
import com.qtasnim.eoffice.ws.dto.ParaDto;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class ParaService extends AbstractFacade<Para> {

    @Inject
    private ParaDetailService paraDetailService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private MasterUserService userService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    @ISessionContext
    private Session user;

    @Override
    protected Class<Para> getEntityClass() {
        return Para.class;
    }

    public JPAJinqStream<Para> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && !q.getIsDeleted());
    }

    public void saveOrEdit(ParaDto dao, Long id) {
        boolean isNew = id == null;
        Para model = new Para();
        DateUtil dateUtil = new DateUtil();
        if (isNew) {
            model.setNamaPara(dao.getNamaPara());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if (dao.getEndDate() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            if (dao.getCompanyId() != null) {
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setIsPosition(dao.getIsPosition());
            model.setIsDeleted(false);
            this.create(model);
            getEntityManager().flush();

            if (dao.getDetailIds() != null && !dao.getDetailIds().isEmpty()) {
                for (Long a : dao.getDetailIds()) {
                    if(dao.getIsPosition().booleanValue()) {
                        ParaDetail obj = new ParaDetail();
                        MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(a);
                        obj.setOrganisasi(org);
                        obj.setPara(model);
                        obj.setStart(model.getStart());
                        obj.setEnd(model.getEnd());
                        paraDetailService.create(obj);
                    }else{
                        ParaDetail obj = new ParaDetail();
                        MasterUser usr = userService.find(a);
                        obj.setUser(usr);
                        obj.setPara(model);
                        obj.setStart(model.getStart());
                        obj.setEnd(model.getEnd());
                        paraDetailService.create(obj);
                    }
                }
            }
        } else {
            model = this.find(id);
            model.setNamaPara(dao.getNamaPara());
            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            model.setIsPosition(dao.getIsPosition());
            model.setIsDeleted(false);
            if (dao.getEndDate() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            if (dao.getCompanyId() != null) {
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            this.edit(model);
            getEntityManager().flush();

            List<ParaDetail> rmDetail = paraDetailService.getByIdPara(id).collect(Collectors.toList());
            for (ParaDetail pd : rmDetail) {
                paraDetailService.remove(pd);
            }

            if (dao.getDetailIds() != null && !dao.getDetailIds().isEmpty()) {
                for (Long a : dao.getDetailIds()) {
                    if(dao.getIsPosition().booleanValue()) {
                        ParaDetail obj = new ParaDetail();
                        MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(a);
                        obj.setOrganisasi(org);
                        obj.setPara(model);
                        obj.setStart(model.getStart());
                        obj.setEnd(model.getEnd());
                        paraDetailService.create(obj);
                    }else{
                        ParaDetail obj = new ParaDetail();
                        MasterUser usr = userService.find(a);
                        obj.setUser(usr);
                        obj.setPara(model);
                        obj.setStart(model.getStart());
                        obj.setEnd(model.getEnd());
                        paraDetailService.create(obj);
                    }
                }
            }
            getEntityManager().flush();
            getEntityManager().refresh(model);
        }
    }

    public void deleteData(Long id) {
        try {
            Para para = this.find(id);
//            List<ParaDetail> rmDetail = paraDetailService.getByIdPara(id).collect(Collectors.toList());
//            if (!rmDetail.isEmpty()) {
//                for (ParaDetail pd : rmDetail) {
//                    paraDetailService.remove(pd);
//                }
//            }
            para.setIsDeleted(true);
            this.edit(para);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public JPAJinqStream<Para> getDataById(Long idPara) {
        Para obj = this.find(idPara);
        return db().where(q -> q.equals(obj));
    }

    public TypedQuery<Para> getResultList(String filter, String start, String end, Boolean descending, String sort) {
//        Date n = new Date();
        DateUtil dateUtil = new DateUtil();
        int kondisi = 0;

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Para> q = cb.createQuery(Para.class);
        Root<Para> root = q.from(Para.class);

        Join<Para, ParaDetail> detail = root.join("paraDetails", JoinType.LEFT);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted"))
        );
        predicates.add(mainCond);

        //FilterBy
        if (!Strings.isNullOrEmpty(filter)) {
            Predicate pFilter = cb.or(
                    cb.like(cb.lower(root.get("namaPara")), "%" + filter.toLowerCase() + "%"),
                    cb.like(detail.get("organisasi").get("organizationName"),"%"+filter+"%"),
                    cb.or(
                            cb.and(
                                cb.isNotNull(detail.get("organisasi")),
                                cb.or(
                                    cb.like(detail.join("organisasi", JoinType.LEFT).join("user", JoinType.LEFT)
                                        .get("nameFront"), "%" + filter + "%"),
                                    cb.like(detail.join("organisasi", JoinType.LEFT).join("user", JoinType.LEFT)
                                        .get("nameMiddleLast"), "%" + filter + "%"),
                                    cb.like(
                                        cb.concat(detail.join("organisasi", JoinType.LEFT).join("user", JoinType.LEFT)
                                                .get("nameFront"), cb.concat(" ", detail.join("organisasi", JoinType.LEFT).join("user", JoinType.LEFT)
                                                .get("nameMiddleLast"))), "%" + filter + "%"
                                    )
                                )
                            ),
                            cb.and(
                                    cb.isNotNull(detail.get("user")),
                                    cb.or(
                                            cb.like(detail.join("user", JoinType.LEFT).get("nameFront"), "%" + filter + "%"),
                                            cb.like(detail.join("user", JoinType.LEFT).get("nameMiddleLast"), "%" + filter + "%"),
                                            cb.like(
                                                    cb.concat(detail.join("user", JoinType.LEFT)
                                                            .get("nameFront"), cb.concat(" ", detail.join("user", JoinType.LEFT)
                                                            .get("nameMiddleLast"))), "%" + filter + "%"
                                            )
                                    )
                            )
                    )
            );
            predicates.add(pFilter);
        }

        // WHERE CONDITION        
        if (Strings.isNullOrEmpty(start)) {
            kondisi = Strings.isNullOrEmpty(end) ? 0 : 2;
        } else {
            kondisi = Strings.isNullOrEmpty(end) ? 1 : 3;
        }

        System.out.println("kondisi: " + kondisi);

        if (kondisi > 0) {
            Date startDate, endDate;
            Path<Date> rootStart, rootEnd;

            switch (kondisi) {
                case 1:
//                    startDate = dateUtil.getDateFromISOString_Biasa(start + "T00:00:00.000Z");
//                    endDate = dateUtil.getDateFromISOString_Biasa(start + "T23:59:59.999Z");
                    startDate = dateUtil.getDateTimeStart(start);
                    endDate = dateUtil.getDateTimeEnd(start);
                    rootStart = root.<Date>get("start");
                    rootEnd = root.<Date>get("start");
                    break;
                case 2:
//                    startDate = dateUtil.getDateFromISOString_Biasa(end + "T00:00:00.000Z");
//                    endDate = dateUtil.getDateFromISOString_Biasa(end + "T23:59:59.999Z");
                    startDate = dateUtil.getDateTimeStart(end);
                    endDate = dateUtil.getDateTimeEnd(end);
                    rootStart = root.<Date>get("end");
                    rootEnd = root.<Date>get("end");
                    break;
                default:
//                    startDate = dateUtil.getDateFromISOString_Biasa(start + "T00:00:00.000Z");
//                    endDate = dateUtil.getDateFromISOString_Biasa(end + "T23:59:59.999Z");
                    startDate = dateUtil.getDateTimeStart(start);
                    endDate = dateUtil.getDateTimeEnd(end);
                    rootStart = root.<Date>get("start");
                    rootEnd = root.<Date>get("end");
                    break;
            }

            System.out.println("start date: " + startDate);
            System.out.println("end date: " + endDate);

            Predicate pBetween
                    = cb.and(
                            cb.greaterThanOrEqualTo(rootStart, startDate),
                            cb.lessThanOrEqualTo(rootEnd, endDate)
                    );
            predicates.add(pBetween);
        }

//        predicates.add(mainCond);
        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);

        if (descending != null && !Strings.isNullOrEmpty(sort)) {
            if (descending) {
                q.orderBy(cb.desc(root.get(sort)));
            } else {
                q.orderBy(cb.asc(root.get(sort)));
            }
        }

        System.out.println("query string: " + q);

        return getEntityManager().createQuery(q);
    }

    public HashMap<String, Object> getFiltered(String filter, String start, String end, int skip, int limit, Boolean descending, String sort) {
        List<ParaDto> result = new ArrayList<>();
        TypedQuery<Para> all = getResultList(filter, start, end, descending, sort);
        Long length = all.getResultList().stream().count();
        result = MappingToDto(all, skip, limit);
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    private List<ParaDto> MappingToDto(TypedQuery<Para> all, int skip, int limit) {
        List<ParaDto> result = null;

        if (all != null) {
            result = all.setFirstResult(skip).setMaxResults(limit).getResultStream().map(q->{
                ParaDto para = new ParaDto();
                para.setIdPara(q.getIdPara());
                para.setNamaPara(q.getNamaPara());
                para.setIsPosition(q.getIsPosition());

                DateUtil dateUtil = new DateUtil();
                para.setCreatedDate(dateUtil.getCurrentISODate(q.getCreatedDate()));
                para.setCreatedBy(q.getCreatedBy());
                if(q.getModifiedDate()!=null) {
                    para.setModifiedDate(dateUtil.getCurrentISODate(q.getModifiedDate()));
                }
                para.setModifiedBy(q.getModifiedBy());
                para.setStartDate(dateUtil.getCurrentISODate(q.getStart()));
                para.setEndDate(dateUtil.getCurrentISODate(q.getEnd()));
                para.setCompanyCode(Optional.ofNullable(q.getCompanyCode()).map(CompanyCodeDto::new).orElse(null));
                para.setCompanyId(Optional.ofNullable(q.getCompanyCode()).map(s->s.getId()).orElse(null));
                para.setIsDeleted(q.getIsDeleted());

                if(!para.getIsPosition().booleanValue()){
                    List<ParaDetailDto> detailDtos = new ArrayList<>();
                    for(ParaDetail pd:q.getParaDetails()){
                        ParaDetailDto pds = new ParaDetailDto(pd);
                        MasterStrukturOrganisasi org = masterStrukturOrganisasiService.getByIdUser(pd.getUser().getId());
                        if(org!=null) {
                            pds.setOrgList(new MasterStrukturOrganisasiDto(org));
                            pds.setIdOrganization(org.getIdOrganization());
                            detailDtos.add(pds);
                        }
                    }
                    para.setParaDetails(detailDtos);
                }else {
                    para.setParaDetails(q.getParaDetails().stream().map(ParaDetailDto::new).collect(Collectors.toList()));
                }

                return para;
            }).collect(Collectors.toList());
        }
        return result;
    }

    private Date get_DateTime(String iso) throws InternalServerErrorException {
        try {
            DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            return df1.parse(iso);
        } catch (ParseException e) {
            throw new InternalServerErrorException("Invalid date format, must be yyyy-MM-dd'T'HH:mm:ss", "QT-ERR-WS-01", e);
        }
    }

    private Date getFormattedFromDateTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    private Date getFormattedToDateTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    public void uploadData(boolean isPosition, FormDataBodyPart body) throws Exception {
        try {
            BodyPartEntity bodyPartEntity = (BodyPartEntity) body.getEntity();
            byte[] documentContent = IOUtils.toByteArray(bodyPartEntity.getInputStream());
            InputStream inputStream = new ByteArrayInputStream(documentContent);

            XSSFWorkbook wb = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = wb.getSheetAt(0);
            Iterator<Row> rowIter = sheet.rowIterator();
            rowIter.next();
            rowIter.next();
            rowIter.next();
            rowIter.next();
            rowIter.next();
            List<String> list = new ArrayList<>();
            int rows = 0;
            String namaGrup = "";
            while (rowIter.hasNext()) {
                Row row = rowIter.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                cellIterator.next();
                int i = 1;
                int j = 1;
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    switch (cell.getCellType()) {
                        case NUMERIC:
                            j++;
                            list.add(String.valueOf((int)cell.getNumericCellValue()));
                            break;
                        case STRING:
                            j++;
                            if (i == 1) {
                                namaGrup = cell.getStringCellValue();
                            } else {
                                list.add(cell.getStringCellValue());
                            }
                            break;
                    }
                    i++;
                }

                if(j>1){
                    rows++;
                }
            }

            wb.close();

            MasterUser usr = user.getUserSession().getUser();
            DateUtil dateUtil = new DateUtil();
            Date n = new Date();
            List<String> notFound  = new ArrayList<>();
            if(!list.isEmpty() && rows == list.size()) {
                if (!checkGroupName(namaGrup, usr.getCompanyCode().getId())) {
                    String iso = dateUtil.getCurrentISODate(n);
                    if (isPosition) {
                        if (!list.isEmpty()) {
                            List<MasterStrukturOrganisasi> orgTmp = new ArrayList<>();
                            for (String ls : list) {
                                MasterStrukturOrganisasi org = masterStrukturOrganisasiService.getByName(ls);
                                if (org != null) {
                                    orgTmp.add(org);
                                } else {
                                    notFound.add(ls);
                                }
                            }

                            if (notFound.isEmpty()) {
                                Para obj = new Para();
                                obj.setNamaPara(namaGrup);
                                obj.setIsPosition(isPosition);
                                obj.setCompanyCode(usr.getCompanyCode());
                                obj.setStart(dateUtil.getDateTimeStart(iso));
                                obj.setEnd(dateUtil.getDefValue());
                                obj.setIsDeleted(false);
                                this.create(obj);
                                getEntityManager().flush();

                                for (MasterStrukturOrganisasi org : orgTmp) {
                                    ParaDetail pd = new ParaDetail();
                                    pd.setPara(obj);
                                    pd.setStart(dateUtil.getDateTimeStart(iso));
                                    pd.setEnd(dateUtil.getDefValue());
                                    pd.setOrganisasi(org);
                                    paraDetailService.create(pd);
                                }
                            } else {
                                String listNotFound = "";
                                for (int i = 0; i < notFound.size(); i++) {
                                    if (i == notFound.size() - 1) {
                                        listNotFound = listNotFound + notFound.get(i);
                                    } else {
                                        listNotFound = listNotFound + notFound.get(i) + " , ";
                                    }
                                }
                                throw new Exception("Kesalahan pada data yang diunggah : " + listNotFound);
                            }
                        }
                    } else {
                        if (!list.isEmpty()) {
                            List<MasterUser> tmpUser = new ArrayList<>();
                            for (String ls : list) {
                                MasterUser mu = userService.findUserByNIPP(ls);
                                if (mu != null) {
                                    tmpUser.add(mu);
                                } else {
                                    notFound.add(ls);
                                }
                            }

                            if (notFound.isEmpty()) {
                                Para obj = new Para();
                                obj.setNamaPara(namaGrup);
                                obj.setIsPosition(isPosition);
                                obj.setCompanyCode(usr.getCompanyCode());
                                obj.setStart(dateUtil.getDateTimeStart(iso));
                                obj.setEnd(dateUtil.getDefValue());
                                obj.setIsDeleted(false);
                                this.create(obj);
                                getEntityManager().flush();

                                for (MasterUser mu : tmpUser) {
                                    ParaDetail pd = new ParaDetail();
                                    pd.setPara(obj);
                                    pd.setStart(dateUtil.getDateTimeStart(iso));
                                    pd.setEnd(dateUtil.getDefValue());
                                    pd.setUser(mu);
                                    paraDetailService.create(pd);
                                }
                            } else {
                                String listNotFound = "";
                                for (int i = 0; i < notFound.size(); i++) {
                                    if (i == notFound.size() - 1) {
                                        listNotFound = listNotFound + " " + notFound.get(i);
                                    } else {
                                        listNotFound = listNotFound + notFound.get(i) + " ,";
                                    }
                                }
                                throw new Exception("Kesalahan pada data yang diunggah : " + listNotFound);
                            }
                        }
                    }
                } else {
                    throw new Exception("Grup Para sudah Ada !");
                }
            }else{
                throw new Exception("Data yang diupload tidak lengkap");
            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    private boolean checkGroupName(String groupName,Long companyId){
        return this.db().anyMatch(a->a.getNamaPara().toLowerCase().equals(groupName.toLowerCase()) && a.getCompanyCode().getId().equals(companyId) && !a.getIsDeleted());
    }

    public boolean validate(Long id, String groupName, Long companyId){
        boolean isValid = false;
        JPAJinqStream<Para> query = db().where(a->!a.getIsDeleted() && a.getCompanyCode().getId().equals(companyId));
        if(id==null){
            List <Para> datas = query.collect(Collectors.toList());
            isValid = datas.stream().noneMatch(b->b.getNamaPara().toLowerCase().equals(groupName.toLowerCase()));
        }else{
            List<Para> datas = query.where(b->!b.getIdPara().equals(id)).collect(Collectors.toList());
            isValid = datas.stream().noneMatch(c->c.getNamaPara().toLowerCase().equals(groupName.toLowerCase()));
        }

        return isValid;
    }
}
