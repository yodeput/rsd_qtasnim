package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.helpers.FileUtils;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class ArsipService extends AbstractFacade<Arsip> {

    private static final long serialVersionUID = 1L;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    CompanyCodeService companyCodeService;

    @Inject
    private MasterStrukturOrganisasiService strukturOrganisasiService;

    @Inject
    private MasterKlasifikasiMasalahService klasifikasiMasalahService;

    @Inject
    private MasterKlasifikasiKeamananService klasifikasiKeamananService;

    @Inject
    private MasterSatuanService satuanService;

    @Inject
    private MasterBahasaService bahasaService;

    @Inject
    private MasterJenisDokumenService jenisDokumenService;

    @Inject
    private MasterTingkatPerkembanganService tingkatPerkembanganService;

    @Inject
    private MasterTingkatAksesPublikService tingkatAksesPublikService;

    @Inject
    private MasterTingkatUrgensiService urgensiService;

    @Inject
    private MasterMediaArsipService mediaArsipService;

    @Inject
    private MasterKategoriArsipService kategoriArsipService;

    @Inject
    private MasterPenciptaArsipService penciptaArsipService;

    @Inject
    private BerkasService berkasService;

    @Inject
    private FileService fileService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private MasterKorsaService korsaService;

    @Inject
    private MasterAutoNumberService autoNumberService;

    @Inject
    private SuratService suratService;

    @Override
    protected Class<Arsip> getEntityClass() {
        return Arsip.class;
    }

    public JPAJinqStream<Arsip> getFiltered(String name) {
        return db().where(q -> q.getNomor().toLowerCase().contains(name));
    }

    public JPAJinqStream<Arsip> getByBerkas(Long id) {
        Berkas berkas = berkasService.find(id);
        return db().where(q -> !q.getIsDeleted() && q.getBerkas().equals(berkas));
    }

    public JPAJinqStream<Arsip> getArsipByStatus(String status) {
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();

        if (status.equals("aktif")) {
            return db().where(q -> !q.getIsDeleted() && q.getBerkas().getUnit().equals(korsa) && q.getBerkas().getIsAktif());
        } else {
            return db().where(q -> !q.getIsDeleted() && q.getBerkas().getUnit().equals(korsa) && !q.getBerkas().getIsAktif());
        }
    }

    public JPAJinqStream<Arsip> getArsipAktifToInaktif() {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();
        return db().where(q -> !q.getIsDeleted() && q.getBerkas().getUnit().equals(korsa) && q.getBerkas().getIsAktif() && q.getBerkas().getClosedDate() != null);

    }

    public JPAJinqStream<Arsip> getArsipHabisRetensi() {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();
        Date n = new Date();
        return db().where(q -> !q.getIsDeleted() && q.getBerkas().getUnit().equals(korsa)
                && (n.after(q.getBerkas().getJraInaktif())));

    }

    public JPAJinqStream<Arsip> getArsipAkanMusnah() {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();
        return db().where(q -> !q.getIsDeleted()
                && q.getBerkas().getUnit().equals(korsa) && q.getBerkas().getRencanaMusnahDate() != null);
    }

    public Arsip save(Long id, ArsipDto dao) {
        Arsip model = new Arsip();
        if (id == null) {
            this.create(data(model, dao));
            setBerkas(dao);
        } else {
            model = this.find(id);
            this.edit(data(model, dao));
            setBerkas(dao);
        }

        return model;
    }

    private Arsip data(Arsip arsip, ArsipDto dao) {
        Arsip model = arsip;
        DateUtil dateUtil = new DateUtil();
        model.setNomor(dao.getNomor());
        model.setStatus(dao.getStatus());
        model.setDeskripsi(dao.getDeskripsi());
        model.setPerihal(dao.getPerihal());
        model.setIsAsset(dao.getIsAsset());
        model.setIsVital(dao.getIsVital());
        model.setJumlah(dao.getJumlah());
        model.setIsDeleted(false);
        model.setTglNaskah(new Date());

        Berkas berkas = berkasService.find(dao.getIdBerkas());
        model.setBerkas(berkas);

        if (dao.getIdOrganisasi() != null) {
            MasterStrukturOrganisasi org = strukturOrganisasiService.find(dao.getIdOrganisasi());
            model.setOrganisasi(org);
        } else {
            model.setOrganisasi(null);
        }

        if (dao.getIdUnit() != null) {
            MasterKorsa korsa = korsaService.findById(dao.getIdUnit());
            model.setUnit(korsa);
        } else {
            model.setUnit(null);
        }

        if (dao.getIdCompany() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getIdCompany());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }

        if (dao.getIdMasalah() != null) {
            MasterKlasifikasiMasalah klasifikasiMasalah = klasifikasiMasalahService.find(dao.getIdMasalah());
            model.setKlasifikasiMasalah(klasifikasiMasalah);
        } else {
            model.setKlasifikasiMasalah(null);
        }

        if (dao.getIdKeamanan() != null) {
            MasterKlasifikasiKeamanan keamanan = klasifikasiKeamananService.find(dao.getIdKeamanan());
            model.setKlasifikasiKeamanan(keamanan);
        } else {
            model.setKlasifikasiKeamanan(null);
        }

        if (dao.getIdJenisDokumen() != null) {
            MasterJenisDokumen jenisDokumen = jenisDokumenService.find(dao.getIdJenisDokumen());
            model.setJenisDokumen(jenisDokumen);
        } else {
            model.setJenisDokumen(null);
        }
        if (dao.getIdBahasa() != null) {
            MasterBahasa bahasa = bahasaService.find(dao.getIdBahasa());
            model.setBahasa(bahasa);
        } else {
            model.setBahasa(null);
        }
        if (dao.getIdSatuan() != null) {
            MasterSatuan satuan = satuanService.find(dao.getIdSatuan());
            model.setSatuan(satuan);
        } else {
            model.setSatuan(null);
        }
        if (dao.getIdTingkatPerkembangan() != null) {
            MasterTingkatPerkembangan tingkatPerkembangan = tingkatPerkembanganService.find(dao.getIdTingkatPerkembangan());
            model.setTingkatPerkembangan(tingkatPerkembangan);
        } else {
            model.setTingkatPerkembangan(null);
        }
        if (dao.getIdTingkatUrgensi() != null) {
            MasterTingkatUrgensi urgensi = urgensiService.find(dao.getIdTingkatUrgensi());
            model.setTingkatUrgensi(urgensi);
        } else {
            model.setTingkatUrgensi(null);
        }
        if (dao.getIdKategoriArsip() != null) {
            MasterKategoriArsip kategoriArsip = kategoriArsipService.find(dao.getIdKategoriArsip());
            model.setKategoriArsip(kategoriArsip);
        } else {
            model.setKategoriArsip(null);
        }
        if (dao.getIdTingkatAkses() != null) {
            MasterTingkatAksesPublik tingkatAksesPublik = tingkatAksesPublikService.find(dao.getIdTingkatAkses());
            model.setTingkatAkses(tingkatAksesPublik);
        } else {
            model.setTingkatAkses(null);
        }

        if (dao.getIdMediaArsip() != null) {
            MasterMediaArsip mediaArsip = mediaArsipService.find(dao.getIdMediaArsip());
            model.setMediaArsip(mediaArsip);
        } else {
            model.setMediaArsip(null);
        }

        if (dao.getIdPenciptaArsip() != null) {
            MasterPenciptaArsip penciptaArsip = penciptaArsipService.find(dao.getIdPenciptaArsip());
            model.setPenciptaArsip(penciptaArsip);
        } else {
            model.setPenciptaArsip(null);
        }

        if (!Strings.isNullOrEmpty(dao.getJraAktif())) {
            String tglAktif = dateUtil.getCurrentISODate(model.getJraAktif());
            if (!tglAktif.equals(dao.getJraAktif())) {
                tglAktif = dao.getJraAktif();
                model.setJraAktif(dateUtil.getDateFromISOString(tglAktif));
            }
        }

        if (!Strings.isNullOrEmpty(dao.getJraInaktif())) {
            String tglInaktif = dateUtil.getCurrentISODate(model.getJraInaktif());
            if (!tglInaktif.equals(dao.getJraInaktif())) {
                tglInaktif = dao.getJraInaktif();
                model.setJraInaktif(dateUtil.getDateFromISOString(tglInaktif));
            }
        }

        if(model.getId() == null) {
            String nomor = autoNumberService.from(Arsip.class).next(t -> t.getNomor(), model);
            model.setNomor(nomor);
        }

        return model;
    }

    private void setBerkas(ArsipDto dao) {
        getEntityManager().flush();
        Berkas berkas = berkasService.find(dao.getIdBerkas());
        JPAJinqStream<Arsip> arsipSearch1 = db().where(q -> q.getBerkas().equals(berkas));
        JPAJinqStream<Arsip> arsipSearch2 = db().where(q -> q.getBerkas().equals(berkas));
        Date minDate = arsipSearch1.toList().stream().map(u -> u.getModifiedDate()).min(Date::compareTo).get();
        Date maxDate = arsipSearch2.toList().stream().map(u -> u.getModifiedDate()).max(Date::compareTo).get();

        berkas.setStartKurun(minDate);
        berkas.setEndKurun(maxDate);
        berkasService.edit(berkas);
    }

    public Arsip delete(Long id) throws Exception {
        Arsip model = this.find(id);
        try{
            List<GlobalAttachment> files = globalAttachmentService.getAttachmentDataByRef("t_arsip", model.getId()).collect(Collectors.toList());
            if(files.size() > 0){
                throw new Exception("Folder sudah digunakan.");
            }
            else{
                model.setIsDeleted(true);
                this.edit(model);
                /*@TODO hapus identitas arsip di surat*/
                Surat objSurat = suratService.getSuratByIdArsip(model.getId());
                if(objSurat!=null){
                    objSurat.setIdArsip(null);
                    objSurat.setFolderArsip(null);
                    suratService.edit(objSurat);
                }
            }

        }catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
        /*Tidak boleh di delete*/
        /*List<GlobalAttachment> files = globalAttachmentService.getAttachmentDataByRef("t_arsip", model.getId()).collect(Collectors.toList());
        for (GlobalAttachment ga : files) {
            ga.setIsDeleted(true);
            globalAttachmentService.edit(ga);
        }*/

        return model;
    }

    public void uploadAttachment(Long idArsip, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";
        try {
            String filename = formDataContentDisposition.getFileName();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is2 = new ByteArrayInputStream(baos.toByteArray());

            byte[] documentContent = IOUtils.toByteArray(is2);

            Arsip arsip = this.find(idArsip);

//            String number = autoNumberService.from(SuratDocLib.class).next(SuratDocLib::getNumber);
            List<String> extractedText = textExtractionService.parsingFile(is1, formDataContentDisposition, body).getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            docId = fileService.upload(filename, documentContent);
            docId = docId.replace("workspace://SpacesStore/", "");

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString().concat(FileUtility.GetFileExtension(filename)));
            obj.setDocName(filename);
            obj.setCompanyCode(arsip.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setSize(new Long(documentContent.length));
            obj.setIsDeleted(false);
            obj.setReferenceId(idArsip);
            obj.setReferenceTable("t_arsip");
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);
        } catch (Exception ex) {
            if (!StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }
            throw ex;
        }
    }

    public List<Arsip> getArsipAktif(String filter, String sort, boolean descending, int skip, int limit) {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();

        List<Arsip> arsipFilter = this.getResultList(filter, sort, descending, skip, limit);
        List<Arsip> arsipFilter2 = db().where(c -> !c.getIsDeleted() && !c.getUnit().equals(korsa)).toList();
        arsipFilter.retainAll(arsipFilter2);
        return arsipFilter;
    }

    public List<Arsip> filterByArsipIds(List<Long> ids) {
        return db().where(a -> !a.getIsDeleted() && ids.contains(a.getId())).collect(Collectors.toList());
    }

    public ScanDocumentDto scanDocument(Long idArsip, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";

        try {
            if (idArsip == null) {
                throw new Exception("idArsip null");
            }

            Arsip arsip = this.find(idArsip);

            if (arsip == null) {
                throw new Exception("Arsip tidak tersedia");
            }

            ScanResultDto scanned = textExtractionService.parsingFileFromScan(inputStream, formDataContentDisposition, body);

            if (scanned.getPdf() == null) {
                throw new Exception("scanned source not tiff nor pdf / pdf unrecognized");
            }

            byte[] pdf = scanned.getPdf();

            docId = fileService.upload("scanned.pdf", pdf);
            docId = docId.replace("workspace://SpacesStore/", "");

            List<String> extractedText = scanned.getText().getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType("application/pdf");
            obj.setDocGeneratedName(UUID.randomUUID().toString().concat(".pdf"));
            obj.setDocName(FileUtils.getFileName(formDataContentDisposition.getFileName()).concat(".pdf"));
            obj.setCompanyCode(arsip.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setIsDeleted(false);
            obj.setSize(new Long(pdf.length));
            obj.setReferenceTable("t_arsip");
            obj.setReferenceId(idArsip);
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);

            return new ScanDocumentDto(scanned.getText(), docId);
        } catch (Exception ex) {
            if (!StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }

            throw ex;
        }
    }

    public GlobalAttachment deleteFile(Long id) {
        GlobalAttachment global = globalAttachmentService.find(id);

        if (global.getReferenceTable().toLowerCase().equals("t_arsip")) {
            global.setIsDeleted(true);
            globalAttachmentService.edit(global);
        }

        return global;
    }

    public HashMap<String, Object> getAll(String filter, String sort, Boolean descending, int skip, int limit) {
        List<ArsipDto> result = new ArrayList<>();

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Arsip> q = cb.createQuery(Arsip.class);
        Root<Arsip> root = q.from(Arsip.class);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted"))
        );

        if (!Strings.isNullOrEmpty(filter)) {
            Predicate pkeyword
                    = cb.or(
                            cb.like(root.get("perihal"), "%" + filter + "%"),
                            cb.like(root.get("deskripsi"), "%" + filter + "%"),
                            cb.like(root.get("nomor"), "%" + filter + "%")
                    );

            predicates.add(pkeyword);
        }

        // WHERE CONDITION
        predicates.add(mainCond);

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);

        if (descending != null && !Strings.isNullOrEmpty(sort)) {
            if (descending) {
                q.orderBy(cb.desc(root.get(sort)));
            } else {
                q.orderBy(cb.asc(root.get(sort)));
            }
        }

        TypedQuery<Arsip> all = getEntityManager().createQuery(q).setFirstResult(skip).setMaxResults(limit);
        result = MappingToDto(all);

        Long length = all.getResultStream().count();

        HashMap<String, Object> sum = new HashMap<String, Object>();
        sum.put("data", result);
        sum.put("length", length);
        return sum;
    }

    private List<ArsipDto> MappingToDto(TypedQuery<Arsip> all) {
        List<ArsipDto> result = null;

        if (all != null) {
            result = all.getResultStream()
                    .map(ArsipDto::new).collect(Collectors.toList());
        }
        return result;
    }

    public HashMap<String, Object> getFilteredArsipByStatus(String status, String startDate, String endDate, String namaBerkas, String judulArsip, String noArsip, Long idJenisDokumen, String idUnit, Long idKArsip, int skip, int limit, String sort, Boolean descending) {
        List<ReportArsipDto> result = new ArrayList<>();
        TypedQuery<Arsip> all = this.getResultByStatus(status, startDate, endDate, namaBerkas, judulArsip, noArsip, idJenisDokumen, idUnit, idKArsip, sort, descending);
        result = this.mappingToReportDto(all, skip, limit);

        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    private TypedQuery<Arsip> getResultByStatus(String status, String start, String end, String namaBerkas, String judulArsip, String noArsip, Long idJenisDokumen, String idUnit, Long idKArsip, String sort, Boolean descending) {
        DateUtil dateUtil = new DateUtil();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Arsip> q = cb.createQuery(Arsip.class);
        Root<Arsip> root = q.from(Arsip.class);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted")),
                cb.equal(root.get("berkas").get("unit").get("id"), korsa.getId())
        );

        Expression<Boolean> isAktif = root.get("berkas").get("isAktif");
        Predicate cond1 = status.equalsIgnoreCase("aktif") ?
                cb.and(cb.isTrue(isAktif),cb.isNull(root.get("berkas").get("closedDate")))
                : cb.and(cb.isFalse(isAktif),cb.equal(root.get("berkas").get("status"),"INAKTIF"));
        predicates.add(cond1);

        Path<Date> tanggal = (root.<Date>get("tglNaskah"));
        if (!Strings.isNullOrEmpty(start)) {
            Boolean single = Strings.isNullOrEmpty(end);
            Date startDate = dateUtil.getDateTimeStart(start + "T00:00:00.000Z");
            if (single) {
                Date endDate = dateUtil.getDateTimeEnd(start + "T23:59:59.999Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(tanggal, startDate),
                        cb.lessThanOrEqualTo(tanggal, endDate));
                predicates.add(pBetween);
            } else {
                Predicate pStart = cb.greaterThanOrEqualTo(tanggal, startDate);
                predicates.add(pStart);
            }
        }

        if (!Strings.isNullOrEmpty(end)) {
            Boolean single = Strings.isNullOrEmpty(start);
            Date endDate = dateUtil.getDateTimeEnd(end + "T23:59:59.999Z");
            if (single) {
                Date startDate = dateUtil.getDateTimeStart(end + "T00:00:00.000Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(tanggal, startDate),
                        cb.lessThanOrEqualTo(tanggal, endDate));
                predicates.add(pBetween);
            } else {
                Predicate pEnd = cb.lessThanOrEqualTo(tanggal, endDate);
                predicates.add(pEnd);
            }
        }

        if (!Strings.isNullOrEmpty(namaBerkas)) {
            Predicate pAdd = cb.like(root.get("berkas").get("judul"), "%" + namaBerkas + "%");
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(judulArsip)) {
            Predicate pAdd = cb.like(root.get("perihal"), "%" + judulArsip + "%");
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(noArsip)) {
            Predicate pAdd = cb.like(root.get("nomor"), "%" + noArsip + "%");
            predicates.add(pAdd);
        }

        if (idJenisDokumen != null) {
            Predicate pAdd = cb.equal(root.get("jenisDokumen").get("idJenisDokumen"), idJenisDokumen);
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(idUnit)) {
            Predicate pAdd = cb.equal(root.get("unit").get("id"), idUnit);
            predicates.add(pAdd);
        }

        if (idKArsip != null) {
            Predicate pAdd = cb.equal(root.get("klasifikasiMasalah").get("idKlasifikasiMasalah"), idKArsip);
            predicates.add(pAdd);
        }

        predicates.add(mainCond);

        System.out.println("predicate: " + predicates.toArray().toString());

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);
        
        if (Strings.isNullOrEmpty(sort)) {
            q.orderBy(cb.desc(root.get("modifiedDate")));
        } else {
            Expression exp = root.get(sort);
            if (descending) {
                q.orderBy(cb.desc(exp));
            } else {
                q.orderBy(cb.asc(exp));
            }
        }

        System.out.println("data: " + getEntityManager().createQuery(q).getResultList());

        return getEntityManager().createQuery(q);
    }

    private List<ReportArsipDto> mappingToReportDto(TypedQuery<Arsip> all, Integer skip, Integer limit) {
        List<ReportArsipDto> result = null;

        if (all != null) {
            result = all.getResultStream().map(q -> {
                ReportArsipDto dto = new ReportArsipDto();
                DateUtil dateUtil = new DateUtil();

                dto.setId(q.getId());
                dto.setJudulArsip(q.getPerihal());
                dto.setNomorArsip(q.getNomor());
                dto.setTglNaskah(dateUtil.getCurrentISODate(q.getTglNaskah()));

                if (q.getBerkas() != null) {
                    dto.setNamaBerkas(q.getBerkas().getJudul());
                    
                    if (q.getBerkas().getClosedDate() != null) {
                        dto.setTglCloseFolder(dateUtil.getCurrentISODate(q.getBerkas().getClosedDate()));
                    }

                    if (q.getUnit() != null) {
                        dto.setUnit(q.getUnit().getNama());
                    }
                }

                if (q.getKlasifikasiMasalah() != null) {
                    dto.setKlasifikasiArsip(q.getKlasifikasiMasalah().getKodeKlasifikasiMasalah() + " " + q.getKlasifikasiMasalah().getNamaKlasifikasiMasalah());
                    dto.setTglRetensiAktif(dateUtil.getCurrentISODate(q.getKlasifikasiMasalah().getTglRetensiAktif()));
                    dto.setTglRetensiInAktif(dateUtil.getCurrentISODate(q.getKlasifikasiMasalah().getTglRetensiInaktif()));
                }

                if (q.getJenisDokumen() != null) {
                    dto.setJenisDokumen(q.getJenisDokumen().getKodeJenisDokumen() + " " + q.getJenisDokumen().getNamaJenisDokumen());
                }

                return dto;
            })
                    // .sorted(Comparator.comparing(ReportArsipDto::getId))
                    .skip(skip).limit(limit)
                    .collect(Collectors.toList());
        }
        return result;
    }

    public HashMap<String, Object> getArsipSoonInaktif(String startDate, String endDate, String namaBerkas, String judulArsip, String noArsip, Long idJenisDokumen, String idUnit, Long idKArsip, int skip, int limit, String sort, Boolean descending) {
        List<ReportArsipDto> result = new ArrayList<>();
        TypedQuery<Arsip> all = this.getResultSoonInaktif(startDate, endDate, namaBerkas, judulArsip, noArsip, idJenisDokumen, idUnit, idKArsip, sort, descending);
        result = this.mappingToReportDto(all, skip, limit);

        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    private TypedQuery<Arsip> getResultSoonInaktif(String start, String end, String namaBerkas, String judulArsip, String noArsip, Long idJenisDokumen, String idUnit, Long idKArsip, String sort, Boolean descending) {
        DateUtil dateUtil = new DateUtil();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Arsip> q = cb.createQuery(Arsip.class);
        Root<Arsip> root = q.from(Arsip.class);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted")),
                cb.equal(root.get("berkas").get("unit").get("id"), korsa.getId()),
                cb.isNotNull(root.get("berkas").get("closedDate"))
        );

        Expression<Boolean> isAktif = root.get("berkas").get("isAktif");
        Predicate cond1 = cb.isTrue(isAktif);
        predicates.add(cond1);

        Path<Date> tanggal = (root.<Date>get("tglNaskah"));
        if (!Strings.isNullOrEmpty(start)) {
            Boolean single = Strings.isNullOrEmpty(end);
            Date startDate = dateUtil.getDateTimeStart(start + "T00:00:00.000Z");
            if (single) {
                Date endDate = dateUtil.getDateTimeEnd(start + "T23:59:59.999Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(tanggal, startDate),
                        cb.lessThanOrEqualTo(tanggal, endDate));
                predicates.add(pBetween);
            } else {
                Predicate pStart = cb.greaterThanOrEqualTo(tanggal, startDate);
                predicates.add(pStart);
            }
        }

        if (!Strings.isNullOrEmpty(end)) {
            Boolean single = Strings.isNullOrEmpty(start);
            Date endDate = dateUtil.getDateTimeEnd(end + "T23:59:59.999Z");
            if (single) {
                Date startDate = dateUtil.getDateTimeStart(end + "T00:00:00.000Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(tanggal, startDate),
                        cb.lessThanOrEqualTo(tanggal, endDate));
                predicates.add(pBetween);
            } else {
                Predicate pEnd = cb.lessThanOrEqualTo(tanggal, endDate);
                predicates.add(pEnd);
            }
        }

        if (!Strings.isNullOrEmpty(namaBerkas)) {
            Predicate pAdd = cb.like(root.get("berkas").get("judul"), "%" + namaBerkas + "%");
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(judulArsip)) {
            Predicate pAdd = cb.like(root.get("perihal"), "%" + judulArsip + "%");
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(noArsip)) {
            Predicate pAdd = cb.like(root.get("nomor"), "%" + noArsip + "%");
            predicates.add(pAdd);
        }

        if (idJenisDokumen != null) {
            Predicate pAdd = cb.equal(root.get("jenisDokumen").get("idJenisDokumen"), idJenisDokumen);
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(idUnit)) {
            Predicate pAdd = cb.equal(root.get("unit").get("id"), idUnit);
            predicates.add(pAdd);
        }

        if (idKArsip != null) {
            Predicate pAdd = cb.equal(root.get("klasifikasiMasalah").get("idKlasifikasiMasalah"), idKArsip);
            predicates.add(pAdd);
        }

        predicates.add(mainCond);

        System.out.println("predicate: " + predicates.toArray().toString());

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);
        
        if (Strings.isNullOrEmpty(sort)) {
            q.orderBy(cb.desc(root.get("modifiedDate")));
        } else {
            Expression exp = root.get(sort);
            if (descending) {
                q.orderBy(cb.desc(exp));
            } else {
                q.orderBy(cb.asc(exp));
            }
        }

        System.out.println("data: " + getEntityManager().createQuery(q).getResultList());

        return getEntityManager().createQuery(q);
        // .setFirstResult(skip).setMaxResults(limit);
    }

    public HashMap<String, Object> getArsipInaktifRetensi(String startDate, String endDate, String namaBerkas, String judulArsip, String noArsip, Long idJenisDokumen, String idUnit, Long idKArsip, int skip, int limit, String sort, Boolean descending) {
        List<ReportArsipDto> result = new ArrayList<>();
        TypedQuery<Arsip> all = this.getResultInaktifRetensi(startDate, endDate, namaBerkas, judulArsip, noArsip, idJenisDokumen, idUnit, idKArsip, sort, descending);
        result = this.mappingToReportDto(all, skip, limit);

        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    private TypedQuery<Arsip> getResultInaktifRetensi(String start, String end, String namaBerkas, String judulArsip, String noArsip, Long idJenisDokumen, String idUnit, Long idKArsip, String sort, Boolean descending) {
        DateUtil dateUtil = new DateUtil();
        Date n = new Date();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Arsip> q = cb.createQuery(Arsip.class);
        Root<Arsip> root = q.from(Arsip.class);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted")),
                cb.equal(root.get("berkas").get("unit").get("id"), korsa.getId()),
                cb.lessThanOrEqualTo(root.get("berkas").<Date>get("jraInaktif"), n)
        );

        Expression<Boolean> isAktif = root.get("berkas").get("isAktif");
        Predicate cond1 = cb.isFalse(isAktif);
        predicates.add(cond1);

        Path<Date> tanggal = (root.<Date>get("tglNaskah"));
        if (!Strings.isNullOrEmpty(start)) {
            Boolean single = Strings.isNullOrEmpty(end);
            Date startDate = dateUtil.getDateTimeStart(start + "T00:00:00.000Z");
            if (single) {
                Date endDate = dateUtil.getDateTimeEnd(start + "T23:59:59.999Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(tanggal, startDate),
                        cb.lessThanOrEqualTo(tanggal, endDate));
                predicates.add(pBetween);
            } else {
                Predicate pStart = cb.greaterThanOrEqualTo(tanggal, startDate);
                predicates.add(pStart);
            }
        }

        if (!Strings.isNullOrEmpty(end)) {
            Boolean single = Strings.isNullOrEmpty(start);
            Date endDate = dateUtil.getDateTimeEnd(end + "T23:59:59.999Z");
            if (single) {
                Date startDate = dateUtil.getDateTimeStart(end + "T00:00:00.000Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(tanggal, startDate),
                        cb.lessThanOrEqualTo(tanggal, endDate));
                predicates.add(pBetween);
            } else {
                Predicate pEnd = cb.lessThanOrEqualTo(tanggal, endDate);
                predicates.add(pEnd);
            }
        }

        if (!Strings.isNullOrEmpty(namaBerkas)) {
            Predicate pAdd = cb.like(root.get("berkas").get("judul"), "%" + namaBerkas + "%");
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(judulArsip)) {
            Predicate pAdd = cb.like(root.get("perihal"), "%" + judulArsip + "%");
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(noArsip)) {
            Predicate pAdd = cb.like(root.get("nomor"), "%" + noArsip + "%");
            predicates.add(pAdd);
        }

        if (idJenisDokumen != null) {
            Predicate pAdd = cb.equal(root.get("jenisDokumen").get("idJenisDokumen"), idJenisDokumen);
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(idUnit)) {
            Predicate pAdd = cb.equal(root.get("unit").get("id"), idUnit);
            predicates.add(pAdd);
        }

        if (idKArsip != null) {
            Predicate pAdd = cb.equal(root.get("klasifikasiMasalah").get("idKlasifikasiMasalah"), idKArsip);
            predicates.add(pAdd);
        }

        predicates.add(mainCond);

        System.out.println("predicate: " + predicates.toArray().toString());

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);
        
        if (Strings.isNullOrEmpty(sort)) {
            q.orderBy(cb.desc(root.get("modifiedDate")));
        } else {
            Expression exp = root.get(sort);
            if (descending) {
                q.orderBy(cb.desc(exp));
            } else {
                q.orderBy(cb.asc(exp));
            }
        }

        System.out.println("data: " + getEntityManager().createQuery(q).getResultList());

        return getEntityManager().createQuery(q);
        // .setFirstResult(skip).setMaxResults(limit);
    }

    public HashMap<String, Object> getArsipInaktifMusnah(String startDate, String endDate, String namaBerkas, String judulArsip, String noArsip, Long idJenisDokumen, String idUnit, Long idKArsip, int skip, int limit, String sort, Boolean descending) {
        List<ReportArsipDto> result = new ArrayList<>();
        TypedQuery<Arsip> all = this.getResultInaktifMusnah(startDate, endDate, namaBerkas, judulArsip, noArsip, idJenisDokumen, idUnit, idKArsip, sort, descending);
        result = this.mappingToReportDto(all, skip, limit);

        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    private TypedQuery<Arsip> getResultInaktifMusnah(String start, String end, String namaBerkas, String judulArsip, String noArsip, Long idJenisDokumen, String idUnit, Long idKArsip, String sort, Boolean descending) {
        DateUtil dateUtil = new DateUtil();
        Date n = new Date();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Arsip> q = cb.createQuery(Arsip.class);
        Root<Arsip> root = q.from(Arsip.class);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted")),
                cb.equal(root.get("berkas").get("unit").get("id"), korsa.getId()),
                cb.isNotNull(root.get("berkas").get("rencanaMusnahDate"))
        );

        Expression<Boolean> isAktif = root.get("berkas").get("isAktif");
        Predicate cond1 = cb.isFalse(isAktif);
        predicates.add(cond1);

        Path<Date> tanggal = (root.<Date>get("tglNaskah"));
        if (!Strings.isNullOrEmpty(start)) {
            Boolean single = Strings.isNullOrEmpty(end);
            Date startDate = dateUtil.getDateTimeStart(start + "T00:00:00.000Z");
            if (single) {
                Date endDate = dateUtil.getDateFromISOString_Biasa(start + "T23:59:59.999Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(tanggal, startDate),
                        cb.lessThanOrEqualTo(tanggal, endDate));
                predicates.add(pBetween);
            } else {
                Predicate pStart = cb.greaterThanOrEqualTo(tanggal, startDate);
                predicates.add(pStart);
            }
        }

        if (!Strings.isNullOrEmpty(end)) {
            Boolean single = Strings.isNullOrEmpty(start);
            Date endDate = dateUtil.getDateFromISOString_Biasa(end + "T23:59:59.999Z");
            if (single) {
                Date startDate = dateUtil.getDateTimeStart(end + "T00:00:00.000Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(tanggal, startDate),
                        cb.lessThanOrEqualTo(tanggal, endDate));
                predicates.add(pBetween);
            } else {
                Predicate pEnd = cb.lessThanOrEqualTo(tanggal, endDate);
                predicates.add(pEnd);
            }
        }

        if (!Strings.isNullOrEmpty(namaBerkas)) {
            Predicate pAdd = cb.like(root.get("berkas").get("judul"), "%" + namaBerkas + "%");
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(judulArsip)) {
            Predicate pAdd = cb.like(root.get("perihal"), "%" + judulArsip + "%");
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(noArsip)) {
            Predicate pAdd = cb.like(root.get("nomor"), "%" + noArsip + "%");
            predicates.add(pAdd);
        }

        if (idJenisDokumen != null) {
            Predicate pAdd = cb.equal(root.get("jenisDokumen").get("idJenisDokumen"), idJenisDokumen);
            predicates.add(pAdd);
        }

        if (!Strings.isNullOrEmpty(idUnit)) {
            Predicate pAdd = cb.equal(root.get("unit").get("id"), idUnit);
            predicates.add(pAdd);
        }

        if (idKArsip != null) {
            Predicate pAdd = cb.equal(root.get("klasifikasiMasalah").get("idKlasifikasiMasalah"), idKArsip);
            predicates.add(pAdd);
        }

        predicates.add(mainCond);

        System.out.println("predicate: " + predicates.toArray().toString());

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);
        
        if (Strings.isNullOrEmpty(sort)) {
            q.orderBy(cb.desc(root.get("modifiedDate")));
        } else {
            Expression exp = root.get(sort);
            if (descending) {
                q.orderBy(cb.desc(exp));
            } else {
                q.orderBy(cb.asc(exp));
            }
        }

        System.out.println("data: " + getEntityManager().createQuery(q).getResultList());

        return getEntityManager().createQuery(q);
        // .setFirstResult(skip).setMaxResults(limit);
    }

    private TypedQuery<Arsip> getChartData(Long idJenisDokumen, String idUnit, String start, String end) {
//        Long org = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();
        String korsa = userSession.getUserSession().getUser().getKorsa() != null ? userSession.getUserSession().getUser().getKorsa().getId() : null;
        String id = org.apache.commons.lang3.StringUtils.isNotEmpty(idUnit) ? idUnit : !Strings.isNullOrEmpty(korsa) ? korsa : null;

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Arsip> q = cb.createQuery(Arsip.class);
        Root<Arsip> root = q.from(Arsip.class);

        q.select(root);
        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.isFalse(root.get("isDeleted"));

        Join<Arsip, Berkas> berkas = root.join("berkas", JoinType.INNER);
        Predicate pNotNull = cb.and(
                cb.isFalse(berkas.get("isDeleted")),
                cb.equal(berkas.get("status"), "MUSNAH")
        );
        predicates.add(pNotNull);


        if (idJenisDokumen != null) {
            Predicate pJenisDokumen = cb.equal(root.join("jenisDokumen", JoinType.INNER).get("idJenisDokumen"), idJenisDokumen);
            predicates.add(pJenisDokumen);
        }

        if(!Strings.isNullOrEmpty(id)) {
            Predicate pUnit = cb.equal(root.join("unit", JoinType.INNER).get("id"), id);
            predicates.add(pUnit);
        }

        DateUtil dateUtil = new DateUtil();
        Date dStart = dateUtil.getDateTimeStart(start), dEnd = dateUtil.getDateTimeEnd(end);
        Path<Date> rootDate = berkas.<Date>get("modifiedDate");
        Predicate pBetween
                = cb.and(
                cb.greaterThanOrEqualTo(rootDate, dStart),
                cb.lessThanOrEqualTo(rootDate, dEnd)
        );
        predicates.add(pBetween);

        predicates.add(mainCond);
        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);
        q.orderBy(cb.desc(root.get("modifiedDate")));

        return getEntityManager().createQuery(q);
    }

    public List<ChartDashboardDto> getChartDestroyedDoc(Long idJenisDok, String idUnit, String start, String end) {
        try {
            TypedQuery<Arsip> all = getChartData(idJenisDok, idUnit, start, end);
            return getChartDocData(all);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private List<ChartDashboardDto> getChartDocData(TypedQuery<Arsip> all) {
        try {
            List<Arsip> filtered = all.getResultList();

            Map<String, Long> counted = filtered.stream().map(q -> q.getJenisDokumen().getKodeJenisDokumen()).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            List<ChartDashboardDto> hasil = new ArrayList<>();
            for (String str : counted.keySet()) {
                Long value = counted.get(str);
                ChartDashboardDto dto = new ChartDashboardDto();
                dto.setName(str);
                dto.setValue(value);
                MasterJenisDokumen jenisDokumen = jenisDokumenService.getByCode(str);
                dto.setJenisDokumen(jenisDokumen == null ? "Undefined" : jenisDokumen.getNamaJenisDokumen());
                hasil.add(dto);
            }

            return hasil;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}
