package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.SuratUndangan;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class SuratUndanganService extends AbstractFacade<SuratUndangan> {
    @Override
    protected Class<SuratUndangan> getEntityClass() {
        return SuratUndangan.class;
    }
}
