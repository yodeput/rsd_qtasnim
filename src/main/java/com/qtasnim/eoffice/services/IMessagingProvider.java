package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.MessageType;

import java.util.Map;
import java.util.function.Consumer;

public interface IMessagingProvider {
    void send(MessageType type, Consumer<Map<String, String>> onBuild, String message);
}
