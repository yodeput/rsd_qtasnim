package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterKewenangan;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterKewenanganDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterKewenanganService extends AbstractFacade<MasterKewenangan> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterKewenangan> getEntityClass() {
        return MasterKewenangan.class;
    }

    public JPAJinqStream<MasterKewenangan> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && !q.getIsDeleted());
    }

    public JPAJinqStream<MasterKewenangan> getFiltered(String name) {
        return db().where(q -> q.getKewenangan().toLowerCase().contains(name));
    }

    public void saveOrEdit(Long id, MasterKewenanganDto dao){
        boolean isNew = id==null;
         MasterKewenangan model = new MasterKewenangan();
         DateUtil dateUtil = new DateUtil();

         if(isNew){
             model.setGrade(dao.getGrade());
             model.setKewenangan(dao.getKewenangan());
             if(dao.getCompanyId()!=null){
                 CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                 model.setCompanyCode(cCode);
             } else {
                 model.setCompanyCode(null);
             }
             model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
             if(dao.getEndDate()!=null){
                 model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
             } else {
                 model.setEnd(dateUtil.getDefValue());
             }
             model.setIsDeleted(false);
             this.create(model);
         } else {
             model = this.find(id);
             model.setGrade(dao.getGrade());
             model.setKewenangan(dao.getKewenangan());
             if(dao.getCompanyId()!=null){
                 CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                 model.setCompanyCode(cCode);
             } else {
                 model.setCompanyCode(null);
             }
             model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
             if(dao.getEndDate()!=null){
                 model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
             } else {
                 model.setEnd(dateUtil.getDefValue());
             }
             model.setIsDeleted(false);
             this.edit(model);
         }
     }

     public boolean validate(Long id, String grade, Long companyId){
        boolean isValid = false;
        if (id==null){
            JPAJinqStream<MasterKewenangan> query = db().where(a->!a.getIsDeleted() && a.getCompanyCode().getId().equals(companyId));
            isValid = query.noneMatch(b->b.getGrade().toLowerCase().equals(grade.toLowerCase()));
        }else{
            JPAJinqStream<MasterKewenangan> query = db().where(a->!a.getIsDeleted() && a.getCompanyCode().getId().equals(companyId) && !a.getId().equals(id));
            isValid = query.noneMatch(b->b.getGrade().toLowerCase().equals(grade.toLowerCase()));
        }
        return isValid;
     }
}