package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.TandaTangan;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.TandaTanganDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class TandaTanganService extends AbstractFacade<TandaTangan>{

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterUserService masterUserService;

    @Override
    protected Class<TandaTangan> getEntityClass() {
        return TandaTangan.class;
    }

    public JPAJinqStream<TandaTangan> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && !q.getIsDeleted());
    }

    public HashMap<String,Object> getFiltered(String filter, String user, String start,String end,int skip, int limit, Boolean descending, String sort){
        List<TandaTanganDto> result = new ArrayList<>();
        TypedQuery<TandaTangan> all = getResultList(filter,user,start,end,skip,limit,descending,sort);
        result = MappingToDto(all);
        Long length = all.getResultStream().count();
        HashMap<String,Object> retur = new HashMap<String,Object>();
        retur.put("data",result);
        retur.put("length",length);
        return retur;
    }

    public void saveOrEdit(TandaTanganDto dao, Long id) throws Exception {
        boolean isNew =id==null;
        TandaTangan model = new TandaTangan();
        DateUtil dateUtil = new DateUtil();

        try {
            if (isNew) {
                String type = dao.getTipe().toLowerCase().equals("1") ? "Tandatangan" : "Paraf";
                boolean invalid = validateSign(type, dao.getIdUser());
                if (invalid==false) {
                    model.setTandatangan(Base64.getDecoder().decode(dao.getTandatangan()));
                    model.setTipe(dao.getTipe());
                    MasterUser user = masterUserService.find(dao.getIdUser());
                    model.setUser(user);
                    String kode = this.generatedCode(user, dao.getTipe());
                    model.setKode(kode);

                    if (dao.getCompanyId() != null) {
                        CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                        model.setCompanyCode(cCode);
                    } else {
                        model.setCompanyCode(null);
                    }

                    model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                    if (dao.getEndDate() != null) {
                        model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                    } else {
                        model.setEnd(dateUtil.getDefValue());
                    }
                    model.setIsDeleted(false);
                    this.create(model);
                } else {
                    if(invalid==true) {
                        throw new Exception("Sudah ada tandatangan/paraf yang diregistrasikan");
                    }
                }
            } else {
                model = this.find(id);
                Date n = new Date();
                String endDate = dateUtil.getCurrentISODate(n);
                model.setEnd(dateUtil.getDateFromISOString(endDate));
                model.setIsDeleted(true);
                this.edit(model);
                saveOrEdit(dao, null);
            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    private String generatedCode(MasterUser user, String tipe){
        String lastCode = this.findLastCode(user,tipe);
        Integer count =1;
        String code;
        if(Strings.isNullOrEmpty(lastCode)){
            code = user.getEmployeeId()+tipe+count;
        }else{
            String temp = lastCode.substring(Math.max(lastCode.length() - 1, 0));
            count = Integer.parseInt(temp)+count;
            code = user.getEmployeeId()+tipe+count;
        }

        return code;
    }

    private String findLastCode(MasterUser user, String kode){
        String last="";
        List<String> tmp =  db().where(q->q.getUser().equals(user) && q.getTipe().toLowerCase().equals(kode.toLowerCase())).sortedBy(q->q.getId()).map(a->a.getKode()).collect(Collectors.toList());
        if(!tmp.isEmpty()) {
            last = tmp.get(tmp.size() - 1);
        }
        return last;
    }

    public TypedQuery<TandaTangan> getResultList(String filter, String user,String start,String end,int skip,int limit,Boolean descending,String sort) {
        Date n = new Date();
        DateUtil dateUtil = new DateUtil();

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<TandaTangan> q = cb.createQuery(TandaTangan.class);
        Root<TandaTangan> root = q.from(TandaTangan.class);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.and(
                cb.lessThan(root.<Date>get("start"),n),
                cb.greaterThan(root.<Date>get("end"),n),
                cb.isFalse(root.get("isDeleted"))
        );

        //FilterBy
        if (!Strings.isNullOrEmpty(filter)) {
            Predicate pkeyword = cb.or(
                    cb.like(root.get("tipe"), "%"+filter+"%"),
                    cb.like(root.get("kode"), "%"+filter+"%")
                    );
            predicates.add(pkeyword);
        }

        if (!Strings.isNullOrEmpty(start)) {
            Date startDate = dateUtil.getDateFromISOString(start+"T00:00:00.000Z");
            Predicate pStart = cb.lessThan(root.<Date>get("start"), startDate);
            predicates.add(pStart);
        }

        if (!Strings.isNullOrEmpty(end)) {
            Date endDate = dateUtil.getDateFromISOString(end+"T23:59:59.999Z");
            Predicate pEnd = cb.greaterThan(root.<Date>get("end"), endDate);
            predicates.add(pEnd);
        }

        if(!Strings.isNullOrEmpty(user)){
            Predicate pPejabat = cb.or(
                    cb.like(root.join("user").get("nameFront"),"%"+user+"%"),
                    cb.like(root.join("user").get("nameMiddleLast"),"%"+user+"%"),
                    cb.like(cb.concat(root.join("user").get("nameFront"),
                            cb.concat(" ",root.join("user").get("nameMiddleLast"))),"%"+user+"%")
            );
            predicates.add(pPejabat);
        }

        // WHERE CONDITION
        predicates.add(mainCond);


        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);

        if (descending != null && !Strings.isNullOrEmpty(sort)) {
            if (descending) {
                q.orderBy(cb.desc(root.get(sort)));
            } else {
                q.orderBy(cb.asc(root.get(sort)));
            }
        }

        return getEntityManager().createQuery(q).setFirstResult(skip).setMaxResults(limit);
    }

    private List<TandaTanganDto> MappingToDto(TypedQuery<TandaTangan> all) {
        List<TandaTanganDto> result = null;

        if (all != null) {
            result = all.getResultList().stream().map(TandaTanganDto::new).collect(Collectors.toList());
        }
        return result;
    }

    private boolean validateSign(String tipe,Long idUser){
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && !q.getIsDeleted()).anyMatch(a->a.getTipe().toLowerCase()
                .equals(tipe.toLowerCase()) && a.getUser().getId().equals(idUser));
    }
}
