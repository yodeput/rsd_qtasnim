package com.qtasnim.eoffice.services;

import com.google.gson.Gson;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.*;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class RegistrasiDokumenEksternalService extends AbstractFacade<Surat> {

    @Inject
    private FormFieldService fieldService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private FormDefinitionService formDefinitionService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private PemeriksaSuratService pemeriksaSuratService;

    @Inject
    private PenandatanganSuratService penandatanganSuratService;

    @Inject
    private TembusanSuratService tembusanSuratService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private ParaService paraService;

    @Inject
    private ParaDetailService paraDetailService;

    @Inject
    private TembusanParaService tembusanParaService;

    @Inject
    private PenerimaSuratService penerimaSuratService;

    @Inject
    private PenerimaParaService penerimaParaService;

    @Inject
    private ReferensiSuratService referensiSuratService;

    @Inject
    private ReferensiSuratDetailService referensiSuratDetailService;

    @Inject
    private SuratService suratService;

    @Inject
    private WorkflowService workflowService;

    @Inject
    private MasterAutoNumberService autoNumberService;

    @Override
    protected Class<Surat> getEntityClass() {
        return Surat.class;
    }

    public SuratDto saveOrEdit(Long id, SuratDto dao) throws Exception {
        SuratDto result;
        try {
            boolean isNew = id == null;
            Surat obj = new Surat();
            List<FieldDataDto> tmp = new ArrayList<>();

            for (MetadataValueDto ff : dao.getMetadata()) {
                Gson g = new Gson();
                if(ff.getFieldData()!=null && ff.getFieldId()!=null) {
                    FormField objF = fieldService.find(ff.getFieldId());
                    objF.setData(ff.getFieldData());
                    fieldService.edit(objF);
                    String json = ff.getFieldData().replace("\\", "");
                    FieldDataDto field = g.fromJson(json, FieldDataDto.class);
                    field.setName(ff.getName());
                    tmp.add(field);
                }
            }

            if (isNew) {
                /*Save Surat*/
                obj.setIsDeleted(dao.getIsDeleted());
                obj.setKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
                FormDefinition form = formDefinitionService.find(dao.getIdFormDefinition());
                obj.setFormDefinition(form);
                obj.setStatus(dao.getStatus());
                obj.setCompanyCode(dao.getIdCompany() != null ? companyCodeService.find(dao.getIdCompany()) : null);
                obj.setIsPemeriksaParalel(dao.getIsPemeriksaParalel());
                this.create(obj);
                getEntityManager().flush();
                /*end save surat*/

                /*save pemeriksa*/
                if (!dao.getPemeriksaSurat().isEmpty()) {
                    for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
                        PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
                        pemeriksaSurat.setOrganization(pemeriksa);
                        pemeriksaSurat.setSeq(pem.getSeq());
                        pemeriksaSurat.setSurat(obj);
                        pemeriksaSuratService.create(pemeriksaSurat);
                    }
                }
                /*end save pemeriksa*/


                /*save tembusan*/
                if (!dao.getTembusanSurat().isEmpty()) {
                    for (TembusanSuratDto tembus : dao.getTembusanSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi tembusan = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        TembusanSurat tembusanSurat = new TembusanSurat();
                        if (tembus.getIdOrganization() != null) {
                            tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setOrganization(tembusan);
                            tembusanSuratService.create(tembusanSurat);
                        } else {
                            paraObj = paraService.find(tembus.getIdPara());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setPara(paraObj);
                            tembusanSuratService.create(tembusanSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    TembusanPara tp = new TembusanPara();
                                    tp.setOrganisasi(pdd.getOrganisasi());
                                    tp.setTembusanSurat(tembusanSurat);
                                    tembusanParaService.create(tp);
                                }
                            }
                        }
                    }
                }
                /*end save tembusan*/

                /*save penerima*/
                if (!dao.getPenerimaSurat().isEmpty()) {
                    for (PenerimaSuratDto terima : dao.getPenerimaSurat().stream().distinct().collect(Collectors.toList())) {
                        PenerimaSurat penerimaSurat = new PenerimaSurat();
                        MasterStrukturOrganisasi penerima = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        if (terima.getIdOrganization() != null) {
                            penerima = masterStrukturOrganisasiService.find(terima.getIdOrganization());
                            penerimaSurat.setOrganization(penerima);
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSuratService.create(penerimaSurat);
                        } else {
                            paraObj = paraService.find(terima.getIdPara());
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSurat.setPara(paraObj);
                            penerimaSuratService.create(penerimaSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    PenerimaPara pp = new PenerimaPara();
                                    pp.setOrganisasi(pdd.getOrganisasi());
                                    pp.setPenerimaSurat(penerimaSurat);
                                    penerimaParaService.create(pp);
                                }
                            }
                        }
                    }
                }
                /*end save penerima*/
            } else {
                obj = this.find(id);
                /*Save Surat*/
                obj.setIsDeleted(dao.getIsDeleted());
                obj.setKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
                FormDefinition form = formDefinitionService.find(dao.getIdFormDefinition());
                obj.setFormDefinition(form);
                obj.setCompanyCode(dao.getIdCompany() != null ? companyCodeService.find(dao.getIdCompany()) : null);
                obj.setIsPemeriksaParalel(dao.getIsPemeriksaParalel());
                this.edit(obj);
                getEntityManager().flush();
                /*end save surat*/

                /*save pemeriksa*/
                if (!dao.getPemeriksaSurat().isEmpty()) {
                    List<PemeriksaSurat> rmvPemeriksa = pemeriksaSuratService.getAllBy(id);
                    if (!rmvPemeriksa.isEmpty()) {
                        for (PemeriksaSurat ps : rmvPemeriksa) {
                            pemeriksaSuratService.remove(ps);
                        }
                    }

                    for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
                        PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
                        pemeriksaSurat.setOrganization(pemeriksa);
                        pemeriksaSurat.setSeq(pem.getSeq());
                        pemeriksaSurat.setSurat(obj);
                        pemeriksaSuratService.create(pemeriksaSurat);
                    }
                }
                /*end save pemeriksa*/

                /*save tembusan*/
                if (!dao.getTembusanSurat().isEmpty()) {
                    List<TembusanSurat> rmvTembusan = tembusanSuratService.getAllBy(id);
                    if (!rmvTembusan.isEmpty()) {
                        for (TembusanSurat bs : rmvTembusan) {
                            List<TembusanPara> tp = tembusanParaService.getListTembusanPara(bs.getId());
                            if (tp.isEmpty()) {
                                tembusanSuratService.remove(bs);
                            } else {
                                for (TembusanPara ctr : tp) {
                                    tembusanParaService.remove(ctr);
                                }
                                tembusanSuratService.remove(bs);
                            }
                        }
                    }

                    for (TembusanSuratDto tembus : dao.getTembusanSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi tembusan = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        TembusanSurat tembusanSurat = new TembusanSurat();
                        if (tembus.getIdOrganization() != null) {
                            tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setOrganization(tembusan);
                            tembusanSuratService.create(tembusanSurat);
                        } else {
                            paraObj = paraService.find(tembus.getIdPara());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setPara(paraObj);
                            tembusanSuratService.create(tembusanSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    TembusanPara tp = new TembusanPara();
                                    tp.setOrganisasi(pdd.getOrganisasi());
                                    tp.setTembusanSurat(tembusanSurat);
                                    tembusanParaService.create(tp);
                                }
                            }
                        }
                    }
                }
                /*end save tembusan*/

                /*save penerima*/
                if (!dao.getPenerimaSurat().isEmpty()) {
                    List<PenerimaSurat> rmvPenerima = penerimaSuratService.getAllBy(id);
                    if (!rmvPenerima.isEmpty()) {
                        for (PenerimaSurat bs : rmvPenerima) {
                            List<PenerimaPara> tp = penerimaParaService.getPenerimaParaList(bs.getId());
                            if (tp.isEmpty()) {
                                penerimaSuratService.remove(bs);
                            } else {
                                for (PenerimaPara ctr : tp) {
                                    penerimaParaService.remove(ctr);
                                }
                                penerimaSuratService.remove(bs);
                            }
                        }
                    }
                    for (PenerimaSuratDto terima : dao.getPenerimaSurat().stream().distinct().collect(Collectors.toList())) {
                        PenerimaSurat penerimaSurat = new PenerimaSurat();
                        MasterStrukturOrganisasi penerima = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        if (terima.getIdOrganization() != null) {
                            penerima = masterStrukturOrganisasiService.find(terima.getIdOrganization());
                            penerimaSurat.setOrganization(penerima);
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSuratService.create(penerimaSurat);
                        } else {
                            paraObj = paraService.find(terima.getIdPara());
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSurat.setPara(paraObj);
                            penerimaSuratService.create(penerimaSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    PenerimaPara pp = new PenerimaPara();
                                    pp.setOrganisasi(pdd.getOrganisasi());
                                    pp.setPenerimaSurat(penerimaSurat);
                                    penerimaParaService.create(pp);
                                }
                            }
                        }
                    }
                }
                /*end save penerima*/
            }

            if (dao.getReferensiId() != null && !dao.getReferensiId().isEmpty()) {
                for(Long a:dao.getReferensiId()){
                    ReferensiSurat referensiSurat = referensiSuratService.find(a);
                    ReferensiSuratDetail referensiSuratDetail = new ReferensiSuratDetail();
                    referensiSuratDetail.setSurat(obj);
                    referensiSuratDetail.setReferensiSurat(referensiSurat);
                    referensiSuratDetailService.create(referensiSuratDetail);
                }
            }

            formDefinitionService.saveDynamicForm(dao, obj, isNew, this);

            String jenisDokumen = "";
            try {
                jenisDokumen = obj.getFormDefinition().getJenisDokumen().getKodeJenisDokumen();
            } catch (Exception ex) { jenisDokumen = ""; }
            String number = autoNumberService.from(Surat.class).next(t -> t.getNoDokumen(), obj);
            obj.setNoDokumen(number);
            this.edit(obj);
            getEntityManager().flush();
            getEntityManager().refresh(obj);

            result = new SuratDto(obj);

            return result;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public void delete(Long id) {
       try{
            Surat surat = this.find(id);
            surat.setIsDeleted(true);
            suratService.edit(surat);
       }catch (Exception ex){
           ex.printStackTrace();
       }
    }

    public JPAJinqStream<Surat> getDraft() {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        return db().where(q -> q.getKonseptor().equals(org) && !q.getIsDeleted()
                && q.getStatus().equals("DRAFT") && q.getFormDefinition().getJenisDokumen().getIsKorespondensi().booleanValue()==false);
    }

    public SuratDto submit(SuratDto dao) throws Exception {
        SuratDto result;
        try {
            boolean isNew = dao.getId() == null;
            Surat obj = new Surat();
            List<FieldDataDto> tmp = new ArrayList<>();
            String workflowStatus = "DRAFT";

            for (MetadataValueDto ff : dao.getMetadata()) {
                Gson g = new Gson();
                if(ff.getFieldData()!=null && ff.getFieldId()!=null) {
                    FormField objF = fieldService.find(ff.getFieldId());
                    objF.setData(ff.getFieldData());
                    fieldService.edit(objF);
                    String json = ff.getFieldData().replace("\\", "");
                    FieldDataDto field = g.fromJson(json, FieldDataDto.class);
                    field.setName(ff.getName());
                    tmp.add(field);
                }
            }

            if (isNew) {
                /*Save Surat*/
                obj.setIsDeleted(dao.getIsDeleted());
                obj.setKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
                FormDefinition form = formDefinitionService.find(dao.getIdFormDefinition());
                obj.setFormDefinition(form);
                obj.setStatus(dao.getStatus());
                obj.setCompanyCode(dao.getIdCompany() != null ? companyCodeService.find(dao.getIdCompany()) : null);
                obj.setIsPemeriksaParalel(dao.getIsPemeriksaParalel());
                this.create(obj);
                getEntityManager().flush();
                /*end save surat*/

                /*save pemeriksa*/
                if (!dao.getPemeriksaSurat().isEmpty()) {
                    for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
                        PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
                        pemeriksaSurat.setOrganization(pemeriksa);
                        pemeriksaSurat.setSeq(pem.getSeq());
                        pemeriksaSurat.setSurat(obj);
                        pemeriksaSuratService.create(pemeriksaSurat);
                    }
                }
                /*end save pemeriksa*/


                /*save tembusan*/
                if (!dao.getTembusanSurat().isEmpty()) {
                    for (TembusanSuratDto tembus : dao.getTembusanSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi tembusan = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        TembusanSurat tembusanSurat = new TembusanSurat();
                        if (tembus.getIdOrganization() != null) {
                            tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setOrganization(tembusan);
                            tembusanSuratService.create(tembusanSurat);
                        } else {
                            paraObj = paraService.find(tembus.getIdPara());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setPara(paraObj);
                            tembusanSuratService.create(tembusanSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    TembusanPara tp = new TembusanPara();
                                    tp.setOrganisasi(pdd.getOrganisasi());
                                    tp.setTembusanSurat(tembusanSurat);
                                    tembusanParaService.create(tp);
                                }
                            }
                        }
                    }
                }
                /*end save tembusan*/

                /*save penerima*/
                if (!dao.getPenerimaSurat().isEmpty()) {
                    for (PenerimaSuratDto terima : dao.getPenerimaSurat().stream().distinct().collect(Collectors.toList())) {
                        PenerimaSurat penerimaSurat = new PenerimaSurat();
                        MasterStrukturOrganisasi penerima = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        if (terima.getIdOrganization() != null) {
                            penerima = masterStrukturOrganisasiService.find(terima.getIdOrganization());
                            penerimaSurat.setOrganization(penerima);
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSuratService.create(penerimaSurat);
                        } else {
                            paraObj = paraService.find(terima.getIdPara());
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSurat.setPara(paraObj);
                            penerimaSuratService.create(penerimaSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    PenerimaPara pp = new PenerimaPara();
                                    pp.setOrganisasi(pdd.getOrganisasi());
                                    pp.setPenerimaSurat(penerimaSurat);
                                    penerimaParaService.create(pp);
                                }
                            }
                        }
                    }
                }
                /*end save penerima*/
            } else {
                obj = this.find(dao.getId());
                /*Save Surat*/
                obj.setIsDeleted(dao.getIsDeleted());
                obj.setKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
                FormDefinition form = formDefinitionService.find(dao.getIdFormDefinition());
                obj.setFormDefinition(form);
                obj.setStatus(dao.getStatus());
                obj.setCompanyCode(dao.getIdCompany() != null ? companyCodeService.find(dao.getIdCompany()) : null);
                obj.setIsPemeriksaParalel(dao.getIsPemeriksaParalel());
                this.edit(obj);
                getEntityManager().flush();
                /*end save surat*/

                /*save pemeriksa*/
                if (!dao.getPemeriksaSurat().isEmpty()) {
                    List<PemeriksaSurat> rmvPemeriksa = pemeriksaSuratService.getAllBy(dao.getId());
                    if (!rmvPemeriksa.isEmpty()) {
                        for (PemeriksaSurat ps : rmvPemeriksa) {
                            pemeriksaSuratService.remove(ps);
                        }
                    }

                    for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
                        PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
                        pemeriksaSurat.setOrganization(pemeriksa);
                        pemeriksaSurat.setSeq(pem.getSeq());
                        pemeriksaSurat.setSurat(obj);
                        pemeriksaSuratService.create(pemeriksaSurat);
                    }
                }
                /*end save pemeriksa*/

                /*save tembusan*/
                if (!dao.getTembusanSurat().isEmpty()) {
                    List<TembusanSurat> rmvTembusan = tembusanSuratService.getAllBy(dao.getId());
                    if (!rmvTembusan.isEmpty()) {
                        for (TembusanSurat bs : rmvTembusan) {
                            List<TembusanPara> tp = tembusanParaService.getListTembusanPara(bs.getId());
                            if (tp.isEmpty()) {
                                tembusanSuratService.remove(bs);
                            } else {
                                for (TembusanPara ctr : tp) {
                                    tembusanParaService.remove(ctr);
                                }
                                tembusanSuratService.remove(bs);
                            }
                        }
                    }

                    for (TembusanSuratDto tembus : dao.getTembusanSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi tembusan = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        TembusanSurat tembusanSurat = new TembusanSurat();
                        if (tembus.getIdOrganization() != null) {
                            tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setOrganization(tembusan);
                            tembusanSuratService.create(tembusanSurat);
                        } else {
                            paraObj = paraService.find(tembus.getIdPara());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setPara(paraObj);
                            tembusanSuratService.create(tembusanSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    TembusanPara tp = new TembusanPara();
                                    tp.setOrganisasi(pdd.getOrganisasi());
                                    tp.setTembusanSurat(tembusanSurat);
                                    tembusanParaService.create(tp);
                                }
                            }
                        }
                    }
                }
                /*end save tembusan*/

                /*save penerima*/
                if (!dao.getPenerimaSurat().isEmpty()) {
                    List<PenerimaSurat> rmvPenerima = penerimaSuratService.getAllBy(dao.getId());
                    if (!rmvPenerima.isEmpty()) {
                        for (PenerimaSurat bs : rmvPenerima) {
                            List<PenerimaPara> tp = penerimaParaService.getPenerimaParaList(bs.getId());
                            if (tp.isEmpty()) {
                                penerimaSuratService.remove(bs);
                            } else {
                                for (PenerimaPara ctr : tp) {
                                    penerimaParaService.remove(ctr);
                                }
                                penerimaSuratService.remove(bs);
                            }
                        }
                    }
                    for (PenerimaSuratDto terima : dao.getPenerimaSurat().stream().distinct().collect(Collectors.toList())) {
                        PenerimaSurat penerimaSurat = new PenerimaSurat();
                        MasterStrukturOrganisasi penerima = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        if (terima.getIdOrganization() != null) {
                            penerima = masterStrukturOrganisasiService.find(terima.getIdOrganization());
                            penerimaSurat.setOrganization(penerima);
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSuratService.create(penerimaSurat);
                        } else {
                            paraObj = paraService.find(terima.getIdPara());
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSurat.setPara(paraObj);
                            penerimaSuratService.create(penerimaSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    PenerimaPara pp = new PenerimaPara();
                                    pp.setOrganisasi(pdd.getOrganisasi());
                                    pp.setPenerimaSurat(penerimaSurat);
                                    penerimaParaService.create(pp);
                                }
                            }
                        }
                    }
                }
                /*end save penerima*/
            }

            if (dao.getReferensiId() != null && !dao.getReferensiId().isEmpty()) {
                for(Long a:dao.getReferensiId()){
                    ReferensiSurat referensiSurat = referensiSuratService.find(a);
                    ReferensiSuratDetail referensiSuratDetail = new ReferensiSuratDetail();
                    referensiSuratDetail.setSurat(obj);
                    referensiSuratDetail.setReferensiSurat(referensiSurat);
                    referensiSuratDetailService.create(referensiSuratDetail);
                }
            }

            formDefinitionService.saveDynamicForm(dao, obj, isNew, this);

            String jenisDokumen = "";
            try {
                jenisDokumen = obj.getFormDefinition().getJenisDokumen().getKodeJenisDokumen();
            } catch (Exception ex) { jenisDokumen = ""; }
            String number = autoNumberService.from(Surat.class).next(t -> t.getNoDokumen(), obj);
            obj.setNoDokumen(number);
            this.edit(obj);
            getEntityManager().flush();
            getEntityManager().refresh(obj);

            result = new SuratDto(obj);
            result.setWorkflowAction(dao.getWorkflowAction());
            workflowService.execute(result, workflowStatus, obj, this);

            return result;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public Long generateSubmittedDate(Long id) {
        Long idSurat = null;
        try {
            Surat surat = this.find(id);
            String jenisDokumen = "";
            try {
                jenisDokumen = surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen();
            } catch (Exception ex) { jenisDokumen = ""; }

            surat.setSubmittedDate(new Date());
            this.edit(surat);

            return surat.getId();
        } catch (Exception ex) {
            return null;
        }
    }
}
