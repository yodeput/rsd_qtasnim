package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterPenciptaArsip;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterPenciptaArsipDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterPenciptaArsipService extends AbstractFacade<MasterPenciptaArsip> {
    private static final long serialVersionUID = 1L;
    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterPenciptaArsip> getEntityClass() {
        return MasterPenciptaArsip.class;
    }

    public JPAJinqStream<MasterPenciptaArsip> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }


    public JPAJinqStream<MasterPenciptaArsip> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name));
    }


    public MasterPenciptaArsip save(Long id, MasterPenciptaArsipDto dao) {
        MasterPenciptaArsip model = new MasterPenciptaArsip();
        if (id == null) {
            this.create(data(model, dao));
        } else {
            model = this.find(id);
            this.edit(data(model, dao));
        }
        return model;
    }

    public MasterPenciptaArsip data(MasterPenciptaArsip masterPenciptaArsip, MasterPenciptaArsipDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterPenciptaArsip model = masterPenciptaArsip;
        model.setNama(dao.getNama());
        model.setDescription(dao.getDescription());
        if (dao.getCompanyId() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }
        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        return model;
    }

    public MasterPenciptaArsip delete(Long id) {
        MasterPenciptaArsip model = this.find(id);
        this.remove(model);
        return model;
    }


    public MasterPenciptaArsip findByNama(String value) {
        return db().where(t -> t.getNama().equals(value)).findFirst().orElse(null);
    }
}