package com.qtasnim.eoffice.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@LocalBean
@Stateless
public class DigitalSignatureService {
    public byte[] sign(byte[] pdfContent) {
        return pdfContent;
    }
}
