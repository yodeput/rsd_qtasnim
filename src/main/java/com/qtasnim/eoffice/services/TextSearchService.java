package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.Arsip;
import com.qtasnim.eoffice.db.Berkas;
import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.ws.dto.GlobalAttachmentDto;
import com.qtasnim.eoffice.ws.dto.TextSearchAttachsDetailDto;
import com.qtasnim.eoffice.ws.dto.TextSearchAttachsDto;
import com.qtasnim.eoffice.ws.dto.TextSearchGeneralDto;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Stateless
@LocalBean
public class TextSearchService {
    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private ArsipService arsipService;

//    @Inject
//    private SuratService suratService;

    @Inject
    private TextExtractionService textExtractionService;

    public List<TextSearchGeneralDto> findTextInArsip(String query) {
        List<TextSearchGeneralDto> result = new LinkedList<>();

        if(StringUtils.isBlank(query)) return result;

        try {
            List<Long> referenceIds = globalAttachmentService.findReferenceIdByTextExtracted(query, "t_arsip");

            List<Arsip> arsips = arsipService.filterByArsipIds(referenceIds);

            arsips.forEach(a -> {
                Berkas berkas = a.getBerkas();

                TextSearchGeneralDto textSearchGeneralDto = new TextSearchGeneralDto();

                textSearchGeneralDto.setJenisDokumen(a.getJenisDokumen().getNamaJenisDokumen());
                textSearchGeneralDto.setNomorArsip(a.getNomor());
                textSearchGeneralDto.setFolderName(berkas.getFolderName());
                textSearchGeneralDto.setNomorBerkas(berkas.getNomor());
                textSearchGeneralDto.setJudulBerkas(berkas.getJudul());

                result.add(textSearchGeneralDto);
            });

//            referenceIds = globalAttachmentService.findReferenceIdByTextExtracted(query, "t_surat");
//
//            List<Surat>
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public TextSearchAttachsDto findTextInAttachments(String query) {
        TextSearchAttachsDto result = new TextSearchAttachsDto();

        if(StringUtils.isBlank(query)) return result;

        try {
            List<GlobalAttachmentDto> attachments = globalAttachmentService.findAttachmentByTextExtracted(query);

            result.setAttachments(attachments);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public TextSearchAttachsDetailDto findTextInPage(Long idAttachment, String query) {
        TextSearchAttachsDetailDto detail = new TextSearchAttachsDetailDto();

        if(StringUtils.isBlank(query) || idAttachment == null) return detail;

        try {
            GlobalAttachment attachmentDto = globalAttachmentService.find(idAttachment);

            List<String> pages = textExtractionService.deserializeExtractedText(attachmentDto.getExtractedText());

            List<Integer> pagesNo = new LinkedList<>();

            for (int i = 1; i <= pages.size(); i++) {
                if (pages.get(i - 1).toLowerCase().contains(query.toLowerCase())) {
                    pagesNo.add(i);
                }
            }

            detail.setPages(pagesNo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return detail;
    }
}
