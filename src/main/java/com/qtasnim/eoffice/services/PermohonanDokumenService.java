package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.workflows.*;
import com.qtasnim.eoffice.ws.dto.*;

import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Stateless
@LocalBean
public class PermohonanDokumenService extends AbstractFacade<PermohonanDokumen> implements IWorkflowService<PermohonanDokumen> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private ArsipService arsipService;

    @Inject
    private MasterKorsaService korsaService;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private SuratService suratService;

    @Inject
    private PenerimaPermohonanDokumenService penerimaPermohonanDokumenService;

    @Inject
    private TembusanPermohonanDokumenService tembusanPermohonanDokumenService;

    @Inject
    private MasterStrukturOrganisasiService orgService;

    @Inject
    private ProcessDefinitionService processDefinitionService;

    @Inject
    private ProcessInstanceService processInstanceService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private WorkflowService workflowService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private UserTaskService userTaskService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private DokumenHistoryService docHistoryService;

    @Override
    protected Class<PermohonanDokumen> getEntityClass() {
        return PermohonanDokumen.class;
    }

    //#region PENDING TASKQUERY
    public List<PermohonanDokumen> getPendingTasksResult(String filterBy, String sortedField, boolean descending, int skip, int limit) {
        return getPendingTasksResult(filterBy, sortedField, descending, skip, limit, null);
    }

    public List<PermohonanDokumen> getPendingTasksResult(String filterBy, String sortedField, boolean descending, int skip, int limit, Long idOrganisasi) {
        try {
            return this.getPendingTasksQuery(filterBy, sortedField, descending, idOrganisasi).setFirstResult(skip).setMaxResults(limit).getResultList();
        } catch (Exception ex) {
            return new ArrayList<>();
        }

    }

    public Long getTypedQueryCount(String filterBy, String sortedField, boolean descending, Long idOrganisasi) {
        try {
            return Long.valueOf(this.getPendingTasksQuery(filterBy, sortedField, descending, idOrganisasi).getResultStream().count());
        } catch (Exception ex) {
            return 0L;
        }

    }

    public TypedQuery<PermohonanDokumen> getPendingTasksQuery(String filterBy, String sortedField, boolean descending, Long idOrganisasi) {
        try {
            String username = userSession.getUserSession().getUser().getLoginUserName().toLowerCase();
            Long entityId = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();
            String entityCode = userSession.getUserSession().getUser().getOrganizationEntity().getOrganizationCode();

            Long allowedEntityId = masterDelegasiService.getByPenggantiIdList(Stream.of(entityCode).collect(Collectors.toList())).stream().map(q -> q.getFrom().getIdOrganization()).findFirst().orElse(entityId);

            List<Long> pejabat = new ArrayList<>();
            pejabat.add(entityId);
            if (allowedEntityId != null) {
                pejabat.add(allowedEntityId);
            }

            // Exec Query
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<PermohonanDokumen> q = cb.createQuery(PermohonanDokumen.class);
            Root<PermohonanDokumen> root = q.from(PermohonanDokumen.class);

            // Join<PermohonanDokumen, ProcessTask> pemohon = root.join("pemohon", JoinType.LEFT);
            //#region Sub Query
            Subquery<ProcessTask> sq = q.subquery(ProcessTask.class);
            Root<ProcessTask> rootProcessTask = sq.from(ProcessTask.class);

            Join<ProcessTask, ProcessInstance> rootProcessInstance = rootProcessTask.join("processInstance", JoinType.INNER);

            Expression recordRefId_exp = rootProcessInstance.get("recordRefId").as(Long.class); // String to Long
            sq.select(recordRefId_exp);

            List<Predicate> sqPredicates = new ArrayList<>();
            Predicate sqPredicate_1 = cb.equal(rootProcessInstance.get("relatedEntity"), "com.qtasnim.eoffice.db.PermohonanDokumen");
//            Predicate sqPredicate_2 = rootProcessTask.join("assignee").get("idOrganization").in(pejabat);
            if (idOrganisasi != null) {
                entityId = idOrganisasi;
            }
            Predicate sqPredicate_2 = cb.equal(rootProcessTask.join("assignee").get("idOrganization"), entityId);
            Predicate sqPredicate_3 = cb.equal(rootProcessTask.get("executionStatus"), ExecutionStatus.PENDING);

            sqPredicates.add(sqPredicate_1);
            sqPredicates.add(sqPredicate_2);
            sqPredicates.add(sqPredicate_3);
            sq.where(sqPredicates.toArray(new Predicate[sqPredicates.size()]));
            //#endregion Sub Query

            q.select(root).distinct(true);

            List<Predicate> predicates = new ArrayList<>();
            Predicate pInTasks = cb.or(
                    root.get("id").in(sq),
                    cb.and(
                            cb.equal(root.get("status"), "SUBMITTED"),
                            cb.equal(root.join("penerima", JoinType.LEFT).get("organization").get("idOrganization"), entityId)
                    )
            );
            Predicate pIsDeleted = cb.isFalse(root.get("isDeleted"));

            // Predicate pAssignee = 
            //     cb.or(
            //         cb.equal(root.get("createdBy"), username), 
            //         cb.or(
            //             pemohon.get("id").in(allowedEntityId),
            //             cb.or(
            //                 pengelola1.get("id").in(allowedEntityId),
            //                 pengelola2.get("id").in(allowedEntityId)
            //             )
            //         )
            //     );
            if (!Strings.isNullOrEmpty(filterBy)) {
                Predicate pkeyword
                        = cb.or(
                                cb.like(root.get("perihal"), "%" + filterBy + "%"),
                                cb.like(root.get("keperluan"), "%" + filterBy + "%")
                        );
                predicates.add(pkeyword);
            }

            // WHERE CONDITION
            // predicates.add(cb.and(pInTasks, pIsDeleted));
            predicates.add(pIsDeleted);
            predicates.add(pInTasks);
            // predicates.add(pAssignee);

            q.where(predicates.toArray(new Predicate[predicates.size()]));

            if (Strings.isNullOrEmpty(sortedField)) {
                sortedField = "createdDate";
            }
            if (descending) {
                q.orderBy(cb.desc(root.get(sortedField)));
            } else {
                q.orderBy(cb.asc(root.get(sortedField)));
            }

            return getEntityManager().createQuery(q);//.setFirstResult(skip).setMaxResults(limit).getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    //#endregion

    //#region DataList
    public List<PermohonanDokumen> execCriteriaQuery(CriteriaQuery<PermohonanDokumen> criteriaQuery, int skip, int limit) {
        try {
            return getEntityManager().createQuery(criteriaQuery).setFirstResult(skip).setMaxResults(limit).getResultList();
        } catch (Exception ex) {
            return new ArrayList<>();
        }

    }

    public Long countCriteriaQuery(CriteriaQuery<PermohonanDokumen> criteriaQuery) {
        try {
            return getEntityManager().createQuery(criteriaQuery).getResultStream().count();
        } catch (Exception ex) {
            return 0L;
        }

    }

    public CriteriaQuery<PermohonanDokumen> getAllQuery(String filterBy, String tipeLayanan, String sortedField, boolean descending) {
        return getAllQuery(filterBy, tipeLayanan, sortedField, descending, null);
    }

    public CriteriaQuery<PermohonanDokumen> getAllQuery(String filterBy, String tipeLayanan, String sortedField, boolean descending, Long idOrganisasi) {
        try {
            String username = "";
            Long entityId = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();

            if (idOrganisasi == null) {
                /* ketika id organisasi == null maka kebutuhan query dari session */
                idOrganisasi = entityId;
                username = userSession.getUserSession().getUser().getLoginUserName().toLowerCase();
            }else{
                if(!idOrganisasi.equals(entityId)){
                    /*Ketika kondisi idOrganisasi yang dilemparkan tidak sama dengan user yg login*/
                    MasterStrukturOrganisasi posisi = orgService.getByIdOrganisasi(idOrganisasi);
                    if(posisi.getUser()!=null){
                        username = posisi.getUser().getLoginUserName().toLowerCase();
                    }
                }else{
                    username = userSession.getUserSession().getUser().getLoginUserName().toLowerCase();
                }
            }

            // Exec Query
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<PermohonanDokumen> q = cb.createQuery(PermohonanDokumen.class);
            Root<PermohonanDokumen> root = q.from(PermohonanDokumen.class);

            Join<PermohonanDokumen, PermohonanDokumenPenerima> penerima = root.join("penerima", JoinType.LEFT);
            Join<PermohonanDokumen, PermohonanDokumenTembusan> tembusan = root.join("tembusan", JoinType.LEFT);

            q.select(root);

            List<Predicate> predicates = new ArrayList<>();
            Predicate pIsDeleted = cb.isFalse(root.get("isDeleted"));

            Predicate pAssignee
                    = cb.or(
                            cb.like(root.get("createdBy"), "%" + username + "%"),
                            cb.or(
                                    cb.and(
                                            cb.notEqual(root.get("status"), "DRAFT"),
                                            cb.equal(penerima.get("organization").get("idOrganization"), (idOrganisasi))
                                    ),
                                    cb.and(
                                            cb.or(
                                                    cb.equal(root.get("status"), "APPROVED"),
                                                    cb.equal(root.get("status"), "RETURNED")
                                            ),
                                            cb.equal(tembusan.get("organization").get("idOrganization"), (idOrganisasi))
                                    )
                            )
                    );

            if (!Strings.isNullOrEmpty(filterBy)) {
                Predicate pkeyword
                        = cb.or(
                                cb.like(root.get("perihal"), "%" + filterBy + "%"),
                                cb.like(root.get("keperluan"), "%" + filterBy + "%")
                        );
                predicates.add(pkeyword);
            }

            if (!Strings.isNullOrEmpty(tipeLayanan)) {
                Predicate ptipeLayanan = cb.equal(root.get("tipeLayanan"), tipeLayanan);
                predicates.add(ptipeLayanan);
            }

            // WHERE CONDITION
            predicates.add(pIsDeleted);
            predicates.add(pAssignee);

            q.where(predicates.toArray(new Predicate[predicates.size()]));
            q.distinct(true);

            if (Strings.isNullOrEmpty(sortedField)) {
                sortedField = "createdDate";
            }
            if (descending) {
                q.orderBy(cb.desc(root.get(sortedField)));
            } else {
                q.orderBy(cb.asc(root.get(sortedField)));
            }

            return q;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public CriteriaQuery<PermohonanDokumen> getAllQuery(Long idOrganization) {
        try {
            // Exec Query
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<PermohonanDokumen> q = cb.createQuery(PermohonanDokumen.class);
            Root<PermohonanDokumen> root = q.from(PermohonanDokumen.class);

            // Join<PermohonanDokumen, ProcessTask> pemohon = root.join("pemohon", JoinType.LEFT);
            //#region Sub Query
            Subquery<ProcessTask> sq = q.subquery(ProcessTask.class);
            Root<ProcessTask> rootProcessTask = sq.from(ProcessTask.class);

            Join<ProcessTask, ProcessInstance> rootProcessInstance = rootProcessTask.join("processInstance", JoinType.INNER);

            Expression recordRefId_exp = rootProcessInstance.get("recordRefId").as(Long.class); // String to Long
            sq.select(recordRefId_exp);

            List<Predicate> sqPredicates = new ArrayList<>();
            Predicate sqPredicate_1 = cb.equal(rootProcessInstance.get("relatedEntity"), "com.qtasnim.eoffice.db.PermohonanDokumen");
            Predicate sqPredicate_2 = rootProcessTask.join("assignee").get("idOrganization").in(idOrganization);
            Predicate sqPredicate_3 = cb.equal(rootProcessTask.get("executionStatus"), ExecutionStatus.PENDING);

            sqPredicates.add(sqPredicate_1);
            sqPredicates.add(sqPredicate_2);
            sqPredicates.add(sqPredicate_3);
            sq.where(sqPredicates.toArray(new Predicate[sqPredicates.size()]));
            //#endregion Sub Query

            q.select(root);

            List<Predicate> predicates = new ArrayList<>();
            Predicate pInTasks = root.get("id").in(sq);
            Predicate pIsDeleted = cb.isFalse(root.get("isDeleted"));

            // WHERE CONDITION
            predicates.add(pIsDeleted);
            predicates.add(pInTasks);

            q.where(predicates.toArray(new Predicate[predicates.size()]));

            return q;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    //#endregion

    public void saveOrEditDraftSalinanDokumen(Long id, PermohonanDokumenDto dao) {
        try {
            boolean isNew = id == null;
            PermohonanDokumen obj = new PermohonanDokumen();
            DateUtil dateUtil = new DateUtil();

            if (isNew) {
                if (dao.getIdArsip() != null) {
                    Arsip arsip = arsipService.find(dao.getIdArsip());
                    obj.setArsip(arsip);
                } else {
                    obj.setArsip(null);
                }

                if (dao.getIdSurat() != null) {
                    Surat surat = suratService.find(dao.getIdSurat());
                    obj.setSurat(surat);
                } else {
                    obj.setSurat(null);
                }

                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                obj.setCompanyCode(company);
                obj.setTipeLayanan(dao.getTipeLayanan());
                obj.setNoRegistrasi(UUID.randomUUID().toString());
                obj.setPerihal(dao.getPerihal());
                obj.setKeperluan(dao.getKeperluan());
                obj.setTglPinjam(dateUtil.getDateFromISOString(dao.getTglPinjam()));
                obj.setStatus(dao.getStatus());
                obj.setUserPeminjam(userSession.getUserSession().getUser());
                if (dao.getIdUnitPeminjam() != null) {
                    MasterKorsa korsa = korsaService.find(dao.getIdUnitPeminjam());
                    obj.setUnitPeminjam(korsa);
                } else {
                    obj.setUnitPeminjam(null);
                }
                if (dao.getIdUnitTujuan() != null) {
                    MasterKorsa korsa = korsaService.find(dao.getIdUnitTujuan());
                    obj.setUnitTujuan(korsa);
                } else {
                    obj.setUnitTujuan(null);
                }
                obj.setUserTujuan(null);
//                if(dao.getIdOrgpenyerah()!=null) {
//                    MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getIdOrgpenyerah());
//                    obj.setOrgPenyerah(org);
//                }else{
//                    obj.setOrgPenyerah(null);
//                }
                obj.setIsDeleted(false);
                this.create(obj);
            } else {

                obj = this.find(id);
                if (dao.getIdArsip() != null) {
                    Arsip arsip = arsipService.find(dao.getIdArsip());
                    obj.setArsip(arsip);
                } else {
                    obj.setArsip(null);
                }

                if (dao.getIdSurat() != null) {
                    Surat surat = suratService.find(dao.getIdSurat());
                    obj.setSurat(surat);
                } else {
                    obj.setSurat(null);
                }

                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                obj.setCompanyCode(company);
                obj.setTipeLayanan(dao.getTipeLayanan());
                obj.setNoRegistrasi(UUID.randomUUID().toString());
                obj.setPerihal(dao.getPerihal());
                obj.setKeperluan(dao.getKeperluan());
                obj.setTglPinjam(dateUtil.getDateFromISOString(dao.getTglPinjam()));
                obj.setStatus(dao.getStatus());
                obj.setUserPeminjam(userSession.getUserSession().getUser());
                if (dao.getIdUnitPeminjam() != null) {
                    MasterKorsa korsa = korsaService.find(dao.getIdUnitPeminjam());
                    obj.setUnitPeminjam(korsa);
                } else {
                    obj.setUnitPeminjam(null);
                }
                if (dao.getIdUnitTujuan() != null) {
                    MasterKorsa korsa = korsaService.find(dao.getIdUnitTujuan());
                    obj.setUnitTujuan(korsa);
                } else {
                    obj.setUnitTujuan(null);
                }
                if (dao.getIdUserTujuan() != null) {
                    MasterUser userDto = masterUserService.find(dao.getIdUserTujuan());
                    obj.setUserTujuan(userDto);
                } else {
                    obj.setUserTujuan(null);
                }
//                if(dao.getIdOrgpenyerah()!=null) {
//                    MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getIdOrgpenyerah());
//                    obj.setOrgPenyerah(org);
//                }else{
//                    obj.setOrgPenyerah(null);
//                }
                obj.setIsDeleted(false);
                this.edit(obj);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public PermohonanDokumen addData(Long id, PermohonanDokumenDto dao) throws Exception {
        PermohonanDokumen obj = new PermohonanDokumen();

        try {
            if (id == null) {
                this.create(save(obj, dao));
            } else {
                obj = db().where(q -> q.getId() == id).findOne().orElse(null);
                this.edit(save(obj, dao));
            }
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }

        return obj;
    }

    public void startWorkflow(PermohonanDokumen obj) {
        if (obj.getStatus().equals("SUBMITTED")) {

            final PermohonanDokumen surat = obj;
            ProcessDefinition activated = processDefinitionService.getActivated("Layanan Dokumen");
            IWorkflowProvider workflowProvider = masterWorkflowProviderService.getByProviderName(applicationContext.getApplicationConfig().getWorkflowProvider()).getWorkflowProvider();
            workflowProvider.startWorkflow(obj, activated, ProcessEventHandler.DEFAULT
                    .onCompleted((processInstance, status) -> onWorkflowCompleted(status, processInstance, surat, workflowProvider, null, null))
                    .onTasksCreated((prevTask, processTasks) -> onTasksCreated(surat, prevTask, processTasks)),
                     new HashMap<>()
            );
        }
    }

    private PermohonanDokumen save(PermohonanDokumen obj, PermohonanDokumenDto dao) {
        DateUtil dateUtil = new DateUtil();

        if (dao.getIdArsip() != null) {
            Arsip arsip = arsipService.find(dao.getIdArsip());
            obj.setArsip(arsip);
        } else {
            obj.setArsip(null);
        }

        if (dao.getIdSurat() != null) {
            Surat surat = suratService.find(dao.getIdSurat());
            obj.setSurat(surat);
        } else {
            obj.setSurat(null);
        }

        CompanyCode company = companyCodeService.find(dao.getIdCompany());
        obj.setCompanyCode(company);

        obj.setTipeLayanan(dao.getTipeLayanan());
        obj.setNoRegistrasi(UUID.randomUUID().toString());
        obj.setPerihal(dao.getPerihal());
        obj.setKeperluan(dao.getKeperluan());
        obj.setTglPinjam(dateUtil.getDateFromISOString(dao.getTglPinjam()));
        obj.setTglTarget(dateUtil.getDateFromISOString(dao.getTglTarget()));
        obj.setStatus(dao.getStatus());
        obj.setUserPeminjam(userSession.getUserSession().getUser());

        if (dao.getIdUnitPeminjam() != null) {
            MasterKorsa korsa = korsaService.find(dao.getIdUnitPeminjam());
            obj.setUnitPeminjam(korsa);
        } else {
            obj.setUnitPeminjam(null);
        }
        if (dao.getIdUnitTujuan() != null) {
            MasterKorsa korsa = korsaService.find(dao.getIdUnitTujuan());
            obj.setUnitTujuan(korsa);
        } else {
            obj.setUnitTujuan(null);
        }
        if (dao.getIdUserTujuan() != null) {
            MasterUser userDto = masterUserService.find(dao.getIdUserTujuan());
            obj.setUserTujuan(userDto);
        } else {
            obj.setUserTujuan(null);
        }
        obj.setIsDeleted(dao.getIsDeleted());
        if (dao.getTipeLayanan().toLowerCase().equals("peminjaman dokumen asli")) {
            obj.setLamaPinjam(dao.getLamaPinjam());
            Calendar c = Calendar.getInstance();
            c.setTime(dateUtil.getDateFromISOString(dao.getTglTarget()));
            c.add(Calendar.DATE, dao.getLamaPinjam());
            obj.setTglKembali(c.getTime());
        }

        //#region Penerima
        if (!dao.getPenerima().isEmpty()) {
            List<PermohonanDokumenPenerima> penerimaList = new ArrayList<>();
            Integer index = 0;

            for (PermohonanDokumenPenerimaDto assignee : dao.getPenerima()) {
                PermohonanDokumenPenerima penerima = new PermohonanDokumenPenerima();
                MasterStrukturOrganisasi org = orgService.find(assignee.getIdOrganization());
                penerima.setOrganization(org);
                penerima.setSeq(Optional.ofNullable(assignee.getSeq()).orElse(index));
                penerima.setPermohonanDokumen(obj);
                penerimaList.add(penerima);
                index++;
            }
            obj.setPenerima(penerimaList);
        }
        //#endregion

        //#region Tembusan
        if (!dao.getTembusan().isEmpty()) {
            // delete
            if (obj.getId() != null) {
                tembusanPermohonanDokumenService.removeAllBy(obj.getId());
            }

            // insert
            List<PermohonanDokumenTembusan> tembusanList = new ArrayList<>();
            for (PermohonanDokumenTembusanDto assignee : dao.getTembusan()) {
                PermohonanDokumenTembusan tembusan = new PermohonanDokumenTembusan();
                MasterStrukturOrganisasi org = orgService.find(assignee.getIdOrganization());
                tembusan.setOrganization(org);
                tembusan.setPermohonanDokumen(obj);
                tembusan.setSeq(assignee.getSeq());
                tembusanList.add(tembusan);
            }
            obj.setTembusan(tembusanList);
        }
        //#endregion

        return obj;
    }

    public PermohonanDokumen perpanjangan(Long id, PermohonanDokumenDto dao) {
        DateUtil dateUtil = new DateUtil();
        PermohonanDokumen obj = this.find(id);
        if (obj != null) {
            Integer lama = dao.getLamaPinjam().intValue() + dao.getPerpanjangan().intValue();
            obj.setPerpanjangan(dao.getPerpanjangan());
            Calendar c = Calendar.getInstance();
            c.setTime(dateUtil.getDateFromISOString(dao.getTglTarget()));
            c.add(Calendar.DATE, lama);
            obj.setTglKembali(c.getTime());
            obj.setLamaPinjam(lama);
            this.edit(obj);
            return obj;
        } else {
            return null;
        }
    }

    public void saveOrEditDraftPeminjamanDokumen(Long id, PermohonanDokumenDto dao) {
        try {
            boolean isNew = id == null;
            PermohonanDokumen obj = new PermohonanDokumen();
            DateUtil dateUtil = new DateUtil();
            Arsip arsip = new Arsip();
            Surat surat = new Surat();
            if (isNew) {
                if (dao.getIdArsip() != null) {
                    arsip = arsipService.find(dao.getIdArsip());
                    obj.setArsip(arsip);
                } else {
                    obj.setArsip(null);
                }
                if (dao.getIdSurat() != null) {
                    surat = suratService.find(dao.getIdSurat());
                    obj.setSurat(surat);
                } else {
                    obj.setSurat(null);
                }
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                obj.setCompanyCode(company);
                obj.setNoRegistrasi(UUID.randomUUID().toString());
                obj.setPerihal(dao.getPerihal());
                obj.setKeperluan(dao.getKeperluan());
                obj.setTglPinjam(dateUtil.getDateFromISOString(dao.getTglPinjam()));
                obj.setStatus(dao.getStatus());
                obj.setUserPeminjam(userSession.getUserSession().getUser());
                if (dao.getIdUnitPeminjam() != null) {
                    MasterKorsa korsa = korsaService.find(dao.getIdUnitPeminjam());
                    obj.setUnitPeminjam(korsa);
                } else {
                    obj.setUnitPeminjam(null);
                }
                if (dao.getIdUnitTujuan() != null) {
                    MasterKorsa korsa = korsaService.find(dao.getIdUnitTujuan());
                    obj.setUnitTujuan(korsa);
                } else {
                    obj.setUnitTujuan(null);
                }
                if (dao.getIdUserTujuan() != null) {
                    MasterUser userDto = masterUserService.find(dao.getIdUserTujuan());
                    obj.setUserTujuan(userDto);
                } else {
                    obj.setUserTujuan(null);
                }
                obj.setTipeLayanan(dao.getTipeLayanan());
                obj.setLamaPinjam(dao.getLamaPinjam());

                Calendar c = Calendar.getInstance();
                c.setTime(dateUtil.getDateFromISOString(dao.getTglTarget()));
                c.add(Calendar.DATE, dao.getLamaPinjam());
                obj.setTglKembali(c.getTime());
                obj.setIsDeleted(false);
                this.create(obj);
            } else {
                obj = this.find(id);
                if (dao.getIdArsip() != null) {
                    arsip = arsipService.find(dao.getIdArsip());
                    obj.setArsip(arsip);
                } else {
                    obj.setArsip(null);
                }
                if (dao.getIdSurat() != null) {
                    surat = suratService.find(dao.getIdSurat());
                    obj.setSurat(surat);
                } else {
                    obj.setSurat(null);
                }
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                obj.setCompanyCode(company);
                obj.setNoRegistrasi(UUID.randomUUID().toString());
                obj.setPerihal(dao.getPerihal());
                obj.setKeperluan(dao.getKeperluan());
                obj.setTglPinjam(dateUtil.getDateFromISOString(dao.getTglPinjam()));
                obj.setStatus(dao.getStatus());
                obj.setUserPeminjam(userSession.getUserSession().getUser());
                if (dao.getIdUnitPeminjam() != null) {
                    MasterKorsa korsa = korsaService.find(dao.getIdUnitPeminjam());
                    obj.setUnitPeminjam(korsa);
                } else {
                    obj.setUnitPeminjam(null);
                }
                if (dao.getIdUnitTujuan() != null) {
                    MasterKorsa korsa = korsaService.find(dao.getIdUnitTujuan());
                    obj.setUnitTujuan(korsa);
                } else {
                    obj.setUnitTujuan(null);
                }
                if (dao.getIdUserTujuan() != null) {
                    MasterUser userDto = masterUserService.find(dao.getIdUserTujuan());
                    obj.setUserTujuan(userDto);
                } else {
                    obj.setUserTujuan(null);
                }
                obj.setTipeLayanan(dao.getTipeLayanan());
                obj.setLamaPinjam(dao.getLamaPinjam());
                obj.setIsDeleted(false);

                Calendar c = Calendar.getInstance();
                c.setTime(dateUtil.getDateFromISOString(dao.getTglTarget()));
                c.add(Calendar.DATE, dao.getLamaPinjam());
                obj.setTglKembali(c.getTime());
                this.edit(obj);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public PermohonanDokumen approvePermohonan(Long id) {
        PermohonanDokumen obj = db().where(q -> q.getId() == id).findOne().orElse(null);
        if (obj != null) {
            obj.setUserTujuan(userSession.getUserSession().getUser());
            obj.setStatus(EntityStatus.APPROVED.toString());
            this.edit(obj);
            return obj;
        } else {
            return null;
        }

    }

    public void permohonanSalinanDokumen(PermohonanDokumenDto dao) {
        PermohonanDokumen obj = new PermohonanDokumen();
        DateUtil dateUtil = new DateUtil();

        if (dao.getIdArsip() != null) {
            Arsip arsip = arsipService.find(dao.getIdArsip());
            obj.setArsip(arsip);
        }
        if (dao.getIdSurat() != null) {
            Surat surat = suratService.find(dao.getIdSurat());
            obj.setSurat(surat);
        }
        CompanyCode company = companyCodeService.find(dao.getIdCompany());
        obj.setCompanyCode(company);
        obj.setTipeLayanan(dao.getTipeLayanan());
        obj.setNoRegistrasi(UUID.randomUUID().toString());
        obj.setPerihal(dao.getPerihal());
        obj.setKeperluan(dao.getKeperluan());
        obj.setTglPinjam(dateUtil.getDateFromISOString(dao.getTglPinjam()));
        obj.setStatus(dao.getStatus());
        obj.setUserPeminjam(userSession.getUserSession().getUser());
        if (dao.getIdUnitPeminjam() != null) {
            MasterKorsa korsa = korsaService.find(dao.getIdUnitPeminjam());
            obj.setUnitPeminjam(korsa);
        } else {
            obj.setUnitPeminjam(null);
        }
        if (dao.getIdUnitTujuan() != null) {
            MasterKorsa korsa = korsaService.find(dao.getIdUnitTujuan());
            obj.setUnitTujuan(korsa);
        } else {
            obj.setUnitTujuan(null);
        }
        if (dao.getIdUserTujuan() != null) {
            MasterUser userDto = masterUserService.find(dao.getIdUserTujuan());
            obj.setUserTujuan(userDto);
        } else {
            obj.setUserTujuan(null);
        }
        //MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getIdOrgpenyerah());
        //obj.setOrgPenyerah(org);
        obj.setIsDeleted(false);
        this.create(obj);
    }

    public void peminjamanDokumenAsli(PermohonanDokumenDto dao) {
        try {
            PermohonanDokumen obj = new PermohonanDokumen();
            DateUtil dateUtil = new DateUtil();
            Arsip arsip = arsipService.find(dao.getIdArsip());
            obj.setArsip(arsip);

            CompanyCode company = companyCodeService.find(dao.getIdCompany());
            obj.setCompanyCode(company);
            obj.setNoRegistrasi(UUID.randomUUID().toString());
            obj.setPerihal(dao.getPerihal());
            obj.setKeperluan(dao.getKeperluan());
            obj.setTglPinjam(dateUtil.getDateFromISOString(dao.getTglPinjam()));
            obj.setStatus(dao.getStatus());
            obj.setUserPeminjam(userSession.getUserSession().getUser());
            if (dao.getIdUnitPeminjam() != null) {
                MasterKorsa korsa = korsaService.find(dao.getIdUnitPeminjam());
                obj.setUnitPeminjam(korsa);
            } else {
                obj.setUnitPeminjam(null);
            }
            if (dao.getIdUnitTujuan() != null) {
                MasterKorsa korsa = korsaService.find(dao.getIdUnitTujuan());
                obj.setUnitTujuan(korsa);
            } else {
                obj.setUnitTujuan(null);
            }
            if (dao.getIdUserTujuan() != null) {
                MasterUser userDto = masterUserService.find(dao.getIdUserTujuan());
                obj.setUserTujuan(userDto);
            } else {
                obj.setUserTujuan(null);
            }
            //MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getIdOrgpenyerah());
            //obj.setOrgPenyerah(org);
            obj.setTipeLayanan(dao.getTipeLayanan());
            obj.setLamaPinjam(dao.getLamaPinjam());

            Calendar c = Calendar.getInstance();
            c.setTime(dateUtil.getDateFromISOString(dao.getTglTarget()));
            c.add(Calendar.DATE, dao.getLamaPinjam());
            obj.setTglKembali(c.getTime());
            this.create(obj);

            arsip.setStatus("Sedang Dipinjam");
            arsipService.edit(arsip);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public JPAJinqStream<PermohonanDokumen> getDraftSalinanDokumen() {
        MasterUser usr = userSession.getUserSession().getUser();
        return db().where(q -> !q.getIsDeleted() && q.getStatus().toUpperCase().equals("DRAFT") && q.getTipeLayanan().equals("Permohonan Salinan Dokumen")
                && q.getUserPeminjam().equals(usr));
    }

    public JPAJinqStream<PermohonanDokumen> getDraftPeminjamanDokumen() {
        MasterUser usr = userSession.getUserSession().getUser();
        return db().where(q -> !q.getIsDeleted() && q.getStatus().equals(EntityStatus.DRAFT) && q.getTipeLayanan().equals("Peminjaman Dokumen Asli")
                && q.getUserPeminjam().equals(usr));
    }

    public JPAJinqStream<PermohonanDokumen> getSalinanDokumen() {
        MasterUser usr = userSession.getUserSession().getUser();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();
        return db().where(q -> !q.getIsDeleted() && !q.getStatus().equals(EntityStatus.APPROVED) && q.getTipeLayanan().equals("Permohonan Salinan Dokumen")
                && (q.getUserPeminjam().equals(usr))
        );
    }

    public JPAJinqStream<PermohonanDokumen> getPeminjamanDokumen() {
        MasterUser usr = userSession.getUserSession().getUser();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();
        return db().where(q -> !q.getIsDeleted() && !q.getStatus().equals(EntityStatus.APPROVED) && q.getTipeLayanan().equals("Peminjaman Dokumen Asli")
                && (q.getUserPeminjam().equals(usr))
        );
    }

    public PermohonanDokumen del(Long id) {
        //PermohonanDokumen model = this.find(id);
        PermohonanDokumen model = db().where(q -> q.getId() == id).findOne().orElse(null);
        if (model != null) {
            model.setIsDeleted(true);
            this.edit(model);
            return model;
        } else {
            return null;
        }
    }

    public HashMap<String, Object> getRPeminjamanDokumen(String start, String end, Long idJenisDokumen, String idUnit,
            String sort, boolean descending, int skip, int limit) {
        List<PermohonanDokumenDto> result = new ArrayList<>();
        TypedQuery<PermohonanDokumen> all = getResultData("APPROVED", sort, descending, start, end, idUnit, idJenisDokumen);
        result = RPermohonanDokumen(all, skip, limit);
        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    public TypedQuery<PermohonanDokumen> getResultData(String status, String sort, Boolean desc, String start, String end, String idUnit, Long idJenisDokumen) {
        DateUtil dateUtil = new DateUtil();
        Date n = new Date();
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<PermohonanDokumen> q = cb.createQuery(PermohonanDokumen.class);
        Root<PermohonanDokumen> root = q.from(PermohonanDokumen.class);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted")),
                cb.equal(root.get("status"), status),
                cb.equal(root.get("tipeLayanan"), "Peminjaman Dokumen Asli"),
                cb.equal(root.get("unitTujuan"), korsa)
        );

        predicates.add(mainCond);

        /*filter tanggal pinjam - tanggal kembali*/
        if (!Strings.isNullOrEmpty(start)) {
            if (!Strings.isNullOrEmpty(end)) {
                Date mulai = dateUtil.getDateTimeStart(start + "T00:00:00.000Z");
                Date selesai = dateUtil.getDateTimeEnd(end + "T23:59:59.999Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(root.<Date>get("tglPinjam"), mulai),
                        cb.lessThanOrEqualTo(root.<Date>get("tglKembali"), selesai)
                );
                predicates.add(pBetween);
            } else {
                Date mulai = dateUtil.getDateTimeStart(start + "T00:00:00.000Z");
                Date mulai2 = dateUtil.getDateTimeEnd(start + "T23:59:59.999Z");
                Predicate pStart = cb.and(
                        cb.greaterThanOrEqualTo(root.<Date>get("tglPinjam"), mulai),
                        cb.lessThanOrEqualTo(root.<Date>get("tglPinjam"), mulai2)
                );
                predicates.add(pStart);
            }
        } else {
            if (!Strings.isNullOrEmpty(end)) {
                Date selesai = dateUtil.getDateTimeStart(end + "T00:00:00.000Z");
                Date selesai2 = dateUtil.getDateTimeEnd(end + "T23:59:59.999Z");
                Predicate pEnd = cb.and(
                        cb.greaterThanOrEqualTo(root.<Date>get("tglKembali"), selesai),
                        cb.lessThanOrEqualTo(root.<Date>get("tglKembali"), selesai2)
                );
                predicates.add(pEnd);
            }
        }

        /*filter unit*/
        if (!Strings.isNullOrEmpty(idUnit)) {
            Predicate pUnit = cb.equal(root.join("unitPeminjam", JoinType.LEFT).get("id"), idUnit);
            predicates.add(pUnit);
        }

        /*filter idJenisDokumen*/
        if (idJenisDokumen != null) {
            Predicate pJenisDokumen = cb.equal(root.join("surat", JoinType.INNER).join("formDefinition", JoinType.INNER)
                    .join("jenisDokumen", JoinType.INNER).get("idJenisDokumen"), idJenisDokumen);
            predicates.add(pJenisDokumen);
        }

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);
        
        if (Strings.isNullOrEmpty(sort)) {
            q.orderBy(cb.desc(root.get("modifiedDate")));
        } else {
            Expression exp = root.get(sort);
            if (desc) {
                q.orderBy(cb.desc(exp));
            } else {
                q.orderBy(cb.asc(exp));
            }
        }

        System.out.println("data: " + getEntityManager().createQuery(q).getResultList());

        return getEntityManager().createQuery(q);
                // .setFirstResult(skip).setMaxResults(limit);
    }

    private List<PermohonanDokumenDto> RPermohonanDokumen(TypedQuery<PermohonanDokumen> all, Integer skip, Integer limit) {
        List<PermohonanDokumenDto> result = null;

        if (all != null) {
            result = all.getResultStream().skip(skip).limit(limit).map(q -> new PermohonanDokumenDto(q)).collect(Collectors.toList());
        }
        return result;
    }

    public boolean validate(Long id, PermohonanDokumenDto dto) {
        boolean isValid = false;
        JPAJinqStream<PermohonanDokumen> query = db().where(a -> !a.getIsDeleted());
        Long companyId = dto.getIdCompany();
        Long idUserPeminjam = userSession.getUserSession().getUser().getId();
        String idUnitPeminjam = dto.getIdUnitPeminjam();
        String idUnitTujuan = dto.getIdUnitTujuan();
        Long idSurat = dto.getIdSurat();
        String perihal = dto.getPerihal();
        String keperluan = dto.getKeperluan();
        String tipeLayanan = dto.getTipeLayanan();
        if (id == null) {
            List<PermohonanDokumen> tmp = query.where(b -> b.getCompanyCode().getId().equals(companyId) && b.getUserPeminjam().getId().equals(idUserPeminjam)
                    && b.getTipeLayanan().toLowerCase().equals(tipeLayanan.toLowerCase()) && b.getUnitPeminjam().getId().equals(idUnitPeminjam)
                    && b.getUnitTujuan().getId().equals(idUnitTujuan)).collect(Collectors.toList());
            isValid = tmp.stream().noneMatch(c -> c.getSurat().getId().equals(idSurat) && c.getPerihal().toLowerCase().equals(perihal.toLowerCase())
                    && c.getKeperluan().toLowerCase().equals(keperluan.toLowerCase()));
        } else {
            List<PermohonanDokumen> tmp = query.where(b -> !b.getId().equals(id) && b.getCompanyCode().getId().equals(companyId) && b.getUserPeminjam().getId().equals(idUserPeminjam)
                    && b.getTipeLayanan().toLowerCase().equals(tipeLayanan.toLowerCase()) && b.getUnitPeminjam().getId().equals(idUnitPeminjam)
                    && b.getUnitTujuan().getId().equals(idUnitTujuan)).collect(Collectors.toList());
            isValid = tmp.stream().noneMatch(c -> c.getSurat().getId().equals(idSurat) && c.getPerihal().toLowerCase().equals(perihal.toLowerCase())
                    && c.getKeperluan().toLowerCase().equals(keperluan.toLowerCase()));
        }

        return isValid;
    }

    private List<PermohonanDokumen> pendingPermohonan(Long idSurat, Long exceptId) {
        return db().where(q -> q.getSurat().getId().equals(idSurat) && q.getStatus().equals("SUBMITTED") && q.getId() != exceptId).collect(Collectors.toList());
    }

    private void sendMailToPenerima(ProcessTask processTask, IWorkflowEntity entity) {
        MasterUser user = processTask.getAssignee().getUser();
        MasterUser initiator = ((PermohonanDokumen) entity).getUserPeminjam();

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        notificationService.sendEmail(Constants.LAYANAN_NEED_APPROVAL, vars -> {
            vars.put("initiator", initiator);
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationConfig.getFrontEndUrl(), entity.getPageLink()));
        }, user);
    }

    private void sendMailToPemohon(IWorkflowEntity entity) {
        MasterUser initiator = ((PermohonanDokumen) entity).getUserPeminjam();
        MasterUser penerima = ((PermohonanDokumen) entity).getPenerima().get(0).getOrganization().getUser();
        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        notificationService.sendEmail(Constants.INFO_PEMOHON_LAYANAN, vars -> {
            vars.put("initiator", penerima);
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationConfig.getFrontEndUrl(), entity.getPageLink()));
        }, initiator);
    }

    private void sendMailToTembusan(IWorkflowEntity entity) {
        List<MasterUser> tembusanList = ((PermohonanDokumen) entity).getTembusan().stream().map(a -> a.getOrganization().getUser()).collect(Collectors.toList());
        MasterUser initiator = ((PermohonanDokumen) entity).getPenerima().get(0).getOrganization().getUser();
        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        notificationService.sendEmail(Constants.TEMBUSAN_LAYANAN, vars -> {
            vars.put("initiator", initiator);
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationConfig.getFrontEndUrl(), entity.getPageLink()));
        }, tembusanList.toArray(new MasterUser[tembusanList.size()]));
    }

    /**
     * START Workflow Event Handler
     */
    public IWorkflowEntity getEntity(ProcessTask processTask) {
        return find(Long.parseLong(processTask.getProcessInstance().getRecordRefId()));
    }

    @Override
    public boolean isTaskInvalid(TaskActionDto dto, ProcessTask processTask, Map<String, Object> userResponse) throws Exception {
        return false;
    }

    @Override
    public void onWorkflowCompleted(ProcessStatus status, ProcessInstance processInstance, IWorkflowEntity entity, IWorkflowProvider workflowProvider, String digisignUsername, String passphrase) {
        try {
            PermohonanDokumen surat = (PermohonanDokumen) entity;

            if (ProcessStatus.APPROVED.equals(status)) {
                surat.setStatus(EntityStatus.APPROVED.toString());
                surat.getPenerima().forEach(penerima -> {
                    penerima.setUserApprover(userSession.getUserSession().getUser());
                });

                edit(surat);

                Surat objSurat = surat.getSurat();

                if (surat.getTipeLayanan().equals("Peminjaman Dokumen Asli")) {
                    objSurat.setIsPinjam(true);
                    suratService.edit(objSurat);
                }

                List<PermohonanDokumen> pendingApproval = pendingPermohonan(objSurat.getId(), surat.getId());
                if (!pendingApproval.isEmpty()) {
                    for (PermohonanDokumen p : pendingApproval) {
                        ProcessInstance process = processInstanceService.getByRelatedEntity("com.qtasnim.eoffice.db.PermohonanDokumen", p.getId().toString());
                        workflowProvider.terminateProcess(process);
                        p.setStatus(EntityStatus.DRAFT.toString());
                        edit(p);
                    }
                }

                entity.setTaskResponse("Disetujui");
            } else if (ProcessStatus.REJECTED.equals(status)) {

                surat.setStatus(EntityStatus.DRAFT.toString());
                edit(surat);
                entity.setTaskResponse("Ditolak");
            }

            sendMailToPemohon(entity);
            sendMailToTembusan(entity);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new InternalServerErrorException(null, ex);
        }
    }

    @Override
    public void onTasksCreated(IWorkflowEntity entity, ProcessTask prevTask, List<ProcessTask> processTasks) {
        for (ProcessTask task : processTasks) {
            sendMailToPenerima(task, entity);
            userTaskService.saveOrEdit(null, task, entity);
        }
    }

    @Override
    public void onTaskExecuting(ProcessTask task) {

    }

    @Override
    public void onTaskExecuted(TaskActionDto dto, IWorkflowProvider workflowProvider, IWorkflowEntity entity, ProcessTask task, TaskEventHandler taskEventHandler, Map<String, Object> userResponse) {
        /*@TODO Tambah insert ke tabel dokumen history*/
        if (userSession != null && userSession.getUserSession() != null) {
            String responseVar = task.getResponseVar();
            String response = (String)userResponse.get(responseVar);

            if (response.toLowerCase().contains("kembalikan")) {
                Long idLayanan = ((PermohonanDokumen) entity).getId();
                DokumenHistoryDto history = new DokumenHistoryDto();
                history.setReferenceTable("t_permohonan_dokumen");
                history.setReferenceId(idLayanan);
                history.setStatus("REJECT");
                history.setIdOrganisasi(task.getAssignee().getIdOrganization());
                history.setIdUser(task.getAssignee().getUser().getId());

                docHistoryService.saveHistory(history);
            }

            if (response.toLowerCase().contains("setujui")){
                Long idLayanan = ((PermohonanDokumen) entity).getId();
                DokumenHistoryDto history = new DokumenHistoryDto();
                history.setReferenceTable("t_permohonan_dokumen");
                history.setReferenceId(idLayanan);
                history.setStatus("APPROVE");
                history.setIdOrganisasi(task.getAssignee().getIdOrganization());
                history.setIdUser(task.getAssignee().getUser().getId());

                docHistoryService.saveHistory(history);
            }
        }

        notificationService.sendInfo("Info", "Persetujuan Layanan berhasil dikirim");
    }

    @Override
    public List<ProcessTaskDto> onTaskHistoryRequested(List<ProcessTask> myTasks) {
        return myTasks.stream().map(ProcessTaskDto::new).collect(Collectors.toList());
    }

    @Override
    public void onTaskActionGenerated(ProcessTask processTask, TaskActionResponseDto response) {

    }
    /**
     * END Workflow Event Handler
     */

    /**
     * START Mail Handler
     */
    public void setMailProperties(IWorkflowEntity entity) {
        String className = entity.getClassName();

        String jenisDokumen = "";
        String perihal = "";
        String nomor = "";
        String klasifikasiKeamanan = "";

        if (className.contains("PermohonanDokumen")){
            jenisDokumen = ((PermohonanDokumen) entity).getTipeLayanan();
            perihal = ((PermohonanDokumen) entity).getPerihal();
            nomor = ((PermohonanDokumen) entity).getSurat().getNoDokumen();
            Locale locale = new Locale("in", "ID");
            String formatted = new SimpleDateFormat("dd MMMMMMMMMM yyyy", locale).format(((PermohonanDokumen) entity).getTglPinjam());

            entity.setFormattedTanggal(formatted);
        } else if (className.contains("DistribusiDokumen")){
            jenisDokumen = "Distribusi Dokumen (" + ((DistribusiDokumen) entity).getJenis() + ")";
            perihal = ((DistribusiDokumen) entity).getPerihal();
        }

        entity.setJenisDokumen(jenisDokumen);
        entity.setPerihal(perihal);
        entity.setKlasifikasiKeamanan(klasifikasiKeamanan);

        if(className.contains("PermohonanDokumen")) {
            entity.setNomor(nomor);
        } else {
            entity.setNomor(entity.getNoDokumen());
        }
    }

    public void newTaskNotification(List<ProcessTask> processTasks, IWorkflowEntity entity, String docId) {
        for (ProcessTask processTask : processTasks) {
            MasterUser user = processTask.getAssignee().getUser();
            MasterUser initiator = masterUserService.findUserByUsername(entity.getCreatedBy());

            // < SET MAIL PROPS >
            setMailProperties(entity);
            // </ SET MAIL PROPS >

            notificationService.sendEmail(Constants.TEMPLATE_NEW_TASK_ASSIGNMENT, vars -> {
                vars.put("initiator", initiator);
                vars.put("assignee", user);
                vars.put("entity", entity);
                vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
            }, docId, null, user);
        }
    }

    public void newTaskNotification(ProcessTask processTask, IWorkflowEntity entity, String docId) {
        MasterUser user = processTask.getAssignee().getUser();

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        notificationService.sendEmail(Constants.TEMPLATE_NEW_TASK_ASSIGNMENT, vars -> {
            vars.put("assignee", user);
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
        }, docId, null, user);
    }

    public void infoTaskNotification(ProcessTask processTask, IWorkflowEntity entity, String docId) {
        // < INIT TASK DICTIONARY >
        Map<String, String> taskExecutorVars = new HashMap<>();
        taskExecutorVars.put("Task_PemeriksaPelakhar_Approval", "Pelakhar Pemeriksa");
        taskExecutorVars.put("Task_PemeriksaPYMT_Approval", "PYMT Pemeriksa");
        taskExecutorVars.put("Task_Pemeriksa_Approval", "Pemeriksa");

        taskExecutorVars.put("Task_PenandatanganPelakhar_Approval", "Pelakhar Penandatangan");
        taskExecutorVars.put("Task_PenandatanganPelakhar_Approval_2", "Pelakhar Penandatangan");
        taskExecutorVars.put("Task_PenandatanganPYMT_Approval", "PYMT Penandatangan");
        taskExecutorVars.put("Task_Penandatangan_Approval", "Penandatangan");

        taskExecutorVars.put("Setujui", "Disetujui");
        taskExecutorVars.put("TO PYMT", "Disetujui");
        taskExecutorVars.put("Kembalikan", "Dikembalikan");
        taskExecutorVars.put("Kembalikan ke Konseptor", "Dikembalikan");
        taskExecutorVars.put("Kembalikan ke Pemeriksa", "Dikembalikan");

        // </ INIT TASK DICTIONARY >
        String executor = taskExecutorVars.get(processTask.getTaskName());
        if (!Strings.isNullOrEmpty(executor)) {
            entity.setTaskExecutor(executor);
        } else {
            entity.setTaskExecutor("");
        }

        String response = taskExecutorVars.get(processTask.getResponse());

        if (!Strings.isNullOrEmpty(response)) {
            entity.setTaskResponse(response);
        } else {
            entity.setTaskResponse("");
        }

        MasterUser user = processTask.getAssignee().getUser();
        String[] initiatorParts = entity.getCreatedBy().split("\\|");
        MasterUser initiator = masterUserService.findUserByUsername(initiatorParts[0].trim());

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_INFO, vars -> {
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
        }, docId, null, initiator);
    }
    /**
     * END Mail Handler
     */
}
