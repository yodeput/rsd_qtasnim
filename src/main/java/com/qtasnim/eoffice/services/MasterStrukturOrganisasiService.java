package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.*;
import org.apache.commons.lang.StringUtils;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.jpa.JPQL;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;


@Stateless
@LocalBean
public class MasterStrukturOrganisasiService extends AbstractFacade<MasterStrukturOrganisasi> {
    private static final long serialVersionUID = 1L;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterAreaService areaService;

    @Inject
    private MasterGradeService gradeService;

    @Inject
    private MasterKorsaService korsaService;

    @Inject
    private MasterKewenanganPenerimaService kewenanganPenerimaService;

    @Inject
    private BackgroundServiceLoggerService loggerService;

    @Inject
    private OrganisationSyncScheduler organisationSyncScheduler;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private MasterNonStrukturalService nonStrukturalService;

    @Override
    protected Class<MasterStrukturOrganisasi> getEntityClass() {
        return MasterStrukturOrganisasi.class;
    }

    public JPAJinqStream<MasterStrukturOrganisasi> getAll() {
        return db();
    }

    public JPAJinqStream<MasterStrukturOrganisasi> getAllValid() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && (!q.getIsDeleted() || q.getIsActive())
                && (!q.getUser().getIsDeleted() || q.getUser().getIsActive()));
    }

    public JPAJinqStream<MasterStrukturOrganisasi> getFiltered(String name, String code, String isActive, String isDeleted,
                                                               String parentId, String level) {
        JPAJinqStream<MasterStrukturOrganisasi> query = db();

        if (!Strings.isNullOrEmpty(name)) {
            query = query.where(q -> q.getOrganizationName().contains(name));
        }

        if (!Strings.isNullOrEmpty(code)) {
            query = query.where(q -> q.getOrganizationCode().contains(code));
        }

        if (!Strings.isNullOrEmpty(isActive)) {
            boolean isActiv = Boolean.parseBoolean(isActive);
            query = query.where(q -> q.getIsActive() == isActiv);
        }

        if (!Strings.isNullOrEmpty(isDeleted)) {
            boolean isDel = Boolean.parseBoolean(isActive);
            query = query.where(q -> q.getIsDeleted() == isDel);
        }

        if (!Strings.isNullOrEmpty(parentId)) {
            MasterStrukturOrganisasi obj = this.find(Long.parseLong(parentId));
            query = query.where(q -> q.getParent().equals(obj));
        }

        if (!Strings.isNullOrEmpty(level)) {
            int lvl = Integer.parseInt(level);
            query = query.where(q -> q.getLevel() == lvl);
        }

        return query;
    }

    public MasterStrukturOrganisasi getByCode(String entityCode) {
        return db()
                .where(t -> (t.getIsActive() || !t.getIsDeleted()) && t.getOrganizationCode().equals(entityCode))
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public List<MasterStrukturOrganisasi> getByCode(List<String> entityCodes) {
        if (entityCodes.size() > 0) {
            return db()
                    .where(t -> (t.getIsActive() || !t.getIsDeleted()) && JPQL.isInList(t.getOrganizationCode(), entityCodes))
                    .toList();
        }

        return new ArrayList<>();
    }

    public MasterStrukturOrganisasi getByIdOrganisasi(Long idOrganisasi) {
        return db()
                .where(t -> t.getIdOrganization().equals(idOrganisasi))
                .findFirst()
                .orElse(null);
    }

    public JPAJinqStream<MasterStrukturOrganisasi> getByParent(Long parent,String sort,Boolean descending) {
        Date n = new Date();
        JPAJinqStream<MasterStrukturOrganisasi> query = db().where(q->n.after(q.getStart()) && n.before(q.getEnd())
                && (!q.getIsDeleted() || q.getIsActive()));

        if(parent==null){
            query = query.where(q->q.getParent().equals(null));
        }else{
            query = query.where(q->q.getParent().getIdOrganization().equals(parent));
        }

        if (!Strings.isNullOrEmpty(sort)) {
            if (descending) {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedDescendingBy(q->q.getOrganizationName());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedDescendingBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedDescendingBy(q -> q.getEnd());
                }
            } else {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedBy(q -> q.getOrganizationName());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedBy(q -> q.getEnd());
                }
            }
        }
        return query;
    }

    public MasterStrukturOrganisasiDto getByUserId(Long idUser) {
        return db().where(q -> (!q.getIsDeleted() || q.getIsActive()) && q.getUser().getId().equals(idUser) &&
                (!q.getUser().getIsDeleted() || q.getUser().getIsActive())).map(MasterStrukturOrganisasiDto::new).findFirst()
                .orElse(null);
    }

    public MasterStrukturOrganisasi getByIdUser(Long idUser) {
        return db().where(q -> (!q.getIsDeleted() || q.getIsActive()) && q.getUser().getId().equals(idUser) &&
                (!q.getUser().getIsDeleted() || q.getUser().getIsActive())).findFirst().orElse(null);
    }

//    @Transactional(value = Transactional.TxType.REQUIRES_NEW)
    public void sync(List<OrganizationWrapper> apiOrg) {
        SyncLog syncLog = organisationSyncScheduler.getSyncLog();

        try {
            Instant start = Instant.now();
            loggerService.info(syncLog, String.format("Start saving organization | %s", new Date()));

            Date now = new Date();
            DateUtil dateUtil = new DateUtil();
            List<MasterArea> areas = areaService.findAll();
            List<MasterGrade> masterGrades = gradeService.findAll();
            List<MasterKorsa> masterKorsas = korsaService.findAll();
            CompanyCode company = companyCodeService.getByCode("I100");

            List<OrganizationWrapper> db = ((List<MasterStrukturOrganisasi>) getEntityManager().createQuery("SELECT A FROM MasterStrukturOrganisasi A LEFT OUTER JOIN FETCH A.user B LEFT OUTER JOIN FETCH A.parent C WHERE A.isHris = TRUE").getResultList())
                    .stream()
                    .map(t -> new OrganizationWrapper(t, null, t.getOrganizationCode(), t.getOrganizationName(), t.getSingkatan(), Optional.ofNullable(t.getParent()).map(MasterStrukturOrganisasi::getOrganizationCode).orElse(""),t.getIsPosisi().booleanValue(),t.getIsChief().booleanValue(),Optional.ofNullable(t.getArea()).map(MasterArea::getId).orElse(null), Optional.ofNullable(t.getCompanyCode()).map(CompanyCode::getCode).orElse(null),Optional.ofNullable(t.getClazz()).orElse(null),Optional.ofNullable(t.getGrade()).map(a->a.getId()).orElse(null),
                            Optional.ofNullable(t.getUnit()).map(MasterKorsa::getId).orElse(null))).collect(Collectors.toList());

            //PROP CHANGED
            List<OrganizationWrapper> propChanged = db.stream()
                    .filter(t -> apiOrg.stream().anyMatch(a ->
                            t.getMasterStrukturOrganisasi().getIsActive() && now.after(t.getMasterStrukturOrganisasi().getStart()) && now.before(t.getMasterStrukturOrganisasi().getEnd())
                                    && a.getCode().equals(t.getCode())
                                    && a.getParent().equals(t.getParent())
                                    && (!a.getName().equals(t.getName()) || !Optional.ofNullable(a.getSingkatan()).orElse("")
                                    .equals(Optional.ofNullable(t.getSingkatan()).orElse(""))
                                    || !a.getOrganizationDto().getLevel().equals(t.getMasterStrukturOrganisasi().getLevel())
                                    || a.isChief() != t.isChief() || a.isPosisi() != t.isPosisi()
                                    || !Optional.ofNullable(a.getClazz()).orElse("").equals(Optional.ofNullable(t.getClazz()).orElse(""))
                                    || !Optional.ofNullable(a.getGrade()).orElse("").equals(Optional.ofNullable(t.getGrade()).orElse(""))
                                    || !Optional.ofNullable(a.getKorsa()).orElse("").equals(Optional.ofNullable(t.getKorsa()).orElse(""))
                            ))).collect(Collectors.toList());
            loggerService.info(syncLog, "Updated Organization count: " + propChanged.size());

            propChanged
                    .forEach(t -> {
                        OrganizationWrapper updated = apiOrg.stream().filter(a -> a.getCode().equals(t.getCode())).findFirst().orElse(new OrganizationWrapper());
                        MasterGrade masterGrade = getOrCreateMasterGrade(masterGrades, updated.getGrade(), updated.getGrade(), company);
                        MasterKorsa masterKorsa = masterKorsas.stream().filter(q->q.getId().equals(updated.getKorsa())).findFirst().orElse(null);

                        MasterStrukturOrganisasi masterStrukturOrganisasi = t.getMasterStrukturOrganisasi();
                        masterStrukturOrganisasi.setOrganizationName(updated.getName());
                        masterStrukturOrganisasi.setLevel(updated.getOrganizationDto().getLevel());
                        masterStrukturOrganisasi.setSingkatan(updated.getSingkatan());
                        masterStrukturOrganisasi.setIsChief(Boolean.valueOf(updated.isChief()));
                        masterStrukturOrganisasi.setIsPosisi(Boolean.valueOf(updated.isPosisi()));
                        masterStrukturOrganisasi.setClazz(updated.getClazz());
                        masterStrukturOrganisasi.setGrade(masterGrade);

                        if(masterKorsa!=null){
                            masterStrukturOrganisasi.setUnit(masterKorsa);
                        }

                        getEntityManager().merge(masterStrukturOrganisasi);
                    });

            if (propChanged.size() > 0) {
                flushEntityManager();
            }

            //DELETED
            List<OrganizationWrapper> deleted = db.stream()
                    .filter(t ->
                            t.getMasterStrukturOrganisasi().getIsActive() && now.after(t.getMasterStrukturOrganisasi().getStart()) && now.before(t.getMasterStrukturOrganisasi().getEnd())
                                    && (apiOrg.stream().noneMatch(a -> a.getCode().equals(t.getCode()) && a.getParent().equals(t.getParent())) //belum ada
                                    || apiOrg.stream().anyMatch(a -> a.getCode().equals(t.getCode()) && !a.getParent().equals(t.getParent()))) //ada, tapi pindah, tetap dianggap terhapus
                    )
                    .collect(Collectors.toList());
            loggerService.info(syncLog, "Deleted Organization count: " + deleted.size());

            deleted
                    .forEach(t -> {
                        MasterStrukturOrganisasi masterStrukturOrganisasi = t.getMasterStrukturOrganisasi();

                        if(masterStrukturOrganisasi.getUser()!=null) {
                            MasterUser user = masterStrukturOrganisasi.getUser();
                            user.setOrganizationEntity(null);
                            masterUserService.edit(user);
                        }

                        masterStrukturOrganisasi.setIsActive(false);
                        masterStrukturOrganisasi.setIsDeleted(true);
                        masterStrukturOrganisasi.setEnd(now);

                        getEntityManager().merge(masterStrukturOrganisasi);
                    });

            if (deleted.size() > 0) {
                flushEntityManager();
            }

            //NOT ACTIVATED
            List<OrganizationWrapper> inactive = db
                    .stream()
                    .filter(t -> !(t.getMasterStrukturOrganisasi().getIsActive() && now.after(t.getMasterStrukturOrganisasi().getStart()) && now.before(t.getMasterStrukturOrganisasi().getEnd())))
                    .collect(Collectors.toList());

            //CREATED
            int[] batchIndex = {0};
            List<OrganizationWrapper> created = apiOrg.stream()
                    .filter(a -> db.stream().noneMatch(t ->
                            t.getMasterStrukturOrganisasi().getIsActive() && now.after(t.getMasterStrukturOrganisasi().getStart()) && now.before(t.getMasterStrukturOrganisasi().getEnd())
                                    && a.getCode().equals(t.getCode()) && a.getParent().equals(t.getParent())
                    ))
                    .sorted(Comparator.comparing(t -> t.getOrganizationDto().getLevel()))
                    .collect(Collectors.toList());
            loggerService.info(syncLog, "Created Organization count: " + created.size());

            created
                    .forEach(t -> {
                        OrganizationWrapper organizationWrapper = inactive.stream().filter(a -> t.getCode().equals(a.getCode())).findFirst().orElse(null);
                        MasterGrade masterGrade = getOrCreateMasterGrade(masterGrades, t.getGrade(), t.getGrade(), company);
                        MasterKorsa masterKorsa = masterKorsas.stream().filter(q->q.getId().equals(t.getKorsa())).findFirst().orElse(null);
                        MasterStrukturOrganisasi parent = db
                                .stream()
                                .filter(d -> d.getCode().equals(t.getParent()))
                                .map(OrganizationWrapper::getMasterStrukturOrganisasi)
                                .findFirst()
                                .orElse(null);

                        if (parent != null || t.getParent().equals("00000000")) {
                            if (organizationWrapper != null) { //re-use inactivated org
                                MasterStrukturOrganisasi masterStrukturOrganisasi = organizationWrapper.getMasterStrukturOrganisasi();
                                masterStrukturOrganisasi.setEnd(dateUtil.getDefValue());
                                masterStrukturOrganisasi.setIsActive(true);
                                masterStrukturOrganisasi.setIsDeleted(false);
                                masterStrukturOrganisasi.setOrganizationName(t.getName());
                                masterStrukturOrganisasi.setSingkatan(t.getSingkatan());
                                masterStrukturOrganisasi.setLevel(Optional.ofNullable(t.getOrganizationDto().getLevel()).orElse(Optional.ofNullable(parent).map(p -> p.getLevel() + 1).orElse(0)));
                                masterStrukturOrganisasi.setParent(parent);
                                masterStrukturOrganisasi.setIsChief(Boolean.valueOf(t.isChief()));
                                masterStrukturOrganisasi.setIsPosisi(Boolean.valueOf(t.isPosisi()));
                                if(!Strings.isNullOrEmpty(t.getClazz())){
                                    masterStrukturOrganisasi.setClazz(t.getClazz());
                                }
                                if(masterGrade!=null) {
                                    masterStrukturOrganisasi.setGrade(masterGrade);
                                }
                                if(masterKorsa!=null){
                                    masterStrukturOrganisasi.setUnit(masterKorsa);
                                }
                                getEntityManager().merge(masterStrukturOrganisasi);
                            } else { //completely new
                                MasterStrukturOrganisasi masterStrukturOrganisasi = new MasterStrukturOrganisasi();
                                masterStrukturOrganisasi.setStart(now);
                                masterStrukturOrganisasi.setEnd(dateUtil.getDefValue());
                                masterStrukturOrganisasi.setIsActive(true);
                                masterStrukturOrganisasi.setIsDeleted(false);
                                masterStrukturOrganisasi.setOrganizationCode(t.getCode());
                                masterStrukturOrganisasi.setOrganizationName(t.getName());
                                masterStrukturOrganisasi.setSingkatan(t.getSingkatan());
                                masterStrukturOrganisasi.setLevel(Optional.ofNullable(t.getOrganizationDto().getLevel()).orElse(Optional.ofNullable(parent).map(p -> p.getLevel() + 1).orElse(0)));
                                masterStrukturOrganisasi.setParent(parent);
                                masterStrukturOrganisasi.setIsChief(Boolean.valueOf(t.isChief()));
                                masterStrukturOrganisasi.setIsPosisi(Boolean.valueOf(t.isPosisi()));
                                if(!Strings.isNullOrEmpty(t.getClazz())){
                                    masterStrukturOrganisasi.setClazz(t.getClazz());
                                }
                                if(masterGrade!=null) {
                                    masterStrukturOrganisasi.setGrade(masterGrade);
                                }
                                if(masterKorsa!=null){
                                    masterStrukturOrganisasi.setUnit(masterKorsa);
                                }
                                getEntityManager().persist(masterStrukturOrganisasi);
                                db.add(new OrganizationWrapper(masterStrukturOrganisasi, t.getOrganizationDto(), masterStrukturOrganisasi.getOrganizationCode(), masterStrukturOrganisasi.getOrganizationName(), masterStrukturOrganisasi.getSingkatan(), Optional.ofNullable(masterStrukturOrganisasi.getParent()).map(MasterStrukturOrganisasi::getOrganizationCode).orElse("00000000"),masterStrukturOrganisasi.getIsPosisi(),masterStrukturOrganisasi.getIsChief(),Optional.ofNullable(masterStrukturOrganisasi.getArea()).map(MasterArea::getId).orElse(null),Optional.ofNullable(masterStrukturOrganisasi.getCompanyCode()).map(CompanyCode::getCode).orElse(null),Optional.ofNullable(masterStrukturOrganisasi.getClazz()).orElse(null),Optional.ofNullable(masterStrukturOrganisasi.getGrade()).map(a->a.getId()).orElse(null),Optional.ofNullable(masterStrukturOrganisasi.getUnit()).map(a->a.getId()).orElse(null)));
                            }
                        } else {
                            System.out.println("");
                        }

                        if ((batchIndex[0] % 30) == 0) {
                            flushEntityManager();
                        }

                        batchIndex[0] = batchIndex[0] + 1;
                    });

            if ((batchIndex[0] % 30) != 0) {
                flushEntityManager();
            }

            //COMPANY AND PERSONAL AREA
            List<OrganizationWrapper> companyArea = db.stream()
                    .filter(t ->
                            apiOrg.stream().anyMatch(a ->
                                    t.getMasterStrukturOrganisasi().getIsActive() && now.after(t.getMasterStrukturOrganisasi().getStart())
                                            && now.before(t.getMasterStrukturOrganisasi().getEnd())
                                            && a.getCode().equals(t.getCode())
                                            && (t.getCompany()==null || t.getPersa()==null || (a.getPersa()!=null && !a.getPersa().equals(Optional.ofNullable(t.getPersa()).orElse(null))) || (!Optional.ofNullable(a.getCompany()).orElse(null).equals(Optional.ofNullable(t.getCompany()).orElse(null))))
                            )).collect(Collectors.toList());
            loggerService.info(syncLog, "Updated Company & Persa Organization count: " + companyArea.size());

            companyArea.forEach(t -> {

                MasterStrukturOrganisasi masterStrukturOrganisasi = t.getMasterStrukturOrganisasi();
                MasterStrukturOrganisasi parent = t.getMasterStrukturOrganisasi().getParent();

                OrganizationWrapper updated = apiOrg.stream().filter(a -> a.getCode().equals(t.getCode())).findFirst().orElse(null);
                if(updated!=null) {
                    if(!Strings.isNullOrEmpty(updated.getCompany())) {
                        if(company!=null) {
                            masterStrukturOrganisasi.setCompanyCode(company);
                        }
                    }

                    if(!Strings.isNullOrEmpty(updated.getPersa())){
                        MasterArea area = areas.stream().filter(a->a.getId().equals(updated.getPersa())).findFirst().orElse(null);
                        if(area!=null){
                            masterStrukturOrganisasi.setArea(area);
                        }
                    }else{
                        if(parent!=null){
                            MasterArea pArea = areas.stream().filter(a->a.getId().equals(parent.getArea().getId())).findFirst().orElse(null);
                            if(pArea!=null){
                                masterStrukturOrganisasi.setArea(pArea);
                            }
                        }
                    }
                    getEntityManager().merge(masterStrukturOrganisasi);
                }
            });

            Instant end = Instant.now();
            loggerService.info(syncLog, String.format("Organization sync completed on: %s seconds", Duration.between(start, end).getSeconds()));
            loggerService.info(syncLog, String.format("End saving organization | %s", new Date()));
        } catch (Exception e) {
            loggerService.error(syncLog, e);
        }
    }

    private void flushEntityManager() {
//        try {
//            getEntityManager().flush();
//        } catch (Exception e){
//
//        }
    }

    public JPAJinqStream<MasterStrukturOrganisasi> getOrgBy(String nama, String nipp, String idGrade, String idKorsa, String posisi, String idPersa) {
        Date n = new Date();
        JPAJinqStream<MasterStrukturOrganisasi> query = db().where(q -> q.getIsDeleted().booleanValue()!=true && q.getIsActive().booleanValue()==true && n.after(q.getStart()) && n.before(q.getEnd())
                && !q.getOrganizationCode().equals("99999999"));

            query = query.where(b-> b.getUser()!=null && b.getUser().getIsDeleted().booleanValue()!=true && b.getUser().getIsActive().booleanValue()==true);

            if (!Strings.isNullOrEmpty(nama)) {
                query = query.where(a -> a.getUser().getNameFront().contains(nama.toUpperCase()) || a.getUser().getNameMiddleLast().contains(nama.toUpperCase()) ||
                        (a.getUser().getNameFront() + " " + a.getUser().getNameMiddleLast()).contains(nama.toUpperCase()) ||
                        a.getUser().getLoginUserName().contains(nama));
            }

            if (!Strings.isNullOrEmpty(nipp)) {
                query = query.where(a -> a.getUser().getEmployeeId().equals(nipp));
            }

            if (!Strings.isNullOrEmpty(idGrade)) {
                query = query.where(a-> a.getUser().getGrade() != null);
                query = query.where(a -> a.getUser().getGrade().getId().equals(idGrade));
            }

            if (!Strings.isNullOrEmpty(idKorsa)) {
                query = query.where(a -> a.getUser().getKorsa() != null);
                query = query.where(a -> a.getUser().getKorsa().getId().equals(idKorsa));
            }

            if (!Strings.isNullOrEmpty(posisi)) {
                query = query.where(a -> a.getOrganizationName().toLowerCase().contains(posisi.toLowerCase()));
            }

            if(!Strings.isNullOrEmpty(idPersa)){
                query = query.where(a -> a.getUser().getArea()!=null && a.getUser().getArea().getId().equals(idPersa));
            }
        return query;
    }

    public MasterStrukturOrganisasi save(Long id, MasterStrukturOrganisasiDto dao) throws Exception {
        MasterStrukturOrganisasi model = new MasterStrukturOrganisasi();
        DateUtil dateUtil = new DateUtil();
        try {
            if (id == null) {
                model.setOrganizationName(dao.getOrganizationName());
                model.setOrganizationCode("x");
                model.setLevel(0);
                model.setIsActive(dao.getIsActive());
                model.setIsDeleted(false);
                if (dao.getParentId() != null) {
                    MasterStrukturOrganisasi parent = this.find(dao.getParentId());
                    model.setParent(parent);
                } else {
                    model.setParent(null);
                }

                if (dao.getCompanyId() != null) {
                    CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                    model.setCompanyCode(cCode);
                } else {
                    model.setCompanyCode(null);
                }
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if (dao.getEndDate() != null) {
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                } else {
                    model.setEnd(dateUtil.getDefValue());
                }
                model.setIsHris(false);
                model.setIsPosisi(dao.getIsPosisi());
                model.setIsChief(dao.getIsChief());
                this.create(model);

                getEntityManager().flush();
                getEntityManager().refresh(model);

                /*save to non struktural tabel*/
                if(model.getParent()!=null && model.getParent().getIsHris().booleanValue()==false && dao.getIdUser()!=null){
                    MasterUser user = masterUserService.find(dao.getIdUser());
                    MasterNonStruktural nonStruktural = new MasterNonStruktural();
                    nonStruktural.setOrganisasi(model);
                    nonStruktural.setUser(user);
                    nonStruktural.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                    if (dao.getEndDate() != null) {
                        nonStruktural.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                    } else {
                        nonStruktural.setEnd(dateUtil.getDefValue());
                    }
                    nonStrukturalService.create(nonStruktural);
                }else{
                    if(model.getParent()!=null && model.getParent().getIsHris().booleanValue()==true && dao.getIdUser()!=null){
                        MasterUser user = masterUserService.find(dao.getIdUser());
                        user.setOrganizationEntity(model);
                        masterUserService.edit(user);
                        model.setUser(user);
                        edit(model);
                    }
                }

            } else {
                model = this.find(id);

                if(dao.getIsHris().booleanValue()){
                    if(dao.getIdUser()!=null){
                        MasterUser user = masterUserService.find(dao.getIdUser());
                        user.setOrganizationEntity(model);
                        user.setJabatan(model.getOrganizationName());
                        user.setKedudukan(model.getOrganizationName());
                        masterUserService.edit(user);
                        model.setUser(user);
                        edit(model);
                    }
                }else {
                    model.setOrganizationName(dao.getOrganizationName());
//                model.setOrganizationCode(dao.getOrganizationCode());
//                model.setLevel(dao.getLevel());
                    model.setIsActive(dao.getIsActive());
                    model.setIsDeleted(false);
                    if (dao.getParentId() != null) {
                        MasterStrukturOrganisasi parent = this.find(dao.getParentId());
                        model.setParent(parent);
                    } else {
                        model.setParent(null);
                    }

                    if (dao.getCompanyId() != null) {
                        CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                        model.setCompanyCode(cCode);
                    } else {
                        model.setCompanyCode(null);
                    }

                    model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

                    if (dao.getEndDate() != null) {
                        model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                    } else {
                        model.setEnd(dateUtil.getDefValue());
                    }
                    model.setIsHris(false);
                    model.setIsPosisi(dao.getIsPosisi());
                    model.setIsChief(dao.getIsChief());
                    this.edit(model);

                    getEntityManager().flush();
                    getEntityManager().refresh(model);

                    /*save to non struktural tabel*/
                    if (model.getParent() != null && model.getParent().getIsHris().booleanValue() == false && dao.getIdUser() != null) {
                        MasterUser user = masterUserService.find(dao.getIdUser());
                        MasterNonStruktural nonStruktural = nonStrukturalService.getByOrg(model.getIdOrganization());
                        if (nonStruktural == null) {
                            nonStruktural = new MasterNonStruktural();
                            nonStruktural.setOrganisasi(model);
                            nonStruktural.setUser(user);
                            nonStruktural.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                            if (dao.getEndDate() != null) {
                                nonStruktural.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                            } else {
                                nonStruktural.setEnd(dateUtil.getDefValue());
                            }
                            nonStrukturalService.create(nonStruktural);
                        } else {
                            nonStruktural.setOrganisasi(model);
                            nonStruktural.setUser(user);
                            nonStruktural.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                            if (dao.getEndDate() != null) {
                                nonStruktural.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                            } else {
                                nonStruktural.setEnd(dateUtil.getDefValue());
                            }
                            nonStrukturalService.edit(nonStruktural);
                        }
                    } else {
                        if (model.getParent() != null && model.getParent().getIsHris().booleanValue() == false && dao.getIdUser() == null) {
                            MasterNonStruktural nonStruktural = nonStrukturalService.getByOrg(model.getIdOrganization());
                            if (nonStruktural != null) {
                                nonStruktural.setUser(null);
                                nonStrukturalService.edit(nonStruktural);
                            }
                        } else {
                            if (model.getParent() != null && model.getParent().getIsHris().booleanValue() == true && dao.getIdUser() != null) {
                                MasterUser user = masterUserService.find(dao.getIdUser());
                                user.setOrganizationEntity(model);
                                user.setJabatan(model.getOrganizationName());
                                user.setKedudukan(model.getOrganizationName());
                                masterUserService.edit(user);
                                model.setUser(user);
                                edit(model);
                            } else {
                                if (model.getParent() != null && model.getParent().getIsHris().booleanValue() == true && dao.getIdUser() == null) {
                                    MasterUser lastUser = masterUserService.find(masterUserService.getByOrgId(model.getIdOrganization()).getId());
                                    lastUser.setOrganizationEntity(null);
                                    lastUser.setJabatan("No Position");
                                    lastUser.setKedudukan("No Position");
                                    masterUserService.edit(lastUser);
                                    model.setUser(null);
                                    edit(model);
                                }
                            }
                        }
                    }
                }
            }

            return model;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterStrukturOrganisasi delete(Long id) throws Exception {
        try {
            MasterStrukturOrganisasi model = this.find(id);
            if(model.getParent() != null && model.getParent().getIsHris().booleanValue()==false){
                MasterNonStruktural nonStruktural = nonStrukturalService.getByOrg(model.getIdOrganization());
                if(nonStruktural!=null) {
                    nonStrukturalService.remove(nonStruktural);
                }

                model.setIsDeleted(true);
                model.setIsActive(false);
                this.edit(model);
            }else {
                model.setIsDeleted(true);
                model.setIsActive(false);
                this.edit(model);

                /*edit organisasi di user*/
                MasterUser user = model.getUser();
                if (user != null) {
                    user.setOrganizationEntity(null);
                    user.setKedudukan("No Position");
                    user.setJabatan("No Position");
                    masterUserService.edit(user);
                }
            }
            return model;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public List<MasterStrukturOrganisasiDto> validateGrade(List<MasterStrukturOrganisasiDto> data, KewenanganPenerimaDto dao){
        List<MasterStrukturOrganisasiDto>result = new ArrayList<>();
        List<String> grade = new ArrayList<>();
        List<String> gradeTemp = new ArrayList<>();
        if(!data.isEmpty()){
            for(Long ids:dao.getIdsPenandatangan()){
                MasterStrukturOrganisasi org = this.find(ids);
                if(org.getUser()!=null){
                    if(org.getUser().getGrade()!=null){
                        gradeTemp.add(org.getUser().getGrade().getId());
                    }
                }
            }
            if(!gradeTemp.isEmpty()){
                String gradePengirim = StringUtils.join(gradeTemp,",");
                grade = Arrays.asList(kewenanganPenerimaService.getKewenanganPenerima(gradePengirim, dao.getIdJenisDokumen())
                        .split("\\s*,\\s*"));

                for (MasterStrukturOrganisasiDto so : data) {
                    MasterUserDto tmpUser = so.getUser();
                    if(tmpUser!=null) {
                        if (tmpUser.getGrade() != null) {
                            if (grade.contains(tmpUser.getGrade().getId())) {
                                result.add(so);
                            }else{
                                so.setUser(new MasterUserDto());
                                result.add(so);
                            }
                        }else{
                            so.setUser(new MasterUserDto());
                            result.add(so);
                        }
                    }else{
                        so.setUser(new MasterUserDto());
                        result.add(so);
                    }
                }
            }
        }
        return result;
    }

    public MasterStrukturOrganisasi getByName(String entityName) {
        MasterStrukturOrganisasi org = new MasterStrukturOrganisasi();
        if (!Strings.isNullOrEmpty(entityName)) {
            org = db().where(t -> (t.getIsActive() || !t.getIsDeleted()) && t.getOrganizationName().toLowerCase().equals(entityName.toLowerCase())).findFirst().orElse(null);
        }
        return org;
    }

    public String validate(Long id, String nama, String kode, Long idParent) {
        String result;
        JPAJinqStream<MasterStrukturOrganisasi> query = db().where(a -> !a.getIsDeleted());
        boolean isNama = false;
        if(id==null){
            if(idParent!=null){
                List<MasterStrukturOrganisasi> tmp = query.where(b -> b.getParent()!=null && b.getParent().getIdOrganization().equals(idParent) &&
                        (b.getOrganizationName().toLowerCase().equals(nama.toLowerCase())))
                        .collect(Collectors.toList());
                if(!tmp.isEmpty()) {
                    isNama = tmp.stream().anyMatch(c -> c.getOrganizationName().toLowerCase().equals(nama.toLowerCase()));
                }
            }else{
                List<MasterStrukturOrganisasi> tmp = query.where(b -> b.getParent()==null && (b.getOrganizationName().toLowerCase().equals(nama.toLowerCase())
                       )).collect(Collectors.toList());
                if(!tmp.isEmpty()) {
                    isNama = tmp.stream().anyMatch(c -> c.getOrganizationName().toLowerCase().equals(nama.toLowerCase()));
                }
            }
        }else {
            if(idParent==null){
                List<MasterStrukturOrganisasi> tmp = query.where(b -> b.getIdOrganization() != id && b.getParent()==null && (b.getOrganizationName().toLowerCase().equals(nama.toLowerCase())
                       )).collect(Collectors.toList());
                if (!tmp.isEmpty()) {
                    isNama = tmp.stream().anyMatch(c -> c.getOrganizationName().toLowerCase().equals(nama.toLowerCase()));
                }
            }else {
                List<MasterStrukturOrganisasi> tmp = query.where(b -> b.getIdOrganization() != id && b.getParent().getIdOrganization().equals(idParent) && (b.getOrganizationName().toLowerCase().equals(nama.toLowerCase())
                        )).collect(Collectors.toList());
                if (!tmp.isEmpty()) {
                    isNama = tmp.stream().anyMatch(c -> c.getOrganizationName().toLowerCase().equals(nama.toLowerCase()));
                }
            }
        }

        result =  isNama ? "nama" : "" ;
        return result;
    }

    private MasterGrade getOrCreateMasterGrade(List<MasterGrade> masterGrades, String id, String name, CompanyCode companyCode) {
        MasterGrade masterKorsa = null;

        if (org.apache.commons.lang3.StringUtils.isNotEmpty(id) && org.apache.commons.lang3.StringUtils.isNotEmpty(name)) {
            masterKorsa = masterGrades.stream().filter(t -> t.getId().equals(id)).findFirst().orElse(null);

            if (masterKorsa == null) {
                Date now = new Date();
                DateUtil dateUtil = new DateUtil();
                masterKorsa = new MasterGrade();
                masterKorsa.setId(id);
                masterKorsa.setNama(name);
                masterKorsa.setStart(now);
                masterKorsa.setEnd(dateUtil.getDefValue());
                masterKorsa.setCompanyCode(companyCode);

                getEntityManager().persist(masterKorsa);

                masterGrades.add(masterKorsa);
            } else if (
                    !Optional.ofNullable(name).orElse("").equals(Optional.ofNullable(masterKorsa.getNama()).orElse(""))
                            || !Optional.ofNullable(masterKorsa.getCompanyCode()).orElse(new CompanyCode(-1L)).getId().equals(Optional.ofNullable(companyCode).orElse(new CompanyCode(-1L)).getId())) {
                masterKorsa.setCompanyCode(companyCode);
                masterKorsa.setNama(name);

                getEntityManager().merge(masterKorsa);
            }
        }

        return masterKorsa;
    }
}
