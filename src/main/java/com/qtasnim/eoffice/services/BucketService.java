package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.BucketDto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class BucketService extends AbstractFacade<Bucket> {

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyService;

    @Inject
    private BoardService boardService;

    @Override
    protected Class<Bucket> getEntityClass() {
        return Bucket.class;
    }


    public Bucket saveOrEdit(BucketDto dao, Long id) throws Exception {
        try{
            Bucket obj = new Bucket();
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = companyService.getByCode(user.getCompanyCode().getCode());
            if(id==null){
                obj.setTitle(dao.getTitle());
                obj.setIsDeleted(false);
                obj.setCompanyCode(company);
                if(dao.getBoardId()!=null){
                    Board board = boardService.find(dao.getBoardId());
                    obj.setBoard(board);
                }
                if(dao.getSort()!=null){
                    obj.setSort(dao.getSort());
                }
                create(obj);
            }else{
                obj = find(id);
                obj.setTitle(dao.getTitle());
                obj.setIsDeleted(false);
                obj.setCompanyCode(company);
                if(dao.getBoardId()!=null){
                    Board board = boardService.find(dao.getBoardId());
                    obj.setBoard(board);
                }
                if(dao.getSort()!=null){
                    obj.setSort(dao.getSort());
                }
                edit(obj);
            }
            getEntityManager().flush();
            getEntityManager().refresh(obj);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public Bucket deleteBucket(Long id) throws Exception {
        try{
            Bucket obj = find(id);
            obj.setIsDeleted(true);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
}
