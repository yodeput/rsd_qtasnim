package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterNonStrukturalDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterNonStrukturalService extends AbstractFacade<MasterNonStruktural> {
    private static final long serialVersionUID = 1L;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterUserService userService;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    @Override
    protected Class<MasterNonStruktural> getEntityClass() {
        return MasterNonStruktural.class;
    }

    public JPAJinqStream<MasterNonStruktural> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public MasterNonStruktural getByUserLogin() {
        MasterUser user = userSession.getUserSession().getUser();
        Date n = new Date();
        return db().where(q -> q.getUser().equals(user) && (n.after(q.getStart()) && n.before(q.getEnd()))).findFirst().orElse(null);
    }


    public MasterNonStruktural save(Long id, MasterNonStrukturalDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterNonStruktural model = new MasterNonStruktural();

        if (id == null) {
            this.create(data(model, dao));
        } else {
            model = this.find(id);
            this.edit(data(model, dao));
        }

        return model;
    }

    public MasterNonStruktural data(MasterNonStruktural masterNonStruktural, MasterNonStrukturalDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterNonStruktural model = masterNonStruktural;
        model.setId(dao.getId());

        if (dao.getUserId() != null) {
            MasterUser user = userService.find(dao.getUserId());
            model.setUser(user);
        } else {
            model.setUser(null);
        }

        if (dao.getOrganisasiId() != null) {
            MasterStrukturOrganisasi organisasi = organisasiService.find(dao.getOrganisasiId());
            model.setOrganisasi(organisasi);
        } else {
            model.setOrganisasi(null);
        }

        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        return model;
    }

    public MasterNonStruktural delete(Long id) {
        MasterNonStruktural model = this.find(id);
        this.remove(model);
        return model;
    }

    public MasterNonStruktural getByOrgUser (Long idOrg,Long idUser){
        Date n = new Date();
        return this.db().where(a->n.after(a.getStart()) && n.before(a.getEnd()) &&
                a.getUser().getId().equals(idUser) && a.getOrganisasi().getIdOrganization().equals(idOrg))
                .findFirst().orElse(null);
    }

    public MasterNonStruktural getByOrg(Long idOrg){
        Date n = new Date();
        return this.db().where(a->n.after(a.getStart()) && n.before(a.getEnd()) && a.getOrganisasi().getIdOrganization().equals(idOrg))
                .findFirst().orElse(null);
    }
}