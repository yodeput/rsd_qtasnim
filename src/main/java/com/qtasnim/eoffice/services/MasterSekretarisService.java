package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterSekretaris;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterSekretarisDto;
import com.qtasnim.eoffice.ws.dto.MasterSekretarisDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service untuk fungsi CRUD table m_user
 * 
 * @author seno@qtasnim.com
 */


 @Stateless
 @LocalBean
public class MasterSekretarisService extends AbstractFacade<MasterSekretaris>{

    private static final long serialVersionUID = -4184209743573831159L;

    @Inject
    MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private CompanyCodeService companyCodeService;


    @Override
    protected Class<MasterSekretaris> getEntityClass() {
        return MasterSekretaris.class;
    }

    public JPAJinqStream<MasterSekretaris> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && !q.getIsDeleted());
    }
    
    public MasterSekretaris save(Long id, MasterSekretarisDto dao){
        MasterSekretaris model = new MasterSekretaris();
        DateUtil dateUtil = new DateUtil();
        if(id==null) {
            MasterStrukturOrganisasi mOrgPejabat = masterStrukturOrganisasiService.find(dao.getPejabatId());
            model.setPejabat(mOrgPejabat);

            MasterStrukturOrganisasi mOrgSekretaris = masterStrukturOrganisasiService.find(dao.getSekretarisId());
            model.setSekretaris(mOrgSekretaris);

            model.setTipePenerimaan(dao.getTipePenerimaan());
            model.setIsActive(true);
            model.setIsDeleted(false);

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.create(model);
        } else {
            model = this.find(id);
            MasterStrukturOrganisasi mOrgPejabat = masterStrukturOrganisasiService.find(dao.getPejabatId());
            model.setPejabat(mOrgPejabat);

            MasterStrukturOrganisasi mOrgSekretaris = masterStrukturOrganisasiService.find(dao.getSekretarisId());
            model.setSekretaris(mOrgSekretaris);
            model.setTipePenerimaan(dao.getTipePenerimaan());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }

      return model;
    }

    public MasterSekretaris delete(Long id){
        MasterSekretaris model = this.find(id);
        model.setIsActive(false);
        model.setIsDeleted(true);
        this.edit(model);
        
        return model;
    }

    public List<MasterStrukturOrganisasi> getPejabatBySekretaris(Long organizationId) {
        Date date = new Date();

        return db()
                .where(t ->
                        t.getSekretaris().getIdOrganization().equals(organizationId)
                                && t.getStart().before(date)
                                && date.before(t.getEnd())
                                && t.getIsActive()
                                && !t.getIsDeleted())
                .select(MasterSekretaris::getPejabat)
                .toList();
    }

    public Boolean isSecretary (Long idPejabat,Long idUser){
        Date date = new Date();
        return db()
                .anyMatch(t ->
                        t.getPejabat().getIdOrganization().equals(idPejabat)
                                && t.getSekretaris().getIdOrganization().equals(idUser)
                                && t.getStart().before(date)
                                && date.before(t.getEnd())
                                && t.getIsActive()
                                && !t.getIsDeleted());
    }
}