package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.PenerimaSurat;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class PenerimaSuratService extends AbstractFacade<PenerimaSurat>{

    @Override
    protected Class<PenerimaSurat> getEntityClass() {
        return PenerimaSurat.class;
    }

    @Inject
    private SuratService suratService;

    @Inject
    @ISessionContext
    private Session userSession;

    public List<PenerimaSurat> getAllBy(Long idSurat){
        Surat surat = suratService.find(idSurat);
        return db().where(q->q.getSurat().equals(surat)).toList();
    }

    public PenerimaSurat getByOrgAndIdSurat(Long idSurat){
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        Surat surat = suratService.find(idSurat);
        return db().where(q->q.getSurat().equals(surat) && q.getOrganization().equals(org)).findFirst().orElse(null);
    }

    public PenerimaSurat findPenerima(Long idOrganization,Long idSurat){
        return db().where(q->q.getOrganization().getIdOrganization().equals(idOrganization) && q.getSurat().getId().equals(idSurat)).findFirst().orElse(null);
    }
}
