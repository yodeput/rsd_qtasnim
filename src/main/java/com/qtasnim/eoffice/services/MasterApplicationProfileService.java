package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterApplicationProfile;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class MasterApplicationProfileService extends AbstractFacade<MasterApplicationProfile> {
    @Override
    protected Class<MasterApplicationProfile> getEntityClass() {
        return MasterApplicationProfile.class;
    }

    public MasterApplicationProfile getCurrentProfile() {
        return db().where(MasterApplicationProfile::getIsActive).findFirst().orElse(null);
    }
}
