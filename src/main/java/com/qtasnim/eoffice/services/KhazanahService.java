package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.ws.dto.*;
import org.apache.commons.compress.utils.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.criteria.*;
import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class KhazanahService extends AbstractFacade<Khazanah> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterNegaraService negaraService;

    @Inject
    private MasterBahasaService bahasaService;

    @Inject
    private MasterKlasifikasiKhazanahService klasifikasiKhazanahService;

    @Inject
    private MasterKlasifikasiKhazanahService masterKlasifikasiKhazanahService;

    @Inject
    private FileService fileService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private KhazanahActionHistoryService historyService;

    @Override
    protected Class<Khazanah> getEntityClass() {
        return Khazanah.class;
    }

    public JPAJinqStream<Khazanah> getAll() {
        return db();
    }

    public void saveOrEdit(KhazanahDto dao, Long id, List<KhazanahAttachmentDto> attachmentDtos, List<FormDataBodyPart> bodyParts, List<Long> deletedAttachmentIds) throws InternalServerErrorException, IOException, Exception {
        try {
            boolean isNew = id == null;
            Khazanah model = isNew ? new Khazanah() : this.find(id);
            model.setTipeKhazanah(TipeKhazanah.valueOf(dao.getTipeKhazanah().toUpperCase()).toString());
            model.setJudul(dao.getJudul());
            model.setDeskripsi(dao.getDeskripsi());
            model.setPenerbit(dao.getPenerbit());
            model.setTahunTerbit(dao.getTahunTerbit().shortValue());
            model.setPenulis(dao.getPenulis());
            model.setJmlHalaman(dao.getJmlHalaman());
            model.setLokasiSimpan(dao.getLokasiSimpan());
            model.setPenulisSkenario(dao.getPenulisSkenario());
            model.setDraft(dao.getDraft());
            model.setStatus(dao.getStatus());

            if (dao.getIdBahasa() != null) {
                MasterBahasa bahasa = bahasaService.find(dao.getIdBahasa());
                model.setBahasa(bahasa);
            }

            if (dao.getIdNegara() != null) {
                MasterNegara negara = negaraService.find(dao.getIdNegara());
                model.setNegara(negara);
            }

            if (dao.getIdKlasifikasiKhazanah() != null) {
                MasterKlasifikasiKhazanah klasifikasiKhazanah = klasifikasiKhazanahService.find(dao.getIdKlasifikasiKhazanah());
                model.setKlasifikasiKhazanah(klasifikasiKhazanah);
            }

            if (dao.getIdCompany() != null) {
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
            }

            if (dao.getJenisBuku() != null) {
                model.setJenisBuku(JenisBuku.valueOf(dao.getJenisBuku().toUpperCase()).toString());
            }

            if (!Strings.isNullOrEmpty(dao.getDurasi())) {
                DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                Date dur = sdf.parse(dao.getDurasi());
                model.setDurasi(dur);
            }

            if (dao.getMedia() != null) {
                model.setMedia(Media.valueOf(dao.getMedia().toUpperCase()).toString());
            }


            if (isNew) {
                this.create(model);
                getEntityManager().flush();
                getEntityManager().refresh(model);

                this.createAttachments(model, attachmentDtos, bodyParts);
            } else {
                this.edit(model);

                this.deleteAttachments(deletedAttachmentIds);
                this.createAttachments(model, attachmentDtos, bodyParts);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteAttachments(List<Long> deletedAttachmentIds) {
        if (deletedAttachmentIds != null) {
            deletedAttachmentIds.forEach((fileId) -> {
                GlobalAttachment attachment = globalAttachmentService.find(fileId);
                if (attachment != null) {
                    fileService.delete(attachment.getDocId());
                    attachment.setIsDeleted(true);
                    globalAttachmentService.edit(attachment);
                    getEntityManager().flush();
                    getEntityManager().refresh(attachment);
                }
            });
        }
    }

    private void createAttachments(Khazanah model, List<KhazanahAttachmentDto> attachmentDtos, List<FormDataBodyPart> bodyParts) {
        if (bodyParts != null) {
            bodyParts.forEach((part) -> {
                String fileName = part.getContentDisposition().getFileName();
                if (!Strings.isNullOrEmpty(fileName)) {

                    KhazanahAttachmentDto attachDto = attachmentDtos.stream().filter(q -> q.getFileName().equals(fileName)).findFirst().orElse(null);
                    if (attachDto != null) {
                        String tipe = attachDto.getTipe().toLowerCase();
                        InputStream inputStream = part.getValueAs(InputStream.class);
                        String number = String.format("%s_%s-%s", attachDto.getTipe(), model.getTipeKhazanah(), model.getId());
                        try {
                            GlobalAttachment attachment = globalAttachmentService
                                    .getAllDataByRef("t_khazanah", model.getId())
                                    .where(q -> q.getNumber().toLowerCase().contains(tipe))
                                    .findFirst().orElse(null);
                            if (attachment == null) {
                                this.uploadAttachment(model, number, inputStream, part);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            });
        }
    }

    public JPAJinqStream<Khazanah> getByTipe(String type) {
        return db().where(a -> a.getTipeKhazanah().equals(type));
    }

    public JPAJinqStream<Khazanah> getByKlasifikasi(Long idKlasifikasi) {
        return db().where(q -> q.getKlasifikasiKhazanah().getId().equals(idKlasifikasi));
    }

    public JPAJinqStream<Khazanah> getByTipeAndKlasifikasi(String type, Long idKlasifikasi) {
        String tipeKhazanah = type.toUpperCase();
        return db().where(q -> q.getTipeKhazanah().equals(tipeKhazanah) && q.getKlasifikasiKhazanah().getId().equals(idKlasifikasi));
    }

    private KhazanahPageDto populatePaging(Long id) {
        KhazanahPageDto dto = new KhazanahPageDto();
        dto.setIdKlasifikasi(id);
        dto.setLimit(5);
        dto.setSkip(0);

        return dto;
    }

    private Short convertToShort(String filter) {
        Short result;

        try {
            result = Short.parseShort(filter);
        } catch (NumberFormatException e) {
            result = null;
        }

        return result;
    }

    private JPAJinqStream<Khazanah> filtering(JPAJinqStream<Khazanah> all, String filter) {
        String filterStr = filter.toLowerCase();

//        if (year == null) {
            return all.where(q -> q.getJudul().toLowerCase().contains(filterStr) ||
                    q.getPenerbit().toLowerCase().contains(filterStr) ||
                    q.getPenulis().toLowerCase().contains(filterStr) ||
                    q.getPenulisSkenario().toLowerCase().contains(filterStr) ||
                    q.getTahunTerbit().toString().contains(filterStr));
//        } else {
//            return all.where(q -> );
//        }
    }

    private CriteriaQuery<Khazanah> getAllQuery(String filterBy, String tipeKhazanah, Long idKlasifikasi) {
        try {
            // Exec Query
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Khazanah> q = cb.createQuery(Khazanah.class);
            Root<Khazanah> root = q.from(Khazanah.class);

            q.select(root);

            List<Predicate> predicates = new ArrayList<>();

            Predicate mainCond = cb.and(
                            cb.equal(root.get("klasifikasiKhazanah").get("id"), idKlasifikasi)
                            , cb.equal(root.get("tipeKhazanah"), tipeKhazanah)
            );
            predicates.add(mainCond);

            if (!Strings.isNullOrEmpty(filterBy)) {
                Predicate pkeyword = cb.or(
                        cb.like(root.get("judul"), "%" + filterBy + "%")
                        , cb.like(root.get("penerbit"), "%" + filterBy + "%")
                        , cb.like(root.get("penulis"), "%" + filterBy + "%")
                        , cb.like(root.get("penulisSkenario"), "%" + filterBy + "%")
                        , cb.like(root.get("tahunTerbit").as(String.class), "%" + filterBy + "%")
                );
                predicates.add(pkeyword);
            }

            q.where(predicates.toArray(new Predicate[predicates.size()]));
            q.distinct(true);

            return q;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private List<Khazanah> execCriteriaQuery(CriteriaQuery<Khazanah> criteriaQuery, int skip, int limit) {
        try {
            return getEntityManager().createQuery(criteriaQuery).setFirstResult(skip).setMaxResults(limit).getResultList();
        } catch (Exception ex) {
            return new ArrayList<>();
        }

    }

    private Long countCriteriaQuery(CriteriaQuery<Khazanah> criteriaQuery) {
        try {
            return getEntityManager().createQuery(criteriaQuery).getResultStream().count();
        } catch (Exception ex) {
            return 0L;
        }

    }

//    public List<KhazanahListDto> getByTipeKhazanah(String tipe, String filter, Short year, List<KhazanahPageDto> pagingDto) {
    public List<KhazanahListDto> getByTipeKhazanah(String tipe, String filter, List<KhazanahPageDto> pagingDto) {
        List<KhazanahListDto> result = new ArrayList<>();
        JPAJinqStream<MasterKlasifikasiKhazanah> source = masterKlasifikasiKhazanahService.getByParentName(tipe, null, "nama", false);
        if (source != null) {
            result = source.map(KhazanahListDto::new).collect(Collectors.toList());
            result.forEach(item -> {
                List<KhazanahDto> children = null;
                KhazanahPageDto paging;
                if (pagingDto.isEmpty()) {
                    paging = this.populatePaging(item.getIdKlasifikasi());
                } else {
                    paging = pagingDto.stream().filter(q -> q.getIdKlasifikasi().equals(item.getIdKlasifikasi())).findFirst().orElse(this.populatePaging(item.getIdKlasifikasi()));
                }

                Long count = 0L;
                CriteriaQuery<Khazanah> allKhazanah = this.getAllQuery(filter, tipe, item.getIdKlasifikasi());

                try {
                    count = this.countCriteriaQuery(allKhazanah);
                    children = this.execCriteriaQuery(allKhazanah, paging.getSkip(), paging.getLimit()).stream().map(KhazanahDto::new).collect(Collectors.toList());
                    this.setFiles(children);
                } finally {
                    item.setCount(count);
                    item.setChildren(children);
                }
            });
        }

        return result;
    }

    public void uploadAttachment(Khazanah khazanah, String number, InputStream inputStream, FormDataBodyPart body) throws Exception {
        try {
            byte[] media = IOUtils.toByteArray(inputStream);
            String fileName = body.getContentDisposition().getFileName(),
                    fileType = body.getMediaType().toString(),
                    tmpDoc = "", jsonRepresentation = "";

            //<editor-fold desc="ocr" >
            if (fileType.toLowerCase().contains("pdf")) {
                byte[] pdfOcr = Arrays.copyOf(media, media.length);
                String tempFileName = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID().toString() + ".pdf";
                FileOutputStream fos = new FileOutputStream(tempFileName);
                fos.write(pdfOcr);
                fos.close();
                File tempFile = new File(tempFileName);

                List<String> extractedText = textExtractionService.extractText(tempFile);
                jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

                Files.deleteIfExists(tempFile.toPath());
            }
            //</editor-fold>


            tmpDoc = fileService.upload(fileName, media);
            tmpDoc = tmpDoc.replace("workspace://SpacesStore/", "");

            GlobalAttachment obj = new GlobalAttachment();
            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString());
            obj.setDocName(fileName);
            obj.setCompanyCode(khazanah.getCompanyCode());
            obj.setDocId(tmpDoc);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setIsDeleted(false);
            obj.setSize((long) media.length);
            obj.setReferenceId(khazanah.getId());
            obj.setReferenceTable("t_khazanah");
            obj.setNumber(number);

            if (fileType.contains("pdf")) {
                obj.setExtractedText(jsonRepresentation);
            }
            globalAttachmentService.create(obj);

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public void uploadAttachment(Long idKhazanah, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";
        Khazanah model = this.find(idKhazanah);
        try {
            String filename = formDataContentDisposition.getFileName();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is2 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is3 = new ByteArrayInputStream(baos.toByteArray());

            byte[] documentContent = org.apache.commons.io.IOUtils.toByteArray(is1);

            docId = fileService.upload(filename, documentContent);
            docId = docId.replace("workspace://SpacesStore/", "");

            List<String> extractedText = textExtractionService.parsingFile(is2, formDataContentDisposition, body).getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString());
            obj.setDocName(filename);
            obj.setCompanyCode(model.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setSize(new Long(documentContent.length));
            obj.setIsDeleted(false);
            obj.setReferenceId(idKhazanah);
            obj.setReferenceTable("t_khazanah");
            String number = String.format("khazanah_%s_%s", model.getId(), model.getTipeKhazanah());
            obj.setNumber(number);
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);
        } catch (Exception ex) {
            if (!org.apache.commons.lang.StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }

            throw ex;
        }
    }

    public void setFiles(List<KhazanahDto> list) {
        list.forEach(item -> {
            List<GlobalAttachmentDto> files = globalAttachmentService.getAllDataByRef("t_khazanah", item.getId())
                    .map(attach -> {
                        String downloadUrl = fileService.getViewLink(attach.getDocId(), false);
                        GlobalAttachmentDto dto = new GlobalAttachmentDto(attach);
                        dto.setDownloadLink(downloadUrl);

                        if (attach.getDocName().lastIndexOf(".") > 0) {
                            dto.setViewLink(fileService.getViewLink(attach.getDocId(), FileUtility.GetFileExtension(attach.getDocName())));
                        } else {
                            dto.setViewLink(fileService.getViewLink(attach.getDocId()));
                        }

                        return dto;
                    }).collect(Collectors.toList());

            GlobalAttachmentDto cover = files.stream().filter(q -> q.getNumber().toLowerCase().contains("cover")).findFirst().orElse(null);
            item.setCover(cover);

            GlobalAttachmentDto content = files.stream().filter(q -> q.getNumber().toLowerCase().contains("content")).findFirst().orElse(null);
            item.setContent(content);
        });
    }

//    public void setCount(List<KhazanahListDto> list) {
//        list.forEach(dto -> {
//            List<Khazanah> childs = this.getByKlasifikasi(dto.getIdKlasifikasi()).collect(Collectors.toList());
//            dto.setCount(childs.size());
//        });
//    }

    public void delete(Long id) {
        List<GlobalAttachment> attachments = globalAttachmentService
                .getAllDataByRef("t_khazanah", id)
                .collect(Collectors.toList());

        if (!attachments.isEmpty()) {
            attachments.forEach((attach) -> {
                fileService.delete(attach.getDocId());

                attach.setIsDeleted(true);
                globalAttachmentService.edit(attach);

                getEntityManager().flush();
                getEntityManager().refresh(attach);
            });
        }

        Khazanah model = this.find(id);
        String tableName = "t_khazanah";
        List<GlobalAttachment> all = globalAttachmentService.getAllDataByRef(tableName, id).collect(Collectors.toList());
        if (all.isEmpty()) {
            this.remove(model);
        }
    }

    public List<KhazanahActionHistoryDto> getHistoryByKhazanah(Long idKhazanah) {
        return historyService.getByKhazanah(idKhazanah).stream().map(KhazanahActionHistoryDto::new).collect(Collectors.toList());
    }
}
