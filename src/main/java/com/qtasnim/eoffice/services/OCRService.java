/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.util.ImageProcessing;
import com.qtasnim.eoffice.util.OCRProcess;
import net.sourceforge.tess4j.TesseractException;
import org.apache.commons.io.IOUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author acdwisu
 */

@LocalBean
@Stateless
public class OCRService {

    private List<String> tiffExtensions;

    public OCRService() {
        tiffExtensions = Arrays.asList(new String[]{".tif", ".tiff"});
    }

    public String extractText(byte[] image) throws Exception {
        OCRProcess ocrProcess = new OCRProcess(image);
        
        return ocrProcess.extractText();
    }
    
    public String extractText(InputStream inputStream) throws Exception {
        OCRProcess ocrProcess = new OCRProcess(inputStream);
        
        return ocrProcess.extractText();
    }

    public List<String> extractText(InputStream inputStream, String fileName) throws Exception {
        List<String> texts;

        String fileExtension = FileUtility.GetFileExtension(fileName).toLowerCase();

        texts = textExtracting(inputStream, fileExtension);

        return texts;
    }

    public List<String> extractText(File file) {
        List<String> texts = new LinkedList<>();

        String fileName = file.getName();
        String fileExtension = FileUtility.GetFileExtension(fileName).toLowerCase();

        try {
            FileInputStream inputStream = new FileInputStream(file);

            texts = textExtracting(inputStream, fileExtension);
        } catch(Exception e) {
            e.printStackTrace();
        }

        return texts;
    }

    private List<String> textExtracting(InputStream inputStream, String fileExtension) throws Exception {
        List<String> texts = new LinkedList<>();

        if(tiffExtensions.contains(fileExtension)) {
            List<BufferedImage> imgs = new ImageProcessing().imagesFromTiff(inputStream);

            OCRProcess ocrProcess = new OCRProcess();

            for(BufferedImage img : imgs) {
                ocrProcess.setImage(img);

                String text = ocrProcess.extractText();

                texts.add(text);
            }
        } else {
//            byte[] content = IOUtils.toByteArray(inputStream);
//
//            OCRProcess ocrProcess = new OCRProcess(content);

            OCRProcess ocrProcess = new OCRProcess(inputStream);

            String text = ocrProcess.extractText();

            texts.add(text);
        }

        return texts;
    }

    public List<String> textExtractingMultiImages(List<BufferedImage> imgs) throws Exception {
        List<String> texts = new LinkedList<>();

        OCRProcess ocrProcess = new OCRProcess();

        for(BufferedImage img : imgs) {
            ocrProcess.setImage(img);

            String text = ocrProcess.extractText();

            texts.add(text);
        }

        return texts;
    }
}
