package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.CardCategoryDetailDto;
import com.qtasnim.eoffice.ws.dto.CardChecklistDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class CardChecklistService extends AbstractFacade<CardChecklist>{
    @Override
    protected Class<CardChecklist> getEntityClass() {
        return CardChecklist.class;
    }

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyService;

    @Inject
    private CardService cardService;

    public JPAJinqStream<CardChecklist> getAll(){
        Long idCompany = userSession.getUserSession().getUser().getCompanyCode().getId();
        CompanyCode company = companyService.find(idCompany);
        return db().where(a->a.getIsDeleted().booleanValue()==false && a.getCompanyCode().equals(company));
    }

    public CardChecklist saveOrEdit(CardChecklistDto dao, Long id) throws Exception {
        try{
            CardChecklist obj = new CardChecklist();
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = companyService.getByCode(user.getCompanyCode().getCode());
            if(id==null){
                obj.setTitle(dao.getTitle());
                obj.setDescription(dao.getDescription());
                obj.setIsDeleted(false);
                if(dao.getCardId()!=null){
                    Card card = cardService.find(dao.getCardId());
                    obj.setCard(card);
                }
                obj.setCompanyCode(company);
                create(obj);
            }else{
                obj.setTitle(dao.getTitle());
                obj.setDescription(dao.getDescription());
                obj.setIsDeleted(false);
                if(dao.getCardId()!=null){
                    Card card = cardService.find(dao.getCardId());
                    obj.setCard(card);
                }
                obj.setCompanyCode(company);
                edit(obj);
            }
            getEntityManager().flush();
            getEntityManager().refresh(obj);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public CardChecklist deleteCheckList(Long id) throws Exception {
        try{
            CardChecklist obj = find(id);
            obj.setIsDeleted(true);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
}
