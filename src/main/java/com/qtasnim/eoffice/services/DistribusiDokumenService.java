package com.qtasnim.eoffice.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.NotAuthorizedException;
import com.qtasnim.eoffice.security.PasswordHash;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.workflows.*;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import org.apache.commons.lang3.StringUtils;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class DistribusiDokumenService extends AbstractFacade<DistribusiDokumen> implements IWorkflowService<DistribusiDokumen> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private DistribusiDokumenDetailService detailService;

    @Inject
    private PemeriksaDistribusiService pemeriksaService;

    @Inject
    private PenandatanganDistribusiService penandatanganService;

    @Inject
    private PenerimaDistribusiService penerimaService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private UserTaskService userTaskService;

    @Inject
    private ProcessDefinitionService processDefinitionService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private PasswordHash passwordHash;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    private NotificationService notificationService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private DokumenHistoryService docHistoryService;

    @Inject
    private DistribusiCommentService commentService;

    @Inject
    private Logger logger;

    @Override
    protected Class<DistribusiDokumen> getEntityClass() {
        return DistribusiDokumen.class;
    }

    public JPAJinqStream<DistribusiDokumen> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public DistribusiDokumen saveOrEditTransaction(Long id, DistribusiDokumenDto dto, String password, List<Long> deletedDetailIds, List<Long> deletedPemeriksaIds, List<Long> deletedPenandatanganIds, List<Long> deletedPenerimaIds) throws Exception {
        try {
            DistribusiDokumen model = saveOrEdit(id, dto, password, deletedDetailIds, deletedPemeriksaIds, deletedPenandatanganIds, deletedPenerimaIds);
            getEntityManager().flush();
            getEntityManager().refresh(model);

            MasterUser user = userSession.getUserSession().getUser();
            boolean isKonseptor = model.getKonseptor().getIdOrganization().equals(user.getOrganizationEntity().getIdOrganization()) ? true : false;
            if(isKonseptor) {
                startWorkflow(model);
            } else{
                UserTask userTask = userTaskService.getUserTask(model.getId().toString(),DistribusiDokumen.class.getName(),user.getOrganizationEntity().getIdOrganization());
                if(userTask!=null){
                    userTaskService.updatePerihal(userTask.getId(),model.getPerihal());
                }
            }

            return model;
        } catch (Exception ex) {
            throw new InternalServerErrorException(ex.getMessage(), ExceptionUtil.getRealCause(ex));
        }
    }

    public DistribusiDokumen saveOrEdit(Long id, DistribusiDokumenDto dto, String password, List<Long> deletedDetailIds, List<Long> deletedPemeriksaIds, List<Long> deletedPenandatanganIds, List<Long> deletedPenerimaIds) throws Exception {
        boolean isNew = id == null;

        if (isNew) {
            DistribusiDokumen model = this.getModel(new DistribusiDokumen(), dto);
            DistribusiComment commentModel = new DistribusiComment();
            if (!Strings.isNullOrEmpty(password)) {
                List<String> keys = passwordHash.getSaltPassword(password);
                String hash = keys.get(1);
                String salt = keys.get(0);

                model.setPasswordHash(hash);
                model.setPasswordSalt(salt);
            }

            this.create(model);
            getEntityManager().flush();

            this.saveDetailDistribusi(model, dto);
            this.savePemeriksa(model, dto);
            this.savePenandatangan(model, dto);
            this.savePenerima(model, dto);
            this.saveDistribusiKomentar(model, dto);

            return model;

        } else {
            DistribusiDokumen model = this.getModel(this.find(id), dto);

            if (!Strings.isNullOrEmpty(password)) {
                List<String> keys = passwordHash.getSaltPassword(password);
                String hash = keys.get(1);
                String salt = keys.get(0);

                model.setPasswordHash(hash);
                model.setPasswordSalt(salt);
            }

            this.edit(model);
            getEntityManager().flush();

            if (deletedDetailIds != null && !deletedDetailIds.isEmpty()) {
                for (Long deletedId : deletedPenerimaIds) {
                    this.deleteDetail(deletedId);
                }
            }

            if (deletedPemeriksaIds != null && !deletedPemeriksaIds.isEmpty()) {
                for (Long deletedId : deletedPemeriksaIds) {
                    this.deletePemeriksa(deletedId);
                }

                for (PemeriksaDistribusi ps : model.getPemeriksa()
                        .stream()
                        .filter(t -> deletedPemeriksaIds.stream().anyMatch(d -> t.getId().equals(d)))
                        .collect(Collectors.toList())) {
                    model.getPemeriksa().remove(ps);
                }
            }

            if (deletedPenandatanganIds != null && !deletedPenandatanganIds.isEmpty()) {
                for (Long deletedId : deletedPenandatanganIds) {
                    this.deletePenandatangan(deletedId);
                }

                for (PenandatanganDistribusi ps : model.getPenandatangan()
                        .stream()
                        .filter(t -> deletedPenandatanganIds.stream().anyMatch(d -> t.getId().equals(d)))
                        .collect(Collectors.toList())) {
                    model.getPenandatangan().remove(ps);
                }
            }

            if (deletedPenerimaIds != null && !deletedPenerimaIds.isEmpty()) {
                for (Long deletedId : deletedPenerimaIds) {
                    this.deletePenerima(deletedId);
                }

                for (PenerimaDistribusi ps : model.getPenerima()
                        .stream()
                        .filter(t -> deletedPenerimaIds.stream().anyMatch(d -> t.getId().equals(d)))
                        .collect(Collectors.toList())) {
                    model.getPenerima().remove(ps);
                }
            }

            this.saveDetailDistribusi(model, dto);
            this.savePemeriksa(model, dto);
            this.savePenandatangan(model, dto);
            this.savePenerima(model, dto);
            this.saveDistribusiKomentar(model, dto);
            return model;

        }
    }

    private DistribusiDokumen getModel(DistribusiDokumen model, DistribusiDokumenDto dto) {
        DateUtil dateUtil = new DateUtil();

        if(model.getKonseptor()==null) {
            model.setKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
        }
        model.setPerihal(dto.getPerihal());
        model.setJenis(dto.getJenis());
        if (dto.getApprovedDate() != null) {
            model.setApprovedDate(dateUtil.getDateFromISOString(dto.getApprovedDate()));
        }
        if (dto.getStart() != null) {
            model.setStart(dateUtil.getDateFromISOString(dto.getStart()));
        }
        if (dto.getEnd() != null) {
            model.setEnd(dateUtil.getDateFromISOString(dto.getEnd()));
        }
        model.setIsDeleted(dto.getIsDeleted());
        model.setStatus(dto.getStatus());

        if (dto.getIdCompany() != null) {
            CompanyCode company = companyCodeService.find(dto.getIdCompany());
            model.setCompanyCode(company);
        }

        return model;
    }

    private void saveDetailDistribusi(DistribusiDokumen model, DistribusiDokumenDto modelDto) throws Exception {
        if (!modelDto.getDetail().isEmpty()) {
            for (DistribusiDokumenDetailDto dto : modelDto.getDetail()) {
                DateUtil dateUtil = new DateUtil();
                DistribusiDokumenDetail detail;
                if (dto.getId() == null) {
                    detail = new DistribusiDokumenDetail();
                    detail.setReferenceId(dto.getIdReference());
                    detail.setReferenceTable(dto.getReferenceTable());
                    detail.setDistribusi(model);
//                    detail.setCreatedBy(dto.getCreatedBy());
//                    detail.setCreatedDate(dateUtil.getDateFromISOString(dto.getCreatedDate()));
                    detailService.create(detail);
                } else {
                    detail = detailService.find(dto.getId());
                    detail.setReferenceId(dto.getIdReference());
                    detail.setReferenceTable(dto.getReferenceTable());
//                    detail.setModifiedBy(dto.getModifiedBy());
//                    detail.setModifiedDate(dateUtil.getDateFromISOString(dto.getModifiedDate()));
                    detailService.edit(detail);
                }
            }
        }
    }

    private void savePemeriksa(DistribusiDokumen model, DistribusiDokumenDto modelDto) throws Exception {
        if (!modelDto.getPemeriksa().isEmpty()) {
            Integer index = 0;

            for (PemeriksaDistribusiDto dto : modelDto.getPemeriksa().stream().distinct().collect(Collectors.toList())) {
                PemeriksaDistribusi obj;

                if (dto.getId() == null) {
                    MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dto.getIdPemeriksa());

                    obj = new PemeriksaDistribusi();
                    obj.setOrganization(org);
                    obj.setUser(org.getUser());
                    obj.setDistribusi(model);
                    obj.setSeq(Optional.ofNullable(dto.getSeq()).orElse(index));

                    pemeriksaService.create(obj);
                } else {
                    obj = pemeriksaService.find(dto.getId());

                    if (!Optional.ofNullable(obj.getSeq()).orElse(-1).equals(Optional.ofNullable(dto.getSeq()).orElse(-1))) {
                        obj.setSeq(Optional.ofNullable(dto.getSeq()).orElse(index));
                        pemeriksaService.edit(obj);
                    }
                }

                index++;
            }
        }
    }

    private void savePenandatangan(DistribusiDokumen model, DistribusiDokumenDto modelDto) throws Exception {
        if (!modelDto.getPenandatangan().isEmpty()) {
            Integer index = 0;

            for (PenandatanganDistribusiDto dto : modelDto.getPenandatangan().stream().distinct().collect(Collectors.toList())) {
                PenandatanganDistribusi obj;

                if (dto.getId() == null) {
                    MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dto.getIdPenandatangan());

                    obj = new PenandatanganDistribusi();
                    obj.setOrganization(org);
                    obj.setUser(org.getUser());
                    obj.setDistribusi(model);
                    obj.setSeq(Optional.ofNullable(dto.getSeq()).orElse(index));

                    penandatanganService.create(obj);
                } else {
                    obj = penandatanganService.find(dto.getId());

                    if (!Optional.ofNullable(obj.getSeq()).orElse(-1).equals(Optional.ofNullable(dto.getSeq()).orElse(-1))) {
                        obj.setSeq(Optional.ofNullable(dto.getSeq()).orElse(index));
                        penandatanganService.edit(obj);
                    }
                }

                index++;
            }
        }
    }

    private void savePenerima(DistribusiDokumen model, DistribusiDokumenDto modelDto) throws Exception {
        if (!modelDto.getPenerima().isEmpty()) {
            Integer index = 0;

            for (PenerimaDistribusiDto dto : modelDto.getPenerima().stream().distinct().collect(Collectors.toList())) {
                DateUtil dateUtil = new DateUtil();
                PenerimaDistribusi obj;

                if (dto.getId() == null) {
                    MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dto.getIdPenerima());

                    obj = new PenerimaDistribusi();
                    obj.setOrganization(org);
                    obj.setUser(org.getUser());
                    obj.setDistribusi(model);
                    obj.setSeq(Optional.ofNullable(dto.getSeq()).orElse(index));
                    obj.setIsDownload(dto.getIsDownload());
                    obj.setIsRead(dto.getIsRead());

                    if (!Strings.isNullOrEmpty(dto.getDownloadDate())) {
                        obj.setDownloadDate(dateUtil.getDateFromISOString(dto.getDownloadDate()));
                    }

                    if (!Strings.isNullOrEmpty(dto.getReadDate())) {
                        obj.setReadDate(dateUtil.getDateFromISOString(dto.getReadDate()));
                    }

                    penerimaService.create(obj);
                } else {
                    obj = penerimaService.find(dto.getId());
                    obj.setSeq(Optional.ofNullable(dto.getSeq()).orElse(index));
                    obj.setIsDownload(dto.getIsDownload());
                    obj.setIsRead(dto.getIsRead());

                    if (!Strings.isNullOrEmpty(dto.getDownloadDate())) {
                        obj.setDownloadDate(dateUtil.getDateFromISOString(dto.getDownloadDate()));
                    }

                    if (!Strings.isNullOrEmpty(dto.getReadDate())) {
                        obj.setReadDate(dateUtil.getDateFromISOString(dto.getReadDate()));
                    }

                    penerimaService.edit(obj);
                }

                index++;
            }
        }
    }

    private void deleteDetail(Long idDetail) throws Exception {
        DistribusiDokumenDetail detail = detailService.find(idDetail);
        detailService.remove(detail);
    }

    private void deletePemeriksa(Long idPemeriksa) throws Exception {
        PemeriksaDistribusi pemeriksa = pemeriksaService.find(idPemeriksa);
        pemeriksaService.remove(pemeriksa);
    }

    private void deletePenandatangan(Long idPenandatangan) throws Exception {
        PenandatanganDistribusi penandatangan = penandatanganService.find(idPenandatangan);
        penandatanganService.remove(penandatangan);
    }

    private void deletePenerima(Long idPenerima) throws Exception {
        PenerimaDistribusi penerima = penerimaService.find(idPenerima);
        penerimaService.remove(penerima);
    }

    public void startWorkflow(DistribusiDokumen obj) {
        if (obj.getStatus().equals("SUBMITTED")) {

            final DistribusiDokumen surat = obj;
            ProcessDefinition activated = processDefinitionService.getActivated("Distribusi Dokumen");
            IWorkflowProvider workflowProvider = masterWorkflowProviderService.getByProviderName(applicationContext.getApplicationConfig().getWorkflowProvider()).getWorkflowProvider();
            workflowProvider.startWorkflow(obj, activated, ProcessEventHandler.DEFAULT
                    .onCompleted((processInstance, status) -> onWorkflowCompleted(status, processInstance, surat, workflowProvider, null, null))
                    .onTasksCreated((prevTask,processTasks) -> onTasksCreated(surat, prevTask, processTasks)),
                    new HashMap<>()
            );
        }
    }

    public void cekPassword(DistribusiDokumen model, String password, Long idPenerima) throws InternalServerErrorException {
        String salt = model.getPasswordSalt();
        String hash = model.getPasswordHash();
        String inputPassHash = passwordHash.getHash(password, salt);

        if (!hash.equals(inputPassHash)) {
            throw new NotAuthorizedException("Password Salah");
        } else {
            updateDownloadDate(idPenerima);
        }
    }

    public void updateDownloadDate(Long idPenerima) throws InternalServerErrorException {
        PenerimaDistribusi penerima = penerimaService.find(idPenerima);
        if (!penerima.getIsDownload()) {
            penerima.setIsDownload(true);
            penerima.setDownloadDate(new Date());
            penerimaService.edit(penerima);
        }else{
            penerima.setDownloadDate(new Date());
            penerimaService.edit(penerima);
        }
    }

    public void readDistribusi(DistribusiDokumen model, Long idPenerima) throws InternalServerErrorException {
        PenerimaDistribusi penerima = penerimaService.find(idPenerima);

        if (!penerima.getIsRead()) {
            penerima.setIsRead(true);
            penerima.setReadDate(new Date());
            penerimaService.edit(penerima);
        }

        DokumenHistoryDto history = new DokumenHistoryDto();
        history.setReferenceTable("t_distribusi_dokumen");
        history.setReferenceId(model.getId());
        history.setStatus("READ");

        MasterUserDto usr = masterUserService.getByOrgId(idPenerima);
        history.setIdOrganisasi(idPenerima);
        history.setIdUser(usr.getId());
        docHistoryService.saveHistory(history);
    }
    
    public List<PenerimaDistribusi> getPenerimaRead(Long idDistribusi) throws InternalServerErrorException {
        return penerimaService.getAllBy(idDistribusi).stream().filter(q -> q.getIsRead()).collect(Collectors.toList());
    }
    
    public List<PenerimaDistribusi> getPenerimaDownload(Long idDistribusi) throws InternalServerErrorException {
        return penerimaService.getAllBy(idDistribusi).stream().filter(q -> q.getIsDownload()).collect(Collectors.toList());
    }

    /**
     * START Workflow Event Handler
     */
    public IWorkflowEntity getEntity(ProcessTask processTask) {
        return find(Long.parseLong(processTask.getProcessInstance().getRecordRefId()));
    }

    @Override
    public boolean isTaskInvalid(TaskActionDto dto, ProcessTask processTask, Map<String, Object> userResponse) throws Exception {
        return false;
    }

    @Override
    public void onWorkflowCompleted(ProcessStatus status, ProcessInstance processInstance, IWorkflowEntity entity, IWorkflowProvider workflowProvider, String digisignUsername, String passphrase) {
        try {
            DistribusiDokumen surat = (DistribusiDokumen) entity;

            if (ProcessStatus.APPROVED.equals(status)) {
                surat.setStatus(EntityStatus.APPROVED.toString());
                surat.setApprovedDate(new Date());
                surat.getPenerima().forEach(penerima -> {
                    penerima.setUser(penerima.getUser());
                });

                edit(surat);

                List<UserTask> userTaskList = userTaskService.getListUserTask(surat.getId().toString(),surat.getClassName());
                if(!userTaskList.isEmpty()){
                    userTaskList.forEach(ut->{
                        ut.setStatus(EntityStatus.APPROVED.toString());
                        userTaskService.edit(ut);
                    });
                }
            } else if (ProcessStatus.REJECTED.equals(status)) {

                surat.setStatus(EntityStatus.DRAFT.toString());
                edit(surat);

                List<UserTask> userTaskList = userTaskService.getListUserTask(surat.getId().toString(),surat.getClassName());
                if(!userTaskList.isEmpty()){
                    userTaskList.forEach(ut->{
                        ut.setStatus(EntityStatus.REJECTED.toString());
                        ut.setExecutionStatus(ExecutionStatus.SKIP);
                        userTaskService.edit(ut);
                    });
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onTasksCreated(IWorkflowEntity entity, ProcessTask prevTask, List<ProcessTask> processTasks) {
        newTaskNotification(processTasks, entity, null);

        //#region INSERT INTO USER TASK
        for (ProcessTask task : processTasks) {
            userTaskService.saveOrEdit(null, task, entity);
        }
        //#endregion
    }

    @Override
    public void onTaskExecuting(ProcessTask task) {

    }

    @Override
    public void onTaskExecuted(TaskActionDto dto, IWorkflowProvider workflowProvider, IWorkflowEntity entity, ProcessTask task, TaskEventHandler taskEventHandler, Map<String, Object> userResponse) {
        DistribusiDokumen obj = (DistribusiDokumen) entity;

        //#region Update user approver
        if (task.getResponse().equalsIgnoreCase("Setujui")) {

            if (task.getTaskName().toLowerCase().contains("pemeriksa")) {

                obj.getPemeriksa()
                        .stream()
                        .filter(q -> q.getOrganization().getIdOrganization().equals(task.getAssignee().getIdOrganization()))
                        .forEach(pemeriksa -> {
                            pemeriksa.setUser(userSession.getUserSession().getUser());
                        });

                userTaskService.saveOrEdit(task);

            } else if (task.getTaskName().toLowerCase().contains("penandatangan")) {

                obj.getPenandatangan()
                        .stream()
                        .filter(q -> q.getOrganization().getIdOrganization().equals(task.getAssignee().getIdOrganization()))
                        .forEach(t -> {
                            t.setUser(userSession.getUserSession().getUser());
                        });

                userTaskService.saveOrEdit(task);

            }

            try {
                edit(obj);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
        //#endregion

        /*@TODO Tambah insert ke tabel dokumen history*/
        if (userSession != null && userSession.getUserSession() != null) {
            String responseVar = task.getResponseVar();
            String response = (String)userResponse.get(responseVar);

            if (response.toLowerCase().contains("kembalikan")) {
                Long idDistribusi = ((DistribusiDokumen) entity).getId();
                DokumenHistoryDto history = new DokumenHistoryDto();
                history.setReferenceTable("t_distribusi_dokumen");
                history.setReferenceId(idDistribusi);
                history.setStatus("REJECT");
                history.setIdOrganisasi(task.getAssignee().getIdOrganization());
                history.setIdUser(task.getAssignee().getUser().getId());

                docHistoryService.saveHistory(history);
            }

            if (response.toLowerCase().contains("setujui")) {
                Long idDistribusi = ((DistribusiDokumen) entity).getId();
                DokumenHistoryDto history = new DokumenHistoryDto();
                history.setReferenceTable("t_distribusi_dokumen");
                history.setReferenceId(idDistribusi);
                history.setStatus("APPROVE");
                history.setIdOrganisasi(task.getAssignee().getIdOrganization());
                history.setIdUser(task.getAssignee().getUser().getId());

                docHistoryService.saveHistory(history);
            }
        }

        notificationService.sendInfo("Info", "Persetujuan Distribusi berhasil dikirim");
    }

    @Override
    public List<ProcessTaskDto> onTaskHistoryRequested(List<ProcessTask> myTasks) {
        return myTasks.stream().map(ProcessTaskDto::new).collect(Collectors.toList());
    }

    @Override
    public void onTaskActionGenerated(ProcessTask processTask, TaskActionResponseDto response) {
        if (processTask.getProcessInstance().getRelatedEntity().equals(DistribusiDokumen.class.getName())) {
            DistribusiDokumen distribusi = find(Long.parseLong(processTask.getProcessInstance().getRecordRefId()));

            if (processTask.getTaskType().equals("Approval")) {
                boolean isPemeriksa = processTask.getTaskName().toLowerCase().contains("pemeriksa");
                boolean isPenandatangan = processTask.getTaskName().toLowerCase().contains("penandatangan");

                if (isPenandatangan) {
                    // Hide "kembalikan ke pemeriksa" jika tidak ada pemeriksa
                    boolean isNoPemeriksa = distribusi.getPemeriksa() == null || distribusi.getPemeriksa().size() == 0;
                    if (isNoPemeriksa) {
                        response.getForms().stream().filter(t -> t.getName().equals(processTask.getResponseVar())).forEach(t -> {
                            try {
                                OptionsDto options = getObjectMapper().readValue(t.getOtherData(), new TypeReference<OptionsDto>() {});
                                options.setOptions(options.getOptions().stream().filter(v -> !v.toLowerCase().contains("pemeriksa")).collect(Collectors.toList()));
                                t.setOtherData(getObjectMapper().writeValueAsString(options));
                            } catch (IOException e) {
                                logger.error(null, e);
                            }
                        });
                    }
                }

                if (isPemeriksa) {
                    boolean isFirstPemeriksa = Optional.ofNullable(processTask.getSeq()).orElse(0) == 0;

                    if (isFirstPemeriksa) {
                        // Hide "kembalikan" jika pemeriksa pertama
                        response.getForms().stream().filter(t -> t.getName().equals(processTask.getResponseVar())).forEach(t -> {
                            try {
                                OptionsDto options = getObjectMapper().readValue(t.getOtherData(), new TypeReference<OptionsDto>() {});
                                options.setOptions(options.getOptions().stream().filter(v -> !v.toLowerCase().equals("kembalikan")).collect(Collectors.toList()));
                                t.setOtherData(getObjectMapper().writeValueAsString(options));
                            } catch (IOException e) {
                                logger.error(null, e);
                            }
                        });
                    }
                }
            }
        }
    }
    /**
     * END Workflow Event Handler
     */

    /**
     * START Mail Handler
     */
    public void setMailProperties(IWorkflowEntity entity) {
        String className = entity.getClassName();

        String jenisDokumen = "";
        String perihal = "";
        String nomor = "";
        String klasifikasiKeamanan = "";

        if (className.contains("PermohonanDokumen")){
            jenisDokumen = ((PermohonanDokumen) entity).getTipeLayanan();
            perihal = ((PermohonanDokumen) entity).getPerihal();
            nomor = ((PermohonanDokumen) entity).getSurat().getNomor();
        } else if (className.contains("DistribusiDokumen")){
            jenisDokumen = "Distribusi Dokumen (" + ((DistribusiDokumen) entity).getJenis() + ")";
            perihal = ((DistribusiDokumen) entity).getPerihal();
        }

        entity.setJenisDokumen(jenisDokumen);
        entity.setPerihal(perihal);
        entity.setKlasifikasiKeamanan(klasifikasiKeamanan);
        if(className.contains("PermohonanDokumen")) {
            entity.setNomor(nomor);
        } else {
            entity.setNomor(entity.getNoDokumen());
        }
    }

    public void newTaskNotification(List<ProcessTask> processTasks, IWorkflowEntity entity, String docId) {
        for (ProcessTask processTask : processTasks) {
            MasterUser user = processTask.getAssignee().getUser();
            MasterUser initiator = masterUserService.findUserByUsername(entity.getCreatedBy());

            // < SET MAIL PROPS >
            setMailProperties(entity);
            // </ SET MAIL PROPS >

            notificationService.sendEmail(Constants.TEMPLATE_NEW_TASK_ASSIGNMENT, vars -> {
                vars.put("initiator", initiator);
                vars.put("assignee", user);
                vars.put("entity", entity);
                vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
            }, docId, null, user);
        }
    }

    public void newTaskNotification(ProcessTask processTask, IWorkflowEntity entity, String docId) {
        MasterUser user = processTask.getAssignee().getUser();

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        notificationService.sendEmail(Constants.TEMPLATE_NEW_TASK_ASSIGNMENT, vars -> {
            vars.put("assignee", user);
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
        }, docId, null, user);
    }

    public void infoTaskNotification(ProcessTask processTask, IWorkflowEntity entity, String docId) {
        // < INIT TASK DICTIONARY >
        Map<String, String> taskExecutorVars = new HashMap<>();
        taskExecutorVars.put("Task_PemeriksaPelakhar_Approval", "Pelakhar Pemeriksa");
        taskExecutorVars.put("Task_PemeriksaPYMT_Approval", "PYMT Pemeriksa");
        taskExecutorVars.put("Task_Pemeriksa_Approval", "Pemeriksa");

        taskExecutorVars.put("Task_PenandatanganPelakhar_Approval", "Pelakhar Penandatangan");
        taskExecutorVars.put("Task_PenandatanganPelakhar_Approval_2", "Pelakhar Penandatangan");
        taskExecutorVars.put("Task_PenandatanganPYMT_Approval", "PYMT Penandatangan");
        taskExecutorVars.put("Task_Penandatangan_Approval", "Penandatangan");

        taskExecutorVars.put("Setujui", "Disetujui");
        taskExecutorVars.put("TO PYMT", "Disetujui");
        taskExecutorVars.put("Kembalikan", "Dikembalikan");
        taskExecutorVars.put("Kembalikan ke Konseptor", "Dikembalikan");
        taskExecutorVars.put("Kembalikan ke Pemeriksa", "Dikembalikan");

        // </ INIT TASK DICTIONARY >
        String executor = taskExecutorVars.get(processTask.getTaskName());
        if (!Strings.isNullOrEmpty(executor)) {
            entity.setTaskExecutor(executor);
        } else {
            entity.setTaskExecutor("");
        }

        String response = taskExecutorVars.get(processTask.getResponse());

        if (!Strings.isNullOrEmpty(response)) {
            entity.setTaskResponse(response);
        } else {
            entity.setTaskResponse("");
        }

        MasterUser user = processTask.getAssignee().getUser();
        String[] initiatorParts = entity.getCreatedBy().split("\\|");
        MasterUser initiator = masterUserService.findUserByUsername(initiatorParts[0].trim());

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_INFO, vars -> {
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
        }, docId, null, initiator);
    }
    /**
     * END Mail Handler
     */

    private void saveDistribusiKomentar(DistribusiDokumen model, DistribusiDokumenDto modelDto) throws Exception {
        if(StringUtils.isNotEmpty(modelDto.getKomentar())) {
            DistribusiComment comment = new DistribusiComment();
            comment.setIsDeleted(false);
            comment.setDistribusi(model);
            comment.setKomentar(modelDto.getKomentar());
            MasterUser user = userSession.getUserSession().getUser();
            comment.setUser(user);
            commentService.create(comment);
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
