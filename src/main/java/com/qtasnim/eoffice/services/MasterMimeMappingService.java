package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterMimeMapping;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class MasterMimeMappingService extends AbstractFacade<MasterMimeMapping> {
    @Override
    protected Class<MasterMimeMapping> getEntityClass() {
        return MasterMimeMapping.class;
    }

    public String getMime(String fileExtension) {
        return db()
                .where(t -> t.getExtension().equals(fileExtension))
                .findFirst()
                .map(MasterMimeMapping::getMime)
                .orElse(null);
    }

    public MasterMimeMapping getByExtension(String extension) {
        return db().where(t -> t.getExtension().equals(extension)).findFirst().orElse(null);
    }

    public String getExtensionBy(String fileMime) {
        return db().where(t -> t.getMime().equals(fileMime))
                .map(q -> q.getExtension()).findFirst().orElse("");
    }
}
