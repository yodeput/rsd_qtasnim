package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.AgendaDto;
import com.qtasnim.eoffice.ws.dto.SuratInteractionDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class SuratInteractionService extends AbstractFacade<SuratInteraction> {
    @Inject
    private SuratService suratService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private AgendaService agendaService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Override
    protected Class<SuratInteraction> getEntityClass() {
        return SuratInteraction.class;
    }

    public List<SuratInteraction> getByIdSurat(Long idSurat, String filter, String sort, boolean descending, int skip, int limit){
        Surat surat = suratService.find(idSurat);
        List<SuratInteraction> interaction = this.getResultList(filter, sort, descending, skip, limit);
        List<SuratInteraction> interaction2 = db().where(c->c.getSurat().equals(surat)).toList();
        interaction.retainAll(interaction2);
        return interaction;
    }

    public SuratInteraction getByUser(Long idSurat){
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        Surat surat = suratService.find(idSurat);
        SuratInteraction interaction = db().where(c->c.getSurat().equals(surat) && c.getOrganization().equals(org)).findFirst().orElse(null);
        return interaction;
    }

    public JPAJinqStream<SuratInteraction> getFavoritByUser(MasterStrukturOrganisasi org){
        return db().where(c->c.getOrganization().equals(org) && c.getIsStarred());
    }

    public List<SuratInteraction> getByIdSurats(List<Long> ids){
//        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
//        Long idOrganization = org.getIdOrganization();

        return db().where(c -> ids.contains(c.getSurat().getId())).toList();
    }

    public SuratInteraction save(Long id, SuratInteractionDto dao){
        SuratInteraction model = new SuratInteraction();
        DateUtil dateUtil = new DateUtil();

        if(id==null){
            MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getOrganzationId());
            model.setOrganization(org);
            Surat surat = suratService.find(dao.getSuratId());
            model.setSurat(surat);
            CompanyCode companyCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(companyCode);

            model.setIsRead(dao.getIsRead());
            model.setIsImportant(dao.getIsImportant());
            model.setIsStarred(dao.getIsStarred());
            model.setIsFinished(dao.getIsFinished());
            model.setFinishedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));

            this.create(model);
        } else {
            model = find(id);
            MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getOrganzationId());
            model.setOrganization(org);
            Surat surat = suratService.find(dao.getSuratId());
            model.setSurat(surat);
            CompanyCode companyCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(companyCode);
            model.setIsRead(dao.getIsRead());
            model.setIsImportant(dao.getIsImportant());
            model.setIsStarred(dao.getIsStarred());
            model.setIsFinished(dao.getIsFinished());
            model.setFinishedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
            this.edit(model);
        }
        return model;
    }

    public JPAJinqStream<SuratInteraction> getBySurat(Long id) {
        Surat surat = suratService.find(id);
        return db().where(q -> q.getSurat().getId().equals(surat.getId()));
    }

    public JPAJinqStream<SuratInteraction> getRiwayat(Long suratId) {
        return db()
                .where(q -> q.getSurat().getId().equals(suratId) && q.getReadDate() != null)
                .sortedDescendingBy(SuratInteraction::getReadDate);
    }

    public SuratInteraction setStared(Long idSurat, Boolean stared){
        Surat surat = suratService.find(idSurat);
        SuratInteraction model = getBySuratIdOrCreate(surat, userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());
        if (!model.getIsStarred()) {
            model.setIsStarred(true);
            edit(model);
        } else {
            model.setIsStarred(false);
        }
        return model;
    }

    public SuratInteraction setFinished(Long idSurat, Boolean finished){
        Surat surat = suratService.find(idSurat);
        SuratInteraction model = getBySuratIdOrCreate(surat, userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());
        DateUtil dateUtil = new DateUtil();
        model.setIsFinished(finished);
        model.setFinishedDate(dateUtil.getDateFromISOString(dateUtil.getCurrentISODate(new Date())));
        edit(model);
        return model;
    }

    public SuratInteraction setImportant(Long idSurat, Boolean isImportant){
        Surat surat = suratService.find(idSurat);
        SuratInteraction model = getBySuratIdOrCreate(surat, userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());
        if (!model.getIsImportant()) {
            model.setIsImportant(true);
            edit(model);
        } else {
            model.setIsImportant(false);
        }
        return model;
    }

    public SuratInteraction setIsRead(Long idSurat){
        Surat surat = suratService.find(idSurat);
        SuratInteraction model = null;

        if (!surat.getKonseptor().getIdOrganization().equals(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization())) {
            model = getBySuratIdOrCreate(surat, userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());

            if (!model.getIsRead()) {
                model.setIsRead(true);
                model.setReadDate(new Date());
                edit(model);
            } else {
                model.setIsRead(false);
            }
        }

        return model;
    }

    public void addToCalendar(AgendaDto dao){
        try {
            Agenda model = new Agenda();
            DateUtil dateUtil = new DateUtil();
            model.setStart(dateUtil.getDateTimeStart(dao.getStart()));
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEnd()));
            if (dao.getIdOrganization() != null) {
                MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getIdOrganization());
                model.setOrganization(org);
            } else {
                model.setOrganization(null);
            }
            model.setLocation(dao.getLocation());
            model.setPerihal(dao.getPerihal());
            model.setDescription(dao.getDescription());
            if (dao.getIdCompany() != null) {
                CompanyCode cc = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(cc);
            } else {
                model.setCompanyCode(null);
            }
            agendaService.create(model);
        }catch (Exception ex){
            ex.getMessage();
        }
    }


    public SuratInteraction getBySuratIdOrCreate(Surat surat, Long idOrganization) {
        Long suratId = surat.getId();
        SuratInteraction suratInteraction = db()
                .where(t -> t.getSurat().getId().equals(suratId) && t.getOrganization().getIdOrganization().equals(idOrganization))
                .findFirst()
                .orElse(null);

        if (suratInteraction == null) {
            suratInteraction = new SuratInteraction();
            suratInteraction.setSurat(surat);
            suratInteraction.setCompanyCode(userSession.getUserSession().getUser().getCompanyCode());
            suratInteraction.setOrganization(userSession.getUserSession().getUser().getOrganizationEntity());

            suratService.create(surat);
        }

        return suratInteraction;
    }

    public SuratInteraction getBySuratIdOrCreateByOrg(Surat surat, Long idOrganization) {
        Long suratId = surat.getId();
        SuratInteraction suratInteraction = db()
                .where(t -> t.getSurat().getId().equals(suratId) && t.getOrganization().getIdOrganization().equals(idOrganization))
                .findFirst()
                .orElse(null);

        if (suratInteraction == null) {
            suratInteraction = new SuratInteraction();
            suratInteraction.setSurat(surat);

            MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(idOrganization);
            if(org.getUser()!=null) {
                suratInteraction.setCompanyCode(org.getUser().getCompanyCode());
            }
            suratInteraction.setOrganization(org);

            this.create(suratInteraction);
        }

        return suratInteraction;
    }
}
