package com.qtasnim.eoffice.services;

import com.github.tennaito.rsql.jpa.JpaCriteriaQueryVisitor;
import com.github.tennaito.rsql.jpa.PredicateBuilder;
import com.qtasnim.eoffice.InternalServerErrorException;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;
import org.apache.commons.lang3.StringUtils;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.jpa.JinqJPAStreamProvider;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.util.List;
import java.util.function.BiConsumer;

public abstract class AbstractFacade<T> extends com.qtasnim.ejb.AbstractFacade<T> {
    @PersistenceContext(unitName = "eofficePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Inject
    private Logger logger;

    protected ClassLoader getLambdaClassLoader() {
        return getClass().getClassLoader();
    }

    protected <V> JPAJinqStream<V> db(Class<V> clazz) {
        if (this.stream == null) {
            this.stream = new JinqJPAStreamProvider(this.getEntityManager().getMetamodel());
            this.stream.setHint("lambdaClassLoader", clazz.getClassLoader());
        }

        return this.stream.streamAll(this.getEntityManager(), clazz);
    }

    public CriteriaQuery<T> parseRSQL(String rsql) throws InternalServerErrorException {
        try {
            if (StringUtils.isNotEmpty(rsql)) {

                // Create the JPA Visitor
                RSQLVisitor<CriteriaQuery<T>, EntityManager> visitor = new JpaCriteriaQueryVisitor<T>((T) this.getEntityClass().newInstance());

                // Parse a RSQL into a Node
                Node rootNode = new RSQLParser().parse(rsql);

                // Visit the node to retrieve CriteriaQuery
                CriteriaQuery<T> query = rootNode.accept(visitor, getEntityManager());

                return query;
            } else {
                CriteriaQuery cq = this.getEntityManager().getCriteriaBuilder().createQuery();
                cq.select(cq.from(this.getEntityClass()));
                return cq;
            }
        } catch (Exception e) {
            logger.error("Invalid rsql", e);

            throw new InternalServerErrorException("Invalid rsql", e);
        }
    }

    private String parseLikeNotation(String rsql) {

        // @see rsql parser: https://github.com/jirutka/rsql-parser

        String[] filterParts = rsql.split(";");     
        try {
            Class entity = this.getEntityClass().newInstance().getClass();
            boolean isStartEnd=false;
            if(rsql.contains("start")&&rsql.contains("end")){
                isStartEnd=true;
            }

            if(rsql.contains("mulai")&&rsql.contains("selesai")){
                isStartEnd=true;
            }

            for (int i = 0; i < filterParts.length; i++) {
                String[] queryParam = filterParts[i].split("==");
                Field field = entity.getDeclaredField(queryParam[0]);
                String type = field.getType().getName();
    
                switch (type) {
    
                    case "java.lang.String":{
                        try {
                            queryParam[1] = "*" + queryParam[1] + "*";
                            filterParts[i] = queryParam[0] + "==" + queryParam[1];
                        } catch (Exception ex) {
                            filterParts[i] = filterParts[i];
                        }
                        break;
                    }

                    /*menyisipkan filter date*/
                    case "java.util.Date":{
                        try{
                            if(queryParam[0].contains("start") && isStartEnd==true){
                                String[] params = queryParam[1].split("T");
                                String filter = params[0]+"T00:00:00.000Z";
                                filterParts[i] = queryParam[0] + "=ge=" +filter;
                            }
                            if(queryParam[0].contains("end") && isStartEnd==true){
                                String[] params = queryParam[1].split("T");
                                String filter = params[0]+"T23:59:59.999Z";
                                filterParts[i] = queryParam[0] + "=le=" + filter;
                            }
                            if(queryParam[0].contains("mulai") && isStartEnd==true){
                                String[] params = queryParam[1].split("T");
                                String filter = params[0]+"T00:00:00.000Z";
                                filterParts[i] = queryParam[0] + "=ge=" + filter;
                            }
                            if(queryParam[0].contains("selesai") && isStartEnd==true){
                                String[] params = queryParam[1].split("T");
                                String filter = params[0]+"T23:59:59.999Z";
                                filterParts[i] = queryParam[0] + "=le=" + filter;
                            }
                            if(isStartEnd==false){
                                String[] params = queryParam[1].split("T");
                                String t1="";
                                String t2="";
                                String filter="";
                                if(queryParam[0].contains("start") || queryParam[0].contains("mulai")){
                                    t1 = params[0]+"T00:00:00.000Z";
                                    t2 = params[0]+"T23:59:59.999Z";
                                }
                                if(queryParam[0].contains("end") || queryParam[0].contains("selesai")){
                                    t1 = params[0]+"T00:00:00.000Z";
                                    t2 = params[0]+"T23:59:59.999Z";
                                }
                                filterParts[i] = queryParam[0] + "=ge=" + t1 +";"+ queryParam[0] + "=le=" + t2;
                            }
                        }catch (Exception ex){
                            filterParts[i] = filterParts[i];
                        }
                        break;
                    }
                }
            }

            return String.join(";", filterParts);

        } catch (Exception ex) {
            return rsql;
        }

    }

    public int count(String rsql) throws InternalServerErrorException {
        CriteriaQuery cq = parseRSQL(rsql);

        Root<T> root = (Root<T>) cq.getRoots().iterator().next();
        cq.select(this.getEntityManager().getCriteriaBuilder().countDistinct(root));
        Query q = this.getEntityManager().createQuery(cq);
        return ((Long)q.getSingleResult()).intValue();
    }

    public List<T> getResultList(String rsql, String sort, int skip, int limit) throws InternalServerErrorException {
        return getResultList(rsql, sort, null, skip, limit);
    }

    public List<T> getResultList(String rsql, String sort, Boolean descending, int skip, int limit) throws InternalServerErrorException {
        return getResultList(rsql, sort, descending, skip, limit, null);
    }

    public List<T> getResultList(String rsql, String sort, Boolean descending, int skip, int limit, BiConsumer<Root<T>, EntityManager> onQuery) throws InternalServerErrorException {
        CriteriaQuery query = parseRSQL(rsql);
        query.distinct(true);

        if (StringUtils.isNotEmpty(sort)) {
            try {
                CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
                Root<T> root = (Root<T>) query.getRoots().iterator().next();
                JpaCriteriaQueryVisitor<T> visitor = new JpaCriteriaQueryVisitor<T>();
                Path sortPath = PredicateBuilder.findPropertyPath(sort, root, getEntityManager(), visitor.getBuilderTools());

                if (descending != null) {
                    if (descending) {
                        query.orderBy(builder.desc(sortPath));
                    } else {
                        query.orderBy(builder.asc(sortPath));
                    }
                }

                if (onQuery != null) {
                    onQuery.accept(root, getEntityManager());
                }
            } catch (Exception e) {
                logger.error("Invalid ordering path: " + sort, e);
            }
        }

        String queryString = getEntityManager().createQuery(query).unwrap(org.hibernate.jpa.internal.QueryImpl.class).getHibernateQuery().getQueryString();
        Object queryString2 = getEntityManager().createQuery(query).unwrap(org.hibernate.jpa.internal.QueryImpl.class).getParameters();
        System.out.println(queryString);

        return getEntityManager()
                .createQuery(query)
                .setFirstResult(skip)
                .setMaxResults(limit)
                .getResultList();
    }

    public List<T> getResultList(String rsql, String sort) throws InternalServerErrorException {
        return getResultList(rsql, sort, null);
    }

    public List<T> getResultList(String rsql, String sort, Boolean descending) throws InternalServerErrorException {
        return getResultList(rsql, sort, descending, null);
    }

    public List<T> getResultList(String rsql, String sort, Boolean descending, BiConsumer<Root<T>, EntityManager> onQuery) throws InternalServerErrorException {
        CriteriaQuery query = parseRSQL(rsql);
        query.distinct(true);

        if (StringUtils.isNotEmpty(sort)) {
            try {
                CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
                Root<T> root = (Root<T>) query.getRoots().iterator().next();
                JpaCriteriaQueryVisitor<T> visitor = new JpaCriteriaQueryVisitor<T>();
                Path sortPath = PredicateBuilder.findPropertyPath(sort, root, getEntityManager(), visitor.getBuilderTools());

                if (descending != null) {
                    if (descending) {
                        query.orderBy(builder.desc(sortPath));
                    } else {
                        query.orderBy(builder.asc(sortPath));
                    }
                }

                if (onQuery != null) {
                    onQuery.accept(root, getEntityManager());
                }
            } catch (Exception e) {
                logger.error("Invalid ordering path: " + sort, e);
            }
        }

        String queryString = getEntityManager().createQuery(query).unwrap(org.hibernate.jpa.internal.QueryImpl.class).getHibernateQuery().getQueryString();
        Object queryString2 = getEntityManager().createQuery(query).unwrap(org.hibernate.jpa.internal.QueryImpl.class).getParameters();
        System.out.println(queryString);

        return getEntityManager()
                .createQuery(query)
                .getResultList();
    }

    public CriteriaQuery getCriteriaQuery(String rsql, String sort, boolean descending, BiConsumer<CriteriaQuery, Root<T>> onQuery) throws InternalServerErrorException {
        CriteriaQuery query = parseRSQL(rsql);
        query.distinct(true);
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        Root<T> root = (Root<T>) query.getRoots().iterator().next();

        if (StringUtils.isNotEmpty(sort)) {
            if (descending) {
                query.orderBy(builder.desc(root.get(sort)));
            } else {
                query.orderBy(builder.asc(root.get(sort)));
            }
        }

        if (onQuery != null) {
            onQuery.accept(query, root);
        }

        return query;
    }

    public Object getResultList(CriteriaQuery query, int skip, int limit) throws InternalServerErrorException {
        return getEntityManager()
                .createQuery(query)
                .setFirstResult(skip)
                .setMaxResults(limit)
                .getResultList();
    }

    public int count(CriteriaQuery cq) throws InternalServerErrorException {
        Root<T> root = (Root<T>) cq.getRoots().iterator().next();
        cq.select(this.getEntityManager().getCriteriaBuilder().countDistinct(root));
        Query q = this.getEntityManager().createQuery(cq);
        return ((Long)q.getSingleResult()).intValue();
    }
}
