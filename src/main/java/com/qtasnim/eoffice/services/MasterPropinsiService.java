package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterPropinsi;
import com.qtasnim.eoffice.db.MasterPropinsi;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterPropinsiDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class MasterPropinsiService extends AbstractFacade<MasterPropinsi> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterPropinsi> getEntityClass() {
        return MasterPropinsi.class;
    }

    public JPAJinqStream<MasterPropinsi> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterPropinsi>getFiltered(String kode,String nama,Boolean isActive){
        JPAJinqStream<MasterPropinsi> query = db();

        if(!Strings.isNullOrEmpty(kode)){
            query = query.where(q ->q.getKode().toLowerCase().contains(kode.toLowerCase()));
        }

        if(!Strings.isNullOrEmpty(nama)){
            query = query.where(q ->q.getNama().toLowerCase().contains(nama.toLowerCase()));
        }

        return query;
    }

    public void saveOrEdit(MasterPropinsiDto dao, Long id){
        boolean isNew =id==null;
        MasterPropinsi model = new MasterPropinsi();
        DateUtil dateUtil = new DateUtil();

        if(isNew){
            model.setKode(dao.getKode());
            model.setNama(dao.getNama());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }

            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.create(model);
        }else{
            model = this.find(id);
            model.setKode(dao.getKode());
            model.setNama(dao.getNama());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }

            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }
    }
}
