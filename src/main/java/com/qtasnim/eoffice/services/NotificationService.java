package com.qtasnim.eoffice.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.NotificationType;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.context.RequestContext;
import com.qtasnim.eoffice.db.MasterEmailTemplate;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.IRequestContext;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Transaction;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.NotificationMessageDto;
import com.qtasnim.eoffice.ws.dto.NotificationPayloadDto;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.apache.commons.jexl2.UnifiedJEXL;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.*;
import javax.inject.Inject;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@LocalBean
@Stateless
public class NotificationService {

    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private MasterEmailTemplateService masterEmailTemplateService;

    @Inject
    private Logger logger;

    @Inject
    private GlobalPushEventService globalPushEventService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    @IRequestContext
    private Transaction transaction;

    public void sendNotification(NotificationType notificationType, String title, String body, MasterUser ...users) {
        DateUtil dateUtil = new DateUtil();
        NotificationMessageDto message = new NotificationMessageDto();
        message.setPayload(new NotificationPayloadDto(dateUtil.getCurrentISODate(new Date()), notificationType.toString(), title, body, Optional.ofNullable(transaction.getRequestContext()).map(RequestContext::getTransactionId).orElse(null)));

        globalPushEventService.push(message, users);
    }

    public void sendInfo(String title, String body) {
        if (userSession != null && userSession.getUserSession() != null && userSession.getUserSession().getUser() != null) {
            sendNotification(NotificationType.INFO, title, body, userSession.getUserSession().getUser());
        }
    }

    public void sendError(String title, String body) {
        if (userSession != null && userSession.getUserSession() != null && userSession.getUserSession().getUser() != null) {
            sendNotification(NotificationType.ERROR, title, body, userSession.getUserSession().getUser());
        }
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendEmail(String templateKey, Consumer<Map<String, Object>> varMappings, String docId, String... targets) {
        _sendEmail(templateKey, varMappings, docId, null, targets);
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendEmail(String templateKey, Consumer<Map<String, Object>> varMappings, MasterUser... targets) {
        _sendEmail(templateKey, varMappings, null, null, Stream.of(targets).filter(t -> !StringUtils.isEmpty(t.getEmail())).map(MasterUser::getEmail).toArray(String[]::new));
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendEmail(String templateKey, Consumer<Map<String, Object>> varMappings, String docId, List<String> cc,MasterUser... targets) {
        _sendEmail(templateKey, varMappings, docId, cc, Stream.of(targets).filter(t -> !StringUtils.isEmpty(t.getEmail())).map(MasterUser::getEmail).toArray(String[]::new));
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendEmail(String subject, String messageBody, String... targets) {
        _sendEmail(subject, messageBody, null, null, targets);
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendEmail(String subject, String messageBody, MasterUser... targets) {
        _sendEmail(subject, messageBody, null, null,Stream.of(targets).filter(t -> !StringUtils.isEmpty(t.getEmail())).map(MasterUser::getEmail).toArray(String[]::new));
    }

    private void _sendEmail(String templateKey, Consumer<Map<String, Object>> varMappings, String docId, List<String> cc, String... targets) {
        MasterEmailTemplate masterEmailTemplate = masterEmailTemplateService.find(templateKey);

        JexlEngine jexl = new JexlEngine();
        JexlContext jc = new MapContext();
        UnifiedJEXL ujexl = new UnifiedJEXL(jexl);

        Map<String, Object> vars = new HashMap<>();
        varMappings.accept(vars);
        vars.forEach(jc::set);

        String subject = ujexl.parse(masterEmailTemplate.getSubject()).evaluate(jc).toString();
        String template = ujexl.parse(masterEmailTemplate.getTemplate()).evaluate(jc).toString();

        _sendEmail(subject, template, docId, cc, targets);
    }

    private void _sendEmail(String subject, String messageBody, String docId, List<String> cc, String... targets) {
        try {
            if (applicationConfig.getEmailEnabled()) {

                Properties prop = new Properties();
                prop.put("mail.smtp.auth", true);
                prop.put("mail.smtp.host", applicationConfig.getEmailHost());
                prop.put("mail.smtp.port", applicationConfig.getEmailPort());

                if (applicationConfig.getEmailTls()) {
                    prop.put("mail.smtp.starttls.enable", "true");
                    prop.put("mail.smtp.ssl.trust", applicationConfig.getEmailHost());
                }

                String username = applicationConfig.getEmailUsername();
                String password = applicationConfig.getEmailPassword();

                javax.mail.Session session = javax.mail.Session.getInstance(prop, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

                if(applicationConfig.getIsEmailTest()) {
                    for(int i=0; i<targets.length; i++){
                        Message message = new MimeMessage(session);
                        message.setFrom(new InternetAddress(applicationConfig.getEmailFromAddress()));
                        message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(applicationConfig.getEmailTest()));
                        if(cc != null && !cc.isEmpty()){
                            String addr = "";
                            for(String s : cc){
                                if(addr != "") {
                                    addr = addr + "," + "69698@kai.id";
                                }else{
                                    addr = "luq.muse@gmail.com";
                                }
                            }
                            message.addRecipients(Message.RecipientType.CC,InternetAddress.parse(addr));
                        }
                        message.setSubject(subject);

                        MimeBodyPart mimeBodyPart = new MimeBodyPart();
                        mimeBodyPart.setContent(messageBody, "text/html");

//                        Multipart multipart = new MimeMultipart("related");
                        Multipart multipart = new MimeMultipart();
                        multipart.addBodyPart(mimeBodyPart);

//                        if (!Strings.isNullOrEmpty(docId)) {
//                            CMISDocument file = fileService.getFile(docId);
//                            String extension = FileUtils.getFileExtension(file.getName()).replace(".", "");
//                            byte[] attachment = fileService.download(docId);
//
//                            MimeBodyPart attachmentBodyPart = new MimeBodyPart();
//                            ByteArrayDataSource bds = new ByteArrayDataSource(attachment, file.getMime());
//                            attachmentBodyPart.setDataHandler(new DataHandler(bds));
//                            attachmentBodyPart.setFileName("Lampiran." + extension);
//                            multipart.addBodyPart(attachmentBodyPart);
//                        }

                        /*Image logo rds*/
//                        MimeBodyPart logoPart = new MimeBodyPart();
//                        byte[] logos = Base64.getDecoder().decode(applicationConfig.getLogo());
//                        ByteArrayDataSource bds = new ByteArrayDataSource(logos, "image/*");
//                        logoPart.setDataHandler(new DataHandler(bds));
//                        logoPart.setContentID("<image>");
//                        multipart.addBodyPart(logoPart);

                        message.setContent(multipart);

                        Transport.send(message);
                    }
                }else{
                    Message message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(applicationConfig.getEmailFromAddress()));
                    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(String.join(",", Stream.of(targets).collect(Collectors.toList()))));
                    if(cc != null && !cc.isEmpty()){
                        String addr = "";
                        for(String s : cc){
                            if(addr != "") {
                                addr = addr + "," + s;
                            }else{
                                addr = s;
                            }
                        }
                        message.addRecipients(Message.RecipientType.CC,InternetAddress.parse(addr));
                    }
                    message.setSubject(subject);

                    MimeBodyPart mimeBodyPart = new MimeBodyPart();
                    mimeBodyPart.setContent(messageBody, "text/html");

                    Multipart multipart = new MimeMultipart();
                    multipart.addBodyPart(mimeBodyPart);

//                    if (!Strings.isNullOrEmpty(docId)) {
//                        CMISDocument file = fileService.getFile(docId);
//                        String extension = FileUtils.getFileExtension(file.getName()).replace(".", "");
//                        byte[] attachment = fileService.download(docId);
//
//                        MimeBodyPart attachmentBodyPart = new MimeBodyPart();
//                        ByteArrayDataSource bds = new ByteArrayDataSource(attachment, file.getMime());
//                        attachmentBodyPart.setDataHandler(new DataHandler(bds));
//                        attachmentBodyPart.setFileName("Lampiran." + extension);
//                        multipart.addBodyPart(attachmentBodyPart);
//                    }

                    message.setContent(multipart);

                    Transport.send(message);
                }
            }
        } catch (MessagingException e) {
            logger.error(String.format("Failed send email to %s", String.join(", ", Stream.of(targets).collect(Collectors.toList()))), e);
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
