package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class PemeriksaNomorManualService extends AbstractFacade<PemeriksaNomorManual> {

    @Override
    protected Class<PemeriksaNomorManual> getEntityClass() {
        return PemeriksaNomorManual.class;
    }

    @Inject
    private PenomoranManualService penomoranManualService;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    public List<PemeriksaNomorManual> getAllBy(Long id){
        PenomoranManual permohonan = penomoranManualService.find(id);
        return db().where(q->q.getPermohonan().equals(permohonan)).toList();
    }

    public PemeriksaNomorManual getByIdPermohonanOrg(Long id, Long idOrg){
        PenomoranManual permohonan = penomoranManualService.find(id);
        MasterStrukturOrganisasi org = organisasiService.find(idOrg);
        return db().where(q->q.getOrganization().equals(org) && q.getPermohonan().equals(permohonan)).findFirst().orElse(null);
    }
}
