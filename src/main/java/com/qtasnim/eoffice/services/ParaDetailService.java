package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.Para;
import com.qtasnim.eoffice.db.ParaDetail;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterStrukturOrganisasiDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class ParaDetailService extends AbstractFacade<ParaDetail>  {
    @Inject
    ParaService paraService;

    @Inject
    MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Override
    protected Class<ParaDetail> getEntityClass() {
        return ParaDetail.class;
    }

    public JPAJinqStream<ParaDetail> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<ParaDetail> getByIdPara(Long idPara){
        return db().where(q->q.getPara().getIdPara()==idPara);
    }

    public void saveOrEdit(List<Long> orgIds, Long idPara){
        DateUtil dateUtil = new DateUtil();
        List<ParaDetail> tmp_pd = this.getByIdPara(idPara).collect(Collectors.toList());
        Para obj = paraService.find(idPara);
        if(tmp_pd.isEmpty()){
            if(!orgIds.isEmpty()){
//                for(MasterStrukturOrganisasiDto so : dao){
//                    ParaDetail model = new ParaDetail();
//                    MasterStrukturOrganisasi mSo = masterStrukturOrganisasiService.find(so.getOrganizationId());
//                    model.setOrganisasi(mSo);
//                    model.setPara(obj);
//                    this.create(model);
//                }
                for(Long ctr:orgIds){
                    ParaDetail model = new ParaDetail();
                    MasterStrukturOrganisasi mSo = masterStrukturOrganisasiService.find(ctr);
                    model.setOrganisasi(mSo);
                    model.setPara(obj);
                    this.create(model);
                }
            }
        }else{

            for(ParaDetail pd:tmp_pd) {
                this.remove(pd);
            }

            if(!orgIds.isEmpty()){
//                for(MasterStrukturOrganisasiDto so : dao){
//                    ParaDetail model = new ParaDetail();
//                    MasterStrukturOrganisasi mSo = masterStrukturOrganisasiService.find(so.getOrganizationId());
//                    model.setOrganisasi(mSo);
//                    model.setPara(obj);
//                    this.create(model);
//                }
                for(Long ctr:orgIds){
                    ParaDetail model = new ParaDetail();
                    MasterStrukturOrganisasi mSo = masterStrukturOrganisasiService.find(ctr);
                    model.setOrganisasi(mSo);
                    model.setPara(obj);
                    this.create(model);
                }
            }
        }
    }
}
