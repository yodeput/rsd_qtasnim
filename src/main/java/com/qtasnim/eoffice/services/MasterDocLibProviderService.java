package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.cmis.CMISProviderNotFoundException;
import com.qtasnim.eoffice.db.MasterDocLibProvider;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class MasterDocLibProviderService extends AbstractFacade<MasterDocLibProvider> {
    @Override
    protected Class<MasterDocLibProvider> getEntityClass() {
        return MasterDocLibProvider.class;
    }

    public MasterDocLibProvider getProvider(String name) throws CMISProviderNotFoundException {
        return db().where(t -> t.getName().equals(name)).findFirst().orElseThrow(CMISProviderNotFoundException::new);
    }
}
