package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.workflows.WorkflowException;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class ProcessTaskService extends AbstractFacade<ProcessTask> {
    @Override
    protected Class<ProcessTask> getEntityClass() {
        return ProcessTask.class;
    }

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private UserTaskService userTaskService;

    public void saveOrEdit(ProcessTask task) {
        ProcessTask found = this.getTaskId(task.getTaskId());
        if (found == null) {
			this.create(task);
            getEntityManager().flush();
        } else if (task.getExecutionStatus().equals(ExecutionStatus.EXECUTED)){
            this.edit(task);
            getEntityManager().flush();
        }
    }

    public List<ProcessTask> getPendingTaskByEntityCode(String entityCode) {
        return db()
                .where(t ->
                        t.getAssignee().getOrganizationCode().equals(entityCode)
                        && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                )
                .toList();
    }

    public List<ProcessTask> getDelegasiApprovalPendingTask() {
        return db()
                .where(t ->
                        (t.getTaskName().contains("Pelakhar") || t.getTaskName().contains("PYMT"))
                        && t.getTaskType().equals("Approval")
                        && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                        && t.getProcessInstance().getProcessDefinition().getDefinition().contains("Next")
                )
                .toList();
    }

    public List<ProcessTask> getNonDelegasiApprovalPendingTask() {
        return db()
                .where(t ->
                        !t.getTaskName().contains("Pelakhar")
                        && !t.getTaskName().contains("PYMT")
                        && t.getTaskType().equals("Approval")
                        && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                        && t.getProcessInstance().getProcessDefinition().getDefinition().contains("Next")
                )
                .toList();
    }

    public List<ProcessTask> getTaskJabatanKosong(List<MasterStrukturOrganisasi> lists) {
        List<ProcessTask> query = db()
                .where(t ->
                        t.getTaskType().equals("Approval")
                                && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                && t.getProcessInstance().getProcessDefinition().getDefinition().contains("Next")
                ).toList();
        List<ProcessTask> result = query.stream().filter(a-> lists.stream().anyMatch(b->b.getIdOrganization().equals(a.getAssignee()
                .getIdOrganization()))).collect(Collectors.toList());
        return result;
    }

    public List<ProcessTask> getPendingTask(String recordRefId,String entityName) {
        List<ProcessTask> query = db()
                .where(t ->
                        t.getTaskType().equals("Approval")
                                && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                && t.getProcessInstance().getRelatedEntity().equals(entityName)
                                && t.getProcessInstance().getRecordRefId().equals(recordRefId)
                ).toList();
        return query;
    }

    public List<ProcessTask> getMyTaskByRecordRefId(String formName, String recordRefId) throws WorkflowException {
        return db()
                .where(t ->
                        t.getProcessInstance().getRecordRefId().equals(recordRefId)
                                && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                && t.getProcessInstance().getProcessDefinition().getFormName().equals(formName))
                .collect(Collectors.toList());
    }

    public List<ProcessTask> getMyTaskByRecordRefId(String formName, String recordRefId, String organizationEntityCode) throws WorkflowException {
        return db()
                .where(t ->
                        t.getProcessInstance().getRecordRefId().equals(recordRefId)
                                && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                && t.getProcessInstance().getProcessDefinition().getFormName().equals(formName)
                                && t.getAssignee().getOrganizationCode().equals(organizationEntityCode))
                .collect(Collectors.toList());
    }

    public List<ProcessTask> getMyTaskByRecordRefId(String formName, String recordRefId, List<Long> pejabatIds) throws WorkflowException {
        return db()
                .where(t ->
                        t.getProcessInstance().getRecordRefId().equals(recordRefId)
                                && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                && t.getProcessInstance().getProcessDefinition().getFormName().equals(formName)
                                && pejabatIds.contains(t.getAssignee().getIdOrganization()))
                .collect(Collectors.toList());
    }

    public List<ProcessTask> getMyTaskByRecordRefId(Long idJenisDokumen, String recordRefId, String organizationEntityCode) throws WorkflowException {
        return db()
                .where(t ->
                        t.getProcessInstance().getRecordRefId().equals(recordRefId)
                                && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                && t.getProcessInstance().getProcessDefinition().getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen)
                                && t.getAssignee().getOrganizationCode().equals(organizationEntityCode))
                .collect(Collectors.toList());
    }

    public List<ProcessTask> getMyTaskByRecordRefId(Long idJenisDokumen, String recordRefId, List<Long> pejabatIds) throws WorkflowException {
        return db()
                .where(t ->
                        t.getProcessInstance().getRecordRefId().equals(recordRefId)
                                && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                && t.getProcessInstance().getProcessDefinition().getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen)
                                && pejabatIds.contains(t.getAssignee().getIdOrganization()))
                .collect(Collectors.toList());
    }

    public List<ProcessTask> getTaskHistoryByRecordRefId(String formName, String recordRefId) throws WorkflowException {
        return db()
                .where(t ->
                        t.getProcessInstance().getRecordRefId().equals(recordRefId)
                                && !t.getExecutionStatus().equals(ExecutionStatus.SKIP)
                                && t.getProcessInstance().getProcessDefinition().getFormName().equals(formName))
                .collect(Collectors.toList());
    }

    public List<ProcessTask> getTaskHistoryByRecordRefId(Long idJenisDokumen, String recordRefId) throws WorkflowException {
        return db()
                .where(t ->
                        t.getProcessInstance().getRecordRefId().equals(recordRefId)
                                && t.getProcessInstance().getProcessDefinition().getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen))
                .collect(Collectors.toList());
    }

    public List<ProcessTask> getTaskHistoryByRecordRefId(Long idJenisDokumen, String formName, String recordRefId) throws WorkflowException {
        return (List<ProcessTask>) getEntityManager().createQuery("SELECT A FROM ProcessTask A WHERE A.processInstance.recordRefId = :recordRefId AND A.executionStatus <> com.qtasnim.eoffice.db.ExecutionStatus.SKIP AND (A.processInstance.processDefinition.jenisDokumen.idJenisDokumen = :idJenisDokumen OR A.processInstance.processDefinition.formName = :formName)")
                .setParameter("recordRefId", recordRefId)
                .setParameter("idJenisDokumen", idJenisDokumen)
                .setParameter("formName", formName)
                .getResultList();
    }

    public List<ProcessTask> getPendingTasks(IWorkflowEntity entity) throws WorkflowException {
        String relatedEntity = entity.getClassName();
        String relatedId = entity.getRelatedId();

        return db()
                .where(t ->
                        t.getProcessInstance().getRecordRefId().equals(relatedId)
                                && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                && t.getProcessInstance().getRelatedEntity().equals(relatedEntity))
                .collect(Collectors.toList());
    }

    public List<ProcessTask> getExecutedTasks(IWorkflowEntity entity) throws WorkflowException {
        String relatedEntity = entity.getClassName();
        String relatedId = entity.getRelatedId();

        return db()
                .where(t ->
                        t.getProcessInstance().getRecordRefId().equals(relatedId)
                                && t.getExecutionStatus().equals(ExecutionStatus.EXECUTED)
                                && t.getProcessInstance().getRelatedEntity().equals(relatedEntity))
                .collect(Collectors.toList());
    }

    public ProcessTask getTaskId(String taskId) {
        return db()
                .where(t ->
                        t.getTaskId().equals(taskId)
                )
                .findFirst()
                .orElse(null);
    }

    public ProcessTask getTaskId(String taskId, String organizationEntityCode) {
        return db()
                .where(t ->
                        t.getTaskId().equals(taskId)
                                && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                && t.getAssignee().getOrganizationCode().equals(organizationEntityCode)
                )
                .findFirst()
                .orElse(null);
    }

    public String getProcessDefinitionByTaskId(String taskId) {
        return db()
                .where(t ->
                        t.getTaskId().equals(taskId)
                )
                .select(t -> t.getProcessInstance().getProcessDefinition().getDefinition())
                .findFirst()
                .orElse(null);
    }

    public List<ProcessTask> getByRecordRefIds(String formId, List<String> recordRefIds) {
        if (recordRefIds.size() == 0) {
            return new ArrayList<>();
        }

        return db()
                .where(t ->
                        t.getProcessInstance().getProcessDefinition().getFormName().equals(formId)
                        && recordRefIds.contains(t.getProcessInstance().getRecordRefId())
                        && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                )
                .toList();
    }

    public List<ProcessTask> getByRecordRefIds(List<String> formIds, List<String> recordRefIds) {
        if (recordRefIds.size() == 0 || formIds.size() == 0) {
            return new ArrayList<>();
        }

        return db()
                .where(t ->
                        formIds.contains(t.getProcessInstance().getProcessDefinition().getFormName())
                        && recordRefIds.contains(t.getProcessInstance().getRecordRefId())
                        && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                )
                .toList();
    }

    public JPAJinqStream<ProcessTask> getPendingTaskByOrgId(Long idOrg) {
        return db()
                .where(t -> t.getAssignee().getIdOrganization().equals(idOrg)
                        && t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                );
    }

    public JPAJinqStream<ProcessTask> getExecutedTaskByOrgId(Long idOrg) {
        return db()
                .where(t -> t.getAssignee().getIdOrganization().equals(idOrg)
                        && t.getExecutionStatus().equals(ExecutionStatus.EXECUTED)
                );
    }

    public ProcessTask getByRecordId(Long idRecord){
        String recId = String.valueOf(idRecord.intValue());
        return db().where(a-> a.getExecutionStatus().equals(ExecutionStatus.EXECUTED) &&
                a.getProcessInstance().getRecordRefId().equals(recId)).sortedDescendingBy(b->b.getId())
                .findFirst().orElse(null);
    }

    public void flush() {
        getEntityManager().flush();
    }

    public JinqStream<ProcessTask> findChildByTaskId(String taskId) {
        return db().where(t -> t.getPrevious() != null && t.getPrevious().getTaskId().equals(taskId));
    }

    public JinqStream<ProcessTask> findDuplicateDisposisi(ProcessTask processTask) {
        Long assigneeId = processTask.getAssignee().getIdOrganization();
        Long taskId = processTask.getId();
        ExecutionStatus status = ExecutionStatus.PENDING;
        String taskType = processTask.getTaskType();
        String relatedTable = processTask.getProcessInstance().getRelatedEntity();
        String relatedRecordRef = processTask.getProcessInstance().getRecordRefId();

        return db().where(t ->
                t.getAssignee().getIdOrganization().equals(assigneeId)
                && t.getExecutionStatus().equals(status)
                && t.getTaskType().equals(taskType)
                && t.getProcessInstance().getRelatedEntity().equals(relatedTable)
                && t.getProcessInstance().getRecordRefId().equals(relatedRecordRef)
                && !t.getId().equals(taskId)
        );
    }

    public JinqStream<ProcessTask> getAll() {
        return db();
    }
}
