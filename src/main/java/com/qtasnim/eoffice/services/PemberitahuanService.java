package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.Pemberitahuan;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.PemberitahuanDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class PemberitahuanService extends AbstractFacade<Pemberitahuan> {
    private static final long serialVersionUID = 1L;

    @Inject
    CompanyCodeService companyCodeService;

    @Override
    protected Class<Pemberitahuan> getEntityClass() {
        return Pemberitahuan.class;
    }

    public JPAJinqStream<Pemberitahuan> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<Pemberitahuan> getFiltered(String name) {
        return db().where(q -> q.getPerihal().toLowerCase().contains(name) || q.getKonten().toLowerCase().contains(name));
    }
     public Pemberitahuan save(Long id, PemberitahuanDto dao){
         Pemberitahuan model = new Pemberitahuan();
         if(id==null){
             this.create(data(model, dao));
         } else {
             model = this.find(id);
             this.edit(data(model, dao));
         }
        return model;
     }

     private Pemberitahuan data (Pemberitahuan model, PemberitahuanDto dao){
         DateUtil dateUtil = new DateUtil();
         model.setPerihal(dao.getPerihal());
         model.setKonten(dao.getKonten());

         if(dao.getCompanyId()!=null){
             CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
             model.setCompanyCode(cCode);
         } else {
             model.setCompanyCode(null);
         }
         model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
         if(dao.getEndDate()!=null){
             model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
         } else {
             model.setEnd(dateUtil.getDefValue());
         }

         return model;
     }

    public Pemberitahuan delete(Long id){
        Pemberitahuan model = this.find(id);
        this.remove(model);
        return model;
    }

}