package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterJenisDokumen;
import com.qtasnim.eoffice.db.MasterKewenanganPenerima;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterKewenanganPenerimaDto;
import liquibase.util.StringUtils;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Stateless
@LocalBean
public class MasterKewenanganPenerimaService extends AbstractFacade<MasterKewenanganPenerima> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterJenisDokumenService jenisDokumenService;

    @Override
    protected Class<MasterKewenanganPenerima> getEntityClass() {
        return MasterKewenanganPenerima.class;
    }

    public JPAJinqStream<MasterKewenanganPenerima> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && !q.getIsDeleted());
    }

    public JPAJinqStream<MasterKewenanganPenerima> getFiltered(String name) {
        return db().where(q -> q.getKewenangan().toLowerCase().contains(name));
    }
     public void saveOrEdit(Long id, MasterKewenanganPenerimaDto dao){
        boolean isNew = id == null;
         MasterKewenanganPenerima model = new MasterKewenanganPenerima();
         DateUtil dateUtil = new DateUtil();

         if(isNew){
             model.setGradePenerima(dao.getGradePenerima());
             model.setGradePengirim(dao.getGradePengirim());

             if(StringUtils.isNotEmpty(dao.getKewenangan())) model.setKewenangan(dao.getKewenangan());

             if(dao.getCompanyId()!=null){
                 CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                 model.setCompanyCode(cCode);
             } else {
                 model.setCompanyCode(null);
             }

             model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

             if(dao.getEndDate()!=null){
                 model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
             } else {
                 model.setEnd(dateUtil.getDefValue());
             }

             if(dao.getIdJenisDokumen()!=null){
                 MasterJenisDokumen doc = jenisDokumenService.find(dao.getIdJenisDokumen());
                 model.setJenisDokumen(doc);
             }else{
                 model.setJenisDokumen(null);
             }
             model.setIsDeleted(false);
             this.create(model);
         } else {
             model = this.find(id);
             model.setGradePenerima(dao.getGradePenerima());
             model.setGradePengirim(dao.getGradePengirim());
             model.setKewenangan(dao.getKewenangan());

             if(dao.getCompanyId()!=null){
                 CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                 model.setCompanyCode(cCode);
             } else {
                 model.setCompanyCode(null);
             }

             model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

             if(dao.getEndDate()!=null){
                 model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
             } else {
                 model.setEnd(dateUtil.getDefValue());
             }

             if(dao.getIdJenisDokumen()!=null){
                 MasterJenisDokumen doc = jenisDokumenService.find(dao.getIdJenisDokumen());
                 model.setJenisDokumen(doc);
             }else{
                 model.setJenisDokumen(null);
             }
             model.setIsDeleted(false);
             this.edit(model);
         }
     }

     public String getKewenanganPenerima(String gradePengirim, Long idDokumen) {
         String result = "";
         MasterKewenanganPenerima obj=new MasterKewenanganPenerima();
         if (idDokumen != null){
             MasterJenisDokumen doc = jenisDokumenService.find(idDokumen);
             obj = db().where(a -> a.getGradePengirim().toLowerCase().contains(gradePengirim.toLowerCase())
                     && a.getJenisDokumen().equals(doc) && !a.getIsDeleted()).findAny().orElse(null);
         }else{
             obj = db().where(a->a.getGradePengirim().toLowerCase().contains(gradePengirim.toLowerCase()) && !a.getIsDeleted()).findAny()
                     .orElse(null);
         }

        if(obj!=null){
            result = obj.getGradePenerima();
        }

        return result;
     }

    public boolean validate(Long id, String pengirim, Long idJenisDokumen, Long companyId) {
        boolean isValid = false;
        JPAJinqStream<MasterKewenanganPenerima> query = db().where(a->!a.getIsDeleted() && a.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen) && a.getCompanyCode().getId().equals(companyId));
        if(id==null){
            List<MasterKewenanganPenerima> tmp = query.collect(Collectors.toList());
            if(pengirim.contains(";")){
                List<String> grades = Arrays.asList(pengirim.split(";"));
                for(String gr:grades){
                    isValid = tmp.stream().noneMatch(b -> b.getGradePengirim().contains(gr));
                    if(!isValid){
                        break;
                    }
                }
            }else{
                isValid = tmp.stream().noneMatch(b -> b.getGradePengirim().contains(pengirim));
            }
        }else {
            List <MasterKewenanganPenerima> data = query.where(b -> b.getId()!=id).collect(Collectors.toList());
            MasterKewenanganPenerima lastData = this.find(id);
            List<String> gradePengirim = Arrays.asList(pengirim.split(";"));
            List<String> gradePengirimOld = Arrays.asList(lastData.getGradePengirim().split(";"));
            List<String> sisaGrade = new ArrayList<String>(gradePengirim);
            if(gradePengirim.size()>gradePengirimOld.size()){
                sisaGrade.removeAll(gradePengirimOld);
            }else{
                sisaGrade = gradePengirim;
            }

            if(!sisaGrade.isEmpty()){
                String tempGrade="";
                for(int i=0;i<sisaGrade.size();i++){
                    if(Strings.isNullOrEmpty(tempGrade)){
                        tempGrade = sisaGrade.get(i);
                    }else {
                        tempGrade = tempGrade + ";" +sisaGrade.get(i);
                    }
                }
                String paramsGrade = tempGrade;
                if(paramsGrade.contains(";")){
                    List<String> grades2 = Arrays.asList(paramsGrade.split(";"));
                    for(String ss:grades2){
                        isValid = data.stream().noneMatch(b -> b.getGradePengirim().contains(ss));
                        if(!isValid){
                            break;
                        }
                    }
                }else{
                    isValid = data.stream().noneMatch(b -> b.getGradePengirim().contains(paramsGrade));
                }
            }
        }

        return isValid;
    }
}