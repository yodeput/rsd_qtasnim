package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.KontakFavoritDto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class KontakFavoritService extends AbstractFacade<KontakFavorit> {

    private static final long serialVersionUID = 1L;

    @Inject
    private MasterUserService userService;

    @Inject
    private MasterStrukturOrganisasiService orgService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Override
    protected Class<KontakFavorit> getEntityClass() {
        return KontakFavorit.class;
    }

    public HashMap<String, Object> getAll(String filter, int skip, int limit, Boolean descending, String sort) {
        List<KontakFavoritDto> result = new ArrayList<>();
        TypedQuery<KontakFavorit> all = getResultList(filter, descending, sort);
        Long length = all.getResultList().stream().count();
        result = MappingToDto(all, skip, limit);
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    public void saveOrEdit(Long id, KontakFavoritDto dao) {
        boolean isNew = id == null;
        KontakFavorit obj = new KontakFavorit();

        if (isNew) {
            MasterStrukturOrganisasi org = orgService.find(dao.getIdOrg());
            obj.setOrganisasi(org);
            obj.setUserOwn(userSession.getUserSession().getUser());
            this.create(obj);
        } else {
            obj = this.find(id);
            MasterStrukturOrganisasi org = orgService.find(dao.getIdOrg());
            obj.setOrganisasi(org);
            this.edit(obj);
        }
    }

    public boolean toggledFavourite(Long idOrganisasi) {
        MasterUser usr = userSession.getUserSession().getUser();

        KontakFavorit found = db().where(q -> q.getUserOwn().equals(usr) && q.getOrganisasi().getIdOrganization().equals(idOrganisasi)).findFirst().orElse(null);
        if (found == null) {
            MasterStrukturOrganisasi org = orgService.getByIdOrganisasi(idOrganisasi);

            KontakFavorit data = new KontakFavorit();
            data.setUserOwn(usr);
            data.setOrganisasi(org);

            this.create(data);
            return true;
        } else {
            this.remove(found);
            return false;
        }
    }

    public TypedQuery<KontakFavorit> getResultList(String filter, Boolean descending, String sort) {
        Long org = userSession.getUserSession().getUser().getId();

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<KontakFavorit> q = cb.createQuery(KontakFavorit.class);
        Root<KontakFavorit> root = q.from(KontakFavorit.class);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.equal(root.get("userOwn").get("id"), org);

        //FilterBy
        if (!Strings.isNullOrEmpty(filter)) {
            Predicate pkeyword
                    = cb.or(
                            cb.like(root.join("organisasi", JoinType.INNER).join("user", JoinType.INNER)
                                    .get("employeeId"), "%" + filter + "%"),
                            cb.like(root.join("organisasi", JoinType.INNER).join("user", JoinType.INNER)
                                    .get("nameFront"), "%" + filter + "%"),
                            cb.like(root.join("organisasi", JoinType.INNER).join("user", JoinType.INNER)
                                    .get("nameMiddleLast"), "%" + filter + "%")
                    );

            predicates.add(pkeyword);
        }

        // WHERE CONDITION
        predicates.add(mainCond);

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);

        if (descending != null && !Strings.isNullOrEmpty(sort)) {
            if (descending) {
                q.orderBy(cb.desc(root.get(sort)));
            } else {
                q.orderBy(cb.asc(root.get(sort)));
            }
        }

        return getEntityManager().createQuery(q);
    }

    private List<KontakFavoritDto> MappingToDto(TypedQuery<KontakFavorit> all, int skip, int limit) {
        List<KontakFavoritDto> result = null;

        if (all != null) {
            result = all.getResultStream().skip(skip).limit(limit).map(KontakFavoritDto::new).collect(Collectors.toList());
        }
        return result;
    }

    public List<MasterStrukturOrganisasi> getOrganisasi() {
        MasterUser user = userSession.getUserSession().getUser();
        return db().where(q -> q.getUserOwn().equals(user)).map(q -> q.getOrganisasi()).collect(Collectors.toList());
    }

    public KontakFavorit getByIdOrg(Long idOrganization) {
        MasterUser usr = userSession.getUserSession().getUser();
        return db().where(q -> q.getUserOwn().equals(usr) && q.getOrganisasi().getIdOrganization().equals(idOrganization)).findFirst().orElse(null);
    }

    public boolean getIsFavoritBy(Long idOrganization) {
        boolean result = false;

        if(userSession.getUserSession() != null) {
            MasterUser usr = userSession.getUserSession().getUser();
            KontakFavorit found = db().where(q -> q.getUserOwn().equals(usr) && q.getOrganisasi().getIdOrganization().equals(idOrganization)).findFirst().orElse(null);
            if (found != null) {
                result = true;
            }
        }

        return result;
    }
}
