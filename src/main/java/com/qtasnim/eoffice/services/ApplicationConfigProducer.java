package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.MasterApplicationConfig;
import com.qtasnim.eoffice.db.MasterApplicationProfile;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import java.util.Optional;

@Stateless
public class ApplicationConfigProducer {
    @Inject
    private MasterApplicationProfileService masterApplicationProfileService;

    @Produces
    public ApplicationConfig produceLogger() {
        MasterApplicationProfile currentProfile = masterApplicationProfileService.getCurrentProfile();

        if (currentProfile != null) {
            ApplicationConfig applicationConfig = ApplicationConfig.defaultConfig();

            for (MasterApplicationConfig config: currentProfile.getConfig()) {
                switch (config.getName()) {
                    case Constants
                            .APPLICATIONCONFIG_APPLICATIONDESCRIPTION: {
                        applicationConfig.setApplicationDescription(config.getValue());
                        break;
                    }
                    case Constants
                            .APPLICATIONCONFIG_APPLICATIONNAME: {
                        applicationConfig.setApplicationName(config.getValue());
                        break;
                    }
                    case Constants
                            .APPLICATIONCONFIG_APPLICATIONLOGO: {
                        applicationConfig.setLogo(config.getValue());
                        break;
                    }
                    case Constants
                            .APPLICATIONCONFIG_JWTKEY: {
                        applicationConfig.setJwtKey(config.getValue());
                        break;
                    }
                    case Constants
                            .APPLICATIONCONFIG_CMISPROVIDER: {
                        applicationConfig.setCmisProvider(config.getValue());
                        break;
                    }
                    case Constants
                            .APPLICATIONCONFIG_DIGISIGNPROVIDER: {
                        applicationConfig.setDigisignProvider(config.getValue());
                        break;
                    }
                    case Constants
                            .APPLICATIONCONFIG_TIMEOUT: {
                        applicationConfig.setTimeout(Integer.parseInt(config.getValue()));
                        break;
                    }
                    case Constants
                            .APPLICATIONCONFIG_BASEURL: {
                        applicationConfig.setBaseUrl(config.getValue());
                        break;
                    }
                    case Constants
                            .APPLICATIONCONFIG_FRONTEND_URL: {
                        applicationConfig.setFrontEndUrl(config.getValue());
                        break;
                    }
                    case Constants
                            .APPLICATIONCONFIG_TEMPDIR: {
                        applicationConfig.setTempDir(Optional.ofNullable(config.getValue()).filter(StringUtils::isNotEmpty).orElse(System.getProperty("java.io.tmpdir")));
                        break;
                    }
                    case Constants
                            .SHARED_DOCLIB_EXPIRATION_DAYS: {
                        applicationConfig.setDocLibExpirationDays(Integer.parseInt(config.getValue()));
                        break;
                    }

                    //HRIS
                    case Constants
                            .INTEGRATION_HRISURL: {
                        applicationConfig.setHrisUrl(config.getValue());
                        break;
                    }
                    case Constants
                            .INTEGRATION_HRISAUTHBEARER: {
                        applicationConfig.setHrisAuthBearer(config.getValue());
                        break;
                    }
                    case Constants
                            .INTEGRATION_LOGINURL: {
                        applicationConfig.setLoginUrl(config.getValue());
                        break;
                    }
                    case Constants
                            .INTEGRATION_SKIPKAILOGIN: {
                        applicationConfig.setSkipKaiLogin(BooleanUtils.toBooleanObject(config.getValue()));
                        break;
                    }

                    //EMAIL
                    case Constants
                            .EMAIL_ENABLED: {
                        applicationConfig.setEmailEnabled(BooleanUtils.toBooleanObject(config.getValue()));
                        break;
                    }
                    case Constants
                            .EMAIL_FROMADDRESS: {
                        applicationConfig.setEmailFromAddress(config.getValue());
                        break;
                    }
                    case Constants
                            .EMAIL_USERNAME: {
                        applicationConfig.setEmailUsername(config.getValue());
                        break;
                    }
                    case Constants
                            .EMAIL_PASSWORD: {
                        applicationConfig.setEmailPassword(config.getValue());
                        break;
                    }
                    case Constants
                            .EMAIL_HOST: {
                        applicationConfig.setEmailHost(config.getValue());
                        break;
                    }
                    case Constants
                            .EMAIL_PORT: {
                        applicationConfig.setEmailPort(config.getValue());
                        break;
                    }
                    case Constants
                            .EMAIL_TLS: {
                        applicationConfig.setEmailTls(BooleanUtils.toBooleanObject(config.getValue()));
                        break;
                    }
                    case Constants
                            .IS_EMAIL_TEST: {
                        applicationConfig.setIsEmailTest(BooleanUtils.toBooleanObject(config.getValue()));
                        break;
                    }
                    case Constants
                            .EMAIL_TEST: {
                        applicationConfig.setEmailTest(config.getValue());
                        break;
                    }

                    //ONLYOFFICE
                    case Constants
                            .ONLYOFFICE_URLDOCUMENTCONVERTER : {
                        applicationConfig.setOnlyOfficeUrlDocumentConverter(config.getValue());
                        break;
                    }
                    case Constants
                            .ONLYOFFICE_URLTEMPSTORAGE : {
                        applicationConfig.setOnlyOfficeUrlTempStorage(config.getValue());
                        break;
                    }
                    case Constants
                            .ONLYOFFICE_URLAPI : {
                        applicationConfig.setOnlyOfficeUrlApi(config.getValue());
                        break;
                    }
                    case Constants
                            .ONLYOFFICE_URLPRELOADER : {
                        applicationConfig.setOnlyOfficeUrlPreLoader(config.getValue());
                        break;
                    }
                    case Constants
                            .ONLYOFFICE_SECRET : {
                        applicationConfig.setOnlyOfficeSecret(config.getValue());
                        break;
                    }
                    case Constants
                            .ONLYOFFICE_CONTAINERORIGIN : {
                        applicationConfig.setOnlyofficeContainerOrigin(config.getValue());
                        break;
                    }
                    case Constants
                            .ONLYOFFICE_ORIGIN : {
                        applicationConfig.setOnlyofficeOrigin(config.getValue());
                        break;
                    }
                    case Constants
                            .ONLYOFFICE_PLUGINLAYOUTINGKEYAPIPATH : {
                        applicationConfig.setOnlyOfficePluginLayoutingKeyApiPath(config.getValue());
                        break;
                    }
                    case Constants
                            .ONLYOFFICE_AUTOSTARTPLUGINS : {
                        applicationConfig.setOnlyOfficeAutoStartPlugins(config.getValue());
                        break;
                    }

                    //SYNC
                    case Constants
                            .SYNC_ORGHOST : {
                        applicationConfig.setSyncOrgHost(config.getValue());
                        break;
                    }
                    case Constants
                            .SYNC_EMPLOYEEHOST : {
                        applicationConfig.setSyncEmployeeHost(config.getValue());
                        break;
                    }
                    case Constants
                            .SYNC_POSISIHOST : {
                        applicationConfig.setSyncPosisiHost(config.getValue());
                        break;
                    }
                    case Constants
                            .SYNC_AUTHBEARER : {
                        applicationConfig.setSyncAuthBearer(config.getValue());
                        break;
                    }

                    //QRCODE
                    case Constants
                            .QRCODE_CONTENTDEFAULT : {
                        applicationConfig.setQrcodeContentDefault(config.getValue());
                        break;
                    }
                    case Constants
                            .QRCODE_SIZEDEFAULT : {
                        applicationConfig.setQrcodeSizeDefault(config.getValue());
                        break;
                    }
                    case Constants
                            .QRCODE_SIZEMINIMAL : {
                        applicationConfig.setQrcodeSizeMinimal(config.getValue());
                        break;
                    }
                    case Constants
                            .QRCODE_SIZELOGOHEIGHTPERCENTAGE  : {
                        applicationConfig.setQrcodeSizeLogoHeightPercentage(config.getValue());
                        break;
                    }

                    //OCR
                    case Constants
                            .OCR_ALLOWEDIMAGEFORMAT : {
                        applicationConfig.setOcrAllowedImageFormat(config.getValue());
                        break;
                    }
                    case Constants
                            .OCR_PREPROCESSISBINARYZE : {
                        applicationConfig.setOcrPreprocessIsBinaryze(config.getValue());
                        break;
                    }
                    case Constants
                            .OCR_PREPROCESSISDESKEW : {
                        applicationConfig.setOcrPreprocessIsDeskew(config.getValue());
                        break;
                    }
                    case Constants
                            .OCR_PREPROCESSISDENOISE : {
                        applicationConfig.setOcrPreprocessIsDenoise(config.getValue());
                        break;
                    }
                    case Constants
                            .OCR_PREPROCESSISENHANCE : {
                        applicationConfig.setOcrPreprocessIsEnhance(config.getValue());
                        break;
                    }

                    //DATAMASTER
                    case Constants
                            .DATAMASTER_LOCATIONDBSPACKAGE : {
                        applicationConfig.setLocationDbsPackage(config.getValue());
                        break;
                    }
                    case Constants
                            .DATAMASTER_LOCATIONSERVICESPACKAGE : {
                        applicationConfig.setLocationServicesPackage(config.getValue());
                        break;
                    }
                    case Constants
                            .DATAMASTER_LOCATIONDTOPACKAGE : {
                        applicationConfig.setLocationDtoPackage(config.getValue());
                        break;
                    }
                    case Constants
                            .DATAMASTER_LISTTOIGNORETRAVERS : {
                        applicationConfig.setDatamasterListToIgnoreTravers(config.getValue());
                        break;
                    }
                    case Constants
                            .DATAMASTER_LISTTOPARENTHANDLE : {
                        applicationConfig.setDatamasterListToParentHandle(config.getValue());
                        break;
                    }

                    //CONVERT
                    case Constants
                            .CONVERT_FILESDOCSERVICEVIEWEDDOCS : {
                        applicationConfig.setFilesDocServiceViewedDocs(config.getValue());
                        break;
                    }
                    case Constants
                            .CONVERT_FILESDOCSERVICEEDITEDDOCS : {
                        applicationConfig.setFilesDocServiceEditedDocs(config.getValue());
                        break;
                    }
                    case Constants
                            .CONVERT_FILESDOCSERVICECONVERTDOCS : {
                        applicationConfig.setFilesDocServiceConvertDocs(config.getValue());
                        break;
                    }
                    case Constants
                            .CONVERT_FILESDOCSERVICETIMEOUT : {
                        applicationConfig.setFilesDocServiceTimeout(config.getValue());
                        break;
                    }
                    case Constants
                            .URL_RESET_PASSWORD : {
                        applicationConfig.setResetPasswordUrl(config.getValue());
                        break;
                    }
                    case Constants
                            .URL_VALIDATE_CODE : {
                        applicationConfig.setValidateCodeUrl(config.getValue());
                        break;
                    }
                    case Constants
                            .URL_CHANGE_PASSWORD : {
                        applicationConfig.setChangePasswordUrl(config.getValue());
                        break;
                    }
                    case Constants
                            .URL_FORGOT_PASSWORD : {
                        applicationConfig.setForgotPasswordUrl(config.getValue());
                        break;
                    }

                    //TWAIN PLUGIN
                    case Constants
                            .PLUGIN_SCANNING_TWAIN_KEY : {
                        applicationConfig.setPlugginScanningTwainKey(config.getValue());
                        break;
                    }
                    case Constants
                            .PLUGIN_SCANNING_TWAIN_IS_TRIAL : {
                        applicationConfig.setPlugginScanningTwainIsTrial(BooleanUtils.toBooleanObject(config.getValue()));
                        break;
                    }

                    //TWAIN PLUGIN
                    case Constants
                            .AMQP_HOST : {
                        applicationConfig.setAmqpHost(config.getValue());
                        break;
                    }
                    case Constants
                            .AMQP_USERNAME : {
                        applicationConfig.setAmqpUsername(config.getValue());
                        break;
                    }
                    case Constants
                            .AMQP_PASSWORD : {
                        applicationConfig.setAmqpPassword(config.getValue());
                        break;
                    }
                    case Constants
                            .AMQP_SECURE : {
                        applicationConfig.setAmqpSecure(BooleanUtils.toBooleanObject(config.getValue()));
                        break;
                    }
                    case Constants
                            .MESSAGE_SERVICE_PROVIDER : {
                        applicationConfig.setMessageServiceProvider(config.getValue());
                        break;
                    }

                    //BACKGROUND SERVICE
                    case Constants
                            .BACKGROUND_SERVICE_WORKER : {
                        applicationConfig.setBackgroundServiceWorker(config.getValue());
                        break;
                    }
                }
            }

            return applicationConfig;
        }

        return ApplicationConfig.defaultConfig();
    }
}
