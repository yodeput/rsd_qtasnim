package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.cmis.ICMISProvider;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.ErrorReportDto;
import com.qtasnim.eoffice.ws.dto.GlobalAttachmentDto;
import com.qtasnim.eoffice.ws.dto.ErrorReportImageDto;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.compress.utils.IOUtils;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class ErrorReportService extends AbstractFacade<ErrorReport> {

    private static final long serialVersionUID = 1L;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private FileService fileService;

    @Inject
    private ICMISProvider cmisProvider;

    @Inject
    @ISessionContext
    private Session userSession;

    @Override
    protected Class<ErrorReport> getEntityClass() {
        return ErrorReport.class;
    }

    public JPAJinqStream<ErrorReport> getAll() {
        return db();
    }

    public void saveOrEdit(Long id, ErrorReportDto dao, List<FormDataBodyPart> body, List<String> deletedIds) throws InternalServerErrorException, IOException, Exception {
        boolean isNew = id == null;
        ErrorReport model;

        if (isNew) {
            model = new ErrorReport();
            model.setKategori(dao.getKategori());
            model.setUraian(dao.getUraian());
            model.setCompanyCode(userSession.getUserSession().getUser().getCompanyCode());
            this.create(model);
            getEntityManager().flush();
            getEntityManager().refresh(model);
        } else {
            model = this.find(id);
//            model.setDocId(dao.getDocId());
            if (!model.getUraian().equals(dao.getUraian())) {
                model.setUraian(dao.getUraian());
            }
            this.edit(model);

            if (deletedIds != null) {
                this.deleteAttachment(deletedIds);

                String docId = "";
                List<String> split = Arrays.asList(model.getDocId().split(";"));
                split = split.stream().filter(q -> !deletedIds.contains(q)).collect(Collectors.toList());
                if (split.stream().count() > 0) {
                    for (String strId : split) {
                        if ("".equals(docId)) {
                            docId = strId;
                        } else {
                            docId = docId + ";" + strId;
                        }
                    }
                }

                model.setDocId(docId);
                this.edit(model);
            }
        }

        Long idReport = model.getId();
        if (body != null && !body.isEmpty()) {
            this.uploadAttachment(idReport, body);
        }
    }

    public ErrorReport save(Long id, ErrorReportDto dao) {
        ErrorReport model = new ErrorReport();

        if (id == null) {
            this.create(addOrEdit(model, dao));
        } else {
            model = this.find(id);
            this.edit(addOrEdit(model, dao));
        }
        return model;
    }

    private ErrorReport addOrEdit(ErrorReport model, ErrorReportDto dao) {
        model.setKategori(dao.getKategori());
        model.setUraian(dao.getUraian());
        model.setCompanyCode(userSession.getUserSession().getUser().getCompanyCode());
        return model;
    }

    public ErrorReport delete(Long id) throws Exception {
        ErrorReport model = this.find(id);
        this.deleteAttachment(id);
        this.remove(model);
        return model;
    }

    public void uploadAttachment(Long idError, List<FormDataBodyPart> body) throws InternalServerErrorException, Exception {
        String docId = "";
//        try {
        ErrorReport error = this.find(idError);
        GlobalAttachment obj = globalAttachmentService.getAttachmentDataByRef("t_error_report", idError).findFirst().orElse(null);

        if (body != null && !body.isEmpty()) {
            if (obj != null) {
                globalAttachmentService.deleteAttachment("t_error_report", idError);
            }

            for (FormDataBodyPart fd : body) {
                String tmpDoc = "";
                BodyPartEntity bodyPartEntity = (BodyPartEntity) fd.getEntity();
                String filename = fd.getContentDisposition().getFileName();

                byte[] documentContent = IOUtils.toByteArray(bodyPartEntity.getInputStream());

                tmpDoc = fileService.upload(filename, documentContent);
                tmpDoc = tmpDoc.replace("workspace://SpacesStore/", "");

                obj = new GlobalAttachment();
                obj.setMimeType(fd.getMediaType().toString());
                obj.setDocGeneratedName(UUID.randomUUID().toString());
                obj.setDocName(filename);
                obj.setCompanyCode(userSession.getUserSession().getUser().getCompanyCode());
                obj.setDocId(tmpDoc);
                obj.setIsKonsep(false);
                obj.setIsPublished(false);
                obj.setSize(new Long(documentContent.length));
                obj.setIsDeleted(false);
                obj.setReferenceId(idError);
                obj.setReferenceTable("t_error_report");
                obj.setNumber("Error_1111");

                globalAttachmentService.create(obj);

                if (docId == "") {
                    docId = tmpDoc;
                } else {
                    docId = docId + ";" + tmpDoc;
                }
            }

            error.setDocId(docId);
            this.edit(error);
        }
//        } catch (Exception ex) {
//            ex.getMessage();
//        }
    }

    public void deleteAttachment(Long idError) throws InternalServerErrorException, Exception {
//        try {
        ErrorReport error = this.find(idError);
        List<String> split = Arrays.asList(error.getDocId().split(";"));
        for (String docId : split) {
            GlobalAttachmentDto attachmentDto = globalAttachmentService.findByDocId(docId);
            GlobalAttachment attachment = globalAttachmentService.find(attachmentDto.getId());

            fileService.delete(attachment.getDocId());
            globalAttachmentService.remove(attachment);
        }
//        } catch (Exception ex) {
//            ex.getMessage();
//        }
    }

    public void deleteAttachment(List<String> deletedFilesId) throws InternalServerErrorException, IOException, Exception {
//        try {
        if (deletedFilesId != null) {
//            ErrorReport error = this.find(idError);
//            List<String> split = Arrays.asList(error.getDocId().split(";"));
//                List<String> deletedIds = Arrays.asList(deletedFilesId.split(";"));

            for (String docId : deletedFilesId) {
                GlobalAttachmentDto attachmentDto = globalAttachmentService.findByDocId(docId);
                GlobalAttachment attachment = globalAttachmentService.find(attachmentDto.getId());

                fileService.delete(docId);
                globalAttachmentService.remove(attachment);

//                    split.remove(split.indexOf(docId));
//                split = split.stream().filter(q -> !q.equals(docId)).collect(Collectors.toList());
            }

//            String docId = "";
//            if (!split.isEmpty()) {
//                for (String id : split) {
//                    if (docId == "") {
//                        docId = id;
//                    } else {
//                        docId = docId + ";" + id;
//                    }
//                }
//            }
//
//            error.setDocId(docId);
//            this.edit(error);
        }

//        } catch (Exception ex) {
//            ex.getMessage();
//        }
    }
//

    public void setDocumentURLs(List<ErrorReportDto> list) {
        list.forEach(a -> {
            if (!Strings.isNullOrEmpty(a.getDocId())) {
                String[] split = a.getDocId().split(";");
                List<ErrorReportImageDto> files = new ArrayList<>();
//                List<AttachmentPreviewDto> files = new ArrayList<>();
                if (split.length > 0) {
                    for (String inc : split) {
                        GlobalAttachmentDto attachmentDto = globalAttachmentService.findByDocId(inc);
                        if (attachmentDto != null) {
//                            try {
//                                AttachmentPreviewDto dto = globalAttachmentService.getPreviewDto(attachmentDto.getId());
//                                files.add(dto);
//                            } catch (CMISProviderException ex) {
//                                Logger.getLogger(ErrorReportService.class.getName()).log(Level.SEVERE, null, ex);
//                            }
                            String url = fileService.getViewLink(attachmentDto.getDocId(), false);
//                            if (Strings.isNullOrEmpty(urls)) {
                            ErrorReportImageDto dto = new ErrorReportImageDto();
                            dto.setDocId(inc);
                            dto.setFileName(attachmentDto.getDocName());
                            dto.setFileType(attachmentDto.getMimeType());
                            dto.setUrl(url);
                            files.add(dto);
//                            }
                        }
                    }
                }
                a.setFiles(files);
            }
        });
    }
}
