package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterNegara;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterNegaraDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class MasterNegaraService extends AbstractFacade<MasterNegara> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterNegara> getEntityClass() {
        return MasterNegara.class;
    }

    public JPAJinqStream<MasterNegara> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterNegara> getFiltered(String kode, String nama, Boolean isActive) {
        JPAJinqStream<MasterNegara> query = db();

        if (!Strings.isNullOrEmpty(nama)) {
            query = query.where(q -> q.getNama().toLowerCase().contains(nama.toLowerCase()));
        }

        return query;
    }

    public void saveOrEdit(MasterNegaraDto dao, Long id) {
        boolean isNew = id == null;
        MasterNegara model = new MasterNegara();
        DateUtil dateUtil = new DateUtil();

        if (isNew) {
            model.setNama(dao.getNama());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if (dao.getCompanyId() != null) {
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }

            if (dao.getEndDate() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.create(model);
        } else {
            model = this.find(id);
            model.setNama(dao.getNama());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if (dao.getCompanyId() != null) {
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }

            if (dao.getEndDate() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }
    }
}
