package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.JerseyClient;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import javax.enterprise.inject.Produces;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class JerseyClientProducer {
    @Produces
    @JerseyClient
    public Client produceLogger() {
        try {
            SSLContext sslcontext = SSLContext.getInstance("TLS");
            sslcontext.init(null, new TrustManager[]{new X509TrustManager()
            {
                public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException{}
                public X509Certificate[] getAcceptedIssuers()
                {
                    return new X509Certificate[0];
                }

            }}, new java.security.SecureRandom());

            HostnameVerifier allowAll = (hostname, session) -> true;

            final ClientConfig clientConfig = new ClientConfig()
                    .register(MultiPartFeature.class);

            return ClientBuilder
                    .newBuilder()
                    .sslContext(sslcontext)
                    .hostnameVerifier(allowAll)
                    .withConfig(clientConfig)
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw new InternalServerErrorException(null, e);
        }
    }
}
