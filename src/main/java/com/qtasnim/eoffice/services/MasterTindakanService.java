package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterTindakan;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterTindakanDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterTindakanService extends AbstractFacade<MasterTindakan> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;


    @Override
    protected Class<MasterTindakan> getEntityClass() {
        return MasterTindakan.class;
    }

    public JPAJinqStream<MasterTindakan> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterTindakan> getByCompanyId(Long id) {
        Date n = new Date();
        CompanyCode companyCode = companyCodeService.find(id);
        return db().where(q -> q.getCompanyCode().equals(companyCode)&&(n.after(q.getStart()) && n.before(q.getEnd())));
    }

    public JPAJinqStream<MasterTindakan> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name));
    }

    public MasterTindakan save(Long id, MasterTindakanDto dao){
        MasterTindakan model = new MasterTindakan();
        DateUtil dateUtil = new DateUtil();

        if(id==null) {
            model.setNama(dao.getNama());
            model.setIsActive(dao.getIsActive());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setCreatedBy(dao.getCreatedBy());
            model.setCreatedDate(new Date());
            model.setModifiedBy("-");
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.create(model);
        } else {
            model = this.find(id);
            model.setNama(dao.getNama());
            model.setIsActive(dao.getIsActive());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }
        return  model;
    }

    public MasterTindakan delete(Long id){
        MasterTindakan model = this.find(id);
        this.remove(model);
        return model;
    }
}