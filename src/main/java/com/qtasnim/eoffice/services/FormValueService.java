package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.FormField;
import com.qtasnim.eoffice.db.FormValue;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.FormValueDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

/**
 * Service untuk fungsi CRUD table m_user
 * 
 * @author seno@qtasnim.com
 */


 @Stateless
 @LocalBean
public class FormValueService extends AbstractFacade<FormValue>{

    private static final long serialVersionUID = -4184209743573831159L;

    @Inject
    private FormFieldService formFieldService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<FormValue> getEntityClass() {
        return FormValue.class;
    }

    public JPAJinqStream<FormValue> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public FormValue findBy(FormField field) {
        Date n = new Date();     
        JPAJinqStream<FormValue> all = db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
        return all.where(q -> q.getFormField().equals(field)).findFirst().orElse(null);
    }
    
    public FormValue save(Long id, FormValueDto dao){
        FormValue model = new FormValue();
        DateUtil dateUtil = new DateUtil();

        if(id==null) {
            FormField formField = formFieldService.find(dao.getFormId());
            model.setFormField(formField);

            model.setValue(dao.getValue());
            model.setCreatedBy(dao.getCreatedBy());
            model.setCreatedDate(new Date());
            model.setModifiedBy("-");
            model.setModifiedDate(new Date());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.create(model);
        } else {
            model = this.find(id);

            FormField formField = formFieldService.find(dao.getFormId());
            model.setFormField(formField);

            model.setFormField(formField);
            model.setValue(dao.getValue());
            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.edit(model);
        }

      return model;
    }

    public FormValue delete(Long id){
        FormValue model = this.find(id);
        this.remove(model);
        
        return model;
    }
}