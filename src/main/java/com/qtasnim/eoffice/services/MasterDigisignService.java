package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.bssn.BSSNProviderNotFoundException;
import com.qtasnim.eoffice.db.MasterDigisign;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class MasterDigisignService extends AbstractFacade<MasterDigisign> {
    @Override
    protected Class<MasterDigisign> getEntityClass() {
        return MasterDigisign.class;
    }

    public MasterDigisign getProvider(String cmisProvider) throws BSSNProviderNotFoundException {
        return db().where(t -> t.getName().equals(cmisProvider)).findFirst().orElseThrow(BSSNProviderNotFoundException::new);
    }
}
