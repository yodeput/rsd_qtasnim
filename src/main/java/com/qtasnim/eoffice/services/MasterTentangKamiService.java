package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterTentangKami;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterTentangKamiDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Base64;
import java.util.Objects;


@Stateless
@LocalBean
public class MasterTentangKamiService extends AbstractFacade<MasterTentangKami> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;


    @Override
    protected Class<MasterTentangKami> getEntityClass() {
        return MasterTentangKami.class;
    }

    public JPAJinqStream<MasterTentangKami> getAll() {
//        Date n = new Date();
//        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
        return db();
    }
    
    public MasterTentangKami getBySort(Long sort) {
        return db().where(q -> Objects.equals(q.getSort(), sort)).findFirst().orElse(null);
    }

    public void saveOrEdit(Long id, MasterTentangKamiDto dao) {
        Long count = this.getAll().count();
        MasterTentangKami model;
        DateUtil dateUtil = new DateUtil();
        boolean isNew = id == null;
        if (isNew) {
            model = new MasterTentangKami();
            model.setPerihal(dao.getPerihal());
            model.setInstansi(dao.getInstansi());
            model.setJabatan(dao.getJabatan());
            model.setIsLeftSide(dao.getIsLeftSide());
            model.setKonten(dao.getKonten());
            if (!Strings.isNullOrEmpty(dao.getPhoto())) {
                model.setPhoto(Base64.getDecoder().decode(dao.getPhoto()));
            }
            if (dao.getCompanyId() != null) {
                CompanyCode company = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(company);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStart()));
            if (dao.getEnd() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEnd()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            model.setSort(count + 1);
            this.create(model);
        } else {
            model = this.find(id);
            model.setPerihal(dao.getPerihal());
            model.setInstansi(dao.getInstansi());
            model.setJabatan(dao.getJabatan());
            model.setIsLeftSide(dao.getIsLeftSide());
            model.setKonten(dao.getKonten());
            if (!Strings.isNullOrEmpty(dao.getPhoto())) {
                model.setPhoto(Base64.getDecoder().decode(dao.getPhoto()));
            }
            if (dao.getCompanyId() != null) {
                CompanyCode company = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(company);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStart()));
            if (dao.getEnd() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEnd()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            if (!Objects.equals(model.getSort(), dao.getSort())) {
                MasterTentangKami other = this.getBySort(dao.getSort());
                if (!Objects.equals(other.getId(), model.getId())) {
                    other.setSort(model.getSort());
                    this.edit(other);
                }
            }
            model.setSort(dao.getSort());
            this.edit(model);
        }
    }

    public MasterTentangKami deleteData(Long id) {
        MasterTentangKami model = this.find(id);
        this.remove(model);

        return model;
    }
}