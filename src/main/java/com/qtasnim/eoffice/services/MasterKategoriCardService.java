package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.KategoriCardDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class MasterKategoriCardService extends AbstractFacade<MasterKategoriCard> {

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyService;

    @Override
    protected Class<MasterKategoriCard> getEntityClass() {
        return MasterKategoriCard.class;
    }

    public JPAJinqStream<MasterKategoriCard> getAll(){
        Long idCompany = userSession.getUserSession().getUser().getCompanyCode().getId();
        CompanyCode company = companyService.find(idCompany);
        return db().where(a->a.getIsDeleted().booleanValue()==false && a.getCompanyCode().equals(company));
    }

    public MasterKategoriCard saveOrEdit(KategoriCardDto dao, Long id) throws Exception {
        try{
            MasterKategoriCard obj = new MasterKategoriCard();
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = companyService.getByCode(user.getCompanyCode().getCode());
            if(id==null){
                obj.setName(dao.getName());
                obj.setColor(dao.getColor());
                obj.setIsDeleted(false);
                obj.setCompanyCode(company);
                create(obj);
            }else{
                obj = find(id);
                obj.setName(dao.getName());
                obj.setColor(dao.getColor());
                obj.setIsDeleted(false);
                obj.setCompanyCode(company);
                edit(obj);
            }
            getEntityManager().flush();
            getEntityManager().refresh(obj);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterKategoriCard deleteKategoriCard(Long id) throws Exception {
        try{
            MasterKategoriCard obj = find(id);
            obj.setIsDeleted(true);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
}
