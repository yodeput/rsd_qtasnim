package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.*;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class SuratDisposisiService extends AbstractFacade<SuratDisposisi> {

    
    @Inject
    private SuratService suratService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private MasterTindakanService masterTindakanService;

    @Inject
    private TindakanDisposisiService tindakanDisposisiService;

    @Override
    protected Class<SuratDisposisi> getEntityClass() {
        return SuratDisposisi.class;
    }

    public JPAJinqStream<SuratDisposisi> getAll() {return db();}

    public SuratDisposisi save(Long id, SuratDisposisiDto dao){
        SuratDisposisi model = new SuratDisposisi();
        DateUtil dateUtil = new DateUtil();

        if(id==null){
            model.setComment(dao.getComment());
            model.setIsRead(false);
            MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getOrganizationId());
            model.setOrganization(org);
            Surat surat = suratService.find(dao.getSuratId());
            model.setSurat(surat);
            /*MasterTindakan tindakan = masterTindakanService.find(dao.getTindakanId());
            model.setTindakan(tindakan);*/
            if(dao.getIdParent()!=null) {
                SuratDisposisi parent = this.find(dao.getIdParent());
                model.setParent(parent);
            } else {
                model.setParent(null);
            }
            if(dao.getAssignedDate()!=null){
                model.setAssignedDate(dateUtil.getDateFromISOString(dao.getAssignedDate()));
            } else {
                model.setAssignedDate(null);
            }
            if(dao.getExecutedDate()!=null){
                model.setExecutedDate((dateUtil.getDateFromISOString(dao.getExecutedDate())));
            } else {
                model.setExecutedDate(null);
            }
            if(dao.getTargetDate()!=null){
                model.setTargetDate((dateUtil.getDateFromISOString(dao.getTargetDate())));
            } else {
                model.setTargetDate(null);
            }

            model.setStatus(dao.getExecutionStatus());
            this.create(model);
            this.create(model);
            getEntityManager().flush();

            if(!dao.getTindakanDisposisi().isEmpty()){
                for(TindakanDisposisiDto pem : dao.getTindakanDisposisi().stream().distinct().collect(Collectors.toList())){
                    SuratDisposisi suratDisposisi = this.find(model.getId());
                    MasterTindakan tindakan = masterTindakanService.find(pem.getIdTindakan());
                    TindakanDisposisi tindakanDisposisi = new TindakanDisposisi();
                    tindakanDisposisi.setSuratDisposisi(suratDisposisi);
                    tindakanDisposisi.setTindakan(tindakan);
                    tindakanDisposisiService.create(tindakanDisposisi);
                }
            }
        } else {
            model = find(id);
            model.setComment(dao.getComment());
            model.setIsRead(dao.getIsRead());
            MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getOrganizationId());
            model.setOrganization(org);
            Surat surat = suratService.find(dao.getSuratId());
            model.setSurat(surat);
            /*MasterTindakan tindakan = masterTindakanService.find(dao.getTindakanId());
            model.setTindakan(tindakan);*/
            if(dao.getIdParent()!=null) {
                SuratDisposisi parent = this.find(dao.getIdParent());
                model.setParent(parent);
            } else {
                model.setParent(null);
            }
            if(dao.getExecutedDate()!=null){
                model.setExecutedDate((dateUtil.getDateFromISOString(dao.getExecutedDate())));
            } else {
                model.setExecutedDate(null);
            }
            if(dao.getTargetDate()!=null){
                model.setTargetDate((dateUtil.getDateFromISOString(dao.getTargetDate())));
            } else {
                model.setTargetDate(null);
            }
            model.setStatus(dao.getExecutionStatus());
            this.edit(model);
        }
        return model;
    }

    public SuratDisposisi markAsRead(Long id){
        SuratDisposisi model = this.find(id);
            model.setIsRead(true);
            this.edit(model);
        return model;
    }

    public SuratDisposisi addTindakan(Long id, Long idTindakan){
        SuratDisposisi model = this.find(id);
        MasterTindakan tindakan = masterTindakanService.find(idTindakan);
        TindakanDisposisi tindakanDisposisi = new TindakanDisposisi();
        tindakanDisposisi.setSuratDisposisi(model);
        tindakanDisposisi.setTindakan(tindakan);
        tindakanDisposisiService.create(tindakanDisposisi);
        return model;
    }

    public List<SuratDisposisi> findByIdSurat(Long id){
        return db().where(q-> q.getSurat().getId().equals(id)).toList();
    }

    public List<SuratDisposisi> findPendingByParentId(Long idSurat, Long parentId){
        if (parentId == null) {
            return db().where(q-> q.getSurat().getId().equals(idSurat)
                    && q.getStatus()==null).toList();
        } else {
            return db().where(q-> q.getSurat().getId().equals(idSurat)
                    && q.getStatus()==null
                    && q.getParent().getId().equals(parentId)).toList();
        }
    }

    public List<SuratDisposisi> findPendingByParentOrg(Long idSurat, String organizationCode){
        if (organizationCode == null) {
            return db()
                    .where(q-> q.getSurat().getId().equals(idSurat) && q.getStatus().equals("PENDING"))
                    .toList();
        } else {
            return db()
                    .where(q-> q.getSurat().getId().equals(idSurat)
                            && q.getStatus().equals("PENDING")
                            && q.getParent().getOrganization().getOrganizationCode().equals(organizationCode)
                            && q.getParent().getStatus().equals("DISPOSISI")
                    )
                    .toList();
        }
    }

    public void create(List<MasterStrukturOrganisasi> masterStrukturOrganisasis, Surat surat, SuratDisposisi parent) {
        Date now = new Date();

        masterStrukturOrganisasis.forEach(t -> {
            SuratDisposisi suratDisposisi = new SuratDisposisi();
            suratDisposisi.setOrganization(t);
            suratDisposisi.setSurat(surat);
            suratDisposisi.setIsRead(false);
            suratDisposisi.setComment(null);
            suratDisposisi.setStatus(null);
            suratDisposisi.setAssignedDate(now);
            suratDisposisi.setParent(parent);

            create(suratDisposisi);
        });
    }
    public void create(List<MasterStrukturOrganisasi> masterStrukturOrganisasis, Surat surat) {
        create(masterStrukturOrganisasis, surat, null);
    }

    public SuratDisposisi findPendingByAssignee(Long idSurat, String organizationCode) {
        return db()
                .where(q->
                        q.getSurat().getId().equals(idSurat)
                                && q.getOrganization().getOrganizationCode().equals(organizationCode)
                                && q.getStatus()==null)
                .findFirst()
                .orElse(null);
    }
}
