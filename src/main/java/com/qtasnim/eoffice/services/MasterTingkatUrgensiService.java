package com.qtasnim.eoffice.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterTingkatUrgensi;

import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterTingkatUrgensiDto;
import org.jinq.jpa.JPAJinqStream;

import java.util.Date;

@Stateless
@LocalBean
public class MasterTingkatUrgensiService extends AbstractFacade<MasterTingkatUrgensi> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterTingkatUrgensi> getEntityClass() {
        return MasterTingkatUrgensi.class;
    }

    public JPAJinqStream<MasterTingkatUrgensi> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public MasterTingkatUrgensiDto saveOrEdit(Long id, MasterTingkatUrgensiDto dao)throws Exception {
        try {
            MasterTingkatUrgensiDto result;
            boolean isNew = id == null;
            MasterTingkatUrgensi model = new MasterTingkatUrgensi();
            DateUtil dateUtil = new DateUtil();
            if (isNew) {
                model.setName(dao.getName());
                CompanyCode company = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(company);
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null){
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.create(model);
            } else {
                model = this.find(id);
                model.setName(dao.getName());
                CompanyCode company = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(company);
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null){
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.edit(model);
            }

            result = new MasterTingkatUrgensiDto(model);
            return result;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterTingkatUrgensi findByNama(String value) {
        return db().where(t -> t.getName().equals(value)).findFirst().orElse(null);
    }
}