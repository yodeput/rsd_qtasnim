package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.PermohonanDokumenPenerima;
import com.qtasnim.eoffice.db.PermohonanDokumenPenerima;
import com.qtasnim.eoffice.db.Surat;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class PenerimaPermohonanDokumenService extends AbstractFacade<PermohonanDokumenPenerima> {

    @Override
    protected Class<PermohonanDokumenPenerima> getEntityClass() {
        return PermohonanDokumenPenerima.class;
    }

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    public List<PermohonanDokumenPenerima> getAllBy(Long idPermohonanDokumen){
        return db().where(q -> q.getPermohonanDokumen().getId().equals(idPermohonanDokumen)).toList();
    }

}
