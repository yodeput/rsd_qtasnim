package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.SyncLog;
import lombok.Getter;

import javax.ejb.Asynchronous;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.util.stream.Stream;

@Singleton
public class DummyService implements IService {
    @Inject
    private BackgroundServiceLoggerService loggerService;

    @Inject
    private SyncLogService syncLogService;

    private Boolean isRun = false;

    @Getter
    private SyncLog syncLog;

    public String getClassName() {
        return Stream.of(getClass().getName().split("\\$")).findFirst().orElse(getClass().getName());
    }

    @Override
    public String getName() {
        return "Dummy Service";
    }

    @Override
    @Asynchronous
    public void run() {
        if (!isRun) {
            isRun = true;
            Instant start = Instant.now();
            syncLog = syncLogService.start(getClassName());
            loggerService.info(syncLog, "Organization Sync Started");

            for (int i = 0; i < 5; i++) {
                loggerService.info(syncLog, Integer.toString(i));

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            loggerService.info(syncLog, "Service stopped");
            loggerService.info(syncLog, "DONE");
            syncLogService.stop(syncLog, start);
            syncLog = null;
            isRun = false;
        }
    }

    @Override
    public Boolean isRun() {
        return isRun;
    }

}
