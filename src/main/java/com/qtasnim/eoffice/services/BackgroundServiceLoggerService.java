package com.qtasnim.eoffice.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.MessageType;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.SyncLog;
import com.qtasnim.eoffice.ws.dto.LoggerMessageDto;
import com.qtasnim.eoffice.ws.dto.LoggerPayloadDto;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;
import java.io.PrintWriter;
import java.io.StringWriter;

@Stateless
@LocalBean
public class BackgroundServiceLoggerService {
    @Resource(mappedName = Constants.JNDI_LOGGER_QUEUE)
    private Queue loggerQueue;

    @Inject
    private JMSContext jmsContext;

    @Inject
    private Logger logger;

    @Inject
    private SyncLogDetailService syncLogDetailService;

    @Inject
    private SyncLogService syncLogService;

    @Inject
    private ApplicationConfig applicationConfig;

    @Asynchronous
    public void info(SyncLog syncLog, String log) {
        log(syncLog, "info", log);
    }

    @Asynchronous
    public void error(SyncLog syncLog, Throwable e) {
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        error(syncLog, stringWriter.toString());
    }

    public void error(SyncLog syncLog, String log) {
        syncLogService.error(syncLog);
        log(syncLog, "error", log);
    }

    public void log(SyncLog syncLog, String type, String log) {
        syncLogDetailService.log(syncLog, type, log);

        LoggerMessageDto message = new LoggerMessageDto();
        message.setPayload(new LoggerPayloadDto(getClass().getName(), log));
        message.setType("[BackgroundServiceState] Log Updated");

        try {
            IMessagingProvider messagingProvider = applicationConfig.getMessagingProvider();
            messagingProvider.send(MessageType.LOG, map -> map.put("logger", syncLog.getType()), getObjectMapper().writeValueAsString(message));
        } catch (Exception e) {
            logger.error(null, e);
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
