package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class PenandatanganDistribusiService extends AbstractFacade<PenandatanganDistribusi> {

    @Override
    protected Class<PenandatanganDistribusi> getEntityClass() {
        return PenandatanganDistribusi.class;
    }

    @Inject
    private DistribusiDokumenService distribusiService;

    @Inject
    @ISessionContext
    private Session userSession;

    public List<PenandatanganDistribusi> getAllBy(Long idDistribusi) {
        DistribusiDokumen obj = distribusiService.find(idDistribusi);
        return db().where(q -> q.getDistribusi().equals(obj)).toList();
    }

    public PenandatanganDistribusi getByIdDistribusiOrg(Long idDistribusi) {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        DistribusiDokumen distribusi = distribusiService.find(idDistribusi);
        return db().where(q -> q.getDistribusi().equals(distribusi) && q.getOrganization().equals(org)).findFirst().orElse(null);
    }

    public PenandatanganDistribusi getByIdOrganisasi(Long idOrg) {
        return db().where(t -> t.getOrganization().getIdOrganization().equals(idOrg)).findFirst().orElse(null);
    }

    public List<DistribusiDokumen> getByIdOrganization(List<MasterStrukturOrganisasi> lists){
        return db().where(t-> lists.contains(t.getOrganization()) && t.getDistribusi().getStatus().equals("SUBMITTED")).select(u->u.getDistribusi()).collect(Collectors.toList());
    }
}
