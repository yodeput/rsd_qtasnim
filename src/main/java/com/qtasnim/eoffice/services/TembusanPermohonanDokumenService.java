package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.db.PermohonanDokumenPenerima;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class TembusanPermohonanDokumenService extends AbstractFacade<PermohonanDokumenTembusan> {

    @Override
    protected Class<PermohonanDokumenTembusan> getEntityClass() {
        return PermohonanDokumenTembusan.class;
    }

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    public List<PermohonanDokumenTembusan> getAllBy(Long idPermohonanDokumen){
        return db().where(q -> q.getPermohonanDokumen().getId().equals(idPermohonanDokumen)).toList();
    }

    public void removeAllBy(Long idPermohonanDokumen){
        List<PermohonanDokumenTembusan> all = db().where(q -> q.getPermohonanDokumen().getId().equals(idPermohonanDokumen)).toList();
        for (PermohonanDokumenTembusan data : all) {
            this.remove(data);
        }
    }

}
