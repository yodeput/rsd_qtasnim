package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterWorkflowProvider;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class MasterWorkflowProviderService extends AbstractFacade<MasterWorkflowProvider> {
    @Override
    protected Class<MasterWorkflowProvider> getEntityClass() {
        return MasterWorkflowProvider.class;
    }

    public MasterWorkflowProvider getByProviderName(String name) {
        return db()
                .where(t -> t.getName().equals(name))
                .findFirst()
                .orElse(null);
    }
}
