package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.ProcessDefinitionVar;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class ProcessDefinitionVarService extends AbstractFacade<ProcessDefinitionVar> {
    @Override
    protected Class<ProcessDefinitionVar> getEntityClass() {
        return ProcessDefinitionVar.class;
    }
}
