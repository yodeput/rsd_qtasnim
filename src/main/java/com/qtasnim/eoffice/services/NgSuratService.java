package com.qtasnim.eoffice.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.RequirePassphraseException;
import com.qtasnim.eoffice.caching.FileCache;
import com.qtasnim.eoffice.cmis.CMISDocument;
import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.cmis.ICMISProvider;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.ClassUtil;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.workflows.*;
import com.qtasnim.eoffice.ws.WorkflowResource;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import com.qtasnim.eoffice.ws.dto.ng.ShowPassphraseConfirmation;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Stateless
@LocalBean
public class NgSuratService extends AbstractFacade<Surat> implements IWorkflowService<Surat> {

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private PemeriksaSuratService pemeriksaSuratService;

    @Inject
    private PenandatanganSuratService penandatanganSuratService;

    @Inject
    private TembusanSuratService tembusanSuratService;

    @Inject
    private FormDefinitionService formDefinitionService;

    @Inject
    private PenerimaSuratService penerimaSuratService;

    @Inject
    private ParaService paraService;

    @Inject
    private ProcessDefinitionService processDefinitionService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private MasterVendorService masterVendorService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private ICMISProvider icmisProvider;

    @Inject
    private FileService fileService;

    @Inject
    private FileCache fileCache;

    @Inject
    private JenisDokumenDocLibService jenisDokumenDocLibService;

    @Inject
    private TemplatingService templatingService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private KonversiOnlyOfficeService konversiOnlyOfficeService;

    @Inject
    private ArsipService arsipService;

    @Inject
    private ReferensiSuratService referensiSuratService;

    @Inject
    private ReferensiSuratDetailService referensiSuratDetailService;

    @Inject
    private MasterAutoNumberService masterAutoNumberService;

    @Inject
    private SuratDisposisiService suratDisposisiService;

    @Inject
    private MasterTindakanService masterTindakanService;

    @Inject
    private TindakanDisposisiService tindakanDisposisiService;

    @Inject
    private ProcessTaskService processTaskService;

    @Inject
    private SuratUndanganService suratUndanganService;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    private Logger logger;

    @Inject
    private ProcessInstanceService processInstanceService;

    @Inject
    private AgendaService agendaService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private PemeriksaPelakharPymtSuratService pemeriksaPelakharPymtSuratService;

    @Inject
    private PenandatanganPelakharPymtSuratService penandatanganPelakharPymtSuratService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private UserTaskService userTaskService;

    @Inject
    private BerkasService berkasService;

    @Inject
    private MasterBahasaService masterBahasaService;

    @Inject
    private MasterKlasifikasiKeamananService masterKlasifikasiKeamananService;

    @Inject
    private MasterKlasifikasiMasalahService masterKlasifikasiMasalahService;

    @Inject
    private MasterTingkatPerkembanganService masterTingkatPerkembanganService;

    @Inject
    private MasterTingkatUrgensiService masterTingkatUrgensiService;

    @Inject
    private MasterKategoriArsipService masterKategoriArsipService;

    @Inject
    private MasterTingkatAksesPublikService masterTingkatAksesPublikService;

    @Inject
    private MasterMediaArsipService masterMediaArsipService;

    @Inject
    private MasterPenciptaArsipService masterPenciptaArsipService;

    @Inject
    private MasterKorsaService masterKorsaService;

    @Inject
    private MasterDigisignService masterDigisignService;

    @Inject
    private GlobalPushEventService globalPushEventService;

    @Inject
    private SharedDocLibService sharedDocLibService;

    @Inject
    private DocLibHistoryService docLibHistoryService;

    @Inject
    private DokumenHistoryService docHistoryService;

    @Inject
    private MasterDelegasiService delegasiService;

    @Inject
    private MasterSekretarisService sekretarisService;

    @Inject
    @ISessionContext
    private Session user;

    @Inject
    private MasterAreaService areaService;

    @Inject
    private MasterKorsaService unitService;

    @Override
    protected Class<Surat> getEntityClass() {
        return Surat.class;
    }

    public JPAJinqStream<Surat> getAll() {
        return db().where(q -> !q.getIsDeleted());
    }

    public NgSuratDto saveOrEditDraft(Long id, SuratDto dao, InputStream stream, FormDataBodyPart body) {
        try {
            boolean isNew = id == null;
            Surat obj = new Surat();
            FormDefinition form = formDefinitionService.find(dao.getIdFormDefinition());
            MasterStrukturOrganisasi konseptor = masterStrukturOrganisasiService.find(dao.getIdKonseptor());

            if (isNew) {
                bindDao(obj, dao, konseptor, form);
                create(obj);

                if (obj.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_UNDANGAN)) {
                    bindUndanganDao(obj, dao);
                }

                getEntityManager().flush();
                handleBalas(obj, dao, konseptor);
                saveAddresses(obj, dao);
            } else {
                obj = find(id);
                bindDao(obj, dao, konseptor, form);
                edit(obj);

                if (obj.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_UNDANGAN)) {
                    bindUndanganDao(obj, dao);
                }

                getEntityManager().flush();

                editAddresses(obj, dao);
            }

            getEntityManager().flush();
            getEntityManager().refresh(obj);

            formDefinitionService.saveDynamicForm(dao, obj, isNew, this);

            NgSuratDto ngSuratDto = new NgSuratDto(obj);
            String docId;

            if (obj.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_REGISTRASI_SURAT)) {
                if (stream != null && body != null) {
                    docId = publishPdf(stream, body, obj, null, null);
                } else {
                    docId = globalAttachmentService.getPublished("t_surat", obj.getId()).findFirst().map(GlobalAttachment::getDocId).orElse(null);
                }

                ngSuratDto.setEditLink(fileService.getEditLink(docId));
                ngSuratDto.setViewLink(fileService.getViewLink(docId, "pdf"));
            } else {
                GlobalAttachment globalAttachment = globalAttachmentService.getDraftDocDataByRef("t_surat", obj.getId()).findFirst().orElse(null);

                if (globalAttachment == null) {
                    docId = setupKonsep(dao, obj);
                } else {
                    docId = updateKonsep(obj, globalAttachment);
                }

                ngSuratDto.setEditLink(fileService.getEditLink(docId));
                ngSuratDto.setViewLink(fileService.getViewLink(docId));
            }

            return ngSuratDto;
        } catch (Exception ex) {
            throw new InternalServerErrorException(ex.getMessage(), ex);
        }
    }

    private void handleBalas(Surat obj, SuratDto dao, MasterStrukturOrganisasi org) {
        if (dao.getIdSuratBalas() != null) {
            Surat refSurat = find(dao.getIdSuratBalas());

            ReferensiSurat referensiSurat = new ReferensiSurat();
            referensiSurat.setOrganisasi(org);
            referensiSurat.setSurat(refSurat);
            referensiSurat.setUser(user.getUserSession().getUser());
            referensiSuratService.create(referensiSurat);

            ReferensiSuratDetail referensiSuratDetail = new ReferensiSuratDetail();
            referensiSuratDetail.setSurat(obj);
            referensiSuratDetail.setReferensiSurat(referensiSurat);
            referensiSuratDetailService.create(referensiSuratDetail);
        }
    }

    private void bindDao(Surat surat, SuratDto dao, MasterStrukturOrganisasi konseptor, FormDefinition form) {
        CompanyCode companyCode = companyCodeService.find(dao.getIdCompany());
        DateUtil dateUtil = new DateUtil();

        surat.setIsDeleted(dao.getIsDeleted());
        surat.setKonseptor(konseptor);
        if(surat.getUserKonseptor()==null) {
            if(konseptor.getUser()!=null) {
                surat.setUserKonseptor(konseptor.getUser());
            }else{
                MasterUser pymt = delegasiService.findByPejabat(konseptor.getIdOrganization()).getTo().getUser();
                if(pymt!=null){
                    surat.setUserKonseptor(pymt);
                    surat.setDelegasiType("PYMT");
                }
            }
        }
        surat.setFormDefinition(form);
        surat.setParent(dao.getIdParent() != null ? this.find(dao.getIdParent()) : null);
        surat.setStatus(dao.getStatus());
        surat.setCompanyCode(companyCode);
        surat.setIsPemeriksaParalel(Optional.ofNullable(dao.getIsPemeriksaParalel()).orElse(false));
        surat.setIsNeedPublishByUnitDokumen(Optional.ofNullable(dao.getIsNeedPublishByUnitDokumen()).orElse(false));
        surat.setIsMultiPemeriksa(Optional.ofNullable(form.getJenisDokumen().getIsMultiPemeriksa()).orElse(true));
        surat.setIsPemeriksaMandatory(Optional.ofNullable(form.getJenisDokumen().getIsPemeriksaMandatory()).orElse(false));
        surat.setIsIncludePejabatMengetahui(Optional.ofNullable(form.getJenisDokumen().getIsIncludePejabatMengetahui()).orElse(false));
        surat.setIsRegulasi(Optional.ofNullable(form.getJenisDokumen().getIsRegulasi()).orElse(false));
        surat.setJumlahPenandatangan(Optional.ofNullable(form.getJenisDokumen().getJumlahPenandatangan()).orElse(1));

        if (dao.getStatusNavigasi() != null) {
            surat.setStatusNavigasi(dao.getStatusNavigasi());
        }

        if (!surat.getIsKonsepEdited()) {
            surat.setIsKonsepEdited(Optional.ofNullable(dao.getIsKonsepEdited()).orElse(false));
        }

        if (form.getJenisDokumen().getBaseCode().equals(Constants.KODE_REGISTRASI_SURAT)) {
            surat.setNoDokumen(dao.getNoDokumen());
        }
        Date date = new Date();
        surat.setModifiedDate(date);

        PenandatanganSuratDto sign = dao.getPenandatanganSurat().stream().findFirst().orElse(null);
        if(sign!=null && sign.getPenandatangan() != null && sign.getPenandatangan().getPersa() != null){
            MasterArea area = areaService.getAreaById(sign.getPenandatangan().getPersa().getId());
            if(area != null) {
                surat.setArea(area);
            }
        }

        if(sign!=null && sign.getPenandatangan() != null && sign.getPenandatangan().getKorsa() != null){
            MasterKorsa unit = unitService.findById(sign.getPenandatangan().getKorsa().getId());
            if(unit != null) {
                surat.setKorsa(unit);
            }
        }

        if(dao.getBonNomor() != null && dao.getBonNomor().booleanValue()){
            surat.setBonNomor(dao.getBonNomor());
            surat.setApprovedDate(dateUtil.getDateFromISOString(dao.getApprovedDate()));
        }else{
            surat.setBonNomor(false);
            surat.setApprovedDate(null);
        }
    }

    private void bindUndanganDao(Surat surat, SuratDto dao) {
        DateUtil dateUtil = new DateUtil();

        surat.getUndangans().stream().filter(t -> dao.getUndangans().stream().noneMatch(v -> v.getId() != null && v.getId().equals(t.getId()))).forEach(t -> {
            suratUndanganService.remove(t);
        });

        dao.getUndangans().forEach(t -> {
            SuratUndangan suratUndangan = surat.getUndangans().stream().filter(v -> v.getId().equals(t.getId())).findFirst().orElse(new SuratUndangan());
            suratUndangan.setStart(dateUtil.getDateFromISOString(t.getUndanganStart()));
            suratUndangan.setEnd(dateUtil.getDateFromISOString(t.getUndanganEnd()));
            suratUndangan.setZona(t.getZona());
            suratUndangan.setSurat(surat);

            if (suratUndangan.getId() != null) {
                suratUndanganService.edit(suratUndangan);
            } else {
                suratUndanganService.create(suratUndangan);
            }
        });

    }

    public NgSuratDto kirimSurat(SuratDto dao, InputStream stream, FormDataBodyPart body) throws Exception {
        boolean isNew = dao.getId() == null;
        Surat obj = new Surat();

        try {
            MasterStrukturOrganisasi konseptor = masterStrukturOrganisasiService.find(dao.getIdKonseptor());
            FormDefinition form = formDefinitionService.find(dao.getIdFormDefinition());
            Date now = new Date();

            if (isNew) {
                bindDao(obj, dao, konseptor, form);

                if (obj.getSubmittedDate() == null) {
                    obj.setSubmittedDate(now);
                }

                obj.setStatusNavigasi(Constants.STATUS_NAVIGASI_KIRIM_OLEH_KONSEPTOR);
                create(obj);

                if (obj.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_UNDANGAN)) {
                    bindUndanganDao(obj, dao);
                }

                getEntityManager().flush();

                handleBalas(obj, dao, konseptor);
                saveAddresses(obj, dao);
            } else {
                obj = this.find(dao.getId());

                if (obj.getSubmittedDate() == null) {
                    obj.setSubmittedDate(now);
                }

                obj.setStatusNavigasi(Constants.STATUS_NAVIGASI_KIRIM_OLEH_KONSEPTOR);
                bindDao(obj, dao, konseptor, form);

                if (obj.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_UNDANGAN)) {
                    bindUndanganDao(obj, dao);
                }

                this.edit(obj);
                getEntityManager().flush();

                editAddresses(obj, dao);

                List<PemeriksaPelakharPymtSurat> assignees = pemeriksaPelakharPymtSuratService.findBySurat(obj.getId());
                assignees.forEach(t -> pemeriksaPelakharPymtSuratService.remove(t));
            }

            getEntityManager().flush();
            getEntityManager().refresh(obj);

            formDefinitionService.saveDynamicForm(dao, obj, isNew, this);
            final Surat surat = obj;
            boolean isKonseptorEqPenandatangan = surat.getKonseptor() != null && surat.getPenandatanganSurat().stream().anyMatch(t -> t.getOrganization() != null && t.getOrganization().getOrganizationCode().equals(surat.getKonseptor().getOrganizationCode()));
            MasterDigisign masterDigisign = masterDigisignService.getProvider(applicationContext.getApplicationConfig().getDigisignProvider());
            IDigiSignService digiSignService = masterDigisign.getWorkflowProvider();

            if (StringUtils.isEmpty(dao.getPassphrase()) && isKonseptorEqPenandatangan && digiSignService.isRequirePassphrase()) {
                throw new RequirePassphraseException();
            }

            if (StringUtils.isNotEmpty(dao.getPassphrase()) && isKonseptorEqPenandatangan && digiSignService.isRequirePassphrase()) {
                digiSignService.validate(user.getUserSession().getUser().getOrganizationEntity(), dao.getPassphrase());
            }

            ProcessDefinition activated = processDefinitionService.getActivated(obj.getFormDefinition().getJenisDokumen().getIdJenisDokumen());
            IWorkflowProvider workflowProvider = masterWorkflowProviderService.getByProviderName(applicationContext.getApplicationConfig().getWorkflowProvider()).getWorkflowProvider();
            workflowProvider.startWorkflow(obj, activated, ProcessEventHandler.DEFAULT
                    .onCompleted((processInstance, status) -> onWorkflowCompleted(status, processInstance, surat, workflowProvider,Optional.ofNullable(dao.getKonseptor().getUser()).map(MasterUserDto::getUsername).orElse(null), dao.getPassphrase()))
                    .onProcessCreated(processInstance -> processInstance.setSurat(surat))
                    .onTasksCreated((prevTask, processTasks) -> onTasksCreated(surat, prevTask, processTasks)), new HashMap<>()
            );

            NgSuratDto ngSuratDto = new NgSuratDto(obj);

            String docId = "";
            if (obj.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_REGISTRASI_SURAT)) {
                if (stream != null && body != null) {
                    publishPdf(stream, body, obj, null, null);
                }
            } else {
                GlobalAttachment globalAttachment = globalAttachmentService.getDraftDocDataByRef("t_surat", obj.getId()).findFirst().orElse(null);

                if (globalAttachment == null) {
                    docId = setupKonsep(dao, obj);
                } else {
                    docId = updateKonsep(obj, globalAttachment);
                }

                ngSuratDto.setEditLink(fileService.getEditLink(docId));
            }

            notificationService.sendInfo("Info", "Dokumen berhasil dikirim");

            return ngSuratDto;
        } catch (RequirePassphraseException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new InternalServerErrorException(ex.getMessage(), ex);
        }
    }

    @Override
    public IWorkflowEntity getEntity(ProcessTask processTask) {
        return find(Long.parseLong(processTask.getProcessInstance().getRecordRefId()));
    }

    @Override
    public boolean isTaskInvalid(TaskActionDto dto, ProcessTask processTask, Map<String, Object> userResponse) throws Exception {
        String taskVarResponse = getValue(userResponse, processTask.getResponseVar()).map(Object::toString).orElse("");
        boolean isPenandatangan = processTask.getTaskName().contains("Penandatangan");
        boolean isApprove = taskVarResponse.equalsIgnoreCase("Setujui");
        boolean isNeedDigiSignPassphrase = processTask.getProcessInstance().getRelatedEntity().equals(Surat.class.getName()) && isApprove && isPenandatangan;
        MasterDigisign masterDigisign = masterDigisignService.getProvider(applicationContext.getApplicationConfig().getDigisignProvider());
        IDigiSignService digiSignService = masterDigisign.getWorkflowProvider();

        if (StringUtils.isEmpty(dto.getPassphrase()) && isNeedDigiSignPassphrase && digiSignService.isRequirePassphrase()) {
            globalPushEventService.push(new ShowPassphraseConfirmation(getObjectMapper().writeValueAsString(dto), "actionSubmit"), user.getUserSession().getUser());
            notificationService.sendInfo("Info", "Passphrase Confirmation");

            return true;
        }

        if (StringUtils.isNotEmpty(dto.getPassphrase()) && isNeedDigiSignPassphrase && digiSignService.isRequirePassphrase()) {
            digiSignService.validate(user.getUserSession().getUser().getOrganizationEntity(), dto.getPassphrase());
        }

        return false;
    }

    @Override
    public void onWorkflowCompleted(ProcessStatus status, ProcessInstance processInstance, IWorkflowEntity entity, IWorkflowProvider workflowProvider, String digisignUsername, String passphrase) {
        Surat surat = (Surat) entity;
        String docId = "";
        GlobalAttachment attachment = globalAttachmentService
                .getKonsep("t_surat", Long.valueOf(entity.getRelatedId()))
                .sortedDescendingBy(GlobalAttachment::getId)
                .findFirst()
                .orElse(null);
        MasterUser initiator = surat.getPenandatanganSurat()
                .stream()
                .filter(t -> t.getOrganization() != null)
                .reduce((first, second) -> second)
                .map(t -> {
                    if (t.getUser() != null) {
                        return t.getUser();
                    } else {
                        return t.getOrganization().getUser();
                    }
                }).orElse(null);

        if (attachment != null) docId = attachment.getDocId();

        Consumer<Map<String, Object>> varMappings = vars -> {
            vars.put("initiator", initiator);
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
        };

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        if (ProcessStatus.APPROVED.equals(status)) {
            surat.setStatusNavigasi(Constants.STATUS_NAVIGASI_KIRIM_OLEH_PENANDATANGAN);
            surat.setCompletionStatus(EntityStatus.APPROVED);
            if(surat.getApprovedDate() == null) {
                surat.setApprovedDate(new Date());
            }

            if (StringUtils.isEmpty(surat.getNoDokumen())) {
                surat.setNoDokumen(masterAutoNumberService.from(Surat.class).next(Surat::getNoDokumen, surat));
            }

            if (StringUtils.isEmpty(surat.getNoAgenda()) && surat.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_REGISTRASI_SURAT)) {
                String klasifikasiMasalah = "";
                if(surat.getFormValue() != null && !surat.getFormValue().isEmpty()){
                    klasifikasiMasalah = surat.getFormValue().stream().filter(a->a.getFormField().getType().equals("master-klasifikasi-masalah")).map(b->b.getValue()).findFirst().orElse(null);
                }
                if(!Strings.isNullOrEmpty(klasifikasiMasalah)) {
                    surat.setKlasifikasiMasalah(klasifikasiMasalah);
                }
                surat.setNoAgenda(masterAutoNumberService.from(Surat.class).next(Surat::getNoAgenda, surat));
            }

            edit(surat);

            GlobalAttachment konsepGlobalAttachment = globalAttachmentService.getDraftDocDataByRef("t_surat", surat.getId()).findFirst().orElse(null);
            if (konsepGlobalAttachment != null) {
                docId = setupPdf(surat, konsepGlobalAttachment, digisignUsername, passphrase);
            }

            List<MasterStrukturOrganisasi> validDisposisi = Stream
                    .concat(
                            surat.getPenerimaSurat().stream().filter(t -> t.getOrganization() != null).map(PenerimaSurat::getOrganization),
                            surat.getTembusanSurat().stream().filter(t -> t.getOrganization() != null).map(TembusanSurat::getOrganization))
                    .collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparingLong(MasterStrukturOrganisasi::getIdOrganization))), ArrayList::new));
            if (validDisposisi.size() > 0) {
                /**
                 * Handle disposisi
                 */
                suratDisposisiService.create(validDisposisi, surat);
                ProcessDefinition activated = processDefinitionService.getActivated(WorkflowResource.DEFAULT_FORM_NAME);
                workflowProvider.startWorkflow(surat, activated, ProcessEventHandler.DEFAULT.onProcessCreated(pi -> pi.setSurat(surat)), new HashMap<>());
            }

            //Send Mail To Penerima & Tembusan External
            penerimaExternalNotification(
                    Stream
                        .concat(
                                surat.getTembusanSurat().stream().filter(t -> t.getVendor() != null).map(TembusanSurat::getVendor),
                                surat.getPenerimaSurat().stream().filter(t -> t.getVendor() != null).map(PenerimaSurat::getVendor))
                        .collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparingLong(MasterVendor::getId))), ArrayList::new)),
                    surat,
                    docId);

            //Send Mail To Penerima Internal
            List<String> listCC = new ArrayList<>();
            for(PenandatanganSurat ps : surat.getPenandatanganSurat()){
                if(ps.getOrganization() != null && ps.getOrganization().getUser() != null){
                    if(!Strings.isNullOrEmpty(ps.getOrganization().getUser().getEmail())) {
                        listCC.add(ps.getOrganization().getUser().getEmail());
                    }
                }else{
                    if(ps.getUser() != null){
                        if(!Strings.isNullOrEmpty(ps.getUser().getEmail())){
                            listCC.add(ps.getUser().getEmail());
                        }
                    }
                }
            }

            for(PemeriksaSurat ps : surat.getPemeriksaSurat()){
                if(ps.getOrganization() != null && ps.getOrganization().getUser() != null){
                    if(!Strings.isNullOrEmpty(ps.getOrganization().getUser().getEmail())) {
                        listCC.add(ps.getOrganization().getUser().getEmail());
                    }
                }else{
                    if(ps.getUser() != null){
                        if(!Strings.isNullOrEmpty(ps.getUser().getEmail())){
                            listCC.add(ps.getUser().getEmail());
                        }
                    }
                }
            }

            notificationService.sendEmail(
                    Constants.TEMPLATE_WORKFLOW_SIGN,
                    varMappings,
                    docId,
                    listCC,
                    surat.getPenerimaSurat()
                            .stream()
                            .filter(q -> q.getOrganization() != null)
                            .map(q -> q.getOrganization().getUser())
                            .toArray(MasterUser[]::new));

            //Send Mail To Tembusan Internal
            notificationService.sendEmail(
                    Constants.TEMPLATE_WORKFLOW_TEMBUSAN,
                    varMappings,
                    docId,
                    null,
                    surat.getTembusanSurat()
                            .stream()
                            .filter(q -> q.getOrganization() != null)
                            .map(t -> t.getOrganization().getUser())
                            .toArray(MasterUser[]::new));

            //set user in t_penandatangan when konseptor == penandatangan
           if(surat.getJumlahPenandatangan().intValue()==1) {
               PenandatanganSurat tempPenandatangan = surat.getPenandatanganSurat().stream().findFirst().orElse(null);
               if (surat.getKonseptor().equals(tempPenandatangan.getOrganization())){
                   MasterUser userSign = tempPenandatangan.getOrganization().getUser();
                   if(userSign!=null) {
                       tempPenandatangan.setUser(userSign);
                       penandatanganSuratService.edit(tempPenandatangan);
                   }else{
                       MasterUser delegasi = user.getUserSession().getUser();
                       boolean isPymt = delegasiService.isPYMT(tempPenandatangan.getOrganization().getIdOrganization(),delegasi.getOrganizationEntity().getIdOrganization());
                       if(isPymt){
                           tempPenandatangan.setUser(delegasi);
                           tempPenandatangan.setDelegasiType("PYMT");
                           penandatanganSuratService.edit(tempPenandatangan);
                       }
                   }
               }
           }

        } else if (ProcessStatus.REJECTED.equals(status)) {
            surat.setStatusNavigasi(Constants.STATUS_NAVIGASI_SIMPAN_OLEH_KONSEPTOR);
            surat.setCompletionStatus(EntityStatus.REJECTED);
            edit(surat);

            EntityResponseDto response = new EntityResponseDto();
            response.setTaskResponse("Dikembalikan");
            response.setTaskExecutor("");
            response.setJenisDokumen(entity.getJenisDokumen());
            response.setPerihal(entity.getPerihal());
            response.setKlasifikasiKeamanan(entity.getKlasifikasiKeamanan());
            MasterUser finalInitiator = initiator;

            List<String> listCC = new ArrayList<>();
            for(PenandatanganSurat ps : surat.getPenandatanganSurat()){
                if(ps.getOrganization() != null && ps.getOrganization().getUser() != null){
                    if(!Strings.isNullOrEmpty(ps.getOrganization().getUser().getEmail())) {
                        listCC.add(ps.getOrganization().getUser().getEmail());
                    }
                }else{
                    if(ps.getUser() != null){
                        if(!Strings.isNullOrEmpty(ps.getUser().getEmail())){
                            listCC.add(ps.getUser().getEmail());
                        }
                    }
                }
            }

            for(PemeriksaSurat ps : surat.getPemeriksaSurat()){
                if(ps.getOrganization() != null && ps.getOrganization().getUser() != null){
                    if(!Strings.isNullOrEmpty(ps.getOrganization().getUser().getEmail())) {
                        listCC.add(ps.getOrganization().getUser().getEmail());
                    }
                }else{
                    if(ps.getUser() != null){
                        if(!Strings.isNullOrEmpty(ps.getUser().getEmail())){
                            listCC.add(ps.getUser().getEmail());
                        }
                    }
                }
            }

            notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_REJECTED, pars -> {
                pars.put("initiator", finalInitiator);
                pars.put("entity", response);
                pars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
            }, docId, listCC, initiator);
        }
    }

    @Override
    public void onTasksCreated(IWorkflowEntity entity, ProcessTask prevTask, List<ProcessTask> tasks) {
        MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
        IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();
        Surat surat = (Surat) entity;

        if (tasks.stream().anyMatch(t -> t.getTaskType().equals("Approval") && t.getTaskName().toLowerCase().contains("pemeriksa"))) {
            surat.setStatusNavigasi(Constants.STATUS_NAVIGASI_KIRIM_OLEH_KONSEPTOR);
            edit(surat);
        } else if (tasks.stream().anyMatch(t -> t.getTaskType().equals("Approval") && t.getTaskName().toLowerCase().contains("penandatangan"))) {
            surat.setStatusNavigasi(Constants.STATUS_NAVIGASI_KIRIM_OLEH_PEMERIKSA);
            edit(surat);
        }

        //handle pymt/pelakhar untuk kebutuhan navigasi
        tasks.forEach(task -> {
            boolean isPenandatangan = task.getTaskName().toLowerCase().contains("penandatangan");
            boolean isPemeriksa = task.getTaskName().toLowerCase().contains("pemeriksa");
            boolean isPelakhar = task.getTaskName().toLowerCase().contains("pelakhar");
            boolean isPymt = task.getTaskName().toLowerCase().contains("pymt");

            if (isPelakhar) {
                if (isPenandatangan) {
                    PenandatanganPelakharPymtSurat penandatangan = new PenandatanganPelakharPymtSurat();
                    penandatangan.setOrganization(task.getAssignee());
                    penandatangan.setSurat(surat);
                    penandatangan.setStatus("pelakhar");

                    MasterUser userAssign = Optional.ofNullable(task.getAssignee().getUser()).orElse(null);
                    if(userAssign!=null) {
                        penandatangan.setUser(userAssign);
                    }else{
                        penandatangan.setUser(user.getUserSession().getUser());
                    }
                    penandatanganPelakharPymtSuratService.create(penandatangan);
                } else if (isPemeriksa) {
                    PemeriksaPelakharPymtSurat pemeriksa = new PemeriksaPelakharPymtSurat();
                    pemeriksa.setOrganization(task.getAssignee());
                    pemeriksa.setSurat(surat);
                    pemeriksa.setStatus("pelakhar");

                    MasterUser userAssign = Optional.ofNullable(task.getAssignee().getUser()).orElse(null);
                    if(userAssign!=null) {
                        pemeriksa.setUser(userAssign);
                    }else{
                        pemeriksa.setUser(user.getUserSession().getUser());
                    }
                    pemeriksaPelakharPymtSuratService.create(pemeriksa);
                }
            } else if (isPymt) {
                if (isPenandatangan) {
                    PenandatanganPelakharPymtSurat penandatangan = new PenandatanganPelakharPymtSurat();
                    penandatangan.setOrganization(task.getAssignee());
                    penandatangan.setSurat(surat);
                    penandatangan.setStatus("pymt");

                    MasterUser userAssign = Optional.ofNullable(task.getAssignee().getUser()).orElse(null);
                    if(userAssign!=null) {
                        penandatangan.setUser(userAssign);
                    }else{
                        penandatangan.setUser(user.getUserSession().getUser());
                    }
                    penandatanganPelakharPymtSuratService.create(penandatangan);
                } else if (isPemeriksa) {
                    PemeriksaPelakharPymtSurat pemeriksa = new PemeriksaPelakharPymtSurat();
                    pemeriksa.setOrganization(task.getAssignee());
                    pemeriksa.setSurat(surat);
                    pemeriksa.setStatus("pymt");

                    MasterUser userAssign = Optional.ofNullable(task.getAssignee().getUser()).orElse(null);
                    if(userAssign!=null) {
                        pemeriksa.setUser(userAssign);
                    }else{
                        pemeriksa.setUser(user.getUserSession().getUser());
                    }
                    pemeriksaPelakharPymtSuratService.create(pemeriksa);
                }
            } else {
                if (isPemeriksa) {
                    PemeriksaPelakharPymtSurat existing = pemeriksaPelakharPymtSuratService.find("assignee", surat.getId(), task.getAssignee().getIdOrganization());

                    if (existing == null) {
                        PemeriksaPelakharPymtSurat pemeriksa = new PemeriksaPelakharPymtSurat();
                        pemeriksa.setOrganization(task.getAssignee());
                        pemeriksa.setSurat(surat);
                        pemeriksa.setStatus("assignee");

                        MasterUser userAssign = Optional.ofNullable(task.getAssignee().getUser()).orElse(null);
                        if(userAssign!=null) {
                            pemeriksa.setUser(userAssign);
                        }else {
                            pemeriksa.setUser(user.getUserSession().getUser());
                        }
                        pemeriksaPelakharPymtSuratService.create(pemeriksa);
                    }
                }
            }

            //#region INSERT INTO USER TASK
            userTaskService.saveOrEdit(null, task, entity);
            //#endregion
        });

        String docId = "";
        GlobalAttachment attchment = globalAttachmentService
                .getKonsep("t_surat", Long.valueOf(entity.getRelatedId()))
                .sortedDescendingBy(q -> q.getId()).findFirst().orElse(null);

        if (attchment != null) docId = attchment.getDocId();

        if (prevTask != null) {
            if(prevTask.getResponse().equals("Kembalikan ke Pemeriksa")){
                for (ProcessTask t : tasks) {
                    toPemeriksaNotification(t, entity, docId, prevTask);
                }
            } else {
                if(!prevTask.getResponse().equals("Kembalikan")) {
                    for (ProcessTask t : tasks) {
                        newTaskNotification(t, entity, docId);
                    }
                }else{
                    for (ProcessTask t : tasks) {
                        toPemeriksaNotification(t, entity, docId, prevTask);
                    }
                }
            }
        } else {
            for (ProcessTask t : tasks) {
                newTaskNotification(t, entity, docId);
            }
        }
    }

    @Override
    public void onTaskExecuting(ProcessTask task) {

    }

    @Override
    public void onTaskExecuted(TaskActionDto dto, IWorkflowProvider workflowProvider, IWorkflowEntity entity, ProcessTask task, TaskEventHandler taskEventHandler, Map<String, Object> userResponse) {
        Surat surat = (Surat) entity;

        if (task.getTaskType().equals("Disposisi")) {
            removeDuplicateDisposisi(task, workflowProvider);

            Date n = new Date();
            surat.setModifiedDate(n);
            edit(surat);

            SuratDisposisi relatedDisposisi = suratDisposisiService.findPendingByAssignee(surat.getId(), task.getAssignee().getOrganizationCode());
            relatedDisposisi.setComment(task.getNote());
            relatedDisposisi.setExecutedDate(new Date());
            relatedDisposisi.setIsSecretaryExecuted(Optional.ofNullable(dto.getIsSecretary()).orElse(false));

            dto.getForms().stream().filter(t -> t.getName().equals("Tindakan")).findFirst().map(FormFieldDto::getValue).filter(StringUtils::isNotEmpty).map(t -> {
                try {
                    return getObjectMapper().readValue(t, new TypeReference<List<MasterTindakanDto>>() {
                    });
                } catch (Exception e) {
                    return new ArrayList<MasterTindakanDto>();
                }
            }).orElse(new ArrayList<>()).stream().forEach(t -> {
                MasterTindakan masterTindakan = masterTindakanService.find(t.getId());
                TindakanDisposisi tindakanDisposisi = new TindakanDisposisi();
                tindakanDisposisi.setSuratDisposisi(relatedDisposisi);
                tindakanDisposisi.setTindakan(masterTindakan);

                tindakanDisposisiService.create(tindakanDisposisi);
            });

            FilePreviewModelDto buktiDisposisi = (FilePreviewModelDto) dto.getForms().stream().filter(t -> t.getName().equals("bukti-disposisi")).findFirst().map(FormFieldDto::getValue).filter(StringUtils::isNotEmpty).map(t -> {
                try {
                    return getObjectMapper().readValue(t, new TypeReference<FilePreviewModelDto>() {
                    });
                } catch (Exception e) {
                    return null;
                }
            }).orElse(null);

            if (buktiDisposisi != null) {
                byte[] content = fileCache.getThenRemove(buktiDisposisi.getFileId());

                if (content != null) {
                    uploadGlobalAttachment("t_surat_disposisi", relatedDisposisi.getId(), buktiDisposisi.getFileName(), buktiDisposisi.getMime(), surat.getCompanyCode(), content);
                }
            }

            AtomicReference<Boolean> _isHadir = new AtomicReference<>();
            dto.getForms()
                    .stream()
                    .filter(t -> t.getName().equals("kehadiran"))
                    .findFirst()
                    .map(FormFieldDto::getValue)
                    .filter(StringUtils::isNotEmpty)
                    .map(t -> t.equals("Hadir"))
                    .ifPresent(isHadir -> {
                        if (isHadir) {
                            addToCalendar(task.getAssignee().getIdOrganization(), surat.getId());
                        }

                        _isHadir.set(isHadir);
                    });

            if (StringUtils.isNotEmpty(task.getResponse())) {
                //Disposisi
                relatedDisposisi.setStatus("DISPOSISI");
                relatedDisposisi.setIsHadir(_isHadir.get());
                MasterUser extUser = user.getUserSession().getUser();
                PenerimaSurat ps = surat.getPenerimaSurat().stream().filter(a-> a.getOrganization()!= null &&
                        a.getOrganization().getIdOrganization().equals(task.getAssignee().getIdOrganization()))
                        .findFirst().orElse(null);
                if(ps!=null){
                    /*Kondisi dimana penerima merupakan pejabat asli*/
                    if(task.getAssignee().getIdOrganization().equals(extUser.getOrganizationEntity().getIdOrganization())){
                        ps.setUser(extUser);
                        penerimaSuratService.edit(ps);
                    }else {
                        /*Kondisi dimana yang melakukan aksi adalah delegasi/sekretaris*/
                        boolean isPYMT = delegasiService.isPYMT(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        boolean isPelakhar = delegasiService.isPelakhar(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        boolean isSecretary = sekretarisService.isSecretary(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        if (isPYMT) {
                            ps.setUser(extUser);
                            ps.setDelegasiType("PYMT");
                        }else if(isSecretary){
                            ps.setUser(extUser);
                            ps.setDelegasiType("Sekretaris");
                        }else if(isPelakhar){
                            ps.setUser(extUser);
                            ps.setDelegasiType("Pelakhar");
                        }
                        penerimaSuratService.edit(ps);
                    }
                }

                TembusanSurat ts = surat.getTembusanSurat().stream().filter(a-> a.getOrganization()!= null &&
                        a.getOrganization().getIdOrganization().equals(task.getAssignee().getIdOrganization()))
                        .findFirst().orElse(null);
                if(ts!=null){
                    /*Kondisi dimana penerima merupakan pejabat asli*/
                    if(task.getAssignee().getIdOrganization().equals(extUser.getOrganizationEntity().getIdOrganization())){
                        ts.setUser(extUser);
                        tembusanSuratService.edit(ts);
                    }else {
                        /*Kondisi dimana yang melakukan aksi adalah delegasi/sekretaris*/
                        boolean isPYMT = delegasiService.isPYMT(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        boolean isPelakhar = delegasiService.isPelakhar(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        boolean isSecretary = sekretarisService.isSecretary(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        if (isPYMT) {
                            ts.setUser(extUser);
                            ts.setDelegasiType("PYMT");
                        }else if(isSecretary){
                            ts.setUser(extUser);
                            ts.setDelegasiType("Sekretaris");
                        }else if(isPelakhar){
                            ts.setUser(extUser);
                            ts.setDelegasiType("Pelakhar");
                        }
                        tembusanSuratService.edit(ts);
                    }
                }
                relatedDisposisi.setUser(extUser);
                suratDisposisiService.edit(relatedDisposisi);

                task.setIsHadir(_isHadir.get());
                processTaskService.edit(task);

                try {
                    List<String> codes = getObjectMapper().readValue(task.getResponse(), new TypeReference<List<String>>() {
                    });
                    List<MasterStrukturOrganisasi> strukturOrganisasis = masterStrukturOrganisasiService.getByCode(codes);
                    suratDisposisiService.create(strukturOrganisasis, surat, relatedDisposisi);

                    ProcessDefinition activated = processDefinitionService.getActivated(WorkflowResource.DEFAULT_FORM_NAME);
                    HashMap<String, Object> props = new HashMap<>();
                    props.put("parent", relatedDisposisi);
                    workflowProvider.startWorkflow(surat, activated, ProcessEventHandler.DEFAULT
                            .onProcessCreated(pi -> pi.setSurat(surat))
                            .onTasksCreated(taskEventHandler.onTasksCreated()), props);
                } catch (IOException e) {
                    throw new InternalServerErrorException(null, e);
                }
            } else {
                //Tindak lanjut
                relatedDisposisi.setStatus("TINDAKLANJUT");
                relatedDisposisi.setIsHadir(_isHadir.get());
                MasterUser extUser = user.getUserSession().getUser();
                PenerimaSurat ps = surat.getPenerimaSurat().stream().filter(a->a.getOrganization().getIdOrganization().equals(task.getAssignee().getIdOrganization())).findFirst().orElse(null);
                if(ps!=null){
                    /*Kondisi dimana penerima merupakan pejabat asli*/
                    if(task.getAssignee().getIdOrganization().equals(extUser.getOrganizationEntity().getIdOrganization())){
                        ps.setUser(extUser);
                        penerimaSuratService.edit(ps);
                    }else {
                        /*Kondisi dimana yang melakukan aksi adalah delegasi/sekretaris*/
                        boolean isPYMT = delegasiService.isPYMT(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        boolean isPelakhar = delegasiService.isPelakhar(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        boolean isSecretary = sekretarisService.isSecretary(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        if (isPYMT) {
                            ps.setUser(extUser);
                            ps.setDelegasiType("PYMT");
                        }else if(isSecretary){
                            ps.setUser(extUser);
                            ps.setDelegasiType("Sekretaris");
                        }else if(isPelakhar){
                            ps.setUser(extUser);
                            ps.setDelegasiType("Pelakhar");
                        }
                        penerimaSuratService.edit(ps);
                    }
                }

                TembusanSurat ts = surat.getTembusanSurat().stream().filter(a->a.getOrganization().getIdOrganization().equals(task.getAssignee().getIdOrganization())).findFirst().orElse(null);
                if(ts!=null){
                    /*Kondisi dimana penerima merupakan pejabat asli*/
                    if(task.getAssignee().getIdOrganization().equals(extUser.getOrganizationEntity().getIdOrganization())){
                        ts.setUser(extUser);
                        tembusanSuratService.edit(ts);
                    }else {
                        /*Kondisi dimana yang melakukan aksi adalah delegasi/sekretaris*/
                        boolean isPYMT = delegasiService.isPYMT(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        boolean isPelakhar = delegasiService.isPelakhar(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        boolean isSecretary = sekretarisService.isSecretary(ps.getOrganization().getIdOrganization(),extUser.getOrganizationEntity().getIdOrganization());
                        if (isPYMT) {
                            ts.setUser(extUser);
                            ts.setDelegasiType("PYMT");
                        }else if(isSecretary){
                            ts.setUser(extUser);
                            ts.setDelegasiType("Sekretaris");
                        }else if(isPelakhar){
                            ts.setUser(extUser);
                            ts.setDelegasiType("Pelakhar");
                        }
                        tembusanSuratService.edit(ts);
                    }
                }
                relatedDisposisi.setUser(extUser);
                suratDisposisiService.edit(relatedDisposisi);

                task.setIsHadir(_isHadir.get());
                processTaskService.edit(task);
            }

            //All tindak lanjut, status surat -> CLOSED
            List<ProcessTask> pendingTasks = processTaskService.getPendingTasks(surat);
            if (pendingTasks.size() == 0) {
                surat.setCompletionStatus(EntityStatus.CLOSED);
                edit(surat);
            }
        } else if (task.getTaskType().equals("Approval")) {
            if (Optional.ofNullable(task.getResponse()).orElse("").toLowerCase().contains("setuju") && StringUtils.isNotEmpty(task.getRelatedAssigneeRecordId())) {
                boolean isPenandatangan = task.getTaskName().toLowerCase().contains("penandatangan");
                boolean isPemeriksa = task.getTaskName().toLowerCase().contains("pemeriksa");
                boolean isPelakhar = task.getTaskName().toLowerCase().contains("pelakhar");
                boolean isPymt = task.getTaskName().toLowerCase().contains("pymt");

                if (isPemeriksa) {
                    PemeriksaSurat pemeriksaSurat = pemeriksaSuratService.find(Long.parseLong(task.getRelatedAssigneeRecordId()));
                    MasterUser user1 = Optional.ofNullable(task.getAssignee().getUser()).orElse(null);
                    if(user1!=null) {
                        pemeriksaSurat.setUser(user1);
                    }else{
                        pemeriksaSurat.setUser(user.getUserSession().getUser());
                    }

                    if (isPelakhar) {
                        pemeriksaSurat.setDelegasiType("Pelakhar");
                    }

                    if (isPymt) {
                        pemeriksaSurat.setDelegasiType("PYMT");
                    }

                    pemeriksaSuratService.edit(pemeriksaSurat);
                }

                if (isPenandatangan) {
                    PenandatanganSurat penandatanganSurat = penandatanganSuratService.find(Long.parseLong(task.getRelatedAssigneeRecordId()));
                    MasterUser user1 =  Optional.ofNullable(task.getAssignee().getUser()).orElse(null);
                    if(user1!=null) {
                        penandatanganSurat.setUser(user1);
                    }else{
                        penandatanganSurat.setUser(user.getUserSession().getUser());
                    }

                    if (isPelakhar) {
                        penandatanganSurat.setDelegasiType("Pelakhar");
                    }

                    if (isPymt) {
                        penandatanganSurat.setDelegasiType("PYMT");
                    }

                    penandatanganSuratService.edit(penandatanganSurat);
                }
            }

            surat.setModifiedDate(new Date());
            edit(surat);

            /*@TODO Tambah insert ke tabel dokumen history*/
            if (user != null && user.getUserSession() != null) {
                String responseVar = task.getResponseVar();
                String response = (String)userResponse.get(responseVar);

                if (response.toLowerCase().contains("kembalikan")) {
                    Long idSurat = ((Surat) entity).getId();
                    DokumenHistoryDto history = new DokumenHistoryDto();
                    history.setReferenceTable("t_surat");
                    history.setReferenceId(idSurat);
                    history.setStatus("REJECT");
                    history.setIdOrganisasi(task.getAssignee().getIdOrganization());
                    MasterUser user1 = Optional.ofNullable(task.getAssignee().getUser()).orElse(null);
                    if(user1!=null) {
                        history.setIdUser(user1.getId());
                    }else{
                        history.setIdUser(user.getUserSession().getUser().getId());
                    }

                    docHistoryService.saveHistory(history);
                }

                if (response.toLowerCase().contains("setujui")) {
                    Long idSurat = ((Surat) entity).getId();
                    DokumenHistoryDto history = new DokumenHistoryDto();
                    history.setReferenceTable("t_surat");
                    history.setReferenceId(idSurat);
                    history.setStatus("APPROVE");
                    history.setIdOrganisasi(task.getAssignee().getIdOrganization());
                    MasterUser user1 = Optional.ofNullable(task.getAssignee().getUser()).orElse(null);
                    if(user1!=null) {
                        history.setIdUser(user1.getId());
                    }else{
                        history.setIdUser(user.getUserSession().getUser().getId());
                    }

                    docHistoryService.saveHistory(history);
                }
            }
        }

        //#region UPDATE User Task
        userTaskService.saveOrEdit(task);
        //#endregion

        // Handle email Notification
        String docId = "";
        GlobalAttachment attchment = globalAttachmentService.getAllDataByRef("t_surat", Long.valueOf(entity.getRelatedId()))
                .sortedDescendingBy(GlobalAttachment::getId).findFirst().orElse(null);
        if (attchment != null) docId = attchment.getDocId();
        if(!task.getResponse().equals("Kembalikan ke Konseptor")) {
            infoTaskNotification(task, entity, docId);
        }

        // Handle web socket Notification
        String taskContextInfo = "Tugas";
        String taskActionInfo = "Diselesaikan";

        if (task.getTaskType().equals("Disposisi")) {
            taskContextInfo = "Surat";

            if (StringUtils.isNotEmpty(task.getResponse())) {
                taskActionInfo = "Didisposisikan";
            } else {
                taskActionInfo = "Ditindaklanjuti";
            }
        }

        String comment = getValue(userResponse, "Comment").map(Object::toString).orElse(null);
        if (!"[REFRESH]".equals(comment)) {
            try {
                notificationService.sendInfo("Info", String.format("%s Berhasil %s", taskContextInfo, taskActionInfo));
            } catch (Exception e) {

            }
        }
    }

    @Override
    public List<ProcessTaskDto> onTaskHistoryRequested(List<ProcessTask> myTasks) {
        return myTasks
                .stream()
                .sorted(Comparator.comparing(ProcessTask::getId).reversed())
                .map(t -> {
                    ProcessTaskDto dto = new ProcessTaskDto(t);

                    boolean isDisposisi = t.getTaskType().equals("Disposisi");

                    if (StringUtils.isNotEmpty(t.getRelatedAssigneeRecordId())) {
                        boolean isPenandatangan = t.getTaskName().toLowerCase().contains("penandatangan");
                        boolean isPemeriksa = t.getTaskName().toLowerCase().contains("pemeriksa");
                        boolean isPelakhar = t.getTaskName().toLowerCase().contains("pelakhar");
                        boolean isPymt = t.getTaskName().toLowerCase().contains("pymt");
                        String delegasiType = "";

                        if (isPelakhar || isPymt) {
                            if (isPelakhar) {
                                delegasiType = "Pelakhar";
                            }

                            if (isPymt) {
                                delegasiType = "PYMT";
                            }

                            if (isPemeriksa) {
                                PemeriksaSurat pemeriksaSurat = pemeriksaSuratService.find(Long.parseLong(t.getRelatedAssigneeRecordId()));

                                if (pemeriksaSurat != null) {
                                    dto.setJabatanDelegasi(String.format("%s %s", delegasiType, pemeriksaSurat.getOrganization().getOrganizationName()));
                                }
                            }

                            if (isPenandatangan) {
                                PenandatanganSurat penandatanganSurat = penandatanganSuratService.find(Long.parseLong(t.getRelatedAssigneeRecordId()));

                                if (penandatanganSurat != null) {
                                    dto.setJabatanDelegasi(String.format("%s %s", delegasiType, penandatanganSurat.getOrganization().getOrganizationName()));
                                }
                            }
                        }else{
                            PenandatanganSurat penandatanganSurat = penandatanganSuratService.find(Long.parseLong(t.getRelatedAssigneeRecordId()));
                            if (penandatanganSurat != null && penandatanganSurat.getOrgAN() != null) {
                                dto.setJabatanDelegasi(String.format("a.n. %s",penandatanganSurat.getOrgAN().getOrganizationName()));
                            }
                        }
                    }

                    if(isDisposisi){
                        Long idSurat = Long.parseLong(t.getProcessInstance().getRecordRefId());
                        PenerimaSurat penerima = penerimaSuratService.findPenerima(t.getAssignee().getIdOrganization(),idSurat);
                        TembusanSurat tembusan = tembusanSuratService.findTembusan(t.getAssignee().getIdOrganization(),idSurat);

                        if(penerima != null){
                            if(!Strings.isNullOrEmpty(penerima.getDelegasiType())){
                                dto.setJabatanDelegasi(String.format("%s %s",penerima.getDelegasiType(), penerima.getOrganization().getOrganizationName()));
                                if(penerima.getUser()!=null) {
                                    MasterUserDto userDto = new MasterUserDto(penerima.getUser());
                                    dto.setAssignee(userDto);
                                }
                            }
                        }else if(tembusan != null){
                            if(!Strings.isNullOrEmpty(tembusan.getDelegasiType())){
                                dto.setJabatanDelegasi(String.format("%s %s",tembusan.getDelegasiType(), tembusan.getOrganization().getOrganizationName()));
                                if(tembusan.getUser()!=null) {
                                    MasterUserDto userDto = new MasterUserDto(tembusan.getUser());
                                    dto.setAssignee(userDto);
                                }
                            }
                        }

                        /*Tindakkan Disposisi*/
                        if(t.getExecutionStatus().equals(ExecutionStatus.PENDING)){
                            SuratDisposisi parent = suratDisposisiService.findByIdSurat(idSurat).stream()
                                    .filter(a -> a.getOrganization().getIdOrganization().equals(t.getAssignee().getIdOrganization()) && a.getParent() != null)
                                    .map(b->b.getParent())
                                    .findFirst().orElse(null);
                            if (parent != null && !parent.getTindakanDisposisi().isEmpty()) {
                                List<String> tindakan = new ArrayList<>();
                                for (TindakanDisposisi td : parent.getTindakanDisposisi()) {
                                    tindakan.add(td.getTindakan().getNama());
                                }
                                dto.setTindakkanDisposisi(tindakan);
                            }

                            if (!Strings.isNullOrEmpty(t.getNote())) {
                                dto.setNote(parent.getComment());
                            }
                        }else {
                            SuratDisposisi sd = suratDisposisiService.findByIdSurat(idSurat).stream()
                                    .filter(a -> a.getOrganization().getIdOrganization().equals(t.getAssignee().getIdOrganization()))
                                    .findFirst().orElse(null);
                            if (sd != null && !sd.getTindakanDisposisi().isEmpty()) {
                                List<String> tindakan = new ArrayList<>();
                                for (TindakanDisposisi td : sd.getTindakanDisposisi()) {
                                    tindakan.add(td.getTindakan().getNama());
                                }
                                dto.setTindakkanDisposisi(tindakan);
                            }

                            if (!Strings.isNullOrEmpty(t.getNote())) {
                                dto.setNote(t.getNote());
                            }
                        }
                    }

                    return dto;
                })
                .collect(Collectors.toList());
    }

    public void addToCalendar(Long idOrganisasi, Long idSurat) {
        Surat surat = find(idSurat);
        MasterStrukturOrganisasi masterStrukturOrganisasi = masterStrukturOrganisasiService.find(idOrganisasi);
        addToCalendar(masterStrukturOrganisasi, surat);
    }

    private void addToCalendar(MasterStrukturOrganisasi assignee, Surat surat) {
        surat.getUndangans().forEach(t -> {
            Agenda model = new Agenda();

            model.setStart(t.getStart());
            model.setEnd(t.getEnd());
            model.setOrganization(assignee);
            model.setLocation(getMetadataByName(surat, "lokasi", "tempat"));
            model.setPerihal(getMetadataByName(surat, "perihal"));
            model.setDescription(getMetadataByName(surat, "catatan"));
            model.setCompanyCode(surat.getCompanyCode());

            agendaService.create(model);
        });


        notificationService.sendInfo("Info", String.format("%s hadir berhasil ditambahkan ke agenda", surat.getFormDefinition().getJenisDokumen().getNamaJenisDokumen()));
    }

    private String getMetadataByName(Surat surat, String... names) {
        return surat.getFormValue().stream()
                .filter(t -> Arrays.stream(names).anyMatch(v -> v.equals(t.getFormField().getMetadata().getNama())) && t.getValue() != null)
                .map(FormValue::getValue).findFirst().orElse(null);
    }

    private String setupKonsep(SuratDto dao, Surat obj) {
        try {
            icmisProvider.openConnection();

            FormDefinition formDefinition = formDefinitionService.find(dao.getIdFormDefinition());
            JenisDokumenDocLib konsep = jenisDokumenDocLibService.getPublished(formDefinition.getJenisDokumen().getIdJenisDokumen()).findFirst().orElse(null);

            if (konsep == null) {
                throw new InternalServerErrorException("No published doclib found");
            }

            byte[] konsepContent = fileService.download(icmisProvider, konsep.getDocId(), konsep.getCmisVersion());
            byte[] templating = templatingService.dbBasedTemplating(konsepContent, obj);
            CMISDocument file = fileService.getFile(icmisProvider, konsep.getDocId());
            String fileExtension = FileUtility.GetFileExtension(file.getName());
            String realDocName = UUID.randomUUID().toString() + fileExtension;

            return uploadGlobalAttachment(icmisProvider, "t_surat", obj.getId(), realDocName, file.getMime(), obj.getCompanyCode(), templating);
        } catch (CMISProviderException ex) {
            throw new InternalServerErrorException(ex.getMessage(), ex);
        } finally {
            icmisProvider.closeConnection();
        }
    }

    private String uploadGlobalAttachment(ICMISProvider icmisProvider, String refferencedTable, Long refferencedId, String realDocName, String mime, CompanyCode companyCode, byte[] templating) {
        String docId = null;

        try {
            String fileExtension = FileUtility.GetFileExtension(realDocName);
            String docGeneratedName = UUID.randomUUID().toString() + fileExtension;
            docId = fileService.upload(icmisProvider, docGeneratedName, templating);

            GlobalAttachment globalAttachment = new GlobalAttachment();
            globalAttachment.setMimeType(mime);
            globalAttachment.setDocGeneratedName(docGeneratedName);
            globalAttachment.setDocName(realDocName);
            globalAttachment.setCompanyCode(companyCode);
            globalAttachment.setDocId(docId);
            globalAttachment.setIsKonsep(true);
            globalAttachment.setIsPublished(false);
            globalAttachment.setIsDeleted(false);
            globalAttachment.setSize((long) templating.length);
            globalAttachment.setReferenceTable(refferencedTable);
            globalAttachment.setReferenceId(refferencedId);
            globalAttachment.setNumber("-");

            String idDocument = masterAutoNumberService.from(GlobalAttachment.class).next(q->q.getIdDocument(), globalAttachment);
            globalAttachment.setIdDocument(idDocument);

            globalAttachmentService.create(globalAttachment);
        } catch (Exception ex) {
            if (StringUtils.isNotEmpty(docId)) {
                fileService.delete(icmisProvider, docId);
            }

            throw new InternalServerErrorException(ex.getMessage(), ex);
        }

        return docId;
    }

    private String uploadGlobalAttachment(String refferencedTable, Long refferencedId, String realDocName, String mime, CompanyCode companyCode, byte[] templating) {
        try {
            icmisProvider.openConnection();

            return uploadGlobalAttachment(icmisProvider, refferencedTable, refferencedId, realDocName, mime, companyCode, templating);
        } catch (CMISProviderException ex) {
            throw new InternalServerErrorException(ex.getMessage(), ex);
        } finally {
            icmisProvider.closeConnection();
        }
    }

    private String updateKonsep(Surat obj, GlobalAttachment globalAttachment) {
        String docId;

        try {
            icmisProvider.openConnection();
            byte[] currentContext = fileService.download(icmisProvider, globalAttachment.getDocId());
            byte[] templating = templatingService.dbBasedTemplating(currentContext, obj);
            docId = fileService.update(icmisProvider, globalAttachment.getDocId(), templating);
            globalAttachment.setSize((long) templating.length);
            DocLibHistory docHistory = docLibHistoryService.getLatestVersion(docId);

            if(docHistory!=null){
                String [] temp = globalAttachment.getIdDocument().split("#");
                if(temp.length>1){
                    String result = temp[0] + "# " +docHistory.getVersion();
                    globalAttachment.setIdDocument(result);
                }else{
                    String idDoc = globalAttachment.getIdDocument()+ " # " + docHistory.getVersion();
                    globalAttachment.setIdDocument(idDoc);
                }
            }else{
                globalAttachment.setIdDocument(globalAttachment.getIdDocument());
            }

            globalAttachmentService.edit(globalAttachment);
        } catch (Exception ex) {
            throw new InternalServerErrorException(ex.getMessage(), ex);
        } finally {
            icmisProvider.closeConnection();
        }

        return docId;
    }

    public String setupPdf(Surat surat, GlobalAttachment konsepGlobalAttachment, String digisignUsername, String passphrase) {
        try {
            icmisProvider.openConnection();

            CMISDocument file = fileService.getFile(icmisProvider, konsepGlobalAttachment.getDocId());
            byte[] konsep = fileService.download(icmisProvider, konsepGlobalAttachment.getDocId());

            //<editor-fold desc="templating ulang">
            byte[] retemplated = templatingService.dbBasedTemplating(konsep, surat, true);

            fileService.update(icmisProvider, konsepGlobalAttachment.getDocId(), retemplated);
            //</editor-fold>

            //<editor-fold desc="hapus watermark draft">
            byte[] noWatermark = templatingService.deleteWatermarkText(retemplated);
            //</editor-fold>

            byte[] converted = konversiOnlyOfficeService.convert(noWatermark, FileUtility.GetFileExtension(file.getName()), ".pdf");
            String fileName = FileUtility.GetFileNameWithoutExtension(konsepGlobalAttachment.getDocName()) + ".pdf";

            return publishPdf(converted, fileName, file.getMime(), surat, icmisProvider, digisignUsername, passphrase);
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        } finally {
            icmisProvider.closeConnection();
        }
    }

    public String publishPdf(byte[] pdf, String fileName, String mime, Surat surat, ICMISProvider icmisProvider, String digisignUsername, String passphrase) {
        try {
            byte[] pdfOcr = Arrays.copyOf(pdf, pdf.length), pdfQr = Arrays.copyOf(pdf, pdf.length);
            MasterStrukturOrganisasi penandatangan = surat.getPenandatanganSurat().stream().map(PenandatanganSurat::getOrganization).filter(Objects::nonNull).findFirst().orElse(null);
            byte[] signed = pdf;

            //<editor-fold desc="ocr" >
            String tempFileName = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID().toString() + ".pdf";
            FileOutputStream fos = new FileOutputStream(tempFileName);
            fos.write(pdfOcr);
            fos.close();
            File tempFile = new File(tempFileName);

            List<String> extractedText = textExtractionService.extractText(tempFile);
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            Files.deleteIfExists(tempFile.toPath());
            //</editor-fold>

            if (penandatangan != null) {
                MasterDigisign masterDigisign = masterDigisignService.getProvider(applicationContext.getApplicationConfig().getDigisignProvider());
                IDigiSignService digiSignService = masterDigisign.getWorkflowProvider();

                signed = digiSignService
                        .onSigning(map -> {
                            map.put("tampilan", "invisible");
                            map.put("passphrase", passphrase);
                            map.put("nik", digisignUsername);
                        })
                        .sign(surat.getPenandatanganSurat().stream().map(PenandatanganSurat::getOrganization).filter(Objects::nonNull).findFirst().orElse(null), pdfQr);
            }

            icmisProvider.openConnection();

            String docId = fileService.upload(icmisProvider, fileName, signed);

            if(surat.getFormDefinition().getJenisDokumen().getBaseCode().equals("SM")) {
                GlobalAttachment globalAttachment = new GlobalAttachment();
                globalAttachment.setMimeType(mime);
                globalAttachment.setDocGeneratedName(UUID.randomUUID().toString());
                globalAttachment.setDocName(fileName);
                globalAttachment.setCompanyCode(surat.getCompanyCode());
                globalAttachment.setDocId(docId);
                globalAttachment.setIsKonsep(false);
                globalAttachment.setIsPublished(true);
                globalAttachment.setIsDeleted(false);
                globalAttachment.setSize((long) pdf.length);
                globalAttachment.setReferenceTable("t_surat");
                globalAttachment.setReferenceId(surat.getId());
                globalAttachment.setNumber(surat.getNoDokumen());
                globalAttachment.setExtractedText(jsonRepresentation);

                String idDocument = masterAutoNumberService.from(GlobalAttachment.class).next(q->q.getIdDocument(), globalAttachment);
                globalAttachment.setIdDocument(idDocument);

                globalAttachmentService.create(globalAttachment);
            }else{
                GlobalAttachment globalAttachment = new GlobalAttachment();
                globalAttachment.setMimeType(mime);
                globalAttachment.setDocGeneratedName(UUID.randomUUID().toString());
                globalAttachment.setDocName(fileName);
                globalAttachment.setCompanyCode(surat.getCompanyCode());
                globalAttachment.setDocId(docId);
                globalAttachment.setIsKonsep(true);
                globalAttachment.setIsPublished(true);
                globalAttachment.setIsDeleted(false);
                globalAttachment.setSize((long) pdf.length);
                globalAttachment.setReferenceTable("t_surat");
                globalAttachment.setReferenceId(surat.getId());
                globalAttachment.setNumber(surat.getNoDokumen());
                globalAttachment.setExtractedText(jsonRepresentation);

                GlobalAttachment konsep = globalAttachmentService.getDraftDocDataByRef("t_surat", surat.getId()).findFirst().orElse(null);
                globalAttachment.setIdDocument(konsep.getIdDocument());
                globalAttachmentService.create(globalAttachment);
            }

            return docId;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        } finally {
            icmisProvider.closeConnection();
        }
    }

    private String publishPdf(InputStream stream, FormDataBodyPart body, Surat obj, String digisignUsername, String passphrase) {
        String docId = globalAttachmentService.getPublished("t_surat", obj.getId()).findFirst().map(GlobalAttachment::getDocId).orElse(null);

        if (StringUtils.isEmpty(docId)) {
            try {
                icmisProvider.openConnection();
                docId = publishPdf(IOUtils.toByteArray(stream), body.getFormDataContentDisposition().getFileName(), body.getMediaType().toString(), obj, icmisProvider, digisignUsername, passphrase);
            } catch (Exception ex) {
                if (StringUtils.isNotEmpty(docId)) {
                    fileService.delete(icmisProvider, docId);
                }

                throw new InternalServerErrorException(ex.getMessage(), ex);
            } finally {
                icmisProvider.closeConnection();
            }
        } else {
            GlobalAttachment attachment = globalAttachmentService.findByDocId("t_surat", obj.getId(), docId);

            if (attachment != null) {
                try {
                    globalAttachmentService.remove(attachment);
                    fileService.delete(docId);
                    icmisProvider.openConnection();
                    docId = publishPdf(IOUtils.toByteArray(stream), body.getFormDataContentDisposition().getFileName(), body.getMediaType().toString(), obj, icmisProvider, digisignUsername, passphrase);
                } catch (Exception ex) {
                    if (StringUtils.isNotEmpty(docId)) {
                        fileService.delete(icmisProvider, docId);
                    }

                    throw new InternalServerErrorException(ex.getMessage(), ex);
                } finally {
                    icmisProvider.closeConnection();
                }
            }
        }

        return docId;
    }

    private void saveAddresses(Surat obj, SuratDto dao) {
        /**
         * SAVE PEMERIKSA
         */

        List<PemeriksaSuratDto> pemeriksaSuratList = Optional.ofNullable(dao.getPemeriksaSurat()).orElse(new ArrayList<>());
        for (PemeriksaSuratDto pem : pemeriksaSuratList) {
            MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());

            PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
            pemeriksaSurat.setSurat(obj);
            pemeriksaSurat.setOrganization(pemeriksa);
            pemeriksaSurat.setSeq(pem.getSeq());
            pemeriksaSuratService.create(pemeriksaSurat);
        }
        /**
         * END SAVE PEMERIKSA
         */

        /**
         * SAVE PENANDATANGAN
         */
        for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat()) {
            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
            penandatanganSurat.setSurat(obj);
            penandatanganSurat.setSeq(ttd.getSeq());

            if (ttd.getIdPenandatangan() != null) {
                MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
                penandatanganSurat.setOrganization(penandatangan);
            }

            if (ttd.getIdANOrg() != null) {
                MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdANOrg());
                penandatanganSurat.setOrgAN(penandatangan);
            }

            if (ttd.getIdVendor() != null) {
                MasterVendor masterVendor = masterVendorService.find(ttd.getIdVendor());
                penandatanganSurat.setVendor(masterVendor);
            }

            penandatanganSuratService.create(penandatanganSurat);
        }
        /**
         * END SAVE PENANDATANGAN
         */

        /**
         * SAVE TEMBUSAN
         */
        for (TembusanSuratDto tembus : Optional.ofNullable(dao.getTembusanSurat()).orElse(new ArrayList<>())) {
            TembusanSurat tembusanSurat = new TembusanSurat();
            tembusanSurat.setSurat(obj);
            tembusanSurat.setSeq(tembus.getSeq());

            if (tembus.getIdOrganization() != null) {
                MasterStrukturOrganisasi tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());
                tembusanSurat.setOrganization(tembusan);
            }

            if (tembus.getIdPara() != null) {
                Para para = paraService.find(tembus.getIdPara());
                tembusanSurat.setPara(para);
            }

            if (tembus.getIdVendor() != null) {
                MasterVendor masterVendor = masterVendorService.find(tembus.getIdVendor());
                tembusanSurat.setVendor(masterVendor);
            }

            tembusanSuratService.create(tembusanSurat);
        }
        /**
         * END SAVE TEMBUSAN
         */

        /**
         * SAVE PENERIMA
         */
        for (PenerimaSuratDto penerimaSuratDto : dao.getPenerimaSurat()) {
            PenerimaSurat penerimaSurat = new PenerimaSurat();
            penerimaSurat.setSurat(obj);
            penerimaSurat.setSeq(penerimaSuratDto.getSeq());

            if (penerimaSuratDto.getIdOrganization() != null) {
                MasterStrukturOrganisasi penerima = masterStrukturOrganisasiService.find(penerimaSuratDto.getIdOrganization());
                penerimaSurat.setOrganization(penerima);
//                if(penerima.getUser()!=null) {
//                    penerimaSurat.setUser(penerima.getUser());
//                }
            }

            if (penerimaSuratDto.getIdPara() != null) {
                Para para = paraService.find(penerimaSuratDto.getIdPara());
                penerimaSurat.setPara(para);
            }

            if (penerimaSuratDto.getIdVendor() != null) {
                MasterVendor masterVendor = masterVendorService.find(penerimaSuratDto.getIdVendor());
                penerimaSurat.setVendor(masterVendor);
            }

            penerimaSuratService.create(penerimaSurat);
        }
        /**
         * END SAVE PENERIMA
         */
    }

    private void editAddresses(Surat obj, SuratDto dao) {
        /**
         * SAVE PEMERIKSA
         */
        List<PemeriksaSurat> pemeriksaSuratList = obj.getPemeriksaSurat();
        List<PemeriksaSuratDto> newPemeriksaSuratList = Optional.ofNullable(dao.getPemeriksaSurat()).orElse(new ArrayList<>());

        //deleted pemeriksa
        for (PemeriksaSurat ps : pemeriksaSuratList
                .stream()
                .filter(t -> newPemeriksaSuratList.stream().noneMatch(d -> t.getOrganization().getIdOrganization().equals(d.getIdPemeriksa())))
                .collect(Collectors.toList())) {
            pemeriksaSuratService.remove(ps);
            pemeriksaSuratList.remove(ps);
        }

        //new pemeriksa
        for (PemeriksaSuratDto pem : newPemeriksaSuratList
                .stream()
                .filter(t -> pemeriksaSuratList.stream().noneMatch(o -> o.getOrganization().getIdOrganization().equals(t.getIdPemeriksa())))
                .collect(Collectors.toList())) {
            MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());

            PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
            pemeriksaSurat.setSurat(obj);
            pemeriksaSurat.setOrganization(pemeriksa);
            pemeriksaSurat.setSeq(pem.getSeq());
            pemeriksaSuratService.create(pemeriksaSurat);
        }
        /**
         * END SAVE PEMERIKSA
         */

        /**
         * SAVE PENANDATANGAN
         */
        List<PenandatanganSurat> penandatanganSuratList = obj.getPenandatanganSurat().stream().filter(t -> t.getOrganization() != null).collect(Collectors.toList());
        List<PenandatanganSuratDto> newPenandatanganSurat = dao.getPenandatanganSurat().stream().filter(t -> t.getIdPenandatangan() != null).collect(Collectors.toList());;

        //deleted penandatangan
        for (PenandatanganSurat ps : penandatanganSuratList
                .stream()
                .filter(t -> newPenandatanganSurat.stream().noneMatch(d -> d.getIdPenandatangan().equals(t.getOrganization().getIdOrganization())))
                .collect(Collectors.toList())) {
            penandatanganSuratService.remove(ps);
            penandatanganSuratList.remove(ps);
        }

        //new penandatangan
        for (PenandatanganSuratDto ttd : newPenandatanganSurat
                .stream()
                .filter(t -> penandatanganSuratList.stream().noneMatch(o -> o.getOrganization().getIdOrganization().equals(t.getIdPenandatangan())))
                .collect(Collectors.toList())) {
            MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());

            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
            penandatanganSurat.setSurat(obj);
            penandatanganSurat.setOrganization(penandatangan);
            penandatanganSurat.setSeq(ttd.getSeq());

            if (ttd.getIdANOrg() != null) {
                MasterStrukturOrganisasi an = masterStrukturOrganisasiService.find(ttd.getIdANOrg());
                penandatanganSurat.setOrgAN(an);
            }

            penandatanganSuratService.create(penandatanganSurat);
        }

        //edit penandatangan
        for (PenandatanganSurat ps : penandatanganSuratList
                .stream()
                .filter(t -> newPenandatanganSurat.stream().anyMatch(d -> d.getIdPenandatangan().equals(t.getOrganization().getIdOrganization())))
                .collect(Collectors.toList())) {
            PenandatanganSuratDto penandatanganSuratDto = newPenandatanganSurat.stream().filter(d -> d.getIdPenandatangan().equals(ps.getOrganization().getIdOrganization())).findFirst().orElse(null);

            if (penandatanganSuratDto != null && !Optional.ofNullable(penandatanganSuratDto.getIdANOrg()).orElse(-1L).equals(Optional.ofNullable(ps.getOrgAN()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(-1L))) {
                if (penandatanganSuratDto.getIdANOrg() == null) {
                    ps.setOrgAN(null);
                } else {
                    MasterStrukturOrganisasi an = masterStrukturOrganisasiService.find(penandatanganSuratDto.getIdANOrg());
                    ps.setOrgAN(an);
                }

                penandatanganSuratService.edit(ps);
            }
        }

        List<PenandatanganSurat> penandatanganExternalList = obj.getPenandatanganSurat().stream().filter(t -> t.getVendor() != null).collect(Collectors.toList());
        List<PenandatanganSuratDto> newPenandatanganExternalList = dao.getPenandatanganSurat().stream().filter(t -> t.getIdVendor() != null).collect(Collectors.toList());

        //deleted penandatangan external
        for (PenandatanganSurat penandatanganSurat : penandatanganExternalList
                .stream()
                .filter(t -> newPenandatanganExternalList
                .stream()
                .noneMatch(d
                        -> Optional.ofNullable(t.getVendor()).map(o -> o.getId().equals(d.getIdVendor())).orElse(false)
                ))
                .collect(Collectors.toList())) {
            penandatanganSuratService.remove(penandatanganSurat);
            penandatanganExternalList.remove(penandatanganSurat);
        }

        //new penandatangan external
        for (PenandatanganSuratDto penandatanganSuratDto : newPenandatanganExternalList
                .stream()
                .filter(t -> penandatanganExternalList
                .stream()
                .noneMatch(d -> Optional.ofNullable(d.getVendor()).map(o -> o.getId().equals(t.getIdVendor())).orElse(false)))
                .collect(Collectors.toList())) {
            MasterVendor penandatangan = masterVendorService.find(penandatanganSuratDto.getIdVendor());

            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
            penandatanganSurat.setSurat(obj);
            penandatanganSurat.setVendor(penandatangan);
            penandatanganSurat.setSeq(penandatanganSuratDto.getSeq());

            penandatanganSuratService.create(penandatanganSurat);
        }
        /**
         * END SAVE PENANDATANGAN
         */

        /**
         * SAVE TEMBUSAN
         */
        List<TembusanSuratDto> tembusanSuratListDto = Optional.ofNullable(dao.getTembusanSurat()).orElse(new ArrayList<>());
        List<TembusanSurat> tembusanInternalList = obj.getTembusanSurat().stream().filter(t -> t.getOrganization() != null).collect(Collectors.toList());
        List<TembusanSuratDto> newTembusanInternalList = tembusanSuratListDto.stream().filter(t -> t.getIdOrganization() != null).collect(Collectors.toList());

        //deleted tembusan
        for (TembusanSurat bs : tembusanInternalList
                .stream()
                .filter(t -> newTembusanInternalList
                .stream()
                .noneMatch(d -> Optional
                .ofNullable(t.getOrganization())
                .map(o -> o.getIdOrganization().equals(d.getIdOrganization()))
                .orElse(false)))
                .collect(Collectors.toList())) {
            tembusanSuratService.remove(bs);
            tembusanInternalList.remove(bs);
        }

        //edited Tembusan
        for (TembusanSurat tembusanSurat : tembusanInternalList
                .stream()
                .filter(t -> newTembusanInternalList
                .stream()
                .anyMatch(d -> Optional.ofNullable(t.getOrganization()).map(o -> o.getIdOrganization().equals(d.getIdOrganization())).orElse(false)
                && !Optional.ofNullable(t.getPara()).map(Para::getIdPara).orElse(-1L).equals(d.getIdPara())
                ))
                .collect(Collectors.toList())) {
            TembusanSuratDto tembusanSuratDto = newTembusanInternalList.stream().filter(t -> t.getIdOrganization().equals(tembusanSurat.getOrganization().getIdOrganization())).findFirst().orElse(null);

            if (tembusanSuratDto.getIdPara() == null && tembusanSurat.getPara() != null) {
                tembusanSurat.setPara(null);
                tembusanSuratService.edit(tembusanSurat);
            } else if (tembusanSuratDto.getIdPara() != null && tembusanSurat.getPara() != null && !tembusanSurat.getPara().getIdPara().equals(tembusanSuratDto.getIdPara())) {
                Para para = paraService.find(tembusanSuratDto.getIdPara());
                tembusanSurat.setPara(para);
                tembusanSuratService.edit(tembusanSurat);
            }
        }

        //new tembusan
        for (TembusanSuratDto tembus : newTembusanInternalList
                .stream()
                .filter(t -> tembusanInternalList.stream()
                .noneMatch(d -> Optional
                .ofNullable(d.getOrganization())
                .map(o -> o.getIdOrganization().equals(t.getIdOrganization()))
                .orElse(false)))
                .collect(Collectors.toList())) {
            MasterStrukturOrganisasi tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());

            TembusanSurat tembusanSurat = new TembusanSurat();
            tembusanSurat.setSurat(obj);
            tembusanSurat.setOrganization(tembusan);
            if(tembusan.getUser()!=null) {
                tembusanSurat.setUser(tembusan.getUser());
            }
            tembusanSurat.setSeq(tembus.getSeq());

            if (tembus.getIdPara() != null) {
                Para para = paraService.find(tembus.getIdPara());
                tembusanSurat.setPara(para);
            }

            tembusanSuratService.create(tembusanSurat);
        }

        List<TembusanSurat> tembusanExternalList = obj.getTembusanSurat().stream().filter(t -> t.getVendor() != null).collect(Collectors.toList());
        List<TembusanSuratDto> newTembusanExternalList = tembusanSuratListDto.stream().filter(t -> t.getIdVendor() != null).collect(Collectors.toList());

        //deleted tembusan external
        for (TembusanSurat bs : tembusanExternalList
                .stream()
                .filter(t -> newTembusanExternalList
                .stream()
                .noneMatch(d
                        -> Optional.ofNullable(t.getVendor()).map(o -> o.getId().equals(d.getIdVendor())).orElse(false)
                ))
                .collect(Collectors.toList())) {
            tembusanSuratService.remove(bs);
            tembusanExternalList.remove(bs);
        }

        //new tembusan external
        for (TembusanSuratDto tembus : newTembusanExternalList
                .stream()
                .filter(t -> tembusanExternalList
                .stream()
                .noneMatch(d -> Optional.ofNullable(d.getVendor()).map(o -> o.getId().equals(t.getIdVendor())).orElse(false)))
                .collect(Collectors.toList())) {
            MasterVendor penerima = masterVendorService.find(tembus.getIdVendor());

            TembusanSurat tembusanSurat = new TembusanSurat();
            tembusanSurat.setSurat(obj);
            tembusanSurat.setVendor(penerima);
            tembusanSurat.setSeq(tembus.getSeq());

            tembusanSuratService.create(tembusanSurat);
        }
        /**
         * END SAVE TEMBUSAN
         */

        /**
         * SAVE PENERIMA
         */
        List<PenerimaSurat> penerimaInternalList = obj.getPenerimaSurat().stream().filter(t -> t.getOrganization() != null).collect(Collectors.toList());
        List<PenerimaSuratDto> newPenerimaInternalList = dao.getPenerimaSurat().stream().filter(t -> t.getIdOrganization() != null).collect(Collectors.toList());

        //deleted penerima internal
        for (PenerimaSurat bs : penerimaInternalList
                .stream()
                .filter(t -> newPenerimaInternalList
                .stream()
                .noneMatch(d
                        -> Optional.ofNullable(t.getOrganization()).map(o -> o.getIdOrganization().equals(d.getIdOrganization())).orElse(false)
                ))
                .collect(Collectors.toList())) {
            penerimaSuratService.remove(bs);
            penerimaInternalList.remove(bs);
        }

        //edited penerima internal
        for (PenerimaSurat penerimaSurat : penerimaInternalList
                .stream()
                .filter(t -> newPenerimaInternalList
                .stream()
                .anyMatch(d -> Optional.ofNullable(t.getOrganization()).map(o -> o.getIdOrganization().equals(d.getIdOrganization())).orElse(false)
                && !Optional.ofNullable(t.getPara()).map(Para::getIdPara).orElse(-1L).equals(d.getIdPara())
                )
                )
                .collect(Collectors.toList())) {
            PenerimaSuratDto penerimaSuratDto = newPenerimaInternalList.stream().filter(t -> t.getIdOrganization().equals(penerimaSurat.getOrganization().getIdOrganization())).findFirst().orElse(null);

            if (penerimaSuratDto.getIdPara() == null && penerimaSurat.getPara() != null) {
                penerimaSurat.setPara(null);
                penerimaSuratService.edit(penerimaSurat);
            } else if (penerimaSuratDto.getIdPara() != null && penerimaSurat.getPara() != null && !penerimaSurat.getPara().getIdPara().equals(penerimaSuratDto.getIdPara())) {
                Para para = paraService.find(penerimaSuratDto.getIdPara());
                penerimaSurat.setPara(para);
                penerimaSuratService.edit(penerimaSurat);
            }
        }

        //new penerima internal
        for (PenerimaSuratDto tembus : newPenerimaInternalList
                .stream()
                .filter(t -> penerimaInternalList
                .stream()
                .noneMatch(d -> Optional.ofNullable(d.getOrganization()).map(o -> o.getIdOrganization().equals(t.getIdOrganization())).orElse(false)))
                .collect(Collectors.toList())) {
            MasterStrukturOrganisasi penerima = masterStrukturOrganisasiService.find(tembus.getIdOrganization());

            PenerimaSurat penerimaSurat = new PenerimaSurat();
            penerimaSurat.setSurat(obj);
            penerimaSurat.setOrganization(penerima);
            penerimaSurat.setSeq(tembus.getSeq());

            if (tembus.getIdPara() != null) {
                Para para = paraService.find(tembus.getIdPara());
                penerimaSurat.setPara(para);
            }

            penerimaSuratService.create(penerimaSurat);
        }

        List<PenerimaSurat> penerimaExternalList = obj.getPenerimaSurat().stream().filter(t -> t.getVendor() != null).collect(Collectors.toList());
        List<PenerimaSuratDto> newPenerimaExternalList = dao.getPenerimaSurat().stream().filter(t -> t.getIdVendor() != null).collect(Collectors.toList());

        //deleted penerima external
        for (PenerimaSurat bs : penerimaExternalList
                .stream()
                .filter(t -> newPenerimaExternalList
                .stream()
                .noneMatch(d
                        -> Optional.ofNullable(t.getVendor()).map(o -> o.getId().equals(d.getIdVendor())).orElse(false)
                ))
                .collect(Collectors.toList())) {
            penerimaSuratService.remove(bs);
            penerimaExternalList.remove(bs);
        }

        //new penerima external
        for (PenerimaSuratDto tembus : newPenerimaExternalList
                .stream()
                .filter(t -> penerimaExternalList
                .stream()
                .noneMatch(d -> Optional.ofNullable(d.getVendor()).map(o -> o.getId().equals(t.getIdVendor())).orElse(false)))
                .collect(Collectors.toList())) {
            MasterVendor penerima = masterVendorService.find(tembus.getIdVendor());

            PenerimaSurat penerimaSurat = new PenerimaSurat();
            penerimaSurat.setSurat(obj);
            penerimaSurat.setVendor(penerima);
            penerimaSurat.setSeq(tembus.getSeq());

            penerimaSuratService.create(penerimaSurat);
        }
        /**
         * END SAVE PENERIMA
         */
    }

    public Surat arsipkan(Long idSurat, Long idBerkas) {
        Surat surat = find(idSurat);
        List<GlobalAttachment> suratAttachments = globalAttachmentService.getAllDataByRef("t_surat", idSurat).toList();

        // Tambahan Create Arsip on Berkas
        Arsip arsip = new Arsip();
        arsip.setNomor(surat.getNoDokumen());
        arsip.setStatus("aktif");
        arsip.setIsAsset(false);
        arsip.setIsVital(false);
        arsip.setJumlah(suratAttachments.size());
        arsip.setIsDeleted(false);
        arsip.setTglNaskah(surat.getApprovedDate());
        arsip.setJenisDokumen(surat.getFormDefinition().getJenisDokumen());
        arsip.setCompanyCode(surat.getCompanyCode());

        arsip.setSatuan(null); // Sumber satuan to be determined

        Berkas berkas = berkasService.find(idBerkas);
        arsip.setBerkas(berkas);

        Boolean isExistOrganisasi = surat.getPenandatanganSurat().stream().anyMatch(q -> q.getOrganization() != null);
        if (isExistOrganisasi) {
            MasterStrukturOrganisasi organisasi = surat.getPenandatanganSurat().stream().map(q -> q.getOrganization()).filter(q -> q != null).findFirst().orElse(null);
            if (organisasi != null) {
                arsip.setOrganisasi(organisasi);
            }
        }

        if (surat.getFormDefinition().getJenisDokumen().getIsKorespondensi()) {
            Boolean isUserExist = surat.getPenandatanganSurat().stream().anyMatch(q -> q.getUser() != null);
            if (isUserExist) {
                MasterUser user = surat.getPenandatanganSurat().stream().map(q -> q.getUser()).filter(q -> q != null).findFirst().orElse(null);
                if (user != null) {
                    arsip.setUnit(user.getKorsa());
                }
            }
        } else {
            FormValue fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("unit"))
                    .findFirst().orElse(null);
            if (fv != null) {
                MasterKorsa unit = masterKorsaService.find(fv.getValue());
                if (unit != null) {
                    arsip.setUnit(unit);
                }
            }
        }

        MasterBahasa bahasa = masterBahasaService.findByNama(null); // Sumber bahasa to be determined
        if (bahasa != null) {
            arsip.setBahasa(bahasa);
        }

        FormValue fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("klasifikasi_keamanan"))
                .findFirst().orElse(null);
        if (fv != null) {
            MasterKlasifikasiKeamanan keamanan = masterKlasifikasiKeamananService.findByNamaKlasifikasiKeamanan(fv.getValue());
            if (keamanan != null) {
                arsip.setKlasifikasiKeamanan(keamanan);
            }
        }

        String[] arr = new String[]{"klasifikasi_masalah", "klasifikasi_dokumen"};
        fv = surat.getFormValue().stream().filter(b -> Arrays.asList(arr).contains(b.getFormField().getMetadata().getNama().toLowerCase())).findFirst().orElse(null);
        if (fv != null) {
            String kode = fv.getValue().split("-", 2)[0].trim();
            MasterKlasifikasiMasalah masalah = masterKlasifikasiMasalahService.findByKode(kode);
            if (masalah != null) {
                arsip.setKlasifikasiMasalah(masalah);
            }
        }

        fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("tingkat_perkembangan"))
                .findFirst().orElse(null);
        if (fv != null) {
            MasterTingkatPerkembangan perkembangan = masterTingkatPerkembanganService.findByNama(fv.getValue());
            if (perkembangan != null) {
                arsip.setTingkatPerkembangan(perkembangan);
            }
        }

        fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("tingkat_urgensi"))
                .findFirst().orElse(null);
        if (fv != null) {
            MasterTingkatUrgensi urgensi = masterTingkatUrgensiService.findByNama(fv.getValue());
            if (urgensi != null) {
                arsip.setTingkatUrgensi(urgensi);
            }
        }

        fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("kategori_arsip"))
                .findFirst().orElse(null);
        if (fv != null) {
            MasterKategoriArsip kategori = masterKategoriArsipService.findByNama(fv.getValue());
            if (kategori != null) {
                arsip.setKategoriArsip(kategori);
            }
        }

        fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("tingkat_akses"))
                .findFirst().orElse(null);
        if (fv != null) {
            MasterTingkatAksesPublik akses = masterTingkatAksesPublikService.findByNama(fv.getValue());
            if (akses != null) {
                arsip.setTingkatAkses(akses);
            }
        }

        fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("media_arsip"))
                .findFirst().orElse(null);
        if (fv != null) {
            MasterMediaArsip media = masterMediaArsipService.findByNama(fv.getValue());
            if (media != null) {
                arsip.setMediaArsip(media);
            }
        }

        fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("pencipta_arsip"))
                .findFirst().orElse(null);
        if (fv != null) {
            MasterPenciptaArsip pencipta = masterPenciptaArsipService.findByNama(fv.getValue());
            if (pencipta != null) {
                arsip.setPenciptaArsip(pencipta);
            }
        }

        String perihal = "", deskripsi = "";
        fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                .findFirst().orElse(null);
        if (fv != null) {
            perihal = fv.getValue();
            deskripsi = fv.getValue();
        }
        arsip.setPerihal(perihal);
        arsip.setDeskripsi(deskripsi);

//        Arsip arsip = arsipService.find(idArsip);
        arsipService.create(arsip);
        getEntityManager().flush();

        surat.setFolderArsip(arsip.getDeskripsi());
        surat.setIdArsip(arsip.getId());
        edit(surat);

//        List<GlobalAttachment> suratAttachments = globalAttachmentService.getAllDataByRef("t_surat", idSurat).toList();
        for (GlobalAttachment attachment : suratAttachments) {
            GlobalAttachment arsipAttachment = new GlobalAttachment();

            arsipAttachment.setVersion(attachment.getVersion());
            arsipAttachment.setModifiedDate(attachment.getModifiedDate());
            arsipAttachment.setModifiedBy(attachment.getModifiedBy());
            arsipAttachment.setCreatedDate(attachment.getCreatedDate());
            arsipAttachment.setCreatedBy(attachment.getCreatedBy());
            arsipAttachment.setSize(attachment.getSize());
            arsipAttachment.setNumber(attachment.getNumber());
            arsipAttachment.setMimeType(attachment.getMimeType());
            arsipAttachment.setCompanyCode(attachment.getCompanyCode());
            arsipAttachment.setDocId(attachment.getDocId());
            arsipAttachment.setIsPublished(attachment.getIsPublished());
            arsipAttachment.setIsKonsep(attachment.getIsKonsep());
            arsipAttachment.setDocGeneratedName(attachment.getDocGeneratedName());
            arsipAttachment.setDocName(attachment.getDocName());
            arsipAttachment.setIsDeleted(attachment.getIsDeleted());
            arsipAttachment.setExtractedText(attachment.getExtractedText());
            arsipAttachment.setReferenceTable("t_arsip");
//            arsipAttachment.setReferenceId(idArsip);
            arsipAttachment.setReferenceId(arsip.getId());
            arsipAttachment.setIdDocument(attachment.getIdDocument());

            globalAttachmentService.create(arsipAttachment);
        }

        //Set Penerima/Tembusan user
        MasterUser extUser = user.getUserSession().getUser();
        PenerimaSurat ps = surat.getPenerimaSurat().stream().filter(a->a.getOrganization().getIdOrganization().equals(extUser.getOrganizationEntity().getIdOrganization())).findFirst().orElse(null);
        if(ps!=null){
            if(ps.getUser()==null){
                ps.setUser(extUser);
                penerimaSuratService.edit(ps);
            }
        }

        TembusanSurat ts = surat.getTembusanSurat().stream().filter(a->a.getOrganization().getIdOrganization().equals(extUser.getOrganizationEntity().getIdOrganization())).findFirst().orElse(null);
        if(ts!=null){
            if(ts.getUser()==null){
                ts.setUser(extUser);
                tembusanSuratService.edit(ts);
            }
        }

        return surat;
    }

    public List<ReferensiSuratDetail> addRefferences(long idSurat, long idOrganization, List<ReferensiSuratDto> dtos) {
        Surat surat = find(idSurat);
        List<ReferensiSuratDto> newInsert = new ArrayList<>();
        List<ReferensiSurat> all = referensiSuratService.getByOrgSurat(idOrganization,idSurat).collect(Collectors.toList());
        if(!all.isEmpty()) {
            newInsert = dtos.stream().filter(t -> all.stream().anyMatch(v ->
                    v.getSuratDetail().stream().noneMatch(w->w.getReferensiSurat().getId().equals(t.getId())))).collect(Collectors.toList());
        }else{
            newInsert = dtos;
        }

        List<ReferensiSuratDetail> result = new ArrayList<>();

        for (ReferensiSuratDto dto : newInsert) {
            ReferensiSurat referensiSurat = referensiSuratService.find(dto.getId());
            ReferensiSuratDetail referensiSuratDetail = new ReferensiSuratDetail();
            referensiSuratDetail.setSurat(surat);
            referensiSuratDetail.setReferensiSurat(referensiSurat);
            referensiSuratDetailService.create(referensiSuratDetail);
            result.add(referensiSuratDetail);
        }

        return result;
    }

    public void removeRefference(Long refDetailId,Long idSurat) {
        ReferensiSuratDetail rsd = referensiSuratDetailService.getByIdSurat(refDetailId,idSurat);

        if (rsd != null) {
//            rsd.getReferensiSurat().setSuratDetail(null);
            referensiSuratDetailService.remove(rsd);

            //remove duplicate
//            referensiSuratService
//                    .getByOrg(rsd.getReferensiSurat().getOrganisasi().getIdOrganization(),idSurat)
//                    .toList()
//                    .stream()
//                    .collect(Collectors.groupingBy(t -> t.getSurat().getId(), Collectors.toList()))
//                    .entrySet()
//                    .stream()
//                    .filter(t -> t.getValue().size() > 0)
//                    .forEach(t -> t.getValue().stream().skip(1).forEach(v -> referensiSuratService.remove(v)));
            ;
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    public void onTaskActionGenerated(ProcessTask processTask, TaskActionResponseDto response) {
        if (processTask.getProcessInstance().getRelatedEntity().equals(Surat.class.getName())) {
            Surat surat = find(Long.parseLong(processTask.getProcessInstance().getRecordRefId()));

            if (processTask.getTaskType().equals("Approval")) {
                boolean isPemeriksa = processTask.getTaskName().toLowerCase().contains("pemeriksa");
                boolean isPenandatangan = processTask.getTaskName().toLowerCase().contains("penandatangan");

                if (isPenandatangan) {
                    // Hide "kembalikan ke pemeriksa" jika tidak ada pemeriksa
                    boolean isNoPemeriksa = surat.getPemeriksaSurat() == null || surat.getPemeriksaSurat().size() == 0;
                    if (isNoPemeriksa) {
                        response.getForms().stream().filter(t -> t.getName().equals(processTask.getResponseVar())).forEach(t -> {
                            try {
                                OptionsDto options = getObjectMapper().readValue(t.getOtherData(), new TypeReference<OptionsDto>() {});
                                options.setOptions(options.getOptions().stream().filter(v -> !v.toLowerCase().contains("pemeriksa")).collect(Collectors.toList()));
                                t.setOtherData(getObjectMapper().writeValueAsString(options));
                            } catch (IOException e) {
                                logger.error(null, e);
                            }
                        });
                    }
                }

                if (isPemeriksa) {
                    boolean isSerial = !Optional.ofNullable(surat.getIsPemeriksaParalel()).orElse(false);

                    if (isSerial) {
                        boolean isFirstPemeriksa = Optional.ofNullable(processTask.getSeq()).orElse(0) == 0;

                        if (isFirstPemeriksa) {
                            // Hide "kembalikan" jika pemeriksa pertama
                            response.getForms().stream().filter(t -> t.getName().equals(processTask.getResponseVar())).forEach(t -> {
                                try {
                                    OptionsDto options = getObjectMapper().readValue(t.getOtherData(), new TypeReference<OptionsDto>() {});
                                    options.setOptions(options.getOptions().stream().filter(v -> !v.toLowerCase().equals("kembalikan")).collect(Collectors.toList()));
                                    t.setOtherData(getObjectMapper().writeValueAsString(options));
                                } catch (IOException e) {
                                    logger.error(null, e);
                                }
                            });
                        }
                    }
                }
            } else if (processTask.getTaskType().equals("Disposisi")) {
                SuratDisposisi relatedDisposisi = suratDisposisiService.findPendingByAssignee(surat.getId(), processTask.getAssignee().getOrganizationCode());

                try {
                    response.setContext(getObjectMapper().writeValueAsString(new SuratDisposisiDto(relatedDisposisi)));
                } catch (JsonProcessingException e) {
                    logger.error(null, e);
                }

                if (response.getIsSecretary()) {
                    FormFieldDto fileUpload = new FormFieldDto();
                    fileUpload.setName("bukti-disposisi");
                    fileUpload.setLabel("Bukti Disposisi");
                    fileUpload.setType("file-upload");
                    fileUpload.setPostToCamunda(false);
                    fileUpload.setRequiredEl("true");
                    fileUpload.setRequired(true);

                    response.getForms().add(fileUpload);
                }

                boolean excepts = surat.getFormDefinition().getJenisDokumen().getNamaJenisDokumen().toLowerCase().contains("notulen");

                if (surat.getUndangans().size() > 0 && !excepts) {
                    try {
                        FormFieldDto hadirField = new FormFieldDto();
                        hadirField.setName("kehadiran");
                        hadirField.setLabel("Kehadiran");
                        hadirField.setType("select");
                        hadirField.setOtherData(getObjectMapper().writeValueAsString(new OptionsDto(Arrays.asList("Hadir", "Tidak Hadir"))));
                        hadirField.setRequired(true);

                        response.getForms().add(hadirField);
                    } catch (Exception e) {

                    }
                }
            }
        }
    }

    public JinqStream<Surat> getFavoritByJabatan(Long idJabatan,Long idUser) {
        if(idUser!=null) {
            return db(SuratInteraction.class)
                    .where(t -> (t.getOrganization().getIdOrganization().equals(idJabatan) || (t.getUser() != null && t.getUser().getId().equals(idUser))) && t.getIsStarred())
                    .select(SuratInteraction::getSurat);
        }else{
            return db(SuratInteraction.class)
                    .where(t -> t.getOrganization().getIdOrganization().equals(idJabatan) && t.getIsStarred())
                    .select(SuratInteraction::getSurat);
        }
    }

    public JinqStream<Surat> getReferensi(Long idJabatan) {
        return db(ReferensiSurat.class)
                .where(t -> t.getOrganisasi().getIdOrganization().equals(idJabatan))
                .select(ReferensiSurat::getSurat);
    }

    public void batalSurat(Surat obj) {
        boolean isApproving = obj.getStatus().equals("SUBMITTED");

        if (isApproving) {
            MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName(applicationContext.getApplicationConfig().getWorkflowProvider());
            IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();
            ProcessInstance processInstance = processInstanceService.getByRelatedEntity(obj.getClass().getName(), obj.getId().toString());
            List<ProcessTask> activeTasks = workflowProvider.getActiveTasks(processInstance);
            boolean isTaskValid = activeTasks.size() > 0;
            MasterStrukturOrganisasi aktor = user.getUserSession().getUser().getOrganizationEntity();
            IWorkflowService wfService = obj.getWorkflowService();
            ProcessTask process = activeTasks.stream().filter(a->a.getAssignee().getIdOrganization().equals(aktor.getIdOrganization())).findFirst().orElse(null);
            IWorkflowEntity entity = wfService.getEntity(process);

            if (isTaskValid) {
                workflowProvider.terminateProcess(processInstance);
            }

            if(aktor != null){
                List<String> listCC = new ArrayList<>();
                PemeriksaSurat pemeriksa = pemeriksaSuratService.getByIdSuratOrg(obj.getId(),aktor.getIdOrganization());
                if(pemeriksa != null){
                    obj.setStatus("CANCEL");
                    obj.setStatusNavigasi(Constants.STATUS_NAVIGASI_BATAL_OLEH_PEMERIKSA);
                    edit(obj);
                }

                PenandatanganSurat penandatangan = penandatanganSuratService.getByOrgSurat(aktor.getIdOrganization(),obj.getId());
                if(penandatangan != null){
                    obj.setStatus("CANCEL");
                    obj.setStatusNavigasi(Constants.STATUS_NAVIGASI_BATAL_OLEH_PENANDATANGAN);
                    edit(obj);

                    List<PemeriksaSurat> listPemeriksa = pemeriksaSuratService.getAllBy(obj.getId());
                    if(!listPemeriksa.isEmpty()) {
                        for (PemeriksaSurat ps : listPemeriksa) {
                            if(ps.getUser() != null && !Strings.isNullOrEmpty(ps.getUser().getEmail())){
                                listCC.add(ps.getUser().getEmail());
                            }else{
                                if(ps.getOrganization() != null && ps.getOrganization().getUser() != null){
                                    MasterUser us = ps.getOrganization().getUser();
                                    if(!Strings.isNullOrEmpty(us.getEmail())){
                                        listCC.add(us.getEmail());
                                    }
                                }
                            }
                        }
                    }
                }

                batalNotification(aktor.getUser(),entity,listCC,obj.getUserKonseptor());
            }else {
                obj.setStatus("CANCEL");
                obj.setStatusNavigasi(Constants.STATUS_NAVIGASI_SIMPAN_OLEH_KONSEPTOR);
                edit(obj);
            }
        }else {
            obj.setStatus("CANCEL");
            obj.setStatusNavigasi(Constants.STATUS_NAVIGASI_SIMPAN_OLEH_KONSEPTOR);
            edit(obj);
        }
    }

    public HashMap<String, Object> getFavoritSurat(Long idJabatan, Long idUser, String filter, int skip, int limit, Boolean descending, String sort) {
        List<SuratDto> result = new ArrayList<>();
        TypedQuery<SuratInteraction> all = getData(idJabatan, idUser, filter, descending, sort);
        Long length = all.getResultList().stream().map(a->a.getSurat()).distinct().count();
        result = MappingToDto(all, skip, limit);
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    public TypedQuery<SuratInteraction> getData(Long idJabatan, Long idUser, String filter, Boolean descending, String sort) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<SuratInteraction> q = cb.createQuery(SuratInteraction.class);
        Root<SuratInteraction> root = q.from(SuratInteraction.class);

        q.select(root);
        List<Predicate> predicates = new ArrayList<>();

        if(idUser!=null) {
            Predicate main = cb.and(
                    cb.or(
                        cb.equal(root.get("organization").get("idOrganization"), idJabatan),
                        cb.and(cb.isNotNull(root.get("user")), cb.equal(root.get("user").get("id"), idUser))
                    ),
                    cb.isTrue(root.get("isStarred"))
            );
            predicates.add(main);
        }else{
            Predicate main = cb.and(
                    cb.equal(root.get("organization").get("idOrganization"), idJabatan),
                    cb.isTrue(root.get("isStarred"))
                    );
            predicates.add(main);
        }

        if(StringUtils.isNotEmpty(filter)){
            Predicate where = cb.like(root.join("surat", JoinType.LEFT).join("formValue",JoinType.LEFT).get("value"),"%" + filter + "%");
            predicates.add(where);
        }

        q.where(predicates.toArray(new Predicate[predicates.size()]));

        if (descending != null && !Strings.isNullOrEmpty(sort)) {
            if (descending) {
                q.orderBy(cb.desc(root.join("surat",JoinType.LEFT).get(sort)));
            } else {
                q.orderBy(cb.asc(root.join("surat",JoinType.LEFT).get(sort)));
            }
        }

        return getEntityManager().createQuery(q);
    }

    private List<SuratDto> MappingToDto(TypedQuery<SuratInteraction> all, int skip, int limit) {
        List<SuratDto> result = null;

        if (all != null) {
            result = all.getResultStream().distinct().skip(skip).limit(limit).map(a->new SuratDto(a.getSurat())).collect(Collectors.toList());
        }
        return result;
    }

    /**
     * Handle kasus TL-017 : RIWAYAT DISPOSISI MENU MASUK
     * @param processTask
     */
    private void removeDuplicateDisposisi(ProcessTask processTask, IWorkflowProvider workflowProvider) {
        List<ProcessTask> processTasks = processTaskService.findDuplicateDisposisi(processTask).toList();

        processTasks.forEach(terminatedTask -> {
            try {
                workflowProvider.terminateProcess(terminatedTask.getProcessInstance());
            } catch (Exception e) {
                logger.error(null, ExceptionUtil.getRealCause(e));
            }
        });
    }

    private String getFormValueBy(String fieldName, FormDefinition formDefinition, List<FormValue> formValues) {
        FormField field = formDefinition.getFormFields().stream()
                .filter(q -> q.getMetadata().getNama().equalsIgnoreCase(fieldName.toLowerCase()))
                .findFirst().orElse(null);
        if (field == null) return "";

        String value = formValues.stream()
                .filter(q -> q.getFormField().getId().equals(field.getId()))
                .map(FormValue::getValue)
                .findFirst().orElse("");

        return value;
    }

    public JinqStream<Surat> findByJenisDokumenId(Long idJenisDokumen) {
        return db().where(t -> t.getFormDefinition().getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen));
    }

    private Optional<Object> getValue(Map<String, Object> map, String key) {
        return map.entrySet().stream().filter(t -> t.getKey().equals(key)).findFirst().map(Map.Entry::getValue);
    }

    /**
     * START Mail Handler
     */
    @Override
    public void setMailProperties(IWorkflowEntity entity) {
        Surat surat = (Surat) entity;
        FormDefinition formDefinition = surat.getFormDefinition();
        List<FormValue> formValues = surat.getFormValue();
        String jenisDokumen = formDefinition.getJenisDokumen().getNamaJenisDokumen();
        String perihal = getFormValueBy("perihal", formDefinition, formValues);
        String klasifikasiKeamanan = getFormValueBy("klasifikasi_keamanan",formDefinition,formValues);

        if (surat.getSubmittedDate() != null) {
            Locale locale = new Locale("in", "ID");
            String formatted = new SimpleDateFormat("dd MMMMMMMMMM yyyy", locale).format(surat.getSubmittedDate());
            entity.setFormattedTanggal(formatted);
        }

        List<PenandatanganSurat> penandatanganInternal = surat.getPenandatanganSurat().stream().filter(t -> t.getOrganization() != null).collect(Collectors.toList());
        PenandatanganSurat penandatanganSurat = penandatanganInternal.stream().filter(t -> t.getOrganization() != null).reduce((first, second) -> second).orElse(null); //get last data?

        if (penandatanganSurat != null) {
            if (penandatanganSurat.getOrgAN() != null) {
                MasterUser penandatangan = penandatanganSurat.getUser()!=null ? penandatanganSurat.getUser() :
                        penandatanganSurat.getOrganization().getUser() != null ? penandatanganSurat.getOrganization().getUser() : null;
                entity.setFormattedSender(String.format("%s %s | %s atas nama %s", penandatangan.getNameFront(),penandatangan.getNameMiddleLast()
                        , penandatangan.getKedudukan(), penandatanganSurat.getOrgAN().getOrganizationName()));
            }else{
                PenandatanganPelakharPymtSurat pymt = penandatanganPelakharPymtSuratService.getBySurat(surat.getId());
                if(pymt != null){
                    MasterUser del = pymt.getUser()!= null ?
                            pymt.getUser() :
                                pymt.getOrganization().getUser() != null ? pymt.getOrganization().getUser() : null ;
                    if(del != null) {
                        entity.setFormattedSender(String.format("PYMT %s | %s %s | %s", penandatanganSurat.getOrganization().getOrganizationName(),
                                pymt.getUser().getNameFront(), pymt.getUser().getNameMiddleLast(), pymt.getUser().getEmployeeId()));
                    }
                }else {
                    MasterUser penandatangan = penandatanganSurat.getUser() != null ? penandatanganSurat.getUser() :
                            penandatanganSurat.getOrganization().getUser() != null ? penandatanganSurat.getOrganization().getUser() : null;
                    if (penandatangan != null) {
                        entity.setFormattedSender(String.format("%s %s | %s", penandatangan.getNameFront(),
                                penandatangan.getNameMiddleLast(), penandatangan.getKedudukan()));
                    }
                }
            }
        }

        entity.setJenisDokumen(jenisDokumen);
        entity.setPerihal(perihal);
        entity.setKlasifikasiKeamanan(klasifikasiKeamanan);
        entity.setNomor(entity.getNoDokumen());
    }

    public void penerimaExternalNotification(List<MasterVendor> recipients, IWorkflowEntity entity, String docId) {
        MasterUser initiator = masterUserService.findUserByUsername(entity.getCreatedBy());

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        recipients.stream().filter(t -> StringUtils.isNotEmpty(t.getEmail())).forEach(masterVendor -> {
            DownloadDto download = new DownloadDto();

            if(!Strings.isNullOrEmpty(docId)) {
                SharedDocLib shared = sharedDocLibService.generate(docId, masterVendor);
                String downloadUrl = applicationContext.getApplicationConfig().getFrontEndUrl() + "/shared-file/" + shared.getToken();
                download.setLink(downloadUrl);
                Locale locale = new Locale("in", "ID");
                String formatted = new SimpleDateFormat("dd MMMMMMMMMM yyyy", locale).format(shared.getExpiredDate());
                download.setActiveDate(formatted);
                download.setUsername(shared.getUsername());
                download.setPassword(shared.getPassword());
            }

            notificationService.sendEmail(Constants.TEMPLATE_SEND_TO_EXTERNAL_INFO, vars -> {
                vars.put("initiator", initiator);
                vars.put("entity", entity);
                vars.put("download",download);
            }, docId, masterVendor.getEmail());
        });
    }

    public void toPemeriksaNotification(ProcessTask processTask, IWorkflowEntity entity, String docId, ProcessTask prevTask) {
        MasterUser user = Optional.ofNullable(processTask.getAssignee().getUser()).orElse(null);
        MasterUser initiator = Optional.ofNullable(prevTask.getAssignee().getUser()).orElse(null);

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        entity.setTaskResponse("Dikembalikan");

        notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_REJECTED, vars -> {
            vars.put("initiator", initiator);
            vars.put("assignee", user);
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
        }, docId, null, user);
    }

    public void batalNotification(MasterUser initiator, IWorkflowEntity entity, List<String> cc, MasterUser assignee) {
        setMailProperties(entity);
        entity.setTaskResponse("Dikembalikan");

        notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_REJECTED, vars -> {
            vars.put("initiator", initiator);
            vars.put("assignee", assignee);
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
        }, null, cc, assignee);
    }
    
    @Override
    public void newTaskNotification(List<ProcessTask> processTasks, IWorkflowEntity entity, String docId) {
        for (ProcessTask processTask : processTasks) {
            MasterUser user = Optional.ofNullable(processTask.getAssignee().getUser()).orElse(null);
            MasterUser initiator = masterUserService.findUserByUsername(entity.getCreatedBy());

            // < SET MAIL PROPS >
            setMailProperties(entity);
            // </ SET MAIL PROPS >

            notificationService.sendEmail(Constants.TEMPLATE_NEW_TASK_ASSIGNMENT, vars -> {
                vars.put("initiator", initiator);
                vars.put("assignee", user);
                vars.put("entity", entity);
                vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
            }, docId, null, user);
        }
    }

    @Override
    public void newTaskNotification(ProcessTask processTask, IWorkflowEntity entity, String docId) {
        MasterUser user = Optional.ofNullable(processTask.getAssignee().getUser()).orElse(null);

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        if(processTask.getTaskType().equals("Disposisi")){
            entity.setTaskResponse("Didisposisi");

            Boolean isDisposisi = ((Surat)entity)
                    .getSuratDisposisi()
                    .stream()
                    .filter(a -> a.getOrganization().getIdOrganization().equals(processTask.getAssignee().getIdOrganization()))
                    .findFirst()
                    .map(t -> t.getParent() != null)
                    .orElse(false);

            if (isDisposisi) {
                SuratDisposisi dis = ((Surat)entity).getSuratDisposisi().stream()
                        .filter(a->a.getOrganization().getIdOrganization().equals(processTask.getAssignee().getIdOrganization()))
                        .map(SuratDisposisi::getParent).findFirst().orElse(null);
                if(dis!=null){
                    MasterUser initiator = dis.getOrganization().getUser();
                    notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_DISPOSISI, vars -> {
                        vars.put("initiator", initiator);
                        vars.put("assignee", user);
                        vars.put("entity", entity);
                        vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
                    }, docId, null, user);
                }
            }
        }else{
            notificationService.sendEmail(Constants.TEMPLATE_NEW_TASK_ASSIGNMENT, vars -> {
                vars.put("assignee", user);
                vars.put("entity", entity);
                vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
            }, docId, null, user);
        }
    }

    @Override
    public void infoTaskNotification(ProcessTask processTask, IWorkflowEntity entity, String docId) {
        // < INIT TASK DICTIONARY >
        Map<String, String> taskExecutorVars = new HashMap<>();
        taskExecutorVars.put("Task_PemeriksaPelakhar_Approval", "Pelakhar Pemeriksa");
        taskExecutorVars.put("Task_PemeriksaPYMT_Approval", "PYMT Pemeriksa");
        taskExecutorVars.put("Task_Pemeriksa_Approval", "Pemeriksa");

        taskExecutorVars.put("Task_PenandatanganPelakhar_Approval", "Pelakhar Penandatangan");
        taskExecutorVars.put("Task_PenandatanganPelakhar_Approval_2", "Pelakhar Penandatangan");
        taskExecutorVars.put("Task_PenandatanganPYMT_Approval", "PYMT Penandatangan");
        taskExecutorVars.put("Task_Penandatangan_Approval", "Penandatangan");

        taskExecutorVars.put("Setujui", "Disetujui");
        taskExecutorVars.put("TO PYMT", "Disetujui");
        taskExecutorVars.put("Kembalikan", "Dikembalikan");
        taskExecutorVars.put("Kembalikan ke Konseptor", "Dikembalikan");
        taskExecutorVars.put("Kembalikan ke Pemeriksa", "Dikembalikan");

        // </ INIT TASK DICTIONARY >

        String executor = taskExecutorVars.get(processTask.getTaskName());
        if (!Strings.isNullOrEmpty(executor)) {
            entity.setTaskExecutor(executor);
        } else {
            entity.setTaskExecutor("");
        }

        if (processTask.getTaskType().equals("Disposisi")) {
            SuratDisposisi sd = ((Surat)entity).getSuratDisposisi().stream().filter(a->a.getOrganization().getIdOrganization().equals(processTask.getAssignee().getIdOrganization()))
                    .findFirst().orElse(null);
            if (StringUtils.isNotEmpty(processTask.getResponse())) {
                entity.setTaskResponse("Didisposisi");
                MasterUser user = Optional.ofNullable(processTask.getAssignee().getUser()).orElse(null);
                if(sd!=null) {
                    MasterUser initiator = sd.getOrganization().getUser();

                    if (sd.getParent() != null) {
                        initiator = sd.getParent().getOrganization().getUser();
                    }

                    setMailProperties(entity);

                    MasterUser finalInitiator = initiator;
                    notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_DISPOSISI, vars -> {
                        vars.put("initiator", finalInitiator);
                        vars.put("assignee", user);
                        vars.put("entity", entity);
                        vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
                    }, docId, null, user);
                }
            } else {
                entity.setTaskResponse("Ditindaklanjuti");
                if(sd!=null){
                    if(sd.getParent()==null){
                        MasterUser user1 = Optional.ofNullable(processTask.getAssignee().getUser()).orElse(null);
                        if(user1 == null){
                            user1 = user.getUserSession().getUser();
                        }
                        List<PenandatanganSurat> penandatangan = ((Surat)entity).getPenandatanganSurat();
                        MasterUser init = penandatangan.stream().filter(t -> t.getOrganization() != null).reduce((first, second) -> second).map(t -> {
                            if (t.getUser() != null) {
                                return t.getUser();
                            } else {
                                return t.getOrganization().getUser();
                            }
                        }).orElse(null);

                        setMailProperties(entity);
                        entity.setTaskExecutor(user1.getNameFront()+" "+user1.getNameMiddleLast()+" | "+user1.getKedudukan());

                        notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_INFO, vars -> {
                            vars.put("entity", entity);
                            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
                        }, docId, null, init);
                    }else {
                        MasterUser user1 = Optional.ofNullable(processTask.getAssignee().getUser()).orElse(null);
                        if(user1==null){
                            user1 = user.getUserSession().getUser();
                        }
                        MasterUser initiator = sd.getParent().getOrganization().getUser();

                        setMailProperties(entity);
                        entity.setTaskExecutor(user1.getNameFront()+" "+user1.getNameMiddleLast()+" | "+user1.getKedudukan());

                        notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_INFO, vars -> {
                            vars.put("entity", entity);
                            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
                        }, docId, null, initiator);
                    }
                }
            }
        } else {
            String response = taskExecutorVars.get(processTask.getResponse());
            if (!Strings.isNullOrEmpty(response)) {
                entity.setTaskResponse(response);
            } else {
                entity.setTaskResponse("");
            }

            MasterUser user = Optional.ofNullable(processTask.getAssignee().getUser()).orElse(null);
            String[] initiatorParts = entity.getCreatedBy().split("\\|");
            MasterUser initiator = masterUserService.findUserByUsername(initiatorParts[0].trim());

            // < SET MAIL PROPS >
            setMailProperties(entity);
            // </ SET MAIL PROPS >

            notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_INFO, vars -> {
                vars.put("entity", entity);
                vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
            }, docId, null, initiator);
        }
    }
    /**
     * END Mail Handler
     */

    public void updateKonsep(Surat obj){
        GlobalAttachment globalAttachment = globalAttachmentService.getDraftDocDataByRef("t_surat", obj.getId()).findFirst().orElse(null);
        updateKonsep(obj, globalAttachment);
    }
}
