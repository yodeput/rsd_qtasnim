package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.ProcessDefinition;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Date;

@Stateless
@LocalBean
public class ProcessDefinitionService extends AbstractFacade<ProcessDefinition> {
    @Override
    protected Class<ProcessDefinition> getEntityClass() {
        return ProcessDefinition.class;
    }

    public JPAJinqStream<ProcessDefinition> getHistory(String name) {
        return db()
                .where(t -> t.getFormName().equals(name))
                .sortedDescendingBy(ProcessDefinition::getCreatedDate);
    }

    public JPAJinqStream<ProcessDefinition> getHistory(Long idJenisDokumen) {
        return db()
                .where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen))
                .sortedDescendingBy(ProcessDefinition::getCreatedDate);
    }

    public ProcessDefinition getByVersion(String name, Double version) {
        return db()
                .where(t -> t.getFormName().equals(name) && t.getVersion().equals(version))
                .sortedDescendingBy(ProcessDefinition::getActivationDate)
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public ProcessDefinition getByVersion(Long idJenisDokumen, Double version) {
        return db()
                .where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen) && t.getVersion().equals(version))
                .sortedDescendingBy(ProcessDefinition::getActivationDate)
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public ProcessDefinition getActivated(String name) {
        Date now = new Date();

        return db()
                .where(t -> t.getFormName().equals(name) && t.getActivationDate().before(now))
                .sortedDescendingBy(ProcessDefinition::getActivationDate)
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public ProcessDefinition getActivated(Long idJenisDokumen) {
        Date now = new Date();

        return db()
                .where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen) && t.getActivationDate().before(now))
                .sortedDescendingBy(ProcessDefinition::getActivationDate)
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public ProcessDefinition getLastest(String name) {
        return db()
                .where(t -> t.getFormName().equals(name))
                .sortedDescendingBy(ProcessDefinition::getVersion)
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public ProcessDefinition getLastest(Long idJenisDokumen) {
        return db()
                .where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen))
                .sortedDescendingBy(ProcessDefinition::getVersion)
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public JinqStream<ProcessDefinition> findByJenisDokumenId(Long idJenisDokumen) {
        return db()
                .where(t -> t.getJenisDokumen().getIdJenisDokumen().equals(idJenisDokumen));
    }
}
