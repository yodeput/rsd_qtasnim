package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.EntityStatus;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.PenandatanganSurat;
import com.qtasnim.eoffice.db.Surat;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class PenandatanganSuratService extends AbstractFacade<PenandatanganSurat>{

    @Override
    protected Class<PenandatanganSurat> getEntityClass() {
        return PenandatanganSurat.class;
    }

    @Inject
    private SuratService suratService;

    public List<PenandatanganSurat> getAllBy(Long idSurat){
        Surat obj = suratService.find(idSurat);
        return db().where(q->q.getSurat().equals(obj)).toList();
    }

    public PenandatanganSurat getByIdOrganisasi(Long idOrganisasi) {
        return db().where(t -> t.getOrganization().getIdOrganization().equals(idOrganisasi)).findFirst().orElse(null);
    }

    public PenandatanganSurat getByIdOrganisasiAN(Long idOrganisasiAN) {
        return db().where(t -> t.getOrgAN().getIdOrganization().equals(idOrganisasiAN)).findFirst().orElse(null);
    }

    public PenandatanganSurat getByOrgSurat(Long org,Long surat) {
        return db().where(t -> t.getOrganization().getIdOrganization().equals(org) && t.getSurat().getId().equals(surat)).findFirst().orElse(null);
    }

    public List<Surat> getByIdOrganization(List<MasterStrukturOrganisasi> lists){
        return db().where(a->lists.contains(a.getOrganization()) && a.getSurat().getStatus().equals("SUBMITTED")).select(b->b.getSurat()).collect(Collectors.toList());
    }
}
