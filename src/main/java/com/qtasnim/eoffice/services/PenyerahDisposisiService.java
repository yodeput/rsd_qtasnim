package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterTindakan;
import com.qtasnim.eoffice.db.PenyerahDisposisi;
import com.qtasnim.eoffice.db.PermohonanDokumen;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.PenyerahDisposisiDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class PenyerahDisposisiService extends AbstractFacade<PenyerahDisposisi>{

    @Inject
    private PermohonanDokumenService permohonanDokumenService;

    @Inject
    private MasterStrukturOrganisasiService orgService;

    @Inject
    private MasterTindakanService tindakanService;

    @Override
    protected Class<PenyerahDisposisi> getEntityClass() {
        return PenyerahDisposisi.class;
    }

    public JPAJinqStream<PenyerahDisposisi>getAll(){
        return db();
    }

    public JPAJinqStream<PenyerahDisposisi> getAllByParent(Long idParent){
        PenyerahDisposisi parent = this.find(idParent);
        return db().where(q->q.getParent().equals(parent));
    }

    public JPAJinqStream<PenyerahDisposisi> getAllByPermohonan(Long idPermohonan){
        PermohonanDokumen dokumen = permohonanDokumenService.find(idPermohonan);
        return db().where(q->q.getPermohonan().equals(dokumen));
    }

    public JPAJinqStream<PenyerahDisposisi> getAllByOrg(Long idOrganization){
        MasterStrukturOrganisasi org = orgService.find(idOrganization);
        return db().where(q->q.getOrganization().equals(org));
    }

    public JPAJinqStream<PenyerahDisposisi> getAllByTindakan(Long idTindakan){
        MasterTindakan tindakan = tindakanService.find(idTindakan);
        return db().where(q->q.getTindakan().equals(tindakan));
    }

    public void saveOrEdit(Long id,PenyerahDisposisiDto dao) {
        try {
            boolean isNew = id == null;
            DateUtil dateUtil = new DateUtil();
            PenyerahDisposisi obj = new PenyerahDisposisi();
            if (isNew) {
                if(dao.getAssignedDate()!=null) {
                    obj.setAssignedDate(dateUtil.getDateFromISOString(dao.getAssignedDate()));
                }else{
                    obj.setAssignedDate(null);
                }
                obj.setComment(dao.getComment());
                if(dao.getExecutedDate()!=null) {
                    obj.setExecutedDate(dateUtil.getDateFromISOString(dao.getExecutedDate()));
                }else {
                    obj.setExecutedDate(null);
                }
                obj.setIsRead(dao.getIsRead());
                MasterStrukturOrganisasi org = orgService.find(dao.getIdOrganization());
                obj.setOrganization(org);

                if(dao.getIdParent()!=null) {
                    PenyerahDisposisi parent = this.find(dao.getIdParent());
                    obj.setParent(parent);
                }else{
                    obj.setParent(null);
                }

                obj.setStatus(dao.getStatus());

                PermohonanDokumen dok = permohonanDokumenService.find(dao.getIdPermohonan());
                obj.setPermohonan(dok);

                MasterTindakan tindak = tindakanService.find(dao.getIdTindakan());
                obj.setTindakan(tindak);
                this.create(obj);
            }else{
                obj = this.find(id);
                if(dao.getAssignedDate()!=null) {
                    obj.setAssignedDate(dateUtil.getDateFromISOString(dao.getAssignedDate()));
                }else{
                    obj.setAssignedDate(null);
                }
                obj.setComment(dao.getComment());
                if(dao.getExecutedDate()!=null) {
                    obj.setExecutedDate(dateUtil.getDateFromISOString(dao.getExecutedDate()));
                }else {
                    obj.setExecutedDate(null);
                }

                obj.setIsRead(dao.getIsRead());

                MasterStrukturOrganisasi org = orgService.find(dao.getIdOrganization());
                obj.setOrganization(org);

                if(dao.getIdParent()!=null) {
                    PenyerahDisposisi parent = this.find(dao.getIdParent());
                    obj.setParent(parent);
                }else{
                    obj.setParent(null);
                }

                obj.setStatus(dao.getStatus());

                PermohonanDokumen dok = permohonanDokumenService.find(dao.getIdPermohonan());
                obj.setPermohonan(dok);

                MasterTindakan tindak = tindakanService.find(dao.getIdTindakan());
                obj.setTindakan(tindak);

                this.edit(obj);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
