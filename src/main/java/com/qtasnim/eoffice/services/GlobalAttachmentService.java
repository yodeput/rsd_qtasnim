package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.helpers.FileUtils;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.ws.dto.AttachmentPreviewDto;
import com.qtasnim.eoffice.ws.dto.GlobalAttachmentDto;
import org.jinq.jpa.JPAJinqStream;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class GlobalAttachmentService extends AbstractFacade<GlobalAttachment> {

    @Inject
    private FileService fileService;

    @Inject
    private ApplicationConfig applicationConfig;

    @Override
    protected Class<GlobalAttachment> getEntityClass() {
        return GlobalAttachment.class;
    }

    private List<String> officeFileExts;

    @PostConstruct
    public void postConstruct() {
        officeFileExts = Arrays.asList(applicationConfig.getFilesDocServiceConvertDocs().split("\\|"));
    }

    public GlobalAttachmentDto getLink(Long id) {
        GlobalAttachment obj = this.find(id);
        return getLink(obj);
    }

    public GlobalAttachmentDto getLink(GlobalAttachment obj) {
        GlobalAttachmentDto result = new GlobalAttachmentDto(obj);
        result.setEditLink(fileService.getEditLink(obj.getDocId()));
        result.setViewLink(getPreviewLink(obj));
        return result;
    }

    public GlobalAttachmentDto findByDocId(String docId){
        return db().where(a->a.getDocId().equals(docId)).map(GlobalAttachmentDto::new).findFirst().orElse(null);
    }

    public GlobalAttachment findByDocId(String docId, String tableName){
        return db().where(a -> a.getDocId().equals(docId) && a.getReferenceTable().equals(tableName)).findFirst().orElse(null);
    }

    public GlobalAttachment findByDocId(String tableName, Long suratId, String docId){
        return db()
                .where(a -> a.getReferenceTable().equals(tableName) && a.getReferenceId().equals(suratId) && a.getDocId().equals(docId))
                .findFirst()
                .orElse(null);
    }

    public JPAJinqStream<GlobalAttachment> getAttachmentDataByRef(String tableName,Long idRef){
        return db().where(a->a.getReferenceId().equals(idRef) && a.getReferenceTable().toLowerCase()
                .contains(tableName.toLowerCase()) && !a.getIsDeleted() && !a.getIsKonsep() && !a.getIsPublished());
    }

    public JPAJinqStream<GlobalAttachment> getAttachmentDataByRefWithSearch(String tableName, Long idRef, String search){
        return db().where(a->a.getReferenceId().equals(idRef) && a.getReferenceTable().toLowerCase()
                .contains(tableName.toLowerCase()) && !a.getIsDeleted() && !a.getIsKonsep() && !a.getIsPublished() && (a.getMimeType().toLowerCase().contains(search) || a.getDocName().toLowerCase().contains(search) || a.getDocId().toLowerCase().contains(search)));
    }

    public JPAJinqStream<GlobalAttachment> getDraftDocDataByRef(String tableName,Long idRef){
        return db().where(a->a.getReferenceId().equals(idRef) && a.getReferenceTable().toLowerCase()
                .contains(tableName.toLowerCase()) && !a.getIsDeleted() && a.getIsKonsep() && !a.getIsPublished());
    }

    public JPAJinqStream<GlobalAttachment> getDocDataByRef(String tableName,Long idRef){
        return db().where(a->a.getReferenceId().equals(idRef) && a.getReferenceTable().toLowerCase()
                .contains(tableName.toLowerCase()) && !a.getIsDeleted() && a.getIsKonsep() && a.getIsPublished());
    }

    public JPAJinqStream<GlobalAttachment> getAllDataByRef(String tableName,Long idRef){
        return db().where(a->a.getReferenceId().equals(idRef) && a.getReferenceTable().toLowerCase()
                .contains(tableName.toLowerCase()) && !a.getIsDeleted());
    }

    public JPAJinqStream<GlobalAttachment> getKonsep(String tableName, Long idRef){
        return db().where(a->a.getReferenceId().equals(idRef) && a.getReferenceTable().toLowerCase()
                .contains(tableName.toLowerCase()) && !a.getIsDeleted() && a.getIsKonsep());
    }

    public JPAJinqStream<GlobalAttachment> getPublished(String tableName, Long idRef){
        return db().where(a->a.getReferenceId().equals(idRef) && a.getReferenceTable().toLowerCase()
                .contains(tableName.toLowerCase()) && !a.getIsDeleted() && a.getIsPublished());
    }

    public void deleteAttachment(String tableName,Long idRef){
        try{
            List<GlobalAttachment> all = this.getAttachmentDataByRef(tableName,idRef).toList();
            if(!all.isEmpty()){
                for(GlobalAttachment obj:all){
                    obj.setIsDeleted(true);
                    this.edit(obj);
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public JPAJinqStream<GlobalAttachment> getFileArsip(Long idArsip){
        String tableName = "t_arsip";
        return db().where(a->!a.getIsDeleted() && a.getReferenceTable().toLowerCase().contains(tableName.toLowerCase())
                && a.getReferenceId().equals(idArsip));
    }

    public void deleteMusnahArsip(Long idArsip){
        try {
            List<GlobalAttachment> arsipFile = this.getFileArsip(idArsip).toList();
            if (!arsipFile.isEmpty()) {
                for (GlobalAttachment arsip : arsipFile) {
                    fileService.delete(arsip.getDocId());
                    this.remove(arsip);
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public List<GlobalAttachmentDto> findAttachmentByTextExtracted(String query) {
        return db().where(a -> a.getExtractedText().toLowerCase().contains(query.toLowerCase())).map(GlobalAttachmentDto::new).collect(Collectors.toList());
    }

    public List<GlobalAttachmentDto> findAttachmentByTextExtracted(String query, List<String> referenceTables) {
        return db().where(a -> a.getExtractedText().toLowerCase().contains(query.toLowerCase()) && referenceTables.contains(a.getReferenceTable())).map(GlobalAttachmentDto::new).collect(Collectors.toList());
    }

    public boolean isTransactionAlreadyHasDoc(String referenceTable, Long referenceId, boolean isKonsep, boolean isPublished, boolean isDeleted) {
        return db().where(i -> i.getReferenceTable().equals(referenceTable) && i.getReferenceId().equals(referenceId)
                && i.getIsKonsep().equals(isKonsep) && i.getIsPublished().equals(isPublished) && i.getIsDeleted().equals(isDeleted)).count() > 0;
    }

    public List<Long> findReferenceIdByTextExtracted(String query, String referenceTable) {
        return db().where(i -> i.getExtractedText().toLowerCase().contains(query.toLowerCase()) && i.getReferenceTable().equals(referenceTable)).map(a -> a.getReferenceId()).collect(Collectors.toList());
    }

    public boolean isRequireOnlyOfficeViewer(Long id) {
        GlobalAttachment globalAttachment = find(id);

        boolean isRequireOnlyOfficeViewer;

        String fileExt = FileUtils.getFileExtension(globalAttachment.getDocName());

        if(fileExt.equalsIgnoreCase(".pdf")) isRequireOnlyOfficeViewer=false;
        else if(officeFileExts.contains(fileExt.toLowerCase())) isRequireOnlyOfficeViewer=true;
        else isRequireOnlyOfficeViewer=false;

        return isRequireOnlyOfficeViewer;
    }

    public boolean isRequireOnlyOfficeViewer(GlobalAttachment globalAttachment) {
        boolean isRequireOnlyOfficeViewer;

        String fileExt = FileUtility.GetFileExtension(globalAttachment.getDocName());

        if(fileExt.equalsIgnoreCase(".pdf")) isRequireOnlyOfficeViewer=false;
        else if(officeFileExts.contains(fileExt.toLowerCase())) isRequireOnlyOfficeViewer=true;
        else isRequireOnlyOfficeViewer=false;

        return isRequireOnlyOfficeViewer;
    }

    public String getPreviewLink(Long id) throws CMISProviderException {
        GlobalAttachment globalAttachment = find(id);

        boolean isRequireOnlyOfficeViewer = isRequireOnlyOfficeViewer(globalAttachment);

        return fileService.getViewLink(globalAttachment.getDocId(), isRequireOnlyOfficeViewer);
    }

    public String getPreviewLink(GlobalAttachment globalAttachment) {
        boolean isRequireOnlyOfficeViewer = isRequireOnlyOfficeViewer(globalAttachment);

        return fileService.getViewLink(globalAttachment.getDocId(), isRequireOnlyOfficeViewer);
    }

    public AttachmentPreviewDto getPreviewDto(Long id) throws CMISProviderException {
        GlobalAttachment globalAttachment = find(id);

        boolean isRequireOnlyOfficeViewer = isRequireOnlyOfficeViewer(globalAttachment);

        return new AttachmentPreviewDto(globalAttachment.getDocName(), fileService.getViewLink(globalAttachment.getDocId(), isRequireOnlyOfficeViewer), isRequireOnlyOfficeViewer, globalAttachment.getMimeType());
    }

    public AttachmentPreviewDto getPreviewDto(GlobalAttachment globalAttachment) throws CMISProviderException {
        boolean isRequireOnlyOfficeViewer = isRequireOnlyOfficeViewer(globalAttachment);

        return new AttachmentPreviewDto(globalAttachment.getDocName(), fileService.getViewLink(globalAttachment.getDocId(), isRequireOnlyOfficeViewer), isRequireOnlyOfficeViewer, globalAttachment.getMimeType());
    }

    public String getMimeType(String docId) {
        return db().where(q -> q.getDocId().equals(docId)).select(q -> q.getMimeType()).findFirst().orElse(null);
    }
}
