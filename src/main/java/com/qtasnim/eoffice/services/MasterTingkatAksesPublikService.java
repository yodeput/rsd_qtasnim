package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterTingkatAksesPublik;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterTingkatAksesPublikDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class MasterTingkatAksesPublikService extends AbstractFacade<MasterTingkatAksesPublik> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterTingkatAksesPublik> getEntityClass() {
        return MasterTingkatAksesPublik.class;
    }

    public JPAJinqStream<MasterTingkatAksesPublik> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public MasterTingkatAksesPublikDto saveOrEdit(Long id, MasterTingkatAksesPublikDto dao)throws Exception {
        try {
            MasterTingkatAksesPublikDto result;
            boolean isNew = id == null;
            MasterTingkatAksesPublik model = new MasterTingkatAksesPublik();
            DateUtil dateUtil = new DateUtil();
            if (isNew) {
                model.setName(dao.getName());
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null){
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.create(model);
            } else {
                model = this.find(id);
                model.setName(dao.getName());
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null){
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.edit(model);
            }

            result = new MasterTingkatAksesPublikDto(model);
            return result;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterTingkatAksesPublik findByNama(String value) {
        return db().where(t -> t.getName().equals(value)).findFirst().orElse(null);
    }
}