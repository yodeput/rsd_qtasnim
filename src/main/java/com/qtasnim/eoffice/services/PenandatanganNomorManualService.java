package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class PenandatanganNomorManualService extends AbstractFacade<PenandatanganNomorManual>{

    @Override
    protected Class<PenandatanganNomorManual> getEntityClass() {
        return PenandatanganNomorManual.class;
    }

    @Inject
    private PenomoranManualService penomoranManualService;

    public List<PenandatanganNomorManual> getAllBy(Long id){
        PenomoranManual obj = penomoranManualService.find(id);
        return db().where(q->q.getPermohonan().equals(obj)).toList();
    }

    public PenandatanganNomorManual getByIdOrganisasi(Long idOrganisasi) {
        return db().where(t -> t.getOrganization().getIdOrganization().equals(idOrganisasi)).findFirst().orElse(null);
    }

    public PenandatanganNomorManual getByIdOrganisasiAN(Long idOrganisasiAN) {
        return db().where(t -> t.getOrgAN().getIdOrganization().equals(idOrganisasiAN)).findFirst().orElse(null);
    }

    public List<PenomoranManual> getByIdOrganization(List<MasterStrukturOrganisasi> lists){
        return db().where(a->lists.contains(a.getOrganization()) && a.getPermohonan().getStatus().equals("SUBMITTED")).select(b->b.getPermohonan()).collect(Collectors.toList());
    }
}
