package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.db.MasterFaq;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterFaqDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterFaqService extends AbstractFacade<MasterFaq> {
    private static final long serialVersionUID = 1L;

    @Inject
    CompanyCodeService companyCodeService;


    @Override
    protected Class<MasterFaq> getEntityClass() {
        return MasterFaq.class;
    }

    public JPAJinqStream<MasterFaq> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public MasterFaq save(Long id, MasterFaqDto dao) {
        MasterFaq model = new MasterFaq();

        if (id == null) {
            this.create(addOrEdit(model, dao));
        } else {
            model = this.find(id);
            this.edit(addOrEdit(model, dao));
        }
        return model;
    }

    private MasterFaq addOrEdit(MasterFaq model, MasterFaqDto dao) {
        DateUtil dateUtil = new DateUtil();

        model.setPertanyaan(dao.getPertanyaan());
        model.setJawaban(dao.getJawaban());
        model.setKategori(dao.getKategori());

        if (dao.getCompanyId() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }
        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        return model;
    }

    public MasterFaq delete(Long id) {
        MasterFaq model = this.find(id);
        this.remove(model);
        return model;
    }

}