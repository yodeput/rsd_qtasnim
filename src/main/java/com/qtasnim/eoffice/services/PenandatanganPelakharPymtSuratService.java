package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.EntityStatus;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.PenandatanganPelakharPymtSurat;
import com.qtasnim.eoffice.db.Surat;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class PenandatanganPelakharPymtSuratService extends AbstractFacade<PenandatanganPelakharPymtSurat> {
    @Override
    protected Class<PenandatanganPelakharPymtSurat> getEntityClass() {
        return PenandatanganPelakharPymtSurat.class;
    }

    public PenandatanganPelakharPymtSurat find(Surat surat, MasterStrukturOrganisasi assignee, String status) {
        Long suratId = surat.getId();
        Long orgId = assignee.getIdOrganization();

        return db()
                .where(t -> t.getSurat().getId().equals(suratId) && t.getOrganization().getIdOrganization().equals(orgId) && t.getStatus().equals(status))
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public List<Surat> getByIdOrganization(List<MasterStrukturOrganisasi> lists){
        return db().where(a->lists.contains(a.getOrganization()) && a.getSurat().getStatus().equals("SUBMITTED")).select(b->b.getSurat()).collect(Collectors.toList());
    }

    public PenandatanganPelakharPymtSurat getBySurat(Long idSurat){
        return db().where(u->u.getSurat().getId().equals(idSurat) && u.getStatus().equals("pymt")).findFirst().orElse(null);
    }
}
