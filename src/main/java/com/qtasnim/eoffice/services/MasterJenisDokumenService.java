package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.ws.FormDefinitionAngularResource;
import com.qtasnim.eoffice.ws.MasterJenisDokumenDocLibResource;
import com.qtasnim.eoffice.ws.WorkflowResource;
import com.qtasnim.eoffice.ws.dto.JenisDokumenDocLibDto;
import com.qtasnim.eoffice.ws.dto.MasterJenisDokumenDto;
import com.qtasnim.eoffice.ws.dto.ProcessDefinitionDto;
import com.qtasnim.eoffice.ws.dto.ng.FormDefinitionDto;
import org.apache.commons.compress.utils.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Stateless
@LocalBean
public class MasterJenisDokumenService extends AbstractFacade<MasterJenisDokumen> {

    @Inject
    private FormDefinitionService formDefinitionService;

    @Inject
    private ProcessDefinitionService processDefinitionService;

    @Inject
    private JenisDokumenDocLibService jenisDokumenDocLibService;

    @Inject
    private FormDefinitionAngularResource formDefinitionResource;

    @Inject
    private WorkflowResource workflowResource;

    @Inject
    private MasterJenisDokumenDocLibResource masterJenisDokumenDocLibResource;

    @Inject
    private FileService fileService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private FormFieldService formFieldService;

    @Inject
    private MasterMetadataService masterMetadataService;

    @Inject
    private ProcessDefinitionFormVarResolverService processDefinitionFormVarResolverService;

    @Inject
    private ProcessInstanceService processInstanceService;

    @Inject
    private ProcessTaskService processTaskService;

    @Inject
    private ProcessDefinitionVarService processDefinitionVarService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private Logger logger;

    @Inject
    private NgSuratService suratService;

    @Override
    protected Class<MasterJenisDokumen> getEntityClass() {
        return MasterJenisDokumen.class;
    }

    public JPAJinqStream<MasterJenisDokumen> getAll(boolean isKorespondensi) {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && q.getIsKorespondensi().booleanValue() == isKorespondensi);
    }

    public JPAJinqStream<MasterJenisDokumen> getByGroupId(Long idGroup) {
        Date n = new Date();
        JPAJinqStream<MasterJenisDokumen> query = db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
        if (idGroup != null) {
            MasterJenisDokumen jenisDoc = this.find(idGroup);
            query = query.where(a -> a.getGroup().equals(jenisDoc));
        } else {
            query = query.where(a -> a.getGroup() == null);
        }
        return query;
    }

    public JPAJinqStream<MasterJenisDokumen> getFiltered(String kode, String nama, Boolean isActive, Boolean isKorespondensi) {
        JPAJinqStream<MasterJenisDokumen> query = db();

        if (!Strings.isNullOrEmpty(kode)) {
            query = query.where(q -> q.getKodeJenisDokumen().toLowerCase().contains(kode.toLowerCase()));
        }

        if (!Strings.isNullOrEmpty(nama)) {
            query = query.where(q -> q.getNamaJenisDokumen().toLowerCase().contains(nama.toLowerCase()));
        }

        if (isActive != null) {
            if (isActive) {
                query = query.where(q -> q.getIsActive());
            } else {
                query = query.where(q -> !q.getIsActive());
            }
        }

        if (isKorespondensi != null) {
            if (isKorespondensi) {
                query = query.where(q -> q.getIsKorespondensi());
            } else {
                query = query.where(q -> !q.getIsKorespondensi());
            }
        }

        return query;
    }

    public MasterJenisDokumenDto saveOrEdit(Long id, MasterJenisDokumenDto dao) throws IOException {
        MasterJenisDokumenDto result;
        boolean isNew = id == null;
        MasterJenisDokumen model = new MasterJenisDokumen();

        if (isNew) {
            bindDao(model, dao);
            create(model);

            if (dao.getParentId() != null) {
                copySettings(model.getParent(), model);
            }
        } else {
            model = this.find(id);
            bindDao(model, dao);
            edit(model);
        }

        getEntityManager().flush();
        getEntityManager().refresh(model);
        result = new MasterJenisDokumenDto(model);

        return result;
    }

    private void copySettings(MasterJenisDokumen from, MasterJenisDokumen to) throws IOException {
        DateUtil dateUtil = new DateUtil();
        String currentISODate = dateUtil.getCurrentISODate();

        FormDefinition formDefinition = formDefinitionService.getPublished(from.getIdJenisDokumen()).findFirst().orElse(null);
        ProcessDefinition processDefinition = processDefinitionService.getActivated(from.getIdJenisDokumen());
        JenisDokumenDocLib jenisDokumenDocLib = jenisDokumenDocLibService.getPublished(from.getIdJenisDokumen()).findFirst().orElse(null);

        if (formDefinition == null) {
            throw new InternalServerErrorException("No published form definition from parent, please activate one");
        }

        if (processDefinition == null) {
            throw new InternalServerErrorException("No published workflow from parent, please activate one");
        }

        if (!from.getBaseCode().equals(Constants.KODE_REGISTRASI_SURAT)) {
            if (jenisDokumenDocLib == null) {
                throw new InternalServerErrorException("No published doc lib from parent, please activate one");
            }

            JenisDokumenDocLibDto jenisDokumenDocLibDto = new JenisDokumenDocLibDto(jenisDokumenDocLib);
            jenisDokumenDocLibDto.setActivationDate(currentISODate);
            byte[] jenisDocumentByteContent = fileService.download(jenisDokumenDocLib.getDocId());

            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(jenisDocumentByteContent)) {
                masterJenisDokumenDocLibResource.save(to.getIdJenisDokumen(), jenisDokumenDocLibDto, inputStream, jenisDokumenDocLib.getDocName(), jenisDokumenDocLib.getMimeType());
            }
        }

        FormDefinitionDto formDefinitionDto = new FormDefinitionDto(formDefinition);
        formDefinitionDto.setActivationDate(currentISODate);
        formDefinitionResource.save(to.getIdJenisDokumen(), formDefinitionDto);

        ProcessDefinitionDto processDefinitionDto = new ProcessDefinitionDto(processDefinition);
        processDefinitionDto.setActivationDate(currentISODate);
        workflowResource.save(to.getIdJenisDokumen(), processDefinitionDto, org.apache.commons.io.IOUtils.toInputStream(processDefinition.getDefinition(), Charset.defaultCharset()), null);
    }

    private void bindDao(MasterJenisDokumen entity, MasterJenisDokumenDto dto) {
        DateUtil dateUtil = new DateUtil();
        CompanyCode companyCode = companyCodeService.find(dto.getCompanyId());

        entity.setBaseCode(dto.getBaseCode());
        entity.setKodeJenisDokumen(dto.getKodeJenisDokumen());
        entity.setNamaJenisDokumen(dto.getNamaJenisDokumen());
        entity.setIsActive(dto.getIsActive());
        entity.setIsHidden(dto.getIsHidden());
        entity.setIsDeletable(dto.getIsDeletable());
        entity.setIsMultiPemeriksa(dto.getIsMultiPemeriksa());
        entity.setIsPemeriksaMandatory(dto.getIsPemeriksaMandatory());
        entity.setIsRegulasi(Optional.ofNullable(dto.getIsRegulasi()).orElse(false));
        entity.setIsIncludePejabatMengetahui(Optional.ofNullable(dto.getIsIncludePejabatMengetahui()).orElse(false));
        entity.setJumlahPenandatangan(Optional.ofNullable(dto.getJumlahPenandatangan()).orElse(1));
        entity.setCompanyCode(companyCode);
        entity.setStart(dateUtil.getDateTimeStart(dto.getStartDate()));

        Optional.ofNullable(dto.getParentId()).ifPresent(id -> {
            MasterJenisDokumen parent = find(id);
            entity.setParent(parent);

            if (entity.getBaseCode() == null) {
                entity.setBaseCode(parent.getBaseCode());
            }
        });

        if (dto.getEndDate() != null) {
            entity.setEnd(dateUtil.getDateTimeEnd(dto.getEndDate()));
        } else {
            entity.setEnd(dateUtil.getDefValue());
        }

        entity.setIsKorespondensi(dto.getIsKorespondensi());

        if (dto.getIdGroup() != null) {
            MasterJenisDokumen group = this.find(dto.getIdGroup());
            entity.setGroup(group);
        }
    }

    public void uploadAttachment(Long idJenisDokumen, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        try {
            String filename = formDataContentDisposition.getFileName();
            byte[] documentContent = IOUtils.toByteArray(inputStream);
            String id = fileService.upload(filename, documentContent);
            MasterJenisDokumen doc = this.find(idJenisDokumen);

//            String number = autoNumberService.from(SuratDocLib.class).next(SuratDocLib::getNumber);

            JenisDokumenDocLib obj = new JenisDokumenDocLib();
            DateUtil dateUtil = new DateUtil();

            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString());
            obj.setDocName(filename);
            obj.setCompanyCode(doc.getCompanyCode());
            obj.setDocId(id);
            obj.setSize(formDataContentDisposition.getSize());
            obj.setNumber("test 123");
            obj.setJenisDokumen(doc);
            obj.setVersion(1.0);
            obj.setStart(new Date());
            obj.setEnd(dateUtil.getDefValue());
            obj.setIsActive(true);

            jenisDokumenDocLibService.create(obj);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public void deleteJenisDokumen(MasterJenisDokumen jenisDokumen) {
        try {
            JenisDokumenDocLib docLib = jenisDokumenDocLibService.getByJenisDokumenId(jenisDokumen.getIdJenisDokumen());
            if (docLib != null) {
                fileService.delete(docLib.getDocId());
                jenisDokumenDocLibService.remove(docLib);
            }
            this.remove(jenisDokumen);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void forceDelete(MasterJenisDokumen from) {
        if (suratService.findByJenisDokumenId(from.getIdJenisDokumen()).count() > 0) {
            throw new InternalServerErrorException("Jenis Dokumen yang sudah digunakan tidak bisa dihapus");
        }

        if (findChildByJenisDokumenId(from.getIdJenisDokumen()).count() > 0) {
            throw new InternalServerErrorException("Jenis Dokumen yang punya Turunan tidak bisa dihapus");
        }

        MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
        IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();

        formDefinitionService.findByJenisDokumenId(from.getIdJenisDokumen()).forEach(formDefinition -> {
            formDefinitionService.remove(formDefinition);
        });

        processDefinitionService.findByJenisDokumenId(from.getIdJenisDokumen()).forEach(processDefinition -> {
            processDefinition.getInstances().forEach(processInstance -> {
                processInstance.getTasks().stream().sorted(Comparator.comparingLong(ProcessTask::getId)).forEach(processTask -> {
                    processTaskService.findChildByTaskId(processTask.getTaskId()).forEach(child -> {
                        child.setPrevious(null);
                        processTaskService.edit(child);
                    });
                });
            });

            if (processDefinition.getDeploymentId() != null) {
                try {
                    workflowProvider.undeploy(processDefinition.getDeploymentId());
                } catch (Exception e) {
                    logger.error(null, e);
                }
            }

            getEntityManager().flush();
            processDefinitionService.remove(processDefinition);
        });

        jenisDokumenDocLibService.findByJenisDokumenId(from.getIdJenisDokumen()).forEach(jenisDokumenDocLib -> {
            try {
                fileService.delete(jenisDokumenDocLib.getDocId());
            } catch (Exception e) {
                logger.error(null, e);
            }

            jenisDokumenDocLibService.remove(jenisDokumenDocLib);
        });

        remove(from);
    }

    private JinqStream<MasterJenisDokumen> findChildByJenisDokumenId(Long idJenisDokumen) {
        return db().where(t -> t.getParent().getIdJenisDokumen().equals(idJenisDokumen));
    }

    public MasterJenisDokumen getByCode(String code) {
        return db().where(t -> t.getKodeJenisDokumen().equals(code)).findFirst().orElse(null);
    }
}
