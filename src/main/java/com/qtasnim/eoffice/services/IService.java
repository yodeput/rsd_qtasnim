package com.qtasnim.eoffice.services;

import javax.ejb.Asynchronous;

public interface IService {
    String getName();
    @Asynchronous
    void run();
    Boolean isRun();
    String getClassName();
    Boolean isRunnable();
}
