package com.qtasnim.eoffice.services;


import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.*;
import com.qtasnim.eoffice.bssn.BeforeSigningEvent;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import sun.security.x509.*;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;


@Stateless
@LocalBean
public class DigiSignService implements IDigiSignService {
    public static final String ALIAS = "kai";
    public static final String PASSWORD = "pass@word1";
    public static final String ORGANIZATION_UNIT = "PT Kereta Api Indonesia";
    public static final long VALIDITY = 1096; // 3 years
    public static final String COUNTRY = "ID";
    public static final String ALGORITHM = "SHA1WithRSA";
    public static final int KEYSIZE = 1024;
    private Consumer<BeforeSigningEvent> onBeforeSigning = event -> {};

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Inject
    private QRGenerateService qrGenerateService;

    public byte[] signFromUsb(byte[] pdfByte, String password) throws Exception {
        char[] passwords = new char[]{};

        if (password != null && !"".equals(password)) {
            passwords = password.toCharArray();
        }

        String pkcs11Config = "name=eToken" + "\n" + "library=C:\\Windows\\System32\\eTPKCS11.dll";
        ByteArrayInputStream configStream = new ByteArrayInputStream(pkcs11Config.getBytes());
        AuthProvider provider = new sun.security.pkcs11.SunPKCS11(configStream);
        Security.addProvider(provider);
        KeyStore keyStore = KeyStore.getInstance("PKCS11");
        keyStore.load(null, passwords);
        Enumeration aliases = keyStore.aliases();

        if (!aliases.hasMoreElements()) {
            throw new Exception("No digital IDs found in token.");
        }

        String alias = (String) aliases.nextElement();
        PrivateKey key = (PrivateKey) keyStore.getKey(alias, passwords);
        Certificate[] chain = keyStore.getCertificateChain(alias);
        ExternalSignature pks = new PrivateKeySignature(key, DigestAlgorithms.SHA256, provider.getName());

        return sign(pks, chain, pdfByte);
    }

    @Override
    public byte[] sign(MasterStrukturOrganisasi strukturOrganisasi, byte[] pdfByte) {
        try {
            BeforeSigningEvent event = new BeforeSigningEvent();
            onBeforeSigning.accept(event);

            if (event.getQrCode() != null) {
                pdfByte = qrGenerateService.embedQRCodeToPdf(pdfByte, event.getQrCode(), new Point(450, 660));
            }

            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            String keyStoreInstance = "JKS";

            byte[] keyStoreByte = getKeyStore(
                    keyStoreInstance,
                    ALIAS,
                    Optional.ofNullable(strukturOrganisasi.getUser()).map(t -> String.format("%s %s", t.getNameFront(), t.getNameMiddleLast()).trim()).orElse(strukturOrganisasi.getOrganizationName()),
                    ORGANIZATION_UNIT,
                    strukturOrganisasi.getOrganizationName(),
                    Optional.of(strukturOrganisasi)
                            .filter(t -> t.getArea() != null && t.getArea().getKota() != null)
                            .map(t -> t.getArea().getKota().getNamaKota())
                            .orElse("Indonesia"),
                    Optional.of(strukturOrganisasi)
                            .filter(t -> t.getArea() != null && t.getArea().getKota() != null && t.getArea().getKota().getPropinsi() != null)
                            .map(t -> t.getArea().getKota().getPropinsi().getNama())
                            .orElse("Indonesia"),
                    PASSWORD
            );

            KeyStore ks = KeyStore.getInstance(keyStoreInstance);

            try (InputStream keyStream = new ByteArrayInputStream(keyStoreByte)) {
                ks.load(keyStream, PASSWORD.toCharArray());
            }

            PrivateKey privateKey = (PrivateKey) ks.getKey(ALIAS, PASSWORD.toCharArray());
            ExternalSignature privateKeySignature = new PrivateKeySignature(privateKey, DigestAlgorithms.SHA256, "BC");

            return sign(privateKeySignature, ks.getCertificateChain(ALIAS), pdfByte);
        } catch (Exception e) {
            logger.error("Failed to sign a pdf", e);
            notificationService.sendError("Error", "Digital signature gagal");
        }

        return pdfByte;
    }

    @Override
    public void validate(MasterStrukturOrganisasi strukturOrganisasi, String passphrase) {

    }

    @Override
    public DigiSignService onSigning(Consumer<Map<String, String>> onSigning) {
        return this;
    }

    @Override
    public IDigiSignService onBeforeSigning(Consumer<BeforeSigningEvent> onBeforeSigning) {
        this.onBeforeSigning = onBeforeSigning;
        return this;
    }

    @Override
    public Boolean isRequirePassphrase() {
        return false;
    }

    public byte[] sign(ExternalSignature pks, Certificate[] chain, byte[] pdfByte) throws Exception {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            PdfReader reader = new PdfReader(pdfByte);
            PdfStamper stp = PdfStamper.createSignature(reader, outputStream, '\0');
            PdfSignatureAppearance sap = stp.getSignatureAppearance();

            ExternalDigest digest = new BouncyCastleDigest();
            MakeSignature.signDetached(sap, digest, pks, chain, null, null, null, 0, null);

            pdfByte = outputStream.toByteArray();
            reader.close();
        }

        return pdfByte;
    }

    private X509Certificate generateCertificate(
            KeyPair keyPair,
            String commonName,
            String organizationalUnit,
            String organization,
            String city,
            String state,
            String country,
            long validity,
            String signatureAlgorithmName) throws GeneralSecurityException, IOException {
        PrivateKey privateKey = keyPair.getPrivate();

        X509CertInfo info = new X509CertInfo();

        Date from = new Date();
        Date to = new Date(from.getTime() + validity * 1000L * 24L * 60L * 60L);

        CertificateValidity interval = new CertificateValidity(from, to);
        BigInteger serialNumber = new BigInteger(64, new SecureRandom());
        X500Name owner = new X500Name(commonName, organizationalUnit, organization, city, state, country);
        AlgorithmId sigAlgId = new AlgorithmId(AlgorithmId.md5WithRSAEncryption_oid);

        info.set(X509CertInfo.VALIDITY, interval);
        info.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(serialNumber));
        info.set(X509CertInfo.SUBJECT, owner);
        info.set(X509CertInfo.ISSUER, owner);
        info.set(X509CertInfo.KEY, new CertificateX509Key(keyPair.getPublic()));
        info.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
        info.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(sigAlgId));

        // Sign the cert to identify the ALGORITHM that's used.
        X509CertImpl certificate = new X509CertImpl(info);
        certificate.sign(privateKey, signatureAlgorithmName);

        // Update the algorith, and resign.
        sigAlgId = (AlgorithmId) certificate.get(X509CertImpl.SIG_ALG);
        info.set(CertificateAlgorithmId.NAME + "." + CertificateAlgorithmId.ALGORITHM, sigAlgId);
        certificate = new X509CertImpl(info);
        certificate.sign(privateKey, signatureAlgorithmName);

        return certificate;
    }

    public byte[] getKeyStore(String keyStoreInstance, String alias, String commonName, String organizationalUnit, String organization, String city, String state, String password) throws IOException, GeneralSecurityException {
        byte[] result;

        KeyStore keyStore = KeyStore.getInstance(keyStoreInstance);
        keyStore.load(null, null);
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(KEYSIZE, new SecureRandom());
        KeyPair keyPair = keyGen.generateKeyPair();
        X509Certificate[] chain = new X509Certificate[1];
        chain[0] = generateCertificate(keyPair, commonName, organizationalUnit, organization, city, state, COUNTRY, VALIDITY, ALGORITHM);
        keyStore.setKeyEntry(alias, keyPair.getPrivate(), password.toCharArray(), chain);

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            keyStore.store(outputStream, password.toCharArray());
            result = outputStream.toByteArray();
        }

        return result;
    }
}
