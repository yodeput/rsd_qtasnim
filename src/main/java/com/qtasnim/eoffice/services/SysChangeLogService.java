package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.SysChangeLog;
import com.qtasnim.eoffice.db.SysChangeLogDetail;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.CompanyCodeDto;
import com.qtasnim.eoffice.ws.dto.SysChangeLogDetailDto;
import com.qtasnim.eoffice.ws.dto.SysChangeLogDto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class SysChangeLogService extends AbstractFacade<SysChangeLog> {

    private static final long serialVersionUID = 1L;

    @Inject
    CompanyCodeService companyCodeService;

    @Inject
    SysChangeLogDetailService sysChangeLogDetailService;

    @Override
    protected Class<SysChangeLog> getEntityClass() {
        return SysChangeLog.class;
    }

    public JPAJinqStream<SysChangeLog> getAll() {
        return db();
    }

    public SysChangeLog save(Long id, SysChangeLogDto dao) {
        SysChangeLog model = new SysChangeLog();

        if (id == null) {
            this.create(addOrEdit(model, dao));
            getEntityManager().flush();
            createChangeLogDetail(model, dao);
        } else {
            model = this.find(id);
            this.edit(addOrEdit(model, dao));
            getEntityManager().flush();
            createChangeLogDetail(model, dao);

        }
        return model;
    }

    public List<SysChangeLogDto> mapToDto(List<SysChangeLog> all) throws InternalServerErrorException {
        List<SysChangeLogDto> result = new ArrayList<>();

        if (all != null && all.size() > 0) {
            Long lastId = all.get(all.size() - 1).getId();
            result = all.stream()
                    .map(q -> {
                        SysChangeLogDto obj = new SysChangeLogDto();
                        DateUtil dateUtil = new DateUtil();

                        obj.setId(q.getId());
                        obj.setLastId(lastId);
                        obj.setVersion(q.getVersion());
                        obj.setIsWeb(q.getIsWeb());
                        obj.setChangeLogDate(dateUtil.getCurrentISODate(q.getChangeLogDate()));

                        if (q.getCompanyCode() != null) {
                            obj.setCompanyCode(Optional.ofNullable(q.getCompanyCode()).map(CompanyCodeDto::new).orElse(null));
                            obj.setCompanyId(Optional.ofNullable(q.getCompanyCode()).map(t -> t.getId()).orElse(null));
                        }

                        List<SysChangeLogDetailDto> details = q.getChangeLogDetailList().stream().map(d -> {
                            SysChangeLogDetailDto dto = new SysChangeLogDetailDto();

                            dto.setId(d.getId());
                            dto.setIdChangeLog(Optional.ofNullable(d.getChangeLog()).map(t -> t.getId()).orElse(null));
                            dto.setDescription(d.getDescription());
                            dto.setCreatedBy(d.getCreatedBy());
                            dto.setCreatedDate(dateUtil.getCurrentISODate(d.getCreatedDate()));

                            dto.setModifiedBy(d.getModifiedBy());
                            if (d.getModifiedDate() != null) {
                                obj.setModifiedDate(dateUtil.getCurrentISODate(d.getModifiedDate()));
                            }

                            return dto;
                        }).collect(Collectors.toList());
                        obj.setDetail(details);

                        obj.setCreatedBy(q.getCreatedBy());
                        obj.setCreatedDate(dateUtil.getCurrentISODate(q.getCreatedDate()));

                        obj.setModifiedBy(q.getModifiedBy());
                        if (q.getModifiedDate() != null) {
                            obj.setModifiedDate(dateUtil.getCurrentISODate(q.getModifiedDate()));
                        }

                        return obj;
                    }).sorted(Comparator.comparing(SysChangeLogDto::getId).reversed()).collect(Collectors.toList());
        }

        return result;
    }

    private void createChangeLogDetail(SysChangeLog model, SysChangeLogDto dao) {
        for (SysChangeLogDetailDto dto : dao.getDetail()) {
            SysChangeLogDetail detail = new SysChangeLogDetail();
            detail.setChangeLog(model);
            detail.setDescription(dto.getDescription());
            sysChangeLogDetailService.create(detail);
        }
    }

    private SysChangeLog addOrEdit(SysChangeLog model, SysChangeLogDto dao) {

        model.setVersion(dao.getVersion());
        model.setChangeLogDate(new DateUtil().getDateFromISOString(dao.getChangeLogDate()));
        model.setIsWeb(dao.getIsWeb());
        if (dao.getCompanyId() != null) {
            CompanyCode companyCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(companyCode);
        } else {
            model.setCompanyCode(null);
        }

        return model;
    }

    public SysChangeLog delete(Long id) {
        SysChangeLog model = this.find(id);
        this.remove(model);
        return model;
    }

    public SysChangeLog saveOrEdit(Long id, SysChangeLogDto dao, List<SysChangeLogDetailDto> details, List<Long> deletedDetailsId) {
        DateUtil dateUtil = new DateUtil();
        boolean isNew = id == null;
        SysChangeLog model = new SysChangeLog();

        if (!isNew) {
            model = this.find(id);
        }
        model.setVersion(dao.getVersion());
        model.setChangeLogDate(dateUtil.getDateFromISOString(dao.getChangeLogDate()));
        model.setIsWeb(dao.getIsWeb());
        if (dao.getCompanyId() != null) {
            CompanyCode companyCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(companyCode);
        } else {
            model.setCompanyCode(null);
        }

        if (isNew) {
            this.create(model);
            getEntityManager().flush();
            createChangeLogDetail(model.getId(), details);
        } else {
            this.edit(model);

            if (deletedDetailsId != null) {
                deleteChangeLogDetail(deletedDetailsId);
            }

            if (details != null) {
                List<SysChangeLogDetailDto> newDetails = details.stream().filter(q -> q.getId() == null).collect(Collectors.toList());
                createChangeLogDetail(id, newDetails);

                List<SysChangeLogDetailDto> oldDetails = details.stream().filter(q -> q.getId() != null).collect(Collectors.toList());
                editChangeLogDetail(oldDetails);
            }
        }

        return model;
    }

    private void deleteChangeLogDetail(List<Long> details) {
        for (Long d : details) {
            sysChangeLogDetailService.delete(d);
        }
    }

    private void createChangeLogDetail(Long id, List<SysChangeLogDetailDto> details) {
        for (SysChangeLogDetailDto d : details) {
            d.setIdChangeLog(id);
            sysChangeLogDetailService.save(null, d);
        }
    }

    private void editChangeLogDetail(List<SysChangeLogDetailDto> details) {
        for (SysChangeLogDetailDto d : details) {
            SysChangeLogDetail detail = sysChangeLogDetailService.find(d.getId());
            if (!detail.getDescription().equals(d.getDescription())) {
                detail.setDescription(d.getDescription());
                sysChangeLogDetailService.save(d.getId(), d);
            }
        }
    }
}
