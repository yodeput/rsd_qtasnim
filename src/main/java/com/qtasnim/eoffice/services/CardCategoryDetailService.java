package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.CardCategoryDetailDto;
import com.qtasnim.eoffice.ws.dto.CardCommentDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class CardCategoryDetailService  extends AbstractFacade<CardCategoryDetail>{
    @Override
    protected Class<CardCategoryDetail> getEntityClass() {
        return CardCategoryDetail.class;
    }

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyService;

    @Inject
    private CardService cardService;

    @Inject
    private MasterKategoriCardService kategoriCardService;

    public JPAJinqStream<CardCategoryDetail> getAll(){
        Long idCompany = userSession.getUserSession().getUser().getCompanyCode().getId();
        CompanyCode company = companyService.find(idCompany);
        return db().where(a->a.getIsDeleted().booleanValue()==false && a.getCompanyCode().equals(company));
    }

    public CardCategoryDetail saveOrEdit(CardCategoryDetailDto dao, Long id) throws Exception {
        try{
            CardCategoryDetail obj = new CardCategoryDetail();
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = companyService.getByCode(user.getCompanyCode().getCode());
            if(id==null){
                obj.setIsDeleted(false);
                if(dao.getCardId()!=null){
                    Card card = cardService.find(dao.getCardId());
                    obj.setCard(card);
                }
                obj.setCompanyCode(company);
                if(dao.getKategoriId()!=null){
                    MasterKategoriCard kategoriCard = kategoriCardService.find(dao.getKategoriId());
                    obj.setKategori(kategoriCard);
                }
                create(obj);
            }else{
                obj.setIsDeleted(false);
                if(dao.getCardId()!=null){
                    Card card = cardService.find(dao.getCardId());
                    obj.setCard(card);
                }
                obj.setCompanyCode(company);
                if(dao.getKategoriId()!=null){
                    MasterKategoriCard kategoriCard = kategoriCardService.find(dao.getKategoriId());
                    obj.setKategori(kategoriCard);
                }
                edit(obj);
            }
            getEntityManager().flush();
            getEntityManager().refresh(obj);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public CardCategoryDetail deleteCategoryDetail(Long id) throws Exception {
        try{
            CardCategoryDetail obj = find(id);
            obj.setIsDeleted(true);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
}
