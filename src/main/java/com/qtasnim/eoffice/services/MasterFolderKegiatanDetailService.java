package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class MasterFolderKegiatanDetailService extends AbstractFacade<MasterFolderKegiatanDetail> {

    @Inject
    private MasterFolderKegiatanService folderKegiatanService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Override
    protected Class<MasterFolderKegiatanDetail> getEntityClass() {
        return MasterFolderKegiatanDetail.class;
    }

    public JPAJinqStream<MasterFolderKegiatanDetail> getAll() { return db(); }

    public JPAJinqStream<MasterFolderKegiatanDetail> getByParentId(Long parent,String sort, Boolean descending, String search){
        JPAJinqStream<MasterFolderKegiatanDetail> query = db();
        MasterFolderKegiatan person = folderKegiatanService.getFolderPersonal("Personal");
        MasterUser user = userSession.getUserSession().getUser();
        if(parent==null){
            query = query.where(q->q.getParent().equals(null) && q.getPersonal().equals(person) && q.getUser().equals(user));
        }else{
            query = query.where(q->q.getParent().getId().equals(parent) && q.getPersonal().equals(person) && q.getUser().equals(user));
        }

        if(!Strings.isNullOrEmpty(search)){
            query = query.where(q->q.getFolderName().toLowerCase().contains(search));
        }


        if (!Strings.isNullOrEmpty(sort)) {
            if (descending) {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedDescendingBy(q->q.getFolderName());
                }
            } else {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedBy(q -> q.getFolderName());
                }
            }
        }
        return query;
    }

    public JPAJinqStream<MasterFolderKegiatanDetail> getByParentId2(Long parent){
        return db().where(a->a.getParent().getId().equals(parent));
    }

    public List<MasterFolderKegiatanDetail> getChilds(Long idParent){
        return db().where(a->a.getParent().getId().equals(idParent)).toList();
    }

    public boolean validateName(String folderName, Long userId, Long id, Long parentId){
        boolean result = false;

        JPAJinqStream<MasterFolderKegiatanDetail> query =  db().where(a->a.getUser().getId().equals(userId));

        if(id==null){
            /*add*/
            if(parentId==null){
                List<MasterFolderKegiatanDetail> tmp = query.where(b -> b.getParent() == null).collect(Collectors.toList());
                result = tmp.stream().noneMatch(c->c.getFolderName().toLowerCase().equals(folderName.toLowerCase()));
            }else{
                List<MasterFolderKegiatanDetail> tmp = query.where(b -> b.getParent().getId().equals(parentId)).collect(Collectors.toList());
                result = tmp.stream().noneMatch(c->c.getFolderName().toLowerCase().equals(folderName.toLowerCase()));
            }
        }else{
            /*edit*/
            if(parentId==null){
                List<MasterFolderKegiatanDetail> tmp = query.where(b -> b.getParent() == null && b.getId()!=id).collect(Collectors.toList());
                result = tmp.stream().noneMatch(c->c.getFolderName().toLowerCase().equals(folderName.toLowerCase()));
            }else{
                List<MasterFolderKegiatanDetail> tmp = query.where(b -> b.getParent().getId().equals(parentId) && b.getId()!=id).collect(Collectors.toList());
                result = tmp.stream().noneMatch(c->c.getFolderName().toLowerCase().equals(folderName.toLowerCase()));
            }
        }
        return result;
    }
}
