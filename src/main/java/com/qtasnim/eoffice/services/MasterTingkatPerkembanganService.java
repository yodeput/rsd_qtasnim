package com.qtasnim.eoffice.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterTingkatPerkembangan;

import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterTingkatPerkembanganDto;
import org.jinq.jpa.JPAJinqStream;

import java.util.Date;

@Stateless
@LocalBean
public class MasterTingkatPerkembanganService extends AbstractFacade<MasterTingkatPerkembangan> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterTingkatPerkembangan> getEntityClass() {
        return MasterTingkatPerkembangan.class;
    }

    public JPAJinqStream<MasterTingkatPerkembangan> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public MasterTingkatPerkembanganDto saveOrEdit(Long id, MasterTingkatPerkembanganDto dao)throws Exception {
        try {
            MasterTingkatPerkembanganDto result;
            boolean isNew = id == null;
            MasterTingkatPerkembangan model = new MasterTingkatPerkembangan();
            DateUtil dateUtil = new DateUtil();
            if (isNew) {
                model.setName(dao.getName());
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null){
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.create(model);
            } else {
                model = this.find(id);
                model.setName(dao.getName());
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null){
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.edit(model);
            }

            result = new MasterTingkatPerkembanganDto(model);
            return result;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterTingkatPerkembangan findByNama(String value) {
        return db().where(t -> t.getName().equals(value)).findFirst().orElse(null);
    }
}