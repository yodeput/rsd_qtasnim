package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.db.SysLog;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MostLoginDashboardDto;
import com.qtasnim.eoffice.ws.dto.SysLogDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Stateless
@LocalBean
public class SysLogService extends AbstractFacade<SysLog> {
    private static final long serialVersionUID = 1L;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    CompanyCodeService companyCodeService;

    @Inject
    MasterUserService masterUserService;

    @Override
    protected Class<SysLog> getEntityClass() {
        return SysLog.class;
    }

    public JPAJinqStream<SysLog> getAll() {
        return db();
    }

     public SysLog save(Long id, SysLogDto dao){
         SysLog model = new SysLog();
         DateUtil dateUtil = new DateUtil();

         if(id==null){
             this.create(addOrEdit(model, dao));
         } else {
             model = this.find(id);
             this.edit(addOrEdit(model, dao));
         }
        return model;
     }

     private SysLog addOrEdit(SysLog model, SysLogDto dao){
        model.setCategory(dao.getCategory());
        model.setContent(dao.getContent());
        model.setExecutorIp(dao.getExecutorIp());
        model.setDocId(dao.getDocId());
        CompanyCode companyCode = companyCodeService.find(dao.getCompanyId());
        model.setCompanyCode(companyCode);

        return model;
     }

    public SysLog delete(Long id){
        SysLog model = this.find(id);
        this.remove(model);
        return model;
    }

    public JPAJinqStream<SysLog> getByCategory(String category) {
        String kategori = category.toLowerCase();
        return db().where(t -> t.getCategory().toLowerCase().equals(kategori));
    }

    private Map<String, Long> getChartData(String start, String end) {
        DateUtil dateUtil = new DateUtil();
        Date dStart = dateUtil.getDateTimeStart(start), dEnd = dateUtil.getDateTimeEnd(end);

        JPAJinqStream<SysLog> source = this.getByCategory("LOGIN")
                .where(t -> t.getContent().toLowerCase().contains("login"))
                .where(t -> dStart.before(t.getCreatedDate()) && dEnd.after(t.getCreatedDate()));

        return source.map(SysLog::getContent).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

//    private TypedQuery<SysLog> getChartData(String idUnit, String start, String end) {
////        Long org = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();
//        String korsa = userSession.getUserSession().getUser().getKorsa().getId();
//        String id = StringUtils.isNotEmpty(idUnit) ? idUnit : korsa;
//
//        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
//        CriteriaQuery<SysLog> q = cb.createQuery(SysLog.class);
//        Root<SysLog> root = q.from(SysLog.class);
//
//        q.select(root);
//        List<Predicate> predicates = new ArrayList<>();
//
//        Root<MasterUser> userRoot = q.from(MasterUser.class);
//        String idUser = root.get("content").toString().substring(4).split(" ", 2)[0];
//        Predicate pFieldEquals = cb.equal(userRoot.get("loginUserName"), idUser);
//        predicates.add(pFieldEquals);
//
//        Predicate pUnit = cb.equal(userRoot.join("korsa", JoinType.INNER).get("id"), id);
//        predicates.add(pUnit);
//
//        DateUtil dateUtil = new DateUtil();
//        Date dStart = dateUtil.getDateTimeStart(start), dEnd = dateUtil.getDateTimeEnd(end);
//        Path<Date> rootDate = root.<Date>get("createdDate");
//        Predicate pBetween
//                = cb.and(
//                cb.greaterThanOrEqualTo(rootDate, dStart),
//                cb.lessThanOrEqualTo(rootDate, dEnd)
//        );
//        predicates.add(pBetween);
//
//        q.where(predicates.toArray(new Predicate[predicates.size()]));
//        q.distinct(true);
//        q.orderBy(cb.desc(root.get("createdDate")));
//
//        return getEntityManager().createQuery(q);
//    }

    public HashMap<String,Object> getChartMostLoginUser(String idUnit, String start, String end,
                                                             Integer skip, Integer limit, String sort, Boolean descending) {
        List<MostLoginDashboardDto> result = new ArrayList<>();
        Map<String, Long> all = getChartData(start, end);

        result = getChartDocData(all, idUnit, skip, limit, sort, descending);
        int length = all.values().size();
        HashMap<String,Object> retur = new HashMap<>();
        retur.put("data",result);
        retur.put("length",length);

        return retur;
    }

    private List<MostLoginDashboardDto> getChartDocData(Map<String, Long> all, String idUnit,
                                                        Integer skip, Integer limit, String sort, Boolean descending) {
        try {
            List<MostLoginDashboardDto> hasil = new ArrayList<>();

            for (String str : all.keySet()) {
                String loginName = str.substring(5).split(" ", 2)[0];
                MasterUser user = masterUserService.findUserByUsername(loginName);
                boolean valid = user != null && user.getKorsa() != null &&
                        !Strings.isNullOrEmpty(user.getJabatan()) && !Strings.isNullOrEmpty(user.getNameFront());

                if (!Strings.isNullOrEmpty(idUnit) && valid) {
                    try {
                        valid = user.getKorsa().getId().equals(idUnit);
                    } catch (Exception e) {
                        valid = false;
                    }
                }

                if (valid) {
                    Long value = all.get(str);
                    MostLoginDashboardDto dto = new MostLoginDashboardDto();
                    dto.setValue(value);
                    dto.setJabatan(user.getJabatan());
                    dto.setUnit(user.getKorsa().getId() + " / " + user.getKorsa().getNama());
                    dto.setNama(user.getNameFront() + (Strings.isNullOrEmpty(user.getNameMiddleLast()) ? "" : " " + user.getNameMiddleLast()));
                    hasil.add(dto);
                }
            }

            if (!Strings.isNullOrEmpty(sort)) {
                Comparator<MostLoginDashboardDto> comparator =
                        sort.equalsIgnoreCase("jabatan") ?
                                Comparator.comparing(MostLoginDashboardDto::getJabatan) :
                                sort.equalsIgnoreCase("unit") ?
                                        Comparator.comparing(MostLoginDashboardDto::getUnit) :
                                        sort.equalsIgnoreCase("nama") ?
                                                Comparator.comparing(MostLoginDashboardDto::getNama) :
                                                Comparator.comparing(MostLoginDashboardDto::getValue);
                if (descending) {
                    hasil = hasil.stream().sorted(comparator.reversed()).collect(Collectors.toList());
                } else {
                    hasil = hasil.stream().sorted(comparator).collect(Collectors.toList());
                }
            } else {
                hasil = hasil.stream().sorted(Comparator.comparing(MostLoginDashboardDto::getValue).reversed()).collect(Collectors.toList());
            }

            return hasil.stream().skip(skip).limit(limit).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}