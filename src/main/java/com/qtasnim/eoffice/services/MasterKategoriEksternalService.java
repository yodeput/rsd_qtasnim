package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterKategoriEksternal;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterKategoriEksternalDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class MasterKategoriEksternalService extends AbstractFacade<MasterKategoriEksternal> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterKategoriEksternal> getEntityClass() {
        return MasterKategoriEksternal.class;
    }

    public JPAJinqStream<MasterKategoriEksternal> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterKategoriEksternal>getFiltered(String kode,String nama,Boolean isActive){
        JPAJinqStream<MasterKategoriEksternal> query = db();

        if(!Strings.isNullOrEmpty(nama)){
            query = query.where(q ->q.getNama().toLowerCase().contains(nama.toLowerCase()));
        }

        return query;
    }

    public void saveOrEdit(MasterKategoriEksternalDto dao, Long id){
        boolean isNew =id==null;
        MasterKategoriEksternal model = new MasterKategoriEksternal();
        DateUtil dateUtil = new DateUtil();

        if(isNew){
            model.setNama(dao.getNama());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }

            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.create(model);
        }else{
            model = this.find(id);
            model.setNama(dao.getNama());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }

            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }
    }
}
