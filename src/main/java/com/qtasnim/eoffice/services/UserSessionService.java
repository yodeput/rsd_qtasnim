package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.UserSession;
import com.qtasnim.eoffice.security.ForbiddenException;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class UserSessionService extends AbstractFacade<UserSession> {
    private static final long serialVersionUID = -2368314413077813254L;

    @Inject
    private JWTService jwtService;

    @Inject
    private ApplicationContext applicationContext;

    @Override
    protected Class<UserSession> getEntityClass() {
        return UserSession.class;
    }

    public UserSession findByToken(String token) {
        Date now = new Date();

        return db()
                .joinFetch(t -> JinqStream.of(t.getUser()))
                .where(t -> t.getToken().equals(token) && t.getTokenValidUntil().after(now))
                .findFirst().orElse(null);
    }

    public UserSession findValidRefreshToken(String token) {
        Date now = new Date();

        return db()
                .where(t -> t.getRefreshToken().equals(token) && t.getRefreshTokenValidUntil().after(now))
                .findFirst().orElse(null);
    }

    public List<UserSession> getActiveSession() {
        Date now = new Date();

        return db()
                .where(t -> t.getTokenValidUntil().after(now))
                .toList();
    }

    @Asynchronous
    public void refreshToken(UserSession userSession) throws ForbiddenException {
        jwtService.refresh(userSession, applicationContext.getApplicationConfig());

        edit(userSession);
    }
}