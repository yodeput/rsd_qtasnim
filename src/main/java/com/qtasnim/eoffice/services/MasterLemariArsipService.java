package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterLemariArsipDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterLemariArsipService extends AbstractFacade<MasterLemariArsip> {
    private static final long serialVersionUID = 1L;
    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterRuangArsipService ruangArsipService;

    @Inject
    private MasterAutoNumberService autoNumberService;

    @Override
    protected Class<MasterLemariArsip> getEntityClass() {
        return MasterLemariArsip.class;
    }

    public JPAJinqStream<MasterLemariArsip> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }


    public JPAJinqStream<MasterLemariArsip> getFiltered(String name) {
        return db().where(q -> q.getJenisLemari().toLowerCase().contains(name));
    }


    public MasterLemariArsip save(Long id, MasterLemariArsipDto dao) throws Exception {
        MasterLemariArsip model = new MasterLemariArsip();
        try {
            if (id == null) {
                this.create(data(model, dao));
            } else {
                model = this.find(id);
                dao.setId(id);
                this.edit(data(model, dao));
            }
            return model;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterLemariArsip data(MasterLemariArsip masterLemariArsip, MasterLemariArsipDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterLemariArsip model = masterLemariArsip;
        model.setId(dao.getId());
        model.setJenisLemari(JenisLemari.valueOf(dao.getJenisLemari().toUpperCase()).toString());
        if (dao.getIdRuangArsip() != null) {
            MasterRuangArsip ruangArsip = ruangArsipService.find(dao.getIdRuangArsip());
            model.setRuangArsip(ruangArsip);
        } else {
            model.setRuangArsip(null);
        }
        if (dao.getCompanyId() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }
        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        if(dao.getId()==null) {
            String kode = autoNumberService.from(MasterLemariArsip.class).next(t -> t.getKode(), model);
            model.setKode(kode);
        }
        return model;
    }

    public MasterLemariArsip delete(Long id) {
        MasterLemariArsip model = this.find(id);
        this.remove(model);
        return model;
    }

}