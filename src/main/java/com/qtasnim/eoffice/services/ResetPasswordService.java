package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.ForgotPassword;
import com.qtasnim.eoffice.ws.dto.ForgotPasswordDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class ResetPasswordService extends AbstractFacade<ForgotPassword>{

    @Override
    protected Class<ForgotPassword> getEntityClass() {
        return ForgotPassword.class;
    }

    public ForgotPasswordDto getDataByUUID(String uuid){
        return db().where(q->q.getUuid().toLowerCase().equals(uuid.toLowerCase()) && q.getIsValid()).map(q->new ForgotPasswordDto(q))
                .findFirst().orElse(null);
    }

    public void destroyValidate(ForgotPasswordDto dao){
        ForgotPassword obj = this.find(dao.getId());
        obj.setIsValid(false);
        this.edit(obj);
    }
}
