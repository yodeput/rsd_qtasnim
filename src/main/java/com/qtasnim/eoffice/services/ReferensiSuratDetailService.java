package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.ReferensiSuratDetailDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class ReferensiSuratDetailService extends AbstractFacade<ReferensiSuratDetail> {

    
    @Inject
    private SuratService suratService;

     @Inject
    private RegistrasiDokumenEksternalService dokumenEksternalService;

    @Inject
    private ReferensiSuratService referensiSuratService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Override
    protected Class<ReferensiSuratDetail> getEntityClass() {
        return ReferensiSuratDetail.class;
    }

    public ReferensiSuratDetail save(Long id, ReferensiSuratDetailDto dao){
        ReferensiSuratDetail model = new ReferensiSuratDetail();
        DateUtil dateUtil = new DateUtil();

        if(id==null) {
            if (dao.getSuratId() != null){
                Surat surat = suratService.find(dao.getSuratId());
                model.setSurat(surat);
            } else {
                model.setSurat(null);
            }
            if (dao.getRefId() != null){
                ReferensiSurat referensiSurat = referensiSuratService.find(dao.getRefId());
                model.setReferensiSurat(referensiSurat);
            } else {
                model.setReferensiSurat(null);
            }
            this.create(model);
        } else {
            model = find(id);
            if (dao.getSuratId() != null){
                Surat surat = suratService.find(dao.getSuratId());
                model.setSurat(surat);
            } else {
                model.setSurat(null);
            }
            if (dao.getRefId() != null){
                ReferensiSurat referensiSurat = referensiSuratService.find(dao.getRefId());
                model.setReferensiSurat(referensiSurat);
            } else {
                model.setReferensiSurat(null);
            }
            this.edit(model);
        }
        return model;
    }

    public JPAJinqStream<ReferensiSuratDetail> getBySurat(Long id) {
        Surat surat = suratService.find(id);

        return db().where(q -> q.getSurat().equals(surat));
    }

    public JPAJinqStream<ReferensiSuratDetail> getBySuratRef(Long id) {
        return db().where(q -> q.getReferensiSurat().getSurat().getId().equals(id));
    }

    public JPAJinqStream<ReferensiSuratDetail> getByRef(Long id) {
        ReferensiSurat referensiSurat = referensiSuratService.find(id);
        return db().where(q -> q.getReferensiSurat().equals(referensiSurat));
    }

    public ReferensiSuratDetail getByIdSurat(Long idDetail,Long idSurat){
        return db().where(a->a.getId().equals(idDetail) && a.getSurat().getId().equals(idSurat)).findFirst().orElse(null);
    }
}
