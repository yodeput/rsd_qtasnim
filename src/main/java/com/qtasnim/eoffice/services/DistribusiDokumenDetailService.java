package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@LocalBean
public class DistribusiDokumenDetailService extends AbstractFacade<DistribusiDokumenDetail> {

    @Override
    protected Class<DistribusiDokumenDetail> getEntityClass() {
        return DistribusiDokumenDetail.class;
    }

    public JPAJinqStream<DistribusiDokumenDetail> getAll() { return db(); }

    public List<DistribusiDokumenDetail> getDetails(Long idDistribusi){
        return db().where(a->a.getDistribusi().getId().equals(idDistribusi)).toList();
    }
}
