package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterJenisRegulasi;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterJenisRegulasiDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class MasterJenisRegulasiService extends AbstractFacade<MasterJenisRegulasi> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterJenisRegulasi> getEntityClass() {
        return MasterJenisRegulasi.class;
    }

    public JPAJinqStream<MasterJenisRegulasi> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterJenisRegulasi>getFiltered(String nama){
        JPAJinqStream<MasterJenisRegulasi> query = db();

        if(!Strings.isNullOrEmpty(nama)){
            query = query.where(q ->q.getNamaJenisRegulasi().toLowerCase().contains(nama.toLowerCase()));
        }

        return query;
    }

    public void saveOrEdit(MasterJenisRegulasiDto dao, Long id){
        boolean isNew =id==null;
        MasterJenisRegulasi model = new MasterJenisRegulasi();
        DateUtil dateUtil = new DateUtil();

        if(isNew){
            model.setNamaJenisRegulasi(dao.getNamaJenisRegulasi());
            model.setDeskripsiJenisRegulasi(dao.getDeskripsiJenisRegulasi());
            model.setSeq(dao.getSeq());
            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.create(model);
        }else{
            model = this.find(id);
            model.setNamaJenisRegulasi(dao.getNamaJenisRegulasi());
            model.setDeskripsiJenisRegulasi(dao.getDeskripsiJenisRegulasi());
            model.setSeq(dao.getSeq());
            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }

            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }
    }
}
