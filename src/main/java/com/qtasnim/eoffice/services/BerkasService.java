package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.helpers.FileUtils;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.BerkasDto;
import com.qtasnim.eoffice.ws.dto.ScanDocumentDto;
import com.qtasnim.eoffice.ws.dto.ScanResultDto;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class BerkasService extends AbstractFacade<Berkas> {
    private static final long serialVersionUID = 1L;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    CompanyCodeService companyCodeService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private MasterKlasifikasiMasalahService klasifikasiMasalahService;

    @Inject
    private MasterStatusAkhirArsipService akhirArsipService;

    @Inject
    private ArsipService arsipService;

    @Inject
    private MasterKorsaService korsaService;

    @Inject
    private FileService fileService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private MasterFolderKegiatanService folderKegiatanService;

    @Inject
    private MasterRuangArsipService ruangArsipService;

    @Inject
    private MasterLemariArsipService lemariArsipService;

    @Inject
    private MasterTrapArsipService trapArsipService;

    @Inject
    private MasterBoksArsipService boksArsipService;

    @Inject
    private MasterAutoNumberService autoNumberService;


    @Override
    protected Class<Berkas> getEntityClass() {
        return Berkas.class;
    }

    public List<Berkas> getByDate(String start, String end, String filter, String sort, boolean descending, int skip, int limit) {
        DateUtil dateUtil = new DateUtil();
        Date startKurun = dateUtil.getDateFromISOString(start);
        Date endKurun = dateUtil.getDateFromISOString(end);
        List<Berkas> listFilter = this.getResultList(filter, sort, descending, skip, limit);
        List<Berkas> listFilterByDate = db().where(q -> !q.getIsDeleted() && q.getStartKurun().after(startKurun) && q.getEndKurun().before(endKurun)).toList();
        listFilter.retainAll(listFilterByDate);
        return listFilter;
    }

    public JPAJinqStream<Berkas> getFiltered(String name) {
        return db().where(q -> q.getJudul().toLowerCase().contains(name));
    }

    public List<Berkas> getByUnit(String idUnit) {
        return db().where(q -> q.getUnit().getId().equals(idUnit)).collect(Collectors.toList());
    }

    public Berkas save(Long id, BerkasDto dao) throws Exception {
        Berkas model = new Berkas();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
            String now = sdf.format(new Date());
            if (id == null) {
                List<Berkas> old = getByUnit(dao.getIdUnit());
                if (!old.isEmpty()) {
                    Berkas find = old.stream().filter(q -> q.getIsDeleted().booleanValue() == false &&
                            dao.getIdMasalah().equals(q.getKlasifikasiMasalah().getIdKlasifikasiMasalah()) &&
                                    dao.getJudul().equals(q.getJudul()) && sdf.format(q.getCreatedDate()).equals(now)).findFirst().orElse(null);
                    if (find != null) {
                        throw new Exception(String.format("Sudah terdapat Berkas '%s' dengan klasifikasi %s pada unit %s",
                                dao.getJudul(), find.getKlasifikasiMasalah().getNamaKlasifikasiMasalah(), find.getUnit().getNama()));
                    }
                }

                this.create(data(model, dao));
            } else {
                List<Berkas> old = getByUnit(dao.getIdUnit());
                if (!old.isEmpty()) {
                    Berkas find = old.stream().filter(q -> !q.getId().equals(id) && q.getIsDeleted().booleanValue() == false &&
                            dao.getIdMasalah().equals(q.getKlasifikasiMasalah().getIdKlasifikasiMasalah()) &&
                                    dao.getJudul().equals(q.getJudul()) && sdf.format(q.getCreatedDate()).equals(now)).findFirst().orElse(null);
                    if (find != null) {
                        throw new Exception(String.format("Sudah terdapat Berkas '%s' dengan klasifikasi %s pada unit %s",
                                dao.getJudul(), find.getKlasifikasiMasalah().getNamaKlasifikasiMasalah(), find.getUnit().getNama()));
                    }
                }

                model = this.find(id);
                this.edit(data(model, dao));
            }
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }

        return model;
    }

    private Berkas data(Berkas berkas, BerkasDto dao) {
        Berkas model = berkas;
        DateUtil dateUtil = new DateUtil();

        model.setJudul(dao.getJudul());
        model.setLokasi(dao.getLokasi());
        model.setDeskripsi(dao.getDeskripsi());
        model.setIsAktif(dao.getIsAktif());
        model.setJenis(dao.getJenis());
        model.setIsDeleted(false);
        model.setStatus("AKTIF");

        //status default value is 1
        model.setStatus(dao.getStatus());
        //MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dao.getIdOrgUnit());
        //model.setOrganisasi(org);

        if (dao.getIdUnit() != null) {
            MasterKorsa korsa = korsaService.find(dao.getIdUnit());
            model.setUnit(korsa);
        } else {
            model.setUnit(null);
        }

        if (dao.getIdCompany() != null) {
            CompanyCode object = companyCodeService.find(dao.getIdCompany());
            model.setCompanyCode(object);
        } else {
            model.setCompanyCode(null);
        }

        if (dao.getIdMasalah() != null) {
            MasterKlasifikasiMasalah object = klasifikasiMasalahService.find(dao.getIdMasalah());
            model.setKlasifikasiMasalah(object);
        } else {
            model.setKlasifikasiMasalah(null);
        }

        if (dao.getIdPenyusutan() != null) {
            MasterStatusAkhirArsip object = akhirArsipService.find(dao.getIdPenyusutan());
            model.setStatusAkhirArsip(object);
        } else {
            model.setStatusAkhirArsip(null);
        }
        if (dao.getRencanaMusnahDate() != null) {
            model.setRencanaMusnahDate(dateUtil.getDateFromISOString(dao.getRencanaMusnahDate()));
        } else {
            model.setRencanaMusnahDate(null);
        }
        if (dao.getTerakhirAksesDate() != null) {
            model.setTerakhirAksesDate(dateUtil.getDateFromISOString(dao.getTerakhirAksesDate()));
        } else {
            model.setTerakhirAksesDate(null);
        }

        if (dao.getIdFolder() != null) {
            MasterFolderKegiatan object = folderKegiatanService.find(dao.getIdFolder());
            model.setFolderKegiatan(object);
        } else {
            model.setFolderKegiatan(null);
        }

        model.setProgress(dao.getProgress());
        model.setBiaya(dao.getBiaya());
        model.setFolderName(dao.getFolderName());

        if (dao.getIdRuangArsip() != null) {
            MasterRuangArsip object = ruangArsipService.find(dao.getIdRuangArsip());
            model.setRuangArsip(object);
        } else {
            model.setRuangArsip(null);
        }
        if (dao.getIdLemariArsip() != null) {
            MasterLemariArsip object = lemariArsipService.find(dao.getIdLemariArsip());
            model.setLemariArsip(object);
        } else {
            model.setLemariArsip(null);
        }
        if (dao.getIdTrapArsip() != null) {
            MasterTrapArsip object = trapArsipService.find(dao.getIdTrapArsip());
            model.setTrapArsip(object);
        } else {
            model.setTrapArsip(null);
        }
        if (dao.getIdBoksArsip() != null) {
            MasterBoksArsip object = boksArsipService.find(dao.getIdBoksArsip());
            model.setBoksArsip(object);
        } else {
            model.setBoksArsip(null);
        }

        if (!Strings.isNullOrEmpty(dao.getJraAktif())) {
            String tglAktif = dateUtil.getCurrentISODate(model.getJraAktif());
            if (!tglAktif.equals(dao.getJraAktif())) {
                tglAktif = dao.getJraAktif();
                model.setJraAktif(dateUtil.getDateFromISOString(tglAktif));
            }
        }

        if (!Strings.isNullOrEmpty(dao.getJraInaktif())) {
            String tglInaktif = dateUtil.getCurrentISODate(model.getJraInaktif());
            if (!tglInaktif.equals(dao.getJraInaktif())) {
                tglInaktif = dao.getJraInaktif();
                model.setJraInaktif(dateUtil.getDateFromISOString(tglInaktif));
            }
        }

        if(model.getId() == null) {
            String nomor = autoNumberService.from(Berkas.class).next(t -> t.getNomor(), model);
            model.setNomor(nomor);
        }

        return model;
    }

    // this to be fixed --> jra inaktif diambil dari tgl jra aktif + 1 hari + tahun dari klasifikasi
    public BerkasDto closeBerkas(Long id) throws Exception {
        try {
            Date date = new Date();
            Berkas model = this.find(id);
            MasterKlasifikasiMasalah klasifikasiMasalah = klasifikasiMasalahService.find(model.getKlasifikasiMasalah().getIdKlasifikasiMasalah());
            List<Arsip> arsips = arsipService.getByBerkas(id).toList();

            if(arsips.isEmpty()){
                throw new Exception("Berkas kosong");
            }else {
                int int_aktif = klasifikasiMasalah.getRetensiAktifYear();
                Calendar cal_aktif = Calendar.getInstance();
                cal_aktif.setTime(date);
                cal_aktif.add(Calendar.YEAR, int_aktif);
                model.setJraAktif(cal_aktif.getTime());

                int int_inaktif = klasifikasiMasalah.getRetensiInaktifYear();
                Calendar cal_inaktif = Calendar.getInstance();
                cal_inaktif.setTime(cal_aktif.getTime());
                cal_inaktif.add(Calendar.YEAR, int_inaktif);
                cal_inaktif.add(Calendar.DATE, 1);
                model.setJraInaktif(cal_inaktif.getTime());

                model.setClosedDate(date);
                //model.setIsAktif(false); --> revisi Close Berkas status tetap aktif
                this.edit(model);

                arsips.forEach((arsip) -> {
                    MasterKlasifikasiMasalah masalah = klasifikasiMasalahService.find(arsip.getKlasifikasiMasalah().getIdKlasifikasiMasalah());
                    if (masalah != null) {
                        Calendar c_aktif = Calendar.getInstance();
                        c_aktif.setTime(date);
                        c_aktif.add(Calendar.YEAR, masalah.getRetensiAktifYear());
                        arsip.setJraAktif(c_aktif.getTime());

                        Calendar c_inaktif = Calendar.getInstance();
                        c_inaktif.setTime(c_aktif.getTime());
                        c_inaktif.add(Calendar.YEAR, masalah.getRetensiInaktifYear());
                        c_inaktif.add(Calendar.DATE, 1);
                        arsip.setJraInaktif(c_inaktif.getTime());

                        arsipService.edit(arsip);
                    }
                });
                return new BerkasDto(model);
            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public List<Berkas> getCloseBerkas(String filter, String sort, boolean descending, int skip, int limit) {
        String strFilter = String.format("isDeleted==%s and closedDate!=%s and isAktif==%s", false, null, true);

        if (!Strings.isNullOrEmpty(filter)) {
            strFilter = String.format("%s and %s", strFilter, filter);
        }

        List<Berkas> listFilter = this.getResultList(strFilter, sort, descending, skip, limit);

        return listFilter;
    }

    public Integer getCountBerkasBelumClose() {
        MasterKorsa korsa = userSession.getUserSession().getUser().getKorsa();
        List<Berkas>  listFilter = db().where(q -> !q.getIsDeleted() && q.getClosedDate() == null && q.getUnit().equals(korsa)).toList();
        return listFilter.size();
    }

    public BerkasDto setInActive(Long id) throws Exception{
        try {
            Berkas model = this.find(id);
            List<Arsip> arsips = arsipService.getByBerkas(id).toList();

            if(arsips.isEmpty()){
                throw new Exception("Berkas kosong");
            }else {
                DateUtil dateUtil = new DateUtil();
                model.setIsAktif(false);
                model.setInaktifDate(new Date());
                model.setStatus("INAKTIF");
                this.edit(model);
                return new BerkasDto(model);
            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public List<Berkas> getInactiveBerkas(String nomor, String judul, String filter, String sort, boolean descending, int skip, int limit) {
        List<Berkas> listFilter = this.getResultList(filter, sort, descending, skip, limit);
        List<Berkas> listFilterByDate = db().where(q -> !q.getIsDeleted() && !q.getStatus().toUpperCase().equals("STATIS") && q.getClosedDate() != null && q.getIsAktif() == false && q.getNomor().contains(nomor) && q.getJudul().contains(judul) && q.getInaktifDate() != null).toList();
        listFilter.retainAll(listFilterByDate);
        return listFilter;
    }

    public List<Berkas> getBerkasMusnah(String nomor, String judul, String filter, String sort, boolean descending, int skip, int limit) {
        Date n = new Date();
        List<Berkas> listFilter = this.getResultList(filter, sort, descending, skip, limit);
        List<Berkas> listFilterByDate = db().where(q -> !q.getIsDeleted() && n.after(q.getInaktifDate()) && q.getNomor().contains(nomor) && q.getJudul().contains(judul) && q.getStatus().equals("MUSNAH")).toList();
        listFilter.retainAll(listFilterByDate);
        return listFilter;
    }

    public Berkas deleteMusnah(Long id) {
        List<Arsip> arsipList = arsipService.getByBerkas(id).collect(Collectors.toList());
        for (Arsip arsip : arsipList) {
            globalAttachmentService.deleteMusnahArsip(arsip.getId());
            arsipService.remove(arsip);
        }
        Berkas model = this.find(id);
        this.remove(model);
        return model;
    }

    public Berkas setStatusAkhirArsipStatis(Long id) {
        MasterStatusAkhirArsip statusAkhirArsip = akhirArsipService.getFiltered("STATIS").getOnlyValue();
        Berkas model = this.find(id);
        model.setStatusAkhirArsip(statusAkhirArsip);
        this.edit(model);
        return model;
    }

    public Berkas setStatis(Long id) {
        Berkas model = this.find(id);
        model.setStatus("STATIS");
        this.edit(model);
        return model;
    }

    public Berkas getByIdFolderKegiatan(Long idFolderKegiatan) {
        MasterFolderKegiatan folder = folderKegiatanService.find(idFolderKegiatan);
        return db().where(a -> !a.getIsDeleted() && a.getFolderKegiatan().equals(folder)).findOne().orElse(null);
    }

    public JPAJinqStream<Berkas> getByFolder(Long idFolder) {
        MasterFolderKegiatan folder = folderKegiatanService.find(idFolder);
        return db().where(a -> !a.getIsDeleted() && a.getFolderKegiatan().equals(folder) && !a.getStatus().toUpperCase().equals("STATIS"));
    }

    public Berkas updateLastAccess(Long id) {
        Berkas model = this.find(id);
        model.setTerakhirAksesDate(new Date());
        this.edit(model);
        return model;
    }

    public Berkas deleteBerkas(Long id) throws Exception {
        Berkas berkas = this.find(id);
        try{
            if (berkas.getArsipList() != null && !berkas.getArsipList().isEmpty()) {
                /*tidak boleh delete arsip & file*/
            /*for (Arsip ar : berkas.getArsipList()) {
                List<GlobalAttachment> files = globalAttachmentService.getAttachmentDataByRef("t_arsip", ar.getId()).collect(Collectors.toList());
                for (GlobalAttachment ga : files) {
                    ga.setIsDeleted(true);
                    globalAttachmentService.edit(ga);
                }
                ar.setIsDeleted(true);
                arsipService.edit(ar);
            }*/
                throw new Exception("Folder sudah digunakan.");
            }
            else {
                berkas.setIsDeleted(true);
                this.edit(berkas);
            }
        }catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }

        return berkas;
    }

    public HashMap<String, Object> getAll(String filter, String sort, Boolean descending, int skip, int limit) {
        List<BerkasDto> result = new ArrayList<>();

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Berkas> q = cb.createQuery(Berkas.class);
        Root<Berkas> root = q.from(Berkas.class);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted")),
                cb.isNull(root.get("closedDate")),
                cb.isTrue(root.get("isAktif"))
        );

        if (!Strings.isNullOrEmpty(filter)) {
            Predicate pkeyword =
                    cb.or(
                            cb.like(root.get("nomor"), "%" + filter + "%"),
                            cb.like(root.get("judul"), "%" + filter + "%")
                    );

            predicates.add(pkeyword);
        }

        // WHERE CONDITION
        predicates.add(mainCond);

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);

        if (descending != null && !Strings.isNullOrEmpty(sort)) {
            if (descending) {
                q.orderBy(cb.desc(root.get(sort)));
            } else {
                q.orderBy(cb.asc(root.get(sort)));
            }
        }

        TypedQuery<Berkas> all = getEntityManager().createQuery(q).setFirstResult(skip).setMaxResults(limit);
        result = MappingToDto(all);

        Long length = all.getResultStream().count();

        HashMap<String, Object> sum = new HashMap<String, Object>();
        sum.put("data", result);
        sum.put("length", length);
        return sum;
    }

    private List<BerkasDto> MappingToDto(TypedQuery<Berkas> all) {
        List<BerkasDto> result = null;

        if (all != null) {
            result = all.getResultStream()
                    .map(BerkasDto::new).collect(Collectors.toList());
        }
        return result;
    }

    public void uploadAttachment(Long idBerkas, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";
        try {
            String filename = formDataContentDisposition.getFileName();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is2 = new ByteArrayInputStream(baos.toByteArray());

            byte[] documentContent = IOUtils.toByteArray(is2);

            Berkas berkas = this.find(idBerkas);

//            String number = autoNumberService.from(SuratDocLib.class).next(SuratDocLib::getNumber);
            List<String> extractedText = textExtractionService.parsingFile(is1, formDataContentDisposition, body).getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            docId = fileService.upload(filename, documentContent);
            docId = docId.replace("workspace://SpacesStore/", "");

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString().concat(FileUtility.GetFileExtension(filename)));
            obj.setDocName(filename);
            obj.setCompanyCode(berkas.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setSize(new Long(documentContent.length));
            obj.setIsDeleted(false);
            obj.setReferenceId(idBerkas);
            obj.setReferenceTable("t_berkas");
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);
        } catch (Exception ex) {
            if (!StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }
            throw ex;
        }
    }

    public ScanDocumentDto scanDocument(Long idBerkas, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";

        try {
            if (idBerkas == null) {
                throw new Exception("idBerkas null");
            }

            Berkas berkas = this.find(idBerkas);

            if (berkas == null) {
                throw new Exception("Berkas tidak tersedia");
            }

            ScanResultDto scanned = textExtractionService.parsingFileFromScan(inputStream, formDataContentDisposition, body);

            if (scanned.getPdf() == null) {
                throw new Exception("scanned source not tiff nor pdf / pdf unrecognized");
            }

            byte[] pdf = scanned.getPdf();

            docId = fileService.upload("scanned.pdf", pdf);
            docId = docId.replace("workspace://SpacesStore/", "");

            List<String> extractedText = scanned.getText().getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType("application/pdf");
            obj.setDocGeneratedName(UUID.randomUUID().toString().concat(".pdf"));
            obj.setDocName(FileUtils.getFileName(formDataContentDisposition.getFileName()).concat(".pdf"));
            obj.setCompanyCode(berkas.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setIsDeleted(false);
            obj.setSize(new Long(pdf.length));
            obj.setReferenceTable("t_berkas");
            obj.setReferenceId(idBerkas);
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);

            return new ScanDocumentDto(scanned.getText(), docId);
        } catch (Exception ex) {
            if (!StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }

            throw ex;
        }
    }

    public GlobalAttachment deleteFile(Long id) {
        GlobalAttachment global = globalAttachmentService.find(id);

        if (global.getReferenceTable().toLowerCase().equals("t_berkas")) {
            global.setIsDeleted(true);
            globalAttachmentService.edit(global);
        }

        return global;
    }
}