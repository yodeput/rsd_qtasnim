package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterTrapArsip;
import com.qtasnim.eoffice.db.MasterLemariArsip;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterTrapArsipDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterTrapArsipService extends AbstractFacade<MasterTrapArsip> {
    private static final long serialVersionUID = 1L;
    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterLemariArsipService lemariArsipService;

    @Inject
    private MasterAutoNumberService autoNumberService;

    @Override
    protected Class<MasterTrapArsip> getEntityClass() {
        return MasterTrapArsip.class;
    }

    public JPAJinqStream<MasterTrapArsip> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public MasterTrapArsip save(Long id, MasterTrapArsipDto dao) throws Exception {
        MasterTrapArsip model = new MasterTrapArsip();
        try {
            if (id == null) {
                this.create(data(model, dao));
            } else {
                model = this.find(id);
                dao.setId(id);
                this.edit(data(model, dao));
            }
            return model;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public MasterTrapArsip data(MasterTrapArsip masterTrapArsip, MasterTrapArsipDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterTrapArsip model = masterTrapArsip;
        model.setId(dao.getId());
        if (dao.getIdLemariArsip() != null) {
            MasterLemariArsip lemariArsip = lemariArsipService.find(dao.getIdLemariArsip());
            model.setLemariArsip(lemariArsip);
        } else {
            model.setLemariArsip(null);
        }
        if (dao.getCompanyId() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }
        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        if(dao.getId()==null) {
            String kode = autoNumberService.from(MasterTrapArsip.class).next(t -> t.getKode(), model);
            model.setKode(kode);
        }
        return model;
    }

    public MasterTrapArsip delete(Long id) {
        MasterTrapArsip model = this.find(id);
        this.remove(model);
        return model;
    }

}