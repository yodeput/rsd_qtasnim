package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.BoardDto;
import com.qtasnim.eoffice.ws.dto.CardMemberDto;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class CardMemberService extends AbstractFacade<CardMember>{

    @Override
    protected Class<CardMember> getEntityClass() {
        return CardMember.class;
    }

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyService;

    @Inject
    private BoardService boardService;

    @Inject
    private CardService cardService;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    @Inject
    private MasterUserService userService;

    public JPAJinqStream<CardMember> getAll(){
        Long idCompany = userSession.getUserSession().getUser().getCompanyCode().getId();
        CompanyCode company = companyService.find(idCompany);
        return db().where(a->a.getIsDeleted().booleanValue()==false && a.getCompanyCode().equals(company));
    }

    public CardMember saveOrEdit(CardMemberDto dao, Long id) throws Exception {
        try{
            CardMember obj = new CardMember();
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = companyService.getByCode(user.getCompanyCode().getCode());
            if(id==null){
                obj.setIsDeleted(false);
                if(dao.getBoardId()!=null){
                    Board board = boardService.find(dao.getBoardId());
                    obj.setBoard(board);
                }
                if(dao.getCardId()!=null){
                    Card card = cardService.find(dao.getCardId());
                    obj.setCard(card);
                }
                obj.setCompanyCode(company);
                if(dao.getOrganizationId()!=null){
                    MasterStrukturOrganisasi org = organisasiService.find(dao.getOrganizationId());
                    MasterUser usr = userService.find(org.getUser().getId());
                    obj.setOrganization(org);
                    obj.setUser(usr);
                }
                create(obj);
            }else{
                obj = find(id);
                obj.setIsDeleted(false);
                if(dao.getBoardId()!=null){
                    Board board = boardService.find(dao.getBoardId());
                    obj.setBoard(board);
                }
                if(dao.getCardId()!=null){
                    Card card = cardService.find(dao.getCardId());
                    obj.setCard(card);
                }
                obj.setCompanyCode(company);
                if(dao.getOrganizationId()!=null){
                    MasterStrukturOrganisasi org = organisasiService.find(dao.getOrganizationId());
                    MasterUser usr = userService.find(org.getUser().getId());
                    obj.setOrganization(org);
                    obj.setUser(usr);
                }
                edit(obj);
            }
            getEntityManager().flush();
            getEntityManager().refresh(obj);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public CardMember deleteMember(Long id) throws Exception {
        try{
            CardMember obj = find(id);
            obj.setIsDeleted(true);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
    
    public CardMember getByCardAndOrganizationId(Long cardId, Long idOrganization) throws Exception {
        return db().where(q -> q.getCard().getId().equals(cardId) && q.getOrganization().getIdOrganization().equals(idOrganization)).findFirst().orElse(null);
    }
    
    public JPAJinqStream<Board> getByOrganizationId(Long idOrganization, Long idUser){
        JPAJinqStream<Board> query =  db().where(a-> (a.getOrganization().getIdOrganization()
                .equals(idOrganization) || a.getUser().getId().equals(idUser)) && !a.getIsDeleted()
                && !a.getBoard().getIsDeleted()).select(c->c.getBoard());
        return query;
    }
}
