package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.CompanyCodeDto;
import com.qtasnim.eoffice.ws.dto.MasterDelegasiDto;
import com.qtasnim.eoffice.ws.dto.MasterDelegasiTujuanDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service untuk fungsi CRUD table m_user
 *
 * @author seno@qtasnim.com
 */


@Stateless
@LocalBean
public class MasterDelegasiService extends AbstractFacade<MasterDelegasi> {

    private static final long serialVersionUID = -4184209743573831159L;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterDelegasi> getEntityClass() {
        return MasterDelegasi.class;
    }

    public JPAJinqStream<MasterDelegasi> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getMulai()) && n.before(q.getSelesai()) && !q.getIsDeleted());
    }

    public List<MasterDelegasi> getByStartEnd(MasterDelegasiDto delegasi) {
        Date n = new Date();
        DateUtil dateUtil = new DateUtil();
        Long idFrom = delegasi.getFrom().getOrganizationId();
        Date selesai = dateUtil.getDateTimeEnd(delegasi.getSelesai());
        return db().where(q -> q.getSelesai().equals(selesai) && q.getFrom().getIdOrganization().equals(idFrom) && !q.getIsDeleted()).collect(Collectors.toList());
    }

    public MasterDelegasiDto getByFromTipe(MasterDelegasiDto delegasi) {
        Date n = new Date();
        DateUtil dateUtil = new DateUtil();
        Long idFrom = delegasi.getFrom().getOrganizationId();
        Date selesai = dateUtil.getDateTimeEnd(delegasi.getSelesai());
        String tipe = delegasi.getTipe();
        return db().where(q -> q.getSelesai().equals(selesai) && q.getFrom().getIdOrganization().equals(idFrom) && !q.getTipe().equals(tipe) && !q.getIsDeleted()).findOne().map(MasterDelegasiDto::new).orElse(null);
    }

    public MasterDelegasi getByTo(String tipe) {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        Date n = new Date();
        return db().where(q -> (n.after(q.getMulai()) && n.before(q.getSelesai())) && !q.getIsDeleted() && q.getTo().equals(org) && q.getTipe().toLowerCase().equals(tipe.toLowerCase())).findFirst().orElse(null);
    }

    public List<MasterDelegasi> getByToNoTipe() {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        Date n = new Date();
        return db().where(q -> (n.after(q.getMulai()) && n.before(q.getSelesai())) && !q.getIsDeleted() && q.getTo().equals(org)).collect(Collectors.toList());
    }

    public MasterDelegasi getByIdOrg(Long id) {
        MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(id);
        Date n = new Date();
        return db().where(q -> (n.after(q.getMulai()) && n.before(q.getSelesai())) && !q.getIsDeleted() && q.getTo().equals(org)).findFirst().orElse(null);
    }

    public MasterDelegasi getByToOrgCode(String orgCode, String tipe) {
        Date date = new Date();

        return db().where(t ->
                orgCode.contains(t.getFrom().getOrganizationCode())
                        && t.getMulai().before(date)
                        && date.before(t.getSelesai()) && !t.getIsDeleted() && t.getTipe().equals(tipe)).findFirst().orElse(null);
    }

    public JPAJinqStream<MasterDelegasi> getFiltered(Long id, Long fromId, Long toId) {
        JPAJinqStream<MasterDelegasi> query = db();
        if (id != null) {
            query = query.where(q -> q.getId().equals(id));
        }
        if (fromId != null) {
            query = query.where(q -> q.getFrom().getIdOrganization().equals(fromId));
        }
        if (toId != null) {
            query = query.where(q -> q.getTo().getIdOrganization().equals(toId));
        }

        return query;
    }

    public MasterDelegasi save(Long id, MasterDelegasiDto dao) {
        DateUtil dateUtil = new DateUtil();
        List<MasterDelegasiTujuanDto> tujuanList = dao.getTujuan();
        MasterDelegasi model = new MasterDelegasi();
        if (id == null) {
            for (MasterDelegasiTujuanDto tujuan : tujuanList) {
                model = new MasterDelegasi();
                MasterStrukturOrganisasi mOrgFrom = masterStrukturOrganisasiService.find(dao.getOrganisasi_from());
                model.setFrom(mOrgFrom);
                model.setAlasan(dao.getAlasan());
                model.setIsDeleted(false);
                model.setMulai(dateUtil.getDateTimeStart(dao.getMulai()));
                model.setSelesai(dateUtil.getDateTimeEnd(dao.getSelesai()));
                if (dao.getCompanyId() != null) {
                    CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                    model.setCompanyCode(cCode);
                } else {
                    model.setCompanyCode(null);
                }
                MasterStrukturOrganisasi mOrgTo = masterStrukturOrganisasiService.find(tujuan.getOrganisasi_to());
                model.setTo(mOrgTo);
                model.setTipe(tujuan.getTipe());
                this.create(model);
            }
        } else {
            for (MasterDelegasiTujuanDto tujuan : tujuanList) {
                model = this.find(tujuan.getId());
                MasterStrukturOrganisasi mOrgFrom = masterStrukturOrganisasiService.find(dao.getOrganisasi_from());
                model.setFrom(mOrgFrom);
                model.setAlasan(dao.getAlasan());
                model.setIsDeleted(false);
                model.setMulai(dateUtil.getDateTimeStart(dao.getMulai()));
                model.setSelesai(dateUtil.getDateTimeEnd(dao.getSelesai()));
                if (dao.getCompanyId() != null) {
                    CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                    model.setCompanyCode(cCode);
                } else {
                    model.setCompanyCode(null);
                }
                MasterStrukturOrganisasi mOrgTo = masterStrukturOrganisasiService.find(tujuan.getOrganisasi_to());
                model.setTo(mOrgTo);
                model.setTipe(tujuan.getTipe());
                this.edit(model);
            }
        }
        return model;
    }

    public MasterDelegasi delete(Long id) {
        MasterDelegasi model = this.find(id);
        model.setIsDeleted(true);
        this.edit(model);

        return model;
    }

    public MasterDelegasi delete2(Long id1, Long id2) {
        MasterDelegasi model1 = this.find(id1);
        MasterDelegasi model2 = this.find(id2);
        model1.setIsDeleted(true);
        model2.setIsDeleted(true);
        this.edit(model1);
        this.edit(model2);

        return model1;
    }

    public List<MasterDelegasi> getByPejabatCodeList(List<String> codes) {
        Date date = new Date();

        if (codes.size() > 0) {
            return db()
                    .where(t ->
                            codes.contains(t.getFrom().getOrganizationCode())
                                    && t.getMulai().before(date)
                                    && date.before(t.getSelesai()) && !t.getIsDeleted())
                    .toList();
        }

        return new ArrayList<>();
    }

    public List<MasterDelegasi> getByPejabatIdList(List<Long> ids) {
        Date date = new Date();

        if (ids.size() > 0) {
            return db()
                    .where(t ->
                            ids.contains(t.getFrom().getIdOrganization())
                                    && t.getMulai().before(date)
                                    && date.before(t.getSelesai()) && !t.getIsDeleted())
                    .toList();
        }

        return new ArrayList<>();
    }

    public MasterDelegasi findByPejabat(Long jabatanId) {
        Date date = new Date();

        return db()
                .where(t ->
                        t.getFrom().getIdOrganization().equals(jabatanId)
                                && t.getTipe().equals("PYMT")
                                && t.getMulai().before(date)
                                && date.before(t.getSelesai()) && !t.getIsDeleted())
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public List<MasterDelegasi> getByPenggantiIdList(List<String> codes) {
        Date date = new Date();

        if (codes.size() > 0) {
            return db()
                    .where(t ->
                            codes.contains(t.getTo().getOrganizationCode())
                                    && t.getMulai().before(date)
                                    && date.before(t.getSelesai()) && !t.getIsDeleted())
                    .toList();
        }

        return new ArrayList<>();
    }

    public String validate(Long id, MasterDelegasiDto dao) {
        String hasil = "";
        boolean isStart = false;
        boolean isEnd = false;
        boolean isBoth = false;
        DateUtil dateUtil = new DateUtil();
        Long companyId = dao.getCompanyId();
        Long idFrom = dao.getOrganisasi_from();
        Long idTo = dao.getOrganisasi_to();
        if (id == null) {
            Date startDate = dateUtil.getDateTimeStart(dao.getMulai());
            Date endDate = dateUtil.getDateTimeEnd(dao.getSelesai());
            JPAJinqStream<MasterDelegasi> tmp = db().where(a -> !a.getIsDeleted() && a.getCompanyCode().getId().equals(companyId));
            List<MasterDelegasi> match = tmp.where(b -> b.getFrom().getIdOrganization().equals(idFrom) && b.getTo().getIdOrganization().equals(idTo)).collect(Collectors.toList());
            if (!match.isEmpty()) {
                isStart = match.stream().anyMatch(q -> startDate.after(q.getMulai()) && startDate.before(q.getSelesai()) ||
                        (startDate.equals(q.getMulai()) || startDate.equals(q.getSelesai())));
                isEnd = match.stream().anyMatch(r -> endDate.after(r.getMulai()) && endDate.before(r.getSelesai()) ||
                        (endDate.equals(r.getMulai()) || endDate.equals(r.getSelesai())));
                isBoth = match.stream().anyMatch(s ->
                        (startDate.after(s.getMulai()) && endDate.before(s.getSelesai())) ||
                                (startDate.equals(s.getMulai()) && endDate.equals(s.getSelesai())) ||
                                (startDate.before(s.getMulai()) && endDate.after(s.getSelesai()))
                );
            }
        } else {
            JPAJinqStream<MasterDelegasi> tmp = db().where(a -> !a.getIsDeleted() && !a.getId().equals(id) && a.getCompanyCode().getId().equals(companyId));
            Date startDate = dateUtil.getDateTimeStart(dao.getMulai());
            Date endDate = dateUtil.getDateTimeEnd(dao.getSelesai());
            List<MasterDelegasi> match = tmp.where(b -> b.getFrom().getIdOrganization().equals(idFrom) && b.getTo().getIdOrganization().equals(idTo)).collect(Collectors.toList());
            if (!match.isEmpty()) {
                isStart = match.stream().anyMatch(q -> startDate.after(q.getMulai()) && startDate.before(q.getSelesai()) ||
                        (startDate.equals(q.getMulai()) || startDate.equals(q.getSelesai())));
                isEnd = match.stream().anyMatch(r -> endDate.after(r.getMulai()) && endDate.before(r.getSelesai()) ||
                        (endDate.equals(r.getMulai()) || endDate.equals(r.getSelesai())));
                isBoth = match.stream().anyMatch(s ->
                        (startDate.after(s.getMulai()) && endDate.before(s.getSelesai())) ||
                                (startDate.equals(s.getMulai()) && endDate.equals(s.getSelesai())) ||
                                (startDate.before(s.getMulai()) && endDate.after(s.getSelesai()))
                );
            }
        }
        hasil = isBoth ? "all" : isStart ? "start" : isEnd ? "end" : "";
        return hasil;
    }

    public Boolean isPYMT (Long idPejabat, Long idDelegasi){
        Date date = new Date();

        return db().anyMatch(t ->
                t.getFrom().getIdOrganization().equals(idPejabat)
                        && t.getTo().getIdOrganization().equals(idDelegasi)
                        && t.getTipe().equals("PYMT")
                        && t.getMulai().before(date)
                        && date.before(t.getSelesai()) && !t.getIsDeleted());
    }

    public Boolean isPelakhar (Long idPejabat, Long idDelegasi){
        Date date = new Date();

        return db().anyMatch(t ->
                t.getFrom().getIdOrganization().equals(idPejabat)
                        && t.getTo().getIdOrganization().equals(idDelegasi)
                        && t.getTipe().equals("Pelakhar")
                        && t.getMulai().before(date)
                        && date.before(t.getSelesai()) && !t.getIsDeleted());
    }
}