package com.qtasnim.eoffice.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.util.TextExtractionProcess;
import com.qtasnim.eoffice.ws.dto.ListStringDto;
import com.qtasnim.eoffice.ws.dto.ScanResultDto;
import com.qtasnim.eoffice.ws.dto.TextExtractionDto;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class TextExtractionService {
    @Inject
    private KonversiOnlyOfficeService konversiOnlyOfficeService;

    @Inject
    private OCRService ocrService;

    @Inject
    private ApplicationConfig applicationConfig;

    private List<String> allowedOfficeFormat;

    private List<String> allowedImageFormat;

    @PostConstruct
    private void postConstruct() {
        allowedOfficeFormat = Arrays.asList(applicationConfig.getFilesDocServiceConvertDocs().split("\\|"));
        allowedImageFormat = Arrays.asList(applicationConfig.getOcrAllowedImageFormat().split("\\|"));
    }

    public List<String> extractText(File file) throws Exception {
        TextExtractionProcess textExtractionProcess = new TextExtractionProcess();

        List<String> result = textExtractionProcess.extractTextV2(file);

        return result;
    }

    public TextExtractionDto parsingFile(InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        TextExtractionDto extracted = new TextExtractionDto();

        TextExtractionProcess textExtractionProcess = new TextExtractionProcess();

        String fileName = formDataContentDisposition.getFileName();
        String fileFormat = FileUtility.GetFileExtension(fileName).toLowerCase();

        List<String> texts;

        if(allowedOfficeFormat.contains(fileFormat)){
            try {
                File src;

                if(!fileFormat.equals(".pdf")) {
                    byte[] bytes = IOUtils.toByteArray(inputStream);

                    src = konversiOnlyOfficeService.convertFile(bytes, fileFormat, ".pdf");
                } else {
                    src = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString()+fileFormat);

                    FileUtils.copyInputStreamToFile(inputStream, src);
                }

                texts = textExtractionProcess.extractTextV2(src);

                extracted.setFileName(formDataContentDisposition.getFileName());
                extracted.setMimeType(body.getMediaType().toString());
                extracted.setPageCount(texts.size());
                extracted.setTexts(texts);

                Files.deleteIfExists(src.toPath());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if(allowedImageFormat.contains(fileFormat)) {
            texts = ocrService.extractText(inputStream, fileName);

            extracted.setFileName(formDataContentDisposition.getFileName());
            extracted.setMimeType(body.getMediaType().toString());
            extracted.setPageCount(texts.size());
            extracted.setTexts(texts);
        } else {
            extracted.setFileName("");
            extracted.setMimeType("");
            extracted.setPageCount(0);
            extracted.setTexts(new LinkedList<>());
        }

        return extracted;
    }

    public ScanResultDto parsingFileFromScan(InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        ScanResultDto scanResultDto = new ScanResultDto();

        TextExtractionProcess textExtractionProcess = new TextExtractionProcess();

        String fileName = formDataContentDisposition.getFileName();
        String fileFormat = FileUtility.GetFileExtension(fileName).toLowerCase();

        File src = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString()+fileFormat);

        FileUtils.copyInputStreamToFile(inputStream, src);

        List<String> texts;

        TextExtractionDto extracted = new TextExtractionDto();

        if(fileFormat.equals(".pdf")) {
            File tiff = textExtractionProcess.pdfToTiff(src);

            File pdf = textExtractionProcess.produceSearchablePdfFile(tiff);

            texts = textExtractionProcess.extractTextV2(pdf);

            extracted.setFileName(formDataContentDisposition.getFileName());
            extracted.setMimeType(body.getMediaType().toString());
            extracted.setPageCount(texts.size());
            extracted.setTexts(texts);

            scanResultDto.setPdf(IOUtils.toByteArray(pdf.toURI()));
            scanResultDto.setScannedTiff(false);

            Files.deleteIfExists(tiff.toPath());
        } else if (fileFormat.equals(".tif") || fileFormat.equals(".tiff")) {
            File pdf = textExtractionProcess.produceSearchablePdfFile(src);

            texts = textExtractionProcess.extractTextV2(pdf);

            extracted.setFileName(formDataContentDisposition.getFileName());
            extracted.setMimeType(body.getMediaType().toString());
            extracted.setPageCount(texts.size());
            extracted.setTexts(texts);

            scanResultDto.setPdf(IOUtils.toByteArray(pdf.toURI()));
        } else {
            Files.deleteIfExists(src.toPath());

            throw new Exception("scanDocument : Format file tidak didukung");
        }

        Files.deleteIfExists(src.toPath());

        scanResultDto.setText(extracted);

        return scanResultDto;
    }

    public ListStringDto getTextExtractableFiles() {
        List<String> allowed = new LinkedList(allowedOfficeFormat);

        allowed.addAll(allowedImageFormat);

        return new ListStringDto(allowed);
    }

    public String serializeExtractedText(List<String> extractedText) {
        String jsonString;

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        jsonString = gson.toJson(extractedText);

        return jsonString;
    }

    public List<String> deserializeExtractedText(String jsonString) {
        Gson gson = new Gson();

        Type listType = new TypeToken<List<String>>() {}.getType();

        List<String> extractedText;

        try {
            extractedText = gson.fromJson(jsonString, listType);
        } catch (Exception e) {
            extractedText = new LinkedList<>();
            e.printStackTrace();
        }

        return extractedText;
    }
}
