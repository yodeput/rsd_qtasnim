package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.PenandatanganPermohonanDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Stateless
@LocalBean
public class PenandatanganPermohonanService extends AbstractFacade<PenandatanganPermohonan> {
    private static final long serialVersionUID = 1L;

    @Inject
    private MasterUserService userService;

    @Inject
    private PermohonanDokumenService permohonanDokumenService;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;



    @Override
    protected Class<PenandatanganPermohonan> getEntityClass() {
        return PenandatanganPermohonan.class;
    }

    public JPAJinqStream<PenandatanganPermohonan> getAll() {
        Date n = new Date();
        return db();
    }


    public PenandatanganPermohonan save(Long id, PenandatanganPermohonanDto dao) {
        PenandatanganPermohonan model = new PenandatanganPermohonan();
        if (id == null) {
            this.create(data(model, dao));
        } else {
            model = this.find(id);
            this.edit(data(model, dao));
        }
        return model;
    }

    public PenandatanganPermohonan data(PenandatanganPermohonan penandatanganPermohonan, PenandatanganPermohonanDto dao) {

        PenandatanganPermohonan model = penandatanganPermohonan;
        model.setId(dao.getId());

        MasterUser user = userService.find(dao.getIdUser());
        model.setUser(user);

        PermohonanDokumen permohonanDokumen = permohonanDokumenService.find(dao.getIdPermohonan());
        model.setPermohonan(permohonanDokumen);

        MasterStrukturOrganisasi org = organisasiService.find(dao.getIdOrganisasi());
        model.setOrganisasi(org);

        return model;
    }

    public PenandatanganPermohonan delete(Long id) {
        PenandatanganPermohonan model = this.find(id);
        this.remove(model);
        return model;
    }

    public List<PermohonanDokumen> getByIdOrganization(List<MasterStrukturOrganisasi> lists){
        return db().where(t-> lists.contains(t.getOrganisasi()) && t.getPermohonan().getStatus().equals("SUBMITTED")).select(u->u.getPermohonan()).collect(Collectors.toList());
    }
}