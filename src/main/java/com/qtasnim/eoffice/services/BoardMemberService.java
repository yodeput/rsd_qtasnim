package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.BoardMemberDto;
import com.qtasnim.eoffice.ws.dto.CardMemberDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class BoardMemberService extends AbstractFacade<BoardMember>{

    @Override
    protected Class<BoardMember> getEntityClass() {
        return BoardMember.class;
    }

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyService;

    @Inject
    private BoardService boardService;

    @Inject
    private CardService cardService;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    @Inject
    private MasterUserService userService;

    @Inject
    private MasterKorsaService unitService;

    public JPAJinqStream<BoardMember> getAll(){
        Long idCompany = userSession.getUserSession().getUser().getCompanyCode().getId();
        CompanyCode company = companyService.find(idCompany);
        return db().where(a->a.getIsDeleted().booleanValue()==false && a.getCompanyCode().equals(company));
    }

    public void saveOrEdit(BoardMemberDto dao, Long id) throws Exception {
        try{
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = companyService.getByCode(user.getCompanyCode().getCode());
            if(id==null){
                if(dao.getOrganizationId()!=null && !dao.getOrganizationId().isEmpty()) {
                    for(Long o : dao.getOrganizationId()){
                        BoardMember obj = new BoardMember();
                        obj.setIsDeleted(false);
                        if(dao.getBoardId()!=null){
                            Board board = boardService.find(dao.getBoardId());
                            obj.setBoard(board);
                        }
                        obj.setCompanyCode(company);
                        MasterStrukturOrganisasi org = organisasiService.find(o);
                        if(org!=null) {
                            obj.setOrganization(org);
                            if(org.getUser()!=null){
                                obj.setUser(org.getUser());
                                if(org.getUser().getKorsa()!=null){
                                    obj.setUnit(org.getUser().getKorsa());
                                }
                            }
                        }
                        create(obj);
                        getEntityManager().flush();
                        getEntityManager().refresh(obj);
                    }
                }
            }else{
                if(dao.getOrganizationId()!=null && !dao.getOrganizationId().isEmpty()) {
                    List<BoardMember> bmList = getByBoardId(id);
                    if(!bmList.isEmpty()){
                        for(BoardMember bm : bmList){
                            bm.setIsDeleted(true);
                            edit(bm);
                            getEntityManager().flush();
                        }
                    }

                    for(Long o : dao.getOrganizationId()){
                        BoardMember obj = new BoardMember();
                        obj.setIsDeleted(false);
                        if(dao.getBoardId()!=null){
                            Board board = boardService.find(dao.getBoardId());
                            obj.setBoard(board);
                        }
                        obj.setCompanyCode(company);
                        MasterStrukturOrganisasi org = organisasiService.find(o);
                        if(org!=null) {
                            obj.setOrganization(org);
                            if(org.getUser()!=null){
                                obj.setUser(org.getUser());
                                if(org.getUser().getKorsa()!=null){
                                    obj.setUnit(org.getUser().getKorsa());
                                }
                            }
                        }
                        create(obj);
                        getEntityManager().flush();
                        getEntityManager().refresh(obj);
                    }
                }

            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public BoardMember deleteMember(Long id) throws Exception {
        try{
            BoardMember obj = find(id);
            obj.setIsDeleted(true);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    private List<BoardMember> getByBoardId (Long idBoard){
        return db().where(a->!a.getIsDeleted() && a.getBoard().getId().equals(idBoard)).collect(Collectors.toList());
    }
}
