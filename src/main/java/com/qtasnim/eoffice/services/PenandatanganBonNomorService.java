package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.PemeriksaBonNomor;
import com.qtasnim.eoffice.db.PenandatanganBonNomor;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@LocalBean
public class PenandatanganBonNomorService extends AbstractFacade<PenandatanganBonNomor> {

    @Override
    protected Class<PenandatanganBonNomor> getEntityClass() {
        return PenandatanganBonNomor.class;
    }

    public List<PenandatanganBonNomor> getAllBy(Long id){
        return db().where(q->q.getPermohonan().getId()==id).toList();
    }

    public PenandatanganBonNomor getByIdAndOrg(Long id,Long idOrg) {
        return db().where(q -> q.getOrganisasi().getIdOrganization()==idOrg && q.getPermohonan().getId()==id
        ).findFirst().orElse(null);
    }
}
