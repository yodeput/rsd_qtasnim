package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.AppSession;
import com.qtasnim.eoffice.db.UserSession;
import com.qtasnim.eoffice.security.ForbiddenException;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.AppSessionDto;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class AppSessionService extends AbstractFacade<AppSession> {
    private static final long serialVersionUID = -2368314413077813254L;

    @Override
    protected Class<AppSession> getEntityClass() {
        return AppSession.class;
    }

    @Inject
    private JWTService jwtService;

    @Inject
    private ApplicationConfig applicationConfig;

    public AppSession findByToken(String token) {
        Date now = new Date();

        return db()
                .where(t -> t.getToken().equals(token) && t.getStartDate().before(now)
                        && now.before(t.getEndDate()))
                .findFirst().orElse(null);
    }

    public void save(Long id,AppSessionDto dao) throws Exception {
        boolean isNew = id == null ? true : false;
        AppSession obj = new AppSession();
        DateUtil dateUtil = new DateUtil();
        try {
            if (isNew) {
                obj.setAppName(dao.getAppName());
                obj.setStartDate(dateUtil.getDateFromISOString(dao.getStartDate()));
                obj.setEndDate(dateUtil.getDateFromISOString(dao.getEndDate()));
                String token = jwtService.generateJWTApp(obj,applicationConfig);
                obj.setToken(token);
                create(obj);
            } else {
                obj = this.find(id);
                obj.setAppName(dao.getAppName());
                obj.setStartDate(dateUtil.getDateFromISOString(dao.getStartDate()));
                obj.setEndDate(dateUtil.getDateFromISOString(dao.getEndDate()));
                String token = jwtService.generateJWTApp(obj,applicationConfig);
                obj.setToken(token);
                edit(obj);
            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
}