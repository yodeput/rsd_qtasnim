package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.db.HelpDocument;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.GlobalAttachmentDto;
import com.qtasnim.eoffice.ws.dto.HelpDocumentDto;
import com.qtasnim.eoffice.ws.dto.HelpDocumentMediaDto;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

@Stateless
@LocalBean
public class HelpDocumentService extends AbstractFacade<HelpDocument> {

    private static final long serialVersionUID = 1L;

    @Inject
    CompanyCodeService companyCodeService;

    @Inject
    private FileService fileService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Override
    protected Class<HelpDocument> getEntityClass() {
        return HelpDocument.class;
    }

    public JPAJinqStream<HelpDocument> getAll() {
        return db();
    }

    public HashMap<String, Object> getHelpDocuments() {
        List<HelpDocumentDto> result = this.getAll().map(HelpDocumentDto::new).collect(Collectors.toList());
        Long length = this.getAll().count();

        HashMap<String, Object> retur = new HashMap<>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    public HelpDocument saveData(Long id, HelpDocumentDto dao, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        HelpDocument model = new HelpDocument();

        if (id == null) {
            this.create(addOrEdit(model, dao));
            getEntityManager().flush();
            this.uploadAttachment(model.getId(), model.getCompanyCode(), inputStream, formDataContentDisposition, body);
        } else {
            model = this.find(id);
            this.edit(addOrEdit(model, dao));
        }
        return model;
    }

    private HelpDocument addOrEdit(HelpDocument model, HelpDocumentDto dao) {
        model.setTipe(dao.getTipe());
        model.setNama(dao.getNama());
        model.setDeskripsi(dao.getDeskripsi());
        CompanyCode companyCode = companyCodeService.find(dao.getCompanyId());
        model.setCompanyCode(companyCode);

        return model;
    }

    public void delete(Long id, Long fileId) {
        GlobalAttachment attachment = globalAttachmentService.find(fileId);
        if (attachment != null) {
            fileService.delete(attachment.getDocId());
            attachment.setIsDeleted(true);
            globalAttachmentService.edit(attachment);
            getEntityManager().flush();
            getEntityManager().refresh(attachment);
        }

        HelpDocument model = this.find(id);
        String tableName = "t_help_dokumen";
        List<GlobalAttachment> attachments = globalAttachmentService.getAllDataByRef(tableName, id).collect(Collectors.toList());
        if (attachments.isEmpty()) {
            this.remove(model);
        }
    }

    private void uploadAttachment(Long idHelp, CompanyCode companyCode, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";

        try {
            String filename = formDataContentDisposition.getFileName();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is2 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is3 = new ByteArrayInputStream(baos.toByteArray());

            byte[] documentContent = IOUtils.toByteArray(is1);

            docId = fileService.upload(filename, documentContent);
            docId = docId.replace("workspace://SpacesStore/", "");

            List<String> extractedText = textExtractionService.parsingFile(is2, formDataContentDisposition, body).getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString());
            obj.setDocName(filename);
            obj.setCompanyCode(companyCode);
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setSize(new Long(documentContent.length));
            obj.setIsDeleted(false);
            obj.setReferenceId(idHelp);
            obj.setReferenceTable("t_help_dokumen");
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);
        } catch (Exception ex) {
            if (!org.apache.commons.lang.StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }

            throw ex;
        }
    }

    public void setFiles(List<HelpDocumentDto> list) {
        list.forEach(help -> {
            List<GlobalAttachmentDto> files = globalAttachmentService.getAttachmentDataByRef("t_help_dokumen", help.getId())
                    .map(attach -> {
                        String downloadUrl = fileService.getViewLink(attach.getDocId(), false);
                        String viewUrl = fileService.getViewLink(attach.getDocId(), attach.getMimeType());
                        GlobalAttachmentDto dto = new GlobalAttachmentDto(attach);
                        dto.setDownloadLink(fileService.getViewLink(attach.getDocId(), false));

                        if (attach.getDocName().lastIndexOf(".") > 0) {
                            dto.setViewLink(fileService.getViewLink(attach.getDocId(), FileUtility.GetFileExtension(attach.getDocName())));
                        } else {
                            dto.setViewLink(fileService.getViewLink(attach.getDocId()));
                        }

                        return dto;
                    }).collect(Collectors.toList());

            help.setFiles(files);
        });
    }

    public void saveHelpDocument(HelpDocumentDto dao, InputStream stream, FormDataBodyPart body) throws InternalServerErrorException, IOException, Exception {
        HelpDocument model = new HelpDocument();
        model.setTipe(dao.getTipe());
        model.setNama(dao.getNama());
        model.setDeskripsi(dao.getDeskripsi());
        model.setCompanyCode(userSession.getUserSession().getUser().getCompanyCode());
        this.create(model);
        getEntityManager().flush();
        getEntityManager().refresh(model);

        GlobalAttachment obj = globalAttachmentService.getAttachmentDataByRef("t_help_dokumen", model.getId()).findFirst().orElse(null);

        if (obj == null) {
            this.uploadMedia(model, stream, body);
        }
    }

    private void uploadMedia(HelpDocument model, InputStream stream, FormDataBodyPart body) throws InternalServerErrorException, Exception {
        byte[] media = IOUtils.toByteArray(stream);
        String fileName = body.getContentDisposition().getFileName(), fileType = body.getMediaType().toString(),
                tmpDoc = "", jsonRepresentation = "";

        //<editor-fold desc="ocr" >
        if (fileType.toLowerCase().contains("pdf")) {
            byte[] pdfOcr = Arrays.copyOf(media, media.length);
            String tempFileName = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID().toString() + ".pdf";
            FileOutputStream fos = new FileOutputStream(tempFileName);
            fos.write(pdfOcr);
            fos.close();
            File tempFile = new File(tempFileName);

            List<String> extractedText = textExtractionService.extractText(tempFile);
            jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            Files.deleteIfExists(tempFile.toPath());
        }
        //</editor-fold>

        tmpDoc = fileService.upload(fileName, media);
        tmpDoc = tmpDoc.replace("workspace://SpacesStore/", "");

        GlobalAttachment globalAttachment = new GlobalAttachment();
        globalAttachment.setMimeType(fileType);
        globalAttachment.setDocGeneratedName(UUID.randomUUID().toString());
        globalAttachment.setDocName(fileName);
        globalAttachment.setCompanyCode(model.getCompanyCode());
        globalAttachment.setDocId(tmpDoc);
        globalAttachment.setIsKonsep(false);
        globalAttachment.setIsPublished(false);
        globalAttachment.setIsDeleted(false);
        globalAttachment.setSize((long) media.length);
        globalAttachment.setReferenceTable("t_help_dokumen");
        globalAttachment.setReferenceId(model.getId());
        globalAttachment.setNumber("help_dokumen_111");
        if (fileType.contains("pdf")) {
            globalAttachment.setExtractedText(jsonRepresentation);
        }

        globalAttachmentService.create(globalAttachment);
    }
}
