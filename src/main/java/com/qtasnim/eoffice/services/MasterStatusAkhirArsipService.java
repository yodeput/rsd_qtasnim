package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterStatusAkhirArsip;
import com.qtasnim.eoffice.db.MasterStatusAkhirArsip;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterStatusAkhirArsipDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterStatusAkhirArsipService extends AbstractFacade<MasterStatusAkhirArsip> {
    private static final long serialVersionUID = 1L;

    @Inject
    CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterStatusAkhirArsip> getEntityClass() {
        return MasterStatusAkhirArsip.class;
    }

    public JPAJinqStream<MasterStatusAkhirArsip> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterStatusAkhirArsip> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name.toLowerCase()));
    }
     public MasterStatusAkhirArsip save(Long id, MasterStatusAkhirArsipDto dao){
         MasterStatusAkhirArsip model = new MasterStatusAkhirArsip();
         DateUtil dateUtil = new DateUtil();

         if(id==null){
             model.setNama(dao.getNama());

             if(dao.getCompanyId()!=null){
                 CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                 model.setCompanyCode(cCode);
             } else {
                 model.setCompanyCode(null);
             }
             model.setCreatedBy(dao.getCreatedBy());
             model.setCreatedDate(new Date());
             model.setModifiedBy("-");
             model.setModifiedDate(new Date());
             model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
             if(dao.getEndDate()!=null){
                 model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
             } else {
                 model.setEnd(dateUtil.getDefValue());
             }

             this.create(model);
         } else {
             model = this.find(id);
             model.setNama(dao.getNama());
             if(dao.getCompanyId()!=null){
                 CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                 model.setCompanyCode(cCode);
             } else {
                  model.setCompanyCode(null);
             }
             model.setModifiedBy(dao.getModifiedBy());
             model.setModifiedDate(new Date());
             model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

             if(dao.getEndDate()!=null){
                 model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
             } else {
                 model.setEnd(dateUtil.getDefValue());
             }

             this.edit(model);
         }
        return model;
     }

    public MasterStatusAkhirArsip delete(Long id){
        MasterStatusAkhirArsip model = this.find(id);
        this.remove(model);
        return model;
    }

}