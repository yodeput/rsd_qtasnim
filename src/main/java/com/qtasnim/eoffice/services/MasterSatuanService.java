package com.qtasnim.eoffice.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterSatuan;

import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterSatuanDto;
import org.jinq.jpa.JPAJinqStream;

import java.util.Date;

@Stateless
@LocalBean
public class MasterSatuanService extends AbstractFacade<MasterSatuan> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterSatuan> getEntityClass() {
        return MasterSatuan.class;
    }

    public JPAJinqStream<MasterSatuan> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public MasterSatuanDto saveOrEdit(Long id, MasterSatuanDto dao) throws Exception{
        try {
            MasterSatuanDto result;
            boolean isNew = id == null;
            MasterSatuan model = new MasterSatuan();
            DateUtil dateUtil = new DateUtil();
            if (isNew) {
                model.setName(dao.getName());
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null){
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.create(model);
            } else {
                model = this.find(id);
                model.setName(dao.getName());
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null){
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.edit(model);
            }

            result = new MasterSatuanDto(model);
            return result;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
}