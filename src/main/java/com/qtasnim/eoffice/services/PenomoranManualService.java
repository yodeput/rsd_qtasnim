package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.NotAuthorizedException;
import com.qtasnim.eoffice.security.PasswordHash;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.workflows.*;
import com.qtasnim.eoffice.ws.dto.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class PenomoranManualService extends AbstractFacade<PenomoranManual> implements IWorkflowService<PenomoranManual> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private PemeriksaNomorManualService pemeriksaService;

    @Inject
    private PenandatanganNomorManualService penandatanganService;

    @Inject
    private PenerimaNomorManualService penerimaService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private UserTaskService userTaskService;

    @Inject
    private ProcessDefinitionService processDefinitionService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private FileService fileService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private MasterJenisDokumenService jenisDokumenService;

    @Inject
    private MasterKlasifikasiMasalahService klasifikasiMasalahService;

    @Inject
    private MasterKlasifikasiKeamananService klasifikasiKeamananService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private DokumenHistoryService docHistoryService;

    @Inject
    private MasterAutoNumberService autoNumberService;

    @Override
    protected Class<PenomoranManual> getEntityClass() {
        return PenomoranManual.class;
    }

    public JPAJinqStream<PenomoranManual> getAll() {
        Date n = new Date();
        return db();
    }

    public void saveOrEditTransaction(Long id, PenomoranManualDto dto, FormDataBodyPart file1, FormDataBodyPart file2, List<Long> deletedPemeriksaIds, List<Long> deletedPenandatanganIds, List<Long> deletedPenerimaIds) {
        try {
            PenomoranManual model = saveOrEdit(id, dto, file1, file2, deletedPemeriksaIds, deletedPenandatanganIds, deletedPenerimaIds);
            getEntityManager().refresh(model);

            MasterUser user = userSession.getUserSession().getUser();
            boolean isKonseptor = model.getKonseptor().getIdOrganization().equals(user.getOrganizationEntity().getIdOrganization()) ? true : false;
            if (isKonseptor) {
                startWorkflow(model);
            } else {
                UserTask userTask = userTaskService.getUserTask(model.getId().toString(), PenomoranManual.class.getName(), user.getOrganizationEntity().getIdOrganization());
                if (userTask != null) {
                    userTaskService.updatePerihal(userTask.getId(), model.getPerihal());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public PenomoranManual saveOrEdit(Long id, PenomoranManualDto dto, FormDataBodyPart file1, FormDataBodyPart file2, List<Long> deletedPemeriksaIds, List<Long> deletedPenandatanganIds, List<Long> deletedPenerimaIds) throws Exception {
        boolean isNew = id == null;

        if (isNew) {
            PenomoranManual model = this.getModel(new PenomoranManual(), dto);

            this.create(model);
            getEntityManager().flush();

            this.uploadAttachment(model.getId(), file1, "file1");
            this.uploadAttachment(model.getId(), file2, "file1");

            this.savePemeriksa(model, dto);
            this.savePenandatangan(model, dto);
            this.savePenerima(model, dto);
            return model;

        } else {
            PenomoranManual model = this.getModel(this.find(id), dto);

            this.edit(model);
            getEntityManager().flush();

            this.uploadAttachment(model.getId(), file1, "file1");
            this.uploadAttachment(model.getId(), file2, "file1");
            if (deletedPemeriksaIds != null && !deletedPemeriksaIds.isEmpty()) {
                for (Long deletedId : deletedPemeriksaIds) {
                    this.deletePemeriksa(deletedId);
                }
            }

            if (deletedPenandatanganIds != null && !deletedPenandatanganIds.isEmpty()) {
                for (Long deletedId : deletedPenandatanganIds) {
                    this.deletePenandatangan(deletedId);
                }
            }

            this.savePemeriksa(model, dto);
            this.savePenandatangan(model, dto);
            this.savePenerima(model, dto);
            return model;

        }
    }

    private PenomoranManual getModel(PenomoranManual model, PenomoranManualDto dto) {
        DateUtil dateUtil = new DateUtil();

        if (model.getKonseptor() == null) {
            model.setKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
        }
        MasterJenisDokumen jenis = jenisDokumenService.find(dto.getIdJenisDokumen());
        model.setJenisDok(jenis);

        MasterKlasifikasiMasalah masalah = klasifikasiMasalahService.find(dto.getIdKlasifikasiDokumen());
        model.setKlasDokumen(masalah);

        MasterKlasifikasiKeamanan keamanan = klasifikasiKeamananService.find(dto.getIdKlasifikasiKeamanan());
        model.setKlasKeamanan(keamanan);

        model.setStatus(dto.getStatus());
        model.setPerihal(dto.getPerihal());
        model.setLampiran(dto.getLampiran());
        model.setKeterangan(dto.getKeterangan());
        model.setTempatDiterima(dto.getTempatDiterima());
        model.setIsDeleted(dto.getIsDeleted());
        model.setTglDiterima(dateUtil.getDateFromISOString(dto.getTglDiterima()));
        model.setTglDokumen(dateUtil.getDateFromISOString(dto.getTglDokumen()));


        if (dto.getIdCompany() != null) {
            CompanyCode company = companyCodeService.find(dto.getIdCompany());
            model.setCompanyCode(company);
        }

        return model;
    }


    private void savePemeriksa(PenomoranManual model, PenomoranManualDto modelDto) throws Exception {
        if (!modelDto.getPemeriksa().isEmpty()) {
            Integer index = 0;

            for (PemeriksaNomorManualDto dto : modelDto.getPemeriksa().stream().distinct().collect(Collectors.toList())) {
                PemeriksaNomorManual obj;

                if (dto.getId() == null) {
                    MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dto.getIdPemeriksa());
                    obj = new PemeriksaNomorManual();
                    obj.setOrganization(org);
                    obj.setUser(org.getUser());
                    obj.setPermohonan(model);
                    obj.setIsUnitDokumen(dto.getIsUnitDokumen());
                    pemeriksaService.create(obj);
                } else {
                    obj = pemeriksaService.find(dto.getId());
                }

                index++;
            }
        }
    }

    private void savePenandatangan(PenomoranManual model, PenomoranManualDto modelDto) throws Exception {
        if (!modelDto.getPenandatangan().isEmpty()) {
            Integer index = 0;

            for (PenandatanganNomorManualDto ttd : modelDto.getPenandatangan().stream().distinct().collect(Collectors.toList())) {
                PenandatanganNomorManual obj;

                if (ttd.getId() == null) {


                    obj = new PenandatanganNomorManual();
                    if (ttd.getIdPenandatangan() != null) {
                        MasterStrukturOrganisasi penanda = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
                        obj.setOrganization(penanda);
                    }

                    if (ttd.getUserId() != null) {
                        MasterUser user = masterUserService.find(ttd.getUserId());
                        obj.setUser(user);
                    }

                    if (ttd.getIdANOrg() != null) {
                        MasterStrukturOrganisasi penanda = masterStrukturOrganisasiService.find(ttd.getIdANOrg());
                        obj.setOrgAN(penanda);
                    }

                    if (ttd.getIdANUser() != null) {
                        MasterUser user = masterUserService.find(ttd.getIdANUser());
                        obj.setUserAN(user);
                    }
                    penandatanganService.create(obj);
                } else {
                    obj = penandatanganService.find(ttd.getId());

                    if (!Optional.ofNullable(obj.getSeq()).orElse(-1).equals(Optional.ofNullable(ttd.getSeq()).orElse(-1))) {
                        obj.setSeq(Optional.ofNullable(ttd.getSeq()).orElse(index));
                        penandatanganService.edit(obj);
                    }
                }

                index++;
            }
        }
    }

    private void savePenerima(PenomoranManual model, PenomoranManualDto modelDto) throws Exception {
        if (!modelDto.getPenerima().isEmpty()) {
            Integer index = 0;

            for (PenerimaNomorManualDto dto : modelDto.getPenerima().stream().distinct().collect(Collectors.toList())) {
                DateUtil dateUtil = new DateUtil();
                PenerimaNomorManual obj;

                if (dto.getId() == null) {
                    MasterStrukturOrganisasi org = masterStrukturOrganisasiService.find(dto.getIdOrganization());

                    obj = new PenerimaNomorManual();
                    obj.setOrganization(org);
                    obj.setUser(org.getUser());
                    obj.setPermohonan(model);
                    obj.setSeq(Optional.ofNullable(dto.getSeq()).orElse(index));

                    penerimaService.create(obj);
                } else {
                    obj = penerimaService.find(dto.getId());
                    obj.setSeq(Optional.ofNullable(dto.getSeq()).orElse(index));


                    penerimaService.edit(obj);
                }

                index++;
            }
        }
    }

    private void deletePemeriksa(Long idPemeriksa) throws Exception {
        PemeriksaNomorManual pemeriksa = pemeriksaService.find(idPemeriksa);
        pemeriksaService.remove(pemeriksa);
    }

    private void deletePenandatangan(Long idPenandatangan) throws Exception {
        PenandatanganNomorManual penandatangan = penandatanganService.find(idPenandatangan);
        penandatanganService.remove(penandatangan);
    }


    public void startWorkflow(PenomoranManual obj) {
        if (obj.getStatus().equals("SUBMITTED")) {

            final PenomoranManual surat = obj;
            ProcessDefinition activated = processDefinitionService.getActivated("Penomoran Manual");
            IWorkflowProvider workflowProvider = masterWorkflowProviderService.getByProviderName(applicationContext.getApplicationConfig().getWorkflowProvider()).getWorkflowProvider();
            workflowProvider.startWorkflow(obj, activated, ProcessEventHandler.DEFAULT
                            .onCompleted((processInstance, status) -> onWorkflowCompleted(status, processInstance, surat, workflowProvider,null,null))
                            .onTasksCreated((prevTask, processTasks) -> onTasksCreated(surat, prevTask, processTasks)),
                    new HashMap<>()
            );
        }
    }

    public void readDistribusi(PenomoranManual model, Long idOrg) throws InternalServerErrorException {

        DokumenHistoryDto history = new DokumenHistoryDto();
        history.setReferenceTable("t_penomoran_manual");
        history.setReferenceId(model.getId());
        history.setStatus("READ");

        MasterUserDto usr = masterUserService.getByOrgId(idOrg);
        history.setIdOrganisasi(idOrg);
        history.setIdUser(usr.getId());
        docHistoryService.saveHistory(history);
    }

    /**
     * START Workflow Event Handler
     */
    public IWorkflowEntity getEntity(ProcessTask processTask) {
        return find(Long.parseLong(processTask.getProcessInstance().getRecordRefId()));
    }

    @Override
    public boolean isTaskInvalid(TaskActionDto dto, ProcessTask processTask, Map<String, Object> userResponse) throws Exception {
        return false;
    }

    @Override
    public void onWorkflowCompleted(ProcessStatus status, ProcessInstance processInstance, IWorkflowEntity entity, IWorkflowProvider workflowProvider, String digisignUsername, String passphrase) {
        try {
            PenomoranManual penomoranManual = (PenomoranManual) entity;

            if (ProcessStatus.APPROVED.equals(status)) {
                penomoranManual.setStatus(EntityStatus.APPROVED.toString());
                penomoranManual.setTglDokumen(new Date());
                if (StringUtils.isEmpty(penomoranManual.getNoDokumen())) {
                    penomoranManual.setNoDokumen(autoNumberService.from(PenomoranManual.class).next(PenomoranManual::getNoDokumen, penomoranManual));
                }

                edit(penomoranManual);

                List<UserTask> userTaskList = userTaskService.getListUserTask(penomoranManual.getId().toString(), penomoranManual.getClassName());
                if (!userTaskList.isEmpty()) {
                    userTaskList.forEach(ut -> {
                        ut.setStatus(EntityStatus.APPROVED.toString());
                        userTaskService.edit(ut);
                    });
                }
            } else if (ProcessStatus.REJECTED.equals(status)) {

                penomoranManual.setStatus(EntityStatus.DRAFT.toString());
                edit(penomoranManual);

                List<UserTask> userTaskList = userTaskService.getListUserTask(penomoranManual.getId().toString(), penomoranManual.getClassName());
                if (!userTaskList.isEmpty()) {
                    userTaskList.forEach(ut -> {
                        ut.setStatus(EntityStatus.REJECTED.toString());
                        ut.setExecutionStatus(ExecutionStatus.SKIP);
                        userTaskService.edit(ut);
                    });
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onTasksCreated(IWorkflowEntity entity, ProcessTask prevTask, List<ProcessTask> processTasks) {
//        newTaskNotification(processTasks, entity, null);

        //#region INSERT INTO USER TASK
        for (ProcessTask task : processTasks) {
            userTaskService.saveOrEdit(null, task, entity);
        }
        //#endregion
    }

    @Override
    public void onTaskExecuting(ProcessTask task) {

    }

    @Override
    public void onTaskExecuted(TaskActionDto dto, IWorkflowProvider workflowProvider, IWorkflowEntity entity, ProcessTask task, TaskEventHandler taskEventHandler, Map<String, Object> userResponse) {
        PenomoranManual obj = (PenomoranManual) entity;

        //#region Update user approver
        if (task.getResponse().equalsIgnoreCase("Setujui")) {

            if (task.getTaskName().toLowerCase().contains("pemeriksa")) {

                obj.getPemeriksa()
                        .stream()
                        .filter(q -> q.getOrganization().getIdOrganization().equals(task.getAssignee().getIdOrganization()))
                        .forEach(pemeriksa -> {
                            pemeriksa.setUser(userSession.getUserSession().getUser());
                        });

                userTaskService.saveOrEdit(task);

            } else if (task.getTaskName().toLowerCase().contains("penandatangan")) {

                obj.getPenandatangan()
                        .stream()
                        .filter(q -> q.getOrganization().getIdOrganization().equals(task.getAssignee().getIdOrganization()))
                        .forEach(t -> {
                            t.setUser(userSession.getUserSession().getUser());
                        });

                userTaskService.saveOrEdit(task);

            }

            try {
                edit(obj);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
        //#endregion

        /*@TODO Tambah insert ke tabel dokumen history*/
        if (userSession != null && userSession.getUserSession() != null) {
            String responseVar = task.getResponseVar();
            String response = (String) userResponse.get(responseVar);

            if (response.toLowerCase().contains("kembalikan")) {
                Long id = ((PenomoranManual) entity).getId();
                DokumenHistoryDto history = new DokumenHistoryDto();
                history.setReferenceTable("t_penomoran_manual");
                history.setReferenceId(id);
                history.setStatus("REJECT");
                history.setIdOrganisasi(task.getAssignee().getIdOrganization());
                history.setIdUser(task.getAssignee().getUser().getId());

                docHistoryService.saveHistory(history);
            }

            if (response.toLowerCase().contains("setujui")) {
                Long id = ((PenomoranManual) entity).getId();
                DokumenHistoryDto history = new DokumenHistoryDto();
                history.setReferenceTable("t_penomoran_manual");
                history.setReferenceId(id);
                history.setStatus("APPROVE");
                history.setIdOrganisasi(task.getAssignee().getIdOrganization());
                history.setIdUser(task.getAssignee().getUser().getId());

                docHistoryService.saveHistory(history);
            }
        }

        notificationService.sendInfo("Info", "Persetujuan penomoran manual berhasil dikirim");
    }

    @Override
    public List<ProcessTaskDto> onTaskHistoryRequested(List<ProcessTask> myTasks) {
        return myTasks.stream().map(ProcessTaskDto::new).collect(Collectors.toList());
    }

    @Override
    public void onTaskActionGenerated(ProcessTask processTask, TaskActionResponseDto response) {

    }
    /**
     * END Workflow Event Handler
     */

    /**
     * START Mail Handler
     */
    public void setMailProperties(IWorkflowEntity entity) {
        String className = entity.getClassName();

        String jenisDokumen = "";
        String perihal = "";
        String nomor = "";
        String klasifikasiKeamanan = "";

        if (className.contains("PermohonanDokumen")) {
            jenisDokumen = ((PermohonanDokumen) entity).getTipeLayanan();
            perihal = ((PermohonanDokumen) entity).getPerihal();
            nomor = ((PermohonanDokumen) entity).getSurat().getNomor();
        } else if (className.contains("PenomoranManual")) {
            PenomoranManual obj = ((PenomoranManual)entity);
            jenisDokumen = "Penomoran Manual (" + obj.getJenisDokumen() + ")";
            perihal = obj.getPerihal();
        }

        entity.setJenisDokumen(jenisDokumen);
        entity.setPerihal(perihal);
        entity.setKlasifikasiKeamanan(klasifikasiKeamanan);
        if (className.contains("PermohonanDokumen")) {
            entity.setNomor(nomor);
        } else {
            entity.setNomor(entity.getNoDokumen());
        }
    }

    public void newTaskNotification(List<ProcessTask> processTasks, IWorkflowEntity entity, String docId) {
        for (ProcessTask processTask : processTasks) {
            MasterUser user = processTask.getAssignee().getUser();
            MasterUser initiator = masterUserService.findUserByUsername(entity.getCreatedBy());

            // < SET MAIL PROPS >
            setMailProperties(entity);
            // </ SET MAIL PROPS >

            notificationService.sendEmail(Constants.TEMPLATE_NEW_TASK_ASSIGNMENT, vars -> {
                vars.put("initiator", initiator);
                vars.put("assignee", user);
                vars.put("entity", entity);
                vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
            }, docId, null, user);
        }
    }

    public void newTaskNotification(ProcessTask processTask, IWorkflowEntity entity, String docId) {
        MasterUser user = processTask.getAssignee().getUser();

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        notificationService.sendEmail(Constants.TEMPLATE_NEW_TASK_ASSIGNMENT, vars -> {
            vars.put("assignee", user);
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
        }, docId, null, user);
    }

    public void infoTaskNotification(ProcessTask processTask, IWorkflowEntity entity, String docId) {
        // < INIT TASK DICTIONARY >
        Map<String, String> taskExecutorVars = new HashMap<>();
        taskExecutorVars.put("Task_PemeriksaPelakhar_Approval", "Pelakhar Pemeriksa");
        taskExecutorVars.put("Task_PemeriksaPYMT_Approval", "PYMT Pemeriksa");
        taskExecutorVars.put("Task_Pemeriksa_Approval", "Pemeriksa");

        taskExecutorVars.put("Task_PenandatanganPelakhar_Approval", "Pelakhar Penandatangan");
        taskExecutorVars.put("Task_PenandatanganPelakhar_Approval_2", "Pelakhar Penandatangan");
        taskExecutorVars.put("Task_PenandatanganPYMT_Approval", "PYMT Penandatangan");
        taskExecutorVars.put("Task_Penandatangan_Approval", "Penandatangan");

        taskExecutorVars.put("Setujui", "Disetujui");
        taskExecutorVars.put("TO PYMT", "Disetujui");
        taskExecutorVars.put("Kembalikan", "Dikembalikan");
        taskExecutorVars.put("Kembalikan ke Konseptor", "Dikembalikan");
        taskExecutorVars.put("Kembalikan ke Pemeriksa", "Dikembalikan");

        // </ INIT TASK DICTIONARY >
        String executor = taskExecutorVars.get(processTask.getTaskName());
        if (!Strings.isNullOrEmpty(executor)) {
            entity.setTaskExecutor(executor);
        } else {
            entity.setTaskExecutor("");
        }

        String response = taskExecutorVars.get(processTask.getResponse());

        if (!Strings.isNullOrEmpty(response)) {
            entity.setTaskResponse(response);
        } else {
            entity.setTaskResponse("");
        }

        MasterUser user = processTask.getAssignee().getUser();
        String[] initiatorParts = entity.getCreatedBy().split("\\|");
        MasterUser initiator = masterUserService.findUserByUsername(initiatorParts[0].trim());

        // < SET MAIL PROPS >
        setMailProperties(entity);
        // </ SET MAIL PROPS >

        notificationService.sendEmail(Constants.TEMPLATE_WORKFLOW_INFO, vars -> {
            vars.put("entity", entity);
            vars.put("pageLink", String.format("%s%s", applicationContext.getApplicationConfig().getFrontEndUrl(), entity.getPageLink()));
        }, docId, null, initiator);
    }

    public void uploadAttachment(Long id, FormDataBodyPart body, String number) throws InternalServerErrorException, Exception {
        String docId = "";
//        try {
        PenomoranManual manual = this.find(id);
        GlobalAttachment obj = globalAttachmentService.getAttachmentDataByRef("t_penomoran_maunal", id).findFirst().orElse(null);

        if (body != null) {
            if (obj != null) {
                globalAttachmentService.deleteAttachment("t_penomoran_maunal", id);
            }

            String tmpDoc = "";
            BodyPartEntity bodyPartEntity = (BodyPartEntity) body.getEntity();
            String filename = body.getContentDisposition().getFileName();

            byte[] documentContent = org.apache.commons.compress.utils.IOUtils.toByteArray(bodyPartEntity.getInputStream());

            tmpDoc = fileService.upload(filename, documentContent);
            tmpDoc = tmpDoc.replace("workspace://SpacesStore/", "");

            obj = new GlobalAttachment();
            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString());
            obj.setDocName(filename);
            obj.setCompanyCode(userSession.getUserSession().getUser().getCompanyCode());
            obj.setDocId(tmpDoc);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setSize(new Long(documentContent.length));
            obj.setIsDeleted(false);
            obj.setReferenceId(id);
            obj.setReferenceTable("t_penomoran_maunal");
            obj.setNumber(number);

            globalAttachmentService.create(obj);
        }
//        } catch (Exception ex) {
//            ex.getMessage();
//        }
    }

   /* public void uploadAttachment(Long idPenomoran, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";

        try {
            String filename = formDataContentDisposition.getFileName();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is2 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is3 = new ByteArrayInputStream(baos.toByteArray());

            byte[] documentContent = IOUtils.toByteArray(is1);

            docId = fileService.upload(filename, documentContent);
            docId = docId.replace("workspace://SpacesStore/", "");

            PenomoranManual penomoranManual = this.find(idPenomoran);

//            String number = autoNumberService.from(SuratDocLib.class).next(SuratDocLib::getNumber);
            List<String> extractedText = textExtractionService.parsingFile(is2, formDataContentDisposition, body).getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString());
            obj.setDocName(filename);
            obj.setCompanyCode(penomoranManual.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setSize(new Long(documentContent.length));
            obj.setIsDeleted(false);
            obj.setReferenceId(idPenomoran);
            obj.setReferenceTable("t_penomoran_manual");
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);
        } catch (Exception ex) {
            if (!org.apache.commons.lang.StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }

            throw ex;
        }
    }*/

    public void deleteAttachment(Long idSurat, String docId) {
        GlobalAttachment attachment = globalAttachmentService.findByDocId("t_surat", idSurat, docId);

        if (attachment != null) {
            globalAttachmentService.remove(attachment);
            fileService.delete(docId);
        }
    }
}
