package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.PenerimaPara;
import com.qtasnim.eoffice.db.TembusanPara;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class PenerimaParaService extends AbstractFacade<PenerimaPara>{

    @Override
    protected Class<PenerimaPara> getEntityClass() {
        return PenerimaPara.class;
    }

    public JPAJinqStream<PenerimaPara> getAll() {return db();}

    public List<PenerimaPara> getPenerimaParaList(Long idPenerima){
        return db().where(a->a.getPenerimaSurat().getId().equals(idPenerima)).collect(Collectors.toList());
    }
}
