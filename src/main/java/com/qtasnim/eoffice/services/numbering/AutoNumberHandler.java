package com.qtasnim.eoffice.services.numbering;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.RomanNumeralConverter;
import com.qtasnim.eoffice.ws.PenomoranManualResource;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrLookup;
import org.apache.commons.lang3.text.StrSubstitutor;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Stateless
@LocalBean
public class AutoNumberHandler<T> {
    @Inject
    private MasterAutoNumberService autoNumberFacade;
    @Inject
    private FormValueService formValueService;
    @Inject
    private MasterKlasifikasiMasalahService klasifikasiMasalahService;
    @Inject
    private MasterKlasifikasiKeamananService klasifikasiKeamananService;
    @Inject
    private SuratService suratService;

    private Recorder recorder;
    private Class<T> object;
    private static final String SimpleFormat = "{value:0}";
    private static final String DefaultFormat = "{MasterJenisDokumen}.{Jabatan}/{MasterKlasifikasiMasalah}/{rom_month:MM}/{Counter:identifier}{MasterKlasifikasiKeamanan}/{MasterArea}-{now:yyyy}";
    private static final String AgendaFormat = "{Counter:identifier}.{MasterKlasifikasiMasalah}.{rom_month:MM}.{MasterArea}-{now:yyyy}";
    private static final String SuratDinasFormat = "{MasterKlasifikasiMasalah}/{rom_month:MM}/{Counter:identifier}{MasterKlasifikasiKeamanan}/{MasterArea}-{now:yyyy}";
    private static final HashMap<String, Supplier<Object>> ValidTokens = new HashMap<String, Supplier<Object>>(){
        {
            put("now", Date::new);
            put("value", () -> null);
            put("rom_month", () -> RomanNumeralConverter.Convert(new Date().getMonth() + 1));
        }
    };

    public String next(Consumer<T> getter) {
        return next(getter, null);
    }

    public String next(String identifier) {
        String key = identifier;

        try {
            MasterAutoNumber current = autoNumberFacade.findByTitle(key);

            if (current == null) {
                current = new MasterAutoNumber();
                current.setTitle(key);
                current.setFormat(SimpleFormat);
                current.setValue(0);
//                current.setResetPeriod("YEAR");
//
//                Calendar nextReset = Calendar.getInstance();
//                nextReset.add(Calendar.YEAR, 1);
//                nextReset.set(nextReset.get(Calendar.YEAR), Calendar.JANUARY, 1, 0, 0, 0);
//                current.setNextReset(nextReset.getTime());

                current.setResetPeriod("MONTH");

                Calendar nextReset = Calendar.getInstance();
                nextReset.add(Calendar.MONTH, 1);
                int year = Calendar.getInstance().get(Calendar.YEAR);
                nextReset.set(year, nextReset.get(Calendar.MONTH), 1, 0, 0, 0);
                current.setNextReset(nextReset.getTime());

                autoNumberFacade.create(current);
            }

            validateCounterExpiration(current);
            current.setValue(current.getValue() + 1);
            final int currentValue = current.getValue();

            StrLookup strLookup = new StrLookup() {
                @Override
                public String lookup(String key) {
                    String[] nameParts = key.split(":");
                    String _key = nameParts[0];
                    String _format = null;

                    if (nameParts.length > 1) {
                        _format = nameParts[1];
                    }

                    Object value = getValue(_key);

                    if (value == null) {
                        value = currentValue;
                    }

                    switch (_key){
                        case "now": {
                            DateFormat df = new SimpleDateFormat(_format);
                            return df.format(value);
                        }
                        case "value": {
                            if (!StringUtils.isEmpty(_format)) {
                                return String.format("%0" + _format.length() + "d", value);
                            }
                        }
                    }

                    return value.toString();
                }
            };

            StrSubstitutor sub = new StrSubstitutor(strLookup);
            sub.setVariablePrefix("{");
            sub.setVariableSuffix("}");

            autoNumberFacade.edit(current);

            return sub.replace(current.getFormat());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "-1";
    }

    public String next(Consumer<T> getter, String identifier) {
        getter.accept((T)recorder.getObject());
        String fieldName = recorder.getCurrentPropertyName();
        String className = object.getSimpleName();
        String key = String.format("%s.%s", className, fieldName.replace("get", ""));

        if (StringUtils.isNotEmpty(identifier)) {
            key = String.format("%s[%s]", key, identifier);
        }

        try {
            MasterAutoNumber current = autoNumberFacade.findByTitle(key);

            if (current == null) {
                current = new MasterAutoNumber();
                current.setTitle(key);
                current.setFormat(SimpleFormat);
                current.setValue(0);
//                current.setResetPeriod("YEAR");
//
//                Calendar nextReset = Calendar.getInstance();
//                nextReset.add(Calendar.YEAR, 1);
//                nextReset.set(nextReset.get(Calendar.YEAR), Calendar.JANUARY, 1, 0, 0, 0);
//                current.setNextReset(nextReset.getTime());

                current.setResetPeriod("MONTH");

                Calendar nextReset = Calendar.getInstance();
                nextReset.add(Calendar.MONTH, 1);
                int year = Calendar.getInstance().get(Calendar.YEAR);
                nextReset.set(year, nextReset.get(Calendar.MONTH), 1, 0, 0, 0);
                current.setNextReset(nextReset.getTime());

                autoNumberFacade.create(current);
            }

            validateCounterExpiration(current);
            current.setValue(current.getValue() + 1);
            final int currentValue = current.getValue();

            StrLookup strLookup = new StrLookup() {
                @Override
                public String lookup(String key) {
                    String[] nameParts = key.split(":");
                    String _key = nameParts[0];
                    String _format = null;

                    if (nameParts.length > 1) {
                        _format = nameParts[1];
                    }

                    Object value = getValue(_key);

                    if (value == null) {
                        value = currentValue;
                    }

                    switch (_key){
                        case "now": {
                            DateFormat df = new SimpleDateFormat(_format);
                            return df.format(value);
                        }
                        case "value": {
                            if (!StringUtils.isEmpty(_format)) {
                                return String.format("%0" + _format.length() + "d", value);
                            }
                        }
                    }

                    return value.toString();
                }
            };

            StrSubstitutor sub = new StrSubstitutor(strLookup);
            sub.setVariablePrefix("{");
            sub.setVariableSuffix("}");

            autoNumberFacade.edit(current);

            return sub.replace(current.getFormat());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "-1";
    }

    public String next(Consumer<T> getter, Object entityClass) {
        getter.accept((T)recorder.getObject());
        String fieldName = recorder.getCurrentPropertyName();
        String className = object.getSimpleName();
        String key = String.format("%s.%s", className, fieldName.replace("get", ""));

        String identifier = getMainIdentifier(entityClass,fieldName);

        if (StringUtils.isNotEmpty(identifier)) {
            key = String.format("%s[%s]", key, identifier);
        }

        try {
            MasterAutoNumber current = autoNumberFacade.findByTitle(key);
            String counterIdentifier = getIdentifier(entityClass,fieldName);
            String finalFormat = DefaultFormat.replaceAll("identifier", String.format("[%s]", counterIdentifier));
            String suratDinasFormat = SuratDinasFormat.replaceAll("identifier", String.format("[%s]", counterIdentifier));
            String agendaFormat = AgendaFormat.replaceAll("identifier", String.format("[%s]", counterIdentifier));

            if (current == null) {
                current = new MasterAutoNumber();
                current.setTitle(key);
                if(className.equals("Surat")) {
                    if(fieldName.equals("getNoDokumen")) {
                        Surat surat = (Surat) entityClass;
                        if(surat.getFormDefinition().getJenisDokumen().getBaseCode().equals("SD")) {
                            current.setFormat(suratDinasFormat);
                        }else{
                            current.setFormat(finalFormat);
                        }
                    }
                    if(fieldName.equals("getNoAgenda")){
                        current.setFormat(agendaFormat);
                    }
                }else{
                    if(className.equals("PenomoranManual")){
                        current.setFormat(finalFormat);
                    }
                    if(className.equals("MasterRuangArsip")){
                        String format = "RA.{unit}.{value:00}";
                        current.setFormat(format);
                    }
                    if(className.equals("MasterLemariArsip")){
                        String format = "LA.{unit}.{value:00}";
                        current.setFormat(format);
                    }
                    if(className.equals("MasterTrapArsip")){
                        String format = "TA.{kode}.{value:0}";
                        current.setFormat(format);
                    }
                    if(className.equals("MasterBoksArsip")){
                        String format = "BA.{unit}.{value:00}";
                        current.setFormat(format);
                    }
                    if(className.equals("GlobalAttachment")){
                        GlobalAttachment global = (GlobalAttachment) entityClass;
                        String format = global.getIsKonsep().booleanValue() == true ? "B.{now:yyyy} - {value:0}" : "T.{now:yyyy} - {value:0}";
                        current.setFormat(format);
                    }
                    if(className.equals("Berkas")){
                        String format = "FA.{unit}.{value:00}";
                        current.setFormat(format);
                    }
                    if(className.equals("Arsip")){
                        String format = "{berkas}.{value:00}";
                        current.setFormat(format);
                    }

                    /*@TODO tambah kondisi untuk distribusi via jasa pengiriman ,permohonan bon nomor dan regulasi*/

                    if(className.equals("PermohonanBonNomor")){
                        String format = "K.{unit}.{value:00}.{now:yyyy}";
                        current.setFormat(format);
                    }

                    if(className.equals("Regulasi")){
                        String format = "K.{unit}.{value:00}.{now:yyyy}";
                        current.setFormat(format);
                    }
                }
                current.setValue(0);
                current.setResetPeriod("MONTH");

                Calendar nextReset = Calendar.getInstance();
                nextReset.add(Calendar.MONTH, 1);
                int year = Calendar.getInstance().get(Calendar.YEAR);
                nextReset.set(year, nextReset.get(Calendar.MONTH), 1, 0, 0, 0);
                current.setNextReset(nextReset.getTime());

                autoNumberFacade.create(current);
            }

            validateCounterExpiration(current);
            current.setValue(current.getValue() + 1);
            final int currentValue = current.getValue();

            StrLookup strLookup = new StrLookup() {
                @Override
                public String lookup(String key) {
                    String[] nameParts = key.split(":");
                    String _key = nameParts[0];
                    String _format = null;

                    if (nameParts.length > 1) {
                        _format = nameParts[1];
                    }

                    Object value = getValue(_key);

                    if (value == null) {
                        value = currentValue;
                    }

                    switch (_key){
                        case "now": {
                            switch (className) {
                                case "Surat": {
                                    Surat obj = (Surat) entityClass;
                                    if(obj.getBonNomor() != null && obj.getBonNomor().booleanValue()) {
                                        DateFormat df = new SimpleDateFormat(_format);
                                        return df.format(obj.getApprovedDate());
                                    }else{
                                        DateFormat df = new SimpleDateFormat(_format);
                                        return df.format(value);
                                    }
                                }
                                default: {
                                    DateFormat df = new SimpleDateFormat(_format);
                                    return df.format(value);
                                }
                            }
                        }
                        case "rom_month":{
                            switch (className) {
                                case "Surat": {
                                    Surat obj = (Surat) entityClass;
                                    if (obj.getBonNomor() != null && obj.getBonNomor().booleanValue()) {
                                        return RomanNumeralConverter.Convert(obj.getApprovedDate().getMonth() + 1);
                                    }else{
                                        return RomanNumeralConverter.Convert(new Date().getMonth() + 1);
                                    }
                                }

                                default:{
                                    return RomanNumeralConverter.Convert(new Date().getMonth() + 1);
                                }
                            }
                        }
                        case "value": {
                            if (!StringUtils.isEmpty(_format)) {
                                return String.format("%0" + _format.length() + "d", value);
                            }
                        }
                        case "Counter": {
                            if (!StringUtils.isEmpty(_format)) {
                                String counter = next("Counter"+_format);
                                return counter;
                            }
                        }
                        case "MasterJenisDokumen": {
                            switch (className) {
                                case "Surat": {
                                    Surat surat = (Surat)entityClass;
                                    try {
                                        // < SAMPEL MENGGUNAKAN REFLECTION
                                        // Field[] fields = Surat.class.getDeclaredFields();
                                        // Field formDefinitionField = Surat.class.getField("formDefinition");
                                        // formDefinitionField.setAccessible(true);
                                        // FormDefinition formDefinition = (FormDefinition) formDefinitionField.get(surat);
                                        // END REFLECTION >

                                        // get code jenis dokumen
                                        String code = surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen();
                                        return code;
                                    } catch (Exception ex) {
                                        return "";
                                    }
                                }

                                case "PenomoranManual": {
                                    PenomoranManual manual = (PenomoranManual) entityClass;
                                    try{
                                        String code = manual.getJenisDok().getKodeJenisDokumen();
                                        return code;
                                    }catch (Exception ex){
                                        return "";
                                    }
                                }
                            }
                        }
                        case "Jabatan": {
                            switch (className) {
                                case "Surat": {
                                    Surat surat = (Surat) entityClass;
                                    if(surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SME")){
                                        try{
                                            if (surat.getPemeriksaSurat().isEmpty()) return "";
                                            PemeriksaSurat pemeriksa = surat.getPemeriksaSurat().get(0);

                                            if(pemeriksa == null) return "";
                                            return pemeriksa.getOrganization().getSingkatan() != null ? pemeriksa.getOrganization().getSingkatan() : "";
                                        }catch (Exception ex){
                                            return "";
                                        }
                                    }else {
                                        try {
                                            // get code jenis klasifikasi keamanan
                                            if (surat.getPenandatanganSurat().isEmpty()) return "";
                                            PenandatanganSurat penandatangan = surat.getPenandatanganSurat().get(0);

                                            if (penandatangan.getOrganization() == null) return "";
                                            return penandatangan.getOrganization().getSingkatan() != null
                                                    ? penandatangan.getOrganization().getSingkatan() : "";
                                        } catch (Exception ex) {
                                            return "";
                                        }
                                    }
                                }

                                case "PenomoranManual": {
                                    PenomoranManual manual = (PenomoranManual) entityClass;

                                    try {
                                        if (manual.getPenandatangan().isEmpty()) return "";
                                        PenandatanganNomorManual penandatangan = manual.getPenandatangan().get(0);

                                        if (penandatangan.getOrganization() == null) return "";
                                        return penandatangan.getOrganization().getSingkatan() != null
                                                ? penandatangan.getOrganization().getSingkatan() : "";
                                    } catch (Exception ex) {
                                        return "";
                                    }
                                }
                            }
                        }
                        case "MasterKlasifikasiMasalah": {
                            switch (className) {
                                case "Surat": {
                                    Surat surat = (Surat) entityClass;

                                    // FormField :: table source name = nama class = MasterKlasifikasiMasalah
                                    try {
                                        // get code jenis klasifikasi keamanan
                                        FormField klasifikasiMasalahFormField = surat.getFormDefinition().getFormFields()
                                                .stream()
                                                .filter(q -> q.getType().equals("master-klasifikasi-masalah"))
                                                .findFirst().orElse(null);
                                        if (klasifikasiMasalahFormField == null) return "";
                                        FormValue klasifikasiMasalahFormValue = surat.getFormValue()
                                                .stream()
                                                .filter(q -> q.getFormField().getId().equals(klasifikasiMasalahFormField.getId()))
                                                .findFirst().orElse(null);

                                        String nama = "";

                                        if (klasifikasiMasalahFormValue == null){
                                            if(!Strings.isNullOrEmpty(surat.getKlasifikasiMasalah())){
                                                nama = surat.getKlasifikasiMasalah();
                                            }else{
                                                return "";
                                            }
                                        }else{
                                            nama = klasifikasiMasalahFormValue.getValue();
                                        }

                                        if (nama.contains("-")) {
                                            nama = Arrays.stream(nama.split("-")).reduce((first, second) -> second).map(String::trim).orElse("");
                                        }

                                        MasterKlasifikasiMasalah klasifikasiMasalah = klasifikasiMasalahService.findByNamaKlasifikasiMasalah(nama);
                                        if (klasifikasiMasalah == null) return "";

                                        return klasifikasiMasalah.getKodeKlasifikasiMasalah();
                                    } catch (Exception ex) {
                                        return "";
                                    }
                                }

                                case "PenomoranManual": {
                                    PenomoranManual manual = (PenomoranManual) entityClass;

                                    try {
                                        MasterKlasifikasiMasalah klasifikasiMasalah = manual.getKlasDokumen();
                                        if(klasifikasiMasalah != null) {
                                            return klasifikasiMasalah.getKodeKlasifikasiMasalah();
                                        }else{
                                            return "";
                                        }
                                    } catch (Exception ex) {
                                        return "";
                                    }
                                }
                            }
                        }
                        case "MasterKlasifikasiKeamanan": {
                            switch (className) {
                                case "Surat": {
                                    Surat surat = (Surat)entityClass;

                                    // FormField :: table source name = nama class = MasterKlasifikasiKeamanan
                                    try {
                                        // get code jenis klasifikasi keamanan
                                        FormField klasifikasiKeamananFormField = surat.getFormDefinition().getFormFields()
                                                .stream()
                                                .filter(q -> q.getType().equals("master-klasifikasi-keamanan"))
                                                .findFirst().orElse(null);
                                        if (klasifikasiKeamananFormField == null) return "";
                                        FormValue klasifikasiKeamananFormValue = surat.getFormValue()
                                                .stream()
                                                .filter(q -> q.getFormField().getId().equals(klasifikasiKeamananFormField.getId()))
                                                .findFirst().orElse(null);

                                        if (klasifikasiKeamananFormValue == null) return "";
                                        MasterKlasifikasiKeamanan klasifikasiKeamanan = klasifikasiKeamananService.findByNamaKlasifikasiKeamanan(klasifikasiKeamananFormValue.getValue());
                                        if (klasifikasiKeamanan == null) return "";

                                        return klasifikasiKeamanan.getKodeKlasifikasiKeamanan().equals("RH")
                                                ? "/"+klasifikasiKeamanan.getKodeKlasifikasiKeamanan()
                                                : "";
                                    } catch (Exception ex) {
                                        return "";
                                    }
                                }

                                case "PenomoranManual": {
                                    PenomoranManual manual = (PenomoranManual) entityClass;

                                    try {

                                        MasterKlasifikasiKeamanan klasifikasiKeamanan = manual.getKlasKeamanan();
                                        if (klasifikasiKeamanan == null) return "";

                                        return klasifikasiKeamanan.getKodeKlasifikasiKeamanan().equals("RH")
                                                ? "/"+klasifikasiKeamanan.getKodeKlasifikasiKeamanan()
                                                : "";
                                    } catch (Exception ex) {
                                        return "";
                                    }
                                }
                            }
                        }

                        case "MasterArea": {
                            switch (className) {
                                case "Surat": {
                                    Surat surat = (Surat)entityClass;

                                    /*Kondisi untuk handle Registrasi Dokumen Eksternal*/
                                    if(surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SME")){
                                        try{
                                            MasterUser konseptor = surat.getUserKonseptor();
                                            if(konseptor == null) return "";

                                            MasterArea area = konseptor.getArea();
                                            String kodeArea = area != null ? area.getSingkatan() != null ? area.getSingkatan() : "" : "";

                                            return kodeArea;
                                        }catch (Exception ex){
                                            return "";
                                        }
                                    }else {
                                        try {
                                            MasterArea area = surat.getArea();

                                            if (area == null) {
                                                return "";
                                            }

                                            return area.getSingkatan() != null ? area.getSingkatan() : "";
                                        } catch (Exception ex) {
                                            return "";
                                        }
                                    }
                                }

                                case "PenomoranManual": {
                                    PenomoranManual manual = (PenomoranManual) entityClass;

                                    try {
                                        MasterArea area = manual.getPenandatangan().get(0).getOrganization().getArea();

                                        if(area == null){
                                            return "";
                                        }

                                        return area.getSingkatan() != null ? area.getSingkatan() : "";
                                    } catch (Exception ex) {
                                        return "";
                                    }
                                }
                            }
                        }

                        case "unit":{
                            switch (className) {
                                case "MasterRuangArsip" : {
                                    MasterRuangArsip ruangArsip = (MasterRuangArsip) entityClass;
                                    try{
                                        MasterKorsa korsa = ruangArsip.getUnitKerja();
                                        if(korsa==null) return "";
                                        return korsa.getKode();
                                    }catch (Exception ex){
                                        return "";
                                    }
                                }
                                case "MasterLemariArsip":{
                                    MasterLemariArsip lemariArsip = (MasterLemariArsip) entityClass;
                                    try{
                                        MasterKorsa korsa = lemariArsip.getRuangArsip().getUnitKerja();
                                        if(korsa==null) return "";
                                        return korsa.getKode();
                                    }catch (Exception ex){
                                        return "";
                                    }
                                }

                                case "MasterBoksArsip":{
                                    MasterBoksArsip boksArsip = (MasterBoksArsip) entityClass;
                                    try{
                                        MasterTrapArsip tArsip = boksArsip.getTrapArsip();
                                        if(tArsip==null) return "";
                                        MasterLemariArsip lArsip = tArsip.getLemariArsip();
                                        if(lArsip==null) return "";
                                        MasterRuangArsip rArsip = lArsip.getRuangArsip();
                                        if(rArsip==null) return "";
                                        MasterKorsa korsa = rArsip.getUnitKerja();
                                        if(korsa==null) return "";
                                        return korsa.getKode();
                                    }catch(Exception ex){
                                        return "";
                                    }
                                }

                                case "Berkas" : {
                                    Berkas berkas = (Berkas) entityClass;
                                    try{
                                        MasterKorsa korsa = berkas.getUnit();
                                        if(korsa==null) return "";
                                        return korsa.getKode();
                                    }catch (Exception ex){
                                        return "";
                                    }
                                }

                                case "PermohonanBonNomor" : {
                                    PermohonanBonNomor bonNomor = (PermohonanBonNomor) entityClass;
                                    try{
                                        MasterKorsa korsa = bonNomor.getUserKonseptor().getKorsa();
                                        if(korsa==null) return "";
                                        return korsa.getKode();
                                    }catch (Exception ex){
                                        return "";
                                    }
                                }

                                case "Regulasi" : {
                                    Regulasi regulasi = (Regulasi) entityClass;
                                    try{
                                        MasterKorsa korsa = regulasi.getUnitKonseptor();
                                        if(korsa==null) return "";
                                        return korsa.getKode();
                                    }catch (Exception ex){
                                        return "";
                                    }
                                }
                            }
                        }

                        case "kode":{
                            switch (className){
                                case "MasterTrapArsip":{
                                    MasterTrapArsip trapArsip = (MasterTrapArsip) entityClass;
                                    try{
                                        String kode = "";
                                        MasterLemariArsip lArsip = trapArsip.getLemariArsip();
                                        String[] kodeParts = lArsip.getKode().split("\\.");
                                        if(kodeParts.length>1){
                                            kode = kodeParts[1]+"."+kodeParts[2];
                                        }
                                        return kode;
                                    }catch (Exception ex){
                                        return "";
                                    }
                                }
                            }
                        }

                        case "berkas":{
                            switch (className){
                                case "Arsip":{
                                    Arsip arsip = (Arsip) entityClass;
                                    try{
                                        String kode = arsip.getBerkas().getNomor();
                                        return kode;
                                    }catch (Exception ex){
                                        return "";
                                    }
                                }
                            }
                        }
                    }

                    return value.toString();
                }
            };

            StrSubstitutor sub = new StrSubstitutor(strLookup);
            sub.setVariablePrefix("{");
            sub.setVariableSuffix("}");

            autoNumberFacade.edit(current);

            return sub.replace(current.getFormat());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "-1";
    }

    private String getIdentifier(Object entityClass, String fieldName) {
        String identifier = "";
        if (entityClass != null) {

            switch (entityClass.getClass().getSimpleName()) {
                case "Surat": {
                    Surat surat = (Surat) entityClass;

                    if(fieldName.equals("getNoDokumen")) {
                        if(!surat.getFormDefinition().getJenisDokumen().getBaseCode().equals("SD")) {

                            String documentCode = "";
                            try {
                                documentCode = surat.getFormDefinition().getJenisDokumen().getGroup().getKodeJenisDokumen();
                            } catch (Exception ex) {
                                try {
                                    documentCode = surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen();
                                } catch (Exception inner) {
                                    documentCode = "";
                                }
                            }

                            String securityCode = "";
                            try {
                                FormField klasifikasiKeamananFormField = surat.getFormDefinition().getFormFields()
                                        .stream()
                                        .filter(q -> q.getType().equals("master-klasifikasi-keamanan"))
                                        .findFirst().orElse(null);

                                if (klasifikasiKeamananFormField != null) {
                                    FormValue klasifikasiKeamananFormValue = surat.getFormValue()
                                            .stream()
                                            .filter(q -> q.getFormField().getId().equals(klasifikasiKeamananFormField.getId()))
                                            .findFirst().orElse(null);

                                    if (klasifikasiKeamananFormValue != null) {
                                        MasterKlasifikasiKeamanan klasifikasiKeamanan = klasifikasiKeamananService.findByNamaKlasifikasiKeamanan(klasifikasiKeamananFormValue.getValue());
                                        securityCode = klasifikasiKeamanan.getKodeKlasifikasiKeamanan();
                                    }
                                }

                            } catch (Exception ex) {
                                securityCode = "";
                            }

                            if (!Strings.isNullOrEmpty(documentCode)) {
                                identifier = documentCode;
                            }

                            if (!Strings.isNullOrEmpty(securityCode)) {
                                if (Strings.isNullOrEmpty(identifier)) {
                                    identifier = securityCode;
                                } else {
                                    identifier = identifier + ";" + securityCode;
                                }
                            }

                            String classDoc = "";

                            try {

                                FormField klasifikasiMasalahFormField = surat.getFormDefinition().getFormFields()
                                        .stream()
                                        .filter(q -> q.getType().equals("master-klasifikasi-masalah"))
                                        .findFirst().orElse(null);
                                if (klasifikasiMasalahFormField == null) return "";
                                FormValue klasifikasiMasalahFormValue = surat.getFormValue()
                                        .stream()
                                        .filter(q -> q.getFormField().getId().equals(klasifikasiMasalahFormField.getId()))
                                        .findFirst().orElse(null);

                                String nama = "";

                                if (klasifikasiMasalahFormValue == null) {
                                    if (!Strings.isNullOrEmpty(surat.getKlasifikasiMasalah())) {
                                        nama = surat.getKlasifikasiMasalah();
                                    } else {
                                        classDoc = "";
                                    }
                                } else {
                                    nama = klasifikasiMasalahFormValue.getValue();
                                }

                                if (nama.contains("-")) {
                                    nama = Arrays.stream(nama.split("-")).reduce((first, second) -> second).map(String::trim).orElse("");
                                }

                                MasterKlasifikasiMasalah klasifikasiMasalah = klasifikasiMasalahService.findByNamaKlasifikasiMasalah(nama);
                                if (klasifikasiMasalah == null) classDoc = "";

                                classDoc =  klasifikasiMasalah.getKodeKlasifikasiMasalah();
                            }catch (Exception ex){
                                classDoc = "";
                            }

                            if(!Strings.isNullOrEmpty(classDoc)){
                                if(Strings.isNullOrEmpty(identifier)){
                                    identifier = classDoc;
                                }else{
                                    identifier = identifier + ";" + classDoc;
                                }
                            }

                            String area = "";
                            try {
                                area = surat.getArea().getSingkatan();
                            } catch (Exception ex) {
                                area = "";
                            }

                            if (!Strings.isNullOrEmpty(area)) {
                                if (Strings.isNullOrEmpty(identifier)) {
                                    identifier = area;
                                } else {
                                    identifier = identifier + ";" + area;
                                }
                            }

                            return identifier;
                        }else{
                            String classDoc = "";

                            try {

                                FormField klasifikasiMasalahFormField = surat.getFormDefinition().getFormFields()
                                        .stream()
                                        .filter(q -> q.getType().equals("master-klasifikasi-masalah"))
                                        .findFirst().orElse(null);
                                if (klasifikasiMasalahFormField == null) return "";
                                FormValue klasifikasiMasalahFormValue = surat.getFormValue()
                                        .stream()
                                        .filter(q -> q.getFormField().getId().equals(klasifikasiMasalahFormField.getId()))
                                        .findFirst().orElse(null);

                                String nama = "";

                                if (klasifikasiMasalahFormValue == null) {
                                    if (!Strings.isNullOrEmpty(surat.getKlasifikasiMasalah())) {
                                        nama = surat.getKlasifikasiMasalah();
                                    } else {
                                        classDoc = "";
                                    }
                                } else {
                                    nama = klasifikasiMasalahFormValue.getValue();
                                }

                                if (nama.contains("-")) {
                                    nama = Arrays.stream(nama.split("-")).reduce((first, second) -> second).map(String::trim).orElse("");
                                }

                                MasterKlasifikasiMasalah klasifikasiMasalah = klasifikasiMasalahService.findByNamaKlasifikasiMasalah(nama);
                                if (klasifikasiMasalah == null) classDoc = "";

                                classDoc =  klasifikasiMasalah.getKodeKlasifikasiMasalah();
                            }catch (Exception ex){
                                classDoc = "";
                            }

                            String securityCode = "";
                            try {
                                FormField klasifikasiKeamananFormField = surat.getFormDefinition().getFormFields()
                                        .stream()
                                        .filter(q -> q.getType().equals("master-klasifikasi-keamanan"))
                                        .findFirst().orElse(null);

                                if (klasifikasiKeamananFormField != null) {
                                    FormValue klasifikasiKeamananFormValue = surat.getFormValue()
                                            .stream()
                                            .filter(q -> q.getFormField().getId().equals(klasifikasiKeamananFormField.getId()))
                                            .findFirst().orElse(null);

                                    if (klasifikasiKeamananFormValue != null) {
                                        MasterKlasifikasiKeamanan klasifikasiKeamanan = klasifikasiKeamananService.findByNamaKlasifikasiKeamanan(klasifikasiKeamananFormValue.getValue());
                                        securityCode = klasifikasiKeamanan.getKodeKlasifikasiKeamanan();
                                    }
                                }

                            } catch (Exception ex) {
                                securityCode = "";
                            }

                            String area = "";
                            try {
                                area = surat.getArea().getSingkatan();
                            } catch (Exception ex) {
                                area = "";
                            }

                            if (!Strings.isNullOrEmpty(classDoc)) {
                                identifier = classDoc;
                            }

                            if (!Strings.isNullOrEmpty(securityCode)) {
                                if (Strings.isNullOrEmpty(identifier)) {
                                    identifier = securityCode;
                                } else {
                                    identifier = identifier + ";" + securityCode;
                                }
                            }

                            if (!Strings.isNullOrEmpty(area)) {
                                if (Strings.isNullOrEmpty(identifier)) {
                                    identifier = area;
                                } else {
                                    identifier = identifier + ";" + area;
                                }
                            }

                            return identifier;
                        }

                    }

                    if(fieldName.equals("getNoAgenda")){
                        String classDoc = "";

                        try {

                            FormField klasifikasiMasalahFormField = surat.getFormDefinition().getFormFields()
                                    .stream()
                                    .filter(q -> q.getType().equals("master-klasifikasi-masalah"))
                                    .findFirst().orElse(null);
                            if (klasifikasiMasalahFormField == null) return "";
                            FormValue klasifikasiMasalahFormValue = surat.getFormValue()
                                    .stream()
                                    .filter(q -> q.getFormField().getId().equals(klasifikasiMasalahFormField.getId()))
                                    .findFirst().orElse(null);

                            String nama = "";

                            if (klasifikasiMasalahFormValue == null) {
                                if (!Strings.isNullOrEmpty(surat.getKlasifikasiMasalah())) {
                                    nama = surat.getKlasifikasiMasalah();
                                } else {
                                    classDoc = "";
                                }
                            } else {
                                nama = klasifikasiMasalahFormValue.getValue();
                            }

                            if (nama.contains("-")) {
                                nama = Arrays.stream(nama.split("-")).reduce((first, second) -> second).map(String::trim).orElse("");
                            }

                            MasterKlasifikasiMasalah klasifikasiMasalah = klasifikasiMasalahService.findByNamaKlasifikasiMasalah(nama);
                            if (klasifikasiMasalah == null) classDoc = "";

                            classDoc =  klasifikasiMasalah.getKodeKlasifikasiMasalah();
                        }catch (Exception ex){
                            classDoc = "";
                        }

                        String securityCode = "";
                        try {
                            FormField klasifikasiKeamananFormField = surat.getFormDefinition().getFormFields()
                                    .stream()
                                    .filter(q -> q.getType().equals("master-klasifikasi-keamanan"))
                                    .findFirst().orElse(null);

                            if (klasifikasiKeamananFormField != null) {
                                FormValue klasifikasiKeamananFormValue = surat.getFormValue()
                                        .stream()
                                        .filter(q -> q.getFormField().getId().equals(klasifikasiKeamananFormField.getId()))
                                        .findFirst().orElse(null);

                                if (klasifikasiKeamananFormValue != null) {
                                    MasterKlasifikasiKeamanan klasifikasiKeamanan = klasifikasiKeamananService.findByNamaKlasifikasiKeamanan(klasifikasiKeamananFormValue.getValue());
                                    securityCode = klasifikasiKeamanan.getKodeKlasifikasiKeamanan();
                                }
                            }

                        } catch (Exception ex) {
                            securityCode = "";
                        }

                        String area = "";
                        try {
                            area = surat.getArea().getSingkatan();
                        } catch (Exception ex) {
                            area = "";
                        }

                        if (!Strings.isNullOrEmpty(classDoc)) {
                            identifier = classDoc;
                        }

                        if (!Strings.isNullOrEmpty(securityCode)) {
                            if (Strings.isNullOrEmpty(identifier)) {
                                identifier = securityCode;
                            } else {
                                identifier = identifier + ";" + securityCode;
                            }
                        }

                        if (!Strings.isNullOrEmpty(area)) {
                            if (Strings.isNullOrEmpty(identifier)) {
                                identifier = area;
                            } else {
                                identifier = identifier + ";" + area;
                            }
                        }

                        return identifier;
                    }
                }

                case "PenomoranManual": {
                    PenomoranManual manual = (PenomoranManual) entityClass;

                    String documentClassification = "";
                    try {
                        documentClassification = manual.getJenisDok().getGroup().getKodeJenisDokumen();
                    } catch (Exception ex) {
                        try {
                            documentClassification = manual.getJenisDok().getKodeJenisDokumen();
                        } catch (Exception inner) {
                            documentClassification = "";
                        }
                    }

                    String securityCode = "";
                    try {
                        MasterKlasifikasiKeamanan klasifikasiKeamanan = manual.getKlasKeamanan();
                        securityCode = klasifikasiKeamanan.getKodeKlasifikasiKeamanan();
                    } catch (Exception ex) {
                        securityCode = "";
                    }

                    String area = "";
                    try {
                        area = manual.getPenandatangan().get(0).getOrganization().getArea().getSingkatan();
                    } catch (Exception ex) {
                        area = "";
                    }

                    if (!Strings.isNullOrEmpty(documentClassification)) {
                        identifier = documentClassification;
                    }

                    if (!Strings.isNullOrEmpty(securityCode)) {
                        if (Strings.isNullOrEmpty(identifier)) {
                            identifier = securityCode;
                        } else {
                            identifier = identifier + ";" + securityCode;
                        }
                    }

                    if (!Strings.isNullOrEmpty(area)) {
                        identifier = area;
                    }

                    return identifier;
                }
            }
        }

        return identifier;
    }

    private String getMainIdentifier(Object entityClass, String field) {
        String identifier = "";
        if (entityClass != null) {

            switch (entityClass.getClass().getSimpleName()) {
                case "Surat": {
                    Surat surat = (Surat) entityClass;

                    if(!Strings.isNullOrEmpty(field) && field.equals("getNoDokumen")) {
                        if(!surat.getFormDefinition().getJenisDokumen().getBaseCode().equals("SD")) {

                            String documentGroup = "";
                            try {
                                documentGroup = surat.getFormDefinition().getJenisDokumen().getGroup().getKodeJenisDokumen();
                            } catch (Exception ex) {
                                try {
                                    documentGroup = surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen();
                                } catch (Exception inner) {
                                    documentGroup = "";
                                }
                            }

                            if (!Strings.isNullOrEmpty(documentGroup)) {
                                identifier = documentGroup;
                            }

//                            String jabatan = "";
//                            try {
//                                if (surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SME")) {
//                                    try {
//                                        if (surat.getPemeriksaSurat().isEmpty()) jabatan = "";
//                                        PemeriksaSurat pemeriksa = surat.getPemeriksaSurat().get(0);
//
//                                        if (pemeriksa == null) jabatan = "";
//                                        jabatan = pemeriksa.getOrganization().getSingkatan() != null ? pemeriksa.getOrganization().getSingkatan() : "";
//                                    } catch (Exception ex) {
//                                        return "";
//                                    }
//                                } else {
//                                    try {
//                                        if (surat.getPenandatanganSurat().isEmpty()) jabatan = "";
//                                        PenandatanganSurat penandatangan = surat.getPenandatanganSurat().get(0);
//
//                                        if (penandatangan.getOrganization() == null) jabatan = "";
//                                        jabatan = penandatangan.getOrganization().getSingkatan() != null
//                                                ? penandatangan.getOrganization().getSingkatan() : "";
//                                    } catch (Exception ex) {
//                                        return "";
//                                    }
//                                }
//                            } catch (Exception ex) {
//                                jabatan = "";
//                            }
//
//                            if (!Strings.isNullOrEmpty(jabatan)) {
//                                identifier = identifier + ";" + jabatan;
//                            }

                            String classCode = "";
                            try {
                                FormField klasifikasiMasalahFormField = surat.getFormDefinition().getFormFields()
                                        .stream()
                                        .filter(q -> q.getType().equals("master-klasifikasi-masalah"))
                                        .findFirst().orElse(null);
                                if (klasifikasiMasalahFormField == null) return "";
                                FormValue klasifikasiMasalahFormValue = surat.getFormValue()
                                        .stream()
                                        .filter(q -> q.getFormField().getId().equals(klasifikasiMasalahFormField.getId()))
                                        .findFirst().orElse(null);

                                String nama = "";

                                if (klasifikasiMasalahFormValue == null) {
                                    if (!Strings.isNullOrEmpty(surat.getKlasifikasiMasalah())) {
                                        nama = surat.getKlasifikasiMasalah();
                                    } else {
                                        classCode = "";
                                    }
                                } else {
                                    nama = klasifikasiMasalahFormValue.getValue();
                                }

                                if (nama.contains("-")) {
                                    nama = Arrays.stream(nama.split("-")).reduce((first, second) -> second).map(String::trim).orElse("");
                                }

                                MasterKlasifikasiMasalah klasifikasiMasalah = klasifikasiMasalahService.findByNamaKlasifikasiMasalah(nama);
                                if (klasifikasiMasalah == null) classCode = "";

                                classCode = klasifikasiMasalah.getKodeKlasifikasiMasalah();
                            } catch (Exception ex) {
                                try {
                                    classCode = surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen();
                                } catch (Exception inner) {
                                    classCode = "";
                                }
                            }

                            if (!Strings.isNullOrEmpty(classCode)) {
                                if (Strings.isNullOrEmpty(identifier)) {
                                    identifier = classCode;
                                } else {
                                    identifier = identifier + ";" + classCode;
                                }
                            }

                            return identifier;
                        }else{
                            String classCode = "";
                            try {
                                FormField klasifikasiMasalahFormField = surat.getFormDefinition().getFormFields()
                                        .stream()
                                        .filter(q -> q.getType().equals("master-klasifikasi-masalah"))
                                        .findFirst().orElse(null);
                                if (klasifikasiMasalahFormField == null) return "";
                                FormValue klasifikasiMasalahFormValue = surat.getFormValue()
                                        .stream()
                                        .filter(q -> q.getFormField().getId().equals(klasifikasiMasalahFormField.getId()))
                                        .findFirst().orElse(null);

                                String nama = "";

                                if (klasifikasiMasalahFormValue == null){
                                    if(!Strings.isNullOrEmpty(surat.getKlasifikasiMasalah())){
                                        nama = surat.getKlasifikasiMasalah();
                                    }else{
                                        classCode =  "";
                                    }
                                }else{
                                    nama = klasifikasiMasalahFormValue.getValue();
                                }

                                if (nama.contains("-")) {
                                    nama = Arrays.stream(nama.split("-")).reduce((first, second) -> second).map(String::trim).orElse("");
                                }

                                MasterKlasifikasiMasalah klasifikasiMasalah = klasifikasiMasalahService.findByNamaKlasifikasiMasalah(nama);
                                if (klasifikasiMasalah == null) classCode =  "";

                                classCode =  klasifikasiMasalah.getKodeKlasifikasiMasalah();
                            } catch (Exception ex) {
                                try {
                                    classCode = surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen();
                                } catch (Exception inner) {
                                    classCode = "";
                                }
                            }

                            if (!Strings.isNullOrEmpty(classCode)) {
                                identifier = classCode;
                            }

                            return identifier;
                        }
                    }
                    if(!Strings.isNullOrEmpty(field) && field.equals("getNoAgenda")) {

                        String classCode = "";
                        try {
                            FormField klasifikasiMasalahFormField = surat.getFormDefinition().getFormFields()
                                    .stream()
                                    .filter(q -> q.getType().equals("master-klasifikasi-masalah"))
                                    .findFirst().orElse(null);
                            if (klasifikasiMasalahFormField == null) return "";
                            FormValue klasifikasiMasalahFormValue = surat.getFormValue()
                                    .stream()
                                    .filter(q -> q.getFormField().getId().equals(klasifikasiMasalahFormField.getId()))
                                    .findFirst().orElse(null);

                            String nama = "";

                            if (klasifikasiMasalahFormValue == null){
                                if(!Strings.isNullOrEmpty(surat.getKlasifikasiMasalah())){
                                    nama = surat.getKlasifikasiMasalah();
                                }else{
                                    classCode =  "";
                                }
                            }else{
                                nama = klasifikasiMasalahFormValue.getValue();
                            }

                            if (nama.contains("-")) {
                                nama = Arrays.stream(nama.split("-")).reduce((first, second) -> second).map(String::trim).orElse("");
                            }

                            MasterKlasifikasiMasalah klasifikasiMasalah = klasifikasiMasalahService.findByNamaKlasifikasiMasalah(nama);
                            if (klasifikasiMasalah == null) classCode =  "";

                            classCode =  klasifikasiMasalah.getKodeKlasifikasiMasalah();
                        } catch (Exception ex) {
                            try {
                                classCode = surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen();
                            } catch (Exception inner) {
                                classCode = "";
                            }
                        }

                        if (!Strings.isNullOrEmpty(classCode)) {
                            identifier = classCode;
                        }

                        return identifier;
                    }
                }

                case "PenomoranManual": {
                    PenomoranManual manual = (PenomoranManual) entityClass;

                    String documentGroup = "";
                    try {
                        documentGroup = manual.getJenisDok().getGroup().getKodeJenisDokumen();
                    } catch (Exception ex) {
                        try {
                            documentGroup = manual.getJenisDok().getKodeJenisDokumen();
                        } catch (Exception inner) {
                            documentGroup = "";
                        }
                    }

                    if (!Strings.isNullOrEmpty(documentGroup)) {
                        identifier = documentGroup;
                    }

                    return identifier;
                }

                case "MasterRuangArsip": {
                    MasterRuangArsip ruangArsip = (MasterRuangArsip) entityClass;
                    identifier = ruangArsip.getUnitKerja().getKode();
                    return identifier;
                }

                case "MasterLemariArsip": {
                    MasterLemariArsip lemariArsip = (MasterLemariArsip) entityClass;
                    identifier = lemariArsip.getRuangArsip().getKode();
                    return identifier;
                }

                case "MasterTrapArsip":{
                    MasterTrapArsip trapArsip = (MasterTrapArsip) entityClass;
                    identifier = trapArsip.getLemariArsip().getKode();
                    return identifier;
                }

                case "MasterBoksArsip":{
                    MasterBoksArsip boksArsip = (MasterBoksArsip) entityClass;
                    identifier = boksArsip.getTrapArsip().getKode();
                    return identifier;
                }

                case "GlobalAttachment":{
                    GlobalAttachment gA = (GlobalAttachment) entityClass;
                    Surat surat = suratService.getByIdSurat(gA.getReferenceId());
                    identifier = gA.getIsKonsep().booleanValue() == true ? "Dibuat" : "Diterima";
                    return identifier;
                }
            }
        }
        return identifier;
    }

    private void validateCounterExpiration(MasterAutoNumber autoNumber) {
        Calendar nextReset = Calendar.getInstance();

        if (!StringUtils.isEmpty(autoNumber.getResetPeriod()) && nextReset.getTime().compareTo(autoNumber.getNextReset()) >= 0) {
            if (autoNumber.getResetPeriod().equals("YEAR")) {
                nextReset.add(Calendar.YEAR, 1);
                nextReset.set(nextReset.get(Calendar.YEAR), Calendar.JANUARY, 1, 0, 0, 0);
                autoNumber.setNextReset(nextReset.getTime());
                autoNumber.setValue(0);
            } else if(autoNumber.getResetPeriod().equals("MONTH")) {
                nextReset.add(Calendar.MONTH, 1);
                nextReset.set(nextReset.get(Calendar.YEAR), nextReset.get(Calendar.MONTH), 1, 0, 0, 0);
                autoNumber.setNextReset(nextReset.getTime());
                autoNumber.setValue(0);
            } else if(autoNumber.getResetPeriod().equals("MINUTE")) {
                nextReset.add(Calendar.MINUTE, 10);

                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH);
                int date = Calendar.getInstance().get(Calendar.DATE);
                int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                nextReset.set(year, month, date, hours, nextReset.get(Calendar.MINUTE), 0);
                autoNumber.setNextReset(nextReset.getTime());
                autoNumber.setValue(0);
            }
        }
    }

    private Object getValue(String key) {
        Supplier<Object> handler = ValidTokens.get(key);

        if (handler != null) {
            return handler.get();
        }

        return null;
    }

    public void setRecorder(Recorder recorder) {
        this.recorder = recorder;
    }

    public void setObject(Class<T> object) {
        this.object = object;
    }
}
