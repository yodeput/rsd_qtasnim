package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.SyncLog;
import com.qtasnim.eoffice.db.SyncLogStatus;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

@Stateless
@LocalBean
public class SyncLogService extends AbstractFacade<SyncLog> {
    @Override
    protected Class<SyncLog> getEntityClass() {
        return SyncLog.class;
    }

    @Inject
    private SyncLogDetailService syncLogDetailService;

    public JinqStream<SyncLog> findByType(String type) {
        return db().where(t -> t.getType().equals(type)).sortedDescendingBy(SyncLog::getId);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public SyncLog start(String type) {
        SyncLog syncLog = new SyncLog();
        syncLog.setType(type);
        syncLog.setStatus(SyncLogStatus.RUNNING);

        create(syncLog);

        return syncLog;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void stop(SyncLog syncLog, Instant startTime) {
        Instant end = Instant.now();
        syncLog.setCompletedDate(toDate(end));
        syncLog.setDuration(Duration.between(startTime, end).getSeconds());

        if (syncLog.getStatus().equals(SyncLogStatus.RUNNING)) {
            syncLog.setStatus(SyncLogStatus.SUCCESSFUL);
        }

        edit(syncLog);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void error(SyncLog syncLog) {
        syncLog.setStatus(SyncLogStatus.ERROR);

        edit(syncLog);
    }

    private Date toDate(Instant instant) {
        BigInteger milis = BigInteger.valueOf(instant.getEpochSecond()).multiply(
                BigInteger.valueOf(1000));
        milis = milis.add(BigInteger.valueOf(instant.getNano()).divide(
                BigInteger.valueOf(1_000_000)));
        return new Date(milis.longValue());
    }

    public SyncLog findLatestLog(String type) {
        return db()
                .where(t -> t.getType().equals(type))
                .sortedDescendingBy(SyncLog::getId)
                .limit(1)
                .findFirst()
                .orElse(null);
    }
}
