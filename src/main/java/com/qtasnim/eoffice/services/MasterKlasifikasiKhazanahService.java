package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterKlasifikasiKhazanah;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterKlasifikasiKhazanahDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class MasterKlasifikasiKhazanahService extends AbstractFacade<MasterKlasifikasiKhazanah> {

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterKlasifikasiKhazanah> getEntityClass() {
        return MasterKlasifikasiKhazanah.class;
    }

    public JPAJinqStream<MasterKlasifikasiKhazanah> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterKlasifikasiKhazanah> getFiltered(String kode, String nama, Boolean isActive) {
        JPAJinqStream<MasterKlasifikasiKhazanah> query = db();

        if (!Strings.isNullOrEmpty(nama)) {
            query = query.where(q -> q.getNama().toLowerCase().contains(nama.toLowerCase()));
        }

        return query;
    }

    public void saveOrEdit(MasterKlasifikasiKhazanahDto dao, Long id) {
        boolean isNew = id == null;
        MasterKlasifikasiKhazanah model = new MasterKlasifikasiKhazanah();
        DateUtil dateUtil = new DateUtil();

        if (isNew) {
            model.setNama(dao.getNama());

            if (dao.getIdCompany() != null) {
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
            } else {
                model.setCompanyCode(null);
            }

            if (dao.getIdParent() != null) {
                MasterKlasifikasiKhazanah parent = this.find(dao.getIdParent());
                model.setParent(parent);
            } else {
                model.setParent(null);
            }

            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if (dao.getEndDate() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.create(model);
        } else {
            model = this.find(id);
            model.setNama(dao.getNama());

            if (dao.getIdCompany() != null) {
                CompanyCode company = companyCodeService.find(dao.getIdCompany());
                model.setCompanyCode(company);
            } else {
                model.setCompanyCode(null);
            }

            if (dao.getIdParent() != null) {
                MasterKlasifikasiKhazanah parent = this.find(dao.getIdParent());
                model.setParent(parent);
            } else {
                model.setParent(null);
            }

            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if (dao.getEndDate() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }
    }

    public JPAJinqStream<MasterKlasifikasiKhazanah> getByParentName(String parentName, String filter, String sort, Boolean descending) {
        JPAJinqStream<MasterKlasifikasiKhazanah> children = null;
        if (Strings.isNullOrEmpty(parentName)) {
            children = this.getByParent(null, sort, descending);
        } else {
            String parentStr = parentName.toLowerCase();
            MasterKlasifikasiKhazanah parent = db().where(q -> q.getNama().toLowerCase().equals(parentStr)).findFirst().orElse(null);
            if (parent != null) {
                Long id = parent.getId();
                children = this.getByParent(id, sort, descending);
            }
        }
        
        if (!Strings.isNullOrEmpty(filter) && children != null) {
            String filterStr = filter.toLowerCase();
            return children.where(q -> q.getNama().toLowerCase().contains(filterStr));
        }
        
        return children;
    }

    public JPAJinqStream<MasterKlasifikasiKhazanah> getByParent(Long parent, String sort, Boolean descending) {
        Date n = new Date();
        JPAJinqStream<MasterKlasifikasiKhazanah> query = db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
//        JPAJinqStream<MasterKlasifikasiKhazanah> query = db().where(q -> n.compareTo(q.getStart()) >= 0 && n.compareTo(q.getEnd()) <= 0);
        if (parent == null) {
            query = query.where(q -> q.getParent().equals(null));
        } else {
            MasterKlasifikasiKhazanah objParent = this.find(parent);
            query = query.where(q -> q.getParent().equals(objParent));
        }

        if (!Strings.isNullOrEmpty(sort)) {
            if (descending) {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedDescendingBy(q -> q.getNama());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedDescendingBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedDescendingBy(q -> q.getEnd());
                }
            } else {
                if (sort.toLowerCase().equals("nama")) {
                    query = query.sortedBy(q -> q.getNama());
                }
                if (sort.toLowerCase().equals("startdate")) {
                    query = query.sortedBy(q -> q.getStart());
                }
                if (sort.toLowerCase().equals("enddate")) {
                    query = query.sortedBy(q -> q.getEnd());
                }
            }
        }
        return query;
    }
}
