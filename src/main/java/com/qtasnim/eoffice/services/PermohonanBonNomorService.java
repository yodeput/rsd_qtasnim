package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.*;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class PermohonanBonNomorService extends AbstractFacade<PermohonanBonNomor> {

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterJenisDokumenService jenisDokumenService;

    @Inject
    private PemeriksaBonNomorService pemeriksaBonNomorService;

    @Inject
    private PenandatanganBonNomorService penandatanganBonNomorService;

    @Override
    protected Class<PermohonanBonNomor> getEntityClass() {
        return PermohonanBonNomor.class;
    }

    public JPAJinqStream<PermohonanBonNomor> getAll() {
        return db().where(q -> !q.getIsDeleted());
    }

    public JPAJinqStream<PermohonanBonNomor> getByKonseptor() {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        return db().where(q -> !q.getIsDeleted()&&q.getOrgKonseptor().equals(org));
    }

    public JPAJinqStream<PermohonanBonNomor> getByPemeriksa() {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        return db().where(q -> !q.getIsDeleted() && q.getPemeriksaList().contains(org));
    }

    public JPAJinqStream<PermohonanBonNomor> getByPenandatangan() {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        return db().where(q -> !q.getIsDeleted() && q.getPenandatanganList().contains(org));
    }


    public PermohonanBonNomorDto saveOrEdit(Long id, PermohonanBonNomorDto dao) {
        DateUtil dateUtil = new DateUtil();
        PermohonanBonNomorDto result;
        PermohonanBonNomor model = new PermohonanBonNomor();

        if (id == null) {
            model.setNoRegistrasi(UUID.randomUUID().toString());
            model.setIsDeleted(dao.getIsDeleted());
            model.setOrgKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
            model.setUserKonseptor(userSession.getUserSession().getUser());
            model.setStatus(dao.getStatus());
            model.setPerihal(dao.getPerihal());
            if (dao.getTglTarget() != null) {
                model.setTglTarget(dateUtil.getDateFromISOString(dao.getTglTarget()));
            }

            CompanyCode company = companyCodeService.find(dao.getIdCompany());
            model.setCompanyCode(company);

            MasterJenisDokumen jenisDokumen = jenisDokumenService.find(dao.getIdJenisDokumen());
            model.setJenisDokumen(jenisDokumen);

            this.create(model);
            getEntityManager().flush();

            /*save pemeriksa*/
            if (!dao.getPemeriksaBon().isEmpty()) {
                for (PemeriksaBonNomorDto pem : dao.getPemeriksaBon().stream().distinct().collect(Collectors.toList())) {
                    PermohonanBonNomor permohonanBonNomor = this.find(model.getId());
                    MasterStrukturOrganisasi pemeriksa = organisasiService.find(pem.getIdOrgPemeriksa());
                    PemeriksaBonNomor pemeriksaBonNomor = new PemeriksaBonNomor();
                    pemeriksaBonNomor.setPermohonan(permohonanBonNomor);
                    pemeriksaBonNomor.setOrganisasi(pemeriksa);
                    pemeriksaBonNomorService.create(pemeriksaBonNomor);
                }
            }
            /*end save pemeriksa*/

            /*save penandatangan*/
            if (!dao.getPenandatanganBon().isEmpty()) {
                for (PenandatanganBonNomorDto pem : dao.getPenandatanganBon().stream().distinct().collect(Collectors.toList())) {
                    PermohonanBonNomor permohonanBonNomor = this.find(model.getId());
                    MasterStrukturOrganisasi pemeriksa = organisasiService.find(pem.getIdOrgPenandatangan());
                    PenandatanganBonNomor penandatanganBonNomor = new PenandatanganBonNomor();
                    penandatanganBonNomor.setPermohonan(permohonanBonNomor);
                    penandatanganBonNomor.setOrganisasi(pemeriksa);
                    penandatanganBonNomorService.create(penandatanganBonNomor);
                }
            }
            /*end save penandatangan*/
        } else {
            model = this.find(id);
            model.setNoRegistrasi(dao.getNoRegistrasi());
            model.setIsDeleted(dao.getIsDeleted());
            model.setOrgKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
            model.setUserKonseptor(userSession.getUserSession().getUser());
            model.setStatus(dao.getStatus());
            model.setPerihal(dao.getPerihal());
            if (dao.getTglTarget() != null) {
                model.setTglTarget(dateUtil.getDateFromISOString(dao.getTglTarget()));
            }

            CompanyCode company = companyCodeService.find(dao.getIdCompany());
            model.setCompanyCode(company);

            MasterJenisDokumen jenisDokumen = jenisDokumenService.find(dao.getIdJenisDokumen());
            model.setJenisDokumen(jenisDokumen);
            this.edit(model);
            getEntityManager().flush();
/*
            if (!dao.getPemeriksaBon().isEmpty()) {
                for (PemeriksaBonNomorDto pem : dao.getPemeriksaBon().stream().distinct().collect(Collectors.toList())) {
                    PermohonanBonNomor permohonanBonNomor = this.find(model.getId());
                    MasterStrukturOrganisasi pemeriksa = organisasiService.find(pem.getIdOrgPemeriksa());
                    PemeriksaBonNomor pemeriksaBonNomor = pemeriksaBonNomorService.getByIdAndOrg(permohonanBonNomor.getId(),pem.getIdOrgPemeriksa());
                    if (pemeriksaBonNomor != null) {
                        pemeriksaBonNomor.setPermohonan(permohonanBonNomor);
                        pemeriksaBonNomor.setOrganisasi(pemeriksa);
                        pemeriksaBonNomorService.edit(pemeriksaBonNomor);
                    } else {
                        pemeriksaBonNomor = new PemeriksaBonNomor();
                        pemeriksaBonNomor.setPermohonan(permohonanBonNomor);
                        pemeriksaBonNomor.setOrganisasi(pemeriksa);
                        pemeriksaBonNomorService.create(pemeriksaBonNomor);
                    }

                }
            }

            if (!dao.getPenandatanganBon().isEmpty()) {
                for (PenandatanganBonNomorDto pem : dao.getPenandatanganBon().stream().distinct().collect(Collectors.toList())) {
                    PermohonanBonNomor permohonanBonNomor = this.find(model.getId());
                    MasterStrukturOrganisasi pemeriksa = organisasiService.find(pem.getIdOrgPenandatangan());
                    PenandatanganBonNomor penandatanganBonNomor = penandatanganBonNomorService.getByIdAndOrg(permohonanBonNomor.getId(),pem.getIdOrgPenandatangan());
                    if (penandatanganBonNomor != null) {
                        penandatanganBonNomor.setPermohonan(permohonanBonNomor);
                        penandatanganBonNomor.setOrganisasi(pemeriksa);
                        penandatanganBonNomorService.edit(penandatanganBonNomor);
                    } else {
                        penandatanganBonNomor = new PenandatanganBonNomor();
                        penandatanganBonNomor.setPermohonan(permohonanBonNomor);
                        penandatanganBonNomor.setOrganisasi(pemeriksa);
                        penandatanganBonNomorService.create(penandatanganBonNomor);
                    }
                }
            }
            this.edit(model);*/

        }

        getEntityManager().flush();
        getEntityManager().refresh(model);

        result = new PermohonanBonNomorDto(model);

        return result;
    }

    public PermohonanBonNomorDto approve(Long id) {
        PermohonanBonNomorDto result;
        PermohonanBonNomor model = this.find(id);
        List<PemeriksaBonNomor> pemeriksaList = pemeriksaBonNomorService.getAllBy(model.getId());
        for (PemeriksaBonNomor pem : pemeriksaList) {
            pem.setUser(pem.getOrganisasi().getUser());
            pemeriksaBonNomorService.edit(pem);
        }

        List<PenandatanganBonNomor> penandatanganList = penandatanganBonNomorService.getAllBy(model.getId());
        for (PenandatanganBonNomor pem : penandatanganList) {
            pem.setUser(pem.getOrganisasi().getUser());
            penandatanganBonNomorService.edit(pem);
        }

        getEntityManager().flush();
        getEntityManager().refresh(model);

        result = new PermohonanBonNomorDto(model);

        return result;
    }

    public void delete(Long id) {
        PermohonanBonNomor model = this.find(id);
        model.setIsDeleted(true);
        this.edit(model);
    }


}
