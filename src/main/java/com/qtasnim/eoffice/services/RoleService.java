package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.RoleDto;
import com.qtasnim.eoffice.ws.dto.RoleModuleDto;
import com.qtasnim.eoffice.ws.dto.RoleUserDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class RoleService extends AbstractFacade<MasterRole> {

    @Inject
    private MasterStrukturOrganisasiService organisasiService;
    
    @Inject
    private MasterModulService modulService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private RoleModuleService roleModuleService;

    @Override
    protected Class<MasterRole> getEntityClass() {
        return MasterRole.class;
    }

    public JPAJinqStream<MasterRole> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterRole>getFiltered(String kode,String nama,String description, Boolean isActive){
        JPAJinqStream<MasterRole> query = db();

        if(!Strings.isNullOrEmpty(kode)){
            query = query.where(q ->q.getRoleCode().toLowerCase().contains(kode.toLowerCase()));
        }

        if(!Strings.isNullOrEmpty(nama)){
            query = query.where(q ->q.getRoleName().toLowerCase().contains(nama.toLowerCase()));
        }

        if(!Strings.isNullOrEmpty(description)){
            query = query.where(q ->q.getDescription().toLowerCase().contains(description.toLowerCase()));
        }

        if (isActive!=null) {
            if(isActive) {
                query = query.where(q -> q.getIsActive());
            }else{
                query = query.where(q -> !q.getIsActive());
            }
        }

        return query;
    }

    public void saveOrEdit(RoleDto dao, Long id) throws Exception {
        boolean isNew =id==null;
        MasterRole model = new MasterRole();
        DateUtil dateUtil = new DateUtil();
        try {
            if (isNew) {
                model.setRoleCode(dao.getRoleCode());
                model.setRoleName(dao.getRoleName());
                model.setDescription(dao.getDescription());
                model.setIsActive(dao.getIsActive());
//                model.setLevel(dao.getLevel());
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if (dao.getEndDate() != null) {
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                } else {
                    model.setEnd(dateUtil.getDefValue());
                }
                if (dao.getCompanyId() != null) {
                    CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                    model.setCompanyCode(cCode);
                } else {
                    model.setCompanyCode(null);
                }
                this.create(model);
                getEntityManager().flush();
                getEntityManager().refresh(model);

                /*save to UserRole*/
                if (dao.getRoleUser() != null && !dao.getRoleUser().isEmpty()) {
                    for (RoleUserDto ru : dao.getRoleUser()) {
                        MasterStrukturOrganisasi org = organisasiService.getByIdUser(ru.getIdUser());
                        org.getRoleList().add(model);
                        organisasiService.edit(org);
                    }
                }

                /*save to ModuleRole*/
                if (dao.getRoleModule() != null && !dao.getRoleModule().isEmpty()) {
                    for (RoleModuleDto rm : dao.getRoleModule()) {
                        MasterModul modul = modulService.find(rm.getIdModul());
                        RoleModule obj = new RoleModule();
                        obj.setModules(modul);
                        obj.setRoles(model);
                        obj.setAllowCreate(rm.getAllowCreate());
                        obj.setAllowRead(rm.getAllowRead());
                        obj.setAllowUpdate(rm.getAllowUpdate());
                        obj.setAllowDelete(rm.getAllowDelete());
                        obj.setAllowPrint(rm.getAllowPrint());
                        roleModuleService.create(obj);
                    }
                }
            } else {
                model = this.find(id);
                model.setRoleCode(dao.getRoleCode());
                model.setRoleName(dao.getRoleName());
                model.setDescription(dao.getDescription());
//                model.setLevel(dao.getLevel());
                model.setIsActive(dao.getIsActive());
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

                if (dao.getEndDate() != null) {
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                } else {
                    model.setEnd(dateUtil.getDefValue());
                }

                if (dao.getCompanyId() != null) {
                    CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                    model.setCompanyCode(cCode);
                } else {
                    model.setCompanyCode(null);
                }
                this.edit(model);

                getEntityManager().flush();
                getEntityManager().refresh(model);

                /*save to UserRole*/
                if (dao.getRoleUser() != null && !dao.getRoleUser().isEmpty()) {
                    /*Delete Existing Data*/
                    for(MasterStrukturOrganisasi so:model.getOrganisasiList()){
                        so.getRoleList().remove(model);
                    }

                    for(RoleUserDto ru:dao.getRoleUser()){
                        MasterStrukturOrganisasi org = organisasiService.getByIdUser(ru.getIdUser());
                        org.getRoleList().add(model);
                        organisasiService.edit(org);
                    }
                }

                /*save to ModuleRole*/
                if(dao.getRoleModule() !=null && !dao.getRoleModule().isEmpty()){
                    /*Delete Existing Data*/
                    for(RoleModule rol:model.getRoleModuleList()){
                        roleModuleService.remove(rol);
                    }

                    for(RoleModuleDto rm :dao.getRoleModule()){
                        MasterModul modul = modulService.find(rm.getIdModul());
                        RoleModule obj = new RoleModule();
                        obj.setModules(modul);
                        obj.setRoles(model);
                        obj.setAllowCreate(rm.getAllowCreate());
                        obj.setAllowRead(rm.getAllowRead());
                        obj.setAllowUpdate(rm.getAllowUpdate());
                        obj.setAllowDelete(rm.getAllowDelete());
                        obj.setAllowPrint(rm.getAllowPrint());
                        roleModuleService.create(obj);
                    }
                }
            }
        }catch(Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public void saveOrgRole(List<Long> dao,Long idRole){
        MasterRole role = this.find(idRole);
       if(!dao.isEmpty()){
           for(Long orgId:dao){
                MasterStrukturOrganisasi org = organisasiService.find(orgId);
                if(org.getRoleList().isEmpty()){
                    org.getRoleList().add(role);
                    organisasiService.edit(org);
                }else{
                    if(!org.getRoleList().stream().anyMatch(q->q.equals(role))) {
                        org.getRoleList().add(role);
                        organisasiService.edit(org);
                    }
                }
            }
       }
    }

    public void deleteRole(Long id){
        MasterRole role = this.find(id);
        
        for(MasterStrukturOrganisasi org :role.getOrganisasiList()){
            org.getRoleList().remove(role);
            organisasiService.edit(org);
        }
        
        for(RoleModule modul :role.getRoleModuleList()){
            roleModuleService.remove(modul);
        }

        this.remove(role);
    }

    public void deleteOrgRole(Long idRole,long idUser){
        MasterRole role = this.find(idRole);
        MasterStrukturOrganisasi org = organisasiService.find(idUser);
        org.getRoleList().remove(role);
        organisasiService.edit(org);
    }

    public JPAJinqStream<MasterRole>getByIdOrg(Long idOrg){
        MasterStrukturOrganisasi org = organisasiService.find(idOrg);
        return db().where(a->a.getOrganisasiList().contains(org));
    }

    public RoleDto getByIdRole(Long idRole){
        RoleDto result = new RoleDto();
        MasterRole role = db().where(q->q.getIdRole().equals(idRole)).findFirst().orElse(null);
        if(role!=null){
            result = new RoleDto(role);
        }
        return result;
    }

    public boolean checkLevel(RoleDto dao){
        Date n = new Date();
        Integer level = dao.getLevel();
        if(dao.getIdRole() != null) {
            Long id = dao.getIdRole();
            return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && !q.getIdRole().equals(id))
                    .noneMatch(a -> a.getLevel().equals(level));
        }else{
            return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd())).noneMatch(a -> a.getLevel().equals(level));
        }
    }
}
