package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.DocLibHistory;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class DocLibHistoryService extends AbstractFacade<DocLibHistory> {
    @Override
    protected Class<DocLibHistory> getEntityClass() {
        return DocLibHistory.class;
    }

    public JPAJinqStream<DocLibHistory> findAllByDocId(String docId) {
        return db().where(t -> t.getDocId().equals(docId));
    }

    public  DocLibHistory getLatestVersion(String docId){
        DocLibHistory result = null;
        List<String> tmp =  db().where(t->t.getDocId().equals(docId)).map(a->a.getVersion()).collect(Collectors.toList());
        List<Long> parsed1 = new ArrayList<>();

        if(!tmp.isEmpty()) {
            for (String s : tmp) {
                String a[] = s.split("\\.");
                Long i = Long.parseLong(a[0]);
                System.out.println(i);
                parsed1.add(i);
            }

            Long max = Collections.max(parsed1);
            List<Long> parsed2 = new ArrayList<>();

            for (String s : tmp) {
                String a[] = s.split("\\.");
                Long i = Long.parseLong(a[0]);
                if (i == max) {
                    Long j = Long.parseLong(a[1]);
                    parsed2.add(j);
                }
            }

            Long max2 = Collections.max(parsed2);
            String maxVer = String.valueOf(max.longValue()) + "." + String.valueOf(max2.longValue());

            result = db().where(t -> t.getDocId().equals(docId) && t.getVersion().equals(maxVer)).findFirst().orElse(null);
        }

        return result;
    }
}
