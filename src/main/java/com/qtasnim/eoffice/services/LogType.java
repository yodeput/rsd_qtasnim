package com.qtasnim.eoffice.services;

public enum LogType {
    ERROR, INFO, LOGIN, API
}
