package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterArea;
import com.qtasnim.eoffice.db.MasterKota;
import com.qtasnim.eoffice.db.MasterTindakan;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterAreaDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterAreaService extends AbstractFacade<MasterArea> {
    private static final long serialVersionUID = 1L;
    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterKotaService kotaService;

    @Override
    protected Class<MasterArea> getEntityClass() {
        return MasterArea.class;
    }

    public JPAJinqStream<MasterArea> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterArea> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name));
    }


    public MasterArea save(String id, MasterAreaDto dao) {
        MasterArea model = new MasterArea();
        if (id == null) {
            this.create(data(model, dao));
        } else {
            model = this.find(id);
            this.edit(data(model, dao));
        }
        return model;
    }

    public MasterArea data(MasterArea masterArea, MasterAreaDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterArea model = masterArea;
        model.setId(dao.getId());
        model.setNama(dao.getNama());
        if (dao.getCompanyId() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }
        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        if (dao.getIdKota() != null) {
            MasterKota kota = kotaService.find(dao.getIdKota());
            model.setKota(kota);
        } else {
            model.setKota(null);
        }
        return model;
    }

    public MasterArea delete(String id) {
        MasterArea model = this.find(id);
        this.remove(model);
        return model;
    }

    public MasterArea getAreaById(String id){
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && q.getId().equals(id)).findFirst().orElse(null);
    }

}