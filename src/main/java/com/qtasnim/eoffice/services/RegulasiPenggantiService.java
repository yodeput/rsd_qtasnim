package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.RegulasiDto;
import com.qtasnim.eoffice.ws.dto.RegulasiPenggantiDto;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class RegulasiPenggantiService extends AbstractFacade<RegulasiPengganti> {
    private static final long serialVersionUID = 1L;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    CompanyCodeService companyCodeService;

    @Inject
    RegulasiService regulasiService;

    @Inject
    private FileService fileService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;


    @Override
    protected Class<RegulasiPengganti> getEntityClass() {
        return RegulasiPengganti.class;
    }


    public RegulasiPengganti save(Long id, RegulasiPenggantiDto dao){
        RegulasiPengganti model = new RegulasiPengganti();
        if (id==null){
            this.create(data(model, dao));
        } else {
            model = this.find(id);
            this.edit(data(model, dao));
        }

        return  model;
    }

    private RegulasiPengganti data(RegulasiPengganti regulasiPengganti, RegulasiPenggantiDto dao){
        RegulasiPengganti model = regulasiPengganti;
        DateUtil dateUtil = new DateUtil();

        model.setDeskripsi(dao.getDeskripsi());
        model.setIsRenewal(dao.getIsRenewal());

        if(dao.getCompanyId()!=null){
            CompanyCode object = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(object);
        } else {
            model.setCompanyCode(null);
        }

        if(dao.getRegulasiPenggantiId()!=null){
            Regulasi object = regulasiService.find(dao.getRegulasiPenggantiId());
            model.setRegulasiPengganti(object);
        } else {
            model.setRegulasiPengganti(null);
        }

        if(dao.getRegulasiId()!=null){
            Regulasi object = regulasiService.find(dao.getRegulasiId());
            model.setRegulasi(object);
        } else {
            model.setRegulasi(null);
        }

        return model;
    }

    public RegulasiPengganti createOrEdit(Long idRegulasi, Long regulasiPenggantiId, Long companyId){
        RegulasiPengganti model = db().where(q -> q.getRegulasi().getId()==idRegulasi && q.getRegulasiPengganti().getId()==regulasiPenggantiId).findFirst().orElse(null);
        RegulasiPenggantiDto dao = new RegulasiPenggantiDto();
        dao.setRegulasiId(idRegulasi);
        dao.setRegulasiPenggantiId(regulasiPenggantiId);
//        model.setDeskripsi(dao.getDeskripsi());
        dao.setIsRenewal(true);
        dao.setCompanyId(companyId);

        if (model==null){
            model = save(null,dao);
        } else {
            model = save(model.getId(),dao);
        }
        return model;
    }

    public RegulasiPengganti delete(Long id){
        RegulasiPengganti model = this.find(id);
        this.remove(model);
        return model;
    }

    public void uploadAttachment(Long idRegulasi, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";
        try {
            String filename = formDataContentDisposition.getFileName();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is2 = new ByteArrayInputStream(baos.toByteArray());

            byte[] documentContent = IOUtils.toByteArray(is2);

            RegulasiPengganti regulasi = this.find(idRegulasi);

//            String number = autoNumberService.from(SuratDocLib.class).next(SuratDocLib::getNumber);
            List<String> extractedText = textExtractionService.parsingFile(is1, formDataContentDisposition, body).getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            docId = fileService.upload(filename, documentContent);
            docId = docId.replace("workspace://SpacesStore/", "");

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString().concat(FileUtility.GetFileExtension(filename)));
            obj.setDocName(filename);
            obj.setCompanyCode(regulasi.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setSize(new Long(documentContent.length));
            obj.setIsDeleted(false);
            obj.setReferenceId(idRegulasi);
            obj.setReferenceTable("t_regulasi");
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);
        } catch (Exception ex) {
            if (!StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }
            throw ex;
        }
    }

    public GlobalAttachment deleteFile(Long idFile) {
        GlobalAttachment global = globalAttachmentService.find(idFile);

        if (global.getReferenceTable().toLowerCase().equals("t_regulasi")) {
            global.setIsDeleted(true);
            globalAttachmentService.edit(global);
        }

        return global;
    }
}