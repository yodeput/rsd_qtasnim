package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.Arsiparis;
import com.qtasnim.eoffice.db.MasterArsiparis;
import com.qtasnim.eoffice.db.PenandatanganSurat;
import com.qtasnim.eoffice.db.Surat;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class ArsiparisService extends AbstractFacade<Arsiparis>{

    @Override
    protected Class<Arsiparis> getEntityClass() {
        return Arsiparis.class;
    }

    public JPAJinqStream<Arsiparis> getByMarsiparisId(MasterArsiparis mArsiparis){
        return db().where(a->a.getArsiparis().equals(mArsiparis));
    }
}
