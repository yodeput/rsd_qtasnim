package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.DataMasterValueRetrieve;
import com.qtasnim.eoffice.ws.dto.DataMasterValueRetrieveParam;
import com.qtasnim.eoffice.ws.dto.FormDefinitionDto;
import com.qtasnim.eoffice.ws.dto.FormFieldDto;
import org.eclipse.persistence.tools.schemaframework.FieldDefinition;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service untuk fungsi CRUD table m_user
 * 
 * @author seno@qtasnim.com
 */


 @Stateless
 @LocalBean
public class FormFieldService extends AbstractFacade<FormField>{

    private static final long serialVersionUID = -4184209743573831159L;

    @Inject
    private MasterMetadataService masterMetadataService;

    @Inject
    private FormDefinitionService formDefinitionService;

    @Inject
    private DataMasterService dataMasterService;

    @Override
    protected Class<FormField> getEntityClass() {
        return FormField.class;
    }

    public JPAJinqStream<FormField> getAll() {
//        Date n = new Date();
//        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
        return db();
    }
    
    public FormField save(Long id, FormFieldDto dao, FormDefinition formDefinition){
        FormField model = new FormField();
        DateUtil dateUtil = new DateUtil();

        if(id==null) {

            model.setData(dao.getData());
            model.setDefaultValue(dao.getDefaultValue());
            model.setType(dao.getType());
            model.setPlaceholder(dao.getPlaceholder());

            /**
             * @tableSourceName
             * @selectedSourceField
             * @selectedSourceValue
             * mun selected_fields : kolom naon nu rek di munculkeun di dropdown nu aya table_source_name.
             * kan tableSourceName & selected_fields eta di pake mun dropdown na ngareferensi ka table master
             */
            model.setTableSourceName(dao.getTableSourceName());
            model.setSelectedSourceField(dao.getSelectedSourceField());
            model.setSelectedSourceValue(dao.getSelectedSourceValue());

            model.setSeq(dao.getSeq());
            model.setIsRequired(dao.getIsRequired());
            model.setIsActive(dao.getIsActive());
            model.setCreatedBy(dao.getCreatedBy());
            model.setCreatedDate(new Date());
            model.setModifiedBy("-");
            model.setModifiedDate(new Date());

            if(formDefinition == null) {
                FormDefinition definition = formDefinitionService.find(dao.getDefinitionId());
                model.setDefinition(definition);
            } else {
                model.setDefinition(formDefinition);
            }

            // MasterMetadata metadata = masterMetadataService.save(dao.getMetadata().getId(), dao.getMetadata());
            MasterMetadata metadata = masterMetadataService.find(dao.getMetadataId());
            model.setMetadata(metadata);

//            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
//            if(dao.getEndDate()!=null){
//                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
//            } else {
//                model.setEnd(dateUtil.getDefValue());
//            }

            this.create(model);
        } else {
            model = this.find(id);
            model.setData(dao.getData());
            model.setDefaultValue(dao.getDefaultValue());
            model.setType(dao.getType());
            model.setPlaceholder(dao.getPlaceholder());
            model.setTableSourceName(dao.getTableSourceName());
            model.setSelectedSourceField(dao.getSelectedSourceField());
            model.setSelectedSourceValue(dao.getSelectedSourceValue());
            model.setSeq(dao.getSeq());
            model.setIsRequired(dao.getIsRequired());
            model.setIsActive(dao.getIsActive());
            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());

            if(formDefinition == null) {
                FormDefinition definition = formDefinitionService.find(dao.getDefinitionId());
                model.setDefinition(definition);
            } else {
                model.setDefinition(formDefinition);
            }

            // MasterMetadata metadata = masterMetadataService.save(dao.getMetadata().getId(), dao.getMetadata());
            MasterMetadata metadata = masterMetadataService.find(dao.getMetadataId());
            model.setMetadata(metadata);

//            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
//            if(dao.getEndDate()!=null){
//                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
//            } else {
//                model.setEnd(dateUtil.getDefValue());
//            }

            this.edit(model);
        }

      return model;
    }

    public void saveAsNewField(FormDefinition source, FormDefinition newDefinition){

        source.getFormFields().forEach(f -> {
            FormField model = new FormField();
            model.setDefinition(newDefinition);
            model.setMetadata(f.getMetadata());
            model.setData(f.getData());
            model.setDefaultValue(f.getDefaultValue());
            model.setType(f.getType());
            model.setPlaceholder(f.getPlaceholder());
            model.setTableSourceName(f.getTableSourceName());
            model.setSelectedSourceField(f.getSelectedSourceField());
            model.setSelectedSourceValue(f.getSelectedSourceValue());
            model.setSeq(f.getSeq());
            model.setIsRequired(f.getIsRequired());
            model.setIsActive(f.getIsActive());
            model.setModifiedBy(f.getModifiedBy());
            model.setModifiedDate(new Date());

//            model.setStart(f.getStart());
//            model.setEnd(f.getEnd());
           
            this.create(model);
        });

    }

    public FormField delete(Long id){
        FormField model = this.find(id);
        this.remove(model);
        
        return model;
    }

    public List<FormFieldDto> getByDefinitionList(List<FormDefinition> definitionList){
        Date n = new Date();
        //return db().where(q->definitionList.contains(q.getDefinition()) && n.after(q.getStart()) && n.before(q.getEnd())).map(FormFieldDto::new).collect(Collectors.toList());
        return db().where(q->definitionList.contains(q.getDefinition())).map(FormFieldDto::new).collect(Collectors.toList());
    }

    public DataMasterValueRetrieve fillValueFromDataMaster(DataMasterValueRetrieveParam param) {
        return dataMasterService.getValueOfSelectedMasterDataFields(param);
    }
}