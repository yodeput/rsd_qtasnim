package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.PosisiHistory;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;


@Stateless
@LocalBean
public class PosisiHistoryService extends AbstractFacade<PosisiHistory> {
    private static final long serialVersionUID = 1L;
    
    @Override
    protected Class<PosisiHistory> getEntityClass() {
        return PosisiHistory.class;
    }

    public PosisiHistory delete(Long id) {
        PosisiHistory model = this.find(id);
        this.remove(model);
        return model;
    }

}