package com.qtasnim.eoffice.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterKategoriArsip;

import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterKategoriArsipDto;
import org.jinq.jpa.JPAJinqStream;

import java.util.Date;

@Stateless
@LocalBean
public class MasterKategoriArsipService extends AbstractFacade<MasterKategoriArsip> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyService;

    @Override
    protected Class<MasterKategoriArsip> getEntityClass() {
        return MasterKategoriArsip.class;
    }

    public JPAJinqStream<MasterKategoriArsip> getAll() {
        Date n = new Date();
        return db().where(a->n.after(a.getStart())&& n.before(a.getEnd()));
    }

    public void saveOrEdit(Long id,MasterKategoriArsipDto dao){
        boolean isNew = id==null;
        MasterKategoriArsip model = new MasterKategoriArsip();
        DateUtil dateUtil = new DateUtil();
        try {
            if (isNew) {
                CompanyCode company = companyService.find(dao.getCompanyId());
                model.setCompanyCode(company);
                model.setName(dao.getName());
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null) {
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.create(model);
            } else {
                model = this.find(id);
                CompanyCode company = companyService.find(dao.getCompanyId());
                model.setCompanyCode(company);
                model.setName(dao.getName());
                model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
                if(dao.getEndDate()!=null) {
                    model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
                }else{
                    model.setEnd(dateUtil.getDefValue());
                }
                this.edit(model);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public MasterKategoriArsip findByNama(String value) {
        return db().where(t -> t.getName().equals(value)).findFirst().orElse(null);
    }
}