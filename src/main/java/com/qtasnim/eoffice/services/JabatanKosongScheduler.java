package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.ws.WorkflowResource;
import liquibase.util.StringUtils;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Startup
@Singleton
public class JabatanKosongScheduler implements IService {
    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private ProcessTaskService processTaskService;

    @Inject
    private SyncLogService syncLogService;

    @Inject
    private BackgroundServiceLoggerService loggerService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    @Inject
    private SuratService suratService;

    @Inject
    private DistribusiDokumenService distribusiService;

    @Inject
    private PermohonanDokumenService layananService;

    @Inject
    private PenandatanganSuratService psService;

    @Inject
    private PenandatanganPelakharPymtSuratService psDelService;

    @Inject
    private PenandatanganDistribusiService pdService;

    @Inject
    private PenandatanganPermohonanService ppService;

    @Inject
    private Logger logger;

    private SyncLog syncLog;

    private Boolean isRun = false;

    @Override
    public String getName() {
        return "Kembali Ke Konseptor (Jabatan Kosong)";
    }

    @Override
    @Lock(LockType.READ)
    @Schedule(second = "0", minute = "0", hour = "3")
    public void run() {
        if (!isRun && isRunnable()) {
            isRun = true;
            Instant start = Instant.now();
            syncLog = syncLogService.start(getClassName());
            loggerService.info(syncLog, "Service starting");

            try {
                String query = "isDeleted == false and isActive == true and isHris == true and (user == null or (user.isActive == false and user.isDeleted == true))";
                List<MasterStrukturOrganisasi> jabatanKosongList = organisasiService.getResultList(query,null,0,0);

                /*Kondisi dimana tidak ada jabatan kosong*/
                if(jabatanKosongList.isEmpty()){
                    loggerService.info(syncLog,"Tidak terdapat jabatan kosong");
                }else{
                    loggerService.info(syncLog,String.format("Sebanyak %s jabatan kosong ditemukan",jabatanKosongList.size()));

                    /*Ambil task dari jabatan kosong yang belum tereksekusi*/
                    List<ProcessTask> emptyPejabatTask = processTaskService.getTaskJabatanKosong(jabatanKosongList);
                    if(emptyPejabatTask.isEmpty()){
                        loggerService.info(syncLog,"Tidak terdapat task yang belum ditindaklanjuti oleh jabatan kosong");
                    }else{
                        loggerService.info(syncLog,String.format("Sebanyak %s task yang belum dieksekusi pada jabatan kosong",emptyPejabatTask.size()));

                        int countDistribusi = emptyPejabatTask.stream().filter(a->a.getProcessInstance().getRelatedEntity()
                                .equals(DistribusiDokumen.class.getName())).collect(Collectors.toList()).size();
                        int countLayanan = emptyPejabatTask.stream().filter(a->a.getProcessInstance().getRelatedEntity()
                                .equals(PermohonanDokumen.class.getName())).collect(Collectors.toList()).size();
                        int countSurat = emptyPejabatTask.stream().filter(a->a.getProcessInstance().getRelatedEntity()
                                .equals(Surat.class.getName())).collect(Collectors.toList()).size();

                        loggerService.info(syncLog,String.format("Sebanyak %s task yang belum dieksekusi pada Distribusi Dokumen",countDistribusi));
                        loggerService.info(syncLog,String.format("Sebanyak %s task yang belum dieksekusi pada Layanan Dokumen",countLayanan));
                        loggerService.info(syncLog,String.format("Sebanyak %s task yang belum dieksekusi pada Dokumen",countSurat));

                        /*Looping task dari jabatan kosong untuk dikembalikan ke draft*/
                        emptyPejabatTask.forEach(task ->{

                            /*ambil instance dari task*/
                            ProcessInstance instance = task.getProcessInstance();

                            /*Inisiasi workflowProvider*/
                            IWorkflowProvider workflowProvider = masterWorkflowProviderService.getByProviderName(applicationContext.getApplicationConfig().getWorkflowProvider()).getWorkflowProvider();

                            /*Kondisi dimana task merupakan Distribusi Dokumen*/
                            if(instance.getRelatedEntity().equals(DistribusiDokumen.class.getName())){
                                DistribusiDokumen distribusi = distribusiService.find(Long.parseLong(instance.getRecordRefId()));
                                if(distribusi != null) {
                                    distribusi.setStatus(EntityStatus.DRAFT.toString());
                                    distribusiService.edit(distribusi);
                                    try {
                                        workflowProvider.terminateProcess(instance);
                                    }catch(Exception ex){}
                                }
                            }

                            /*Kondisi dimana task merupakan Layanan Dokumen*/
                            if(instance.getRelatedEntity().equals(PermohonanDokumen.class.getName())){
                                PermohonanDokumen layananDokumen = layananService.find(Long.parseLong(instance.getRecordRefId()));
                                if(layananDokumen != null){
                                    layananDokumen.setStatus(EntityStatus.DRAFT.toString());
                                    layananService.edit(layananDokumen);
                                    try {
                                        workflowProvider.terminateProcess(instance);
                                    }catch(Exception ex){}
                                }
                            }

                            /*Kondisi dimana task merupakan Dokumen*/
                            if(instance.getRelatedEntity().equals(Surat.class.getName())){
                                Surat surat = suratService.getByIdSurat(Long.parseLong(instance.getRecordRefId()));
                                if(surat != null){
                                    surat.setStatus(EntityStatus.DRAFT.toString());
                                    surat.setStatusNavigasi(Constants.STATUS_NAVIGASI_SIMPAN_OLEH_KONSEPTOR);
                                    suratService.edit(surat);
                                    try {
                                        workflowProvider.terminateProcess(instance);
                                    }catch (Exception ex){}
                                }
                            }
                        });
                    }

                    /*Handle Dokumen pada kondisi dimana penandatangan jabatan kosong sementara pemerika belum approve*/
                    List<Surat> pSurat = psService.getByIdOrganization(jabatanKosongList);
                    List<Surat> pdSurat = psDelService.getByIdOrganization(jabatanKosongList);
                    int sCount = pSurat.size() + pdSurat.size();
                    loggerService.info(syncLog,String.format("Sebanyak %s Dokumen dikembalikan ke konseptor",sCount));
                    IWorkflowProvider workflowProvider = masterWorkflowProviderService.getByProviderName(applicationContext.getApplicationConfig().getWorkflowProvider()).getWorkflowProvider();
                    if(!pSurat.isEmpty()){
                        pSurat.forEach(ps->{
                            List<ProcessTask> pendingPemeriksa = processTaskService.getPendingTask(ps.getId().toString(),Surat.class.getName());

                            if(!pendingPemeriksa.isEmpty()){
                                pendingPemeriksa.forEach(w->{
                                    try{
                                        workflowProvider.terminateProcess(w.getProcessInstance());
                                    }catch (Exception ex){}
                                });
                            }
                            ps.setStatus(EntityStatus.DRAFT.toString());
                            ps.setStatusNavigasi(Constants.STATUS_NAVIGASI_SIMPAN_OLEH_KONSEPTOR);
                            suratService.edit(ps);
                        });
                    }

                    if(!pdSurat.isEmpty()){
                        pdSurat.forEach(ps->{
                            List<ProcessTask> pendingPemeriksa = processTaskService.getPendingTask(ps.getId().toString(),Surat.class.getName());

                            if(!pendingPemeriksa.isEmpty()){
                                pendingPemeriksa.forEach(w->{
                                    try{
                                        workflowProvider.terminateProcess(w.getProcessInstance());
                                    }catch (Exception ex){}
                                });
                            }
                            ps.setStatus(EntityStatus.DRAFT.toString());
                            ps.setStatusNavigasi(Constants.STATUS_NAVIGASI_SIMPAN_OLEH_KONSEPTOR);
                            suratService.edit(ps);
                        });
                    }

                    /*Handle Distribusi pada kondisi dimana penandatangan jabatan kosong sementara pemerika belum approve*/
                    List<DistribusiDokumen> pDistribusi = pdService.getByIdOrganization(jabatanKosongList);
                    loggerService.info(syncLog,String.format("Sebanyak %s Distribusi dikembalikan ke konseptor",pDistribusi.size()));
                    if(!pDistribusi.isEmpty()){
                        pDistribusi.forEach(ps->{
                            List<ProcessTask> pendingPemeriksa = processTaskService.getPendingTask(ps.getId().toString(),DistribusiDokumen.class.getName());

                            if(!pendingPemeriksa.isEmpty()){
                                pendingPemeriksa.forEach(w->{
                                    try{
                                        workflowProvider.terminateProcess(w.getProcessInstance());
                                    }catch (Exception ex){}
                                });
                            }
                            ps.setStatus(EntityStatus.DRAFT.toString());
                            distribusiService.edit(ps);
                        });
                    }

                    /*Handle Layanan pada kondisi dimana penandatangan jabatan kosong sementara pemerika belum approve*/
                    List<PermohonanDokumen> pDokumen = ppService.getByIdOrganization(jabatanKosongList);
                    loggerService.info(syncLog,String.format("Sebanyak %s Layanan Dokumen dikembalikan ke konseptor",pDokumen.size()));
                    if(!pDokumen.isEmpty()){
                        pDokumen.forEach(ps->{
                            List<ProcessTask> pendingPemeriksa = processTaskService.getPendingTask(ps.getId().toString(),PermohonanDokumen.class.getName());

                            if(!pendingPemeriksa.isEmpty()){
                                pendingPemeriksa.forEach(w->{
                                    try{
                                        workflowProvider.terminateProcess(w.getProcessInstance());
                                    }catch (Exception ex){}
                                });
                            }
                            ps.setStatus(EntityStatus.DRAFT.toString());
                            layananService.edit(ps);
                        });
                    }
                }

            } catch (Exception e) {
                Throwable realCause = ExceptionUtil.getRealCause(e);
                logger.error(realCause.getMessage(), realCause);
                loggerService.error(syncLog, realCause);
            }

            loggerService.info(syncLog, "Service stopped");
            loggerService.info(syncLog, "DONE");
            syncLogService.stop(syncLog, start);
            syncLog = null;
            isRun = false;
        }
    }

    @Override
    public Boolean isRun() {
        return isRun;
    }

    @Override
    public String getClassName() {
        return Stream.of(getClass().getName().split("\\$")).findFirst().orElse(getClass().getName());
    }

    @Override
    public Boolean isRunnable() {
        String servername = System.getProperty("rds.servername");
        String worker = applicationConfig.getBackgroundServiceWorker();

        return Optional.ofNullable(worker).map(s -> worker.equals(servername)).orElse(true);
    }
}
