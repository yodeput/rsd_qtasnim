package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.DokumenHistory;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.DokumenHistoryDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class DokumenHistoryService extends AbstractFacade<DokumenHistory> {

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    @Inject
    private MasterUserService userService;

    @Override
    protected Class<DokumenHistory> getEntityClass() {
        return DokumenHistory.class;
    }

    public JPAJinqStream<DokumenHistory> getHistory(String tableName,Long refId){
        return db().where(q->q.getReferenceTable().equals(tableName) && q.getReferenceId().equals(refId));
    }

    public void saveHistory(DokumenHistoryDto dao){
        DokumenHistory obj = new DokumenHistory();
        DateUtil dateUtil = new DateUtil();
        obj.setActionDate(new Date());
        obj.setStatus(dao.getStatus());
        obj.setReferenceTable(dao.getReferenceTable());
        obj.setReferenceId(dao.getReferenceId());
        if(dao.getIdOrganisasi()!=null) {
            MasterStrukturOrganisasi org = organisasiService.getByIdOrganisasi(dao.getIdOrganisasi());
            if(org!=null) {
                obj.setOrganization(org);
            }
        }
        if(dao.getIdUser()!=null){
            MasterUser user = userService.find(dao.getIdUser());
            if(user!=null) {
                obj.setUser(user);
            }
        }
        if(!Strings.isNullOrEmpty(dao.getDelegasiType())){
            obj.setDelegasiType(dao.getDelegasiType());
        }
        create(obj);
    }
}
