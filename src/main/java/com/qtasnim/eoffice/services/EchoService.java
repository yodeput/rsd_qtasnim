package com.qtasnim.eoffice.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class EchoService {
    public String echo(String message) {
        return message;
    }
}
