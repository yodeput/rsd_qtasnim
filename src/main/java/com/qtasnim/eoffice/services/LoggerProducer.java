package com.qtasnim.eoffice.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.Produces;

import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Named;

public class LoggerProducer {
    @Produces
    @Named("logger")
    public Logger produceLogger(InjectionPoint injectionPoint) {
        return LoggerFactory.getLogger("EOFFICE");
    }
}
