package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.cmis.CMISDocument;
import com.qtasnim.eoffice.cmis.CMISFolder;
import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.cmis.ICMISProvider;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.DocLibHistory;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.helpers.FileUtils;
import com.qtasnim.eoffice.security.ISessionContext;
import org.apache.commons.lang3.StringUtils;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Optional;

@LocalBean
@Stateless
public class FileService {
    @Inject
    private ICMISProvider cmisProvider;

    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private DocLibHistoryService docLibHistoryService;

    @Inject
    @ISessionContext
    private Session userSession;

    public String upload(String fileName, byte[] content) {
        try {
            cmisProvider.openConnection();

            return upload(cmisProvider, fileName, content);
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                cmisProvider.closeConnection();
            } catch (Exception e) {}
        }
    }

    public String upload(ICMISProvider alfrescoProvider, String fileName, byte[] content) {
        try {
            String randomFileName = FileUtils.generateRandomFileName(fileName);
            CMISFolder folder = alfrescoProvider.getRoot();
            CMISDocument cmisDocument = alfrescoProvider.uploadDocument(folder, content, randomFileName);

            return cmisDocument.getId();
        } catch (Exception e) {
            throw e;
        }
    }

    public String update(String id, byte[] content) {
        try {
            cmisProvider.openConnection();

            return update(cmisProvider, id, content);
        } catch (CMISProviderException e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        } finally {
            cmisProvider.closeConnection();
        }
    }

    public String update(ICMISProvider alfrescoProvider, String id, byte[] content) {
        try {
            CMISDocument cmisDocument = alfrescoProvider.updateDocument(id, content);

            return cmisDocument.getId();
        } catch (CMISProviderException e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        }
    }

    public byte[] download(String id) {
        try {
            cmisProvider.openConnection();

            return download(cmisProvider, id);
        } catch (CMISProviderException e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        } finally {
            cmisProvider.closeConnection();
        }
    }

    public byte[] download(ICMISProvider alfrescoProvider, String id) {
        try {
            return alfrescoProvider.getDocumentContent(id);
        } catch (CMISProviderException e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        }
    }

    public byte[] download(ICMISProvider alfrescoProvider, String id, String version) {
        try {
            return alfrescoProvider.getDocumentContent(id, version);
        } catch (CMISProviderException e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        }
    }

    public void delete(String id) {
        try {
            cmisProvider.openConnection();

            delete(cmisProvider, id);
        } catch (CMISProviderException e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        } finally {
            cmisProvider.closeConnection();
        }
    }

    public void delete(ICMISProvider alfrescoProvider, String id) {
        try {
            CMISDocument document = alfrescoProvider.getDocument(id);

            alfrescoProvider.delete(document);
        } catch (CMISProviderException e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        }
    }

    public CMISDocument getFile(String id) {
        try {
            cmisProvider.openConnection();

            return getFile(cmisProvider, id);
        } catch (CMISProviderException e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        } finally {
            cmisProvider.closeConnection();
        }
    }

    public CMISDocument getFile(ICMISProvider alfrescoProvider, String id) {
        try {
            return alfrescoProvider.getDocument(id);
        } catch (CMISProviderException e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        }
    }

    public JPAJinqStream<DocLibHistory> getHistory(String docId) {
        return docLibHistoryService.findAllByDocId(cmisProvider.trimDocId(docId));
    }

    public String getLayoutingLink(String idJenisDokumen, String version) {
        String baseUrl = String.format("%s/Layouting?idJenisDokumen=%s", applicationConfig.getBaseUrl(), idJenisDokumen);

        if (userSession != null && StringUtils.isNotEmpty(userSession.getUserSession().getToken())) {
            baseUrl = String.format("%s&token=%s", baseUrl, userSession.getUserSession().getToken());
        }

        if (StringUtils.isNotEmpty(version)) {
            baseUrl = String.format("%s&version=%s", baseUrl, version);
        }

        return baseUrl;
    }

    public String getEditLink(String id) {
        return getEditLink(id, null);
    }

    public String getEditLink(String id, String version) {
        if (id.contains(";")) {
            id = id.substring(0, id.indexOf(";"));
        }

        String baseUrl = String.format("%s/Document?id=%s", applicationConfig.getBaseUrl(), id.replace("workspace://SpacesStore/", ""));

        if (userSession != null && StringUtils.isNotEmpty(userSession.getUserSession().getToken())) {
            baseUrl = String.format("%s&token=%s", baseUrl, userSession.getUserSession().getToken());
        }

        if (StringUtils.isNotEmpty(version)) {
            baseUrl = String.format("%s&version=%s", baseUrl, version);
        }

        return baseUrl;
    }

    public String getViewLink(String id) {
        return getViewLink(id, true);
    }

    public String getViewLink(String id, String fileType) {
        return getViewLink(id, fileType, null);
    }

    public String getViewLink(String id, String fileType, String version) {
        if (fileType.toLowerCase().contains("pdf")) {
            return String.format("%s/ViewerJS/#../Download?id=%s&version=%s", applicationConfig.getBaseUrl(), trimDocId(id), Optional.ofNullable(version).orElse(""));
        }

        return getViewLink(id, true, version);
    }

    public String getViewLink(String id, Boolean requireOnlyOfficeViewer) {
        return getViewLink(id, requireOnlyOfficeViewer, null);
    }

    public String getViewLink(String id, Boolean requireOnlyOfficeViewer, String version) {
        String baseUrl;

        if(requireOnlyOfficeViewer) {
            baseUrl = String.format("%s/Document?id=%s&mode=view", applicationConfig.getBaseUrl(), trimDocId(id));
        } else {
            baseUrl = String.format("%s/Download?id=%s", applicationConfig.getBaseUrl(), trimDocId(id));
        }

        if (StringUtils.isNotEmpty(version)) {
            baseUrl = String.format("%s&version=%s", baseUrl, version);
        }

        return baseUrl;
    }

    public String trimDocId(String id) {
        return cmisProvider.trimDocId(id);
    }
}
