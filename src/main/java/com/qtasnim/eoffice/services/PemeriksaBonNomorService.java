package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.PemeriksaBonNomor;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@LocalBean
public class PemeriksaBonNomorService extends AbstractFacade<PemeriksaBonNomor> {

    @Override
    protected Class<PemeriksaBonNomor> getEntityClass() {
        return PemeriksaBonNomor.class;
    }

    public List<PemeriksaBonNomor> getAllBy(Long id){
        return db().where(q->q.getPermohonan().getId()==id).toList();
    }

    public PemeriksaBonNomor getByIdAndOrg(Long id,Long idOrg) {
        return db().where(q -> q.getOrganisasi().getIdOrganization()==idOrg && q.getPermohonan().getId()==id
        ).findFirst().orElse(null);
    }
}
