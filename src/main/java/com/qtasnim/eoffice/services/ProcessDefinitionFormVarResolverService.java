package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.ProcessDefinitionFormVarResolver;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class ProcessDefinitionFormVarResolverService extends AbstractFacade<ProcessDefinitionFormVarResolver> {
    @Override
    protected Class<ProcessDefinitionFormVarResolver> getEntityClass() {
        return ProcessDefinitionFormVarResolver.class;
    }
}
