package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.ws.dto.CardChecklistDetailDto;
import com.qtasnim.eoffice.ws.dto.CardChecklistDto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class CardChecklistDetailService extends AbstractFacade<CardChecklistDetail>{
    @Override
    protected Class<CardChecklistDetail> getEntityClass() {
        return CardChecklistDetail.class;
    }

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyService;

    @Inject
    private CardChecklistService checklistService;

    public CardChecklistDetail saveOrEdit(CardChecklistDetailDto dao, Long id) throws Exception {
        try{
            CardChecklistDetail obj = new CardChecklistDetail();
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = companyService.getByCode(user.getCompanyCode().getCode());
            if(id==null){
                obj.setItemName(dao.getItemName());
                obj.setIsDeleted(false);
                if(dao.getChecklistId()!=null){
                    CardChecklist checklist = checklistService.find(dao.getChecklistId());
                    obj.setChecklistCard(checklist);
                }
                obj.setIsChecked(dao.getIsChecked());
                obj.setCompanyCode(company);
//                obj.setProgress(dao.getProgress());
                create(obj);
            }else{
                obj = find(dao.getId());
                obj.setItemName(dao.getItemName());
                obj.setIsDeleted(false);
                if(dao.getChecklistId()!=null){
                    CardChecklist checklist = checklistService.find(dao.getChecklistId());
                    obj.setChecklistCard(checklist);
                }
                obj.setIsChecked(dao.getIsChecked());
                obj.setCompanyCode(company);
//                obj.setProgress(dao.getProgress());
                edit(obj);
            }
            getEntityManager().flush();
            getEntityManager().refresh(obj);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public CardChecklistDetail deleteCheckListDetail(Long id) throws Exception {
        try{
            CardChecklistDetail obj = find(id);
            obj.setIsDeleted(true);
            return obj;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
}
