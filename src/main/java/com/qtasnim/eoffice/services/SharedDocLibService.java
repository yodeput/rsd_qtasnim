package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.StringUtil;
import com.qtasnim.eoffice.ws.dto.GlobalAttachmentDto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Stateless
@LocalBean
public class SharedDocLibService extends AbstractFacade<SharedDocLib> {
    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private SharedDocLibVendorService sharedDocLibVendorService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private SuratService suratService;

    @Override
    protected Class<SharedDocLib> getEntityClass() {
        return SharedDocLib.class;
    }

    public SharedDocLib findByAuth(String token, String username, String password) {
        Date now = new Date();

        return db()
                .where(t -> t.getToken().equals(token) &&  t.getUsername().equals(username) && t.getPassword().equals(password) && now.before(t.getExpiredDate()))
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    public SharedDocLib generate(String docId) {
        return generate(docId, null);
    }

    public SharedDocLib generate(String docId, MasterVendor masterVendor) {
        StringUtil stringUtil = new StringUtil();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, applicationContext.getApplicationConfig().getDocLibExpirationDays());

        SharedDocLib result = new SharedDocLib();
        result.setDocId(docId);
        result.setExpiredDate(calendar.getTime());
        result.setUsername(stringUtil.randomString(8));
        result.setPassword(stringUtil.randomString(8));
        result.setToken(UUID.randomUUID().toString());

        create(result);

        if (masterVendor != null) {
            GlobalAttachment globalAttachment = globalAttachmentService.findByDocId(docId, "t_surat");

            if (globalAttachment != null) {
                Surat surat = suratService.find(globalAttachment.getReferenceId());

                if (surat != null) {
                    SharedDocLibVendor sharedDocLibVendor = new SharedDocLibVendor();
                    sharedDocLibVendor.setSharedDocLib(result);
                    sharedDocLibVendor.setVendor(masterVendor);
                    sharedDocLibVendor.setSurat(surat);

                    getEntityManager().persist(sharedDocLibVendor);
                }
            }
        }

        return result;
    }
}
