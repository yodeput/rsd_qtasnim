package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.workflows.IRdsWorkflowProvider;
import com.qtasnim.eoffice.workflows.TaskEventHandler;
import com.qtasnim.eoffice.ws.dto.*;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class WorkflowService {
    @Inject
    private MasterWorkflowProviderService workflowProviderService;

    // public void execute(IWorkflowDto dto, IWorkflowEntity workflowEntity, AbstractFacade entityService) {
    //     MasterWorkflowProvider masterWorkflowProvider = workflowProviderService.getByProviderName("camunda");
    //     IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();

    //     if ("DRAFT".equals(dto.getWorkflowAction())) {
    //         if (!workflowEntity.getCompletionStatus().equals(EntityStatus.DRAFT)) {
    //             //Resubmit to Draft
    //             workflowEntity.setCompletionStatus(EntityStatus.DRAFT);
    //             entityService.edit(workflowEntity);
    //         }
    //     } else {
    //         if ("SUBMIT".equals(dto.getWorkflowAction()) && workflowEntity.getCompletionStatus().equals(EntityStatus.DRAFT)) {
    //             //Start workflow
    //             workflowEntity.setCompletionStatus(EntityStatus.SUBMITTED);
    //             entityService.edit(workflowEntity);

    //             workflowProvider.startWorkflow(workflowEntity, dto.getWorkflowFormName());
    //         } else {
    //             //Submit task
    //             workflowProvider.execute(workflowEntity, dto, TaskEventHandler.DEFAULT
    //                     .onCompleted((status) -> {
    //                         switch (status) {
    //                             case APPROVED: {
    //                                 workflowEntity.setCompletionStatus(EntityStatus.APPROVED);
    //                                 entityService.edit(workflowEntity);
    //                                 break;
    //                             }
    //                             case REJECTED: {
    //                                 workflowEntity.setCompletionStatus(EntityStatus.REJECTED);
    //                                 entityService.edit(workflowEntity);
    //                                 break;
    //                             }
    //                         }
    //                     })
    //             );
    //         }
    //     }
    // }

    public void execute(SuratDto dto, String workflowStatus, IWorkflowEntity workflowEntity, AbstractFacade entityService) {
        MasterWorkflowProvider masterWorkflowProvider = workflowProviderService.getByProviderName("camunda");
        IRdsWorkflowProvider workflowProvider = masterWorkflowProvider.getRdsWorkflowProvider();

        // if ("SUBMITTED".equals(dto.getStatus()) && workflowStatus.equals("DRAFT")) {
        if (workflowStatus.equals("DRAFT")) {
            //Start workflow
            workflowEntity.setCompletionStatus(EntityStatus.SUBMITTED);
            entityService.edit(workflowEntity);

            workflowProvider.startWorkflow(dto, workflowEntity, dto.getFormDefinition().getJenisDokumen().getIdJenisDokumen());
        } else {
            //Submit task
            workflowProvider.execute(workflowEntity, dto, TaskEventHandler.DEFAULT
                    .onCompleted((processInstance, status) -> {
                        switch (status) {
                            case APPROVED: {
                                workflowEntity.setCompletionStatus(EntityStatus.APPROVED);
                                entityService.edit(workflowEntity);
                                break;
                            }
                            case REJECTED: {
                                workflowEntity.setCompletionStatus(EntityStatus.REJECTED);
                                entityService.edit(workflowEntity);
                                break;
                            }
                            case DRAFT: {
                                workflowEntity.setCompletionStatus(EntityStatus.DRAFT);
                                entityService.edit(workflowEntity);
                                break;
                            }
                        }
                    })
            );
        }
    }
}
