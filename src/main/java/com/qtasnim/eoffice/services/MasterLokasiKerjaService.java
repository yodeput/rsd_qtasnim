package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterLokasiKerja;
import com.qtasnim.eoffice.db.MasterKota;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterLokasiKerjaDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
@LocalBean
public class MasterLokasiKerjaService extends AbstractFacade<MasterLokasiKerja> {

    @Inject
    private MasterKotaService masterKotaService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterLokasiKerja> getEntityClass() {
        return MasterLokasiKerja.class;
    }

    public JPAJinqStream<MasterLokasiKerja> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterLokasiKerja>getFiltered(String kode,String singkatan,String nama,
            Boolean isActive,Long idKota){
        JPAJinqStream<MasterLokasiKerja> query = db();

        if(!Strings.isNullOrEmpty(kode)){
            query = query.where(q ->q.getKodeLoker().toLowerCase().contains(kode.toLowerCase()));
        }
        
        if(!Strings.isNullOrEmpty(singkatan)){
            query = query.where(q ->q.getSingkatan().toLowerCase().contains(singkatan.toLowerCase()));
        }

        if(!Strings.isNullOrEmpty(nama)){
            query = query.where(q ->q.getNamaLoker().toLowerCase().contains(nama.toLowerCase()));
        }

        if (isActive!=null) {
            if(isActive) {
                query = query.where(q -> q.getIsActive());
            }else{
                query = query.where(q -> !q.getIsActive());
            }
        }

        if(idKota!=null){
            query = query.where(q->q.getKota().getIdKota()==idKota);
        }

        return query;
    }

    public void saveOrEdit(Long id,MasterLokasiKerjaDto dao){
        boolean isNew = id==null;
        MasterLokasiKerja model = new MasterLokasiKerja();
        DateUtil dateUtil = new DateUtil();

        if(isNew){
            model.setKodeLoker(dao.getKodeLoker());
            model.setSingkatan(dao.getSingkatan());
            model.setNamaLoker(dao.getNamaLoker());
            model.setIsActive(dao.getIsActive());

            MasterKota kota = masterKotaService.find(dao.getIdKota());
            model.setKota(kota);

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setCreatedBy(dao.getCreatedBy());
            model.setCreatedDate(new Date());
            model.setModifiedBy("-");
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.create(model);
        }else{
            model = this.find(id);
            model.setKodeLoker(dao.getKodeLoker());
            model.setSingkatan(dao.getSingkatan());
            model.setNamaLoker(dao.getNamaLoker());
            model.setIsActive(dao.getIsActive());

            MasterKota kota = masterKotaService.find(dao.getIdKota());
            model.setKota(kota);

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.edit(model);
        }
    }
}
