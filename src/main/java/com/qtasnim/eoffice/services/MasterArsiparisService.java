package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.ArsiparisDto;
import com.qtasnim.eoffice.ws.dto.MasterArsiparisDto;
import com.qtasnim.eoffice.ws.dto.MasterDelegasiDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;


@Stateless
@LocalBean
public class MasterArsiparisService extends AbstractFacade<MasterArsiparis> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterStrukturOrganisasiService strukturOrganisasiService;

    @Inject
    private MasterKorsaService korsaService;

    @Inject
    private ArsiparisService arsiparisService;

    @Override
    protected Class<MasterArsiparis> getEntityClass() {
        return MasterArsiparis.class;
    }

    public JPAJinqStream<MasterArsiparis> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()) && !q.getIsDeleted());
    }

    public MasterArsiparis save(Long id, MasterArsiparisDto dao) {
        MasterArsiparis model = new MasterArsiparis();
        DateUtil dateUtil = new DateUtil();
        boolean isNew = id==null;
        if (isNew) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
            MasterStrukturOrganisasi petugas = strukturOrganisasiService.find(dao.getIdPetugasArsiparis());
            model.setPetugas(petugas);
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if (dao.getEndDate() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            model.setIsDeleted(false);
            this.create(model);
            getEntityManager().flush();
            getEntityManager().refresh(model);

            if(dao.getArsiparisList()!=null){
                for(ArsiparisDto ap:dao.getArsiparisList()){
                    Arsiparis arsiparis = new Arsiparis();
                    arsiparis.setArsiparis(model);
                    MasterStrukturOrganisasi pejabat = strukturOrganisasiService.find(ap.getIdPejabat());
                    arsiparis.setPejabat(pejabat);
                    arsiparisService.create(arsiparis);
                }
            }
        } else {
            model = this.find(id);
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
            MasterStrukturOrganisasi petugas = strukturOrganisasiService.find(dao.getIdPetugasArsiparis());
            model.setPetugas(petugas);
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if (dao.getEndDate() != null) {
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            model.setIsDeleted(false);
            this.edit(model);
            if(model.getArsiparisList()!=null && !model.getArsiparisList().isEmpty()){
                for(Arsiparis ar:model.getArsiparisList()){
                    arsiparisService.remove(ar);
                }

                if(dao.getArsiparisList()!=null){
                    for(ArsiparisDto ap:dao.getArsiparisList()){
                        Arsiparis arsiparis = new Arsiparis();
                        arsiparis.setArsiparis(model);
                        MasterStrukturOrganisasi pejabat = strukturOrganisasiService.find(ap.getIdPejabat());
                        arsiparis.setPejabat(pejabat);
                        arsiparisService.create(arsiparis);
                    }
                }
            }
        }
        return model;
    }

    public MasterArsiparis delete(Long id) {
        MasterArsiparis model = this.find(id);
//        List<Arsiparis> arsiparisList = arsiparisService.getByMarsiparisId(model).collect(Collectors.toList());
//        for(Arsiparis ar:arsiparisList){
//            arsiparisService.remove(ar);
//        }
        model.setIsDeleted(true);
        this.edit(model);
        return model;
    }

    public MasterArsiparisDto getById(Long id){
        MasterArsiparis ar = db().where(a->a.getId().equals(id)).findFirst().orElse(null);
        MasterArsiparisDto result = new MasterArsiparisDto();
        if(ar!=null){
            result = new MasterArsiparisDto(ar);
        }
        return result;
    }

    public HashMap<String,Object> getFilteredByCriteriaB(String filter, String sort, Boolean descending, int skip, int limit,String start,String end) {
        TypedQuery<MasterArsiparis> all = getDataByCB(filter,sort,descending,start,end);
        List<MasterArsiparisDto> result = new ArrayList<>();
        result = MappingToDto(all,skip,limit);
        Long length = all.getResultStream().count();
        HashMap<String,Object> retur = new HashMap<String,Object>();
        retur.put("data",result);
        retur.put("length",length);
        return retur;
    }

    private List<MasterArsiparisDto> MappingToDto(TypedQuery<MasterArsiparis> all,long skip,long limit) {
        List<MasterArsiparisDto> result = null;

        if (all != null) {
            result = all.getResultStream().skip(skip).limit(limit)
                    .map(MasterArsiparisDto::new).collect(Collectors.toList());
        }
        return result;
    }

    public TypedQuery<MasterArsiparis> getDataByCB(String filter,String sort,Boolean descending,String start,String end) {
        Date n = new Date();
        DateUtil dateUtil = new DateUtil();
        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<MasterArsiparis> q = cb.createQuery(MasterArsiparis.class);
        Root<MasterArsiparis> root = q.from(MasterArsiparis.class);

        Join<MasterArsiparis, Arsiparis> detail = root.join("arsiparisList", JoinType.LEFT);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.isFalse(root.get("isDeleted"));

        //FilterBy
        if (!Strings.isNullOrEmpty(filter)) {
            Predicate pkeyword =
                    cb.or(
                            cb.like(detail.join("pejabat",JoinType.LEFT).get("organizationName"),"%"+filter+"%"),
                            cb.like(root.join("petugas",JoinType.LEFT).get("organizationName"),"%"+filter+"%"),
                            cb.like(root.join("petugas",JoinType.LEFT).join("user",JoinType.LEFT).get("loginUserName"),"%"+filter+"%"),
                            cb.like(detail.join("pejabat",JoinType.LEFT).join("user",JoinType.LEFT).get("loginUserName"), "%"+filter+"%"),
                            cb.like(detail.join("pejabat",JoinType.LEFT).join("user",JoinType.LEFT).get("nameFront"), "%"+filter+"%"),
                            cb.like(detail.join("pejabat",JoinType.LEFT).join("user",JoinType.LEFT).get("nameMiddleLast"), "%"+filter+"%"),
                            cb.like(
                                    cb.concat(cb.concat(detail.join("pejabat",JoinType.LEFT).join("user",JoinType.LEFT).get("nameFront")," ")
                                            ,detail.join("pejabat",JoinType.LEFT).join("user",JoinType.LEFT).get("nameMiddleLast")
                                    ), "%"+filter+"%"),
                            cb.like(root.join("petugas",JoinType.LEFT).join("user",JoinType.LEFT).get("nameFront"),"%"+filter+"%"),
                            cb.like(root.join("petugas",JoinType.LEFT).join("user",JoinType.LEFT).get("nameMiddleLast"),"%"+filter+"%"),
                            cb.like(
                                    cb.concat(cb.concat(root.join("petugas",JoinType.LEFT).join("user",JoinType.LEFT).get("nameFront")," ")
                                            ,root.join("petugas",JoinType.LEFT).join("user",JoinType.LEFT).get("nameMiddleLast")
                                    ), "%"+filter+"%")
                    );
            predicates.add(pkeyword);
        }

        if (!Strings.isNullOrEmpty(start)) {
            Date start1 = dateUtil.getDateFromISOString(start+"T00:00:00");
            Date start2 = dateUtil.getDateFromISOString(start+"T23:59:59");
            if (!Strings.isNullOrEmpty(end)) {
                Date end2 = dateUtil.getDateFromISOString(end+"T23:59:59");
                Predicate pEnd =
                        cb.and(
                                cb.greaterThanOrEqualTo(root.<Date>get("start"), start1),
                                cb.lessThanOrEqualTo(root.<Date>get("end"),end2)
                        );
                predicates.add(pEnd);
            }else {
                Predicate pStart = cb.and(
                        cb.greaterThanOrEqualTo(root.<Date>get("start"), start1),
                        cb.lessThanOrEqualTo(root.<Date>get("start"), start2)
                );
                predicates.add(pStart);
            }
        }else {
            if (!Strings.isNullOrEmpty(end)) {
                Date end1 = dateUtil.getDateFromISOString(end + "T00:00:00");
                Date end2 = dateUtil.getDateFromISOString(end + "T23:59:59");
                Predicate pEnd = cb.and(
                        cb.greaterThanOrEqualTo(root.<Date>get("end"), end1),
                        cb.lessThanOrEqualTo(root.<Date>get("end"), end2)
                );
                predicates.add(pEnd);
            }
        }

        // WHERE CONDITION
        predicates.add(mainCond);


        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);

        if (descending != null && !Strings.isNullOrEmpty(sort)) {
            if (descending) {
                q.orderBy(cb.desc(root.join("petugas",JoinType.INNER).join("user",JoinType.INNER).get("nameFront")));
            } else {
                q.orderBy(cb.asc(root.join("petugas",JoinType.INNER).join("user",JoinType.INNER).get("nameFront")));
            }
        }

        return getEntityManager().createQuery(q);
    }

    public String validate(Long id, MasterArsiparisDto dao){
        String hasil = "";
        boolean isStart = false;
        boolean isEnd = false;
        boolean isBoth = false;
        DateUtil dateUtil = new DateUtil();
        Date startDate = dateUtil.getDateTimeStart(dao.getStartDate());
        Date endDate = dateUtil.getDateTimeEnd(dao.getEndDate());
        Long companyId = dao.getCompanyId();
        Long idPetugas = dao.getIdPetugasArsiparis();
        List<Long> idPejabats = dao.getArsiparisList().stream().map(a->a.getIdPejabat()).collect(Collectors.toList());
        if(id==null){
            List<MasterArsiparis> tmp = db().where(a -> !a.getIsDeleted() && a.getCompanyCode().getId().equals(companyId) && a.getPetugas().getIdOrganization().equals(idPetugas)).collect(Collectors.toList());
            if(!tmp.isEmpty()){
                for(MasterArsiparis ma : tmp) {
                    List<Long> idPejabat = ma.getArsiparisList().stream().map(b->b.getPejabat().getIdOrganization()).collect(Collectors.toList());
                    idPejabats.removeAll(idPejabat);
                    if(idPejabats.isEmpty()){
                        isStart = (startDate.after(ma.getStart()) && startDate.before(ma.getEnd()))
                                || (startDate.equals(ma.getStart()) || startDate.equals(ma.getEnd()));
                        isEnd = (endDate.after(ma.getStart()) && endDate.before(ma.getEnd()))
                                || (endDate.equals(ma.getStart()) || endDate.equals(ma.getEnd()));
                        isBoth = (startDate.after(ma.getStart()) && endDate.before(ma.getEnd())) ||
                                (startDate.equals(ma.getStart()) && endDate.equals(ma.getEnd())) ||
                                (startDate.before(ma.getStart()) && endDate.after(ma.getEnd()));
                        if(isStart||isEnd||isBoth) {
                            break;
                        }
                    }
                }
            }
        }else{
            List<MasterArsiparis> tmp = db().where(a -> !a.getId().equals(id) && !a.getIsDeleted() && a.getCompanyCode().getId().equals(companyId) && a.getPetugas().getIdOrganization().equals(idPetugas)).collect(Collectors.toList());
            if(!tmp.isEmpty()){
                for(MasterArsiparis ma : tmp) {
                    List<Long> idPejabat = ma.getArsiparisList().stream().map(b->b.getPejabat().getIdOrganization()).collect(Collectors.toList());
                    idPejabats.removeAll(idPejabat);
                    if(idPejabats.isEmpty()){
                        isStart = (startDate.after(ma.getStart()) && startDate.before(ma.getEnd()))
                                || (startDate.equals(ma.getStart()) || startDate.equals(ma.getEnd()));
                        isEnd = endDate.after(ma.getStart()) && endDate.before(ma.getEnd())
                                || (endDate.equals(ma.getStart()) || endDate.equals(ma.getEnd()));
                        isBoth = (startDate.after(ma.getStart()) && endDate.before(ma.getEnd())) ||
                                (startDate.equals(ma.getStart()) && endDate.equals(ma.getEnd())) ||
                                (startDate.before(ma.getStart()) && endDate.after(ma.getEnd()));
                        if(isStart||isEnd||isBoth) {
                            break;
                        }
                    }
                }
            }
        }
        hasil =  isBoth ? "all" : isStart ? "start" : isEnd ? "end" : "" ;
        return hasil;
    }
}