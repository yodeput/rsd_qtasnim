package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.RoleModule;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class RoleModuleService extends AbstractFacade<RoleModule>{

    @Override
    protected Class<RoleModule> getEntityClass() {
        return RoleModule.class;
    }

}
