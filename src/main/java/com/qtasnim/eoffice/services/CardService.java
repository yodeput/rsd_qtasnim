package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.CardDto;
import com.qtasnim.eoffice.ws.dto.CardEntity;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Stateless
@LocalBean
public class CardService extends AbstractFacade<Card> {

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private CompanyCodeService companyService;

    @Inject
    private BucketService bucketService;

    @Inject
    private MasterPrioritasService prioritasService;

    @Inject
    private MasterStrukturOrganisasiService organisasiService;

    @Inject
    private MasterUserService userService;

    @Inject
    private CardMemberService memberService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private BoardMemberService boardMemberService;

    @Override
    protected Class<Card> getEntityClass() {
        return Card.class;
    }

    public JPAJinqStream<Card> getAll() {
        Long idCompany = userSession.getUserSession().getUser().getCompanyCode().getId();
        CompanyCode company = companyService.find(idCompany);
        return db().where(a -> a.getIsDeleted().booleanValue() == false && a.getCompanyCode().equals(company));
    }

    public Card saveOrEdit(CardDto dao, Long id) throws Exception {
        DateUtil dateUtil = new DateUtil();
        try {
            Card obj = new Card();
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = user.getCompanyCode();
            if (id == null) {
                obj.setKode(dao.getCode());
                obj.setTitle(dao.getTitle());
                obj.setDescription(dao.getDescription());
                obj.setIsDeleted(false);
                if (dao.getBucketId() != null) {
                    Bucket bucket = bucketService.find(dao.getBucketId());
                    obj.setBucket(bucket);
                }
                obj.setProgress(dao.getProgress());
                obj.setCompanyCode(company);
                if (dao.getPrioritasId() != null) {
                    MasterPrioritas prioritas = prioritasService.find(dao.getPrioritasId());
                    obj.setPrioritas(prioritas);
                }
                if (dao.getCardOwnerId() != null) {
                    MasterStrukturOrganisasi owner = organisasiService.find(dao.getCardOwnerId());
                    obj.setCardOwner(owner);
                }
                if (dao.getStart() != null) {
                    obj.setStart(dateUtil.getDateTimeStart(dao.getStart()));
                }
                if (dao.getEnd() != null) {
                    obj.setEnd(dateUtil.getDateTimeEnd(dao.getEnd()));
                }

                create(obj);
                getEntityManager().flush();
                getEntityManager().refresh(obj);

                if (dao.getIdMembers() != null && !dao.getIdMembers().isEmpty()) {
                    List<CardMember> listMember = new ArrayList<>();
                    for (Long i : dao.getIdMembers()) {
                        MasterStrukturOrganisasi org = organisasiService.find(i);
                        MasterUser muser = userService.find(org.getUser().getId());
                        CardMember member = new CardMember();
                        member.setCompanyCode(company);
                        member.setCard(obj);
                        member.setOrganization(org);
                        member.setUser(muser);
                        member.setIsDeleted(false);
                        if (obj.getBucket() != null && obj.getBucket().getBoard() != null) {
                            Board board = obj.getBucket().getBoard();
                            member.setBoard(board);
                        }
                        memberService.create(member);
//                        getEntityManager().flush();
//                        getEntityManager().refresh(member);
                        listMember.add(member);
                    }

                    if (!listMember.isEmpty()) {
                        CardEntity entity = setEmailProperties(obj);
                        notificationService.sendEmail(Constants.TEMPLATE_NEW_OTHER_TUGAS_ASSIGNMENT, vars -> {
                            vars.put("entity", entity);
                            vars.put("pageLink", String.format("%s%s", applicationConfig.getFrontEndUrl(), entity.getPageLink()));
                        }, listMember.stream().map(q -> q.getUser()).toArray(MasterUser[]::new));
                    }
                }
            } else {
                obj = find(id);
                obj.setKode(dao.getCode());
                obj.setTitle(dao.getTitle());
                obj.setDescription(dao.getDescription());
                obj.setIsDeleted(false);
                if (dao.getBucketId() != null) {
                    Bucket bucket = bucketService.find(dao.getBucketId());
                    obj.setBucket(bucket);
                }
                obj.setProgress(dao.getProgress());
                obj.setCompanyCode(company);
                if (dao.getPrioritasId() != null) {
                    MasterPrioritas prioritas = prioritasService.find(dao.getPrioritasId());
                    obj.setPrioritas(prioritas);
                }
                if (dao.getCardOwnerId() != null) {
                    MasterStrukturOrganisasi owner = organisasiService.find(dao.getCardOwnerId());
                    obj.setCardOwner(owner);
                }
                if (dao.getStart() != null && StringUtils.isNotEmpty(dao.getStart())) {
                    obj.setStart(dateUtil.getDateTimeStart(dao.getStart()));
                }
                if (dao.getEnd() != null && StringUtils.isNotEmpty(dao.getEnd())) {
                    obj.setEnd(dateUtil.getDateTimeEnd(dao.getEnd()));
                }

                edit(obj);
                getEntityManager().flush();
                getEntityManager().refresh(obj);

                if (dao.getIdMembers() != null && !dao.getIdMembers().isEmpty()) {
                    List<CardMember> listMember = new ArrayList<>();

                    if (!obj.getMemberList().isEmpty()) {
                        List<Long> existingList = obj.getMemberList().stream().map(t -> t.getOrganization().getIdOrganization()).collect(Collectors.toList());
                        List<Long> newList = dao.getIdMembers();

                        List<Long> similar = new ArrayList<>(existingList);
                        similar.retainAll(newList);

                        List<Long> deletedList = new ArrayList<>(existingList);
                        deletedList.removeAll(similar);

                        for (Long deletedId : deletedList) {
                            CardMember member = memberService.getByCardAndOrganizationId(obj.getId(), deletedId);
                            if (member != null) {
                                member.setIsDeleted(true);
                                memberService.edit(member);
//                                getEntityManager().flush();
//                                getEntityManager().refresh(member);
                            }
                        }
                    }

//                    for (CardMember cm : obj.getMemberList()) {
//                        cm.setIsDeleted(true);
//                        memberService.edit(cm);
//                    }
                    for (Long i : dao.getIdMembers()) {
                        CardMember member = memberService.getByCardAndOrganizationId(obj.getId(), i);
                        if (member != null) {

                            if (obj.getBucket() != null && obj.getBucket().getBoard() != null) {
                                Board board = obj.getBucket().getBoard();
                                member.setBoard(board);
                                memberService.edit(member);
                            }
                        } else {
                            MasterStrukturOrganisasi org = organisasiService.find(i);
                            MasterUser muser = userService.find(org.getUser().getId());
                            CardMember newMember = new CardMember();
                            newMember.setCompanyCode(company);
                            newMember.setCard(obj);
                            newMember.setOrganization(org);
                            newMember.setUser(muser);
                            newMember.setIsDeleted(false);
                            if (obj.getBucket() != null && obj.getBucket().getBoard() != null) {
                                Board board = obj.getBucket().getBoard();
                                newMember.setBoard(board);
                            }
                            memberService.create(newMember);
//                            getEntityManager().flush();
//                            getEntityManager().refresh(newMember);
                            listMember.add(newMember);
                        }
                    }

//                    listMember.addAll(obj.getMemberList().stream().filter(q -> !q.getIsDeleted()).collect(Collectors.toList()));
//                    obj.setMemberList(listMember);
//                    edit(obj);
//                    getEntityManager().flush();
//                    getEntityManager().refresh(obj);
                    if (!listMember.isEmpty()) {
                        CardEntity entity = setEmailProperties(obj);
                        notificationService.sendEmail(Constants.TEMPLATE_NEW_OTHER_TUGAS_ASSIGNMENT, vars -> {
                            vars.put("entity", entity);
                            vars.put("pageLink", String.format("%s%s", applicationConfig.getFrontEndUrl(), entity.getPageLink()));
                        }, null, null, listMember.stream().map(q -> q.getUser()).toArray(MasterUser[]::new));
                    }
                }
            }
            return obj;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public Card saveOrEdit2(CardDto dao, Long id) throws Exception {
        DateUtil dateUtil = new DateUtil();
        try {
            Card obj = new Card();
            MasterUser user = userSession.getUserSession().getUser();
            CompanyCode company = user.getCompanyCode();
            if (id == null) {
                obj.setKode(dao.getCode());
                obj.setTitle(dao.getTitle());
                obj.setDescription(dao.getDescription());
                obj.setIsDeleted(false);
                if (dao.getBucketId() != null) {
                    Bucket bucket = bucketService.find(dao.getBucketId());
                    obj.setBucket(bucket);
                }
                obj.setProgress(dao.getProgress());
                obj.setCompanyCode(company);
                if (dao.getPrioritasId() != null) {
                    MasterPrioritas prioritas = prioritasService.find(dao.getPrioritasId());
                    obj.setPrioritas(prioritas);
                }
                if (dao.getCardOwnerId() != null) {
                    MasterStrukturOrganisasi owner = organisasiService.find(dao.getCardOwnerId());
                    obj.setCardOwner(owner);
                }
                if (dao.getStart() != null) {
                    obj.setStart(dateUtil.getDateTimeStart(dao.getStart()));
                }
                if (dao.getEnd() != null) {
                    obj.setEnd(dateUtil.getDateTimeEnd(dao.getEnd()));
                }

                create(obj);
                getEntityManager().flush();
                getEntityManager().refresh(obj);
            } else {
                obj = find(id);
                obj.setKode(dao.getCode());
                obj.setTitle(dao.getTitle());
                obj.setDescription(dao.getDescription());
                obj.setIsDeleted(false);
                if (dao.getBucketId() != null) {
                    Bucket bucket = bucketService.find(dao.getBucketId());
                    obj.setBucket(bucket);
                }
                obj.setProgress(dao.getProgress());
                obj.setCompanyCode(company);
                if (dao.getPrioritasId() != null) {
                    MasterPrioritas prioritas = prioritasService.find(dao.getPrioritasId());
                    obj.setPrioritas(prioritas);
                }
                if (dao.getCardOwnerId() != null) {
                    MasterStrukturOrganisasi owner = organisasiService.find(dao.getCardOwnerId());
                    obj.setCardOwner(owner);
                }
                if (dao.getStart() != null && StringUtils.isNotEmpty(dao.getStart())) {
                    obj.setStart(dateUtil.getDateTimeStart(dao.getStart()));
                }
                if (dao.getEnd() != null && StringUtils.isNotEmpty(dao.getEnd())) {
                    obj.setEnd(dateUtil.getDateTimeEnd(dao.getEnd()));
                }

                edit(obj);
                getEntityManager().flush();
                getEntityManager().refresh(obj);

                if (dao.getIdMembers() != null && !dao.getIdMembers().isEmpty()) {
                    List<CardMember> listMember = new ArrayList<>();

                    if (!obj.getMemberList().isEmpty()) {
                        List<Long> existingList = obj.getMemberList().stream().map(t -> t.getOrganization().getIdOrganization()).collect(Collectors.toList());
                        List<Long> newList = dao.getIdMembers();

                        List<Long> similar = new ArrayList<>(existingList);
                        similar.retainAll(newList);

                        List<Long> deletedList = new ArrayList<>(existingList);
                        deletedList.removeAll(similar);

                        for (Long deletedId : deletedList) {
                            CardMember member = memberService.getByCardAndOrganizationId(obj.getId(), deletedId);
                            if (member != null) {
                                member.setIsDeleted(true);
                                memberService.edit(member);
                            }
                        }
                    }

                    for (Long i : dao.getIdMembers()) {
                       BoardMember bm = boardMemberService.find(i);
                       if(bm!=null){
                           CardMember cm = new CardMember();
                           cm.setCard(obj);
                           cm.setUser(bm.getUser());
                           cm.setOrganization(bm.getOrganization());
                           cm.setIsDeleted(false);
                           cm.setBoard(bm.getBoard());
                           cm.setCompanyCode(company);
                           memberService.create(cm);
                       }
                    }
                }
            }
            return obj;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public Card deleteCard(Long id) throws Exception {
        try {
            Card obj = find(id);
            obj.setIsDeleted(true);
            return obj;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }
    
    public CriteriaQuery<Card> getCountQuery(Long idOrganization) {
        try {
            Long idOrg = Optional.ofNullable(idOrganization).orElse(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());
            MasterStrukturOrganisasi organisasi = organisasiService.find(idOrg);
            Long idUser = organisasi.getUser().getId();

            // Exec Query
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Card> q = cb.createQuery(Card.class);
            Root<Card> root = q.from(Card.class);

            q.select(root);

            List<Predicate> predicates = new ArrayList<>();

            Join<Card, CardMember> member = root.join("memberList", JoinType.LEFT);

            Predicate mainCond = cb.and(
                    cb.notEqual(root.get("progress"),"Completed"),
                    cb.isFalse(root.get("isDeleted")));
            predicates.add(mainCond);

            Predicate memberCond = cb.and(
                    cb.isFalse(member.get("isDeleted")),
                    cb.or(
                            cb.equal(member.get("organization").get("idOrganization"), idOrg),
                            cb.equal(member.get("user").get("id"), idUser)
                    ));
            predicates.add(memberCond);

            q.where(predicates.toArray(new Predicate[predicates.size()]));
            q.distinct(true);

            return q;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public CriteriaQuery<Card> getAllQuery(String filterBy, String sortedField, boolean descending, String progress, Long idPrioritas) {
        try {
            MasterUser user = userSession.getUserSession().getUser();
            Long idOrg = user.getOrganizationEntity().getIdOrganization();
            Long idUser = user.getId();

            // Exec Query
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Card> q = cb.createQuery(Card.class);
            Root<Card> root = q.from(Card.class);

            q.select(root);

            List<Predicate> predicates = new ArrayList<>();

            Join<Card, CardMember> member = root.join("memberList", JoinType.LEFT);

            Predicate notDeleted = cb.isFalse(root.get("isDeleted"));
            predicates.add(notDeleted);

            Predicate mainCond = cb.and(
                    cb.isFalse(member.get("isDeleted")),
                    cb.or(
                            cb.equal(member.get("organization").get("idOrganization"), idOrg),
                            cb.equal(member.get("user").get("id"), idUser),
                            cb.equal(root.get("cardOwner").get("idOrganization"), idOrg)
                    )
            );
            predicates.add(mainCond);

            if (!Strings.isNullOrEmpty(filterBy)) {
                Predicate pkeyword
                        = cb.or(
                                cb.like(root.get("title"), "%" + filterBy + "%"),
                                cb.like(root.get("description"), "%" + filterBy + "%"),
                                cb.like(root.get("progress"), "%" + filterBy + "%")
                        );
                predicates.add(pkeyword);
            }

            if (!Strings.isNullOrEmpty(progress)) {
                Predicate pProgress = cb.equal(root.get("progress"), progress);
                predicates.add(pProgress);
            }

            if (idPrioritas != null) {
                Predicate pPrioritas = cb.equal(root.get("prioritas").get("id"), idPrioritas);
                predicates.add(pPrioritas);
            }

            q.where(predicates.toArray(new Predicate[predicates.size()]));
            q.distinct(true);

            if (Strings.isNullOrEmpty(sortedField)) {
                q.orderBy(cb.desc(root.get("start")));
            } else {
                Expression exp = root.get(sortedField);
                if (descending) {
                    q.orderBy(cb.desc(exp));
                } else {
                    q.orderBy(cb.asc(exp));
                }
            }

            return q;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Card> execCriteriaQuery(CriteriaQuery<Card> criteriaQuery, int skip, int limit) {
        try {
            return getEntityManager().createQuery(criteriaQuery).setFirstResult(skip).setMaxResults(limit).getResultList();
        } catch (Exception ex) {
            return new ArrayList<>();
        }

    }

    public Long countCriteriaQuery(CriteriaQuery<Card> criteriaQuery) {
        try {
            return getEntityManager().createQuery(criteriaQuery).getResultStream().count();
        } catch (Exception ex) {
            return 0L;
        }

    }

    private CardEntity setEmailProperties(Card card) {
        CardEntity entity = new CardEntity();
        entity.setTitle(Strings.isNullOrEmpty(card.getTitle()) ? "-" : card.getTitle());
        entity.setDesciption(Strings.isNullOrEmpty(card.getDescription()) ? "-" : card.getDescription());

        if (card.getPrioritas() != null) {
            entity.setPrioritas(card.getPrioritas().getNama());
        } else {
            entity.setPrioritas("-");
        }

        if (card.getCardOwner() != null) {
            entity.setCardOwner(card.getCardOwner().getOrganizationName());
        } else {
            entity.setCardOwner("-");
        }

        Locale locale = new Locale("in", "ID");

        if (card.getStart() != null) {
            String formatted = new SimpleDateFormat("dd MMMMMMMMMM yyyy", locale).format(card.getStart());
            entity.setFormattedStart(formatted);
        } else {
            entity.setFormattedStart("-");
        }

        if (card.getEnd() != null) {
            String formatted = new SimpleDateFormat("dd MMMMMMMMMM yyyy", locale).format(card.getEnd());
            entity.setFormattedEnd(formatted);
        } else {
            entity.setFormattedEnd("-");
        }

        return entity;
    }

    public JinqStream<Card> findByBoard(Long boardId) {
        return db().where(t -> t.getBucket().getBoard().getId().equals(boardId));
    }
}
