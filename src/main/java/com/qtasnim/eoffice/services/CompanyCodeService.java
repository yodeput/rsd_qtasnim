package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.CompanyCodeDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.text.DateFormat;
import java.util.Base64;
import java.util.Date;


@Stateless
@LocalBean
public class CompanyCodeService extends AbstractFacade<CompanyCode> {
    private static final long serialVersionUID = 1L;

    @Override
    protected Class<CompanyCode> getEntityClass() {
        return CompanyCode.class;
    }

    public JPAJinqStream<CompanyCode> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<CompanyCode> getFiltered(String name) {
        return db().where(q -> q.getCode().toLowerCase().contains(name));
    }

    public JPAJinqStream<CompanyCode> getCompanyCodeByUrl(String url) {
        return db().where(q -> q.getUrl().toLowerCase().contains(url));
    }

    public CompanyCode save(Long id, CompanyCodeDto dao){
        DateUtil dateUtil = new DateUtil();
        CompanyCode model = new CompanyCode();
        if(id==null){
            model.setCode(dao.getCode());
            model.setName(dao.getName());
            model.setAddress(dao.getAddress());
            model.setEmail(dao.getEmail());
            model.setUrl(dao.getUrl());
            model.setDescription(dao.getDescription());
            if(!Strings.isNullOrEmpty(dao.getLogo())){
                model.setLogo(Base64.getDecoder().decode(dao.getLogo()));
            }
            if(!Strings.isNullOrEmpty(dao.getBg())){
                model.setBg(Base64.getDecoder().decode(dao.getBg()));
            }
            model.setIsActive(dao.getIsActive());

            model.setCreatedBy(dao.getCreatedBy());
            model.setCreatedDate(new Date());
            model.setModifiedBy("-");
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.create(model);

        }else {
            model = this.find(id);
            model.setCode(dao.getCode());
            model.setName(dao.getName());
            model.setAddress(dao.getAddress());
            model.setEmail(dao.getEmail());
            model.setUrl(dao.getUrl());
            model.setDescription(dao.getDescription());
            if(!Strings.isNullOrEmpty(dao.getLogo())){
                model.setLogo(Base64.getDecoder().decode(dao.getLogo()));
            }
            if(!Strings.isNullOrEmpty(dao.getBg())){
                model.setBg(Base64.getDecoder().decode(dao.getBg()));
            }
            model.setIsActive(dao.getIsActive());

            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));

            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }

            this.edit(model);
        }

       return model;
    }

    public CompanyCode delete(Long id){
        CompanyCode masterDivision = this.find(id);
        this.remove(masterDivision);
        return masterDivision;
    }

    public CompanyCode getByCode(String code){
        return db().where(q->q.getCode().equals(code)).findFirst().orElse(null);
    }

    public CompanyCode getOrCreateByCode(String code, String name){
        CompanyCode companyCode = null;

        try {
            if (code != null && name != null) {
                Date now = new Date();
                DateUtil dateUtil = new DateUtil();
                companyCode = db().where(q -> q.getCode().equals(code)).findFirst().orElse(null);

                if(companyCode == null){
                    companyCode = new CompanyCode();
                    companyCode.setName(name);
                    companyCode.setCode(code);
                    companyCode.setAddress("-");
                    companyCode.setEmail("-");
                    companyCode.setUrl("-");
                    companyCode.setDescription("-");
                    companyCode.setIsActive(true);
                    companyCode.setStart(now);
                    companyCode.setEnd(dateUtil.getDefValue());

                    create(companyCode);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return companyCode;
    }
}