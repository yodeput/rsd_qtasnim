package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.KontakFavoritDto;
import com.qtasnim.eoffice.ws.dto.ReferensiSuratDto;
import com.qtasnim.eoffice.ws.dto.SuratDto;
import org.apache.commons.lang3.StringUtils;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.jpa.JPQL;
import org.jinq.orm.stream.JinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class ReferensiSuratService extends AbstractFacade<ReferensiSurat> {

    
    @Inject
    private SuratService suratService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Override
    protected Class<ReferensiSurat> getEntityClass() {
        return ReferensiSurat.class;
    }

    public ReferensiSurat save(Long id, ReferensiSuratDto dao){
        ReferensiSurat model = new ReferensiSurat();
        DateUtil dateUtil = new DateUtil();

        if(id==null) {
            MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
            model.setOrganisasi(org);
            model.setUser(userSession.getUserSession().getUser());
            if (dao.getSuratId() != null){
                Surat surat = suratService.find(dao.getSuratId());
                model.setSurat(surat);
            } else {
                model.setSurat(null);
            }
            this.create(model);
        } else {
            model = find(id);
            MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
            model.setUser(userSession.getUserSession().getUser());
            model.setOrganisasi(org);
            if (dao.getSuratId() != null){
                Surat surat = suratService.find(dao.getSuratId());
                model.setSurat(surat);
            } else {
                model.setSurat(null);
            }
            this.edit(model);
        }
        return model;
    }

    public JPAJinqStream<ReferensiSurat> getBySurat(Long id) {
        Long idOrganization = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();
        return db().where(q -> q.getSurat().getId().equals(id) && q.getOrganisasi().getIdOrganization().equals(idOrganization));
    }

    public JPAJinqStream<ReferensiSurat> getByOrg(Long id,Long idSurat){
        Long idUser = userSession.getUserSession().getUser().getId();
        JPAJinqStream<ReferensiSurat> result = db().where(a->a.getUser().getId().equals(idUser)
                && a.getOrganisasi().getIdOrganization().equals(id));
        return result;
    }

    public JPAJinqStream<ReferensiSurat> getByOrgSurat(Long id,Long idSurat){
        Long idUser = userSession.getUserSession().getUser().getId();
        return db(ReferensiSuratDetail.class).where(a->a.getSurat().getId().equals(idSurat) && (a.getReferensiSurat().getOrganisasi()
                .getIdOrganization().equals(id) || a.getReferensiSurat().getUser().getId().equals(idUser)))
                .select(b->b.getReferensiSurat()).distinct();
    }

    public ReferensiSurat getBySuratOrCreate(Long id) {
        Surat surat = suratService.find(id);
        ReferensiSurat referensiSurat = db().where(q -> q.getSurat().equals(surat)&& q.getOrganisasi().equals(userSession.getUserSession().getUser().getOrganizationEntity())).findFirst().orElse(null);
        if(referensiSurat==null){
            referensiSurat.setSurat(surat);
            referensiSurat.setOrganisasi(userSession.getUserSession().getUser().getOrganizationEntity());
            this.create(referensiSurat);
        }
        return referensiSurat;
    }

    public void deleteBySuratId(Long suratId) {
        Long idOrganization = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();
         List<ReferensiSurat> temp = db().where(a->a.getSurat().getId().equals(suratId) &&
                a.getOrganisasi().getIdOrganization().equals(idOrganization)).collect(Collectors.toList());

         ReferensiSurat referensiSurat = new ReferensiSurat();
         if(!temp.isEmpty()){
            List<ReferensiSurat> refDetail = db(ReferensiSuratDetail.class)
                    .where(q -> q.getReferensiSurat().getSurat().getId().equals(suratId) && q.getReferensiSurat().getSurat().getId().equals(suratId) &&
                            q.getReferensiSurat().getOrganisasi().getIdOrganization().equals(idOrganization)).map(w->w.getReferensiSurat())
                    .collect(Collectors.toList());
            if(!refDetail.isEmpty()){
                referensiSurat = temp.stream().filter(a-> refDetail.stream().anyMatch(b->b.getId().equals(a.getId()))).findFirst().orElse(null);
            }
         }

        if (referensiSurat != null) {
            if (referensiSurat.getSuratDetail() != null) {
                throw new InternalServerErrorException(String.format("Referensi sudah terpakai di surat lain: %s", referensiSurat.getSurat().getNoDokumen()));
            }else{
                referensiSurat = temp.stream().filter(b->b.getSurat().getId().equals(suratId)).findFirst().get();
                remove(referensiSurat);
            }
        }
    }

    public HashMap<String, Object> getFiltered(Long idJabatan, Long idUser, String filter, int skip, int limit, Boolean descending, String sort) {
        List<SuratDto> result = new ArrayList<>();
        TypedQuery<ReferensiSurat> all = getData(idJabatan, idUser, filter, descending, sort);
        Long length = all.getResultList().stream().map(a->a.getSurat()).distinct().count();
        result = MappingToDto(all, skip, limit);
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    public TypedQuery<ReferensiSurat> getData(Long idJabatan, Long idUser, String filter, Boolean descending, String sort) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<ReferensiSurat> q = cb.createQuery(ReferensiSurat.class);
        Root<ReferensiSurat> root = q.from(ReferensiSurat.class);

        q.select(root);
        List<Predicate> predicates = new ArrayList<>();

        if(idUser!=null) {
            Predicate main = cb.or(
                    cb.equal(root.get("organisasi").get("idOrganization"), idJabatan),
                    cb.and(cb.isNotNull(root.get("user")), cb.equal(root.get("user").get("id"), idUser))
            );
            predicates.add(main);
        }else{
            Predicate main = cb.equal(root.get("organisasi").get("idOrganization"), idJabatan);
            predicates.add(main);
        }

        if(StringUtils.isNotEmpty(filter)){
            Predicate where = cb.like(root.join("surat",JoinType.LEFT).join("formValue",JoinType.LEFT).get("value"),"%" + filter + "%");
            predicates.add(where);
        }

        q.where(predicates.toArray(new Predicate[predicates.size()]));

        if (descending != null && !Strings.isNullOrEmpty(sort)) {
            if (descending) {
                q.orderBy(cb.desc(root.join("surat",JoinType.LEFT).get(sort)));
            } else {
                q.orderBy(cb.asc(root.join("surat",JoinType.LEFT).get(sort)));
            }
        }

        return getEntityManager().createQuery(q);
    }

    private List<SuratDto> MappingToDto(TypedQuery<ReferensiSurat> all, int skip, int limit) {
        List<SuratDto> result = null;

        if (all != null) {
            result = all.getResultStream().distinct().skip(skip).limit(limit).map(a->new SuratDto(a.getSurat())).collect(Collectors.toList());
        }
        return result;
    }
}
