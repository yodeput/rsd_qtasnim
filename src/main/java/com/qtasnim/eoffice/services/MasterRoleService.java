package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.MasterRole;
import org.jinq.orm.stream.JinqStream;
import org.jinq.tuples.Pair;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@LocalBean
public class MasterRoleService extends AbstractFacade<MasterRole> {
    @Override
    protected Class<MasterRole> getEntityClass() {
        return MasterRole.class;
    }

    public List<MasterRole> findRolesByUsername(String username) {
        return (List<MasterRole>) getEntityManager()
                .createQuery("SELECT A FROM MasterRole A LEFT JOIN A.organisasiList B WHERE B.user.loginUserName = :loginUserName")
                .setParameter("loginUserName", username)
                .getResultList();
    }

    public MasterRole defaultRole() {
        return db().where(t -> t.getLevel() == 0).limit(1).findFirst().orElse(null);
    }
}
