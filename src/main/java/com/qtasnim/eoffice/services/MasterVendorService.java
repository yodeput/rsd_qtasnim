package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterVendorDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;


@Stateless
@LocalBean
public class MasterVendorService extends AbstractFacade<MasterVendor> {
    private static final long serialVersionUID = 1L;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private MasterUserService userService;

    @Inject
    private MasterKategoriEksternalService kategoriEksternalService;

    @Inject
    private MasterPropinsiService propinsiService;

    @Inject
    private MasterKotaService kotaService;

    @Override
    protected Class<MasterVendor> getEntityClass() {
        return MasterVendor.class;
    }

    public JPAJinqStream<MasterVendor> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public JPAJinqStream<MasterVendor> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name));
    }
     public MasterVendor save(Long id, MasterVendorDto dao){
         MasterVendor model = new MasterVendor();
         if(id==null){
             this.create(data(model,dao));
             updateUserData(model,dao);
         } else {
             model = this.find(id);
             this.edit(data(model,dao));
             updateUserData(model,dao);
         }
        return model;
     }

     public MasterVendor data(MasterVendor model, MasterVendorDto dao){
         DateUtil dateUtil = new DateUtil();

         model.setNama(dao.getNama());
         model.setAlamat(dao.getAlamat());
         model.setPhone(dao.getPhone());
         model.setEmail(dao.getEmail());
         model.setPic(dao.getPic());
         model.setIsActive(dao.getIsActive());
         model.setNamaUnit(dao.getNamaUnit());
         model.setKodePos(dao.getKodePos());
         model.setWebsite(dao.getWebsite());
         model.setFax(dao.getFax());

         if(dao.getCompanyId()!=null){
             CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
             model.setCompanyCode(cCode);
         } else {
             model.setCompanyCode(null);
         }

         model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
         if(dao.getEndDate()!=null){
             model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
         } else {
             model.setEnd(dateUtil.getDefValue());
         }
         if(dao.getIdKategoriEksternal()!=null){
             MasterKategoriEksternal kategori = kategoriEksternalService.find(dao.getIdKategoriEksternal());
             model.setKategoriEksternal(kategori);
         }else{
             model.setKategoriEksternal(null);
         }

         if(dao.getIdPropinsi()!=null){
             MasterPropinsi propinsi = propinsiService.find(dao.getIdPropinsi());
             model.setPropinsi(propinsi);
         } else {
             model.setPropinsi(null);
         }

         if(dao.getIdKota()!=null){
             MasterKota kota = kotaService.find(dao.getIdPropinsi());
             model.setKota(kota);
         } else {
             model.setKota(null);
         }


         return model;
     }

     private void updateUserData(MasterVendor model, MasterVendorDto dao){
         getEntityManager().flush();
         if(dao.getIdUser()!=null){
             for(int i=0;i<dao.getIdUser().size();i++){
                 MasterUser user = userService.find(dao.getIdUser().get(i));
                 user.setVendor(model);
                 user.setIsInternal(false);
                 userService.edit(user);
             }
         } else {
             List<MasterUser> userList = userService.getResultList("vendor.id=="+model.getId(),"",0,0);
            if(!userList.isEmpty()){
                for(MasterUser user:userList){
                    user.setVendor(null);
                    user.setIsInternal(true);
                    userService.edit(user);
                }
            }
         }
     }

    public MasterVendor delete(Long id){
        MasterVendor model = this.find(id);
        this.remove(model);
        return model;
    }

    public JPAJinqStream<MasterVendor> getDataByEksternal(Long idKategoriEksternal){
        MasterKategoriEksternal kategori = kategoriEksternalService.find(idKategoriEksternal);
        return db().where(a->a.getKategoriEksternal().equals(kategori));
    }
}