package com.qtasnim.eoffice.services;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.cmis.ICMISProvider;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.jinq.jpa.JPAJinqStream;

import javax.activation.MimetypesFileTypeMap;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class SuratService extends AbstractFacade<Surat> {

    @Inject
    private ProcessTaskService processTaskService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private PemeriksaSuratService pemeriksaSuratService;

    @Inject
    private PenandatanganSuratService penandatanganSuratService;

    @Inject
    private TembusanSuratService tembusanSuratService;

    @Inject
    private FormDefinitionService formDefinitionService;

    @Inject
    private ReferensiSuratService referensiSuratService;

    @Inject
    private ReferensiSuratDetailService referensiSuratDetailService;

    @Inject
    private RegistrasiDokumenEksternalService dokumenEksternalService;

    @Inject
    private MasterKlasifikasiMasalahService masterKlasifikasiMasalahService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private PenerimaSuratService penerimaSuratService;

    @Inject
    private ParaService paraService;

    @Inject
    private FileService fileService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private ParaDetailService paraDetailService;

    @Inject
    private TembusanParaService tembusanParaService;

    @Inject
    private PenerimaParaService penerimaParaService;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private MasterDelegasiService delegasiService;

    @Inject
    private MasterNonStrukturalService nonStrukturalService;

    @Inject
    private MasterUserService userService;

    @Inject
    private TemplatingService templatingService;

    @Inject
    private SuratInteractionService interactionService;

    @Inject
    private SuratDisposisiService disposisiService;

    @Inject
    private ArsipService arsipService;

    @Inject
    private FormFieldService fieldService;

    @Inject
    private KonversiOnlyOfficeService konversiOnlyOfficeService;

    @Inject
    private MasterAutoNumberService autoNumberService;

    @Inject
    private WorkflowService workflowService;

    @Inject
    private MasterVendorService vendorService;

    @Inject
    private QRGenerateService qrGenerateService;

    @Inject
    private MasterJenisDokumenService masterJenisDokumenService;

    @Inject
    private MasterPrioritasService prioritasService;

    @Inject
    private MasterAreaService areaService;

    @Inject
    private ICMISProvider icmisProvider;

    @Inject
    private MasterKlasifikasiKeamananService klasifikasiKeamananService;
    @Inject
    private MasterTingkatUrgensiService tingkatUrgensiService;
    @Inject
    private MasterTingkatPerkembanganService tingkatPerkembanganService;

    @Override
    protected Class<Surat> getEntityClass() {
        return Surat.class;
    }

    public JPAJinqStream<Surat> getAll() {
        return db().where(q -> !q.getIsDeleted());
    }

    public Surat findSigned(Long suratId) {
        String approved = EntityStatus.APPROVED.toString();

        return db().where(t -> t.getId().equals(suratId) && t.getStatus().equals(approved)).limit(1).findFirst().orElse(null);
    }

    public Long generateNumber(Long id) {
        Long idSurat = null;
        try {
            Surat surat = this.find(id);
            String jenisDokumen = "";
            try {
                jenisDokumen = surat.getFormDefinition().getJenisDokumen().getKodeJenisDokumen();
            } catch (Exception ex) {
                jenisDokumen = "";
            }

            String number = autoNumberService.from(Surat.class).next(t -> t.getNoDokumen(), surat);
            surat.setNoDokumen(number);
            surat.setSubmittedDate(new Date());
            this.edit(surat);

            return surat.getId();
        } catch (Exception ex) {
            return null;
        }
    }

    public SuratDto saveOrEditDraft(Long id, SuratDto dao, String docId) throws Exception {
        SuratDto result;
        try {

            boolean isNew = id == null;
            Surat obj = new Surat();
            List<FieldDataDto> tmp = new ArrayList<>();

            for (MetadataValueDto ff : dao.getMetadata()) {
                Gson g = new Gson();
                if (ff.getFieldData() != null && ff.getFieldId() != null) {
                    FormField objF = fieldService.find(ff.getFieldId());
                    objF.setData(ff.getFieldData());
                    fieldService.edit(objF);
                    String json = ff.getFieldData().replace("\\", "");
                    FieldDataDto field = g.fromJson(json, FieldDataDto.class);
                    field.setName(ff.getName());
                    tmp.add(field);
                }
            }

            if (isNew) {

                /*Save Surat*/
                obj.setIsDeleted(dao.getIsDeleted());
                obj.setKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
                FormDefinition form = formDefinitionService.find(dao.getIdFormDefinition());
                obj.setFormDefinition(form);
                obj.setParent(dao.getIdParent() != null ? this.find(dao.getIdParent()) : null);
                obj.setStatus(dao.getStatus());
                obj.setParent(dao.getIdParent() != null ? this.find(dao.getIdParent()) : null);
                obj.setCompanyCode(dao.getIdCompany() != null ? companyCodeService.find(dao.getIdCompany()) : null);
                obj.setIsPemeriksaParalel(dao.getIsPemeriksaParalel());
                this.create(obj);
                getEntityManager().flush();
                /*end save surat*/

                /*save pemeriksa*/
                if (tmp.stream().anyMatch(a -> a.getName().toLowerCase().contains("penandatangan"))) {
                    FieldDataDto objField = tmp.stream().filter(a -> a.getName().toLowerCase().contains("penandatangan")).findFirst().orElse(null);
                    if (objField != null) {
                        if (objField.getVMySelf()) {
                            MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
                            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                            penandatanganSurat.setOrganization(org);
                            penandatanganSurat.setSeq(0);
                            penandatanganSurat.setSurat(obj);
                            penandatanganSuratService.create(penandatanganSurat);
                        } else {
                            if (!dao.getPemeriksaSurat().isEmpty()) {
                                for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
                                    MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
                                    PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
                                    pemeriksaSurat.setOrganization(pemeriksa);
                                    pemeriksaSurat.setSeq(pem.getSeq());
                                    pemeriksaSurat.setSurat(obj);
                                    pemeriksaSuratService.create(pemeriksaSurat);
                                }
                            }
                            /*end save pemeriksa*/

                            /*save penandatangan*/
                            if (!dao.getPenandatanganSurat().isEmpty()) {
                                FieldDataDto objField2 = tmp.stream().filter(a -> a.getName().toLowerCase().contains("atas nama")).findFirst().orElse(null);
                                if (objField2 != null) {
                                    if (objField2.getVDirectChief() != null && objField2.getVDirectChief()) {
                                        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity().getParent();
                                        PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                        penandatanganSurat.setOrganization(org);
                                        penandatanganSurat.setSeq(0);
                                        penandatanganSurat.setSurat(obj);
                                        penandatanganSuratService.create(penandatanganSurat);
                                    } else {
                                        for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat().stream().distinct().collect(Collectors.toList())) {
                                            MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
                                            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                            penandatanganSurat.setOrganization(penandatangan);
                                            penandatanganSurat.setSeq(ttd.getSeq());
                                            penandatanganSurat.setSurat(obj);
                                            if (ttd.getIdANOrg() != null) {
                                                MasterStrukturOrganisasi orgAN = masterStrukturOrganisasiService.find(ttd.getIdANOrg());
                                                penandatanganSurat.setOrgAN(orgAN);
                                            }
                                            penandatanganSuratService.create(penandatanganSurat);
                                        }
                                    }
                                } else {
                                    for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat().stream().distinct().collect(Collectors.toList())) {
                                        MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
                                        PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                        penandatanganSurat.setOrganization(penandatangan);
                                        penandatanganSurat.setSeq(ttd.getSeq());
                                        penandatanganSurat.setSurat(obj);
                                        penandatanganSuratService.create(penandatanganSurat);
                                    }
                                }
                            }
                        }
                    }
                }
                /*end save penandatangan*/

                /*save tembusan*/
                if (!dao.getTembusanSurat().isEmpty()) {
                    for (TembusanSuratDto tembus : dao.getTembusanSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi tembusan = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        TembusanSurat tembusanSurat = new TembusanSurat();
                        if (tembus.getIdOrganization() != null) {
                            tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setOrganization(tembusan);
                            tembusanSuratService.create(tembusanSurat);
                        } else {
                            paraObj = paraService.find(tembus.getIdPara());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setPara(paraObj);
                            tembusanSuratService.create(tembusanSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    TembusanPara tp = new TembusanPara();
                                    tp.setOrganisasi(pdd.getOrganisasi());
                                    tp.setTembusanSurat(tembusanSurat);
                                    tembusanParaService.create(tp);
                                }
                            }
                        }
                    }
                }
                /*end save tembusan*/

                /*save penerima*/
                if (!dao.getPenerimaSurat().isEmpty()) {
                    for (PenerimaSuratDto terima : dao.getPenerimaSurat().stream().distinct().collect(Collectors.toList())) {
                        PenerimaSurat penerimaSurat = new PenerimaSurat();
                        MasterStrukturOrganisasi penerima = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        if (terima.getIdOrganization() != null) {
                            penerima = masterStrukturOrganisasiService.find(terima.getIdOrganization());
                            penerimaSurat.setOrganization(penerima);
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSuratService.create(penerimaSurat);
                        } else {
                            paraObj = paraService.find(terima.getIdPara());
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSurat.setPara(paraObj);
                            penerimaSuratService.create(penerimaSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    PenerimaPara pp = new PenerimaPara();
                                    pp.setOrganisasi(pdd.getOrganisasi());
                                    pp.setPenerimaSurat(penerimaSurat);
                                    penerimaParaService.create(pp);
                                }
                            }
                        }
                    }
                }
                /*end save penerima*/

                pairDocument(obj.getId(), docId);
            } else {
                obj = this.find(id);

                /*Save Surat*/
                obj.setIsDeleted(dao.getIsDeleted());
                obj.setKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
                FormDefinition form = formDefinitionService.find(dao.getIdFormDefinition());
                obj.setFormDefinition(form);
                obj.setParent(dao.getIdParent() != null ? this.find(dao.getIdParent()) : null);
                obj.setStatus(dao.getStatus());
                obj.setParent(dao.getIdParent() != null ? this.find(dao.getIdParent()) : null);
                obj.setCompanyCode(dao.getIdCompany() != null ? companyCodeService.find(dao.getIdCompany()) : null);
                obj.setIsPemeriksaParalel(dao.getIsPemeriksaParalel());
                this.edit(obj);
                getEntityManager().flush();
                /*end save surat*/

                /*save pemeriksa*/
                if (tmp.stream().anyMatch(a -> a.getName().toLowerCase().contains("penandatangan"))) {
                    FieldDataDto objField = tmp.stream().filter(a -> a.getName().toLowerCase().contains("penandatangan")).findFirst().orElse(null);
                    if (objField != null) {
                        if (objField.getVMySelf()) {
                            List<PenandatanganSurat> rmvPenandatangan = penandatanganSuratService.getAllBy(dao.getId());
                            if (!rmvPenandatangan.isEmpty()) {
                                for (PenandatanganSurat ps : rmvPenandatangan) {
                                    penandatanganSuratService.remove(ps);
                                }
                            }

                            MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
                            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                            penandatanganSurat.setOrganization(org);
                            penandatanganSurat.setSeq(0);
                            penandatanganSurat.setSurat(obj);
                            penandatanganSuratService.create(penandatanganSurat);
                        } else {
                            if (!dao.getPemeriksaSurat().isEmpty()) {
                                List<PemeriksaSurat> rmvPemeriksa = pemeriksaSuratService.getAllBy(dao.getId());
                                if (!rmvPemeriksa.isEmpty()) {
                                    for (PemeriksaSurat ps : rmvPemeriksa) {
                                        pemeriksaSuratService.remove(ps);
                                    }
                                }

                                for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
                                    MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
                                    PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
                                    pemeriksaSurat.setOrganization(pemeriksa);
                                    pemeriksaSurat.setSeq(pem.getSeq());
                                    pemeriksaSurat.setSurat(obj);
                                    pemeriksaSuratService.create(pemeriksaSurat);
                                }
                            }
                            /*end save pemeriksa*/

                            /*save penandatangan*/
                            if (!dao.getPenandatanganSurat().isEmpty()) {
                                List<PenandatanganSurat> rmvPenandatangan = penandatanganSuratService.getAllBy(dao.getId());
                                if (!rmvPenandatangan.isEmpty()) {
                                    for (PenandatanganSurat ps : rmvPenandatangan) {
                                        penandatanganSuratService.remove(ps);
                                    }
                                }

                                FieldDataDto objField2 = tmp.stream().filter(a -> a.getName().toLowerCase().contains("atas nama")).findFirst().orElse(null);
                                if (objField2 != null) {
                                    if (objField2.getVDirectChief() != null && objField2.getVDirectChief()) {
                                        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity().getParent();
                                        PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                        penandatanganSurat.setOrganization(org);
                                        penandatanganSurat.setSeq(0);
                                        penandatanganSurat.setSurat(obj);
                                        penandatanganSuratService.create(penandatanganSurat);
                                    } else {
                                        for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat().stream().distinct().collect(Collectors.toList())) {
                                            MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
                                            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                            penandatanganSurat.setOrganization(penandatangan);
                                            penandatanganSurat.setSeq(ttd.getSeq());
                                            penandatanganSurat.setSurat(obj);
                                            if (ttd.getIdANOrg() != null) {
                                                MasterStrukturOrganisasi orgAN = masterStrukturOrganisasiService.find(ttd.getIdANOrg());
                                                penandatanganSurat.setOrgAN(orgAN);
                                            }
                                            penandatanganSuratService.create(penandatanganSurat);
                                        }
                                    }
                                } else {
                                    for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat().stream().distinct().collect(Collectors.toList())) {
                                        MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
                                        PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                        penandatanganSurat.setOrganization(penandatangan);
                                        penandatanganSurat.setSeq(ttd.getSeq());
                                        penandatanganSurat.setSurat(obj);
                                        penandatanganSuratService.create(penandatanganSurat);
                                    }
                                }
                            }
                        }
                    }
                }
                /*end save penandatangan*/

                /*save tembusan*/
                if (!dao.getTembusanSurat().isEmpty()) {
                    List<TembusanSurat> rmvTembusan = tembusanSuratService.getAllBy(id);
                    if (!rmvTembusan.isEmpty()) {
                        for (TembusanSurat bs : rmvTembusan) {
                            List<TembusanPara> tp = tembusanParaService.getListTembusanPara(bs.getId());
                            if (tp.isEmpty()) {
                                tembusanSuratService.remove(bs);
                            } else {
                                for (TembusanPara ctr : tp) {
                                    tembusanParaService.remove(ctr);
                                }
                                tembusanSuratService.remove(bs);
                            }
                        }
                    }
                    for (TembusanSuratDto tembus : dao.getTembusanSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi tembusan = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        TembusanSurat tembusanSurat = new TembusanSurat();
                        if (tembus.getIdOrganization() != null) {
                            tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setOrganization(tembusan);
                            tembusanSuratService.create(tembusanSurat);
                        } else {
                            paraObj = paraService.find(tembus.getIdPara());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setPara(paraObj);
                            tembusanSuratService.create(tembusanSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    TembusanPara tp = new TembusanPara();
                                    tp.setOrganisasi(pdd.getOrganisasi());
                                    tp.setTembusanSurat(tembusanSurat);
                                    tembusanParaService.create(tp);
                                }
                            }
                        }
                    }
                }
                /*end save tembusan*/

                /*save penerima*/
                if (!dao.getPenerimaSurat().isEmpty()) {
                    List<PenerimaSurat> rmvPenerima = penerimaSuratService.getAllBy(id);
                    if (!rmvPenerima.isEmpty()) {
                        for (PenerimaSurat bs : rmvPenerima) {
                            List<PenerimaPara> tp = penerimaParaService.getPenerimaParaList(bs.getId());
                            if (tp.isEmpty()) {
                                penerimaSuratService.remove(bs);
                            } else {
                                for (PenerimaPara ctr : tp) {
                                    penerimaParaService.remove(ctr);
                                }
                                penerimaSuratService.remove(bs);
                            }
                        }
                    }
                    for (PenerimaSuratDto terima : dao.getPenerimaSurat().stream().distinct().collect(Collectors.toList())) {
                        PenerimaSurat penerimaSurat = new PenerimaSurat();
                        MasterStrukturOrganisasi penerima = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        if (terima.getIdOrganization() != null) {
                            penerima = masterStrukturOrganisasiService.find(terima.getIdOrganization());
                            penerimaSurat.setOrganization(penerima);
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSuratService.create(penerimaSurat);
                        } else {
                            paraObj = paraService.find(terima.getIdPara());
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSurat.setPara(paraObj);
                            penerimaSuratService.create(penerimaSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    PenerimaPara pp = new PenerimaPara();
                                    pp.setOrganisasi(pdd.getOrganisasi());
                                    pp.setPenerimaSurat(penerimaSurat);
                                    penerimaParaService.create(pp);
                                }
                            }
                        }
                    }
                }
                /*end save penerima*/
                GlobalAttachment draftDoc = globalAttachmentService.getDraftDocDataByRef("t_surat", id).findFirst().orElse(null);

                docId = draftDoc.getDocId();

                TemplatingDto templatingDto = new TemplatingDto(dao.getIdFormDefinition(), dao.getMetadata(), docId);

                templatingService.directTemplating(templatingDto);
            }

            getEntityManager().flush();
            getEntityManager().refresh(obj);

            if (dao.getReferensiId() != null && !dao.getReferensiId().isEmpty()) {
                for (Long a : dao.getReferensiId()) {
                    ReferensiSurat referensiSurat = referensiSuratService.find(a);
                    ReferensiSuratDetail referensiSuratDetail = new ReferensiSuratDetail();
                    referensiSuratDetail.setSurat(obj);
                    referensiSuratDetail.setReferensiSurat(referensiSurat);
                    referensiSuratDetailService.create(referensiSuratDetail);
                }
            }

            formDefinitionService.saveDynamicForm(dao, obj, isNew, this);
//            IWorkflowProvider workflowProvider = masterWorkflowProviderService.getByProviderName("camunda").getWorkflowProvider();
//            workflowProvider.startWorkflow(obj, dao.getWorkflowFormName());

            result = new SuratDto(obj);

            return result;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public JPAJinqStream<Surat> getDraft() {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        return db().where(q -> q.getKonseptor().equals(org) && !q.getIsDeleted() && q.getStatus().equals("DRAFT"));
    }

    public List<DokumenDto> getKoreksi(int skip, int limit) {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
//        return db().where(c -> !c.getIsDeleted() && c.getStatus().equals("SUBMITTED") &&
//                (c.getPemeriksaSurat().stream().anyMatch(q->q.getOrganization().equals(org))
//                        || c.getPenandatanganSurat().stream().anyMatch(q->q.getOrganization().equals(org))));
//        TypedQuery<Surat> all = this.getResultList("SUBMITTED");
//        List<DokumenDto> result = this.MappingToDto(all, skip, limit);
        //JPAJinqStream<ProcessTask> processTasks = processTaskService.getPendingTaskByOrgId(org.getIdOrganization());
        List<Long> idOrg = new ArrayList<>();
        List<MasterDelegasi> delegasiList = delegasiService.getByToNoTipe();
        for (MasterDelegasi delegasi : delegasiList) {
            idOrg.add(delegasi.getFrom().getIdOrganization());
        }
        JPAJinqStream<ProcessTask> processTasks = processTaskService.getPendingTaskByOrgId(org.getIdOrganization());
        for (Long i : idOrg) {
            processTasks = processTasks.orUnion(processTaskService.getPendingTaskByOrgId(i));
        }
        List<DokumenDto> result = processTasks.skip(skip).limit(limit)
                .sorted(Comparator.comparing(ProcessTask::getAssignedDate).reversed())
                .map(q -> {
                    DokumenDto obj = new DokumenDto();
                    DateUtil dateUtil = new DateUtil();
                    Long idSurat = Long.parseLong(q.getProcessInstance().getRecordRefId());
                    Surat surat = this.find(idSurat);
                    obj.setId(surat.getId());
                    obj.setTanggal(dateUtil.getCurrentISODate(surat.getCreatedDate()));
                    FormValue fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                            .findFirst().orElse(null);
                    if (fv != null) {
                        obj.setPerihal(fv.getValue());
                    }
                    obj.setAksi(q.getTaskName());
                    MasterJenisDokumen jenisDokumen = surat.getFormDefinition().getJenisDokumen();
                    obj.setJenisDokumen(jenisDokumen.getKodeJenisDokumen() + " " + jenisDokumen.getNamaJenisDokumen());
                    obj.setKorespondensi(jenisDokumen.getIsKorespondensi());
                    if (obj.isKorespondensi()) {
                        List<PengirimDto> pObj = surat.getPenandatanganSurat().stream().map(b -> {
                            PengirimDto pengirim = new PengirimDto();
                            pengirim.setJabatan(b.getOrganization().getOrganizationName());
                            pengirim.setNama(b.getOrganization().getUser().getNameFront() + " " + b.getOrganization().getUser().getNameMiddleLast());
                            pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                            return pengirim;
                        }).collect(Collectors.toList());
                        obj.setPengirim(pObj);
                    } else {
                        List<PengirimDto> pObj = new ArrayList<>();
                        FormValue fv2 = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("dari"))
                                .findFirst().orElse(null);
                        if (fv2 != null) {
                            MasterVendor vendor = vendorService.find(Long.parseLong(fv2.getValue()));
                            PengirimDto po = new PengirimDto();
                            po.setJabatan(vendor.getNama());
                            pObj.add(po);
                        }
                        obj.setPengirim(pObj);
                    }
                    return obj;
                }).collect(Collectors.toList());
        return result;
    }

    //    public List<DokumenDto> getExecuted(int skip, int limit) {
//        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
//        JPAJinqStream<ProcessTask> processTasks = processTaskService.getExecutedTaskByOrgId(org.getIdOrganization());
//        List<DokumenDto> result = processTasks.map(q->{
//            DokumenDto obj = new DokumenDto();
//            DateUtil dateUtil = new DateUtil();
//            Long idSurat = Long.parseLong(q.getProcessInstance().getRecordRefId());
//            Surat surat = this.find(idSurat);
//            obj.setId(surat.getId());
//            obj.setTanggal(dateUtil.getCurrentISODate(surat.getCreatedDate()));
//            FormValue fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
//                    .findFirst().orElse(null);
//            if (fv != null) {
//                obj.setPerihal(fv.getValue());
//            }
//            List<PengirimDto> pObj = surat.getPenandatanganSurat().stream().map(b -> {
//                PengirimDto pengirim = new PengirimDto();
//                pengirim.setJabatan(b.getOrganization().getOrganizationName());
//                pengirim.setNama(b.getOrganization().getUser().getNameFront()+" "+b.getOrganization().getUser().getNameMiddleLast());
//                return pengirim;
//            }).collect(Collectors.toList());
//            obj.setPengirim(pObj);
//            obj.setAksi(q.getTaskName());
//            return obj;
//        }).collect(Collectors.toList());
//
//        List<Surat> surat = getByKonseptor(org);
//        for(Surat s:surat){
//            DokumenDto obj = new DokumenDto();
//            DateUtil dateUtil = new DateUtil();
//            obj.setId(s.getId());
//            obj.setTanggal(dateUtil.getCurrentISODate(s.getCreatedDate()));
//            FormValue fv = s.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
//                    .findFirst().orElse(null);
//            if (fv != null) {
//                obj.setPerihal(fv.getValue());
//            }
//            List<PengirimDto> pObj = s.getPenandatanganSurat().stream().map(b -> {
//                PengirimDto pengirim = new PengirimDto();
//                pengirim.setJabatan(b.getOrganization().getOrganizationName());
//                pengirim.setNama(b.getOrganization().getUser().getNameFront()+" "+b.getOrganization().getUser().getNameMiddleLast());
//                return pengirim;
//            }).collect(Collectors.toList());
//            obj.setPengirim(pObj);
//            result.add(obj);
//        }
//        Collections.sort(result,Comparator.comparing(DokumenDto::getTanggal).reversed());
//        return result;
//    }
    public List<DokumenDto> getExecuted(int skip, int limit) {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        List<Surat> konseptorSurat = getByKonseptor(org);
        List<Long> suratIds = processTaskService.getExecutedTaskByOrgId(org.getIdOrganization()).toList().stream().map(a -> Long.parseLong(a.getProcessInstance()
                .getRecordRefId())).distinct().collect(Collectors.toList());
        List<Surat> processSurat = new ArrayList<>();
        for (Long ids : suratIds) {
            Surat tmpSurat = this.find(ids);
            processSurat.add(tmpSurat);
        }
        List<Surat> allTmp = new ArrayList<>();
        allTmp.addAll(konseptorSurat);
        allTmp.addAll(processSurat);
        List<DokumenDto> result = allTmp.stream().map(q -> {
            DokumenDto obj = new DokumenDto();
            DateUtil dateUtil = new DateUtil();
            obj.setId(q.getId());
            obj.setTanggal(dateUtil.getCurrentISODate(q.getCreatedDate()));
            FormValue fv = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                    .findFirst().orElse(null);
            if (fv != null) {
                obj.setPerihal(fv.getValue());
            }
            MasterJenisDokumen jenisDokumen = q.getFormDefinition().getJenisDokumen();
            obj.setKorespondensi(jenisDokumen.getIsKorespondensi());
            if (obj.isKorespondensi()) {
                List<PengirimDto> pObj = q.getPenandatanganSurat().stream().map(b -> {
                    PengirimDto pengirim = new PengirimDto();
                    pengirim.setJabatan(b.getOrganization().getOrganizationName());
                    pengirim.setNama(b.getOrganization().getUser().getNameFront() + " " + b.getOrganization().getUser().getNameMiddleLast());
                    pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                    return pengirim;
                }).collect(Collectors.toList());
                obj.setPengirim(pObj);
            } else {
                List<PengirimDto> pObj = new ArrayList<>();
                FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("dari"))
                        .findFirst().orElse(null);
                if (fv2 != null) {
                    MasterVendor vendor = vendorService.find(Long.parseLong(fv2.getValue()));
                    PengirimDto po = new PengirimDto();
                    po.setJabatan(vendor.getNama());
                    pObj.add(po);
                }
                obj.setPengirim(pObj);
            }
            ProcessTask processtask = processTaskService.getByRecordId(q.getId());
            if (processtask != null) {
                obj.setAksi(processtask.getTaskName());
            }
            return obj;
        }).collect(Collectors.toList());
        Collections.sort(result, Comparator.comparing(DokumenDto::getTanggal).reversed());
        return result;
    }

    public HashMap<String, Object> getByPenerima(int skip, int limit) {
        TypedQuery<Surat> all = getDataByQuery("APPROVED", null, null, true, null, null, null, null, skip, limit);
        List<SuratDto> result = new ArrayList<>();
        result = MappingSuratDto(all);
        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    public JPAJinqStream<Surat> getDetailByIdSurat(Long idSurat) {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        return db().where(c -> !c.getIsDeleted() && c.getStatus().equals("APPROVED") && c.getPenerimaSurat().stream().anyMatch(q -> q.getOrganization().equals(org))
                && c.getId().equals(idSurat));
    }

    public HashMap<String, Object> getByPenerimaFilter(String filter, String sort, Boolean descending, int skip, int limit) {
        TypedQuery<Surat> all = getDataByQuery("APPROVED", null, null, true, null, filter, sort, descending, skip, limit);
        List<SuratDto> result = new ArrayList<>();
        result = MappingSuratDto(all);
        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    public HashMap<String, Object> getInboxList(String tipe, String sortField, Boolean descending, int skip, int limit) {
        List<DokumenMasukDto> result = new ArrayList<>();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        if (tipe.isEmpty() || tipe.equals("")) {
            MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
//            return db().where(a -> !a.getIsDeleted() && a.getStatus().equals("APPROVED") && a.getNoDokumen() != null
//                    && a.getPenerimaSurat().stream().anyMatch(q->q.getOrganization().equals(org)));
//            TypedQuery<Surat> all = this.getResultList2("APPROVED", org.getIdOrganization(), descending, sortField);
            TypedQuery<Surat> all = getDataByQuery("APPROVED", null, null, true, null, null, sortField, descending, skip, limit);
            result = this.MappingDokumenMasuk(all, tipe);
            Long length = all.getResultStream().count();
            retur.put("data", result);
            retur.put("length", length);
            return retur;
        } else if (tipe.toLowerCase().equals("tim") || tipe.toLowerCase().equals("team")) {
            MasterNonStruktural nonStruktural = nonStrukturalService.getByUserLogin();
            if (nonStruktural != null) {
                MasterStrukturOrganisasi org = nonStruktural.getOrganisasi();
//            return db().where(a -> !a.getIsDeleted() && a.getStatus().equals("APPROVED") && a.getNoDokumen() != null
//                    && a.getPenerimaSurat().stream().anyMatch(q->q.getOrganization().equals(org)));
                TypedQuery<Surat> all = getDataByQuery("APPROVED", org.getIdOrganization(), null, true, null, null, sortField, descending, skip, limit);
                result = this.MappingDokumenMasuk(all, tipe);
                Long length = all.getResultStream().count();
                retur.put("data", result);
                retur.put("length", length);
                return retur;
            } else {
                return null;
            }
        } else {
            MasterDelegasi delegasi = delegasiService.getByTo(tipe);
            if (delegasi != null) {
                MasterStrukturOrganisasi org = delegasi.getFrom();
//                return db().where(a -> !a.getIsDeleted() && a.getStatus().equals("APPROVED") && a.getNoDokumen() != null
//                        && a.getPenerimaSurat().stream().anyMatch(q->q.getOrganization().equals(org)));
                TypedQuery<Surat> all = getDataByQuery("APPROVED", org.getIdOrganization(), null, true, null, null, sortField, descending, skip, limit);
                result = this.MappingDokumenMasuk(all, tipe);
                Long length = all.getResultStream().count();
                retur.put("data", result);
                retur.put("length", length);
                return retur;
            } else {
                return null;
            }

        }
    }

    public void uploadAttachment(Long idSurat, InputStream inputStream, FormDataContentDisposition formDataContentDisposition, FormDataBodyPart body) throws Exception {
        String docId = "";

        try {
            String filename = formDataContentDisposition.getFileName();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is2 = new ByteArrayInputStream(baos.toByteArray());
            InputStream is3 = new ByteArrayInputStream(baos.toByteArray());

            byte[] documentContent = IOUtils.toByteArray(is1);

            docId = fileService.upload(filename, documentContent);
            docId = docId.replace("workspace://SpacesStore/", "");

            Surat surat = this.find(idSurat);

//            String number = autoNumberService.from(SuratDocLib.class).next(SuratDocLib::getNumber);
            List<String> extractedText = textExtractionService.parsingFile(is2, formDataContentDisposition, body).getTexts();
            String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

            GlobalAttachment obj = new GlobalAttachment();

            obj.setMimeType(body.getMediaType().toString());
            obj.setDocGeneratedName(UUID.randomUUID().toString());
            obj.setDocName(filename);
            obj.setCompanyCode(surat.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(false);
            obj.setIsPublished(false);
            obj.setSize(new Long(documentContent.length));
            obj.setIsDeleted(false);
            obj.setReferenceId(idSurat);
            obj.setReferenceTable("t_surat");
            obj.setNumber("test 123");
            obj.setExtractedText(jsonRepresentation);

            globalAttachmentService.create(obj);
        } catch (Exception ex) {
            if (!org.apache.commons.lang.StringUtils.isBlank(docId)) {
                fileService.delete(docId);
            }

            throw ex;
        }
    }

    public void deleteAttachment(Long idSurat, String docId) {
        GlobalAttachment attachment = globalAttachmentService.findByDocId("t_surat", idSurat, docId);

        if (attachment != null) {
            globalAttachmentService.remove(attachment);
            fileService.delete(docId);
        }
    }

    public void pairDocument(Long idSurat, String docId) throws Exception {
        if (idSurat == null) {
            throw new Exception("Surat Service.pairDocument : null idSurat");
        }
        if (StringUtils.isBlank(docId)) {
            throw new Exception("Surat Service.pairDocument : blank docId");
        }

        try {
            if (globalAttachmentService.isTransactionAlreadyHasDoc("t_surat", idSurat, true, false, false)) {
                throw new Exception("Surat Service.pairDocument : surat telah memiliki dokumen konsep");
            }

            String tempFileName = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID() + ".docx";

            FileOutputStream fos = new FileOutputStream(tempFileName);
            byte[] doc = fileService.download(docId);
            fos.write(doc);
            fos.close();

            File tempFile = new File(tempFileName);

            MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
            String mimeType = fileTypeMap.getContentType(tempFile.getName());

            Surat surat = this.find(idSurat);

            if (surat == null) {
                throw new Exception("Surat Service.pairDocument : null surat, surat not found");
            }

            GlobalAttachment obj = new GlobalAttachment();
            obj.setMimeType(mimeType);
            obj.setDocGeneratedName(UUID.randomUUID().toString());
            obj.setDocName(tempFile.getName());
            obj.setCompanyCode(surat.getCompanyCode());
            obj.setDocId(docId);
            obj.setIsKonsep(true);
            obj.setIsPublished(false);
            obj.setIsDeleted(false);
            obj.setSize(tempFile.length());
            obj.setReferenceTable("t_surat");
            obj.setReferenceId(idSurat);
            obj.setNumber("test 123");

            Files.deleteIfExists(tempFile.toPath());

            globalAttachmentService.create(obj);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public SuratDto kirimSurat(SuratDto dao) throws Exception {
        return kirimSurat(dao, null);
    }

    public SuratDto kirimSurat(SuratDto dao, String docId) throws Exception {
        DateUtil dateUtil = new DateUtil();
        Date current = new Date();
        boolean isNew = dao.getId() == null;
        Surat obj = new Surat();
        SuratDto result;
        String workflowStatus = "DRAFT";
        Boolean isKonseptorTTD = false;
        try {
            /*get formField*/
            List<FieldDataDto> tmp = new ArrayList<>();
            for (MetadataValueDto ff : dao.getMetadata()) {
                Gson g = new Gson();
                if (ff.getFieldData() != null && ff.getFieldId() != null) {
                    FormField objF = fieldService.find(ff.getFieldId());
                    objF.setData(ff.getFieldData());
                    fieldService.edit(objF);
                    String json = ff.getFieldData().replace("\\", "");
                    FieldDataDto field = g.fromJson(json, FieldDataDto.class);
                    field.setName(ff.getName());
                    tmp.add(field);
                }
            }

            if (isNew) {
                /*Save Surat*/
                obj.setIsDeleted(dao.getIsDeleted());
                obj.setKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());

                FormDefinition form = formDefinitionService.find(dao.getIdFormDefinition());
                obj.setFormDefinition(form);
                obj.setParent(dao.getIdParent() != null ? this.find(dao.getIdParent()) : null);
                obj.setStatus(dao.getStatus());
                obj.setParent(dao.getIdParent() != null ? this.find(dao.getIdParent()) : null);
                obj.setCompanyCode(dao.getIdCompany() != null ? companyCodeService.find(dao.getIdCompany()) : null);
                obj.setIsPemeriksaParalel(dao.getIsPemeriksaParalel());
                this.create(obj);
                getEntityManager().flush();
                /*end save surat*/

                if (tmp.stream().anyMatch(a -> a.getName().toLowerCase().contains("penandatangan"))) {
                    FieldDataDto objField = tmp.stream().filter(a -> a.getName().toLowerCase().contains("penandatangan")).findFirst().orElse(null);
                    if (objField != null) {
                        if (objField.getVMySelf()) {
                            MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
                            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                            penandatanganSurat.setOrganization(org);
                            penandatanganSurat.setSeq(0);
                            penandatanganSurat.setSurat(obj);
                            penandatanganSuratService.create(penandatanganSurat);
                            isKonseptorTTD = true;
                        } else {
                            if (!dao.getPemeriksaSurat().isEmpty()) {
                                for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
                                    MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
                                    PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
                                    pemeriksaSurat.setOrganization(pemeriksa);
                                    pemeriksaSurat.setSeq(pem.getSeq());
                                    pemeriksaSurat.setSurat(obj);
                                    pemeriksaSuratService.create(pemeriksaSurat);
                                }
                            }
                            /*end save pemeriksa*/

                            /*save penandatangan*/
                            if (!dao.getPenandatanganSurat().isEmpty()) {
                                FieldDataDto objField2 = tmp.stream().filter(a -> a.getName().toLowerCase().contains("atas nama")).findFirst().orElse(null);
                                if (objField2 != null) {
                                    if (objField2.getVDirectChief() != null && objField2.getVDirectChief()) {
                                        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity().getParent();
                                        PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                        penandatanganSurat.setOrganization(org);
                                        penandatanganSurat.setSeq(0);
                                        penandatanganSurat.setSurat(obj);
                                        penandatanganSuratService.create(penandatanganSurat);
                                    } else {
                                        for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat().stream().distinct().collect(Collectors.toList())) {
                                            MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
                                            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                            penandatanganSurat.setOrganization(penandatangan);
                                            penandatanganSurat.setSeq(ttd.getSeq());
                                            penandatanganSurat.setSurat(obj);

                                            if (ttd.getIdANOrg() != null) {
                                                MasterStrukturOrganisasi orgAN = masterStrukturOrganisasiService.find(ttd.getIdANOrg());
                                                penandatanganSurat.setOrgAN(orgAN);
                                            }

                                            //penandatanganSurat.setUser(penandatangan.getUser());
                                            penandatanganSuratService.create(penandatanganSurat);
                                        }
                                    }
                                } else {
                                    for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat().stream().distinct().collect(Collectors.toList())) {
                                        MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
                                        PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                        penandatanganSurat.setOrganization(penandatangan);
                                        penandatanganSurat.setSeq(ttd.getSeq());
                                        penandatanganSurat.setSurat(obj);

                                        //penandatanganSurat.setUser(penandatangan.getUser());
                                        penandatanganSuratService.create(penandatanganSurat);
                                    }
                                }
                            }
                        }
                    }
                }


                /*save tembusan*/
                if (!dao.getTembusanSurat().isEmpty()) {
                    for (TembusanSuratDto tembus : dao.getTembusanSurat().stream().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi tembusan = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        TembusanSurat tembusanSurat = new TembusanSurat();
                        if (tembus.getIdOrganization() != null) {
                            tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setOrganization(tembusan);
                            tembusanSuratService.create(tembusanSurat);
                        } else {
                            paraObj = paraService.find(tembus.getIdPara());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setPara(paraObj);
                            tembusanSuratService.create(tembusanSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    TembusanPara tp = new TembusanPara();
                                    tp.setOrganisasi(pdd.getOrganisasi());
                                    tp.setTembusanSurat(tembusanSurat);
                                    tembusanParaService.create(tp);
                                }
                            }
                        }
                    }
                }
                /*end save tembusan*/

                /*save penerima*/
                if (!dao.getPenerimaSurat().isEmpty()) {
                    for (PenerimaSuratDto terima : dao.getPenerimaSurat().stream().collect(Collectors.toList())) {
                        PenerimaSurat penerimaSurat = new PenerimaSurat();
                        MasterStrukturOrganisasi penerima = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        if (terima.getIdOrganization() != null) {
                            penerima = masterStrukturOrganisasiService.find(terima.getIdOrganization());
                            penerimaSurat.setOrganization(penerima);
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSuratService.create(penerimaSurat);
                        } else {
                            paraObj = paraService.find(terima.getIdPara());
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSurat.setPara(paraObj);
                            penerimaSuratService.create(penerimaSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    PenerimaPara pp = new PenerimaPara();
                                    pp.setOrganisasi(pdd.getOrganisasi());
                                    pp.setPenerimaSurat(penerimaSurat);
                                    penerimaParaService.create(pp);
                                }
                            }
                        }
                    }
                }
                /*end save penerima*/

                pairDocument(obj.getId(), docId);
            } else {
                obj = this.find(dao.getId());

                // trigger: start workflow
                workflowStatus = obj.getStatus();

                /*Save Surat*/
                obj.setIsDeleted(dao.getIsDeleted());
                obj.setKonseptor(userSession.getUserSession().getUser().getOrganizationEntity());
                FormDefinition form = formDefinitionService.find(dao.getIdFormDefinition());
                obj.setFormDefinition(form);
                obj.setParent(dao.getIdParent() != null ? this.find(dao.getIdParent()) : null);
                obj.setStatus(dao.getStatus());
                obj.setParent(dao.getIdParent() != null ? this.find(dao.getIdParent()) : null);
                obj.setCompanyCode(dao.getIdCompany() != null ? companyCodeService.find(dao.getIdCompany()) : null);
                obj.setIsPemeriksaParalel(dao.getIsPemeriksaParalel());
                this.edit(obj);
                getEntityManager().flush();
                /*end save surat*/

                /*save pemeriksa*/
                if (!dao.getPemeriksaSurat().isEmpty()) {
                    List<PemeriksaSurat> rmvPemeriksa = pemeriksaSuratService.getAllBy(dao.getId());
                    if (!rmvPemeriksa.isEmpty()) {
                        for (PemeriksaSurat ps : rmvPemeriksa) {
                            pemeriksaSuratService.remove(ps);
                        }
                    }
                    for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
                        PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
                        pemeriksaSurat.setOrganization(pemeriksa);
                        pemeriksaSurat.setSeq(pem.getSeq());
                        pemeriksaSurat.setSurat(obj);
                        pemeriksaSuratService.create(pemeriksaSurat);
                    }
                }

                if (tmp.stream().anyMatch(a -> a.getName().toLowerCase().contains("penandatangan"))) {
                    FieldDataDto objField = tmp.stream().filter(a -> a.getName().toLowerCase().contains("penandatangan")).findFirst().orElse(null);
                    if (objField != null) {
                        if (objField.getVMySelf()) {
                            List<PenandatanganSurat> rmvPenandatangan = penandatanganSuratService.getAllBy(dao.getId());
                            if (!rmvPenandatangan.isEmpty()) {
                                for (PenandatanganSurat ps : rmvPenandatangan) {
                                    penandatanganSuratService.remove(ps);
                                }
                            }

                            MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
                            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                            penandatanganSurat.setOrganization(org);
                            penandatanganSurat.setSeq(0);
                            penandatanganSurat.setSurat(obj);
                            penandatanganSuratService.create(penandatanganSurat);
                            isKonseptorTTD = true;
                        } else {
                            if (!dao.getPemeriksaSurat().isEmpty()) {
                                List<PemeriksaSurat> rmvPemeriksa = pemeriksaSuratService.getAllBy(dao.getId());
                                if (!rmvPemeriksa.isEmpty()) {
                                    for (PemeriksaSurat ps : rmvPemeriksa) {
                                        pemeriksaSuratService.remove(ps);
                                    }
                                }

                                for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
                                    MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
                                    PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
                                    pemeriksaSurat.setOrganization(pemeriksa);
                                    pemeriksaSurat.setSeq(pem.getSeq());
                                    pemeriksaSurat.setSurat(obj);
                                    pemeriksaSuratService.create(pemeriksaSurat);
                                }
                            }
                            /*end save pemeriksa*/

                            /*save penandatangan*/
                            if (!dao.getPenandatanganSurat().isEmpty()) {
                                List<PenandatanganSurat> rmvPenandatangan = penandatanganSuratService.getAllBy(dao.getId());
                                if (!rmvPenandatangan.isEmpty()) {
                                    for (PenandatanganSurat ps : rmvPenandatangan) {
                                        penandatanganSuratService.remove(ps);
                                    }
                                }

                                FieldDataDto objField2 = tmp.stream().filter(a -> a.getName().toLowerCase().contains("atas nama")).findFirst().orElse(null);
                                if (objField2 != null) {
                                    if (objField2.getVDirectChief() != null && objField2.getVDirectChief()) {
                                        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity().getParent();
                                        PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                        penandatanganSurat.setOrganization(org);
                                        penandatanganSurat.setSeq(0);
                                        penandatanganSurat.setSurat(obj);
                                        penandatanganSuratService.create(penandatanganSurat);
                                    } else {
                                        for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat().stream().distinct().collect(Collectors.toList())) {
                                            MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
                                            PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                            penandatanganSurat.setOrganization(penandatangan);
                                            penandatanganSurat.setSeq(ttd.getSeq());
                                            penandatanganSurat.setSurat(obj);

                                            if (ttd.getIdANOrg() != null) {
                                                MasterStrukturOrganisasi orgAN = masterStrukturOrganisasiService.find(ttd.getIdANOrg());
                                                penandatanganSurat.setOrgAN(orgAN);
                                            }

                                            //penandatanganSurat.setUser(penandatangan.getUser());
                                            penandatanganSuratService.create(penandatanganSurat);
                                        }
                                    }
                                } else {
                                    for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat().stream().distinct().collect(Collectors.toList())) {
                                        MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
                                        PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
                                        penandatanganSurat.setOrganization(penandatangan);
                                        penandatanganSurat.setSeq(ttd.getSeq());
                                        penandatanganSurat.setSurat(obj);

                                        //penandatanganSurat.setUser(penandatangan.getUser());
                                        penandatanganSuratService.create(penandatanganSurat);
                                    }
                                }
                            }
                        }
                    }
                }
                /*end save penandatangan*/

                /*save tembusan*/
                if (!dao.getTembusanSurat().isEmpty()) {
                    List<TembusanSurat> rmvTembusan = tembusanSuratService.getAllBy(dao.getId());
                    if (!rmvTembusan.isEmpty()) {
                        for (TembusanSurat bs : rmvTembusan) {
                            List<TembusanPara> tp = tembusanParaService.getListTembusanPara(bs.getId());
                            if (tp.isEmpty()) {
                                tembusanSuratService.remove(bs);
                            } else {
                                for (TembusanPara ctr : tp) {
                                    tembusanParaService.remove(ctr);
                                }
                                tembusanSuratService.remove(bs);
                            }
                        }
                    }
                    for (TembusanSuratDto tembus : dao.getTembusanSurat().stream().distinct().collect(Collectors.toList())) {
                        MasterStrukturOrganisasi tembusan = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        TembusanSurat tembusanSurat = new TembusanSurat();
                        if (tembus.getIdOrganization() != null) {
                            tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setOrganization(tembusan);
                            tembusanSuratService.create(tembusanSurat);
                        } else {
                            paraObj = paraService.find(tembus.getIdPara());
                            tembusanSurat.setSurat(obj);
                            tembusanSurat.setSeq(tembus.getSeq());
                            tembusanSurat.setPara(paraObj);
                            tembusanSuratService.create(tembusanSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    TembusanPara tp = new TembusanPara();
                                    tp.setOrganisasi(pdd.getOrganisasi());
                                    tp.setTembusanSurat(tembusanSurat);
                                    tembusanParaService.create(tp);
                                }
                            }
                        }
                    }
                }
                /*end save tembusan*/

                /*save penerima*/
                if (!dao.getPenerimaSurat().isEmpty()) {
                    List<PenerimaSurat> rmvPenerima = penerimaSuratService.getAllBy(dao.getId());
                    if (!rmvPenerima.isEmpty()) {
                        for (PenerimaSurat bs : rmvPenerima) {
                            List<PenerimaPara> tp = penerimaParaService.getPenerimaParaList(bs.getId());
                            if (tp.isEmpty()) {
                                penerimaSuratService.remove(bs);
                            } else {
                                for (PenerimaPara ctr : tp) {
                                    penerimaParaService.remove(ctr);
                                }
                                penerimaSuratService.remove(bs);
                            }
                        }
                    }
                    for (PenerimaSuratDto terima : dao.getPenerimaSurat().stream().distinct().collect(Collectors.toList())) {
                        PenerimaSurat penerimaSurat = new PenerimaSurat();
                        MasterStrukturOrganisasi penerima = new MasterStrukturOrganisasi();
                        Para paraObj = new Para();
                        if (terima.getIdOrganization() != null) {
                            penerima = masterStrukturOrganisasiService.find(terima.getIdOrganization());
                            penerimaSurat.setOrganization(penerima);
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSuratService.create(penerimaSurat);
                        } else {
                            paraObj = paraService.find(terima.getIdPara());
                            penerimaSurat.setSurat(obj);
                            penerimaSurat.setSeq(terima.getSeq());
                            penerimaSurat.setPara(paraObj);
                            penerimaSuratService.create(penerimaSurat);
                            getEntityManager().flush();

                            List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
                                    .collect(Collectors.toList());

                            if (!detail.isEmpty()) {
                                for (ParaDetail pdd : detail) {
                                    PenerimaPara pp = new PenerimaPara();
                                    pp.setOrganisasi(pdd.getOrganisasi());
                                    pp.setPenerimaSurat(penerimaSurat);
                                    penerimaParaService.create(pp);
                                }
                            }
                        }
                    }
                }
                /*end save penerima*/
                // < rollback
                GlobalAttachment draftDoc = globalAttachmentService.getDraftDocDataByRef("t_surat", dao.getId()).findFirst().orElse(null);

                docId = draftDoc.getDocId();

                TemplatingDto templatingDto = new TemplatingDto(dao.getIdFormDefinition(), dao.getMetadata(), docId);

                templatingService.directTemplating(templatingDto);
                // rollback >
            }

            getEntityManager().flush();
            getEntityManager().refresh(obj);

            if (dao.getReferensiId() != null && !dao.getReferensiId().isEmpty()) {
                List<ReferensiSuratDetail> refSurat = referensiSuratDetailService.getBySurat(obj.getId()).collect(Collectors.toList());
                if (!refSurat.isEmpty()) {
                    for (ReferensiSuratDetail rsd : refSurat) {
                        referensiSuratDetailService.remove(rsd);
                    }
                }

                for (Long a : dao.getReferensiId()) {
                    ReferensiSurat referensiSurat = referensiSuratService.find(a);
                    ReferensiSuratDetail referensiSuratDetail = new ReferensiSuratDetail();
                    referensiSuratDetail.setSurat(obj);
                    referensiSuratDetail.setReferensiSurat(referensiSurat);
                    referensiSuratDetailService.create(referensiSuratDetail);
                }
            }

            formDefinitionService.saveDynamicForm(dao, obj, isNew, this);

            result = new SuratDto(obj);
            result.setWorkflowAction(dao.getWorkflowAction());
            workflowService.execute(result, workflowStatus, obj, this);

            if (isKonseptorTTD) {
                result.setWorkflowAction("APPROVE");
                workflowStatus = result.getStatus();
                workflowService.execute(result, workflowStatus, obj, this);
            }

            return result;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public void executeApprovalSurat(SuratDto dao) throws Exception {
        Surat obj = new Surat();
        String workflowStatus;
        try {
            // <editor-fold defaultstate="collapsed" desc="Commented Looping Metadata Form">
            /*get formField*/
//            List<FieldDataDto> tmp = new ArrayList<>();
//            for (MetadataValueDto ff : dao.getMetadata()) {
//                Gson g = new Gson();
//                if(ff.getFieldData()!=null && ff.getFieldId()!=null) {
//                    FormField objF = fieldService.find(ff.getFieldId());
//                    objF.setData(ff.getFieldData());
//                    fieldService.edit(objF);
//                    String json = ff.getFieldData().replace("\\", "");
//                    FieldDataDto field = g.fromJson(json, FieldDataDto.class);
//                    field.setName(ff.getName());
//                    tmp.add(field);
//                }
//            }
            // </editor-fold>

            obj = this.find(dao.getId());

            // trigger: start workflow
            workflowStatus = obj.getStatus();

            // <editor-fold defaultstate="collapsed" desc="Commented Save Pemeriksa,Penandatangan, Tembusan dan Penerima">

            /*save pemeriksa*/
            // if (!dao.getPemeriksaSurat().isEmpty()) {
            //     List<PemeriksaSurat> rmvPemeriksa = pemeriksaSuratService.getAllBy(dao.getId());
            //     if (!rmvPemeriksa.isEmpty()) {
            //         for (PemeriksaSurat ps : rmvPemeriksa) {
            //             pemeriksaSuratService.remove(ps);
            //         }
            //     }
            //     for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
            //         MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
            //         PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
            //         pemeriksaSurat.setOrganization(pemeriksa);
            //         pemeriksaSurat.setSeq(pem.getSeq());
            //         pemeriksaSurat.setSurat(obj);
            //         pemeriksaSuratService.create(pemeriksaSurat);
            //     }
            // }
            // if (tmp.stream().anyMatch(a -> a.getName().toLowerCase().contains("penandatangan"))) {
            //     FieldDataDto objField = tmp.stream().filter(a -> a.getName().toLowerCase().contains("penandatangan")).findFirst().orElse(null);
            //     if (objField != null) {
            //         if (objField.getVMySelf()) {
            //             List<PenandatanganSurat> rmvPenandatangan = penandatanganSuratService.getAllBy(dao.getId());
            //             if (!rmvPenandatangan.isEmpty()) {
            //                 for (PenandatanganSurat ps : rmvPenandatangan) {
            //                     penandatanganSuratService.remove(ps);
            //                 }
            //             }
            //             MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
            //             PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
            //             penandatanganSurat.setOrganization(org);
            //             penandatanganSurat.setSeq(0);
            //             penandatanganSurat.setSurat(obj);
            //             penandatanganSuratService.create(penandatanganSurat);
            //         } else {
            //             if (!dao.getPemeriksaSurat().isEmpty()) {
            //                 List<PemeriksaSurat> rmvPemeriksa = pemeriksaSuratService.getAllBy(dao.getId());
            //                 if (!rmvPemeriksa.isEmpty()) {
            //                     for (PemeriksaSurat ps : rmvPemeriksa) {
            //                         pemeriksaSuratService.remove(ps);
            //                     }
            //                 }
            //                 for (PemeriksaSuratDto pem : dao.getPemeriksaSurat().stream().distinct().collect(Collectors.toList())) {
            //                     MasterStrukturOrganisasi pemeriksa = masterStrukturOrganisasiService.find(pem.getIdPemeriksa());
            //                     PemeriksaSurat pemeriksaSurat = new PemeriksaSurat();
            //                     pemeriksaSurat.setOrganization(pemeriksa);
            //                     pemeriksaSurat.setSeq(pem.getSeq());
            //                     pemeriksaSurat.setSurat(obj);
            //                     pemeriksaSuratService.create(pemeriksaSurat);
            //                 }
            //             }
            //             /*end save pemeriksa*/
            //             /*save penandatangan*/
            //             if (!dao.getPenandatanganSurat().isEmpty()) {
            //                 List<PenandatanganSurat> rmvPenandatangan = penandatanganSuratService.getAllBy(dao.getId());
            //                 if (!rmvPenandatangan.isEmpty()) {
            //                     for (PenandatanganSurat ps : rmvPenandatangan) {
            //                         penandatanganSuratService.remove(ps);
            //                     }
            //                 }
            //                 FieldDataDto objField2 = tmp.stream().filter(a -> a.getName().toLowerCase().contains("atas nama")).findFirst().orElse(null);
            //                 if (objField2 != null) {
            //                     if (objField2.getVDirectChief() != null && objField2.getVDirectChief()) {
            //                         MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity().getParent();
            //                         PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
            //                         penandatanganSurat.setOrganization(org);
            //                         penandatanganSurat.setSeq(0);
            //                         penandatanganSurat.setSurat(obj);
            //                         penandatanganSuratService.create(penandatanganSurat);
            //                     } else {
            //                         for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat().stream().distinct().collect(Collectors.toList())) {
            //                             MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
            //                             PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
            //                             penandatanganSurat.setOrganization(penandatangan);
            //                             penandatanganSurat.setSeq(ttd.getSeq());
            //                             penandatanganSurat.setSurat(obj);
            //                             if (ttd.getIdANOrg() != null) {
            //                                 MasterStrukturOrganisasi orgAN = masterStrukturOrganisasiService.find(ttd.getIdANOrg());
            //                                 penandatanganSurat.setOrgAN(orgAN);
            //                             }
            //                             //penandatanganSurat.setUser(penandatangan.getUser());
            //                             penandatanganSuratService.create(penandatanganSurat);
            //                         }
            //                     }
            //                 } else {
            //                     for (PenandatanganSuratDto ttd : dao.getPenandatanganSurat().stream().distinct().collect(Collectors.toList())) {
            //                         MasterStrukturOrganisasi penandatangan = masterStrukturOrganisasiService.find(ttd.getIdPenandatangan());
            //                         PenandatanganSurat penandatanganSurat = new PenandatanganSurat();
            //                         penandatanganSurat.setOrganization(penandatangan);
            //                         penandatanganSurat.setSeq(ttd.getSeq());
            //                         penandatanganSurat.setSurat(obj);
            //                         //penandatanganSurat.setUser(penandatangan.getUser());
            //                         penandatanganSuratService.create(penandatanganSurat);
            //                     }
            //                 }
            //             }
            //         }
            //     }
            // }
            /*end save penandatangan*/

            /*save tembusan*/
            // if (!dao.getTembusanSurat().isEmpty()) {
            //     List<TembusanSurat> rmvTembusan = tembusanSuratService.getAllBy(dao.getId());
            //     if (!rmvTembusan.isEmpty()) {
            //         for (TembusanSurat bs : rmvTembusan) {
            //             List<TembusanPara> tp = tembusanParaService.getListTembusanPara(bs.getId());
            //             if (tp.isEmpty()) {
            //                 tembusanSuratService.remove(bs);
            //             } else {
            //                 for (TembusanPara ctr : tp) {
            //                     tembusanParaService.remove(ctr);
            //                 }
            //                 tembusanSuratService.remove(bs);
            //             }
            //         }
            //     }
            //     for (TembusanSuratDto tembus : dao.getTembusanSurat().stream().distinct().collect(Collectors.toList())) {
            //         MasterStrukturOrganisasi tembusan = new MasterStrukturOrganisasi();
            //         Para paraObj = new Para();
            //         TembusanSurat tembusanSurat = new TembusanSurat();
            //         if (tembus.getIdOrganization() != null) {
            //             tembusan = masterStrukturOrganisasiService.find(tembus.getIdOrganization());
            //             tembusanSurat.setSurat(obj);
            //             tembusanSurat.setSeq(tembus.getSeq());
            //             tembusanSurat.setOrganization(tembusan);
            //             tembusanSuratService.create(tembusanSurat);
            //         } else {
            //             paraObj = paraService.find(tembus.getIdPara());
            //             tembusanSurat.setSurat(obj);
            //             tembusanSurat.setSeq(tembus.getSeq());
            //             tembusanSurat.setPara(paraObj);
            //             tembusanSuratService.create(tembusanSurat);
            //             getEntityManager().flush();
            //             List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
            //                     .collect(Collectors.toList());
            //             if (!detail.isEmpty()) {
            //                 for (ParaDetail pdd : detail) {
            //                     TembusanPara tp = new TembusanPara();
            //                     tp.setOrganisasi(pdd.getOrganisasi());
            //                     tp.setTembusanSurat(tembusanSurat);
            //                     tembusanParaService.create(tp);
            //                 }
            //             }
            //         }
            //     }
            // }
            /*end save tembusan*/

            /*save penerima*/
            // if (!dao.getPenerimaSurat().isEmpty()) {
            //     List<PenerimaSurat> rmvPenerima = penerimaSuratService.getAllBy(dao.getId());
            //     if (!rmvPenerima.isEmpty()) {
            //         for (PenerimaSurat bs : rmvPenerima) {
            //             List<PenerimaPara> tp = penerimaParaService.getPenerimaParaList(bs.getId());
            //             if (tp.isEmpty()) {
            //                 penerimaSuratService.remove(bs);
            //             } else {
            //                 for (PenerimaPara ctr : tp) {
            //                     penerimaParaService.remove(ctr);
            //                 }
            //                 penerimaSuratService.remove(bs);
            //             }
            //         }
            //     }
            //     for (PenerimaSuratDto terima : dao.getPenerimaSurat().stream().distinct().collect(Collectors.toList())) {
            //         PenerimaSurat penerimaSurat = new PenerimaSurat();
            //         MasterStrukturOrganisasi penerima = new MasterStrukturOrganisasi();
            //         Para paraObj = new Para();
            //         if (terima.getIdOrganization() != null) {
            //             penerima = masterStrukturOrganisasiService.find(terima.getIdOrganization());
            //             penerimaSurat.setOrganization(penerima);
            //             penerimaSurat.setSurat(obj);
            //             penerimaSurat.setSeq(terima.getSeq());
            //             penerimaSuratService.create(penerimaSurat);
            //         } else {
            //             paraObj = paraService.find(terima.getIdPara());
            //             penerimaSurat.setSurat(obj);
            //             penerimaSurat.setSeq(terima.getSeq());
            //             penerimaSurat.setPara(paraObj);
            //             penerimaSuratService.create(penerimaSurat);
            //             getEntityManager().flush();
            //             List<ParaDetail> detail = paraDetailService.getByIdPara(paraObj.getIdPara())
            //                     .collect(Collectors.toList());
            //             if (!detail.isEmpty()) {
            //                 for (ParaDetail pdd : detail) {
            //                     PenerimaPara pp = new PenerimaPara();
            //                     pp.setOrganisasi(pdd.getOrganisasi());
            //                     pp.setPenerimaSurat(penerimaSurat);
            //                     penerimaParaService.create(pp);
            //                 }
            //             }
            //         }
            //     }
            // }
            /*end save penerima*/
            // </editor-fold>
            getEntityManager().flush();
            getEntityManager().refresh(obj);

            formDefinitionService.saveDynamicForm(dao, obj, false, this);
            workflowService.execute(dao, workflowStatus, obj, this);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public Surat getByIdSurat(Long idSurat) {
        return db().where(c -> !c.getIsDeleted() && c.getId() == idSurat).findFirst().orElse(null);
    }

    public Surat getByIdSurat(List<Long> idSurat) {
        return db().where(c -> !c.getIsDeleted() && idSurat.contains(c.getId())).findFirst().orElse(null);
    }

    public HashMap<String, Object> getDraftFilter(String filter, String sort, Boolean descending, int skip, int limit) {
        List<DokumenDto> result = new ArrayList<>();
        TypedQuery<Surat> all = getDataByQuery("DRAFT", null, true, null, null, filter, sort, descending, skip, limit);
        result = MappingToDto(all);
        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    public List<Surat> getByParent(Long idSurat, String filter, String sort, boolean descending, int skip, int limit) {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        Surat surat = this.find(idSurat);

        List<Surat> suratFilter = this.getResultList(filter, sort, descending, skip, limit);
        List<Surat> suratFilter2 = db().where(c -> !c.getIsDeleted() && c.getParent().equals(surat)).toList();
        suratFilter.retainAll(suratFilter2);
        return suratFilter;
    }

    public JPAJinqStream<Surat> getMetadataSurat(Long idSurat) {
        Surat surat = this.find(idSurat);
        return db().where(a -> !a.getIsDeleted() && a.equals(surat));
    }

    public HashMap<String, Object> getDokumenKeluar(int skip, int limit) {
        List<DokumenKeluarDto> result = new ArrayList<>();
        TypedQuery<Surat> all = getDataByQuery("APPROVED", null, true, null, null, null, null, null, skip, limit);
        result = MappingDokumenKeluar(all);
        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    public JPAJinqStream<Surat> getDokumenBatal() {
        MasterStrukturOrganisasi org = userSession.getUserSession().getUser().getOrganizationEntity();
        return db().where(a
                -> !a.getIsDeleted()
                && a.getStatus().toUpperCase().equals("BATAL")
                && a.getKonseptor().equals(org)
        );
    }

    public List<String> getDraftLinkPreview(Long idSurat) throws CMISProviderException {
        List<String> links = new LinkedList<>();

        List<GlobalAttachment> globalAttachments = globalAttachmentService.getDraftDocDataByRef("t_surat", idSurat).toList();

        if (globalAttachments == null || globalAttachments.isEmpty()) {
            return links;
        }

        for (GlobalAttachment globalAttachment : globalAttachments) {
            String link = fileService.getViewLink(globalAttachment.getDocId(), true);

            links.add(link);
        }

        return links;
    }

    public List<String> getDocumentLinkPreview(Long idSurat) throws CMISProviderException {
        List<String> links = new LinkedList<>();

        List<GlobalAttachment> globalAttachments = globalAttachmentService.getDocDataByRef("t_surat", idSurat).toList();

        if (globalAttachments == null || globalAttachments.isEmpty()) {
            return links;
        }

        for (GlobalAttachment globalAttachment : globalAttachments) {
            String link = fileService.getViewLink(globalAttachment.getDocId(), false);

            links.add(link);
        }

        return links;
    }

    public List<AttachmentPreviewDto> getAttachmentLinkPreview(Long idSurat) throws CMISProviderException {
        List<AttachmentPreviewDto> attachmentsLinks = new LinkedList<>();

        List<GlobalAttachment> globalAttachments = globalAttachmentService.getAttachmentDataByRef("t_surat", idSurat).toList();

        if (globalAttachments == null || globalAttachments.isEmpty()) {
            return attachmentsLinks;
        }

        for (GlobalAttachment globalAttachment : globalAttachments) {
            attachmentsLinks.add(globalAttachmentService.getPreviewDto(globalAttachment));
        }

        return attachmentsLinks;
    }

    public TypedQuery<Surat> getDataByQuery(String status, Long idOrg, Boolean cond1, Boolean cond2, Boolean cond3,
                                            String filter, String sort, Boolean desc, int skip, int limit) {
        Long org;
        if (idOrg != null) {
            org = masterStrukturOrganisasiService.find(idOrg).getIdOrganization();
        } else {
            org = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();
        }

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Surat> q = cb.createQuery(Surat.class);
        Root<Surat> root = q.from(Surat.class);

        q.select(root);
        List<Predicate> predicates = new ArrayList<>();

        // Join Entity surat to Another
        Join<Surat, PenandatanganSurat> penandatangan = root.join("penandatanganSurat", JoinType.LEFT);
        Join<Surat, PemeriksaSurat> pemeriksa = root.join("pemeriksaSurat", JoinType.LEFT);
        Join<Surat, PenerimaSurat> penerima = root.join("penerimaSurat", JoinType.LEFT);
        Join<Surat, TembusanSurat> tembusan = root.join("tembusanSurat", JoinType.LEFT);

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted")),
                cb.equal(root.get("status"), status)
        );

        if (cond1 != null && cond1.booleanValue() == true) {
            Predicate pCond1 = cb.or(
                    cb.equal(pemeriksa.get("organization").get("idOrganization"), org),
                    cb.equal(penandatangan.get("organization").get("idOrganization"), org),
                    cb.equal(root.get("konseptor").get("idOrganization"), org)
            );
            predicates.add(pCond1);
        }

        if (cond2 != null && cond2.booleanValue() == true) {
            Predicate pCond2 = cb.or(
                    cb.equal(penerima.get("organization").get("idOrganization"), org),
                    cb.equal(tembusan.get("organization").get("idOrganization"), org)
            );

            Predicate pNotNull = cb.or(
                    cb.isNotNull(root.get("noDokumen"))
            );
            predicates.add(pCond2);
            predicates.add(pNotNull);
        }

        if (cond3 != null && cond3.booleanValue() == true) {
            Predicate pCond3 = cb.or(
                    cb.equal(pemeriksa.get("organization").get("idOrganization"), org),
                    cb.equal(penandatangan.get("organization").get("idOrganization"), org),
                    cb.equal(root.get("konseptor").get("idOrganization"), org)
            );

            Predicate pNotNull = cb.or(
                    cb.isNotNull(root.get("noDokumen"))
            );
            predicates.add(pCond3);
            predicates.add(pNotNull);
        }

        predicates.add(mainCond);

        if (desc != null && !Strings.isNullOrEmpty(sort)) {
            if (desc) {
                q.orderBy(cb.desc(root.get(sort)));
            } else {
                q.orderBy(cb.asc(root.get(sort)));
            }
        }

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);

        if (!Strings.isNullOrEmpty(filter)) {
            q = getCriteriaQuery(filter, null, false, (query, r) -> {
                query.select(r).distinct(true);
            });
        }
        return getEntityManager().createQuery(q).setFirstResult(skip).setMaxResults(limit);
    }

    private List<DokumenDto> MappingToDto(TypedQuery<Surat> all) {
        List<DokumenDto> result = null;

        if (all != null) {
            result = all.getResultStream()
                    .map(q -> {
                        DokumenDto obj = new DokumenDto();
                        DateUtil dateUtil = new DateUtil();
                        obj.setId(q.getId());
                        obj.setTanggal(dateUtil.getCurrentISODate(q.getCreatedDate()));
                        FormValue fv = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                                .findFirst().orElse(null);
                        if (fv != null) {
                            obj.setPerihal(fv.getValue());
                        }
                        MasterJenisDokumen jenisDokumen = q.getFormDefinition().getJenisDokumen();
                        obj.setJenisDokumen(jenisDokumen.getKodeJenisDokumen() + " " + jenisDokumen.getNamaJenisDokumen());
                        obj.setKorespondensi(jenisDokumen.getIsKorespondensi());
                        if (obj.isKorespondensi()) {
                            List<PengirimDto> pObj = q.getPenandatanganSurat().stream().map(b -> {
                                PengirimDto pengirim = new PengirimDto();
                                pengirim.setJabatan(b.getOrganization().getOrganizationName());
                                pengirim.setNama(b.getOrganization().getUser().getNameFront() + " " + b.getOrganization().getUser().getNameMiddleLast());
                                pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                                return pengirim;
                            }).collect(Collectors.toList());
                            obj.setPengirim(pObj);
                        } else {
                            List<PengirimDto> pObj = new ArrayList<>();
                            FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("dari"))
                                    .findFirst().orElse(null);
                            if (fv2 != null) {
                                MasterVendor vendor = vendorService.find(Long.parseLong(fv2.getValue()));
                                PengirimDto po = new PengirimDto();
                                po.setJabatan(vendor.getNama());
                                pObj.add(po);
                            }
                            obj.setPengirim(pObj);
                        }
                        ProcessTask processtask = processTaskService.getByRecordId(q.getId());
                        if (processtask != null) {
                            obj.setAksi(processtask.getTaskName());
                        }
                        return obj;
                    }).collect(Collectors.toList());
        }
        return result;
    }

    private List<SuratDto> MappingSuratDto(TypedQuery<Surat> all) {
        List<SuratDto> result = null;

        if (all != null) {
            result = all.getResultStream()
                    .sorted(Comparator.comparing(Surat::getSubmittedDate).reversed())
                    .map(SuratDto::new).collect(Collectors.toList());
        }
        return result;
    }

    private List<DokumenMasukDto> MappingDokumenMasuk(TypedQuery<Surat> all, String tipe) {
        List<DokumenMasukDto> result = null;
        MasterStrukturOrganisasi current = userSession.getUserSession().getUser().getOrganizationEntity();
        List<ProcessTask> processTasks = processTaskService.getExecutedTaskByOrgId(current.getIdOrganization()).toList();
        if (all != null) {
            result = all.getResultStream()
                    .sorted(Comparator.comparing(Surat::getSubmittedDate).reversed())
                    .map(q -> {
                        DokumenMasukDto obj = new DokumenMasukDto();
                        DateUtil dateUtil = new DateUtil();

                        ProcessTask task = new ProcessTask();
                        if (!processTasks.isEmpty()) {
                            task = processTasks.get(processTasks.size() - 1);
                        } else {
                            task = null;
                        }

                        obj.setId(q.getId());
                        MasterJenisDokumen jenisDokumen = q.getFormDefinition().getJenisDokumen();
                        obj.setJenisDokumen(jenisDokumen.getKodeJenisDokumen() + " " + jenisDokumen.getNamaJenisDokumen());
                        obj.setKorespondensi(jenisDokumen.getIsKorespondensi());
                        if (obj.isKorespondensi()) {
                            List<PengirimDto> pObj = q.getPenandatanganSurat().stream().map(b -> {
                                PengirimDto pengirim = new PengirimDto();
                                pengirim.setJabatan(b.getOrganization().getOrganizationName());
                                pengirim.setNama(b.getOrganization().getUser().getNameFront() + " " + b.getOrganization().getUser().getNameMiddleLast());
                                pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                                return pengirim;
                            }).collect(Collectors.toList());
                            obj.setPengirim(pObj);
                        } else {
                            List<PengirimDto> pObj = new ArrayList<>();
                            FormValue form = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("dari"))
                                    .findFirst().orElse(null);
                            if (form != null) {
                                MasterVendor vendor = vendorService.find(Long.parseLong(form.getValue()));
                                PengirimDto po = new PengirimDto();
                                po.setJabatan(vendor.getNama());
                                pObj.add(po);
                            }
                            obj.setPengirim(pObj);
                        }
                        obj.setNoSurat(q.getNoDokumen());
                        obj.setTanggalDikirim(dateUtil.getCurrentISODate(q.getSubmittedDate()));
                        //TODO
                        //seharusnya ambil dari tabel process_task (asisgned_date)
                        if (task != null) {
                            obj.setTanggalDiterima(dateUtil.getCurrentISODate(task.getAssignedDate()));
                        }
                        if (tipe.isEmpty() || tipe.equals("")) {
                            obj.setDelegasiTipe("");
                            obj.setDelegasiUser("");
                            obj.setDelegasiOrganisasi("");
                        } else if (tipe.toLowerCase().equals("tim") || tipe.toLowerCase().equals("team")) {
                            MasterNonStruktural nonStruktural = nonStrukturalService.getByUserLogin();
                            MasterStrukturOrganisasi org = nonStruktural.getOrganisasi();
                            obj.setDelegasiTipe("TIM");
                            obj.setDelegasiUser(org.getUser().getNameFront() + " " + org.getUser().getNameMiddleLast());
                            obj.setDelegasiOrganisasi(org.getOrganizationName());
                        } else {
                            MasterDelegasi delegasi = delegasiService.getByTo(tipe);
                            obj.setDelegasiTipe(delegasi.getTipe());
                            obj.setDelegasiUser(delegasi.getFrom().getUser().getNameFront() + " " + delegasi.getFrom().getUser().getNameMiddleLast());
                            obj.setDelegasiOrganisasi(delegasi.getFrom().getOrganizationName());
                        }

                        SuratInteraction interaction = Optional.ofNullable(interactionService.getByUser(q.getId())).orElse(null);
                        if (interaction != null) {
                            obj.setIsRead(interaction.getIsRead());
                            obj.setIsImportant(interaction.getIsImportant());
                            obj.setIsStarred(interaction.getIsStarred());
                            obj.setIsFinished(interaction.getIsFinished());
                        }
                        List<SuratDisposisiDto> disposisi = disposisiService.findByIdSurat(q.getId()).stream().map(SuratDisposisiDto::new).collect(Collectors.toList());
                        if (!disposisi.isEmpty()) {
                            obj.setDisposisiStatus(true);
                            obj.setSuratDisposisi(disposisi);
                        }

                        FormValue fv1 = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                                .findFirst().orElse(null);
                        if (fv1 != null) {
                            obj.setPerihal(fv1.getValue());
                        }

                        FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("sifat naskah"))
                                .findFirst().orElse(null);
                        if (fv2 != null) {
                            obj.setSifatNaskah(fv2.getValue());
                        }

                        FormValue fv3 = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("klasifikasi masalah"))
                                .findFirst().orElse(null);
                        if (fv3 != null) {
                            obj.setKlasifikasiDokumen(fv3.getValue());
                        }

                        return obj;
                    }).collect(Collectors.toList());
        }
        return result;
    }

    private List<DokumenKeluarDto> MappingDokumenKeluar(TypedQuery<Surat> all) {
        List<DokumenKeluarDto> result = null;

        if (all != null) {
            result = all.getResultStream()
                    .sorted(Comparator.comparing(Surat::getSubmittedDate))
                    .map(q -> {
                        DokumenKeluarDto obj = new DokumenKeluarDto();
                        DateUtil dateUtil = new DateUtil();
                        MasterJenisDokumen jenisDokumen = q.getFormDefinition().getJenisDokumen();
                        obj.setJenisDokumen(jenisDokumen.getKodeJenisDokumen() + " " + jenisDokumen.getNamaJenisDokumen());
                        obj.setKorespondensi(jenisDokumen.getIsKorespondensi());
                        if (obj.isKorespondensi()) {
                            List<PengirimDto> pObj = q.getPenandatanganSurat().stream().map(b -> {
                                PengirimDto pengirim = new PengirimDto();
                                pengirim.setJabatan(b.getOrganization().getOrganizationName());
                                pengirim.setNama(b.getOrganization().getUser().getNameFront() + " " + b.getOrganization().getUser().getNameMiddleLast());
                                pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                                return pengirim;
                            }).collect(Collectors.toList());
                            obj.setPengirim(pObj);
                        } else {
                            List<PengirimDto> pObj = new ArrayList<>();
                            FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("dari"))
                                    .findFirst().orElse(null);
                            if (fv2 != null) {
                                MasterVendor vendor = vendorService.find(Long.parseLong(fv2.getValue()));
                                PengirimDto po = new PengirimDto();
                                po.setJabatan(vendor.getNama());
                                pObj.add(po);
                            }
                            obj.setPengirim(pObj);
                        }
                        obj.setId(q.getId());
                        obj.setNoSurat(q.getNoDokumen());
                        FormValue fv = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                                .findFirst().orElse(null);
                        if (fv != null) {
                            obj.setPerihal(fv.getValue());
                        } else {
                            obj.setPerihal(null);
                        }
                        if (q.getSubmittedDate() != null) {
                            obj.setTanggal(dateUtil.getCurrentISODate(q.getSubmittedDate()));
                        } else {
                            obj.setTanggal("2003-10-03T10:59:36.000+07");
                        }
                        return obj;
                    }).sorted(Comparator.comparing(DokumenKeluarDto::getTanggal).reversed()).collect(Collectors.toList());
        }
        return result;
    }

    public Surat getByNoDokumen(String NoDokumen) {
        return db().where(c -> !c.getIsDeleted() && c.getNoDokumen().equals(NoDokumen)).findFirst().orElse(null);
    }

    public List<DokumenReferensiDto> getRef(Long id) {
        Surat surat = this.find(id);
        JPAJinqStream<ReferensiSuratDetail> list = referensiSuratDetailService.getBySurat(surat.getId());

        return list
                .sorted(Comparator
                        .comparing(ReferensiSuratDetail::getId))
                .map(t -> {
                    GlobalAttachment globalAttachment = globalAttachmentService.getAllDataByRef("t_surat", t.getReferensiSurat().getSurat().getId()).sortedDescendingBy(GlobalAttachment::getId).limit(1).findFirst().orElse(null);

                    DokumenReferensiDto referensiSuratDto = new DokumenReferensiDto(t);

                    if (globalAttachment != null) {
                        referensiSuratDto.setDownloadLink(fileService.getViewLink(globalAttachment.getDocId(), false));
                        referensiSuratDto.setNamaFile(globalAttachment.getDocName());
                    }

                    return referensiSuratDto;
                })
                .collect(Collectors.toList());
    }

    public Integer getCountSuratBelumDiarsipkan() {
        Long org = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();

        // Exec Query
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Surat> q = cb.createQuery(Surat.class);
        Root<Surat> root = q.from(Surat.class);

        Join<Surat, PenerimaSurat> penerima = root.join("penerimaSurat", JoinType.LEFT);

        q.select(root);

        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted")),
                cb.equal(root.get("status"), "APPROVED")
        );

        /*check Kondisi Pemeriksa*/
        Predicate orgCond = cb.or(
                cb.equal(penerima.get("organization").get("idOrganization"), org)
        );

        Predicate pNotNull = cb.or(
                cb.isNotNull(root.get("noDokumen")),
                cb.isNull(root.get("idArsip"))
        );

        // WHERE CONDITION
        predicates.add(mainCond);
        predicates.add(orgCond);
        predicates.add(pNotNull);

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);

        return getEntityManager().createQuery(q).getResultList().size();
    }

    public Surat arsipkan(Long id, Long idArsip) {
        Surat surat = this.find(id);
        Arsip arsip = arsipService.find(idArsip);
        surat.setFolderArsip(arsip.getDeskripsi());
        surat.setIdArsip(arsip.getId());
        this.edit(surat);

        arsipkanAttachmentSurat(id, idArsip);

        return surat;
    }

    private void arsipkanAttachmentSurat(Long idSurat, Long idArsip) {
        List<GlobalAttachment> suratAttachments = new LinkedList<>();

        suratAttachments.addAll(globalAttachmentService.getAllDataByRef("t_surat", idSurat).toList());
        JPAJinqStream<GlobalAttachment> arsipAttachments = globalAttachmentService.getAllDataByRef("t_arsip", idArsip);

        long countArsip = arsipAttachments.count();

        for (GlobalAttachment attachment : suratAttachments) {
            if (countArsip > 0) {
                String docId = attachment.getDocId();
                if (arsipAttachments.where(t -> t.getDocId().equals(docId)).findFirst() != null) {
                    continue;
                }
            }

            GlobalAttachment arsipAttachment = new GlobalAttachment();

            arsipAttachment.setVersion(attachment.getVersion());
            arsipAttachment.setModifiedDate(attachment.getModifiedDate());
            arsipAttachment.setModifiedBy(attachment.getModifiedBy());
            arsipAttachment.setCreatedDate(attachment.getCreatedDate());
            arsipAttachment.setCreatedBy(attachment.getCreatedBy());
            arsipAttachment.setSize(attachment.getSize());
            arsipAttachment.setNumber(attachment.getNumber());
            arsipAttachment.setMimeType(attachment.getMimeType());
            arsipAttachment.setCompanyCode(attachment.getCompanyCode());
            arsipAttachment.setDocId(attachment.getDocId());
            arsipAttachment.setIsPublished(attachment.getIsPublished());
            arsipAttachment.setIsKonsep(attachment.getIsKonsep());
            arsipAttachment.setDocGeneratedName(attachment.getDocGeneratedName());
            arsipAttachment.setDocName(attachment.getDocName());
            arsipAttachment.setIsDeleted(attachment.getIsDeleted());
            arsipAttachment.setExtractedText(attachment.getExtractedText());
            arsipAttachment.setReferenceTable("t_arsip");
            arsipAttachment.setReferenceId(idArsip);

            globalAttachmentService.create(arsipAttachment);
        }
    }

    public String generatePublishDoc(Long idSurat) throws Exception {
        GlobalAttachment draftDoc = globalAttachmentService.getDraftDocDataByRef("t_surat", idSurat).findFirst().orElse(null);
        if (draftDoc == null) {
            return "";
        }

        String draftDocId = draftDoc.getDocId();

        Surat surat = find(idSurat);

//        String noDokumen = surat.getNoDokumen();
//        String tempatSurat, tanggalSurat;
        PenandatanganSurat penandatanganSurat = surat.getPenandatanganSurat().stream().findFirst().orElse(null);

//        if(penandatanganSurat == null) tempatSurat = "  ";
//        else {
//            tempatSurat = penandatanganSurat.getOrganization().getArea().getKota().getNamaKota();
//        }
//
//        if(surat.getApprovedDate() == null) tanggalSurat = "  ";
//        else {
//            Locale locale = new Locale("in", "ID");
//
//            tanggalSurat = new SimpleDateFormat("dd MMMMMMMMMM yyyy", locale).format(surat.getApprovedDate());
//        }
        byte[] konsepContent = fileService.download(draftDocId);

        byte[] retemplated = templatingService.dbBasedTemplating(konsepContent, surat);

//        templatingService.directTemplating(
//                new TemplatingDto(
//                        surat.getFormDefinition().getId(),
//                        Arrays.asList(
//                                new MetadataValueDto[]{
//                                        new MetadataValueDto(
//                                                TemplatingHelper.NO_DOKUMEN_KEY,
//                                                surat.getNoDokumen()==null? " " : surat.getNoDokumen(),
//                                                surat.getNoDokumen()==null? " " : surat.getNoDokumen()),
//                                        new MetadataValueDto(
//                                                TemplatingHelper.TEMPAT_KEY,
//                                                tempatSurat,
//                                                tempatSurat),
//                                        new MetadataValueDto(
//                                                TemplatingHelper.TANGGAL_KEY,
//                                                tanggalSurat,
//                                                tanggalSurat)
//                                }),
//
//                        draftDocId));
        icmisProvider.openConnection();

        fileService.upload(icmisProvider, draftDocId, retemplated);

        byte[] pdfBytes = konversiOnlyOfficeService.convert(retemplated, ".docx", ".pdf");

        byte[] qrCode = qrGenerateService.getQRCode(StringUtils.isEmpty(surat.getNoDokumen()) ? "Nomor Dokumen" : surat.getNoDokumen(), 80);

        byte[] pdfModed = qrGenerateService.embedQRCodeToPdf(pdfBytes, qrCode);

        String tempFileName = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID().toString() + ".pdf";
        FileOutputStream fos = new FileOutputStream(tempFileName);
        fos.write(pdfBytes);
        fos.close();
        File tempFile = new File(tempFileName);

        List<String> extractedText = textExtractionService.extractText(tempFile);
        String jsonRepresentation = textExtractionService.serializeExtractedText(extractedText);

        String pdfDocId = fileService.upload("publish.pdf", pdfModed);
        pdfDocId = pdfDocId.replace("workspace://SpacesStore/", "");

        GlobalAttachment obj = new GlobalAttachment();
        obj.setMimeType("application/pdf");
        obj.setDocGeneratedName(UUID.randomUUID().toString());
        obj.setDocName("publish.pdf");
        obj.setCompanyCode(surat.getCompanyCode());
        obj.setDocId(pdfDocId);
        obj.setIsKonsep(true);
        obj.setIsPublished(true);
        obj.setIsDeleted(false);
        obj.setSize(new Long(pdfBytes.length));
        obj.setReferenceTable("t_surat");
        obj.setReferenceId(idSurat);
        obj.setNumber("test 123");
        obj.setExtractedText(jsonRepresentation);

        globalAttachmentService.create(obj);

        Files.deleteIfExists(tempFile.toPath());

        return pdfDocId;
    }

    public List<PemeriksaSurat> getNeedApprovalPemeriksaList(Long idSurat) {
        Surat surat = this.find(idSurat);
        return getPemeriksaListBy(surat, 1);
    }

    public List<PemeriksaSurat> getNeedApprovalPemeriksaList(Long idSurat, Integer sequence) {
        Surat surat = this.find(idSurat);
        return getPemeriksaListBy(surat, sequence);
    }

    public List<PemeriksaSurat> getPemeriksaListBy(Surat surat, Integer sequence) {
        List<PemeriksaSurat> pemeriksaList = surat.getPemeriksaSurat()
                .stream()
                .filter(q -> q.getSeq() >= sequence)
                .sorted(Comparator.comparing(PemeriksaSurat::getSeq)).collect(Collectors.toList());
        return pemeriksaList;
    }

    public HashMap<String, Object> getRDocMasuk(String start, String end, Long idJenisDokumen, String idArea, String idUnit, Long idKArsip,
            String sort, boolean descending, int skip, int limit) {
        List<ReportDokumenMasukDto> result = new ArrayList<>();
//        TypedQuery<Surat> all = getReportData("APPROVED", null, true, start, end, idJenisDokumen, idArea, idUnit, idKArsip, sort, descending, skip, limit);
        List<String> status = Arrays.asList(new String[]{EntityStatus.APPROVED.toString(), EntityStatus.CLOSED.toString()});
        TypedQuery<Surat> all = getReportData(status, null, true, start, end, idJenisDokumen, idArea, idUnit, idKArsip, sort, descending);
        result = RDokMasukDto(all, skip, limit);
        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    private List<ReportDokumenMasukDto> RDokMasukDto(TypedQuery<Surat> all, Integer skip, Integer limit) {
        List<ReportDokumenMasukDto> result = null;

        if (all != null) {
            result = all.getResultStream().map(q -> {
                ReportDokumenMasukDto obj = new ReportDokumenMasukDto();
                DateUtil dateUtil = new DateUtil();
                obj.setId(q.getId());
                obj.setNoSurat(q.getNoDokumen());
                List<PengirimDto> pObj = q.getPenandatanganSurat().stream().map(b -> {
                    PengirimDto pengirim = new PengirimDto();
                    pengirim.setJabatan(b.getOrganization().getOrganizationName());
                    pengirim.setNama(b.getOrganization().getUser().getNameFront() + " " + b.getOrganization().getUser().getNameMiddleLast());
                    pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                    return pengirim;
                }).collect(Collectors.toList());
                obj.setPengirim(pObj);
                FormValue fv = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                        .findFirst().orElse(null);
                if (fv != null) {
                    obj.setPerihal(fv.getValue());
                } else {
                    obj.setPerihal(null);
                }
                if (q.getApprovedDate() != null) {
                    obj.setTanggal(dateUtil.getCurrentISODate(q.getApprovedDate()));
                }
                if (q.getKonseptor().getUser().getArea() != null) {
                    MasterArea area = q.getKonseptor().getUser().getArea();
                    obj.setArea(area.getNama());
                } else {
                    obj.setArea(null);
                }
                if (q.getKonseptor().getUser() != null) {
                    if (q.getKonseptor().getUser().getKorsa() != null) {
                        MasterKorsa korsa = q.getKonseptor().getUser().getKorsa();
                        obj.setUnit(korsa.getNama());
                    }
                }
//                FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getTableSourceName() != null
//                        && b.getFormField().getTableSourceName().equals("MasterKlasifikasiMasalah")).findFirst().orElse(null);
//                if (fv2 != null) {
//                    if (!Strings.isNullOrEmpty(fv2.getValue())) {
//                        MasterKlasifikasiMasalah klasifikasiMasalah = masterKlasifikasiMasalahService.find(Long.parseLong(fv2.getValue()));
//                        obj.setKlasifikasiArsip(klasifikasiMasalah.getKodeKlasifikasiMasalah() + " " + klasifikasiMasalah.getNamaKlasifikasiMasalah());
//                    }
//                }
                FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getType().toLowerCase().contains("master-klasifikasi-masalah"))
                        .findFirst().orElse(null);
                if (fv2 != null) {
                    obj.setKlasifikasiArsip(fv2.getValue());
                }
                obj.setJenisDokumen(q.getFormDefinition().getJenisDokumen().getKodeJenisDokumen() + " " + q.getFormDefinition().getJenisDokumen().getNamaJenisDokumen());
                return obj;
            })
                    // .sorted(Comparator.comparing(ReportDokumenMasukDto::getTanggal).reversed())
                    .skip(skip).limit(limit)
                    .collect(Collectors.toList());
        }
        return result;
    }

    public HashMap<String, Object> getRDocKeluar(String start, String end, Long idJenisDokumen, String idArea, String idUnit, Long idKArsip,
            String sort, boolean descending, int skip, int limit) {
        List<ReportDocKeluarDto> result = new ArrayList<>();
        List<String> status = Arrays.asList(new String[]{EntityStatus.APPROVED.toString(), EntityStatus.CLOSED.toString()});
        TypedQuery<Surat> all = getReportData(status, true, null, start, end, idJenisDokumen, idArea, idUnit, idKArsip, sort, descending);
        result = RDokKeluarDto(all, skip, limit);
        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    private List<ReportDocKeluarDto> RDokKeluarDto(TypedQuery<Surat> all, Integer skip, Integer limit) {
        List<ReportDocKeluarDto> result = null;

        if (all != null) {
            result = all.getResultStream().map(q -> {
                ReportDocKeluarDto obj = new ReportDocKeluarDto();
                DateUtil dateUtil = new DateUtil();
                obj.setId(q.getId());
                obj.setNoSurat(q.getNoDokumen());
                List<PengirimDto> pObj = q.getPenandatanganSurat().stream().map(b -> {
                    PengirimDto pengirim = new PengirimDto();
                    if(b.getOrganization()!=null) {
                        pengirim.setJabatan(b.getOrganization().getOrganizationName());
                        pengirim.setNama(b.getOrganization().getUser().getNameFront() + " " + b.getOrganization().getUser().getNameMiddleLast());
                        pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                    }
                    return pengirim;
                }).collect(Collectors.toList());
                obj.setPengirim(pObj);

                // added by Agie set Penerima
                List<PenerimaSuratDto> penerimaObj = q.getPenerimaSurat().stream().map(t -> {
                    PenerimaSuratDto dto = new PenerimaSuratDto();
                    if(t.getOrganization()!=null) {
                        dto.setOrganization(new MasterStrukturOrganisasiDto(t.getOrganization()));
                    }else{
                        if(t.getVendor()!=null){
                            dto.setVendor(new MasterVendorDto(t.getVendor()));
                        }
                    }
                    return dto;
                }).collect(Collectors.toList());
                obj.setPenerima(penerimaObj);
                // end added

                FormValue fv = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                        .findFirst().orElse(null);
                if (fv != null) {
                    obj.setPerihal(fv.getValue());
                } else {
                    obj.setPerihal(null);
                }
                if (q.getSubmittedDate() != null) {
                    obj.setTanggal(dateUtil.getCurrentISODate(q.getSubmittedDate()));
                }
                if (q.getKonseptor().getUser().getArea() != null) {
                    MasterArea area = q.getKonseptor().getUser().getArea();
                    obj.setArea(area.getNama());
                } else {
                    obj.setArea(null);
                }
                if (q.getKonseptor().getUser() != null) {
                    if (q.getKonseptor().getUser().getKorsa() != null) {
                        MasterKorsa korsa = q.getKonseptor().getUser().getKorsa();
                        obj.setUnit(korsa.getNama());
                    }
                }
//                FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getTableSourceName() != null
//                        && b.getFormField().getTableSourceName().equals("MasterKlasifikasiMasalah")).findFirst().orElse(null);
//                if (fv2 != null) {
//                    if (!Strings.isNullOrEmpty(fv2.getValue())) {
//                        MasterKlasifikasiMasalah klasifikasiMasalah = masterKlasifikasiMasalahService.find(Long.parseLong(fv2.getValue()));
//                        obj.setKlasifikasiArsip(klasifikasiMasalah.getKodeKlasifikasiMasalah() + " " + klasifikasiMasalah.getNamaKlasifikasiMasalah());
//                    }
//                }
//                FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("klasifikasi_masalah"))
//                        .findFirst().orElse(null);
                FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getType().toLowerCase().contains("master-klasifikasi-masalah"))
                        .findFirst().orElse(null);
                if (fv2 != null) {
                    obj.setKlasifikasiArsip(fv2.getValue());
                }
                obj.setJenisDokumen(q.getFormDefinition().getJenisDokumen().getKodeJenisDokumen() + " " + q.getFormDefinition().getJenisDokumen().getNamaJenisDokumen());
                return obj;
            })
                    // .sorted(Comparator.comparing(ReportDocKeluarDto::getTanggal).reversed())
                    .skip(skip).limit(limit)
                    .collect(Collectors.toList());
        }
        return result;
    }

    private List<Surat> getByKonseptor(MasterStrukturOrganisasi org) {
        return db().where(q -> q.getKonseptor().equals(org) && q.getIsDeleted() != true && q.getStatus().toLowerCase().equals("submitted")).toList();
    }

    public Long suratDraftNeedAttCount(Long organizationId) {
        return db().where(t -> t.getKonseptor().getIdOrganization().equals(organizationId)
                && !t.getIsDeleted()
                && t.getStatus().equals("DRAFT"))
                .count();
    }

    public Long suratKeluarNeedAttCount(Long organizationId) {
        return db().where(t -> t.getKonseptor().getIdOrganization().equals(organizationId)
                && t.getStatus().equals("REJECTED")
                && !t.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SM"))
                .count();
    }

    public Long suratRegNeedAttCount(Long organizationId) {
        return db().where(t -> t.getKonseptor().getIdOrganization().equals(organizationId)
                && t.getStatus().equals("REJECTED")
                && t.getFormDefinition().getJenisDokumen().getKodeJenisDokumen().equals("SM"))
                .count();
    }

    public Long suratSetujuNeedAttCount(Long organizationId) {
        return (Long) getEntityManager()
                .createQuery("SELECT COUNT(DISTINCT v.idSurat) FROM ViewSuratSetujuNeedAtt v WHERE v.idAssignee = :idAssignee")
                .setParameter("idAssignee", organizationId)
                .getSingleResult();
    }

    public Long suratSetujuNeedAttCount(List<Long> organizationIds) {
//        return (Long) getEntityManager()
//                .createQuery("SELECT COUNT(DISTINCT v.idSurat) FROM ViewSuratSetujuNeedAtt v WHERE v.idAssignee IN :idAssignees")
//                .setParameter("idAssignees", organizationIds)
//                .getSingleResult();
        List<String> navPem = new ArrayList<>(Arrays.asList("KIRIM_OLEH_KONSEPTOR","SIMPAN_OLEH_PEMERIKSA"));
        List<String> navPen = new ArrayList<>(Arrays.asList("KIRIM_OLEH_PEMERIKSA","SIMPAN_OLEH_PENANDATANGAN"));
        return (Long) getEntityManager()
                .createQuery("SELECT COUNT(DISTINCT v.idSurat) FROM ViewSuratSetujuV2 v WHERE " +
                        "((v.idPenandatangan IN (:idPenandatangan) OR v.idDelegasiPenandatangan IN (:idDelegasiPenandatangan)) AND v.statusNavigasi IN (:navPenandatangan)) OR " +
                        "((v.idPemeriksa IN (:idPemeriksa) OR v.idDelegasiPemeriksa IN (:idDelegasiPemeriksa)) AND v.statusNavigasi IN (:navPemeriksa))")
                .setParameter("idPenandatangan", organizationIds)
                .setParameter("idDelegasiPenandatangan", organizationIds)
                .setParameter("idPemeriksa", organizationIds)
                .setParameter("idDelegasiPemeriksa", organizationIds)
                .setParameter("navPenandatangan",navPen)
                .setParameter("navPemeriksa",navPem)
                .getSingleResult();
    }

    public Long suratMasukNeedAttCount(Long organizationId) {
        return (Long) getEntityManager()
                .createQuery("SELECT COUNT(DISTINCT v.idSurat) FROM ViewSuratMasukNeedAtt v WHERE v.taskAssignee = :idAssignee AND (v.pemerimaAssignee = :idAssignee OR v.disposisiAssignee = :idAssignee)")
                .setParameter("idAssignee", organizationId)
                .getSingleResult();
    }

    public TypedQuery<Surat> getReportData(List<String> status, Boolean cond1, Boolean cond2, String start, String end,
            Long idJenisDokumen, String idArea, String idUnit, Long idKArsip, String sort,
            Boolean desc/*, Integer skip, Integer limit*/) {
        DateUtil dateUtil = new DateUtil();
        Long org = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Surat> q = cb.createQuery(Surat.class);
        Root<Surat> root = q.from(Surat.class);

        q.select(root);
        List<Predicate> predicates = new ArrayList<>();

        // Join Entity surat to Another
        Join<Surat, PenandatanganSurat> penandatangan = root.join("penandatanganSurat", JoinType.LEFT);
        Join<Surat, PemeriksaSurat> pemeriksa = root.join("pemeriksaSurat", JoinType.LEFT);
        Join<Surat, PenerimaSurat> penerima = root.join("penerimaSurat", JoinType.LEFT);
        Join<Surat, TembusanSurat> tembusan = root.join("tembusanSurat", JoinType.LEFT);

//        Predicate mainCond = Strings.isNullOrEmpty(status)
//                ? cb.isFalse(root.get("isDeleted"))
//                : cb.and(cb.isFalse(root.get("isDeleted")), cb.equal(root.get("status"), status));

        Predicate mainCond = status.isEmpty()
                ? cb.isFalse(root.get("isDeleted"))
                : cb.and(cb.isFalse(root.get("isDeleted")), cb.in(root.get("status")).value(status));

        if (cond1 != null && cond1.booleanValue() == true) {
            Predicate pCond1 = cb.or(
                    cb.equal(pemeriksa.get("organization").get("idOrganization"), org),
                    cb.equal(penandatangan.get("organization").get("idOrganization"), org),
                    cb.equal(root.get("konseptor").get("idOrganization"), org)
            );
            Predicate pNotNull = cb.or(
                    cb.isNotNull(root.get("noDokumen"))
            );
            predicates.add(pCond1);
            predicates.add(pNotNull);
        }

        if (cond2 != null && cond2.booleanValue() == true) {
            Predicate pCond2 = cb.or(
                    cb.equal(penerima.get("organization").get("idOrganization"), org),
                    cb.equal(tembusan.get("organization").get("idOrganization"), org)
            );

            Predicate pNotNull = cb.or(
                    cb.isNotNull(root.get("noDokumen"))
            );
            predicates.add(pCond2);
            predicates.add(pNotNull);
        }

        Path<Date> tanggal = (root.<Date>get("submittedDate"));
        if (!Strings.isNullOrEmpty(start)) {
            Boolean single = Strings.isNullOrEmpty(end);
            Date startDate = dateUtil.getDateTimeStart(start + "T00:00:00.000Z");
            if (single) {
                Date endDate = dateUtil.getDateTimeEnd(start + "T23:59:59.999Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(tanggal, startDate),
                        cb.lessThanOrEqualTo(tanggal, endDate));
                predicates.add(pBetween);
            } else {
                Predicate pStart = cb.greaterThanOrEqualTo(tanggal, startDate);
                predicates.add(pStart);
            }
        }

        if (!Strings.isNullOrEmpty(end)) {
            Boolean single = Strings.isNullOrEmpty(start);
            Date endDate = dateUtil.getDateTimeEnd(end + "T23:59:59.999Z");
            if (single) {
                Date startDate = dateUtil.getDateTimeStart(end + "T00:00:00.000Z");
                Predicate pBetween = cb.and(
                        cb.greaterThanOrEqualTo(tanggal, startDate),
                        cb.lessThanOrEqualTo(tanggal, endDate));
                predicates.add(pBetween);
            } else {
                Predicate pEnd = cb.lessThanOrEqualTo(tanggal, endDate);
                predicates.add(pEnd);
            }
        }

        if (idJenisDokumen != null) {
            Predicate pJenisDokumen = cb.equal(root.join("formDefinition", JoinType.INNER)
                    .join("jenisDokumen", JoinType.INNER).get("idJenisDokumen"), idJenisDokumen);
            predicates.add(pJenisDokumen);
        }

        if (StringUtils.isNotEmpty(idArea)) {
            Predicate pArea = cb.equal(root.join("konseptor", JoinType.INNER)
                    .join("user", JoinType.INNER).join("area", JoinType.INNER).get("id"), idArea);
            predicates.add(pArea);
        }

        if (StringUtils.isNotEmpty(idUnit)) {
            Predicate pUnit = cb.equal(root.join("konseptor", JoinType.INNER)
                    .join("user", JoinType.INNER).join("korsa", JoinType.INNER).get("id"), idUnit);
            predicates.add(pUnit);
        }

        if (idKArsip != null) {
            MasterKlasifikasiMasalah masalah = masterKlasifikasiMasalahService.find(idKArsip);
            String klasifikasi = masalah.getKodeKlasifikasiMasalah().trim() + " - " + masalah.getNamaKlasifikasiMasalah().trim();

            Subquery<FormValue> sq = q.subquery(FormValue.class);
            Root<FormValue> sqRoot = sq.from(FormValue.class);
            sq.select(sqRoot);

            Join<FormValue, FormField> fieldList = sqRoot.join("formField", JoinType.INNER);

            List<Predicate> sqPredicates = new ArrayList<>();

            Predicate fieldPredicate = cb.equal(fieldList.get("type"), "master-klasifikasi-masalah");
            sqPredicates.add(fieldPredicate);

            Predicate valPredicate = cb.notEqual(sqRoot.get("value"), "");
            sqPredicates.add(valPredicate);

            Predicate eqValue = cb.equal(sqRoot.get("value"), klasifikasi);
            sqPredicates.add(eqValue);

            sq.where(sqPredicates.toArray(new Predicate[sqPredicates.size()]));
            sq.select(sqRoot.get("id"));

            Predicate pIn = root.join("formValue", JoinType.LEFT).get("id").in(sq);
            predicates.add(pIn);
        }

        predicates.add(mainCond);
        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);
        
        if (Strings.isNullOrEmpty(sort)) {
            q.orderBy(cb.desc(root.get("modifiedDate")));
        } else {
            Expression exp = root.get(sort);
            if (desc) {
                q.orderBy(cb.desc(exp));
            } else {
                q.orderBy(cb.asc(exp));
            }
        }

//        return (skip != null && limit != null)
//                ? getEntityManager().createQuery(q).setFirstResult(skip).setMaxResults(limit)
//                : getEntityManager().createQuery(q);
        return getEntityManager().createQuery(q);
    }

    public HashMap<String, Object> searchDokumen(
            String filter, String tglDikirim,
            String idArea, Long idJenisDokumen,
            Long idKKeamanan, Long idPrioritas,
            Long idKonseptor, Long idPengirim,
            Long idPenerima, boolean otherUnit
    ) {
        List<CariDokumenDto> result = new ArrayList<>();
        TypedQuery<Surat> all = searchDoc("APPROVED", filter, tglDikirim, idArea, idJenisDokumen, idKKeamanan, idPrioritas, idKonseptor, idPengirim, idPenerima, otherUnit);
        result = MappingCariDokDto(all);
        Long length = all.getResultStream().count();
        HashMap<String, Object> retur = new HashMap<String, Object>();
        retur.put("data", result);
        retur.put("length", length);
        return retur;
    }

    private TypedQuery<Surat> searchDoc(
            String status,
            String filter, String tglDikirim,
            String idArea, Long idJenisDokumen,
            Long idKKeamanan, Long idPrioritas,
            Long idKonseptor, Long idPengirim,
            Long idPenerima, boolean otherUnit) {
        DateUtil dateUtil = new DateUtil();
        MasterUser user = userSession.getUserSession().getUser();
        MasterKorsa korsa = user.getKorsa();

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Surat> q = cb.createQuery(Surat.class);
        Root<Surat> root = q.from(Surat.class);

        q.select(root);
        List<Predicate> predicates = new ArrayList<>();

        // Join Entity surat to Another
        Join<Surat, PenandatanganSurat> penandatangan = root.join("penandatanganSurat", JoinType.LEFT);
        Join<Surat, PenerimaSurat> penerima = root.join("penerimaSurat", JoinType.LEFT);

        Predicate mainCond = cb.and(
                cb.isFalse(root.get("isDeleted")),
                cb.equal(root.get("status"), status)
        );

        predicates.add(mainCond);

        if (!otherUnit) {
            Join<MasterStrukturOrganisasi, PenandatanganSurat> pengirim = penandatangan.join("organization", JoinType.INNER);
            Predicate pUnit = cb.equal(pengirim.get("user").get("korsa").get("id"), korsa.getId());
            predicates.add(pUnit);
        }

        if (StringUtils.isNotEmpty(filter)) {
            Subquery<FormValue> sq = q.subquery(FormValue.class);
            Root<FormValue> sqRoot = sq.from(FormValue.class);
            sq.select(sqRoot);

            Join<FormValue, FormField> fieldList = sqRoot.join("formField", JoinType.INNER);

            List<Predicate> sqPredicates = new ArrayList<>();

            Predicate valPredicate = cb.notEqual(sqRoot.get("value"), "");
            sqPredicates.add(valPredicate);

            Predicate eqValue = cb.like(sqRoot.get("value"), "%" + filter + "%");
            sqPredicates.add(eqValue);

            sq.where(sqPredicates.toArray(new Predicate[sqPredicates.size()]));
            sq.select(sqRoot.get("id"));

            Predicate pFilter = cb.or(
                    cb.like(root.get("noDokumen"), "%" + filter + "%"),
                    root.join("formValue", JoinType.LEFT).get("id").in(sq)
            );
            predicates.add(pFilter);
        }

        if (idJenisDokumen != null) {
            Predicate pJenisDokumen = cb.equal(root.join("formDefinition", JoinType.INNER)
                    .join("jenisDokumen", JoinType.INNER).get("idJenisDokumen"), idJenisDokumen);
            predicates.add(pJenisDokumen);
        }

        if (StringUtils.isNotEmpty(idArea)) {
            Join<MasterStrukturOrganisasi, PenandatanganSurat> pengirim = penandatangan.join("organization", JoinType.INNER);
            Predicate pArea = cb.equal(pengirim.join("area", JoinType.INNER).get("id"), idArea);
            predicates.add(pArea);
        }

        if (idKonseptor != null) {
            Predicate pKonseptor = cb.equal(root.get("konseptor").get("idOrganization"), idKonseptor);
            predicates.add(pKonseptor);
        }

        if (StringUtils.isNotEmpty(tglDikirim)) {
            String [] tglDikirim2 = tglDikirim.split("T");
            Date dikirim1 = dateUtil.getDateTimeStart(tglDikirim2[0] + "T00:00:00.000Z");
            Date dikirim2 = dateUtil.getDateTimeEnd(tglDikirim2[0] + "T23:59:59.999Z");

            Predicate pDikirim = cb.and(
                    cb.greaterThanOrEqualTo(root.<Date>get("submittedDate"), dikirim1),
                    cb.lessThanOrEqualTo(root.<Date>get("submittedDate"), dikirim2)
            );
            predicates.add(pDikirim);
        }

        if (idPengirim != null) {
            Predicate pPengirim = cb.equal(penandatangan.join("organization",JoinType.INNER).get("idOrganization"), idPengirim);
            predicates.add(pPengirim);
        }

        if (idPenerima != null) {
            Predicate pPenerima = cb.equal(penerima.join("organization",JoinType.INNER).get("idOrganization"), idPenerima);
            predicates.add(pPenerima);
        }

        if (idKKeamanan != null) {
            MasterKlasifikasiKeamanan keamanan = klasifikasiKeamananService.find(idKKeamanan);
            Subquery<FormValue> sq = q.subquery(FormValue.class);
            Root<FormValue> sqRoot = sq.from(FormValue.class);
            sq.select(sqRoot);

            Join<FormValue, FormField> fieldList = sqRoot.join("formField", JoinType.INNER);

            List<Predicate> sqPredicates = new ArrayList<>();

            Predicate fieldPredicate = cb.equal(fieldList.get("type"), "master-klasifikasi-keamanan");
            sqPredicates.add(fieldPredicate);

            Predicate valPredicate = cb.notEqual(sqRoot.get("value"), "");
            sqPredicates.add(valPredicate);

            Predicate eqValue = cb.equal(sqRoot.get("value"), keamanan.getNamaKlasifikasiKeamanan());
            sqPredicates.add(eqValue);

            sq.where(sqPredicates.toArray(new Predicate[sqPredicates.size()]));
            sq.select(sqRoot.get("id"));

            Predicate pIn = root.join("formValue", JoinType.LEFT).get("id").in(sq);
            predicates.add(pIn);
        }

        if (idPrioritas != null) {
            MasterPrioritas prioritas = prioritasService.find(idPrioritas);
            Subquery<FormValue> sq = q.subquery(FormValue.class);
            Root<FormValue> sqRoot = sq.from(FormValue.class);
            sq.select(sqRoot);

            Join<FormValue, FormField> fieldList = sqRoot.join("formField", JoinType.INNER);

            List<Predicate> sqPredicates = new ArrayList<>();

            Predicate fieldPredicate = cb.equal(fieldList.get("type"), "master-prioritas");
            sqPredicates.add(fieldPredicate);

            Predicate valPredicate = cb.notEqual(sqRoot.get("value"), "");
            sqPredicates.add(valPredicate);

            Predicate eqValue = cb.equal(sqRoot.get("value"), prioritas.getNama());
            sqPredicates.add(eqValue);

            sq.where(sqPredicates.toArray(new Predicate[sqPredicates.size()]));
            sq.select(sqRoot.get("id"));

            Predicate pIn = root.join("formValue", JoinType.LEFT).get("id").in(sq);
            predicates.add(pIn);
        }

        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);

        System.out.println("data: " + getEntityManager().createQuery(q).getResultList());

        return getEntityManager().createQuery(q);
    }

    private List<CariDokumenDto> MappingCariDokDto(TypedQuery<Surat> all) {
        List<CariDokumenDto> result = null;
        MasterUser user = userSession.getUserSession().getUser();
        MasterKorsa korsa = user.getKorsa();

        if (all != null) {
            result = all.getResultStream().sorted(Comparator.comparing(Surat::getSubmittedDate).reversed()).map(q -> {
                CariDokumenDto cd = new CariDokumenDto();
                DateUtil dateUtil = new DateUtil();
                cd.setId(q.getId());
                if (q.getCompanyCode() != null) {
                    CompanyCodeDto company = new CompanyCodeDto(q.getCompanyCode());
                    cd.setCompanyCode(company);
                    cd.setIdCompany(company.getId());
                }
                if (q.getFormDefinition() != null) {
                    FormDefinitionDto formDefinition = new FormDefinitionDto(q.getFormDefinition());
                    cd.setFormDefinition(formDefinition);
                    cd.setIdFormDefinition(formDefinition.getId());
                }
                if (q.getParent() != null) {
                    CariDokumenDto parent = new CariDokumenDto(q.getParent());
                    cd.setParent(parent);
                    cd.setIdParent(parent.getId());
                }
                if (!Strings.isNullOrEmpty(q.getNoDokumen())) {
                    cd.setNoDokumen(q.getNoDokumen());
                }
                if (!Strings.isNullOrEmpty(q.getNoAgenda())) {
                    cd.setNoAgenda(q.getNoAgenda());
                }

                cd.setStatus(q.getStatus());

                if (q.getKonseptor() != null) {
                    MasterStrukturOrganisasiDto org = new MasterStrukturOrganisasiDto(q.getKonseptor());
                    cd.setKonseptor(org);
                    cd.setIdKonseptor(org.getOrganizationId());
                }

                if(q.getUserKonseptor() != null){
                    MasterUserDto users = new MasterUserDto(q.getUserKonseptor());
                    cd.setKonseptorUser(users);
                }

                if(!Strings.isNullOrEmpty(q.getDelegasiType())){
                    cd.setDelegasiType(q.getDelegasiType());
                }

                if (q.getSubmittedDate() != null) {
                    String submitted = dateUtil.getCurrentISODate(q.getSubmittedDate());
                    cd.setSubmittedDate(submitted);
                }

                cd.setIsDeleted(q.getIsDeleted());
                cd.setCreatedBy(q.getCreatedBy());
                cd.setCreatedDate(dateUtil.getCurrentISODate(q.getCreatedDate()));
                cd.setModifiedBy(q.getModifiedBy());
                if (q.getModifiedDate() != null) {
                    cd.setModifiedDate(dateUtil.getCurrentISODate(q.getModifiedDate()));
                }

                List<FormValueDto> fvList = q.getFormValue().stream().map(a -> new FormValueDto(a)).collect(Collectors.toList());
                cd.setFormValueList(fvList);
                cd.setFormValueIds(fvList.stream().map(a -> a.getId()).collect(Collectors.toList()));

                List<MetadataValueDto> mValue = q.getFormValue().stream().map(s -> {
                    MetadataValueDto valueDto = new MetadataValueDto();
                    valueDto.setName(s.getFormField().getMetadata().getNama());
                    valueDto.setLabel(s.getFormField().getMetadata().getLabel());
                    valueDto.setValue(s.getValue());
                    return valueDto;
                }).collect(Collectors.toList());

                cd.setMetadata(mValue);

                if (q.getPenandatanganSurat() != null) {
                    List<PenandatanganSuratDto> ps = q.getPenandatanganSurat().stream().map(r -> new PenandatanganSuratDto(r)).collect(Collectors.toList());
                    cd.setPenandatanganSurat(ps);
                }

                if (q.getPemeriksaSurat() != null) {
                    List<PemeriksaSuratDto> pSurt = q.getPemeriksaSurat().stream().map(r -> new PemeriksaSuratDto(r,true)).collect(Collectors.toList());
                    cd.setPemeriksaSurat(pSurt);
                }

                if (q.getPenerimaSurat() != null) {
                    List<PenerimaSuratDto> penerimaS = q.getPenerimaSurat().stream().map(a -> new PenerimaSuratDto(a)).collect(Collectors.toList());
                    cd.setPenerimaSurat(penerimaS);
                }

                if (q.getTembusanSurat() != null) {
                    List<TembusanSuratDto> tSurat = q.getTembusanSurat().stream().map(a -> new TembusanSuratDto(a)).collect(Collectors.toList());
                    cd.setTembusanSurat(tSurat);
                }

                cd.setIsPemeriksaParalel(q.getIsPemeriksaParalel());
                cd.setFolderArsip(q.getFolderArsip());
                cd.setIdArsip(q.getIdArsip());
                cd.setIsKonsepEdited(q.getIsKonsepEdited());
                if (q.getApprovedDate() != null) {
                    cd.setApprovedDate(dateUtil.getCurrentISODate(q.getApprovedDate()));
                }
                cd.setUndangans(q.getUndangans().stream().map(SuratUndanganDto::new).collect(Collectors.toList()));
                cd.setStatusNavigasi(q.getStatusNavigasi());
                if (q.getIsPinjam() != null) {
                    cd.setIsPinjam(q.getIsPinjam());
                }
                if (q.getPenandatanganSurat().stream().filter(w->w.getOrganization()!= null && w.getOrganization().getUser()!=null &&
                        w.getOrganization().getUser().getKorsa()!=null).noneMatch(a -> a.getOrganization().getUser().getKorsa().getId().equals(korsa.getId()))) {
                    cd.setIsOtherUnit(true);
                    MasterKorsa pemilik = q.getPenandatanganSurat().stream().filter(w->w.getOrganization()!= null && w.getOrganization().getUser()!=null &&
                            w.getOrganization().getUser().getKorsa()!=null).map(r -> r.getOrganization().getUser().getKorsa()).findFirst().orElse(null);
                   if(pemilik!=null) {
                       cd.setKorsaOther(new MasterKorsaDto(pemilik));
                   }
                } else {
                    cd.setIsOtherUnit(false);
                }
                return cd;
            }).collect(Collectors.toList());
        }
        return result;
    }

    public List<ChartDashboardDto> getChartDocKeluar(Long idJenisDok, String idArea, String idUnit, Long idKArsip, Integer year, Integer month, Integer date) {
//        Integer year = 2020, month = null, date = null;
        DateUtil dateUtil = new DateUtil();
        TypedQuery<Surat> all = getReportData(new ArrayList<>(), true, null, null, null, idJenisDok, idArea, idUnit, idKArsip, null, null);
//        TypedQuery<Surat> filtered = all.getResultStream()

        List<Surat> filtered;
        if (year != null && month != null && date != null) {
            filtered = all.getResultStream().filter(q -> year.equals(q.getCreatedDate().getYear()) && month.equals(q.getCreatedDate().getMonth()) && date.equals(q.getCreatedDate().getDate())).collect(Collectors.toList());
        } else if (year != null && month != null) {
            filtered = all.getResultStream().filter(q -> year.equals(q.getCreatedDate().getYear()) && month.equals(q.getCreatedDate().getMonth())).collect(Collectors.toList());
        } else if (year != null) {
            filtered = all.getResultStream().filter(q -> year.equals(q.getCreatedDate().getYear())).collect(Collectors.toList());
        } else {
            filtered = all.getResultList();
        }

        Map<String, Long> counted = filtered.stream().map(q -> q.getJenisDokumen()).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        List<ChartDashboardDto> hasil = new ArrayList<>();
        for (String str : counted.keySet()) {
            Long value = counted.get(str);
            ChartDashboardDto dto = new ChartDashboardDto();
            dto.setName(str);
            dto.setValue(value);
            hasil.add(dto);
        }

        return hasil;
    }

    private TypedQuery<Surat> getChartData(Long idJenisDokumen, String idUnit, String start, String end, Boolean cond1, Boolean cond2, boolean isArsip) {
//        Long org = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();
        String korsa = userSession.getUserSession().getUser().getKorsa() != null ? userSession.getUserSession().getUser().getKorsa().getId() : null;
        String id = StringUtils.isNotEmpty(idUnit) ? idUnit : !Strings.isNullOrEmpty(korsa) ? korsa : null;

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Surat> q = cb.createQuery(Surat.class);
        Root<Surat> root = q.from(Surat.class);

        q.select(root);
        List<Predicate> predicates = new ArrayList<>();

        Predicate mainCond = cb.isFalse(root.get("isDeleted"));
//        Predicate pCond1 = cb.equal(root.get("konseptor").get("idOrganization"), org);

        if (!isArsip) {
            Predicate pNotNull = cb.and(
                    cb.isNotNull(root.get("noDokumen")),
                    cb.isNotNull(root.get("approvedDate"))
            );
            predicates.add(pNotNull);
        } else {
            Predicate pNotNull = cb.and(
                    cb.isNotNull(root.get("idArsip")),
                    cb.isNotNull(root.get("folderArsip"))
            );
            predicates.add(pNotNull);

            if(!Strings.isNullOrEmpty(id)) {
                Join<Surat, PenandatanganSurat> penandatangan = root.join("penandatanganSurat", JoinType.LEFT);
                Predicate pUnit = cb.or(
                        cb.equal(penandatangan.join("organization", JoinType.INNER).join("unit", JoinType.INNER).get("id"), id),
                        cb.equal(penandatangan.join("organization", JoinType.INNER).join("user", JoinType.INNER)
                                .join("korsa", JoinType.INNER).get("id"), id)
                );
                predicates.add(pUnit);
            }
        }


        if (idJenisDokumen != null) {
            Predicate pJenisDokumen = cb.equal(root.join("formDefinition", JoinType.INNER)
                    .join("jenisDokumen", JoinType.INNER).get("idJenisDokumen"), idJenisDokumen);
            predicates.add(pJenisDokumen);
        }

        if (cond1 != null && cond1 && !Strings.isNullOrEmpty(id)) {
            Predicate pUnit = cb.or(
                    cb.equal(root.join("konseptor", JoinType.INNER).join("unit", JoinType.INNER).get("id"), id),
                    cb.equal(root.join("konseptor", JoinType.INNER)
                            .join("user", JoinType.INNER).join("korsa", JoinType.INNER).get("id"), id)
            );
            predicates.add(pUnit);
        }

        if (cond2 != null && cond2 && !Strings.isNullOrEmpty(id)) {
            Join<Surat, PenerimaSurat> penerima = root.join("penerimaSurat", JoinType.LEFT);
            Predicate pUnit = cb.or(
                    cb.equal(penerima.join("organization", JoinType.INNER).join("unit", JoinType.INNER).get("id"), id),
                    cb.equal(penerima.join("organization", JoinType.INNER).join("user", JoinType.INNER)
                            .join("korsa", JoinType.INNER).get("id"), id)
            );
            predicates.add(pUnit);
        }

        DateUtil dateUtil = new DateUtil();
        Date dStart = dateUtil.getDateTimeStart(start), dEnd = dateUtil.getDateTimeEnd(end);
        Path<Date> rootDate;
        if (isArsip) {
            Root<Arsip> arsipRoot = q.from(Arsip.class);
            Predicate pFieldEquals = cb.equal(root.get("idArsip"), arsipRoot.get("id"));
            predicates.add(pFieldEquals);
            rootDate = arsipRoot.<Date>get("createdDate");
        } else {
            rootDate = root.<Date>get("approvedDate");
        }
        Predicate pBetween
                = cb.and(
                cb.greaterThanOrEqualTo(rootDate, dStart),
                cb.lessThanOrEqualTo(rootDate, dEnd)
        );
        predicates.add(pBetween);

        predicates.add(mainCond);
        q.where(predicates.toArray(new Predicate[predicates.size()]));
        q.distinct(true);
        q.orderBy(cb.desc(root.get("modifiedDate")));

        return getEntityManager().createQuery(q);
    }

    public List<ChartDashboardDto> getChartCreatedDoc(Long idJenisDok, String idUnit, String start, String end) {
        try {
            TypedQuery<Surat> all = getChartData(idJenisDok, idUnit, start, end, true, null, false);
            return getChartDocData(all);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<ChartDashboardDto> getChartIncomingDoc(Long idJenisDok, String idUnit, String start, String end) {
        try {
            TypedQuery<Surat> all = getChartData(idJenisDok, idUnit, start, end, null, true, false);
            return getChartDocData(all);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<ChartDashboardDto> getChartArchivedDoc(Long idJenisDok, String idUnit, String start, String end) {
        try {
            TypedQuery<Surat> all = getChartData(idJenisDok, idUnit, start, end, null, null, true);
            return getChartDocData(all);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private List<ChartDashboardDto> getChartDocData(TypedQuery<Surat> all) {
        try {
            List<Surat> filtered = all.getResultList();

            Map<String, Long> counted = filtered.stream().map(q -> q.getFormDefinition().getJenisDokumen().getKodeJenisDokumen()).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            List<ChartDashboardDto> hasil = new ArrayList<>();
            for (String str : counted.keySet()) {
                Long value = counted.get(str);
                ChartDashboardDto dto = new ChartDashboardDto();
                dto.setName(str);
                dto.setValue(value);
                MasterJenisDokumen jenisDokumen = masterJenisDokumenService.getByCode(str);
                dto.setJenisDokumen(jenisDokumen == null ? "Undefined" : jenisDokumen.getNamaJenisDokumen());
                hasil.add(dto);
            }

            return hasil;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public HashMap<String, Object> getListNeglectedInbox(
            String idUnit, String start, String end, Integer skip, Integer limit, String sort, Boolean descending) {
        List<MostLoginDashboardDto> result = new ArrayList<>();

        String korsa = userSession.getUserSession().getUser().getKorsa() != null ? userSession.getUserSession().getUser().getKorsa().getId() : "";
        String id = StringUtils.isNotEmpty(idUnit) ? idUnit : korsa;

        DateUtil dateUtil = new DateUtil();
        Date dStart = dateUtil.getDateTimeStart(start), dEnd = dateUtil.getDateTimeEnd(end);

        List<MasterUser> listUser = userService.getAllValid().where(t -> t.getKorsa().getId().equals(id)).collect(Collectors.toList());

        for (MasterUser user : listUser) {
            if(user.getOrganizationEntity()!=null) {
                Long idOrganization = user.getOrganizationEntity().getIdOrganization();

                Long count = (Long) getEntityManager()
                        .createQuery("SELECT COUNT(DISTINCT v.idSurat) FROM ViewListInboxNeedAtt v " +
                                "WHERE v.taskAssignee = :idAssignee AND (v.pemerimaAssignee = :idAssignee OR v.disposisiAssignee = :idAssignee) " +
                                " AND v.tglDokumen >= :startDate AND v.tglDokumen <= :endDate")
                        .setParameter("idAssignee", idOrganization)
                        .setParameter("startDate", dStart)
                        .setParameter("endDate", dEnd)
                        .getSingleResult();

                if (count > 0) {
                    MostLoginDashboardDto dto = new MostLoginDashboardDto();
                    dto.setValue(count);
                    dto.setJabatan(user.getJabatan());
                    dto.setUnit(user.getKorsa().getId() + " / " + user.getKorsa().getNama());
                    dto.setNama(user.getNameFront() + (Strings.isNullOrEmpty(user.getNameMiddleLast()) ? "" : " " + user.getNameMiddleLast()));
                    result.add(dto);
                }
            }
        }

        if (!Strings.isNullOrEmpty(sort) && !result.isEmpty()) {
            Comparator<MostLoginDashboardDto> comparator =
                    sort.equalsIgnoreCase("jabatan") ?
                            Comparator.comparing(MostLoginDashboardDto::getJabatan) :
                            sort.equalsIgnoreCase("unit") ?
                                    Comparator.comparing(MostLoginDashboardDto::getUnit) :
                                    sort.equalsIgnoreCase("nama") ?
                                            Comparator.comparing(MostLoginDashboardDto::getNama) :
                                            Comparator.comparing(MostLoginDashboardDto::getValue);
            if (descending) {
                result = result.stream().sorted(comparator.reversed()).collect(Collectors.toList());
            } else {
                result = result.stream().sorted(comparator).collect(Collectors.toList());
            }
        } else {
            result = result.stream().sorted(Comparator.comparing(MostLoginDashboardDto::getValue).reversed()).collect(Collectors.toList());
        }

        int length = result.size();

        result = result.stream().skip(skip).limit(limit).collect(Collectors.toList());
        HashMap<String, Object> retur = new HashMap<>();
        retur.put("data", result);
        retur.put("length", length);

        return retur;
    }

    public Surat getSuratByIdArsip(Long idArsip){
        return db().where(a-> !a.getIsDeleted().booleanValue() && a.getIdArsip().equals(idArsip)).findFirst().orElse(null);
    }

    public List<SuratForDistribusiDto> mapToSuratForDistribusiDto(List<Surat> all) throws InternalServerErrorException {
        List<SuratForDistribusiDto> result = new ArrayList<>();
        if (all != null && !all.isEmpty()) {
            result = all.stream().map((surat) -> {

                SuratForDistribusiDto dto = new SuratForDistribusiDto(surat);

                FormDefinition formDefinition = surat.getFormDefinition();
                List<FormValue> formValue = surat.getFormValue();

                String perihal = getFormValueBy("perihal", formDefinition, formValue);
                dto.setPerihal(perihal);

                String klasifikasiKeamanan = this.getFormValueBy("klasifikasi_keamanan", formDefinition, formValue);
                if (!Strings.isNullOrEmpty(klasifikasiKeamanan)) {
                    MasterKlasifikasiKeamanan klasifikasi = klasifikasiKeamananService.findByNamaKlasifikasiKeamanan(klasifikasiKeamanan);
                    dto.setKeamanan(new MasterKlasifikasiKeamananDto(klasifikasi));
                }

                String tingkatUrgensi = this.getFormValueBy("tingkat_urgensi", formDefinition, formValue);
                if (!Strings.isNullOrEmpty(tingkatUrgensi)) {
                    MasterTingkatUrgensi urgensi = tingkatUrgensiService.findByNama(tingkatUrgensi);
                    dto.setTingkatUrgensi(new MasterTingkatUrgensiDto(urgensi));
                }

                String tingkatPerkembangan = this.getFormValueBy("tingkat_perkembangan", formDefinition, formValue);
                if (!Strings.isNullOrEmpty(tingkatPerkembangan)) {
                    MasterTingkatPerkembangan perkembangan = tingkatPerkembanganService.findByNama(tingkatPerkembangan);
                    dto.setTingkatPerkembangan(new MasterTingkatPerkembanganDto(perkembangan));
                }

                return dto;
            }).collect(Collectors.toList());
        }

        return result;
    }


    private String getFormValueBy(String fieldName, FormDefinition formDefinition, List<FormValue> formValues) {
        FormField field = formDefinition.getFormFields().stream()
                .filter(q -> q.getMetadata().getNama().equalsIgnoreCase(fieldName.toLowerCase()))
                .findFirst().orElse(null);
        if (field == null) return "";

        String value = formValues.stream()
                .filter(q -> q.getFormField().getId().equals(field.getId()))
                .map(FormValue::getValue)
                .findFirst().orElse("");

        return value;
    }

    public Long countRejectedToPemeriksa(Long organizationId) {
        return db().where(a-> !a.getIsDeleted().booleanValue() && a.getStatus().equals("REJECTED") && a.getKonseptor()
        .getIdOrganization().equals(organizationId)).count();
    }
}
