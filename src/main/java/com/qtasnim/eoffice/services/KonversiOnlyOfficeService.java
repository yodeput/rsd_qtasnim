/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.services;

import com.google.gson.Gson;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.servlets.demo.helpers.ConfigManager;
import com.qtasnim.eoffice.servlets.demo.helpers.DocumentManager;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.qtasnim.eoffice.util.ExceptionUtil;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author acdwisu
 * 
 * modified from OnlyOffice sample -ServiceConverter-
 */
@Stateless
@LocalBean
public class KonversiOnlyOfficeService {

    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private com.qtasnim.eoffice.services.Logger logger;

    private int convertTimeout = 120000;
    private String documentConverterUrl;
    
    public KonversiOnlyOfficeService() {
    }

    @PostConstruct
    private void postConstruct() {
        documentConverterUrl = applicationConfig.getOnlyOfficeUrlDocumentConverter();//ConfigManager.GetProperty("files.docservice.url.converter");

        try {
            int timeout = Integer.parseInt(applicationConfig.getFilesDocServiceTimeout());//ConfigManager.GetProperty("files.docservice.timeout"));

            if (timeout > 0)  {
                convertTimeout = timeout;
            }
        }
        catch (Exception ex) {
        }
    }
    
    public byte[] convert(byte[] src, String fromExtension, String toExtension) {
        /**
         * 1. file source simpan ke folder temp
         * 2. Buat servlet untuk download file temp, misalnya: http://localhost:8080/DownloadConversion?fileName=?
         * 3. Request convert API ke OnlyOffice menggunakan url yang tadi
         * 4. Download hasil convert dari onlyoffice, langsung jadi byte[]
         * 5. return byte[]
         * 6. Delete file temp
         */
        try {
            byte[] converted = null;

            String fileNameSrc = UUID.randomUUID().toString().concat(fromExtension);
//        String fileNameRes = UUID.randomUUID().toString().concat(toExtension);

            String pathSrc = applicationConfig.getTempDir()+"/"+fileNameSrc;
//        String pathConverted = applicationConfig.getTempDir()+fileNameRes;

            try (FileOutputStream fos = new FileOutputStream(pathSrc)) {
                fos.write(src);
                fos.close();

                String baseUrl = applicationConfig.getBaseUrl();

                String documentUri = baseUrl+"/filedownload?fileName="+fileNameSrc;
                System.out.println("documentUri : " +documentUri);
                String convertedUrl = this.getConvertedURL(documentUri, fromExtension.replace(".",""), toExtension.replace(".", ""));
                System.out.println("convertedUrl " +convertedUrl);
                converted = downloadConvertedResult(convertedUrl);

//            OutputStream out = new FileOutputStream(pathConverted);
//            out.write(converted);
//            out.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

            Files.deleteIfExists(new java.io.File(pathSrc).toPath());

            return converted;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage(), e);
        }
    }

    public File convertFile(byte[] src, String fromExtension, String toExtension) throws IOException {
        byte[] converted = convert(src, fromExtension, toExtension);

        try {
            File file = new File(applicationConfig.getTempDir(), UUID.randomUUID().toString().concat(toExtension));
            FileOutputStream fos = new FileOutputStream(file);

            fos.write(converted);
            fos.close();

            return file;
        } catch (IOException e) {
            throw e;
        }
    }

    private byte[] downloadConvertedResult(String url) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
          
        try (InputStream inputStream = new URL(url).openStream()) {
            int n = 0;
            byte [] buffer = new byte[ 1024 ];
            while (-1 != (n = inputStream.read(buffer))) {
                output.write(buffer, 0, n);
            }
            
            return output.toByteArray();
        } catch (MalformedURLException ex) {
            Logger.getLogger(KonversiOnlyOfficeService.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(KonversiOnlyOfficeService.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }      
    }
        
    private String getConvertedURL(String documentUri, String fromExtension, String toExtension) throws Exception
    {
        String jsonString = null;

        try {
            fromExtension = fromExtension == null || fromExtension.isEmpty() ? FileUtility.GetFileExtension(documentUri) : fromExtension;

            String title = FileUtility.GetFileName(documentUri).split("fileName=")[1];
            title = title == null || title.isEmpty() ? UUID.randomUUID().toString() : title;

            ConvertBody body = new ConvertBody();
            body.url = documentUri;
            body.outputtype = toExtension.replace(".", "");
            body.filetype = fromExtension.replace(".", "");
            body.title = title;
            body.async = false;
            body.key = title.split("\\.")[0];

            Gson gson = new Gson();
            String bodyString = gson.toJson(body);

            byte[] bodyByte = bodyString.getBytes(StandardCharsets.UTF_8);

            URL url = new URL(documentConverterUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setFixedLengthStreamingMode(bodyByte.length);
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(convertTimeout);

            if (DocumentManager.TokenEnabled()) {
                Map<String, Object> map = new HashMap<>();
                map.put("payload", body);
                String token = DocumentManager.CreateToken(map);
                connection.setRequestProperty("Authorization", "Bearer " + token);
            }

            connection.connect();
            try (OutputStream os = connection.getOutputStream()) {
                os.write(bodyByte);
            }

            InputStream stream = connection.getInputStream();

            if (stream == null)
                throw new Exception("Could not get an answer");

            jsonString = convertStreamToString(stream);

            connection.disconnect();

            return getResponseUri(jsonString);
        } catch (Exception e) {
            logger.error(e.getMessage(), ExceptionUtil.getRealCause(e));

            if (StringUtils.isNotEmpty(jsonString)) {
                System.out.println("jsonString : " + jsonString);
                logger.error(jsonString);
            }

            throw e;
        }
    }
    
    public String generateRevisionId(String expectedKey)
    {
        if (expectedKey.length() > 20)
            expectedKey = Integer.toString(expectedKey.hashCode());

        String key = expectedKey.replace("[^0-9-.a-zA-Z_=]", "_");

        return key.substring(0, Math.min(key.length(), 20));
    }

    private void processConvertServiceResponceError(int errorCode) throws Exception
    {
        String errorMessage = "";
        String errorMessageTemplate = "Error occurred in the ConvertService: ";

        switch (errorCode)
        {
            case -8:
                errorMessage = errorMessageTemplate + "Error document VKey";
                break;
            case -7:
                errorMessage = errorMessageTemplate + "Error document request";
                break;
            case -6:
                errorMessage = errorMessageTemplate + "Error database";
                break;
            case -5:
                errorMessage = errorMessageTemplate + "Error unexpected guid";
                break;
            case -4:
                errorMessage = errorMessageTemplate + "Error download error";
                break;
            case -3:
                errorMessage = errorMessageTemplate + "Error convertation error";
                break;
            case -2:
                errorMessage = errorMessageTemplate + "Error convertation timeout";
                break;
            case -1:
                errorMessage = errorMessageTemplate + "Error convertation unknown";
                break;
            case 0:
                break;
            default:
                errorMessage = "ErrorCode = " + errorCode;
                break;
        }

        throw new Exception(errorMessage);
    }

    private String getResponseUri(String jsonString) throws Exception
    {
        JSONObject jsonObj = convertStringToJSON(jsonString);

        Object error = jsonObj.get("error");

        if (error != null) {
            logger.error(String.format("Error download return from onlyoffice: %s", jsonString));
            processConvertServiceResponceError(Math.toIntExact((long)error));
        }

        Boolean isEndConvert = (Boolean) jsonObj.get("endConvert");

        Long resultPercent = 0l;
        String responseUri = null;

        if (isEndConvert)
        {
            resultPercent = 100l;
            responseUri = (String) jsonObj.get("fileUrl");
        }
        else
        {
            resultPercent = (Long) jsonObj.get("percent");
            resultPercent = resultPercent >= 100l ? 99l : resultPercent;
        }

        return resultPercent >= 100l ? responseUri : "";
    }

    private String convertStreamToString(InputStream stream) throws IOException
    {
        InputStreamReader inputStreamReader = new InputStreamReader(stream);
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line = bufferedReader.readLine();

        while (line != null)
        {
            stringBuilder.append(line);
            line = bufferedReader.readLine();
        }

        String result = stringBuilder.toString();

        return result;
    }

    private JSONObject convertStringToJSON(String jsonString) throws ParseException
    {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(jsonString);
        JSONObject jsonObj = (JSONObject) obj;

        return jsonObj;
    }
    
    private class ConvertBody {
        public String url;
        public String outputtype;
        public String filetype;
        public String title;
        public String key;
        public Boolean async;
        public String token;
    }
}
