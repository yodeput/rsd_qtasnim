package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterMetadata;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterMetadataDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

/**
 * Service untuk fungsi CRUD table m_user
 * 
 * @author seno@qtasnim.com
 */


 @Stateless
 @LocalBean
public class MasterMetadataService extends AbstractFacade<MasterMetadata>{

    private static final long serialVersionUID = -4184209743573831159L;

    @Inject
    MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterMetadata> getEntityClass() {
        return MasterMetadata.class;
    }

    public JPAJinqStream<MasterMetadata> getAll() {
        Date n = new Date();     return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }
    
    public MasterMetadata save(Long id, MasterMetadataDto dao){
        MasterMetadata model = new MasterMetadata();
        DateUtil dateUtil = new DateUtil();

        if(id==null) {
           model.setNama(dao.getNama());
           model.setLabel(dao.getLabel());
           model.setCreatedBy(dao.getCreatedBy());
           model.setCreatedDate(new Date());
           model.setModifiedBy("-");
           model.setModifiedDate(new Date());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.create(model);
        } else {
            model = this.find(id);
            model.setNama(dao.getNama());
            model.setLabel(dao.getLabel());
            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());

            if(dao.getCompanyId()!=null){
                CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
                model.setCompanyCode(cCode);
            } else {
                model.setCompanyCode(null);
            }
            model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
            if(dao.getEndDate()!=null){
                model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
            } else {
                model.setEnd(dateUtil.getDefValue());
            }
            this.edit(model);
        }

      return model;
    }

    public MasterMetadata delete(Long id){
        MasterMetadata model = this.find(id);
        this.remove(model);
        
        return model;
    }
}