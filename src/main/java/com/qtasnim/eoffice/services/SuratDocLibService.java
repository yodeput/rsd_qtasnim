package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.db.SuratDocLib;
import com.qtasnim.eoffice.ws.dto.SuratDocLibDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class SuratDocLibService extends AbstractFacade<SuratDocLib> {

    @Inject
    private FileService fileService;

    @Override
    protected Class<SuratDocLib> getEntityClass() {
        return SuratDocLib.class;
    }

    public JPAJinqStream<SuratDocLib> getAttachment(Long idSurat){
        return db().where(a->a.getIsKonsep()&&a.getIsPublished() && a.getSurat().getId()==idSurat);
    }

    public SuratDocLibDto getLink(Long idSuratDocLib) throws CMISProviderException {
        SuratDocLib obj = this.find(idSuratDocLib);
        return getLink(obj);
    }

    public SuratDocLibDto getLink(SuratDocLib obj) throws CMISProviderException {
        SuratDocLibDto result = new SuratDocLibDto(obj);
        result.setEditLink(fileService.getEditLink(obj.getDocId()));
        result.setViewLink(fileService.getViewLink(obj.getDocId()));
        return result;
    }

    public JPAJinqStream<SuratDocLib> getAttachmentFile(Long idSurat){
        return db().where(a->!a.getIsKonsep() && a.getSurat().getId()==idSurat);
    }

    public SuratDocLibDto findByDocId(String docId){
        return db().where(a->a.getDocId().equals(docId)).map(SuratDocLibDto::new).findFirst().orElse(null);
    }

    public JPAJinqStream<SuratDocLib> getReferenceFile(Long idSurat){
        return db().where((a,surat) -> a.getIsKonsep() && a.getIsPublished() && a.getSurat().getId().equals(
                surat.stream(Surat.class).where(b->b.getParent().getId()==idSurat).select(c->c.getId()).getOnlyValue()));
    }
}
