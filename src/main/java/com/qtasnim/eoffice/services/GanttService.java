package com.qtasnim.eoffice.services;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.ws.dto.GanttGroup;
import com.qtasnim.eoffice.ws.dto.GanttItem;
import com.qtasnim.eoffice.ws.dto.GanttTask;
import com.qtasnim.eoffice.ws.dto.NgGantt;
import me.xdrop.jrand.JRand;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Stateless
@LocalBean
public class GanttService {
    public String compile(List<GanttItem> items) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<NgGantt> result = new ArrayList<>();

        for (GanttItem item :items) {
            if (item instanceof GanttGroup) {
                GanttGroup group = (GanttGroup) item;

                NgGantt ngGantt = new NgGantt();
                ngGantt.setId(group.getId());
                ngGantt.setName(group.getName());
                ngGantt.setNotes(group.getNotes());
                ngGantt.setCaption(group.getCaption());
                ngGantt.setClazz("ggroupblack");
                ngGantt.setDepend(Optional.ofNullable(group.getDepend()).map(GanttItem::getId).orElse(null));
                ngGantt.setMile(0);
                ngGantt.setGroup(1);
                ngGantt.setParent(Optional.ofNullable(group.getParent()).map(GanttItem::getId).orElse(0L));
                ngGantt.setOpen(Optional.of(group).filter(t -> t.getOpen() != null && t.getOpen()).map(t -> 1).orElse(0));

                result.add(ngGantt);
            } else if (item instanceof GanttTask) {
                GanttTask task = (GanttTask) item;

                NgGantt ngGantt = new NgGantt();
                ngGantt.setId(task.getId());
                ngGantt.setName(task.getName());

                //"gtaskblue", "gtaskred", "gtaskyellow", "gtaskpurple", "gtaskgreen", "gtaskpink"
                switch (task.getStatus()) {
                    case NOTSTARTED: {
                        ngGantt.setClazz("gtaskyellow");
                        break;
                    }
                    case COMPLETED: {
                        ngGantt.setClazz("gtaskblue");
                        break;
                    }
                    case ONPROGRESS: {
                        ngGantt.setClazz("gtaskgreen");
                        break;
                    }
                    case LATE: {
                        ngGantt.setClazz("gtaskred");
                        break;
                    }
                }

                ngGantt.setStart(simpleDateFormat.format(task.getStart()));
                ngGantt.setEnd(simpleDateFormat.format(task.getEnd()));
                ngGantt.setMile(0);
                ngGantt.setResource(Optional.ofNullable(task.getResource()).map(t -> String.format("%s %s", t.getNameFront(), t.getNameMiddleLast())).orElse("Belum Ditentukan"));
                ngGantt.setCompletion(task.getCompletion());
                ngGantt.setGroup(0);
                ngGantt.setParent(Optional.ofNullable(task.getParent()).map(GanttItem::getId).orElse(null));
                ngGantt.setDepend(Optional.ofNullable(task.getDepend()).map(GanttItem::getId).orElse(null));
                ngGantt.setNotes(task.getNotes());
                ngGantt.setCaption(task.getCaption());

                result.add(ngGantt);
            }
        }

        try {
            return getObjectMapper().writeValueAsString(result);
        } catch (JsonProcessingException e) {
            return "[]";
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        return objectMapper;
    }
}
