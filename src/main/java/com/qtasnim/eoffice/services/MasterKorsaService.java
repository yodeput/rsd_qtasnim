package com.qtasnim.eoffice.services;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterKorsa;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterKorsaDto;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;


@Stateless
@LocalBean
public class MasterKorsaService extends AbstractFacade<MasterKorsa> {
    private static final long serialVersionUID = 1L;
    @Inject
    private CompanyCodeService companyCodeService;

    @Override
    protected Class<MasterKorsa> getEntityClass() {
        return MasterKorsa.class;
    }

    public JPAJinqStream<MasterKorsa> getAll() {
        Date n = new Date();
        return db().where(q -> n.after(q.getStart()) && n.before(q.getEnd()));
    }

    public MasterKorsa findById(String id){
        return db().where(q-> q.getId().equals(id)).findFirst().orElse(null);
    }

    public JPAJinqStream<MasterKorsa> getFiltered(String name) {
        return db().where(q -> q.getNama().toLowerCase().contains(name));
    }


    public MasterKorsa save(String id, MasterKorsaDto dao) {
        MasterKorsa model = new MasterKorsa();
        if (id == null) {
            this.create(data(model, dao));
        } else {
            model = this.find(id);
            this.edit(data(model, dao));
        }
        return model;
    }

    public MasterKorsa data(MasterKorsa masterKorsa, MasterKorsaDto dao) {
        DateUtil dateUtil = new DateUtil();
        MasterKorsa model = masterKorsa;
        model.setId(dao.getId());
        model.setNama(dao.getNama());
        if (dao.getCompanyId() != null) {
            CompanyCode cCode = companyCodeService.find(dao.getCompanyId());
            model.setCompanyCode(cCode);
        } else {
            model.setCompanyCode(null);
        }
        model.setStart(dateUtil.getDateTimeStart(dao.getStartDate()));
        if (dao.getEndDate() != null) {
            model.setEnd(dateUtil.getDateTimeEnd(dao.getEndDate()));
        } else {
            model.setEnd(dateUtil.getDefValue());
        }
        model.setKode(dao.getKode());
        return model;
    }

    public MasterKorsa delete(String id) {
        MasterKorsa model = this.find(id);
        this.remove(model);
        return model;
    }

}