package com.qtasnim.eoffice.security;

import com.qtasnim.eoffice.InternalServerErrorException;

public class ForbiddenException extends InternalServerErrorException {
    public ForbiddenException(String message) {
        super(message);
    }

    public ForbiddenException(String message, Throwable ex) {
        super(message, ex);
    }
}
