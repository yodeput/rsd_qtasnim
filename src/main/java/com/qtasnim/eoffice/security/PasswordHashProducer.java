package com.qtasnim.eoffice.security;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

public class PasswordHashProducer {
    @Produces
    private PasswordHash produceLogger(InjectionPoint injectionPoint) {
        return new PasswordHash();
    }
}
