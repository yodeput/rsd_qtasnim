package com.qtasnim.eoffice.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.Vetoed;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import com.qtasnim.eoffice.services.Logger;

@Vetoed
public class PasswordHash {
    @Inject
    private Logger logger;

    public String getHash(String password, String salt) {
        MessageDigest md5;
        StringBuilder hexString = new StringBuilder();
        
        try {
            md5 = MessageDigest.getInstance("md5");
            md5.reset();
            String str = salt + password;
            md5.update(str.getBytes());
            byte messageDigest[] = md5.digest();

            for (byte aMessageDigest : messageDigest) {
                hexString.append(Integer.toHexString(0xFF & aMessageDigest));
            }
        } catch (NoSuchAlgorithmException e) {
            logger.error("Can't find md5 algoritm , so ", e);
        }
        return hexString.toString();
    }

    public List<String> getSaltPassword(String password) throws NoSuchAlgorithmException {

        MessageDigest md5;
        StringBuilder hexString = new StringBuilder();

        List<String> result = new ArrayList<String>();
        md5 = MessageDigest.getInstance("md5");
        Date date = new Date();

        md5.reset();
        String salt = Long.toString(date.getTime()) + "!SD@#$D@XC" + password;
        StringBuilder salthash = new StringBuilder();
        md5.update(salt.getBytes());
        byte saltDigest[] = md5.digest();

        for (int i = 0; i < saltDigest.length; i++) {
            salthash.append(Integer.toHexString(0xFF & saltDigest[i]));
        }
        salt = salthash.toString();
        result.add(salthash.toString());
        String ss = salt + password;
        md5.reset();

        md5.update(ss.getBytes());

        byte messageDigest[] = md5.digest();

        for (int i = 0; i < messageDigest.length; i++) {
            hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
        }
        result.add(hexString.toString());

        return result;
    }
}