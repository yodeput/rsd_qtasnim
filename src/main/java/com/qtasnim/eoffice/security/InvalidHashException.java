package com.qtasnim.eoffice.security;

import com.qtasnim.eoffice.InternalServerErrorException;

public class InvalidHashException extends InternalServerErrorException {

    private static final long serialVersionUID = 1L;

    public InvalidHashException(String message) {
        super(message);
    }

    public InvalidHashException(String message, Exception e) {
        super(message, e);
    }
}