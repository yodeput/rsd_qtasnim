package com.qtasnim.eoffice.security;

import com.qtasnim.eoffice.InternalServerErrorException;

public class NotAuthorizedException extends InternalServerErrorException {

    public NotAuthorizedException(String message) {
        super(message);
    }

    public NotAuthorizedException(String message, Throwable ex) {
        super(message, ex);
    }
}
