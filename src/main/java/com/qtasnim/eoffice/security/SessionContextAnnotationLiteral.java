package com.qtasnim.eoffice.security;

import javax.enterprise.util.AnnotationLiteral;

public class SessionContextAnnotationLiteral extends AnnotationLiteral<ISessionContext> implements ISessionContext {
    private String value="";

    public SessionContextAnnotationLiteral(String value) {
        this.value = value;
    }

    public SessionContextAnnotationLiteral() {
        this("");
    }

    public String value() {
        return value;
    }
}
