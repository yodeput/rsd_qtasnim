package com.qtasnim.eoffice.security;

import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.AppSession;
import com.qtasnim.eoffice.db.MasterRole;
import com.qtasnim.eoffice.db.UserSession;
import com.qtasnim.eoffice.services.AppSessionService;
import com.qtasnim.eoffice.services.JWTService;
import com.qtasnim.eoffice.services.MasterRoleService;
import com.qtasnim.eoffice.services.UserSessionService;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Priority;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.security.Principal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * JWT-Filter.
 *
 * @author seno@qtasnim.com
 */
@Provider
@Secured
@Priority(Priorities.AUTHENTICATION)
public class JWTAuthedFilter implements ContainerRequestFilter {
    @Inject
    @ISessionContext
    private Event<UserSession> userAuthenticatedEvent;

    @Inject
    @ISessionContext
    private Event<AppSession> appAuthenticatedEvent;

    @Inject
    private UserSessionService userSessionService;

    @Inject
    private MasterRoleService masterRoleService;

    @Inject
    private JWTService jwtService;

    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private AppSessionService appSessionService;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        try {
            String authorization = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

            if (StringUtils.isEmpty(authorization) || authorization.split(" ").length != 2) {
                throw new ForbiddenException("Invalid token");
            }

            String token = authorization.split(" ")[1];
            SecurityContext originalContext = requestContext.getSecurityContext();
            requestContext.setSecurityContext(null);

            UserSession userSession = userSessionService.findByToken(token);

            if (userSession == null) {
                AppSession appSession = appSessionService.findByToken(token);
                if(appSession!=null){
                    jwtService.validateApp(appSession, applicationConfig);

                    Authorizer authorizer = new Authorizer(
                            appSession.getAppName(),
                            null, originalContext.isSecure(),
                            token);
                    requestContext.setSecurityContext(authorizer);

                    appAuthenticatedEvent.fire(appSession);
                }else {
                    throw new ForbiddenException("Invalid token");
                }
            }else {

                jwtService.validate(userSession, applicationConfig);

                Set<String> roles = new HashSet<>();

                if (userSession.getUser().getIsInternal()) {
                    roles = masterRoleService.findRolesByUsername(userSession.getUser().getLoginUserName()).stream().map(MasterRole::getRoleName).collect(Collectors.toSet());
                }

                Authorizer authorizer = new Authorizer(
                        userSession.getUser().getLoginUserName(),
                        roles,
                        originalContext.isSecure(),
                        token);
                requestContext.setSecurityContext(authorizer);

                userAuthenticatedEvent.fire(userSession);
            }
        } catch (ForbiddenException e) {
            requestContext.abortWith(
                Response
                .ok(new MessageDto(Optional.ofNullable(e.getCause()).map(Throwable::getMessage).orElse(e.getMessage()), "error"))
                .status(Response.Status.FORBIDDEN)
                .header("X-Reason", e.getMessage())
                .header("Access-Control-Expose-Headers", "X-Reason")
                .build()
            );
        }
    }

    public static class Authorizer implements SecurityContext {
        private String username;
        private Set<String> roles;
        private boolean isSecure;
        private String token;

        public Authorizer(String username, Set<String> roles, boolean isSecure, String token) {
            this.username = username;
            this.roles = roles;
            this.isSecure = isSecure;
            this.token = token;
        }

        @Override
        public Principal getUserPrincipal() {
            return new JwtPricipal(this.username, this.token);
        }

        @Override
        public boolean isUserInRole(String role) {
            return roles.contains(role);
        }

        @Override
        public boolean isSecure() {
            return isSecure;
        }

        @Override
        public String getAuthenticationScheme() {
            return "JWT";
        }
    }

    public static class JwtPricipal implements Principal {
        private String name;
        @Getter
        private String token;

        public JwtPricipal(String name, String token) {
            this.name = name;
            this.token = token;
        }

        @Override
        public String getName() {
            return name;
        }
    }
}