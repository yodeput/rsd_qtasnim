package com.qtasnim.eoffice.security;

import com.qtasnim.eoffice.context.RequestContext;
import com.qtasnim.eoffice.db.UserSession;
import com.qtasnim.eoffice.db.Session;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;

@RequestScoped
public class AuthenticatedUserProducer {
    @Produces
    @ISessionContext
    @RequestScoped
    private Session userSession = new Session();

    @Produces
    @IRequestContext
    @RequestScoped
    private Transaction transaction = new Transaction();

    public void handleAuthenticationUserEvent(@Observes @ISessionContext UserSession session) {
        this.userSession.setUserSession(session);
    }

    public void handleTransactionIDEvent(@Observes @IRequestContext RequestContext requestScoped) {
        this.transaction.setRequestContext(requestScoped);
    }
}