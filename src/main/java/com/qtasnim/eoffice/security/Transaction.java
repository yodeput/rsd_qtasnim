package com.qtasnim.eoffice.security;

import com.qtasnim.eoffice.context.RequestContext;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    private RequestContext requestContext;
}
