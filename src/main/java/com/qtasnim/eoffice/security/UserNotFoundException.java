package com.qtasnim.eoffice.security;

import com.qtasnim.eoffice.InternalServerErrorException;

public class UserNotFoundException extends InternalServerErrorException {
    public UserNotFoundException(String message) {
        super(message);
    }

    public UserNotFoundException(String message, Throwable ex) {
        super(message, ex);
    }
}
