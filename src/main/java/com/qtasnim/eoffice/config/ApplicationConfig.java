package com.qtasnim.eoffice.config;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.bssn.BssnDigisignProvider;
import com.qtasnim.eoffice.jms.MessagingProvider;
import com.qtasnim.eoffice.rabbitmq.RabbitMqProvider;
import com.qtasnim.eoffice.services.IDigiSignService;
import com.qtasnim.eoffice.services.IMessagingProvider;
import com.qtasnim.eoffice.util.BeanUtil;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.enterprise.inject.Vetoed;

@Vetoed
@Data
public class ApplicationConfig {
    private String applicationName;
    private String applicationDescription;
    private String logo;
    private String jwtKey;
    private String cmisProvider;
    private String digisignProvider;
    private String baseUrl;
    private String frontEndUrl;
    private Integer docLibExpirationDays = 5;
    private String workflowProvider = "camunda";
    /**
     * Miliseconds
     */
    private Integer timeout;

    private String hrisUrl;
    private String hrisAuthBearer;
    private String loginUrl;
    private Boolean skipKaiLogin;

    private Boolean emailEnabled;
    private String emailFromAddress;
    private String emailUsername;
    private String emailPassword;
    private String emailHost;
    private String emailPort;
    private Boolean emailTls;
    private Boolean isEmailTest;
    private String  emailTest;

    private String filesizeMax = "5242880";
    private String storageFolder = "app_data";

    private String filesDocServiceViewedDocs;
    private String filesDocServiceEditedDocs;
    private String filesDocServiceConvertDocs;
    private String filesDocServiceTimeout;

    private String onlyOfficeUrlDocumentConverter;
    private String onlyOfficeUrlTempStorage;
    private String onlyOfficeUrlApi;
    private String onlyOfficeUrlPreLoader;
    private String onlyOfficeSecret;
    private String onlyofficeContainerOrigin;
    private String onlyofficeOrigin;
    private String onlyOfficePluginLayoutingKeyApiPath;
    private String onlyOfficeAutoStartPlugins;

    private String syncOrgHost;
    private String syncEmployeeHost;
    private String syncPosisiHost;
    private String syncAuthBearer;

    private String locationDbsPackage;
    private String locationServicesPackage;
    private String locationDtoPackage;
    private String datamasterListToIgnoreTravers;
    private String datamasterListToParentHandle;

    private String qrcodeContentDefault;
    private String qrcodeSizeDefault;
    private String qrcodeSizeMinimal;
    private String qrcodeSizeLogoHeightPercentage;

    private String OcrAllowedImageFormat;
    private String OcrPreprocessIsBinaryze;
    private String OcrPreprocessIsDeskew;
    private String OcrPreprocessIsDenoise;
    private String OcrPreprocessIsEnhance;

    private String resetPasswordUrl;
    private String validateCodeUrl;
    private String changePasswordUrl;
    private String forgotPasswordUrl;

    private String plugginScanningTwainKey;
    private Boolean plugginScanningTwainIsTrial;

    private String backgroundServiceWorker;

    private String messageServiceProvider;
    private String amqpHost;
    private String amqpUsername;
    private String amqpPassword;
    private Boolean amqpSecure;

    private String tempDir = System.getProperty("java.io.tmpdir");


    public ApplicationConfig() {

    }

    public IMessagingProvider getMessagingProvider() throws InternalServerErrorException {
        try {
            if (StringUtils.isNotEmpty(messageServiceProvider)) {
                return (IMessagingProvider) BeanUtil.getBean(Class.forName(messageServiceProvider));
            } else {
                return null;
            }
        } catch (ClassNotFoundException ex) {
            throw new InternalServerErrorException("Invalid Digisign class", ex);
        }
    }

    public static ApplicationConfig defaultConfig() {
        ApplicationConfig config = new ApplicationConfig();
        config.setApplicationName("e-Office");
        config.setApplicationDescription("e-Office v2 created by PT Qtasnim Digital Teknologi");
        config.setJwtKey("123");
        config.setCmisProvider("alfresco");
        config.setTimeout(30000);
        config.setBaseUrl("http://localhost:4200");

        config.setHrisUrl("https://ws-int-alpha.kai.id/v2/hris");
        config.setHrisAuthBearer("PMFcJknW_1554951890");
        config.setLoginUrl("https://ws-int-alpha.kai.id/v2/user/login");
        config.setSkipKaiLogin(true);

        config.setEmailEnabled(false);
        config.setPlugginScanningTwainKey("");
        config.setPlugginScanningTwainIsTrial(true);

        config.setDigisignProvider(BssnDigisignProvider.class.getName());

        config.setAmqpHost("docker");
        config.setAmqpUsername("admin");
        config.setAmqpPassword("admin");
        config.setAmqpSecure(false);
        config.setMessageServiceProvider(MessagingProvider.class.getName());

        return config;
    }
}
