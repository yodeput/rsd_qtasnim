package com.qtasnim.eoffice.listeners;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.SessionContextAnnotationLiteral;
import com.qtasnim.eoffice.util.BeanUtil;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;
import java.util.Optional;

public class DocListener {
    @PrePersist
    void onPersist(Object object) {
        if (object instanceof ICreationDoc || object instanceof IModificationDoc) {
            Session userSession = (Session) BeanUtil.getBean(Session.class, new SessionContextAnnotationLiteral());
            Date now = new Date();
            String username = "guest";

            try {
                username = Optional.ofNullable(userSession).filter(t -> t.getUserSession() != null).map(t -> {
                    try {
                        return t.getUserSession().getUser().getEmployeeId()+" | "+t.getUserSession().getUser().getOrganizationEntity().getOrganizationName()+
                                " | "+t.getUserSession().getUser().getNameFront()+" "+t.getUserSession().getUser().getNameMiddleLast();
                    } catch (Exception e) {
                        return "guest";
                    }
                }).orElse("guest");
            } catch (Exception e) {

            }

            if (object instanceof ICreationDoc) {
                ICreationDoc entity = (ICreationDoc) object;
                entity.setCreatedBy(username);
                entity.setCreatedDate(now);
            }

            if (object instanceof IModificationDoc) {
                IModificationDoc entity = (IModificationDoc) object;
                entity.setModifiedBy(username);
                entity.setModifiedDate(now);
            }
        }
    }

    @PreUpdate
    void onUpdate(Object object) {
        if (object instanceof IModificationDoc) {
            Session userSession = (Session) BeanUtil.getBean(Session.class, new SessionContextAnnotationLiteral());
            Date now = new Date();

            IModificationDoc entity = (IModificationDoc) object;

            try {
                entity.setModifiedBy(Optional.ofNullable(userSession).filter(t -> t.getUserSession() != null).map(t -> {
                    try {
                        return t.getUserSession().getUser().getEmployeeId()+" | "+t.getUserSession().getUser().getOrganizationEntity().getOrganizationName()+
                                " | "+t.getUserSession().getUser().getNameFront()+" "+t.getUserSession().getUser().getNameMiddleLast();
                    } catch (Exception e) {
                        return "guest";
                    }
                }).orElse("guest"));
            } catch (Exception e) {
                entity.setModifiedBy("guest");
            }
            entity.setModifiedDate(now);
        }
    }

}
