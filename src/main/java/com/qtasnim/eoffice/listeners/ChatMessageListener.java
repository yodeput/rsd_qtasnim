package com.qtasnim.eoffice.listeners;

import com.qtasnim.eoffice.db.chat.Message;
import com.qtasnim.eoffice.services.chat.MessageService;
import com.qtasnim.eoffice.util.BeanUtil;

import javax.persistence.PrePersist;

public class ChatMessageListener {
    @PrePersist
    void onPersist(Object object) {
        if (object instanceof Message) {
            MessageService messageService = (MessageService) BeanUtil.getBean(MessageService.class);
            Message message = (Message) object;
            messageService.sendMessage(message.getFrom(), message.getTo(), message.getAddTime(), message.getMsg(), message.getChatId());
        }
    }
}
