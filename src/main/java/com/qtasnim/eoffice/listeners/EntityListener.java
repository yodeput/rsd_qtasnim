package com.qtasnim.eoffice.listeners;

import com.qtasnim.eoffice.db.ICreationAudit;
import com.qtasnim.eoffice.db.IModificationAudit;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.SessionContextAnnotationLiteral;
import com.qtasnim.eoffice.util.BeanUtil;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;
import java.util.Optional;

public class EntityListener {
    @PrePersist
    void onPersist(Object object) {
        if (object instanceof ICreationAudit || object instanceof IModificationAudit) {
            Session userSession;
            String username = "guest";
            Date now = new Date();

            try {
                userSession = (Session) BeanUtil.getBean(Session.class, new SessionContextAnnotationLiteral());
                username = Optional.ofNullable(userSession).filter(t -> t.getUserSession() != null).map(t -> {
                    try {
                        return t.getUserSession().getUser().getLoginUserName();
                    } catch (Exception e) {
                        return "guest";
                    }
                }).orElse("guest");
            } catch (Exception e) {}

            if (object instanceof ICreationAudit) {
                ICreationAudit entity = (ICreationAudit) object;
                entity.setCreatedBy(username);
                entity.setCreatedDate(now);
            }

            if (object instanceof IModificationAudit) {
                IModificationAudit entity = (IModificationAudit) object;
                entity.setModifiedBy(username);
                entity.setModifiedDate(now);
            }
        }
    }

    @PreUpdate
    void onUpdate(Object object) {
        if (object instanceof IModificationAudit) {
            Session userSession;
            IModificationAudit entity = (IModificationAudit) object;
            entity.setModifiedBy("guest");
            Date now = new Date();

            try {
                userSession = (Session) BeanUtil.getBean(Session.class, new SessionContextAnnotationLiteral());
                entity.setModifiedBy(Optional.ofNullable(userSession).filter(t -> t.getUserSession() != null).map(t -> {
                    try {
                        return t.getUserSession().getUser().getLoginUserName();
                    } catch (Exception e) {
                        return "guest";
                    }
                }).orElse("guest"));
            } catch (Exception e) {}


            entity.setModifiedDate(now);
        }
    }

}
