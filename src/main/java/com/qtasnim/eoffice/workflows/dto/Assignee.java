package com.qtasnim.eoffice.workflows.dto;

import lombok.Data;

@Data
public class Assignee {
    private String assignee;
    private String pelakhar;
    private String pymt;
}
