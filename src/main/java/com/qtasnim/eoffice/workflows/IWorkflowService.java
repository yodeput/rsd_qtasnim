package com.qtasnim.eoffice.workflows;

import com.qtasnim.eoffice.db.IWorkflowEntity;
import com.qtasnim.eoffice.db.ProcessInstance;
import com.qtasnim.eoffice.db.ProcessTask;
import com.qtasnim.eoffice.ws.dto.ProcessTaskDto;
import com.qtasnim.eoffice.ws.dto.TaskActionDto;
import com.qtasnim.eoffice.ws.dto.TaskActionResponseDto;

import java.util.List;
import java.util.Map;

public interface IWorkflowService<T> {
    void edit(T entity);

    IWorkflowEntity getEntity(ProcessTask processTask);

    boolean isTaskInvalid(TaskActionDto dto, ProcessTask processTask, Map<String, Object> userResponse) throws Exception;

    void onWorkflowCompleted(ProcessStatus status, ProcessInstance processInstance, IWorkflowEntity entity, IWorkflowProvider workflowProvider, String digisignUsername, String passphrase);

    void onTasksCreated(IWorkflowEntity entity, ProcessTask prevTask, List<ProcessTask> tasks);

    void onTaskExecuting(ProcessTask task);

    void onTaskExecuted(TaskActionDto dto, IWorkflowProvider workflowProvider, IWorkflowEntity entity, ProcessTask task, TaskEventHandler taskEventHandler, Map<String, Object> userResponse);

    List<ProcessTaskDto> onTaskHistoryRequested(List<ProcessTask> myTasks);

    void onTaskActionGenerated(ProcessTask processTask, TaskActionResponseDto response);

    void setMailProperties(IWorkflowEntity entity);

    void newTaskNotification(List<ProcessTask> processTasks, IWorkflowEntity entity, String docId);

    void newTaskNotification(ProcessTask processTask, IWorkflowEntity entity, String docId);

    void infoTaskNotification(ProcessTask processTask, IWorkflowEntity entity, String docId);
}
