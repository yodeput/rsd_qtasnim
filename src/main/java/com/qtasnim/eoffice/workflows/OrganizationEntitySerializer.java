package com.qtasnim.eoffice.workflows;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;

import java.io.IOException;

public class OrganizationEntitySerializer extends StdSerializer<MasterStrukturOrganisasi> {

    public OrganizationEntitySerializer() {
        this(null);
    }

    public OrganizationEntitySerializer(Class<MasterStrukturOrganisasi> t) {
        super(t);
    }

    @Override
    public void serialize(
            MasterStrukturOrganisasi masterOrganizationEntity, JsonGenerator jsonGenerator, SerializerProvider serializer) throws IOException {
        jsonGenerator.writeString(masterOrganizationEntity.getOrganizationCode());
    }
}