package com.qtasnim.eoffice.workflows;

import com.qtasnim.eoffice.InternalServerErrorException;

public class WorkflowException extends InternalServerErrorException {
    public WorkflowException(String message, String exceptionCode) {
        super(message, exceptionCode);
    }

    public WorkflowException(String message, String exceptionCode, Exception e) {
        super(message, exceptionCode, e);
    }
}
