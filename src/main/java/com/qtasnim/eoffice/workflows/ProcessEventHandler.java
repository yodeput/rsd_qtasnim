package com.qtasnim.eoffice.workflows;

import com.qtasnim.eoffice.db.ProcessInstance;
import com.qtasnim.eoffice.db.ProcessTask;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ProcessEventHandler {
    private Consumer<ProcessInstance> processCreated = s -> {};
    private BiConsumer<ProcessInstance, ProcessStatus> completed = (s,u) -> {};
    private BiConsumer<ProcessTask, List<ProcessTask>> tasksCreated = (s,u) -> {};

    public Consumer<ProcessInstance> onProcessCreated() {
        return processCreated;
    }

    public ProcessEventHandler onProcessCreated(Consumer<ProcessInstance> onProcessCreated) {
        this.processCreated = onProcessCreated;
        return this;
    }

    public BiConsumer<ProcessInstance, ProcessStatus> onCompleted() {
        return completed;
    }

    public ProcessEventHandler onCompleted(BiConsumer<ProcessInstance, ProcessStatus> completed) {
        this.completed = completed;
        return this;
    }
    public BiConsumer<ProcessTask, List<ProcessTask>> onTasksCreated() {
        return tasksCreated;
    }

    public ProcessEventHandler onTasksCreated(BiConsumer<ProcessTask, List<ProcessTask>> onTasksCreated) {
        this.tasksCreated = onTasksCreated;
        return this;
    }

    public static ProcessEventHandler DEFAULT = new ProcessEventHandler();
}
