package com.qtasnim.eoffice.workflows;

import com.qtasnim.eoffice.db.ProcessInstance;
import com.qtasnim.eoffice.db.ProcessTask;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class TaskEventHandler {
    private BiConsumer<ProcessInstance, ProcessStatus> completed = (s,u) -> {};
    private BiConsumer<ProcessTask, Map<String, Object>> taskExecuted = (s,u) -> {};
    private Consumer<ProcessTask> executing = s -> {};
    private BiConsumer<ProcessTask, List<ProcessTask>> tasksCreated = (s,u) -> {};
    private Consumer<ProcessInstance> processCreated = s -> {};

    public BiConsumer<ProcessInstance, ProcessStatus> onCompleted() {
        return completed;
    }
    public BiConsumer<ProcessTask, Map<String, Object>> onTaskExecuted() {
        return taskExecuted;
    }
    public Consumer<ProcessTask> onExecuting() {
        return executing;
    }
    public BiConsumer<ProcessTask, List<ProcessTask>> onTasksCreated() {
        return tasksCreated;
    }
    public Consumer<ProcessInstance> onProcessCreated() {
        return processCreated;
    }

    public TaskEventHandler onCompleted(BiConsumer<ProcessInstance, ProcessStatus> completed) {
        this.completed = completed;
        return this;
    }

    public TaskEventHandler onTaskExecuted(BiConsumer<ProcessTask, Map<String, Object>> onTaskExecuted) {
        this.taskExecuted = onTaskExecuted;
        return this;
    }

    public TaskEventHandler onExecuting(Consumer<ProcessTask> onExecuting) {
        this.executing = onExecuting;
        return this;
    }

    public TaskEventHandler onTasksCreated(BiConsumer<ProcessTask, List<ProcessTask>> onTasksCreated) {
        this.tasksCreated = onTasksCreated;
        return this;
    }

    public TaskEventHandler onProcessCreated(Consumer<ProcessInstance> onProcessCreated) {
        this.processCreated = onProcessCreated;
        return this;
    }

    public static TaskEventHandler DEFAULT = new TaskEventHandler();
}
