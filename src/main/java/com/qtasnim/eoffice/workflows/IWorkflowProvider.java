package com.qtasnim.eoffice.workflows;

import com.qtasnim.eoffice.db.IWorkflowEntity;
import com.qtasnim.eoffice.db.ProcessDefinition;
import com.qtasnim.eoffice.db.ProcessInstance;
import com.qtasnim.eoffice.db.ProcessTask;

import java.util.List;
import java.util.Map;

public interface IWorkflowProvider {
    ProcessDefinition deploy(ProcessDefinition processDefinition);

    void undeploy(String deploymentId);

    ProcessInstance startWorkflow(IWorkflowEntity entity, ProcessDefinition processDefinition, ProcessEventHandler eventHandler, Map<String, Object> props);

    List<ProcessTask> getActiveTasks(ProcessInstance processInstance);

    void terminateProcess(ProcessInstance processInstance);

    String getSubProcessName(ProcessDefinition processDefinition, String taskName);

    ProcessStatus getProcessStatus(IWorkflowEntity entity);

    void execute(ProcessTask prevTask, IWorkflowEntity entity, Map<String, Object> userResponse, TaskEventHandler eventHandler);
}
