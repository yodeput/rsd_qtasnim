package com.qtasnim.eoffice.workflows.resolvers;

import com.qtasnim.eoffice.db.IWorkflowEntity;
import com.qtasnim.eoffice.db.ProcessTask;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskContext {
    private IWorkflowEntity entity;
    private ProcessTask task;
    private String subProcess;
    private Map<String, Object> userResponse;
    private Map<String, Object> props;
}
