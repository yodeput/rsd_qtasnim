package com.qtasnim.eoffice.workflows.resolvers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.services.MasterDelegasiService;
import com.qtasnim.eoffice.services.MasterWorkflowProviderService;
import com.qtasnim.eoffice.services.PemeriksaPelakharPymtSuratService;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.dto.Assignee;
import com.qtasnim.eoffice.services.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class SuratPemeriksaResolver implements ITaskValueResolver {
    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private PemeriksaPelakharPymtSuratService pemeriksaPelakharPymtSuratService;

    @Inject
    private Logger logger;

    @Override
    public Object resolve(TaskContext context) {
        Surat surat = (Surat) context.getEntity();
        List<PemeriksaSurat> pemeriksaList = new ArrayList<>(surat.getPemeriksaSurat());
        List<MasterDelegasi> delegasiAll = masterDelegasiService.getByPejabatCodeList(pemeriksaList.stream().map(q->q.getOrganization().getOrganizationCode()).collect(Collectors.toList()));
        String approvalResponse = context
                .getUserResponse()
                .entrySet()
                .stream()
                .filter(t -> context.getTask() != null && t.getKey().equals(context.getTask().getResponseVar()))
                .findFirst()
                .map(t -> t.getValue().toString())
                .orElse(null);

        List<Assignee> assignees;

        if (Optional.ofNullable(surat.getIsPemeriksaParalel()).orElse(false)) {
            assignees = paralel(context, pemeriksaList, delegasiAll, approvalResponse);
        } else {
            assignees = serial(context, pemeriksaList, delegasiAll, approvalResponse);
        }

        try {
            return getObjectMapper().writeValueAsString(assignees);
        } catch (JsonProcessingException e) {
            logger.error(null, e);

            return "[]";
        }
    }

    private List<Assignee> paralel(TaskContext context, List<PemeriksaSurat> pemeriksaList, List<MasterDelegasi> delegasiAll, String approvalResponse) {
        if (approvalResponse != null) {
            String comment = getValue(context.getUserResponse(), "Comment").map(Object::toString).orElse(null);
            MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
            IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();

            boolean pemeriksaApproved = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && "Setujui".equals(approvalResponse);
            boolean pemeriksaRejected = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan");
            boolean pemeriksaRefresh = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && "[REFRESH]".equals(comment);
            boolean penandatanganApproved = "SubProcess_Penandatangan".equals(context.getSubProcess()) && "Setujui".equals(approvalResponse);
            boolean penandatanganRefresh = "SubProcess_Penandatangan".equals(context.getSubProcess()) && "[REFRESH]".equals(comment);
            boolean penandatanganRejected = "SubProcess_Penandatangan".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan ke Pemeriksa");
            boolean penandatanganRejectedToKonseptor = "SubProcess_Penandatangan".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan ke Konseptor");

            if (pemeriksaRefresh) {
                List<String> pendingAssignees = context.getTask()
                        .getProcessInstance()
                        .getTasks()
                        .stream()
                        .filter(t ->
                                (t.getExecutionStatus().equals(ExecutionStatus.PENDING) || t.getExecutionStatus().equals(ExecutionStatus.SKIP))
                                        && workflowProvider.getSubProcessName(context.getTask().getProcessInstance().getProcessDefinition(), t.getTaskName()).equals("SubProcess_Pemeriksa")
                        )
                        .map(t -> t.getAssignee().getOrganizationCode())
                        .collect(Collectors.toList());
                pemeriksaList = pemeriksaList
                        .stream()
                        .filter(t -> {
                            List<String> validAssignees = delegasiAll.stream().filter(d -> d.getFrom().getOrganizationCode().equals(t.getOrganization().getOrganizationCode())).map(v -> v.getTo().getOrganizationCode()).collect(Collectors.toList());
                            validAssignees.add(t.getOrganization().getOrganizationCode());

                            return pendingAssignees.stream().anyMatch(p -> validAssignees.contains(t.getOrganization().getOrganizationCode()));
                        })
                        .collect(Collectors.toList());
            } else {
                if (pemeriksaApproved) {
                    List<String> pendingAssignees = context.getTask()
                            .getProcessInstance()
                            .getTasks()
                            .stream()
                            .filter(t ->
                                    t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                            && workflowProvider.getSubProcessName(context.getTask().getProcessInstance().getProcessDefinition(), t.getTaskName()).equals("SubProcess_Pemeriksa")
                            )
                            .map(t -> t.getAssignee().getOrganizationCode())
                            .collect(Collectors.toList());
                    pemeriksaList = pemeriksaList
                            .stream()
                            .filter(t -> {
                                List<String> validAssignees = delegasiAll.stream().filter(d -> d.getFrom().getOrganizationCode().equals(t.getOrganization().getOrganizationCode())).map(v -> v.getTo().getOrganizationCode()).collect(Collectors.toList());
                                validAssignees.add(t.getOrganization().getOrganizationCode());

                                return (pendingAssignees.stream().anyMatch(p -> validAssignees.contains(t.getOrganization().getOrganizationCode())) && !validAssignees.contains(context.getTask().getAssignee().getOrganizationCode())) == true;
                            })
                            .collect(Collectors.toList());
                }

                if (pemeriksaRejected) {
                    pemeriksaList.clear();
                }

                if (penandatanganApproved || penandatanganRefresh || penandatanganRejectedToKonseptor) {
                    pemeriksaList.clear();
                }

                if (penandatanganRejected) {

                }
            }
        }

        return pemeriksaList.stream().map(t -> {
            Assignee assignee = new Assignee();
            assignee.setAssignee(formatAssignee(t.getOrganization().getOrganizationCode(), t.getId()));
            delegasiAll.stream().filter(d -> d.getFrom().getOrganizationCode().equals(t.getOrganization().getOrganizationCode())).forEach(d -> {
                if (d.getTipe().toLowerCase().equals("pelakhar")) {
                    assignee.setPelakhar(formatAssignee(d.getTo().getOrganizationCode(), t.getId()));
                } else if (d.getTipe().toLowerCase().equals("pymt")) {
                    assignee.setPymt(formatAssignee(d.getTo().getOrganizationCode(), t.getId()));
                }
            });

            return assignee;
        }).collect(Collectors.toList());
    }

    private List<Assignee> serial(TaskContext context, List<PemeriksaSurat> pemeriksaList, List<MasterDelegasi> delegasiAll, String approvalResponse) {
        if (approvalResponse != null) {
            Surat surat = (Surat) context.getEntity();
            String comment = getValue(context.getUserResponse(), "Comment").map(Object::toString).orElse(null);
            boolean pemeriksaApproved = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && "Setujui".equals(approvalResponse);
            boolean pemeriksaRefresh = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && "[REFRESH]".equals(comment);
            boolean pemeriksaRejected = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && approvalResponse.equals("Kembalikan");
            boolean pemeriksaRejectedToKonseptor = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan ke Konseptor");
            boolean penandatanganApproved = "SubProcess_Penandatangan".equals(context.getSubProcess()) && "Setujui".equals(approvalResponse);
            boolean penandatanganRefresh = "SubProcess_Penandatangan".equals(context.getSubProcess()) && "[REFRESH]".equals(comment);
            boolean penandatanganRejected = "SubProcess_Penandatangan".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan ke Pemeriksa");
            boolean penandatanganRejectedToKonseptor = "SubProcess_Penandatangan".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan ke Konseptor");

            if (pemeriksaRefresh) {
                pemeriksaList = pemeriksaList.stream().filter(t -> context.getTask().getSeq() != null && t.getSeq() >= context.getTask().getSeq()).collect(Collectors.toList());
            } else {
                if (pemeriksaApproved) {
                    pemeriksaList = pemeriksaList.stream().filter(t -> t.getSeq() > context.getTask().getSeq()).collect(Collectors.toList());
                }

                if (pemeriksaRejected) {
                    Integer rejectTo = context.getTask().getSeq() - 1;

                    if (rejectTo < 0) {
                        pemeriksaList.clear();
                    } else {
                        pemeriksaList = pemeriksaList.stream().filter(t -> t.getSeq() >= rejectTo).collect(Collectors.toList());
                    }
                }

                if (penandatanganApproved) {
                    pemeriksaList.clear();
                }

                if (penandatanganRejected) {
                    if (approvalResponse.contains("|")) {
                        int targetSeq = Integer.parseInt(approvalResponse.split("\\|")[1]);
                        boolean isSeqValid = pemeriksaList.stream().anyMatch(t -> t.getSeq().equals(targetSeq));

                        if (isSeqValid) {
                            pemeriksaList = pemeriksaList.stream().filter(t -> t.getSeq() >= targetSeq).collect(Collectors.toList());
                        }
                    } else {
                        pemeriksaList = pemeriksaList
                                .stream()
                                .max(Comparator.comparing(PemeriksaSurat::getSeq))
                                .map(t -> new ArrayList<>(Arrays.asList(t)))
                                .orElse(new ArrayList<>());
                    }

                    List<PemeriksaPelakharPymtSurat> assignees = pemeriksaPelakharPymtSuratService.findBySurat(surat.getId());
                    assignees.forEach(t -> pemeriksaPelakharPymtSuratService.remove(t));
                }

                if (penandatanganRejectedToKonseptor || penandatanganRefresh || pemeriksaRejectedToKonseptor) {
                    pemeriksaList.clear();
                }
            }
        }

        return pemeriksaList.stream()
                .collect(Collectors.groupingBy(PemeriksaSurat::getSeq, Collectors.toList()))
                .entrySet()
                .stream().min(Comparator.comparing(Map.Entry::getKey))
                .map(Map.Entry::getValue)
                .orElse(new ArrayList<>())
                .stream()
                .map(t -> {
                    Assignee assignee = new Assignee();
                    assignee.setAssignee(formatAssigneeAsSeq(t.getOrganization().getOrganizationCode(), t.getSeq(), t.getId()));
                    delegasiAll.stream().filter(d -> d.getFrom().getOrganizationCode().equals(t.getOrganization().getOrganizationCode())).forEach(d -> {
                        if (d.getTipe().toLowerCase().equals("pelakhar")) {
                            assignee.setPelakhar(formatAssigneeAsSeq(d.getTo().getOrganizationCode(), t.getSeq(), t.getId()));
                        } else if (d.getTipe().toLowerCase().equals("pymt")) {
                            assignee.setPymt(formatAssigneeAsSeq(d.getTo().getOrganizationCode(), t.getSeq(), t.getId()));
                        }
                    });

                    return assignee;
                }).collect(Collectors.toList());
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    private Optional<Object> getValue(Map<String, Object> map, String key) {
        return map.entrySet().stream().filter(t -> t.getKey().equals(key)).findFirst().map(Map.Entry::getValue);
    }

    private String formatAssignee(String assignee, Long relatedAssigneeRecordId) {
        return String.format("%s#%s", assignee, relatedAssigneeRecordId);
    }

    private String formatAssigneeAsSeq(String assignee, Integer seq, Long relatedAssigneeRecordId) {
        return String.format("%s|%s#%s", assignee, seq, relatedAssigneeRecordId);
    }
}
