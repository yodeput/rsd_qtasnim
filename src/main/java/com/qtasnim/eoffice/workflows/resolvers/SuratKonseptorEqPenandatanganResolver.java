package com.qtasnim.eoffice.workflows.resolvers;

import com.qtasnim.eoffice.db.Surat;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class SuratKonseptorEqPenandatanganResolver implements ITaskValueResolver {
    @Override
    public Object resolve(TaskContext context) {
        Surat surat = (Surat) context.getEntity();

        return surat.getKonseptor() != null && surat.getPenandatanganSurat().stream().anyMatch(t -> t.getOrganization().getOrganizationCode().equals(surat.getKonseptor().getOrganizationCode()));
    }
}
