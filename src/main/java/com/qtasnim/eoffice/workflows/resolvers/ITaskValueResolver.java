package com.qtasnim.eoffice.workflows.resolvers;

public interface ITaskValueResolver {
    Object resolve(TaskContext context);
}
