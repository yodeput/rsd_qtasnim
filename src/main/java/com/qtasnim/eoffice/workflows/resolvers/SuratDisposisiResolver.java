package com.qtasnim.eoffice.workflows.resolvers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.db.SuratDisposisi;
import com.qtasnim.eoffice.services.SuratDisposisiService;
import com.qtasnim.eoffice.services.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class SuratDisposisiResolver implements ITaskValueResolver {
    @Inject
    private Logger logger;

    @Inject
    private SuratDisposisiService suratDisposisiService;

    @Override
    public Object resolve(TaskContext context) {
        Surat surat = (Surat) context.getEntity();
        Long parentId = context
                .getProps()
                .entrySet()
                .stream()
                .filter(t -> t.getKey().equals("parent") && t.getValue() != null && t.getValue() instanceof SuratDisposisi)
                .map(t -> ((SuratDisposisi) t.getValue()).getId())
                .findFirst()
                .orElse(null);
        List<String> orgs = suratDisposisiService.findPendingByParentId(surat.getId(), parentId)
                .stream()
                .map(t -> t.getOrganization().getOrganizationCode())
                .distinct()
                .collect(Collectors.toList());

        try {
            return getObjectMapper().writeValueAsString(orgs);
        } catch (JsonProcessingException e) {
            logger.error(null, e);

            return "[]";
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
