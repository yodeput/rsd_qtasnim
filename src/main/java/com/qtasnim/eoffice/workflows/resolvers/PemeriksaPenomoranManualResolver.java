package com.qtasnim.eoffice.workflows.resolvers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.services.MasterDelegasiService;
import com.qtasnim.eoffice.services.MasterWorkflowProviderService;
import com.qtasnim.eoffice.workflows.dto.Assignee;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class PemeriksaPenomoranManualResolver implements ITaskValueResolver {
    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private Logger logger;

    @Override
    public Object resolve(TaskContext context) {
        PenomoranManual obj = (PenomoranManual) context.getEntity();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        List<PemeriksaNomorManual> pemeriksaList = obj.getPemeriksa()
                .stream()
                .peek(t -> {
                    if (t.getSeq() != null) {
                        atomicInteger.set(t.getSeq());
                    }

                    t.setSeq(atomicInteger.getAndIncrement());
                })
                .collect(Collectors.toList());
        List<MasterDelegasi> delegasiAll = masterDelegasiService.getByPejabatCodeList(pemeriksaList.stream().map(q->q.getOrganization().getOrganizationCode()).collect(Collectors.toList()));
        String approvalResponse = context
                .getUserResponse()
                .entrySet()
                .stream()
                .filter(t -> context.getTask() != null && t.getKey().equals(context.getTask().getResponseVar()))
                .findFirst()
                .map(t -> t.getValue().toString())
                .orElse(null);

        List<Assignee> assignees = serial(context, pemeriksaList, delegasiAll, approvalResponse);

        try {
            return getObjectMapper().writeValueAsString(assignees);
        } catch (JsonProcessingException e) {
            logger.error(null, e);

            return "[]";
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    private Optional<Object> getValue(Map<String, Object> map, String key) {
        return map.entrySet().stream().filter(t -> t.getKey().equals(key)).findFirst().map(Map.Entry::getValue);
    }

    private String formatAssigneeAsSeq(String assignee, Integer seq, Long relatedAssigneeRecordId) {
        return String.format("%s|%s#%s", assignee, seq, relatedAssigneeRecordId);
    }

    private List<Assignee> serial(TaskContext context, List<PemeriksaNomorManual> pemeriksaList, List<MasterDelegasi> delegasiAll, String approvalResponse) {
        if (approvalResponse != null) {
            PenomoranManual surat = (PenomoranManual) context.getEntity();
            String comment = getValue(context.getUserResponse(), "Comment").map(Object::toString).orElse(null);
            boolean pemeriksaApproved = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && "Setujui".equals(approvalResponse);
            boolean pemeriksaRefresh = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && "[REFRESH]".equals(comment);
            boolean pemeriksaRejected = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && approvalResponse.equals("Kembalikan");
            boolean pemeriksaRejectedToKonseptor = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan ke Konseptor");

            if (pemeriksaRefresh) {
                pemeriksaList = pemeriksaList.stream().filter(t -> context.getTask().getSeq() != null && t.getSeq() >= context.getTask().getSeq()).collect(Collectors.toList());
            } else {
                if (pemeriksaApproved) {
                    pemeriksaList = pemeriksaList.stream().filter(t -> t.getSeq() > context.getTask().getSeq()).collect(Collectors.toList());
                }

                if (pemeriksaRejected) {
                    Integer rejectTo = context.getTask().getSeq() - 1;

                    if (rejectTo < 0) {
                        pemeriksaList.clear();
                    } else {
                        pemeriksaList = pemeriksaList.stream().filter(t -> t.getSeq() >= rejectTo).collect(Collectors.toList());
                    }
                }

                if (pemeriksaRejectedToKonseptor) {
                    pemeriksaList.clear();
                }
            }
        }

        return pemeriksaList.stream()
                .collect(Collectors.groupingBy(PemeriksaNomorManual::getSeq, Collectors.toList()))
                .entrySet()
                .stream().min(Comparator.comparing(Map.Entry::getKey))
                .map(Map.Entry::getValue)
                .orElse(new ArrayList<>())
                .stream()
                .map(t -> {
                    Assignee assignee = new Assignee();
                    assignee.setAssignee(formatAssigneeAsSeq(t.getOrganization().getOrganizationCode(), t.getSeq(), t.getId()));
                    delegasiAll.stream().filter(d -> d.getFrom().getOrganizationCode().equals(t.getOrganization().getOrganizationCode())).forEach(d -> {
                        if (d.getTipe().toLowerCase().equals("pelakhar")) {
                            assignee.setPelakhar(formatAssigneeAsSeq(d.getTo().getOrganizationCode(), t.getSeq(), t.getId()));
                        } else if (d.getTipe().toLowerCase().equals("pymt")) {
                            assignee.setPymt(formatAssigneeAsSeq(d.getTo().getOrganizationCode(), t.getSeq(), t.getId()));
                        }
                    });

                    return assignee;
                }).collect(Collectors.toList());
    }

}
