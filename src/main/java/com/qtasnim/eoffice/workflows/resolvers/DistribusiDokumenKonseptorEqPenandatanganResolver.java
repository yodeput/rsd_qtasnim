package com.qtasnim.eoffice.workflows.resolvers;

import com.qtasnim.eoffice.db.DistribusiDokumen;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.services.MasterDelegasiService;
import com.qtasnim.eoffice.services.MasterUserService;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class DistribusiDokumenKonseptorEqPenandatanganResolver implements ITaskValueResolver {

    @Inject
    private MasterUserService userService;

    @Override
    public Object resolve(TaskContext context) {
        DistribusiDokumen surat = (DistribusiDokumen) context.getEntity();
        MasterUser konseptor = userService.findUserByUsername(surat.getCreatedBy().trim());

        if (konseptor == null) return false;

        return surat.getPenandatangan().stream().anyMatch(t -> t.getOrganization().getOrganizationCode().equals(konseptor.getOrganizationEntity().getOrganizationCode()));
    }
}
