package com.qtasnim.eoffice.workflows.resolvers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.services.MasterDelegasiService;
import com.qtasnim.eoffice.services.MasterWorkflowProviderService;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.dto.Assignee;
import com.qtasnim.eoffice.services.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class SuratPenandatanganResolver implements ITaskValueResolver {
    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private Logger logger;

    @Override
    public Object resolve(TaskContext context) {
        Surat surat = (Surat) context.getEntity();

        List<PenandatanganSurat> penandatanganList = new ArrayList<>(surat.getPenandatanganSurat());
        List<MasterDelegasi> delegasiAll = masterDelegasiService.getByPejabatCodeList(penandatanganList.stream().map(q->q.getOrganization().getOrganizationCode()).collect(Collectors.toList()));
        String approvalResponse = context
                .getUserResponse()
                .entrySet()
                .stream()
                .filter(t -> context.getTask() != null && t.getKey().equals(context.getTask().getResponseVar()))
                .findFirst()
                .map(t -> t.getValue().toString())
                .orElse(null);

        boolean noPemeriksa = surat.getPemeriksaSurat() == null || surat.getPemeriksaSurat().size() == 0;

        if (noPemeriksa) {
            approvalResponse = "Setujui";
        }

        if (approvalResponse != null) {
            MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
            IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();
            String comment = getValue(context.getUserResponse(), "Comment").map(Object::toString).orElse(null);

            boolean pemeriksaApproved = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && "Setujui".equals(approvalResponse);
            boolean pemeriksaRefresh = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && "[REFRESH]".equals(comment);
            boolean pemeriksaRejected = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && approvalResponse.equals("Kembalikan");
            boolean pemeriksaRejectedToKonseptor = "SubProcess_Pemeriksa".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan ke Konseptor");
            boolean penandatanganApproved = "SubProcess_Penandatangan".equals(context.getSubProcess()) && "Setujui".equals(approvalResponse);
            boolean penandatanganRefresh = "SubProcess_Penandatangan".equals(context.getSubProcess()) && "[REFRESH]".equals(comment);
            boolean penandatanganRejected = "SubProcess_Penandatangan".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan ke Pemeriksa");
            boolean penandatanganRejectedToKonseptor = "SubProcess_Penandatangan".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan ke Konseptor");

            if (penandatanganRefresh) {
                //Remove assignee dari daftar assignes setiap kali task di execute
                List<String> pendingAssignees = context.getTask()
                        .getProcessInstance()
                        .getTasks()
                        .stream()
                        .filter(t ->
                                (t.getExecutionStatus().equals(ExecutionStatus.PENDING) || t.getExecutionStatus().equals(ExecutionStatus.SKIP))
                                && workflowProvider.getSubProcessName(context.getTask().getProcessInstance().getProcessDefinition(), t.getTaskName()).equals("SubProcess_Penandatangan")
                        )
                        .map(t -> t.getAssignee().getOrganizationCode())
                        .collect(Collectors.toList());

                penandatanganList = penandatanganList
                        .stream()
                        .filter(t -> {
                            List<String> validAssignees = delegasiAll.stream().filter(d -> d.getFrom().getOrganizationCode().equals(t.getOrganization().getOrganizationCode())).map(v -> v.getTo().getOrganizationCode()).collect(Collectors.toList());
                            validAssignees.add(t.getOrganization().getOrganizationCode());

                            return pendingAssignees.stream().anyMatch(p -> validAssignees.contains(t.getOrganization().getOrganizationCode()));
                        })
                        .collect(Collectors.toList());
            } else {
                if (pemeriksaApproved) {
                    boolean allPemeriksaApproved = context.getTask()
                            .getProcessInstance()
                            .getTasks()
                            .stream()
                            .noneMatch(t ->
                                    t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                            && workflowProvider.getSubProcessName(context.getTask().getProcessInstance().getProcessDefinition(), t.getTaskName()).equals("SubProcess_Pemeriksa")
                                            && !t.getTaskId().equals(context.getTask().getTaskId()))
                            || noPemeriksa;

                    if (allPemeriksaApproved) {
                        //TODO: Serial Approver Reject handler
                    } else {
                        penandatanganList.clear();
                    }
                }

                if (penandatanganApproved) {
                    //Remove assignee dari daftar assignes setiap kali task di execute
                    List<String> pendingAssignees = context.getTask()
                            .getProcessInstance()
                            .getTasks()
                            .stream()
                            .filter(t ->
                                    t.getExecutionStatus().equals(ExecutionStatus.PENDING)
                                            && workflowProvider.getSubProcessName(context.getTask().getProcessInstance().getProcessDefinition(), t.getTaskName()).equals("SubProcess_Penandatangan")
                            )
                            .map(t -> t.getAssignee().getOrganizationCode())
                            .collect(Collectors.toList());


                    penandatanganList = penandatanganList
                            .stream()
                            .filter(t -> {
                                List<String> validAssignees = delegasiAll.stream().filter(d -> d.getFrom().getOrganizationCode().equals(t.getOrganization().getOrganizationCode())).map(v -> v.getTo().getOrganizationCode()).collect(Collectors.toList());
                                validAssignees.add(t.getOrganization().getOrganizationCode());

                                return (pendingAssignees.stream().anyMatch(p -> validAssignees.contains(t.getOrganization().getOrganizationCode())) && !validAssignees.contains(context.getTask().getAssignee().getOrganizationCode())) == true;
                            })
                            .collect(Collectors.toList());
                }

                if (pemeriksaRejected || pemeriksaRefresh || pemeriksaRejectedToKonseptor || penandatanganRejected || penandatanganRejectedToKonseptor) {
                    penandatanganList.clear();
                }
            }
        } else {
            penandatanganList.clear();
        }

        List<Assignee> assignees = penandatanganList.stream().map(t -> {
            Assignee assignee = new Assignee();
            assignee.setAssignee(formatAssignee(t.getOrganization().getOrganizationCode(), t.getId()));
            delegasiAll.stream().filter(d -> d.getFrom().getOrganizationCode().equals(t.getOrganization().getOrganizationCode())).forEach(d -> {
                if (d.getTipe().toLowerCase().equals("pelakhar")) {
                    assignee.setPelakhar(formatAssignee(d.getTo().getOrganizationCode(), t.getId()));
                } else if (d.getTipe().toLowerCase().equals("pymt")) {
                    assignee.setPymt(formatAssignee(d.getTo().getOrganizationCode(), t.getId()));
                }
            });

            return assignee;
        }).collect(Collectors.toList());

        try {
            return getObjectMapper().writeValueAsString(assignees);
        } catch (JsonProcessingException e) {
            logger.error(null, e);

            return "[]";
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    private Optional<Object> getValue(Map<String, Object> map, String key) {
        return map.entrySet().stream().filter(t -> t.getKey().equals(key)).findFirst().map(Map.Entry::getValue);
    }

    private String formatAssignee(String assignee, Long relatedAssigneeRecordId) {
        return String.format("%s#%s", assignee, relatedAssigneeRecordId);
    }
}
