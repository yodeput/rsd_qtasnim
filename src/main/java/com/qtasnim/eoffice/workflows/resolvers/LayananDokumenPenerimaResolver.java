package com.qtasnim.eoffice.workflows.resolvers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.services.MasterDelegasiService;
import com.qtasnim.eoffice.services.MasterWorkflowProviderService;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.dto.Assignee;
import com.qtasnim.eoffice.services.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class LayananDokumenPenerimaResolver implements ITaskValueResolver {
    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private Logger logger;

    @Override
    public Object resolve(TaskContext context) {
        PermohonanDokumen surat = (PermohonanDokumen) context.getEntity();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        List<PermohonanDokumenPenerima> penerimaList = surat.getPenerima()
                .stream()
                .peek(t -> {
                    if (t.getSeq() != null) {
                        atomicInteger.set(t.getSeq());
                    }

                    t.setSeq(atomicInteger.getAndIncrement());
                })
                .collect(Collectors.toList());
        List<MasterDelegasi> delegasiAll = masterDelegasiService.getByPejabatCodeList(penerimaList.stream().map(q->q.getOrganization().getOrganizationCode()).collect(Collectors.toList()));
        String approvalResponse = context
                .getUserResponse()
                .entrySet()
                .stream()
                .filter(t -> context.getTask() != null && t.getKey().equals(context.getTask().getResponseVar()))
                .findFirst()
                .map(t -> t.getValue().toString())
                .orElse(null);

        if (approvalResponse != null) {
            String comment = getValue(context.getUserResponse(), "Comment").map(Object::toString).orElse(null);
            boolean penerimaApproved = "SubProcess_Penerima".equals(context.getSubProcess()) && "Setujui".equals(approvalResponse);
            boolean penerimaRefresh = "SubProcess_Penerima".equals(context.getSubProcess()) && "[REFRESH]".equals(comment);
            boolean penerimaRejected = "SubProcess_Penerima".equals(context.getSubProcess()) && approvalResponse.equals("Kembalikan");
            boolean penerimaRejectedToKonseptor = "SubProcess_Penerima".equals(context.getSubProcess()) && approvalResponse.contains("Kembalikan ke Konseptor");

            if (penerimaRefresh) {
                penerimaList = penerimaList.stream().filter(t -> context.getTask().getSeq() != null && t.getSeq() >= context.getTask().getSeq()).collect(Collectors.toList());
            } else {
                if (penerimaApproved) {
                    penerimaList = penerimaList.stream().filter(t -> t.getSeq() > context.getTask().getSeq()).collect(Collectors.toList());
                }

                if (penerimaRejected) {
                    Integer rejectTo = context.getTask().getSeq() - 1;

                    if (rejectTo < 0) {
                        penerimaList.clear();
                    } else {
                        penerimaList = penerimaList.stream().filter(t -> t.getSeq() >= rejectTo).collect(Collectors.toList());
                    }
                }

                if (penerimaRejectedToKonseptor) {
                    penerimaList.clear();
                }
            }
        }

        List<Assignee> assignees = penerimaList.stream()
                .collect(Collectors.groupingBy(PermohonanDokumenPenerima::getSeq, Collectors.toList()))
                .entrySet()
                .stream().min(Comparator.comparing(Map.Entry::getKey))
                .map(Map.Entry::getValue)
                .orElse(new ArrayList<>())
                .stream()
                .map(t -> {
                    Assignee assignee = new Assignee();
                    assignee.setAssignee(formatAssigneeAsSeq(t.getOrganization().getOrganizationCode(), t.getSeq(), t.getId()));
                    delegasiAll.stream().filter(d -> d.getFrom().getOrganizationCode().equals(t.getOrganization().getOrganizationCode())).forEach(d -> {
                        if (d.getTipe().toLowerCase().equals("pelakhar")) {
                            assignee.setPelakhar(formatAssigneeAsSeq(d.getTo().getOrganizationCode(), t.getSeq(), t.getId()));
                        } else if (d.getTipe().toLowerCase().equals("pymt")) {
                            assignee.setPymt(formatAssigneeAsSeq(d.getTo().getOrganizationCode(), t.getSeq(), t.getId()));
                        }
                    });

                    return assignee;
                }).collect(Collectors.toList());

        try {
            return getObjectMapper().writeValueAsString(assignees);
        } catch (JsonProcessingException e) {
            logger.error(null, e);

            return "[]";
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    private Optional<Object> getValue(Map<String, Object> map, String key) {
        return map.entrySet().stream().filter(t -> t.getKey().equals(key)).findFirst().map(Map.Entry::getValue);
    }

    private String formatAssigneeAsSeq(String assignee, Integer seq, Long relatedAssigneeRecordId) {
        return String.format("%s|%s#%s", assignee, seq, relatedAssigneeRecordId);
    }
}
