package com.qtasnim.eoffice.workflows.camunda;

public class CamundaConstants {
    public static final String PATH_CREATE = "create";
    public static final String PATH_DEPLOYMENT = "deployment";
    public static final String PATH_PROCESSDEFINITION = "process-definition";
    public static final String PATH_SUBMITFORM = "submit-form";

    public static final String CAMUNDA_RESTURL = "CAMUNDA_RESTURL";
    public static final String CAMUNDA_TENANTID = "CAMUNDA_TENANTID";
    public static final String CAMUNDA_SOURCE = "CAMUNDA_SOURCE";
}
