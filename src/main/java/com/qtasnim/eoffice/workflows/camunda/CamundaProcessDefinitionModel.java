package com.qtasnim.eoffice.workflows.camunda;

import com.qtasnim.eoffice.Bind;
import com.qtasnim.eoffice.db.ProcessDefinitionVar;
import com.qtasnim.workflows.camunda.dto.repository.ProcessDefinitionDto;
import lombok.Data;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Data
public class CamundaProcessDefinitionModel
{
    @Bind
    private String deploymentId;
    @Bind
    private String definitionId;
    @Bind
    private String key;
    @Bind
    private String category;
    @Bind
    private String description;
    @Bind
    private String name;
    @Bind
    private String resource;
    @Bind
    private String diagram;
    @Bind
    private String tenantId;
    @Bind
    private String versionTag;
    @Bind
    private Integer version;
    @Bind
    private Integer historyTimeToLive;
    @Bind
    private Boolean suspended;
    @Bind
    private Boolean startableInTasklist;

    CamundaProcessDefinitionModel() {

    }

    CamundaProcessDefinitionModel(ProcessDefinitionDto dto) {
        if (dto != null) {
            definitionId = dto.getId();
            deploymentId = dto.getDeploymentId();
            key = dto.getKey();
            category = dto.getCategory();
            description = dto.getDescription();
            name = dto.getName();
            version = dto.getVersion();
            resource = dto.getResource();
            diagram = dto.getDiagram();
            suspended = dto.isSuspended();
            tenantId = dto.getTenantId();
            versionTag = dto.getVersionTag();
            historyTimeToLive = dto.getHistoryTimeToLive();
            startableInTasklist = dto.isStartableInTasklist();
        }
    }

    public List<ProcessDefinitionVar> toDb(List<ProcessDefinitionVar> existing) {
        if (existing == null) {
            existing = new ArrayList<>();
        }

        List<ProcessDefinitionVar> result = new ArrayList<>();
        Map<String, ProcessDefinitionVar> map = existing.stream().collect(Collectors.toMap(ProcessDefinitionVar::getName, item -> item));
        Arrays.stream(getClass().getDeclaredFields()).filter(t -> t.isAnnotationPresent(Bind.class)).forEach(t -> {
            try {
                String fieldName = t.getName();
                Field field = getClass().getDeclaredField(fieldName);
                Object fieldValue = field.get(this);
                String fieldType = field.getType().getName();
                ProcessDefinitionVar var;

                if (map.containsKey(fieldName)) {
                    var = map.get(fieldName);
                } else {
                    var = new ProcessDefinitionVar();
                    map.put(fieldName, var);
                }

                var.setName(fieldName);
                var.setValue(Optional.ofNullable(fieldValue).map(Object::toString).orElse(null));
                var.setType(fieldType);

                result.add(var);
            } catch (Exception e) {

            }
        });

        return result;
    }

    public static CamundaProcessDefinitionModel toModel(List<ProcessDefinitionVar> existing) {
        Map<String, Field> map = Arrays.stream(CamundaProcessDefinitionModel.class.getDeclaredFields()).filter(t -> t.isAnnotationPresent(Bind.class)).collect(Collectors.toMap(Field::getName, item -> item));
        CamundaProcessDefinitionModel model = new CamundaProcessDefinitionModel();

        existing.parallelStream().forEach(t -> {
            try {
                Class<?> clazz = Class.forName(t.getType());

                if (map.containsKey(t.getName()) && t.getValue() != null) {
                    Field field = map.get(t.getName());

                    switch (t.getType()) {
                        case "java.lang.Integer": {
                            field.set(model, Integer.valueOf(t.getValue()));
                            break;
                        }
                        case "java.lang.Short": {
                            field.set(model, Short.valueOf(t.getValue()));
                            break;
                        }
                        case "java.lang.Double": {
                            field.set(model, Double.valueOf(t.getValue()));
                            break;
                        }
                        case "java.lang.Boolean": {
                            field.set(model, Boolean.valueOf(t.getValue()));
                            break;
                        }
                        case "java.lang.Long": {
                            field.set(model, Long.valueOf(t.getValue()));
                            break;
                        }
                        case "java.lang.String": {
                            field.set(model, t.getValue());
                            break;
                        }
                        default:
                            break;
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        return model;
    }
}
