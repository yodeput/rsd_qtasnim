package com.qtasnim.eoffice.workflows.camunda;


import com.qtasnim.eoffice.db.MasterWorkflowProvider;
import com.qtasnim.eoffice.db.MasterWorkflowProviderConfig;
import com.qtasnim.eoffice.services.MasterWorkflowProviderService;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

@Stateless
public class CamundaConfigProducer {
    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Produces
    public CamundaConfig produceLogger(InjectionPoint injectionPoint) {
        MasterWorkflowProvider camunda = masterWorkflowProviderService.getByProviderName("camunda");

        if (camunda != null) {
            CamundaConfig result = new CamundaConfig();

            for (MasterWorkflowProviderConfig config: camunda.getConfig()) {
                switch (config.getName()) {
                    case CamundaConstants
                            .CAMUNDA_RESTURL: {
                        result.setRestUrl(config.getValue());
                        break;
                    }
                    case CamundaConstants
                            .CAMUNDA_TENANTID: {
                        result.setTenantId(config.getValue());
                        break;
                    }
                    case CamundaConstants
                            .CAMUNDA_SOURCE: {
                        result.setSource(config.getValue());
                        break;
                    }
                }
            }

            return result;
        }

        return null;
    }
}
