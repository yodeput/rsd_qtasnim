package com.qtasnim.eoffice.workflows.camunda;

import lombok.Data;

import javax.enterprise.inject.Vetoed;

@Data
@Vetoed
public class CamundaConfig {
    private String restUrl;
    private String tenantId;
    private String source;
}
