package com.qtasnim.eoffice.workflows.camunda;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.BeanUtil;
import com.qtasnim.eoffice.util.ClassUtil;
import com.qtasnim.eoffice.workflows.*;
import com.qtasnim.eoffice.workflows.resolvers.DefaultValueResolver;
import com.qtasnim.eoffice.workflows.resolvers.ITaskValueResolver;
import com.qtasnim.eoffice.workflows.resolvers.TaskContext;
import com.qtasnim.workflows.camunda.dto.PatchVariablesDto;
import com.qtasnim.workflows.camunda.dto.VariableValueDto;
import com.qtasnim.workflows.camunda.dto.history.HistoricVariableInstanceDto;
import com.qtasnim.workflows.camunda.dto.repository.DeploymentWithDefinitionsDto;
import com.qtasnim.workflows.camunda.dto.repository.ProcessDefinitionDto;
import com.qtasnim.workflows.camunda.dto.runtime.ProcessInstanceDto;
import com.qtasnim.workflows.camunda.dto.runtime.StartProcessInstanceDto;
import com.qtasnim.workflows.camunda.dto.task.CompleteTaskDto;
import com.qtasnim.workflows.camunda.dto.task.TaskDto;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.ExtensionElements;
import org.camunda.bpm.model.bpmn.instance.SubProcess;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaFormData;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaFormField;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class CamundaProvider implements IWorkflowProvider {
    @Inject
    private CamundaConfig camundaConfig;

    @Inject
    private Logger logger;

    @Inject
    private ProcessDefinitionService processDefinitionService;

    @Inject
    private ProcessDefinitionVarService processDefinitionVarService;

    @Inject
    private ProcessInstanceService processInstanceService;

    @Inject
    private ProcessTaskService processTaskService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private PemeriksaSuratService pemeriksaSuratService;

    @Inject
    private PenandatanganSuratService penandatanganSuratService;

    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Override
    public ProcessDefinition deploy(ProcessDefinition processDefinition) throws WorkflowException {
        try {
            try {
                CamundaProcessDefinitionModel model;

                String filename = processDefinition.getDefinitionName() + ".bpmn";
                StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart();
                streamDataBodyPart.setName("*");
                streamDataBodyPart.setFilename(filename);

                try (InputStream streamEntity = new ByteArrayInputStream(processDefinition.getDefinition().getBytes())) {
                    streamDataBodyPart.setStreamEntity(streamEntity, MediaType.APPLICATION_OCTET_STREAM_TYPE);
                }

                try (FormDataMultiPart formDataMultiPart = new FormDataMultiPart()) {
                    try (FormDataMultiPart multipart = (FormDataMultiPart) formDataMultiPart
                            .field("deployment-name", processDefinition.getDefinitionName())
                            .field("enable-duplicate-filtering", "true")
                            .field("deploy-changed-only", "false")
                            .field("deployment-source", camundaConfig.getSource())
                            .field("tenant-id", camundaConfig.getTenantId())
                            .bodyPart(streamDataBodyPart)) {
                        streamDataBodyPart.setContentDisposition(FormDataContentDisposition.name("file").fileName(filename).build());

                        WebTarget webTarget = getClient()
                                .target(UriBuilder.fromUri(camundaConfig.getRestUrl())
                                        .path(CamundaConstants.PATH_DEPLOYMENT)
                                        .path(CamundaConstants.PATH_CREATE).toString());
                        Response response = webTarget
                                .request(MediaType.MULTIPART_FORM_DATA_TYPE)
                                .accept(MediaType.APPLICATION_JSON_TYPE)
                                .post(Entity.entity(multipart, multipart.getMediaType()));

                        if (response.getStatus() != 200) {
                            throw new WorkflowException(String.format("%s: %s \n%s", "Unexpected http result", response.getStatus(), response.readEntity(String.class)), "QT-ERR-CMDA-02");
                        }

                        String content = response.readEntity(String.class);
                        DeploymentWithDefinitionsDto newDeployment = getObjectMapper().readValue(content, DeploymentWithDefinitionsDto.class);

                        ProcessDefinitionDto processDefinitionDto = newDeployment.getDeployedProcessDefinitions()
                                .entrySet()
                                .stream()
                                .map(Map.Entry::getValue)
                                .findFirst()
                                .orElse(null);

                        if (processDefinitionDto == null) {
                            throw new WorkflowException("Invalid camunda dto return object", "QT-ERR-CMDA-03");
                        }

                        model = new CamundaProcessDefinitionModel(processDefinitionDto);

                        processDefinition.setDefinitionId(model.getDefinitionId());
                        processDefinition.setDeploymentId(model.getDeploymentId());
                        List<ProcessDefinitionVar> processDefinitionVars = model.toDb(processDefinition.getVars());

                        processDefinitionVars.forEach(t -> {
                            if (t.getId() != null) {
                                processDefinitionVarService.edit(t);
                            } else {
                                t.setProcessDefinition(processDefinition);
                                processDefinitionVarService.create(t);
                            }
                        });

                        processDefinition.setVars(processDefinitionVars);

                        processDefinitionService.edit(processDefinition);

                        return processDefinition;
                    }
                }
            } catch (Exception e) {
                throw new WorkflowException("Error when deploying bpmn to Workflow Server", "QT-ERR-CMDA-01", e);
            }
        } catch (WorkflowException e) {
            logger.error(Optional.ofNullable(e.getCause()).map(Throwable::getMessage).orElse(e.getMessage()), e);
            throw e;
        }
    }

    @Override
    public void undeploy(String deploymentId) throws InternalServerErrorException {
        WebTarget webResource = getClient()
                .target(
                        UriBuilder.fromUri(camundaConfig.getRestUrl())
                                .path(CamundaConstants.PATH_DEPLOYMENT)
                                .path(deploymentId)
                                .queryParam("cascade", true).toString());
        Response response = webResource.request(MediaType.MULTIPART_FORM_DATA_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).delete();

        if (response.getStatus() != 204) {
            throw new UnexpectedHttpResultException("Expected http result for delete deployment is 204");
        }
    }

    @Override
    public ProcessInstance startWorkflow(IWorkflowEntity entity, ProcessDefinition processDefinition, ProcessEventHandler eventHandler, Map<String, Object> props) throws WorkflowException {
        try {
            try {
                if (processDefinition == null) {
                    throw new WorkflowException("No bpmn configured", "QT-ERR-WF-01");
                }

                WebTarget webResource = getClient()
                        .target(UriBuilder.fromUri(camundaConfig.getRestUrl())
                                .path(CamundaConstants.PATH_PROCESSDEFINITION)
                                .path(processDefinition.getDefinitionId())
                                .path(CamundaConstants.PATH_SUBMITFORM).toString());

                StartProcessInstanceDto submission = new StartProcessInstanceDto();
                submission.setBusinessKey(UUID.randomUUID().toString());
                Map<String, VariableValueDto> vars = new HashMap<>();

                for (ProcessDefinitionFormVarResolver formVar : processDefinition.getFormVars()) {
                    bindFormVars(null, entity, vars, formVar, null, new HashMap<>(), props);
                }

                submission.setVariables(vars);

                Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(submission, MediaType.APPLICATION_JSON_TYPE));

                if (response.getStatus() != 200) {
                    throw new WorkflowException(String.format("%s: %s \n%s", "Unexpected http result", response.getStatus(), response.readEntity(String.class)), "QT-ERR-CMDA-02");
                }

                String content = response.readEntity(String.class);

                ProcessInstanceDto processInstanceDto = getObjectMapper().readValue(content, ProcessInstanceDto.class);

                if (processInstanceDto == null) {
                    throw new WorkflowException("Invalid camunda dto return object", "QT-ERR-CMDA-03");
                }

                ProcessInstance processInstance = new ProcessInstance();
                processInstance.setRecordRefId(entity.getRelatedId());
                processInstance.setInstanceId(processInstanceDto.getId());
                processInstance.setProcessDefinition(processDefinition);
                processInstance.setRelatedEntity(entity.getClassName());

                eventHandler.onProcessCreated().accept(processInstance);

                processInstanceService.create(processInstance);

                List<ProcessTask> activeTasks = getActiveTasks(processInstance);

                activeTasks.forEach(t -> {
                    processTaskService.create(t);
                    processInstance.getTasks().add(t);
                });

                if (activeTasks.size() > 0) {
                    eventHandler.onTasksCreated().accept(null, activeTasks);
                }

                handleStatusChanged(entity, processInstance, eventHandler.onCompleted());

                return processInstance;
            } catch (Exception e) {
                throw new WorkflowException("Error when start workflow", "QT-ERR-CMDA-05", e);
            }
        } catch (WorkflowException e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    private Optional<Object> getValue(Map<String, Object> map, String key) {
        return map.entrySet().stream().filter(t -> t.getKey().equals(key)).findFirst().map(Map.Entry::getValue);
    }

    public void submitTask(ProcessTask prevTask, IWorkflowEntity entity, Map<String, Object> userResponse, TaskEventHandler eventHandler) throws WorkflowException {
        try {
            try {
                if (prevTask == null) {
                    throw new WorkflowException("Invalid process task", "QT-ERR-WF-04");
                }

                ProcessInstance processInstance = prevTask.getProcessInstance();
                List<ProcessTask> pendingTasks = processInstance.getTasks()
                        .stream()
                        .filter(t -> t.getExecutionStatus().equals(ExecutionStatus.PENDING))
                        .collect(Collectors.toList());

                String prevProcessName = getSubProcessName(processInstance.getProcessDefinition(), prevTask.getTaskName());
                updateVariables(prevProcessName, entity, processInstance, prevTask, userResponse);

                Client client = getClient();
                WebTarget webResource = client.target(UriBuilder.fromUri(camundaConfig.getRestUrl())
                        .path("task")
                        .path(prevTask.getTaskId())
                        .path("submit-form")
                        .toString()
                );

                CompleteTaskDto submission = new CompleteTaskDto();
                Map<String, VariableValueDto> vars = new HashMap<>();

                String taskVarResponse = getValue(userResponse, prevTask.getResponseVar()).map(Object::toString).orElse("");

                if (!userResponse.entrySet().isEmpty()) {
                    VariableValueDto dto = new VariableValueDto();
                    dto.setType(Optional.of(taskVarResponse).map(t -> t.getClass().getSimpleName()).orElse("Object"));
                    dto.setValueInfo(new HashMap<>());
                    dto.setValue(taskVarResponse);

                    vars.put(prevTask.getResponseVar(), dto);

                    submission.setVariables(vars);
                }

                switch (prevTask.getTaskType()) {
                    case "Disposisi": {
                    }
                    case "Approval": {
                        prevTask.setResponse(taskVarResponse);
                        break;
                    }
                }

                Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(submission, MediaType.APPLICATION_JSON_TYPE));

                if (response.getStatus() != 204) {
                    throw new WorkflowException(String.format("%s: %s \n%s", "Unexpected http result", response.getStatus(), response.readEntity(String.class)), "QT-ERR-CMDA-02");
                }

                String comment = getValue(userResponse, "Comment").map(Object::toString).orElse(null);
                boolean isRefreshAssignee = "[REFRESH]".equals(comment) && prevTask.getTaskType().equals("Approval");

                if (isRefreshAssignee) {
                    prevTask.setResponse("Setujui");
                    prevTask.setExecutionStatus(ExecutionStatus.SKIP);
                } else {
                    prevTask.setExecutionStatus(ExecutionStatus.EXECUTED);
                }

                prevTask.setExecutedDate(new Date());
                prevTask.setNote(comment);
                processTaskService.edit(prevTask);

                //remaining pendingTasks
                pendingTasks = pendingTasks.stream().filter(t -> !t.getTaskId().equals(prevTask.getTaskId())).collect(Collectors.toList());

                //skip any other tasks when rejected
                if ("REJECT".equals(taskVarResponse) || isRefreshAssignee) {
                    pendingTasks.forEach(t -> {
                        t.setExecutionStatus(ExecutionStatus.SKIP);
                        processTaskService.edit(t);
                    });

                    pendingTasks.clear();
                }

                //update tasks
                List<ProcessTask> activeTasks = getActiveTasks(processInstance);
                List<ProcessTask> newTasks = new ArrayList<>();
                final List<ProcessTask> pendingTasksCopy = new ArrayList<>(pendingTasks);
                activeTasks.forEach(t -> {
                    t.setPrevious(prevTask);

                    if (pendingTasksCopy.stream().noneMatch(p -> p.getTaskId().equals(t.getTaskId()))) {
                        processTaskService.create(t);
                        newTasks.add(t);
                    }
                });

                if (newTasks.size() > 0) {
                    eventHandler.onTasksCreated().accept(prevTask, newTasks);
                }

                eventHandler.onTaskExecuted().accept(prevTask, userResponse);
            } catch (Exception e) {
                throw new WorkflowException("Error when submit task", "QT-ERR-CMDA-09", e);
            }
        } catch (WorkflowException e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public List<ProcessTask> getActiveTasks(ProcessInstance processInstance) throws WorkflowException {
        try {
            Client client = getClient();
            WebTarget webResource = client.target(UriBuilder.fromUri(camundaConfig.getRestUrl())
                    .path("task")
                    .queryParam("processInstanceId", processInstance.getInstanceId())
                    .queryParam("assigned", true)
                    .toString());

            Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).get();

            if (response.getStatus() != 200) {
                throw new WorkflowException(String.format("%s: %s \n%s", "Unexpected http result", response.getStatus(), response.readEntity(String.class)), "QT-ERR-CMDA-02");
            }

            String content = response.readEntity(String.class);
            List<TaskDto> activeTasks = getObjectMapper().readValue(content, new TypeReference<List<TaskDto>>() {
            });
            List<ProcessTask> processTasks = new ArrayList<>();

            activeTasks.forEach(t -> {
                String camundaAssignee = t.getAssignee();

                ProcessTask processTask = new ProcessTask();
                processTask.setProcessInstance(processInstance);
                processTask.setTaskName(t.getTaskDefinitionKey());
                processTask.setTaskId(t.getId());
                processTask.setTaskType(t.getName());
                processTask.setAssignedDate(new Date());

                if (camundaAssignee.contains("#")) {
                    String[] parts = camundaAssignee.split(Pattern.quote("#"));
                    camundaAssignee = parts[0];
                    processTask.setRelatedAssigneeRecordId(parts[1]);
                }

                if (camundaAssignee.contains("|")) {
                    String[] parts = camundaAssignee.split("\\|");

                    processTask.setAssignee(resolveAssignee(parts[0]));
                    processTask.setSeq(Integer.parseInt(parts[1]));
                } else {
                    processTask.setAssignee(resolveAssignee(camundaAssignee));
                }

                /**
                 * [PATCH ISSUE] TL-53 : REFRESH DELEGASI
                 *      PYMT diganti sebelum pelakhar approve
                 */
                if (StringUtils.isNotEmpty(processTask.getRelatedAssigneeRecordId()) && processTask.getProcessInstance().getRelatedEntity().equals(Surat.class.getName())) {
                    boolean isPenandatangan = processTask.getTaskName().toLowerCase().contains("penandatangan");
                    boolean isPemeriksa = processTask.getTaskName().toLowerCase().contains("pemeriksa");
                    boolean isPelakhar = processTask.getTaskName().toLowerCase().contains("pelakhar");
                    boolean isPymt = processTask.getTaskName().toLowerCase().contains("pymt");

                    if (isPenandatangan) {
                        PenandatanganSurat penandatanganSurat = penandatanganSuratService.find(Long.parseLong(processTask.getRelatedAssigneeRecordId()));

                        if (penandatanganSurat != null && (isPelakhar || isPymt)) {
                            List<MasterDelegasi> degalasiList = masterDelegasiService.getByPejabatCodeList(Arrays.asList(penandatanganSurat.getOrganization().getOrganizationCode()));

                            if (isPymt) {
                                degalasiList.stream().filter(v -> v.getTipe().equals("PYMT")).findFirst().ifPresent(delegasi -> processTask.setAssignee(delegasi.getTo()));
                            }
                        }
                    }

                    if (isPemeriksa) {
                        PemeriksaSurat pemeriksaSurat = pemeriksaSuratService.find(Long.parseLong(processTask.getRelatedAssigneeRecordId()));

                        if (pemeriksaSurat != null && (isPelakhar || isPymt)) {
                            List<MasterDelegasi> degalasiList = masterDelegasiService.getByPejabatCodeList(Arrays.asList(pemeriksaSurat.getOrganization().getOrganizationCode()));

                            if (isPymt) {
                                degalasiList.stream().filter(v -> v.getTipe().equals("PYMT")).findFirst().ifPresent(delegasi -> processTask.setAssignee(delegasi.getTo()));
                            }
                        }
                    }
                }

                try {
                    List<String> varsFromTask = getVarsFromTask(processTask);
                    processTask.setResponseVar(varsFromTask.stream().findFirst().orElse(null));
                } catch (WorkflowException e) {

                }

                processTasks.add(processTask);
            });

            return processTasks;
        } catch (IOException e) {
            throw new WorkflowException("Error when get current active Tasks", "QT-ERR-CMDA-06", e);
        }
    }

    @Override
    public void terminateProcess(ProcessInstance processInstance) {
        Client client = getClient();
        WebTarget webResource = client.target(UriBuilder.fromUri(camundaConfig.getRestUrl())
                .path("process-instance")
                .path(processInstance.getInstanceId())
                .toString());
        Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).delete();

        if (response.getStatus() != 204) {
            throw new WorkflowException(String.format("%s: %s \n%s", "Unexpected http result", response.getStatus(), response.readEntity(String.class)), "QT-ERR-CMDA-02");
        }

        processInstance.getTasks()
                .stream()
                .filter(t -> t.getExecutionStatus().equals(ExecutionStatus.PENDING))
                .forEach(task -> {
                    task.setExecutionStatus(ExecutionStatus.SKIP);
                    processTaskService.edit(task);
                });
    }

    public void updateVariables(String subProcessName, IWorkflowEntity entity, ProcessInstance processInstance, ProcessTask prevTask, Map<String, Object> userResponse) throws WorkflowException {
        Client client = getClient();
        WebTarget webResource = client.target(UriBuilder.fromUri(camundaConfig.getRestUrl())
                .path("process-instance")
                .path(processInstance.getInstanceId())
                .path("variables")
                .toString());

        Map<String, VariableValueDto> vars = new HashMap<>();

        for (ProcessDefinitionFormVarResolver formVar : processInstance.getProcessDefinition().getFormVars()) {
            bindFormVars(subProcessName, entity, vars, formVar, prevTask, userResponse, userResponse);
        }

        PatchVariablesDto submission = new PatchVariablesDto();
        submission.setModifications(vars);

        Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(submission, MediaType.APPLICATION_JSON_TYPE));

        if (response.getStatus() != 204) {
            throw new WorkflowException(String.format("%s: %s \n%s", "Unexpected http result", response.getStatus(), response.readEntity(String.class)), "QT-ERR-CMDA-02");
        }
    }

    private void bindFormVars(String subProcessName, IWorkflowEntity entity, Map<String, VariableValueDto> vars, ProcessDefinitionFormVarResolver formVar, ProcessTask task, Map<String, Object> userResponse, Map<String, Object> props) {
        vars.put(formVar.getVarName(), new VariableValueDto() {{
            ITaskValueResolver valueResolver = (ITaskValueResolver) BeanUtil.getBean(ClassUtil.getClassByName(formVar.getValueResolver()));
            Object val = Optional.ofNullable(valueResolver).orElse(new DefaultValueResolver()).resolve(new TaskContext(entity, task, subProcessName, userResponse, props));
            setType(Optional.ofNullable(val).orElse(new Object()).getClass().getSimpleName().toLowerCase());
            setValue(val);
            setValueInfo(new HashMap<>());
        }});
    }

    @Override
    public String getSubProcessName(ProcessDefinition processDefinition, String taskName) {
        try (InputStream inputStream = new ByteArrayInputStream(processDefinition.getDefinition().getBytes(Charset.forName("UTF-8")))) {
            BpmnModelInstance modelInstance = Bpmn.readModelFromStream(inputStream);

            return modelInstance
                    .getModelElementsByType(modelInstance.getModel().getType(ExtensionElements.class))
                    .stream()
                    .filter(t -> t.getParentElement() instanceof UserTask && ((UserTask) t.getParentElement()).getId().equals(taskName))
                    .map(t -> (UserTask) t.getParentElement())
                    .filter(t -> t.getParentElement() instanceof SubProcess)
                    .map(t -> ((SubProcess) t.getParentElement()).getId())
                    .findFirst()
                    .orElse(taskName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ProcessStatus getProcessStatus(IWorkflowEntity entity) throws WorkflowException {
        try {
            ProcessInstance processInstance = processInstanceService.getByRelatedEntity(entity.getClassName(), entity.getRelatedId());
            Client client = getClient();
            WebTarget webResource = client.target(UriBuilder.fromUri(camundaConfig.getRestUrl())
                    .path("history")
                    .path("variable-instance")
                    .queryParam("processInstanceId", processInstance.getInstanceId())
                    .queryParam("variableName", "Status")
                    .toString());

            Response response = webResource.request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).get();
            String content = response.readEntity(String.class);
            List<HistoricVariableInstanceDto> vars = getObjectMapper().readValue(content, new TypeReference<List<HistoricVariableInstanceDto>>() {
            });

            return ProcessStatus.valueOf(vars.stream().map(t -> t.getValue().toString()).findFirst().orElse("NOT_VALID"));
        } catch (Exception e) {
            throw new WorkflowException("Error when get current process status", "QT-ERR-CMDA-07", e);
        }
    }

    @Override
    public void execute(ProcessTask prevTask, IWorkflowEntity entity, Map<String, Object> fieldValues, TaskEventHandler eventHandler) throws InternalServerErrorException {
        eventHandler.onExecuting().accept(prevTask);

        submitTask(prevTask, entity, fieldValues, eventHandler);

        handleStatusChanged(entity, prevTask.getProcessInstance(), eventHandler.onCompleted());
    }

    private void handleStatusChanged(IWorkflowEntity entity, ProcessInstance processInstance, BiConsumer<ProcessInstance, ProcessStatus> onCompleted) {
        if (onCompleted != null) {
            ProcessStatus processStatus = getProcessStatus(entity);

            if (!processStatus.equals(ProcessStatus.PENDING) && !processStatus.equals(ProcessStatus.NOT_VALID)) {
                onCompleted.accept(processInstance, processStatus);
            }
        }
    }

    private List<String> getVarsFromTask(ProcessTask currentTask) throws WorkflowException {
        try {
            String currentDefinition = processInstanceService.getProcessDefinitionByInstanceId(currentTask.getProcessInstance().getInstanceId());

            try (InputStream inputStream = new ByteArrayInputStream(currentDefinition.getBytes(Charset.forName("UTF-8")))) {
                BpmnModelInstance modelInstance = Bpmn.readModelFromStream(inputStream);


                List<CamundaFormField> formVars = modelInstance
                        .getModelElementsByType(modelInstance.getModel().getType(ExtensionElements.class))
                        .stream()
                        .filter(t -> t.getParentElement() instanceof UserTask && ((UserTask) t.getParentElement()).getId().equals(currentTask.getTaskName()))
                        .flatMap(t -> t.getChildElementsByType(modelInstance.getModel().getType(CamundaFormData.class)).stream())
                        .flatMap(t -> t.getChildElementsByType(modelInstance.getModel().getType(CamundaFormField.class)).stream())
                        .map(t -> (CamundaFormField) t)
                        .collect(Collectors.toList());

                return formVars.stream().map(CamundaFormField::getCamundaId).collect(Collectors.toList());
            }
        } catch (IOException e) {
            throw new WorkflowException("Error when get task variables", "QT-ERR-CMDA-07", e);
        }

    }

    private MasterStrukturOrganisasi resolveAssignee(String assignee) {
        return masterStrukturOrganisasiService.getByCode(assignee);
    }

    private Client getClient() {
        final ClientConfig clientConfig = new ClientConfig()
                .register(MultiPartFeature.class);

        return ClientBuilder.newClient(clientConfig);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
