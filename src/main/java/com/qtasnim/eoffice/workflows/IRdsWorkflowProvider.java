package com.qtasnim.eoffice.workflows;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.ws.dto.IWorkflowDto;
import com.qtasnim.eoffice.ws.dto.SuratDto;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public interface IRdsWorkflowProvider {
    ProcessDefinition deploy(ProcessDefinition processDefinition);
    void undeploy(String deploymentId);
    List<ProcessTask> getActiveTasks(ProcessInstance processInstance);
    String getSubProcessName(ProcessDefinition processDefinition, String taskName);
    ProcessStatus getProcessStatus(IWorkflowEntity entity);

    // Added by pras
    ProcessInstance startWorkflow(SuratDto dto, IWorkflowEntity entity, Long idJenisDokumen);
    // Added by pras
    void execute(IWorkflowEntity entity, SuratDto dto, TaskEventHandler eventHandler) throws InternalServerErrorException;
    // Added by pras
    void submitTask(String taskId, IWorkflowEntity entity, Object userResponse, String approvalNote) throws WorkflowException;
}
