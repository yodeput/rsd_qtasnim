package com.qtasnim.eoffice.workflows;

public enum ProcessStatus {
    PENDING("PENDING"), APPROVED("APPROVED"), REJECTED("REJECTED"), DRAFT("DRAFT"), NOT_VALID("NOT_VALID");

    String toString;

    private ProcessStatus(String toString) {
        this.toString = toString;
    }

    private ProcessStatus() {
    }

    public String toString() {
        return this.toString != null?this.toString:super.toString();
    }
}
