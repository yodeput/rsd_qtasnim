package com.qtasnim.eoffice.workflows;

public class UnexpectedHttpResultException extends WorkflowException {
    public UnexpectedHttpResultException(String exceptionCode) {
        super("QT-ERR-CMDA-04", exceptionCode);
    }

    public UnexpectedHttpResultException(String exceptionCode, Exception e) {
        super("QT-ERR-CMDA-04", exceptionCode, e);
    }
}
