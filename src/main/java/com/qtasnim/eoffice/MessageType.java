package com.qtasnim.eoffice;

public enum MessageType {
    GLOBAL, DOC_ACT, LOG
}
