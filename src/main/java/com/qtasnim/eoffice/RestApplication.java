package com.qtasnim.eoffice;

import io.swagger.annotations.*;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.HttpHeaders;
import java.util.HashMap;
import java.util.Map;

@ApplicationPath("/api")
@SwaggerDefinition(info = @Info(
        title = "RDS Rest Endpoint",
        description = "Semua rest endpoint terdapat validasi security token kecuali '/auth/login'. Token dikirim melalui http header dengan format 'Authorization: Bearer token'.",
        version = "1.0.0",
        contact = @Contact(
                name = "qtasnimdev",
                email = "admin@qtasnim.com",
                url = "http://qtasnim.com"
        )),
        securityDefinition = @SecurityDefinition(
                apiKeyAuthDefinitions = @ApiKeyAuthDefinition(
                        key = "Bearer" ,
                        name = HttpHeaders.AUTHORIZATION,
                        description = "Security Token didapat dari /auth/login",
                        in = ApiKeyAuthDefinition.ApiKeyLocation.HEADER))
)
public class RestApplication extends Application {
    @Override
    public Map<String, Object> getProperties() {
        Map<String, Object> props = new HashMap<>();
        props.put("jersey.config.server.provider.classnames", "org.glassfish.jersey.media.multipart.MultiPartFeature");
        return props;
    }
}
