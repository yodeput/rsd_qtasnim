package com.qtasnim.eoffice;

import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.services.IService;
import com.qtasnim.eoffice.util.ClassUtil;
import com.qtasnim.eoffice.workflows.resolvers.ITaskValueResolver;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Startup
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
public class InitializerBean {

    @Inject
    private ApplicationContext applicationContext;

    @PostConstruct
    protected void bootstrap() {
        try {
            loadWorkflowTaskResolver();
        } catch (Exception e) {

        }
    }

    private void loadWorkflowTaskResolver() throws IOException, ClassNotFoundException {
        List<Class> taskResolversClazzes = new ArrayList<>();
        List<Class> serviceResolversClazzes = new ArrayList<>();

        Arrays.stream(ClassUtil.getClasses("com.qtasnim.eoffice.workflows.resolvers"))
                .filter(c -> Arrays.asList(c.getInterfaces()).contains(ITaskValueResolver.class))
                .forEach(taskResolversClazzes::add);
        applicationContext.setTaskResolvers(taskResolversClazzes);

        Arrays.stream(ClassUtil.getClasses("com.qtasnim.eoffice.services"))
                .filter(c -> Arrays.asList(c.getInterfaces()).contains(IService.class))
                .forEach(serviceResolversClazzes::add);
        applicationContext.setServiceResolvers(serviceResolversClazzes);
    }
}