package com.qtasnim.eoffice;

public enum NotificationType {
    INFO("info"), WARNING("warning"), ERROR("error");

    String toString;

    private NotificationType(String toString) {
        this.toString = toString;
    }

    private NotificationType() {
    }

    public String toString() {
        return this.toString != null?this.toString:super.toString();
    }
}
