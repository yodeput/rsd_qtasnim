package com.qtasnim.eoffice.rabbitmq;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JenisDokumenDocLibExchange extends AmqpEvent {
    private String idJenisDokumen;
}
