package com.qtasnim.eoffice.rabbitmq;

import com.qtasnim.eoffice.MessageType;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.services.IMessagingProvider;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.util.ExceptionUtil;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

@Stateless
@LocalBean
public class RabbitMqProvider implements IMessagingProvider {
    @Inject
    private Event<BackgroundServiceLoggerExchange> backgroundServiceLoggerEvent;

    @Inject
    private Event<FrontEndExchange> frontEndEvent;

    @Inject
    private Event<JenisDokumenDocLibExchange> jenisDokumenDocLibEvent;

    @Inject
    private Logger logger;

    @Inject
    private ApplicationConfig applicationConfig;

    @Override
    public void send(MessageType type, Consumer<Map<String, String>> onBuild, String message) {
        HashMap<String, String> props = new HashMap<>();
        onBuild.accept(props);

        try {
            switch (type) {
                case LOG:{
                    BackgroundServiceLoggerExchange event = new BackgroundServiceLoggerExchange();
                    event.setLogger(props.entrySet().stream().filter(t -> t.getKey().equals("logger")).map(Map.Entry::getValue).findFirst().orElse(null));
                    event.setMessage(message);
                    backgroundServiceLoggerEvent.fire(event);

                    break;
                }
                case DOC_ACT:{
                    JenisDokumenDocLibExchange event = new JenisDokumenDocLibExchange();
                    event.setIdJenisDokumen(props.entrySet().stream().filter(t -> t.getKey().equals("idJenisDokumen")).map(Map.Entry::getValue).findFirst().orElse(null));
                    event.setMessage(message);
                    jenisDokumenDocLibEvent.fire(event);

                    break;
                }
                case GLOBAL:{
                    FrontEndExchange event = new FrontEndExchange();
                    event.setRecipients(props.entrySet().stream().filter(t -> t.getKey().equals("recipients")).map(Map.Entry::getValue).findFirst().orElse(null));
                    event.setMessage(message);
                    frontEndEvent.fire(event);

//                    try {
//                        String fileName = String.format("rabbitmq-debug%s.log", Optional.ofNullable(System.getProperty("rds.servername")).filter(StringUtils::isNotEmpty).map(t -> "-" + t).orElse(""));
//                        String targetPath = applicationConfig.getTempDir() + File.separator + fileName;
//                        String logBody = "SEND" + " | " + new Date().toString() + " | " + message;
//                        Files.write(Paths.get(targetPath), (logBody + System.lineSeparator()).getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
//                    } catch (Exception e) {
//
//                    }

                    break;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), ExceptionUtil.getRealCause(e));
        }
    }
}
