package com.qtasnim.eoffice.rabbitmq;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JenisDokumenDocLibQueue extends AmqpEvent {
    private String idJenisDokumen;
}
