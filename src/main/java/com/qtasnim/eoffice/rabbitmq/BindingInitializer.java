package com.qtasnim.eoffice.rabbitmq;

import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.services.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.IOException;

@Singleton
@Startup
public class BindingInitializer {
    @Inject
    private RabbitBinder binder;

    @Inject
    private Logger logger;

    @Inject
    private ApplicationConfig config;

    @PostConstruct
    public void initialize() {
        if (RabbitMqProvider.class.getName().equals(config.getMessageServiceProvider())) {
            try {
                binder.configuration()
                        .addHost(config.getAmqpHost()) // (1)
                        .setUsername(config.getAmqpUsername()) // (2)
                        .setPassword(config.getAmqpPassword()) // (3)
                        .setSecure(config.getAmqpSecure()) // (4)
                        .setConnectTimeout(60000) // (5)
                        .setConnectRetryWaitTime(1000) // (6)
                        .setRequestedConnectionHeartbeatTimeout(3); // (7)
                binder.initialize();
            } catch (IOException e) {
                logger.error("Unable to initialize", e);
            }
        }
    }
}
