package com.qtasnim.eoffice.rabbitmq;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FrontEndQueue extends AmqpEvent {
    private String recipients;
}
