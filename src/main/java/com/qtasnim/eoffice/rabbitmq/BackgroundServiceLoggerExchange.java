package com.qtasnim.eoffice.rabbitmq;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BackgroundServiceLoggerExchange extends AmqpEvent {
    private String logger;
}
