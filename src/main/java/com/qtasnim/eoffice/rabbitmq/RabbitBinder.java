package com.qtasnim.eoffice.rabbitmq;

import com.rabbitmq.client.BuiltinExchangeType;
import net.reini.rabbitmq.cdi.BindingDeclaration;
import net.reini.rabbitmq.cdi.EventBinder;
import net.reini.rabbitmq.cdi.ExchangeDeclaration;
import net.reini.rabbitmq.cdi.QueueDeclaration;
import org.apache.commons.lang3.StringUtils;

import javax.enterprise.context.Dependent;
import java.util.Optional;


@Dependent
public class RabbitBinder extends EventBinder {
    @Override
    protected void bindEvents() {
        String serverName = Optional.ofNullable(System.getProperty("rds.servername")).filter(StringUtils::isNotEmpty).orElse("");

        /**
         * BINDING
         */
        // Background Service
        bind(BackgroundServiceLoggerExchange.class)
                .toExchange(BackgroundServiceLoggerExchange.class.getName())
                .withRoutingKey("background-service");
        bind(BackgroundServiceLoggerQueue.class)
                .toQueue(BackgroundServiceLoggerQueue.class.getName() + "_" + serverName);

        // Front End
        bind(FrontEndExchange.class)
                .toExchange(FrontEndExchange.class.getName())
                .withRoutingKey("front-end");
        bind(FrontEndQueue.class)
                .toQueue(FrontEndQueue.class.getName() + "_" + serverName);

        // Jenis Dokumen
        bind(JenisDokumenDocLibExchange.class)
                .toExchange(JenisDokumenDocLibExchange.class.getName())
                .withRoutingKey("jenis-dokumen");
        bind(JenisDokumenDocLibQueue.class)
                .toQueue(JenisDokumenDocLibQueue.class.getName() + "_" + serverName);
    }
}
