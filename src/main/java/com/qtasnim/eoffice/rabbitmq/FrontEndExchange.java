package com.qtasnim.eoffice.rabbitmq;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FrontEndExchange extends AmqpEvent {
    private String recipients;
}
