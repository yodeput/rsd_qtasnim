package com.qtasnim.eoffice.helpers;

import com.qtasnim.eoffice.util.TemplatingSeparator;
import org.apache.commons.lang.StringUtils;

public class TemplatingHelper {
    public static final String TANGGAL_DOKUMEN_KEY = "Tanggal Dokumen";
    public static final String NO_DOKUMEN_KEY = "No Dokumen";
    public static final String NO_DOKUMEN_BARCODE_KEY = "No Dokumen - Barcode";
    public static final String PEMERIKSA_SURAT_KEY = "Pemeriksa Surat";
    public static final String PENERIMA_SURAT_KEY = "Kepada Internal";
    public static final String PENERIMA_SURAT_CONDITIONAL_KEY = "Penerima Surat#1"+TemplatingSeparator.SEPARATOR_NEW_ENTITY+"Penerima Surat#2";
    public static final String PENANDATANGAN_KEY = "Penandatangan";
    public static final String PENANDATANGAN_QRCODE_KEY = "Penandatangan QRCode";
    public static final String PENANDATANGAN_CONDITIONAL_KEY = "Penandatangan#1"+TemplatingSeparator.SEPARATOR_NEW_ENTITY+"Penandatangan#2";
    public static final String ATAS_NAMA_KEY = "Atas Nama";
    public static final String LAMPIRAN_LIST_KEY = "Lampiran List";
    public static final String NO_AGENDA_KEY = "No Agenda";
    public static final String TEMBUSAN_KEY = "Tembusan";
    public static final String MENGETAHUI_KEY = "Mengetahui";
    public static final String TEMBUSAN_EKSTERNAL_KEY = "Tembusan Eksternal";
    public static final String PENERIMA_EKSTERNAL_KEY = "Penerima Eksternal";
    public static final String TEMPAT_KEY = "Tempat";
    public static final String TANGGAL_KEY = "Tanggal";
    public static final String UNDANGAN_DATE_KEY = "Undangan Tanggal";
    public static final String UNDANGAN_DAY_KEY = "Undangan Hari";
    public static final String UNDANGAN_START_TIME_KEY = "Undangan Jam Mulai";
    public static final String UNDANGAN_END_TIME_KEY = "Undangan Jam Selesai";
    public static final String UNDANGAN_HARI_KEY = "Undangan Hari,Tanggal";
    public static final String UNDANGAN_JAM_KEY = "Undangan Waktu";
    public static final String KONSEPTOR_KEY = "Konseptor#1"+TemplatingSeparator.SEPARATOR_NEW_ENTITY+"Konseptor#2";
    public static final String PENERIMA_SURAT_LIST_KEY = "Penerima List";
    public static final String PEMIMPIN_RAPAT_CONDITIONAL_KEY = "Pemimpin#1"+TemplatingSeparator.SEPARATOR_NEW_ENTITY+"Pemimpin#2";

    public static final String SUBSTITUTE_COMMA = "!=comma=";

    public static String getGetterName(String fieldName) {
        return "get".concat(capitalizeFirstLetter(fieldName));
    }

    public static String toMetadataFormatName(String fieldName) {
//        String temp = fieldName.replaceAll("(.)(\\p{Upper})", "$1_$2");
//
//        return capitalizeFirstLetter(temp);
        return StringUtils.deleteWhitespace(fieldName).toLowerCase();
    }

    public static String capitalizeFirstLetter(String src) {
        StringBuilder newString = new StringBuilder(src);
        newString.setCharAt(0, Character.toUpperCase(src.charAt(0)));

        return newString.toString();
    }
}
