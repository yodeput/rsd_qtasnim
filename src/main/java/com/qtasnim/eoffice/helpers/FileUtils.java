package com.qtasnim.eoffice.helpers;

import org.apache.commons.io.FilenameUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class FileUtils {
    public static String generateRandomFileName(String originalName) {
        return UUID.randomUUID().toString() + "." + getFileExtension(originalName);
    }

    public static String getFileExtension(String fileName) {
        return FilenameUtils.getExtension(fileName);
    }

    public static String getFileName(String fileName) {
        return FilenameUtils.getName(fileName);
    }

    public static Long getFileSize(InputStream file) throws IOException {
        long size = 0;
        int chunk = 0;
        byte[] buffer = new byte[1024];
        while((chunk = file.read(buffer)) != -1){
            size += chunk;
        }

        return size;
    }
}
