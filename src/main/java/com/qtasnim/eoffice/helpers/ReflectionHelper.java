package com.qtasnim.eoffice.helpers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReflectionHelper {
    public static Map<String,String> reflectionResolve(Object entity, List<String> fieldNames) {
        Map<String,String> mapped = new HashMap<>();

        for(String fieldName : fieldNames) {
            String fieldValue = getFieldValue(entity, fieldName);
            mapped.put(fieldName, fieldValue==null?" ":fieldValue);
        }

        return mapped;
    }

    public static String getFieldValue(Object entity, String fieldName) {
        String[] fieldHierarch = fieldName.split("\\.");

        if(fieldHierarch.length > 1) {
            Object obj = entity;
            for(int i=0; i<fieldHierarch.length-1; i++) {
                String methodName = TemplatingHelper.getGetterName(fieldHierarch[i]);

                obj = executeMethod(entity, methodName);
            }

            String result;
            if(obj == null)
                result = null;
            else
                result = (String) executeMethod(obj, TemplatingHelper.getGetterName(fieldHierarch[fieldHierarch.length-1]));

            return result==null?" ":result;
        } else {
            String methodName = TemplatingHelper.getGetterName(fieldName);

            String result = String.valueOf(executeMethod(entity, methodName));

            return result==null?" ":result;
        }
    }

    public static Object executeMethod(Object entity, String methodName) {
        Class<?> claz = entity.getClass();

        Method method = null;
        try {
            method = claz.getMethod(methodName);
            return method.invoke(entity,null);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();

            return null;
        }
    }

    public static Object executeMethod(Object entity, String methodName, Object[] args, Class<?>[] argTypes) {
        Class<?> claz = entity.getClass();

        Method method = null;
        try {
            method = claz.getMethod(methodName, argTypes);

            Object result = method.invoke(entity,args);
            return result;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();

            return null;
        }
    }
}
