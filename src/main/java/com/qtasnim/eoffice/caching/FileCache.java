package com.qtasnim.eoffice.caching;

import com.google.common.cache.CacheBuilder;
import com.qtasnim.eoffice.InternalServerErrorException;
import org.cacheonix.Cacheonix;
import org.cacheonix.cache.Cache;

import javax.ejb.Singleton;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

@Singleton
public class FileCache {
    private com.google.common.cache.Cache<String, Lock> lockMap = CacheBuilder.newBuilder()
            .softValues()
            .build();
    private Cache<String, byte[]> cache = Cacheonix.getInstance().getCache("application.file.cache");

    public byte[] get(String id) {
        return cache.get(id);
    }

    public byte[] getThenRemove(String id) {
        try {
            return cache.get(id);
        } finally {
            remove(id);
        }
    }

    public byte[] get(String id, Supplier<byte[]> resolver) {
        try {
            Lock lock = lockMap.get(id, ReentrantLock::new);

            if (lock.tryLock()) {
                try {
                    byte[] result = cache.get(id);

                    if (result == null) {
                        return put(id, resolver);
                    }

                    return result;
                } finally {
                    lock.unlock();
                    lockMap.invalidate(id);
                }
            } else {
                return resolver.get();
            }
        } catch (Exception e) {
            return resolver.get();
        }
    }

    public void put(String id, byte[] data) {
        cache.put(id, data);
    }

    public byte[] put(String id, Supplier<byte[]> resolver) {
        byte[] data = resolver.get();

        if (data == null) {
            throw new InternalServerErrorException("Invalid caching data");
        }

        cache.put(id, data);

        return data;
    }

    public void remove(String id) {
        cache.remove(id);
    }
}
