package com.qtasnim.eoffice.cmis;

import lombok.Data;
import lombok.experimental.Delegate;

@Data
public class CMISItem {
    @Delegate
    private String id;
    private String path;
    private String name;
    private Object cmisObject;
    private String version;
}
