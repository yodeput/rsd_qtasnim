package com.qtasnim.eoffice.cmis;

public class CMISProviderException extends RuntimeException {
    private static final long serialVersionUID = -9072748588901423500L;
    private final String exceptionCode;

    public String getExceptionCode() {
        return this.exceptionCode;
    }

    public CMISProviderException(String message, String exceptionCode) {
        super(message);
        this.exceptionCode = exceptionCode;
    }

    public CMISProviderException(String message, String exceptionCode, Exception e) {
        super(message, e);
        this.exceptionCode = exceptionCode;
    }
}
