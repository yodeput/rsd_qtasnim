package com.qtasnim.eoffice.cmis;

import java.util.List;

public interface ICMISProvider {
    List<CMISItem> getContents(CMISFolder parent);
    CMISDocument getDocument(String id);
    List<CMISDocument> getVersions(String id);
    byte[] getDocumentContent(String id);
    byte[] getDocumentContent(String id, String version);
    CMISFolder getFolder(String path);
    CMISFolder getRoot();
    CMISDocument uploadDocument(CMISFolder parent, byte[] fileContent, String newFileName);
    CMISDocument updateDocument(String id, byte[] fileContent);
    void delete(CMISItem item);
    CMISFolder createFolder(CMISFolder parent, String newFolderName);
    void openConnection();
    void closeConnection();
    Boolean validateIntegration();
    String trimDocId(String id);
    String resolveDocId(String id);
    String resolveDocId(String id, String version);
}
