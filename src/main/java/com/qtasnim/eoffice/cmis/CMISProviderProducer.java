package com.qtasnim.eoffice.cmis;

import com.qtasnim.eoffice.cmis.alfresco.AlfrescoProvider;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

public class CMISProviderProducer {
    @Inject
    private AlfrescoProvider alfrescoProvider;

    @Produces
    public ICMISProvider produce() {
        return alfrescoProvider;
    }
}
