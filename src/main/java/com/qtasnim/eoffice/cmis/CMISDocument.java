package com.qtasnim.eoffice.cmis;

import lombok.Data;

@Data
public class CMISDocument extends CMISItem {
    private String mime;
    private Long size;
}
