package com.qtasnim.eoffice.cmis;

import com.qtasnim.eoffice.helpers.FileUtils;
import org.apache.chemistry.opencmis.client.api.CmisObjectProperties;
import org.apache.chemistry.opencmis.client.api.Property;
import org.apache.chemistry.opencmis.client.runtime.PropertyImpl;
import org.apache.chemistry.opencmis.commons.impl.jaxb.CmisPropertyDateTime;
import org.apache.chemistry.opencmis.commons.impl.jaxb.CmisPropertyId;
import org.apache.chemistry.opencmis.commons.impl.jaxb.CmisPropertyInteger;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public class CMISHelper {
    public static String findStringProperty(CmisObjectProperties properties, String id)
    {
        String result = null;

        for (Property property: properties.getProperties())
        {
            if (property.getDefinition().getId().equals(id))
            {
                result = toString(property);
                break;
            }
        }

        return result;
    }

    public static String findIdProperty(CmisObjectProperties properties, String id)
    {
        String result = null;

        for (Property property: properties.getProperties())
        {
            if (property.getDefinition().getId().equals(id))
            {
                result = toId(property);
                break;
            }
        }

        return result;
    }

    public static Integer findIntegerProperty(CmisObjectProperties properties, String id)
    {
        Integer result = null;

        for (Property property: properties.getProperties())
        {
            if (property.getDefinition().getId().equals(id))
            {
                result = toInteger(property);
                break;
            }
        }

        return result;
    }

    public static Date findDateProperty(CmisObjectProperties properties, String id)
    {
        Date result = null;

        for (Property property: properties.getProperties())
        {
            if (property.getDefinition().getId().equals(id))
            {
                result = toDate(property);
                break;
            }
        }

        return result;
    }

    public static String toString(Property property) {
        return property.getValueAsString();
    }

    public static String toId(Property property) {
        String result = null;

        if (property instanceof CmisPropertyId)
        {
            result = ((CmisPropertyId)property).getValue().stream().findFirst().orElse(null);
        }
        return result;
    }

    public static Integer toInteger(Property property) {
        Integer result = null;

        if (property instanceof CmisPropertyInteger)
        {
            result = ((CmisPropertyInteger)property).getValue().stream().map(BigInteger::intValue).findFirst().orElse(null);
        }
        return result;
    }

    public static Long toLong(Property property) {
        return Long.parseLong(Optional.of(property.getValueAsString()).filter(StringUtils::isNotEmpty).orElse("0"));
    }

    public static Date toDate(Property property) {
        Date result = null;

        if (property instanceof CmisPropertyDateTime)
        {
            result = ((CmisPropertyDateTime)property).getValue().stream().map(t -> t.toGregorianCalendar().getTime()).findFirst().orElse(null);
        }
        return result;
    }
}
