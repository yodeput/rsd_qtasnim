package com.qtasnim.eoffice.cmis.alfresco;

import com.qtasnim.eoffice.caching.FileCache;
import com.qtasnim.eoffice.cmis.*;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.MasterDocLibProvider;
import com.qtasnim.eoffice.db.MasterDocLibProviderConfig;
import com.qtasnim.eoffice.helpers.FileUtils;
import com.qtasnim.eoffice.services.MasterDocLibProviderService;
import com.qtasnim.eoffice.services.MasterMimeMappingService;
import org.apache.chemistry.opencmis.client.api.*;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisConnectionException;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import com.qtasnim.eoffice.services.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Stateless
@LocalBean
public class AlfrescoProvider implements ICMISProvider {
    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private MasterDocLibProviderService masterDocLibProviderService;

    @Inject
    private Logger logger;

    @Inject
    private MasterMimeMappingService masterMimeMappingService;

    @Inject
    private FileCache fileCache;

    private Session session;
    private String username;
    private String password;
    private String urlRepository;
    private String basePath;

    @PostConstruct
    private void init() {
        try {
            MasterDocLibProvider masterDocLibProvider = masterDocLibProviderService.getProvider(applicationContext.getApplicationConfig().getCmisProvider());

            for (MasterDocLibProviderConfig config : masterDocLibProvider.getConfig()) {
                switch (config.getName()) {
                    case AlfrescoConstants.ALFRESCO_CMISBASEPATH: {
                        this.basePath = config.getValue();
                        break;
                    }
                    case AlfrescoConstants.ALFRESCO_CMISREPOSITORY: {
                        this.urlRepository = config.getValue();
                        break;
                    }
                    case AlfrescoConstants.ALFRESCO_CMISUSERNAME: {
                        this.username = config.getValue();
                        break;
                    }
                    case AlfrescoConstants.ALFRESCO_CMISPASSWORD: {
                        this.password = config.getValue();
                        break;
                    }
                }
            }
        } catch (CMISProviderNotFoundException e) {
            logger.error(null, e);
        }
    }

    @Override
    public List<CMISItem> getContents(CMISFolder parent) throws CMISProviderException {
        try {
            Folder parentFolder = (Folder) getSession().getObjectByPath(this.basePath + parent.getPath());

            return StreamSupport.stream(parentFolder.getChildren().spliterator(), false)
                    .map(t -> {
                        CMISItem item = new CMISItem();

                        if (t instanceof FileableCmisObject) {
                            if (t instanceof Document) {
                                item = new CMISDocument();
                            } else if (t instanceof Folder) {
                                item = new CMISFolder();
                            }

                            FileableCmisObject fileableCmisObject = (FileableCmisObject) t;

                            extractProperty(item, fileableCmisObject);
                        }

                        return item;
                    })
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new CMISProviderException("", "Failed to get folder contents", e);
        }
    }

    @Override
    public List<CMISDocument> getVersions(String id) throws CMISProviderException {
        try {
            Document document = (Document) getSession().getObject(resolveDocId(id));

            return document.getAllVersions().stream().map(this::getDocument).collect(Collectors.toList());
        } catch (Exception e) {
            throw new CMISProviderException("", "Failed to get Document Versions", e);
        }
    }

    @Override
    public CMISDocument getDocument(String id) throws CMISProviderException {
        try {
            Document document = (Document) getSession().getObject(resolveDocId(id));

            return getDocument(document);
        } catch (Exception e) {
            throw new CMISProviderException("", "Failed to get Document", e);
        }
    }

    public CMISDocument getDocument(String id, String version) throws CMISProviderException {
        try {
            Document document = (Document) getSession().getObject(resolveDocId(id, version));

            return getDocument(document);
        } catch (Exception e) {
            throw new CMISProviderException("", "Failed to get Document", e);
        }
    }

    private CMISDocument getDocument(Document document) {
        CMISDocument cmisDocument = new CMISDocument();
        extractProperty(cmisDocument, document);

        return cmisDocument;
    }

    @Override
    public byte[] getDocumentContent(String id) throws CMISProviderException {
        String trimDocId = trimDocId(id);

        return fileCache.get(trimDocId, () -> {
            CMISDocument cmisDocument;
            try {
                cmisDocument = getDocument(trimDocId);
            } catch (CMISProviderException e) {
                throw new RuntimeException(null, e);
            }

            return getDocumentContent(cmisDocument);
        });
    }

    @Override
    public byte[] getDocumentContent(String id, String version) throws CMISProviderException {
        String cacheId = Stream.of(id, version).filter(Objects::nonNull).collect(Collectors.joining("+"));

        return fileCache.get(cacheId, () -> {
            CMISDocument cmisDocument;

            try {
                cmisDocument = getDocument(resolveDocId(id), version);

            } catch (CMISProviderException e) {
                throw new RuntimeException(null, e);
            }

            return getDocumentContent(cmisDocument);
        });
    }

    private byte[] getDocumentContent(CMISDocument cmisDocument) {
        try {
            Document document = (Document) cmisDocument.getCmisObject();
            ContentStream cs = document.getContentStream(null);
            BufferedInputStream inputStream =new BufferedInputStream(cs.getStream());
            byte[] result = IOUtils.toByteArray(inputStream);
            inputStream.close();

            return result;
        } catch (IOException e) {
            throw new RuntimeException("Download document failed", e);
        }
    }

    @Override
    public CMISFolder getFolder(String path) throws CMISProviderException {
        try {
            Folder t = (Folder) getSession().getObjectByPath(this.basePath + path);

            CMISFolder item = new CMISFolder();
            extractProperty(item, t);

            return item;
        } catch (Exception e) {
            throw new CMISProviderException("", "Failed to get Folder", e);
        }
    }

    private Session getSession() throws CMISProviderException {
        if (session == null) {
            logger.info("TODO: session null, please check your code, openConnection should be called first");
            openConnection();
        }

        return session;
    }

    @Override
    public CMISFolder getRoot() throws CMISProviderException {
        return getFolder("");
    }

    private void extractProperty(CMISItem item, FileableCmisObject t) {
        for (Property property: t.getProperties()) {
            switch (property.getId()) {
                case ("cmis:name"): {
                    item.setName(CMISHelper.toString(property));
                    break;
                }
                case ("cmis:path"): {
                    item.setPath(Optional.of(CMISHelper.toString(property)).map(this::translatePath).orElse(null));
                    break;
                }
                case ("cmis:versionLabel"): {
                    item.setVersion(CMISHelper.toString(property));
                    break;
                }
                case ("cmis:contentStreamMimeType"): {
                    if (item instanceof CMISDocument) {
                        ((CMISDocument) item).setMime(CMISHelper.toString(property));
                    }
                    break;
                }
                case ("cmis:contentStreamLength"): {
                    if (item instanceof CMISDocument) {
                        ((CMISDocument) item).setSize(CMISHelper.toLong(property));
                    }
                    break;
                }
            }
        }

        item.setId(trimDocId(t.getId()));
        item.setCmisObject(t);
    }

    @Override
    public CMISDocument uploadDocument(CMISFolder parent, byte[] fileContent, String newFileName) throws CMISProviderException {
        try {
            String fileExtension = FileUtils.getFileExtension(newFileName);
            String mime = masterMimeMappingService.getMime(fileExtension);
            Long fileSize = (long) fileContent.length;

            if (mime == null) {
                throw new CMISInvalidFileExtensionException("Format berkas tidak valid", "");
            }

            InputStream stream = new ByteArrayInputStream(fileContent);
            ContentStream contentStream = getSession().getObjectFactory().createContentStream(newFileName, fileSize, mime, stream);
            stream.close();

            Folder folder = (Folder) parent.getCmisObject();
            CMISProperties properties = new CMISProperties();
            properties.setDocumentName(newFileName);
            properties.setFolder(false);

            String createdDocumentId = folder.createDocument(properties.getCMISPropertiesMap(), contentStream, VersioningState.MAJOR).getId();
            folder.refresh();

            fileCache.put(createdDocumentId, () -> fileContent);

            return this.getDocument(createdDocumentId);
        }
        catch (CMISInvalidFileExtensionException e) {
            throw e;
        }
        catch (Exception e) {
            throw new CMISProviderException("Upload document failed", "", e);
        }
    }

    @Override
    public CMISDocument updateDocument(String id, byte[] fileContent) throws CMISProviderException {
        try {
            Document document = (Document) getSession().getObject(resolveDocId(id));
            InputStream stream = new ByteArrayInputStream(fileContent);
            ContentStream contentStream = new ContentStreamImpl(document.getContentStreamFileName(), BigInteger.valueOf(fileContent.length), document.getContentStreamMimeType(), stream);
            document.setContentStream(contentStream, true);

            fileCache.put(trimDocId(document.getId()), () -> fileContent);

            return getDocument(document);
        } catch (Exception e) {
            throw new CMISProviderException("", "Failed to get Document", e);
        }
    }

    @Override
    public void delete(CMISItem item) throws CMISProviderException {
        try {
            String id = item.getId();
            if (!(item.getCmisObject() instanceof FileableCmisObject)) {
                throw new CMISDeleteItemException();
            }

            ((FileableCmisObject) item.getCmisObject()).delete();

            item.setId(null);
            item.setPath(null);
            item.setName(null);
            item.setCmisObject(null);

            fileCache.remove(id);
        }
        catch (CMISDeleteItemException e) {
            throw new CMISProviderException("", "Error delete operation", e);
        }
    }

    @Override
    public CMISFolder createFolder(CMISFolder parent, String newFolderName) throws CMISProviderException {
        try {
            Map<String, Object> properties = new HashMap<>();
            properties.put(PropertyIds.NAME, newFolderName);
            properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");

            Folder parentFolder = (Folder) parent.getCmisObject();
            Folder newFolder = parentFolder.createFolder(properties);

            CMISFolder item = new CMISFolder();
            extractProperty(item, newFolder);

            return item;
        } catch (Exception e) {
            throw new CMISProviderException("", "Failed to create Folder", e);
        }
    }

    @Override
    public void openConnection() throws CMISProviderException {
        try {
            if (this.username == null || this.password == null) {
                throw new CMISProviderException("CMISM-90", "Username and password cannot be null");
            }

            SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
            Map<String, String> parameters = new HashMap<>();

            // JwtPricipal credentials.
            parameters.put(SessionParameter.USER, this.username);
            parameters.put(SessionParameter.PASSWORD, this.password);

            // Connection settings.
            parameters.put(SessionParameter.ATOMPUB_URL, this.urlRepository);
            parameters.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
            parameters.put(SessionParameter.AUTH_HTTP_BASIC, "true");
            parameters.put(SessionParameter.COOKIES, "true");

            // Set the alfresco object factory
            parameters.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");

            // Create userSession.
            // Alfresco only provides one repository.
            Repository repository = sessionFactory.getRepositories(parameters).get(0);
            this.session = repository.createSession();
        } catch (CmisConnectionException e) {
            throw new CMISProviderException("CMISM-01", "Error connecting to repository", e);
        }
    }

    @Override
    @Transactional(Transactional.TxType.NOT_SUPPORTED)
    public void closeConnection() {
        if (this.session != null) {
            this.session.clear();
            this.session = null;
        }
    }

    @Override
    public Boolean validateIntegration() {
        return true;
    }

    private String translatePath(String path) {
        return path.replaceFirst(this.basePath, "");
    }

    @Override
    public String trimDocId(String id) {
        if (id.contains(";")) {
            id = id.substring(0, id.indexOf(";"));
        }

        return id.replace("workspace://SpacesStore/", "");
    }

    @Override
    public String resolveDocId(String id) {
        return String.format("workspace://SpacesStore/%s", trimDocId(id));
    }

    @Override
    public String resolveDocId(String id, String version) {
        if (StringUtils.isEmpty(version)) {
            return resolveDocId(id);
        }

        return String.format("workspace://SpacesStore/%s;%s", trimDocId(id), version);
    }
}
