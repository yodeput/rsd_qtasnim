package com.qtasnim.eoffice.cmis.alfresco;

public class AlfrescoConstants {
    public static final String ALFRESCO_CMISUSERNAME = "ALFRESCO_CMISUSERNAME";
    public static final String ALFRESCO_CMISPASSWORD = "ALFRESCO_CMISPASSWORD";
    public static final String ALFRESCO_CMISREPOSITORY = "ALFRESCO_CMISREPOSITORY";
    public static final String ALFRESCO_CMISBASEPATH = "ALFRESCO_CMISBASEPATH";
}
