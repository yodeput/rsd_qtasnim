package com.qtasnim.eoffice.cmis;

public class CMISInvalidFileExtensionException extends CMISProviderException {
    public CMISInvalidFileExtensionException(String message, String exceptionCode) {
        super(message, exceptionCode);
    }
}
