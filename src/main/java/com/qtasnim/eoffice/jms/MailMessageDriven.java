package com.qtasnim.eoffice.jms;

import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.services.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.LocalBean;
import javax.ejb.MessageDriven;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.Date;

//@MessageDriven(activationConfig = {
//        @ActivationConfigProperty(
//                propertyName = "destinationLookup",
//                propertyValue = Constants.JNDI_MAIL_QUEUE),
//        @ActivationConfigProperty(
//                propertyName = "destinationType",
//                propertyValue = "javax.jms.Queue"),
//        @ActivationConfigProperty(
//                propertyName = "subscriptionDurability",
//                propertyValue="Durable")
//})
@MessageDriven(mappedName = Constants.JNDI_MAIL_QUEUE)
@Stateless
public class MailMessageDriven implements MessageListener {
//    @Inject
//    private Event<Message> event;

    @Inject
    private Logger logger;

    @Override
    public void onMessage(Message message) {

//        event.fire(message);
        try {
            logger.info("Message Received");
            logger.info(new Date().toString());
            logger.info(message.getBody(String.class));

            System.out.println("Message Received");
            System.out.println(new Date().toString());
            System.out.println(message.getBody(String.class));
            Thread.sleep(10000);
        } catch (JMSException | InterruptedException e) {
            logger.error(null, e);
        }
    }
}
