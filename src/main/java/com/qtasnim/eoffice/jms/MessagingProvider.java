package com.qtasnim.eoffice.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.MessageType;
import com.qtasnim.eoffice.services.IMessagingProvider;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.util.ExceptionUtil;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

@Stateless
@LocalBean
public class MessagingProvider implements IMessagingProvider {
    @Resource(mappedName = Constants.JNDI_LOGGER_QUEUE)
    private Queue loggerQueue;

    @Resource(mappedName = Constants.JNDI_DOCUMENT_ACTIVITY_QUEUE)
    private Queue docActQueue;

    @Resource(mappedName = Constants.JNDI_NOTIFICATION_QUEUE)
    private Queue globalQueue;

    @Inject
    private JMSContext jmsContext;

    @Inject
    private Logger logger;

    @Override
    public void send(MessageType type, Consumer<Map<String, String>> onBuild, String message) {
        try {
            switch (type) {
                case LOG:{
                    send(loggerQueue, onBuild, message);

                    break;
                }
                case DOC_ACT:{
                    send(docActQueue, onBuild, message);

                    break;
                }
                case GLOBAL:{
                    send(globalQueue, onBuild, message);

                    break;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), ExceptionUtil.getRealCause(e));
        }
    }

    private void send(Queue queue, Consumer<Map<String, String>> onBuild, String message) throws JsonProcessingException {
        JMSProducer producer = jmsContext.createProducer();
        HashMap<String, String> props = new HashMap<>();
        onBuild.accept(props);
        props.forEach((key, value) -> producer.setProperty(key, value));
        producer.send(queue, getObjectMapper().writeValueAsString(message));
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
