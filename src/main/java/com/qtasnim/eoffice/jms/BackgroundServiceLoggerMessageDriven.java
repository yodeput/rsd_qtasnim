package com.qtasnim.eoffice.jms;

import com.qtasnim.eoffice.Constants;

import javax.ejb.LocalBean;
import javax.ejb.MessageDriven;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * @author R Seno Anggoro <seno at qtasnim.com>
 */
@MessageDriven(mappedName = Constants.JNDI_LOGGER_QUEUE)
@Stateless
@LocalBean
public class BackgroundServiceLoggerMessageDriven implements MessageListener {

    @Inject
    @BackgroundServiceLoggerMessage
    Event<Message> jmsEvent;

    @Override
    public void onMessage(Message msg) {
        jmsEvent.fire(msg);
    }
}
