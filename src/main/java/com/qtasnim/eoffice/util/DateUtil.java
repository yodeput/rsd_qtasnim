package com.qtasnim.eoffice.util;

import com.qtasnim.eoffice.InternalServerErrorException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static final String DATE_FORMAT_BIASA = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
    public static final String DATE_FORMAT= "yyyy-MM-dd'T'HH:mm:ss";

    public String getCurrentISODate() {
        Date date = new Date(System.currentTimeMillis());

        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat(DATE_FORMAT);
        return sdf.format(date);
    }

    public String getCurrentISODate(Date date) {
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat(DATE_FORMAT);
        return sdf.format(date);
    }

    public String formatDate(Date date, String format) {
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public Date getDateFromISOString(String iso) throws InternalServerErrorException {
        try {
            DateFormat df1 = new SimpleDateFormat(DATE_FORMAT);
            return df1.parse(iso);
        } catch (ParseException e) {
            throw new InternalServerErrorException("Invalid date format, must be yyyy-MM-dd'T'HH:mm:ss", "QT-ERR-WS-01", e);
        }
    }

    public Date getDateFromISOString_Biasa(String iso) throws InternalServerErrorException {
        try {
            DateFormat df1 = new SimpleDateFormat(DATE_FORMAT_BIASA);
            return df1.parse(iso);
        } catch (ParseException e) {
            throw new InternalServerErrorException("Invalid date format, must be yyyy-MM-dd'T'HH:mm:ss", "QT-ERR-WS-01", e);
        }
    }

    public Date getBirthDate(String iso) throws InternalServerErrorException {
        try {
            DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
            return df1.parse(iso);
        } catch (ParseException e) {
            throw new InternalServerErrorException("Invalid date format, must be yyyy-MM-dd", "QT-ERR-WS-01", e);
        }
    }

    public Date getDateFromString(String date, String format) {
        try {
            DateFormat df1 = new SimpleDateFormat(format);
            return df1.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public Date getDefValue()throws InternalServerErrorException {
        try {
            String datetime = "9999-12-31T23:59:59.000+07";
            DateFormat df1 = new SimpleDateFormat(DATE_FORMAT);
            return df1.parse(datetime);
        } catch (ParseException e) {
            throw new InternalServerErrorException("Invalid date format, must be yyyy-MM-dd'T'HH:mm:ss", "QT-ERR-WS-01", e);
        }
    }

    public Date getDateTimeStartOld(String iso)throws InternalServerErrorException {
        try {
            iso = iso.substring(0,iso.length() - 16);
            String time = "T00:00:00.000+07";
            String datetime = iso+time;
            DateFormat df1 = new SimpleDateFormat(DATE_FORMAT);
            return df1.parse(datetime);
        } catch (ParseException e) {
            throw new InternalServerErrorException("Invalid date format, must be yyyy-MM-dd'T'HH:mm:ss", "QT-ERR-WS-01", e);
        }
    }

    public Date getDateTimeStart(String iso)throws InternalServerErrorException {
        try {
            iso = iso.substring(0,10);
            String time = "T00:00:00.000";
            String datetime = iso+time;
            DateFormat df1 = new SimpleDateFormat(DATE_FORMAT);
            return df1.parse(datetime);
        } catch (ParseException e) {
            throw new InternalServerErrorException("Invalid date format, must be yyyy-MM-dd'T'HH:mm:ss", "QT-ERR-WS-01", e);
        }
    }

    public Date getDateTimeEndOld(String iso)throws InternalServerErrorException {
        try {
            iso = iso.substring(0,iso.length() - 16);
            String time = "T23:59:59.000+07";
            String datetime = iso+time;
            DateFormat df1 = new SimpleDateFormat(DATE_FORMAT);
            return df1.parse(datetime);
        } catch (ParseException e) {
            throw new InternalServerErrorException("Invalid date format, must be yyyy-MM-dd'T'HH:mm:ss", "QT-ERR-WS-01", e);
        }
    }

    public Date getDateTimeEnd(String iso)throws InternalServerErrorException {
        try {
            iso = iso.substring(0,10);
            String time = "T23:59:59.000";
            String datetime = iso+time;
            DateFormat df1 = new SimpleDateFormat(DATE_FORMAT);
            return df1.parse(datetime);
        } catch (ParseException e) {
            throw new InternalServerErrorException("Invalid date format, must be yyyy-MM-dd'T'HH:mm:ss", "QT-ERR-WS-01", e);
        }
    }

}
