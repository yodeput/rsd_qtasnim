package com.qtasnim.eoffice.util;

public class TemplatingSeparator {
    public static final String SEPARATOR_TAB = "!=t=";
    public static final String SEPARATOR_NEW_LINE = "!=n=";
    public static final String SEPARATOR_NEW_ENTITY = ",";
}
