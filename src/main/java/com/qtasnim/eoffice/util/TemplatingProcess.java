/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.util;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.helpers.TemplatingHelper;
import io.jsonwebtoken.lang.Collections;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.awt.*;
import java.io.*;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author acdwisu
 */
public class TemplatingProcess {
    private Map<String, String> pairMetaNameValueOri;
    private Map<String, String> pairMetaNameValue;
    private String pathTemplateRaw;

    private final String keyPrefix;
    private final String keyPostfix;

    private final int TYPE_SDT_RUN = 1;
    private final int TYPE_SDT_BLOCK = 2;

    private final String xmlCtpMultiLineText;
    private final String xmlCtpMultiLineTextStyled;

    private final String xmlTPropertyBold;
    private final String xmlTPropertyItalic;
    private final String xmlTPropertyUnderlined;
    private final String xmlTPropertyStrikethrough;
    private final String xmlTPropertySubscript;
    private final String xmlTPropertySuperscript;
    private final String xmlTPropertyCaps;
    private final String xmlTPropertyColor;
    private final String xmlTPropertyDStrike;
    private final String xmlTPropertyEmboss;
    private final String xmlTPropertyImprint;
    private final String xmlTPropertyOutline;
    private final String xmlTPropertyShadow;
    private final String xmlTPropertySmallCaps;
    private final String xmlTPropertySize;
    private final String xmlTPropertyVanish;

    public TemplatingProcess(Map<String, String> pairMetaNameValue, String pathTemplateRaw) {
        this();

        this.pairMetaNameValueOri = pairMetaNameValue;

        this.pairMetaNameValue = new HashMap<>();

        this.pathTemplateRaw = pathTemplateRaw;

        initPairKeyValue();
    }

    private TemplatingProcess() {
        this.keyPrefix = "${";
        this.keyPostfix = "}";

//        this.xmlCtpMultiLineText = "<xml-fragment xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"><w:pPr><w:contextualSpacing w:val=\"true\" /><w:ind w:left=\"0\" w:right=\"0\" w:firstLine=\"0\" /><w:jc w:val=\"both\" /><w:spacing w:lineRule=\"auto\" w:line=\"240\" w:after=\"0\" /><w:tabs><w:tab w:val=\"left\" w:pos=\"1418\" w:leader=\"none\" /><w:tab w:val=\"left\" w:pos=\"1701\" w:leader=\"none\" /><w:tab w:val=\"left\" w:pos=\"1843\" w:leader=\"none\" /><w:tab w:val=\"left\" w:pos=\"5103\" w:leader=\"none\" /></w:tabs><w:rPr><w:lang w:val=\"en-US\" /></w:rPr></w:pPr><w:r><w:rPr><w:lang w:val=\"en-US\" /></w:rPr><w:t xml:space=\"preserve\">%s</w:t></w:r><w:r><w:rPr><w:lang w:val=\"en-US\" /></w:rPr></w:r><w:r /></xml-fragment>";
//        this.xmlCtpMultiLineTextStyled = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><xml-fragment xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"><w:pPr><w:contextualSpacing w:val=\"true\" /><w:ind w:left=\"0\" w:right=\"0\" w:firstLine=\"0\" /><w:jc w:val=\"both\" /><w:spacing w:lineRule=\"auto\" w:line=\"240\" w:after=\"0\" /><w:tabs><w:tab w:val=\"left\" w:pos=\"1418\" w:leader=\"none\" /><w:tab w:val=\"left\" w:pos=\"1701\" w:leader=\"none\" /><w:tab w:val=\"left\" w:pos=\"1843\" w:leader=\"none\" /><w:tab w:val=\"left\" w:pos=\"5103\" w:leader=\"none\" /></w:tabs><w:rPr><w:lang w:val=\"en-US\" /></w:rPr></w:pPr><w:r><w:rPr>%s</w:rPr><w:t xml:space=\"preserve\">%s</w:t></w:r></xml-fragment>";

        this.xmlCtpMultiLineText = "<xml-fragment xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"><w:r><w:rPr><w:lang w:val=\"en-US\" /></w:rPr><w:t xml:space=\"preserve\">%s</w:t></w:r><w:r><w:rPr><w:lang w:val=\"en-US\" /></w:rPr></w:r><w:r /></xml-fragment>";
        this.xmlCtpMultiLineTextStyled = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><xml-fragment xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"><w:r><w:rPr>%s</w:rPr><w:t xml:space=\"preserve\">%s</w:t></w:r></xml-fragment>";

        this.xmlTPropertyBold = "<w:b />";
        this.xmlTPropertyItalic = "<w:i />";
        this.xmlTPropertyUnderlined = "<w:u w:val=\"single\" />";
        this.xmlTPropertyStrikethrough = "<w:strike />";
        this.xmlTPropertySubscript = "<w:vertAlign w:val=\"subscript\" />";
        this.xmlTPropertySuperscript = "<w:vertAlign w:val=\"superscript\" />";
        this.xmlTPropertyCaps = "<w:caps w:val=\"true\" />";
        this.xmlTPropertyColor = "<w:color w:val=\"%s\" />";
        this.xmlTPropertyDStrike = "<w:dstrike w:val=\"true\"/>";
        this.xmlTPropertyEmboss = "<w:emboss w:val=\"true\" />";
        this.xmlTPropertyImprint = "<w:imprint w:val=\"true\"/>";
        this.xmlTPropertyOutline = "<outline w:val=\"true\"/>";
        this.xmlTPropertyShadow = "<w:shadow w:val=\"true\"/>";
        this.xmlTPropertySmallCaps = "<w:smallCaps w:val=\"true\"/>";
        this.xmlTPropertySize = "<w:sz w:val=\"%s\"/>";
        this.xmlTPropertyVanish = "<vanish/>";
    }

    public Map<String, String> getPairMetaNameValue() {
        return pairMetaNameValue;
    }

    public void setPairMetaNameValue(Map<String, String> pairMetaNameValue) {
        this.pairMetaNameValue = pairMetaNameValue;
    }

    public String getPathTemplateRaw() {
        return pathTemplateRaw;
    }

    public void setPathTemplateRaw(String pathTemplateRaw) {
        this.pathTemplateRaw = pathTemplateRaw;
    }

    public byte[] process() {
        try {
            XWPFDocument docx = new XWPFDocument(new FileInputStream(this.pathTemplateRaw));

            ByteArrayOutputStream out = new ByteArrayOutputStream();

            Map fields = extractSDTs(docx);

            List<CTSdtRun> sdtRuns = (List<CTSdtRun>) fields.get(TYPE_SDT_RUN);
            List<CTSdtBlock> sdtBlocks = (List<CTSdtBlock>) fields.get(TYPE_SDT_BLOCK);

            templatingConditionalPenerimaSurat(sdtBlocks);
            templatingConditionalPenerimaSuratEksternal(sdtBlocks);
            templatingConditionalPenandatanganSurat(sdtBlocks);
            templatingConditionalTembusanSurat(sdtBlocks, TemplatingHelper.TEMBUSAN_KEY);
            templatingConditionalTembusanSurat(sdtBlocks, TemplatingHelper.TEMBUSAN_EKSTERNAL_KEY);
            templatingConditionalKonseptorSurat(sdtBlocks);
            templatingConditionalPenerimaSurat2(sdtBlocks);
            templatingConditionalPemimpinRapat(sdtBlocks);
            templatingListLampiran(sdtBlocks);
            templatingImage(docx, sdtRuns);

            templatingSingleLine(sdtRuns);
            templatingMultiLineNumbered(sdtBlocks);

            docx.write(out);
            docx.close();
            out.close();

            Logger.getLogger(TemplatingProcessOld.class.getName()).log(Level.INFO, "templating success");

            return out.toByteArray();
        } catch (Exception ex) {
            Logger.getLogger(TemplatingProcessOld.class.getName()).log(Level.SEVERE, ex.getMessage());

            throw new InternalServerErrorException(null, ex);
        }
    }

    private void templatingConditionalPenerimaSurat(List<CTSdtBlock> sdtBlocks) throws XmlException {
        String keyPenerimaSurat = this.keyPrefix+TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_SURAT_KEY)+this.keyPostfix;

        if(this.pairMetaNameValue.containsKey(keyPenerimaSurat)) {
            int jumlahPenerimaSurat = pairMetaNameValue.get(keyPenerimaSurat).split(TemplatingSeparator.SEPARATOR_NEW_ENTITY).length;

            String[] penerimaSuratKondisionalKeys = TemplatingHelper.PENERIMA_SURAT_CONDITIONAL_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

            String keyKondisi1 = this.keyPrefix + TemplatingHelper.toMetadataFormatName(penerimaSuratKondisionalKeys[0]) + this.keyPostfix;
            String keyKondisi2 = this.keyPrefix + TemplatingHelper.toMetadataFormatName(penerimaSuratKondisionalKeys[1]) + this.keyPostfix;

            Map<String, String> pairKeyValue = new HashMap<>();

            if(jumlahPenerimaSurat > 0) {
                if (jumlahPenerimaSurat > 3) {
                    pairKeyValue.put(keyKondisi1, "Para Penerima Terlampir");

                    templatingMultiLine(sdtBlocks, pairKeyValue);

                    pairKeyValue.remove(keyKondisi1);

                    pairKeyValue.put(keyKondisi2, "Penerima:,".concat(pairMetaNameValue.get(keyPenerimaSurat)));

                    templatingMultiLineNumbered(sdtBlocks, pairKeyValue, Collections.arrayToList(new Integer[]{Integer.valueOf(1)}));

                    pairMetaNameValue.remove(keyPenerimaSurat);
                } else {
                    pairKeyValue.put(keyKondisi1, pairMetaNameValue.get(keyPenerimaSurat));

                    if(jumlahPenerimaSurat == 1) {
                        templatingMultiLine(sdtBlocks, pairKeyValue);
                    } else {
                        templatingMultiLineNumbered(sdtBlocks, pairKeyValue);
                    }
                    pairKeyValue.remove(keyKondisi1);

//                    deleteSdt(docx, keyKondisi2, sdtBlocks);
//                    removeSDTContent(keyKondisi2, sdtBlocks);

                    pairKeyValue.put(keyKondisi2, "");
                    templatingMultiLine(sdtBlocks, pairKeyValue);

                    pairMetaNameValue.remove(keyPenerimaSurat);
                }
            }
        }
    }

    private void templatingConditionalPenerimaSuratEksternal(List<CTSdtBlock> sdtBlocks) throws XmlException {
        String keyPenerimaSurat = this.keyPrefix+TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_EKSTERNAL_KEY)+this.keyPostfix;

        if(this.pairMetaNameValue.containsKey(keyPenerimaSurat)) {
            Map<String, String> pairKeyValue = new HashMap<>();

            pairKeyValue.put(keyPenerimaSurat, pairMetaNameValue.get(keyPenerimaSurat));

            templatingMultiLineNumbered(sdtBlocks, pairKeyValue, Collections.arrayToList(new Integer[]{Integer.valueOf(1)}));

            pairMetaNameValue.remove(keyPenerimaSurat);
        }
    }

    private void templatingListLampiran(List<CTSdtBlock> sdtBlocks) throws XmlException {
        String keyLampiran = this.keyPrefix+TemplatingHelper.toMetadataFormatName(TemplatingHelper.LAMPIRAN_LIST_KEY)+this.keyPostfix;

        if(this.pairMetaNameValue.containsKey(keyLampiran)) {
            Map<String, String> pairKeyValue = new HashMap<>();

            pairKeyValue.put(keyLampiran, pairMetaNameValue.get(keyLampiran));

            templatingMultiLineNumbered(sdtBlocks, pairKeyValue, Collections.arrayToList(new Integer[]{Integer.valueOf(1)}));

            pairMetaNameValue.remove(keyLampiran);
        }
    }

    private void templatingImage(XWPFDocument docx, List<CTSdtRun> sdtRuns) throws Exception {
        String keyNoDokumenBarcode = this.keyPrefix+TemplatingHelper.toMetadataFormatName(TemplatingHelper.NO_DOKUMEN_BARCODE_KEY)+this.keyPostfix;
        String keyPenandatanganQrCode = this.keyPrefix+TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENANDATANGAN_QRCODE_KEY)+this.keyPostfix;
        Map<String, String> pairKeyValue = new HashMap<>();

        if(this.pairMetaNameValue.containsKey(keyNoDokumenBarcode)) {
            pairKeyValue.put(keyNoDokumenBarcode, pairMetaNameValue.get(keyNoDokumenBarcode));
            pairMetaNameValue.remove(keyNoDokumenBarcode);
        }

        if(this.pairMetaNameValue.containsKey(keyPenandatanganQrCode)) {
            pairKeyValue.put(keyPenandatanganQrCode, pairMetaNameValue.get(keyPenandatanganQrCode));
            pairMetaNameValue.remove(keyPenandatanganQrCode);
        }

        templatingImage(docx, sdtRuns, pairKeyValue);
    }

    private void templatingImage(XWPFDocument document, List<CTSdtRun> sdtRuns, Map<String,String> pairKeyValue) throws Exception {
        for(CTSdtRun stdRun : sdtRuns) {
            String key = stdRun.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
                String valueString = pairKeyValue.get(key);
                byte[] value = Base64.getDecoder().decode(valueString);

                String blipId = document.addPictureData(value, Document.PICTURE_TYPE_PNG);
                CTSdtContentRun sdtContentRun = stdRun.getSdtContent();
                List<CTR> ctrs = sdtContentRun.getRList();

                if (ctrs != null && !ctrs.isEmpty()) {
                    for (int p = 0; p < ctrs.size(); p++) {
                        CTR ctr = ctrs.get(p);

                        if (ctr != null) {
                            for (int i = 0; i < ctr.getDrawingList().size(); i++) {
                                ctr.removeDrawing(i);
                            }

                            CTDrawing ctDrawing = ctr.addNewDrawing();

                            if (key.contains(TemplatingHelper.toMetadataFormatName(TemplatingHelper.NO_DOKUMEN_BARCODE_KEY))) {
                                ctDrawing.set(getInlineWithGraphic(blipId, document.getNextPicNameNumber(Document.PICTURE_TYPE_PNG), 2804850, 224850));
                            } else if (key.contains(TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENANDATANGAN_QRCODE_KEY))) {
                                ctDrawing.set(getInlineWithGraphic(blipId, document.getNextPicNameNumber(Document.PICTURE_TYPE_PNG), 704850, 704850));
                            }

                            List<CTText> texts = ctr.getTList();
                            if (texts != null && !texts.isEmpty()) {
                                CTText text = texts.get(0);

                                if (text != null) {
                                    text.setStringValue("");
                                    sdtContentRun.setRArray(new CTR[]{ctr});

                                    //modify content control lock
                                    CTLock lock = stdRun.getSdtPr().addNewLock();
                                    lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                                    stdRun.getSdtPr().setLockArray(new CTLock[]{lock});
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void templatingConditionalTembusanSurat(List<CTSdtBlock> sdtBlocks, String tembusanKey) throws XmlException {
        String keyTembusanSurat = this.keyPrefix+TemplatingHelper.toMetadataFormatName(tembusanKey)+this.keyPostfix;

        if(this.pairMetaNameValue.containsKey(keyTembusanSurat)) {
            Map<String, String> pairKeyValue = new HashMap<>();

            pairKeyValue.put(keyTembusanSurat, pairMetaNameValue.get(keyTembusanSurat));

            templatingMultiLineNumbered(sdtBlocks, pairKeyValue, Collections.arrayToList(new Integer[]{Integer.valueOf(1)}));

            pairMetaNameValue.remove(keyTembusanSurat);
        }
    }

    private void templatingConditionalPenandatanganSurat(List<CTSdtBlock> sdtBlocks) throws XmlException {
        String[] conditionalKeys = TemplatingHelper.PENANDATANGAN_CONDITIONAL_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

        String keyPenandatangan1 = keyPrefix+TemplatingHelper.toMetadataFormatName(conditionalKeys[0])+keyPostfix;
        String keyPenandatangan2 = keyPrefix+TemplatingHelper.toMetadataFormatName(conditionalKeys[1])+keyPostfix;

        if(this.pairMetaNameValue.containsKey(keyPenandatangan1)) {
            Map map = new HashMap();

            map.put(keyPenandatangan1, pairMetaNameValue.get(keyPenandatangan1));

            templatingMultiLine(sdtBlocks, map);

            pairMetaNameValue.remove(keyPenandatangan1);
        }

        if(this.pairMetaNameValue.containsKey(keyPenandatangan2)) {
            Map mapKeyValue = new HashMap(),
                mapKeyStyle = new HashMap(),
                mapKeyLineToApplyStyle = new HashMap();

            mapKeyValue.put(keyPenandatangan2, pairMetaNameValue.get(keyPenandatangan2));
            mapKeyStyle.put(keyPenandatangan2, new TProperty(false,false,true, false, TPropertyScriptType.none));
            mapKeyLineToApplyStyle.put(keyPenandatangan2, Arrays.asList(new Integer[]{1}));

            templatingMultiLine(sdtBlocks, mapKeyValue, mapKeyStyle, mapKeyLineToApplyStyle);

            pairMetaNameValue.remove(keyPenandatangan2);
        }
    }

    private void templatingConditionalKonseptorSurat(List<CTSdtBlock> sdtBlocks) throws XmlException {
        String[] conditionalKeys = TemplatingHelper.KONSEPTOR_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

        String keyKonseptor1 = keyPrefix+TemplatingHelper.toMetadataFormatName(conditionalKeys[0])+keyPostfix;
        String keyKonseptor2 = keyPrefix+TemplatingHelper.toMetadataFormatName(conditionalKeys[1])+keyPostfix;

        if(this.pairMetaNameValue.containsKey(keyKonseptor1)) {
            Map map = new HashMap();

            map.put(keyKonseptor1, pairMetaNameValue.get(keyKonseptor1));

            templatingMultiLine(sdtBlocks, map);

            pairMetaNameValue.remove(keyKonseptor1);
        }

        if(this.pairMetaNameValue.containsKey(keyKonseptor2)) {
            Map mapKeyValue = new HashMap(),
                    mapKeyStyle = new HashMap(),
                    mapKeyLineToApplyStyle = new HashMap();

            mapKeyValue.put(keyKonseptor2, pairMetaNameValue.get(keyKonseptor2));
            mapKeyStyle.put(keyKonseptor2, new TProperty(false,false,true, false, TPropertyScriptType.none));
            mapKeyLineToApplyStyle.put(keyKonseptor2, Arrays.asList(new Integer[]{1}));

            templatingMultiLine(sdtBlocks, mapKeyValue, mapKeyStyle, mapKeyLineToApplyStyle);

            pairMetaNameValue.remove(keyKonseptor2);
        }
    }

    private void templatingConditionalPemimpinRapat(List<CTSdtBlock> sdtBlocks) throws XmlException {
        String[] conditionalKeys = TemplatingHelper.PEMIMPIN_RAPAT_CONDITIONAL_KEY.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

        String keyPemimpin1 = keyPrefix+TemplatingHelper.toMetadataFormatName(conditionalKeys[0])+keyPostfix;
        String keyPemimpin2 = keyPrefix+TemplatingHelper.toMetadataFormatName(conditionalKeys[1])+keyPostfix;

        if(this.pairMetaNameValue.containsKey(keyPemimpin1)) {
            Map map = new HashMap();

            map.put(keyPemimpin1, pairMetaNameValue.get(keyPemimpin1));

            templatingMultiLine(sdtBlocks, map);

            pairMetaNameValue.remove(keyPemimpin1);
        }

        if(this.pairMetaNameValue.containsKey(keyPemimpin2)) {
            Map mapKeyValue = new HashMap(),
                    mapKeyStyle = new HashMap(),
                    mapKeyLineToApplyStyle = new HashMap();

            mapKeyValue.put(keyPemimpin2, pairMetaNameValue.get(keyPemimpin2));
            mapKeyStyle.put(keyPemimpin2, new TProperty(false,false,true, false, TPropertyScriptType.none));
            mapKeyLineToApplyStyle.put(keyPemimpin2, Arrays.asList(new Integer[]{1}));

            templatingMultiLine(sdtBlocks, mapKeyValue, mapKeyStyle, mapKeyLineToApplyStyle);

            pairMetaNameValue.remove(keyPemimpin2);
        }
    }

    private void templatingConditionalPenerimaSurat2(List<CTSdtBlock> sdtBlocks) throws XmlException {
        String keyPenerimaSurat = this.keyPrefix+TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_SURAT_LIST_KEY)+this.keyPostfix;

        if(this.pairMetaNameValue.containsKey(keyPenerimaSurat)) {
            Map<String, String> pairKeyValue = new HashMap<>();

            pairKeyValue.put(keyPenerimaSurat, pairMetaNameValue.get(keyPenerimaSurat));

            templatingMultiLineNumbered(sdtBlocks, pairKeyValue, Collections.arrayToList(new Integer[]{Integer.valueOf(1)}));

            pairMetaNameValue.remove(keyPenerimaSurat);
        }
    }

    //http://apache-poi.1045710.n5.nabble.com/Unable-to-replace-title-of-docx-using-apache-poi-in-java-tp5733971p5733976.html
    private Map<Integer, List> extractSDTs(XWPFDocument document) throws XmlException {
        List<CTSdtRun> sdtRuns = new LinkedList<>();
        List<CTSdtBlock> sdtBlocks = new LinkedList<>();

        XmlCursor xmlcursor = document.getDocument().getBody().newCursor();

        while (xmlcursor.hasNextToken()) {
            XmlCursor.TokenType tokentype = xmlcursor.toNextToken();
            if (tokentype.isStart()) {
                if (xmlcursor.getObject() instanceof CTSdtRun)
                    sdtRuns.add((CTSdtRun) xmlcursor.getObject());
                else if (xmlcursor.getObject() instanceof CTSdtBlock)
                    sdtBlocks.add((CTSdtBlock) xmlcursor.getObject());
            }
        }

        Map<Integer, List> sdts = new HashMap<>();

        sdts.put(TYPE_SDT_RUN, sdtRuns);
        sdts.put(TYPE_SDT_BLOCK, sdtBlocks);

        return sdts;
    }

    private boolean deleteSdt(XWPFDocument document, String tagName) {
        XmlCursor xmlcursor = document.getDocument().getBody().newCursor();

        while (xmlcursor.hasNextToken()) {
            XmlCursor.TokenType tokentype = xmlcursor.toNextToken();
            if (tokentype.isStart()) {
                if (xmlcursor.getObject() instanceof CTSdtRun) {
                    CTSdtRun sdt = (CTSdtRun) xmlcursor.getObject();

                    if(sdt.getSdtPr().getTagList().get(0).getVal().equals(tagName)) {
                        return xmlcursor.removeXml();
                    }
                }
                else if (xmlcursor.getObject() instanceof CTSdtBlock) {
                    CTSdtBlock sdt = (CTSdtBlock) xmlcursor.getObject();

                    if(sdt.getSdtPr().getTagList().get(0).getVal().equals(tagName)) {
                        return xmlcursor.removeXml();
                    }
                }
            }
        }

        return false;
    }

    private boolean deleteSdt(XWPFDocument document, String tagName, List<CTSdtBlock> sdtBlocks) {
        XmlCursor xmlcursor = document.getDocument().getBody().newCursor();

        while (xmlcursor.hasNextToken()) {
            XmlCursor.TokenType tokentype = xmlcursor.toNextToken();

            if (tokentype.isStart()) {
                if (xmlcursor.getObject() instanceof CTSdtRun) {
                    CTSdtRun sdt = (CTSdtRun) xmlcursor.getObject();

                    if(sdt.getSdtPr().getTagList().get(0).getVal().equals(tagName)) {
                        return xmlcursor.removeXml();
                    }
                }
                else if (xmlcursor.getObject() instanceof CTSdtBlock) {
                    CTSdtBlock sdt = (CTSdtBlock) xmlcursor.getObject();

                    if(sdt.getSdtPr().getTagList().get(0).getVal().equals(tagName)) {
                        sdtBlocks.remove(sdt);
                        return xmlcursor.removeXml();
                    }
                }
            }
        }

        return false;
    }

    private void removeSDTContent(String tagName, List<CTSdtBlock> sdtBlocks) {
        for(CTSdtBlock sdtBlock : sdtBlocks) {
            if(sdtBlock.getSdtPr().getTagList().get(0).getVal().equals(tagName)) {
                CTP ctp = sdtBlock.getSdtContent().addNewP();

                sdtBlock.getSdtContent().setPArray(new CTP[]{ctp});
            }
        }
    }

    private void templating(XWPFDocument document) throws XmlException {
        templating(document, this.pairMetaNameValue);
    }

    private void templating(XWPFDocument document, Map<String,String> pairKeyValue) throws XmlException {
        Map sdts = extractSDTs(document);

        List<CTSdtRun> sdtRuns = (List<CTSdtRun>) sdts.get(TYPE_SDT_RUN);
        List<CTSdtBlock> sdtBlocks = (List<CTSdtBlock>) sdts.get(TYPE_SDT_BLOCK);

        templatingSingleLine(sdtRuns, pairKeyValue);

        templatingMultiLine(sdtBlocks, pairKeyValue);
    }

    private void templatingSingleLine(List<CTSdtRun> sdtRuns) {
        templatingSingleLine(sdtRuns, this.pairMetaNameValue);
    }

    private void templatingSingleLine(List<CTSdtRun> sdtRuns, Map<String,String> pairKeyValue) {
        for(CTSdtRun stdRun : sdtRuns) {
            // replace values
            String key = stdRun.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
//                String value = pairKeyValue.get(key);
//
//                CTSdtContentRun sdtContentRun = stdRun.getSdtContent();
//
//                CTR ctr = sdtContentRun.addNewR();
//
//                CTText text = ctr.addNewT();
//                text.setStringValue(value);
//
//                ctr.setTArray(new CTText[]{text});
//
//                sdtContentRun.setRArray(new CTR[]{ctr});
//
//                //modify content control lock
//                CTLock lock = stdRun.getSdtPr().addNewLock();
//                lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));
//
//                stdRun.getSdtPr().setLockArray(new CTLock[]{lock});

                String value = pairKeyValue.get(key);

                CTSdtContentRun sdtContentRun = stdRun.getSdtContent();

                List<CTR> ctrs = sdtContentRun.getRList();

                if(ctrs != null && !ctrs.isEmpty()) {
                    for(int p=0; p<ctrs.size(); p++) {
                        CTR ctr = ctrs.get(p);

                        if (ctr != null) {
                            List<CTText> texts = ctr.getTList();

                            if (texts != null && !texts.isEmpty()) {
                                CTText text = texts.get(0);

                                if (text != null) {
                                    text.setStringValue(value);

                                    ctr.setTArray(new CTText[]{text});

                                    sdtContentRun.setRArray(new CTR[]{ctr});

                                    //modify content control lock
                                    CTLock lock = stdRun.getSdtPr().addNewLock();
                                    lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                                    stdRun.getSdtPr().setLockArray(new CTLock[]{lock});
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void templatingSingleLine(List<CTSdtRun> sdtRuns, Map<String,String> pairKeyValue, Map<String,TProperty> pairKeyProperty) {
        for(CTSdtRun stdRun : sdtRuns) {
            // replace values
            String key = stdRun.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
                String value = pairKeyValue.get(key);
                TProperty property = pairKeyProperty.get(key);

                CTR ctr = stdRun.getSdtContent().addNewR();

                CTText text = ctr.addNewT();
                text.setStringValue(value);

                ctr.setTArray(new CTText[]{text});

                CTRPr ctrPr = ctr.addNewRPr();

                if(property.bold) {
                    CTOnOff ctOnOff = ctrPr.addNewB();
                    ctOnOff.setVal(STOnOff.ON);
                    ctrPr.setB(ctOnOff);
                }
                if(property.italic) {
                    CTOnOff ctOnOff = ctrPr.addNewI();
                    ctOnOff.setVal(STOnOff.ON);
                    ctrPr.setB(ctOnOff);
                }
                if(property.strikethrough) {
                    CTOnOff ctOnOff = ctrPr.addNewStrike();
                    ctOnOff.setVal(STOnOff.ON);
                    ctrPr.setB(ctOnOff);
                }
                if(property.underlined) {
                    CTUnderline ctUnderline = ctrPr.addNewU();
                    ctUnderline.setVal(STUnderline.SINGLE);
                    ctrPr.setU(ctUnderline);
                }
                switch (property.scriptType) {
                    case subscript:
                        CTVerticalAlignRun ctVerticalAlignRun = ctrPr.addNewVertAlign();
                        ctVerticalAlignRun.setVal(STVerticalAlignRun.SUBSCRIPT);
                        ctrPr.setVertAlign(ctVerticalAlignRun);
                        break;
                    case superscript:
                        CTVerticalAlignRun ctVerticalAlignRun2 = ctrPr.addNewVertAlign();
                        ctVerticalAlignRun2.setVal(STVerticalAlignRun.SUPERSCRIPT);
                        ctrPr.setVertAlign(ctVerticalAlignRun2);
                        break;
                }

                ctr.setRPr(ctrPr);

                stdRun.getSdtContent().setRArray(new CTR[]{ctr});

                //modify content control lock
                CTLock lock = stdRun.getSdtPr().addNewLock();
                lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                stdRun.getSdtPr().setLockArray(new CTLock[]{lock});
            }
        }
    }

    private void templatingMultiLine(List<CTSdtBlock> sdtBlocks) throws XmlException {
        templatingMultiLine(sdtBlocks, this.pairMetaNameValue);
    }

    /*
    memberi style yg sama pada setiap line di masing2 key
     */
    private void templatingMultiLine(List<CTSdtBlock> sdtBlocks, Map<String,String> pairKeyValue, Map<String, TProperty> pairKeyProperty) throws XmlException {
        for(CTSdtBlock sdtBlock : sdtBlocks) {
            String key = sdtBlock.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
                String value = pairKeyValue.get(key);
                TProperty property = pairKeyProperty.get(key);

                String[] values = value.split(",");

                // replace values
                CTSdtContentBlock contentBlock = sdtBlock.getSdtContent();

                CTP[] ctps = new CTP[values.length];

                for(int i=0; i<values.length; i++) {
                    ctps[i] = CTP.Factory.parse(String.format(xmlCtpMultiLineTextStyled, generateXmlTProperties(property), values[i]));
                }

                contentBlock.setPArray(ctps);

                //modify content control lock
                CTLock lock = sdtBlock.getSdtPr().addNewLock();
                lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                sdtBlock.getSdtPr().setLockArray(new CTLock[]{lock});
            }
        }
    }

    /*
    hanya memberi style pada line tertentu yg didefinisikan di masing2 key
     */
    private void templatingMultiLine(List<CTSdtBlock> sdtBlocks, Map<String,String> pairKeyValue, Map<String, TProperty> pairKeyProperty, Map<String,List<Integer>> pairKeyLineToApplyStyle) throws XmlException {
        for(CTSdtBlock sdtBlock : sdtBlocks) {
            String key = sdtBlock.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
                String value = pairKeyValue.get(key);
                TProperty property = pairKeyProperty.get(key);
                List<Integer> lineToApplyStyle = pairKeyLineToApplyStyle.get(key);

                String[] values = value.split(",");

                // replace values
                CTSdtContentBlock contentBlock = sdtBlock.getSdtContent();

                List<CTP> oriCtps = contentBlock.getPList();

                CTP[] ctps = new CTP[values.length];

                for (int i = 0; i < values.length; i++) {
//                    if(lineToApplyStyle.contains(i+1)) ctps[i] = CTP.Factory.parse(String.format(xmlCtpMultiLineTextStyled, generateXmlTProperties(property), values[i]));
//                    else ctps[i] = CTP.Factory.parse(String.format(xmlCtpMultiLineText, values[i]));

                    ctps[i] = CTP.Factory.newInstance();

                    CTR firstR = ctps[i].addNewR();

                    CTText text = firstR.addNewT();
                    text.setStringValue(values[i]);

                    firstR.setTArray(new CTText[]{text});

                    if(oriCtps != null && !oriCtps.isEmpty()) {
                        CTP firstP = oriCtps.get(0);

                        if(firstP != null) {
                            List<CTR> ctrs = firstP.getRList();

                            if (ctrs != null && !ctrs.isEmpty()) {
                                CTRPr rpr = ctrs.get(0).getRPr();

                                if (rpr != null) {
                                    if (lineToApplyStyle.contains(i + 1)) {
                                        firstR.setRPr(alterCTRPrSyncToProperties(property, (CTRPr) rpr.copy()));
                                    } else {
                                        firstR.setRPr(rpr);
                                    }
                                }
                            }

                            ctps[i].setPPr(firstP.getPPr());
                        }
                    }

                    ctps[i].setRArray(new CTR[]{firstR});
                }

                contentBlock.setPArray(ctps);

                //modify content control lock
                CTLock lock = sdtBlock.getSdtPr().addNewLock();
                lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                sdtBlock.getSdtPr().setLockArray(new CTLock[]{lock});
            }
        }
    }

    /*
    memetakan setiap style untuk masing2 line di setiap key
     */
    private void templatingMultiLineStylePerLine(List<CTSdtBlock> sdtBlocks, Map<String,String> pairKeyValue, Map<String, Map<Integer, TProperty>> pairKeyLineToApplyStyle) throws XmlException {
        for(CTSdtBlock sdtBlock : sdtBlocks) {
            String key = sdtBlock.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
                String value = pairKeyValue.get(key);
                Map<Integer, TProperty> propertyPerLine = pairKeyLineToApplyStyle.get(key);

                String[] values = value.split(",");

                // replace values
                CTSdtContentBlock contentBlock = sdtBlock.getSdtContent();

                CTP[] ctps = new CTP[values.length];

                for(int i=0; i<values.length; i++) {
                    if(propertyPerLine.containsKey(i+1)) {
                        TProperty property = propertyPerLine.get(i + 1);
                        ctps[i] = CTP.Factory.parse(String.format(xmlCtpMultiLineTextStyled, generateXmlTProperties(property), values[i]));
                    } else ctps[i] = CTP.Factory.parse(String.format(xmlCtpMultiLineText, values[i]));

                    ctps[i].setPPr(contentBlock.getPList().get(0).getPPr());
                }

                contentBlock.setPArray(ctps);

                //modify content control lock
                CTLock lock = sdtBlock.getSdtPr().addNewLock();
                lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                sdtBlock.getSdtPr().setLockArray(new CTLock[]{lock});
            }
        }
    }

    private void templatingMultiLine(List<CTSdtBlock> sdtBlocks, Map<String,String> pairKeyValue) throws XmlException {
        for(CTSdtBlock sdtBlock : sdtBlocks) {
            String key = sdtBlock.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
                String value = pairKeyValue.get(key);

                String[] values = value.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

                // replace values
                CTSdtContentBlock contentBlock = sdtBlock.getSdtContent();

                List<CTP> oriCtps = contentBlock.getPList();

                CTP[] ctps = new CTP[values.length];

                for (int i = 0; i < values.length; i++) {
//                    ctps[i] = CTP.Factory.parse(String.format(xmlCtpMultiLineText, values[i].replace(TemplatingHelper.SUBSTITUTE_COMMA, ",")));


//                    ctps[i] = CTP.Factory.newInstance();
//
//                    CTR firstR = firstP.getRArray(0);
//
//                    CTText text = firstR.getTArray(0);
//                    text.setStringValue(values[i].replace(TemplatingHelper.SUBSTITUTE_COMMA, ","));
//
//                    firstR.setTArray(new CTText[]{text});
//
//                    ctps[i].setRArray(new CTR[]{firstR});
//                    ctps[i].setPPr(firstP.getPPr());


                    ctps[i] = CTP.Factory.newInstance();

                    CTR firstR = ctps[i].addNewR();

                    CTText text = firstR.addNewT();
                    text.setStringValue(values[i].replace(TemplatingHelper.SUBSTITUTE_COMMA, ","));

                    firstR.setTArray(new CTText[]{text});

                    if(oriCtps != null && !oriCtps.isEmpty()) {
                        CTP firstP = oriCtps.get(0);

                        if(firstP != null) {
                            List<CTR> ctrs = firstP.getRList();

                            if(ctrs != null && !ctrs.isEmpty()) {
                                CTR ctr = ctrs.get(0);

                                if(ctr != null) {
                                    CTRPr rpr = ctr.getRPr();

                                    if (rpr != null) {
                                        firstR.setRPr(rpr);
                                    }
                                }
                            }

                            ctps[i].setPPr(firstP.getPPr());
                        }
                    }

                    ctps[i].setRArray(new CTR[]{firstR});
                }

                contentBlock.setPArray(ctps);

                //modify content control lock
                CTLock lock = sdtBlock.getSdtPr().addNewLock();
                lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                sdtBlock.getSdtPr().setLockArray(new CTLock[]{lock});
            }
        }
    }

    private void templatingMultiLineNumbered(List<CTSdtBlock> sdtBlocks) throws XmlException {
        templatingMultiLineNumbered(sdtBlocks, pairMetaNameValue);
    }

    private void templatingMultiLineNumbered(List<CTSdtBlock> sdtBlocks, Map<String,String> pairKeyValue) throws XmlException {
        for(CTSdtBlock sdtBlock : sdtBlocks) {
            String key = sdtBlock.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
                String value = pairKeyValue.get(key);

                String[] values = value.split(",");

                // replace values
                CTSdtContentBlock contentBlock = sdtBlock.getSdtContent();

                List<CTP> oriCtps = contentBlock.getPList();

                CTP[] ctps = new CTP[values.length];

                for(int i=0; i<values.length; i++) {
//                    if(StringUtils.isNotBlank(values[i])){
//                        ctps[i] = CTP.Factory.parse(String.format(xmlCtpMultiLineText, i + 1 + ".  " + values[i]));
//                    }else{
//                        ctps[i] = CTP.Factory.parse(String.format(xmlCtpMultiLineText," "));
//                    }


//                    ctps[i] = CTP.Factory.newInstance();
//
//                    CTR firstR = firstP.getRArray(0);
//
//                    CTText text = firstR.getTArray(0);
//                    text.setStringValue(i + 1 + ".  " + values[i]);
//
//                    firstR.setTArray(new CTText[]{text});
//
//                    ctps[i].setRArray(new CTR[]{firstR});
//                    ctps[i].setPPr(firstP.getPPr());


                    ctps[i] = CTP.Factory.newInstance();

                    CTR firstR = ctps[i].addNewR();

                    CTText text = firstR.addNewT();
                    text.setStringValue(i + 1 + ".  " + values[i]);

                    firstR.setTArray(new CTText[]{text});

                    if(oriCtps != null && !oriCtps.isEmpty()) {
                        CTP firstP = oriCtps.get(0);

                        if(firstP != null) {
                            List<CTR> ctrs = firstP.getRList();

                            if(ctrs != null && !ctrs.isEmpty()) {
                                CTR ctr = ctrs.get(0);

                                if(ctr != null) {
                                    CTRPr rpr = ctr.getRPr();

                                    firstR.setRPr(rpr);
                                }
                            }

                            ctps[i].setPPr(firstP.getPPr());
                        }
                    }

                    ctps[i].setRArray(new CTR[]{firstR});
                }

                contentBlock.setPArray(ctps);

                //modify content control lock
                CTLock lock = sdtBlock.getSdtPr().addNewLock();
                lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                sdtBlock.getSdtPr().setLockArray(new CTLock[]{lock});
            }
        }
    }

    /*
    templating multi line dengan penomoran dengan pengecualian penomoran pada line yg didefinisikan
     */
    private void templatingMultiLineNumbered(List<CTSdtBlock> sdtBlocks, Map<String,String> pairKeyValue, List<Integer> linesException) throws XmlException {
        for(CTSdtBlock sdtBlock : sdtBlocks) {
            String key = sdtBlock.getSdtPr().getTagList().get(0).getVal();

            if(pairKeyValue.containsKey(key)) {
                String value = pairKeyValue.get(key);

                String[] values = value.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);

                // replace values
                CTSdtContentBlock contentBlock = sdtBlock.getSdtContent();

                List<CTP> oriCtps = contentBlock.getPList();

                CTP[] ctps = new CTP[values.length];

                for (int i = 0, j = 1; i < values.length; i++, j++) {
//                    String newVal = values[i];
//
//                    if(linesException.contains(Integer.valueOf(i+1))){
//                        j=0;
//                    } else {
//                        newVal = j+".  "+values[i];
//                    }
//                    ctps[i] = CTP.Factory.parse(String.format(xmlCtpMultiLineText, newVal));

                    String newVal = values[i];

                    if (linesException.contains(Integer.valueOf(i + 1))) {
                        j = 0;
                    } else {
                        newVal = j + ".  " + values[i];
                    }


//                    ctps[i] = CTP.Factory.newInstance();
//
//                    CTR firstR = firstP.getRArray(0);
//
//                    CTText text = firstR.getTArray(0);
//                    text.setStringValue(newVal);
//
//                    firstR.setTArray(new CTText[]{text});
//
//                    ctps[i].setRArray(new CTR[]{firstR});
//                    ctps[i].setPPr(firstP.getPPr());


                    ctps[i] = CTP.Factory.newInstance();

                    CTR firstR = ctps[i].addNewR();

                    CTText text = firstR.addNewT();
                    text.setStringValue(newVal);

                    firstR.setTArray(new CTText[]{text});

                    if(oriCtps != null && !oriCtps.isEmpty()) {
                        CTP firstP = oriCtps.get(0);

                        if(firstP != null) {
                            List<CTR> ctrs = firstP.getRList();

                            if(ctrs != null && !ctrs.isEmpty()) {
                                CTR ctr = ctrs.get(0);

                                if(ctr != null) {
                                    CTRPr rpr = ctr.getRPr();

                                    if (rpr != null) {
                                        firstR.setRPr(rpr);
                                    }
                                }
                            }

                            ctps[i].setPPr(firstP.getPPr());
                        }
                    }

                    ctps[i].setRArray(new CTR[]{firstR});
                }

                contentBlock.setPArray(ctps);

                //modify content control lock
                CTLock lock = sdtBlock.getSdtPr().addNewLock();
                lock.setVal(STLock.Enum.forInt(STLock.INT_SDT_CONTENT_LOCKED));

                sdtBlock.getSdtPr().setLockArray(new CTLock[]{lock});
            }
        }
    }

    private void initSpecialMetadata() {
        String tglDokumenKey = TemplatingHelper.toMetadataFormatName(TemplatingHelper.TANGGAL_DOKUMEN_KEY);

        if(this.pairMetaNameValueOri.containsKey(tglDokumenKey)) {
            Date tglDokumen = new DateUtil().getDateFromISOString(this.pairMetaNameValueOri.get(tglDokumenKey));

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMMMMMMMMMM yyyy", Locale.forLanguageTag("id-ID"));

            this.pairMetaNameValueOri.replace(tglDokumenKey, simpleDateFormat.format(tglDokumen));
        } else {
            this.pairMetaNameValueOri.put(tglDokumenKey, "___");
        }

        String noDokumenKey = TemplatingHelper.toMetadataFormatName(TemplatingHelper.NO_DOKUMEN_KEY);

        if(!this.pairMetaNameValueOri.containsKey(noDokumenKey)) {
            this.pairMetaNameValueOri.put(noDokumenKey, "___");
        }

        String tembusanKey = TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_KEY);

        if(!this.pairMetaNameValueOri.containsKey(tembusanKey)) {
            this.pairMetaNameValueOri.put(tembusanKey, "___");
        }

        String tembusanEksternalKey = TemplatingHelper.toMetadataFormatName(TemplatingHelper.TEMBUSAN_EKSTERNAL_KEY);

        if(!this.pairMetaNameValueOri.containsKey(tembusanEksternalKey)) {
            this.pairMetaNameValueOri.put(tembusanEksternalKey, "___");
        }

        String penerimaEksternalKey = TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_EKSTERNAL_KEY);

        if(!this.pairMetaNameValueOri.containsKey(penerimaEksternalKey)){
            this.pairMetaNameValueOri.put(penerimaEksternalKey, "___");
        }
    }

    private void initPairKeyValue() {
        initSpecialMetadata();

        for(String key : this.pairMetaNameValueOri.keySet()) {
            this.pairMetaNameValue.put(keyPrefix+key+keyPostfix, this.pairMetaNameValueOri.get(key));
        }
    }

    private String generateXmlTProperties(TProperty properties) {
        String xml = "";

        if(properties.italic != null && properties.italic) xml = xml.concat(this.xmlTPropertyItalic);

        if(properties.bold != null && properties.bold) xml =  xml.concat(this.xmlTPropertyBold);

        if(properties.underlined != null && properties.underlined) xml = xml.concat(this.xmlTPropertyUnderlined);

        if(properties.strikethrough != null && properties.strikethrough) xml = xml.concat(this.xmlTPropertyStrikethrough);

        if(properties.scriptType != null) {
            switch (properties.scriptType) {
                case superscript:
                    xml.concat(this.xmlTPropertySuperscript);
                    break;
                case subscript:
                    xml.concat(this.xmlTPropertySubscript);
                    break;
            }
        }

        if(properties.caps != null && properties.caps) xml = xml.concat(this.xmlTPropertyCaps);

        if(properties.colorHex != null && new HexValidator().validate(properties.colorHex))
            xml = xml.concat(String.format(this.xmlTPropertyColor, properties.colorHex));

        if(properties.doubleStrikethrough != null && properties.doubleStrikethrough)
            xml = xml.concat(this.xmlTPropertyDStrike);

        if(properties.emboss != null && properties.emboss)
            xml = xml.concat(this.xmlTPropertyEmboss);

        if(properties.imprint != null && properties.imprint)
            xml = xml.concat(this.xmlTPropertyImprint);

        if(properties.outline != null && properties.outline)
            xml = xml.concat(this.xmlTPropertyOutline);

        if(properties.shadow != null && properties.shadow)
            xml = xml.concat(this.xmlTPropertyShadow);

        if(properties.smallCaps != null && properties.smallCaps)
            xml = xml.concat(this.xmlTPropertySmallCaps);

        if(properties.vanish != null && properties.vanish)
            xml = xml.concat(this.xmlTPropertyVanish);

        if(properties.size != null)
            xml = xml.concat(String.format(this.xmlTPropertySize, String.valueOf(properties.size*2)));

        return xml;
    }

    private CTRPr generateCTRPrFromProperties(TProperty properties) {
        CTRPr rpr = CTRPr.Factory.newInstance();

        rpr = generateCTRPrFromProperties(properties, rpr);

        return rpr;
    }

    private CTRPr generateCTRPrFromProperties(TProperty properties, CTRPr rpr) {
        if(properties.italic != null && properties.italic) {
            rpr.addNewI().setVal(STOnOff.ON);
        }

        if(properties.bold != null && properties.bold) {
            rpr.addNewB().setVal(STOnOff.ON);
        }

        if(properties.underlined != null && properties.underlined) {
            rpr.addNewU().setVal(STUnderline.SINGLE);
        }

        if(properties.strikethrough != null && properties.strikethrough) {
            rpr.addNewStrike().setVal(STOnOff.ON);
        }

        if(properties.scriptType != null) {
            switch (properties.scriptType) {
                case superscript:
                    rpr.addNewVertAlign().setVal(STVerticalAlignRun.SUPERSCRIPT);

                    break;
                case subscript:
                    rpr.addNewVertAlign().setVal(STVerticalAlignRun.SUBSCRIPT);
                    break;
            }
        }

        if(properties.caps != null && properties.caps) {
            rpr.addNewCaps().setVal(STOnOff.ON);
        }

        if(properties.colorHex != null && new HexValidator().validate(properties.colorHex)) {
            rpr.addNewColor().setThemeColor(STThemeColor.Enum.forString(properties.colorHex));
        }

        if(properties.doubleStrikethrough != null && properties.doubleStrikethrough) {
            rpr.addNewDstrike().setVal(STOnOff.ON);
        }

        if(properties.emboss != null && properties.emboss) {
            rpr.addNewEmboss().setVal(STOnOff.ON);
        }

        if(properties.imprint != null && properties.imprint) {
            rpr.addNewImprint().setVal(STOnOff.ON);
        }

        if(properties.outline != null && properties.outline) {
            rpr.addNewOutline().setVal(STOnOff.ON);
        }

        if(properties.shadow != null && properties.shadow) {
            rpr.addNewShadow().setVal(STOnOff.ON);
        }

        if(properties.smallCaps != null && properties.smallCaps) {
            rpr.addNewSmallCaps().setVal(STOnOff.ON);
        }

        if(properties.vanish != null && properties.vanish) {
            rpr.addNewVanish().setVal(STOnOff.ON);
        }

        if(properties.size != null) {
            rpr.addNewSz().setVal(BigInteger.valueOf(properties.size*2));
        }

        return rpr;
    }

    private CTRPr alterCTRPrSyncToProperties(TProperty properties, CTRPr oldRPr) {
        CTRPr newRPr = generateCTRPrFromProperties(properties, oldRPr);

        return newRPr;
    }

    public byte[] deleteWatermark(String pathTemplateRaw) {
        try {
            XWPFDocument document = new XWPFDocument(new FileInputStream(pathTemplateRaw));
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            try {
                Node watermarkNode = null;
                List<CTP> pList = document.getHeaderFooterPolicy().getDefaultHeader()._getHdrFtr().getPList();

                for (CTP ctp: Optional.ofNullable(pList).orElse(new ArrayList<>())) {
                    if (ctp != null) {
                        List<CTR> rnodes = ctp.getRList();

                        for (CTR ctr : rnodes) {
                            NodeList list = ctr.getDomNode().getChildNodes();

                            for (int i = 0; i < list.getLength(); i++) {
                                Node node = list.item(i);

                                if (node.getPrefix().equals("mc")) {
                                    watermarkNode = node;
                                    break;
                                }
                            }

                            if (watermarkNode != null) {
                                break;
                            }
                        }

                        if (watermarkNode != null) {
                            watermarkNode.getParentNode().removeChild(watermarkNode);

                            break;
                        }
                    }
                }

                document.getHeaderFooterPolicy().getDefaultHeader()._getHdrFtr().getSdtList().clear();
            } catch (Exception e) {
                e.printStackTrace();
            }

            document.write(out);
            document.close();
            out.close();

            Logger.getLogger(TemplatingProcess.class.getName()).log(Level.INFO, "templating success");

            return out.toByteArray();
        } catch (IOException | NumberFormatException ex) {
            Logger.getLogger(TemplatingProcess.class.getName()).log(Level.SEVERE, ex.getMessage());

            throw new InternalServerErrorException(null, ex);
        }
    }

    private static XmlObject getInlineWithGraphic(String drawingDescr, int id, int width, int height) throws Exception {
        String xml =
                "<wp:inline distT=\"0\" distB=\"0\" distL=\"0\" distR=\"0\" xmlns:wpc=\"http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas\" xmlns:cx=\"http://schemas.microsoft.com/office/drawing/2014/chartex\" xmlns:cx1=\"http://schemas.microsoft.com/office/drawing/2015/9/8/chartex\" xmlns:cx2=\"http://schemas.microsoft.com/office/drawing/2015/10/21/chartex\" xmlns:cx3=\"http://schemas.microsoft.com/office/drawing/2016/5/9/chartex\" xmlns:cx4=\"http://schemas.microsoft.com/office/drawing/2016/5/10/chartex\" xmlns:cx5=\"http://schemas.microsoft.com/office/drawing/2016/5/11/chartex\" xmlns:cx6=\"http://schemas.microsoft.com/office/drawing/2016/5/12/chartex\" xmlns:cx7=\"http://schemas.microsoft.com/office/drawing/2016/5/13/chartex\" xmlns:cx8=\"http://schemas.microsoft.com/office/drawing/2016/5/14/chartex\" xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:aink=\"http://schemas.microsoft.com/office/drawing/2016/ink\" xmlns:am3d=\"http://schemas.microsoft.com/office/drawing/2017/model3d\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" xmlns:m=\"http://schemas.openxmlformats.org/officeDocument/2006/math\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:wp14=\"http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing\" xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" xmlns:w14=\"http://schemas.microsoft.com/office/word/2010/wordml\" xmlns:w15=\"http://schemas.microsoft.com/office/word/2012/wordml\" xmlns:w16cid=\"http://schemas.microsoft.com/office/word/2016/wordml/cid\" xmlns:w16se=\"http://schemas.microsoft.com/office/word/2015/wordml/symex\" xmlns:wpg=\"http://schemas.microsoft.com/office/word/2010/wordprocessingGroup\" xmlns:wpi=\"http://schemas.microsoft.com/office/word/2010/wordprocessingInk\" xmlns:wne=\"http://schemas.microsoft.com/office/word/2006/wordml\" xmlns:wps=\"http://schemas.microsoft.com/office/word/2010/wordprocessingShape\">\n" +
                        "  <wp:extent cx=\""+width+"\" cy=\""+height+"\"/>\n" +
                        "  <wp:effectExtent l=\"0\" t=\"0\" r=\"0\" b=\"0\"/>\n" +
                        "  <wp:docPr id=\""+id+"\" name=\"Picture "+id+"\" descr=\"A picture containing drawing\n" +
                        "\n" +
                        "Description automatically generated\"/>\n" +
                        "  <wp:cNvGraphicFramePr>\n" +
                        "    <a:graphicFrameLocks noChangeAspect=\"1\" xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\"/>\n" +
                        "  </wp:cNvGraphicFramePr>\n" +
                        "  <a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">\n" +
                        "    <a:graphicData uri=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">\n" +
                        "      <pic:pic xmlns:pic=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">\n" +
                        "        <pic:nvPicPr>\n" +
                        "          <pic:cNvPr id=\""+id+"\" name=\""+drawingDescr+"\"/>\n" +
                        "          <pic:cNvPicPr/>\n" +
                        "        </pic:nvPicPr>\n" +
                        "        <pic:blipFill>\n" +
                        "          <a:blip r:embed=\""+drawingDescr+"\" cstate=\"print\">\n" +
                        "            <a:extLst>\n" +
                        "              <a:ext uri=\"{28A0092B-C50C-407E-A947-70E740481C1C}\">\n" +
                        "                <a14:useLocalDpi val=\"0\" xmlns:a14=\"http://schemas.microsoft.com/office/drawing/2010/main\"/>\n" +
                        "              </a:ext>\n" +
                        "            </a:extLst>\n" +
                        "          </a:blip>\n" +
                        "          <a:stretch>\n" +
                        "            <a:fillRect/>\n" +
                        "          </a:stretch>\n" +
                        "        </pic:blipFill>\n" +
                        "        <pic:spPr>\n" +
                        "          <a:xfrm>\n" +
                        "            <a:off x=\"0\" y=\"0\"/>\n" +
                        "            <a:ext cx=\""+width+"\" cy=\""+height+"\"/>\n" +
                        "          </a:xfrm>\n" +
                        "          <a:prstGeom prst=\"rect\">\n" +
                        "            <a:avLst/>\n" +
                        "          </a:prstGeom>\n" +
                        "        </pic:spPr>\n" +
                        "      </pic:pic>\n" +
                        "    </a:graphicData>\n" +
                        "  </a:graphic>\n" +
                        "</wp:inline>";

        return XmlObject.Factory.parse(xml);
    }

    private class TProperty {
        Boolean italic;
        Boolean bold;
        Boolean underlined;
        Boolean strikethrough;
        TPropertyScriptType scriptType;
        Boolean caps;
        Boolean smallCaps;
        Boolean doubleStrikethrough;
        Boolean emboss;
        Boolean imprint;
        Boolean outline;
        Boolean shadow;
        Boolean vanish;
        String colorHex;
        Integer size;

        public TProperty() {
        }

        public TProperty(boolean italic, boolean bold, boolean underlined, boolean strikethrough, TPropertyScriptType scriptType) {
            this.italic = italic;
            this.bold = bold;
            this.underlined = underlined;
            this.strikethrough = strikethrough;
            this.scriptType = scriptType;
        }
    }

    private enum TPropertyScriptType {
        superscript,
        subscript,
        none
    }
}
