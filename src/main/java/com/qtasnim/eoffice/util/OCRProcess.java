/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.inject.Inject;

import com.qtasnim.eoffice.config.ApplicationConfig;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract1;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.leptonica.PIX;
import org.bytedeco.tesseract.TessBaseAPI;

import static org.bytedeco.leptonica.global.lept.pixDestroy;
import static org.bytedeco.leptonica.global.lept.pixRead;

/**
 *
 * @author acdwisu
 */
public class OCRProcess {
    @Inject
    private ApplicationConfig applicationConfig;

    private  BufferedImage image;

    private ImageProcessing impro;

    private boolean isBinaryze;
    private boolean isDeskew;

    public OCRProcess() {
        impro = new ImageProcessing();
    }

    @PostConstruct
    private void postConstruct() {
        isBinaryze = Boolean.parseBoolean(applicationConfig.getOcrPreprocessIsBinaryze());
        isDeskew = Boolean.parseBoolean(applicationConfig.getOcrPreprocessIsDeskew());
    }
    
    public OCRProcess(byte[] image) throws IOException {
        this();
        
        this.image = impro.byteToImage(image);
    }
    
    public OCRProcess(InputStream stream) {    
        this();

        try {            
            this.image = ImageIO.read(stream);
        } catch (IOException ex) {
            this.image = null;
            Logger.getLogger(OCRProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public OCRProcess(BufferedImage image) {
        this();

        this.image = image;
    }

    public BufferedImage getImage() {
        return image;
    }

    public byte[] getImageByte() {
        return impro.imageToByte(image);
    }

    public void setImage(byte[] image) throws IOException {
        this.image = impro.byteToImage(image);
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }
    
    public String extractText() throws Exception {
        return extractText2(isBinaryze, isDeskew);
    }
    
    public String extractText1(boolean isBinarizing, boolean isDeskewing) throws Exception {
        String result;

        if(isBinarizing) image = impro.binaryzeImage(image);
        if(isDeskewing) image = impro.deskewImage(image);

        ITesseract tesseract = new Tesseract1();

        tesseract.setDatapath(TesseractDataInitializer.GetTessDataTempPath());
        tesseract.setLanguage("eng");

        result = tesseract.doOCR(image);

        // OCR result spell checking
        // ....
        Logger.getLogger(OCRProcess.class.getName()).log(Level.INFO, "Ekstraksi Teks pada image, selesai");
        return result;
    }

    public String extractText2(boolean isBinarizing, boolean isDeskewing) throws Exception {
        String result;

        if(isBinarizing) image = impro.binaryzeImage(image);
        if(isDeskewing) image = impro.deskewImage(image);

        BytePointer outText;

        TessBaseAPI api = new TessBaseAPI();

        if (api.Init(TesseractDataInitializer.GetTessDataTempPath(), "eng") != 0) {
            throw new Exception("Could not initialize tesseract.");
        }

        File tempImage =  new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString()+".jpg");

        ImageIO.write(image, "jpeg",tempImage);

        // Open input image with leptonica library
        PIX image = pixRead(tempImage.getPath());
        api.SetImage(image);
        // Get OCR result
        outText = api.GetUTF8Text();
        result = outText.getString();

        // Destroy used object and release memory
        api.End();
        outText.deallocate();
        pixDestroy(image);

        tempImage.delete();

        return result;
    }
}
