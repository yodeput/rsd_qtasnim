package com.qtasnim.eoffice.util;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.bytedeco.tesseract.TessBaseAPI;
import org.bytedeco.tesseract.TessResultRenderer;
import org.bytedeco.tesseract.global.tesseract;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class TextExtractionProcess {

    public List<String> extractText(byte[] bytes) {
        List<String> texts = new LinkedList();

        try {
            PdfReader reader = new PdfReader(bytes);
            PdfReaderContentParser parser = new PdfReaderContentParser(reader);

            TextExtractionStrategy strategy;
            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                strategy = parser.processContent(i, new SimpleTextExtractionStrategy());

                String extractedText = strategy.getResultantText();

                texts.add(extractedText);
            }

            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return texts;
    }

    /**
     * Extract text dari pdf dengan mempertimbangkan ada file pdf yg berisi citra (mis. hasil scan)
     * @param bytes
     * @return
     */
    public List<String> extractTextV2(byte[] bytes) throws Exception {
        List<String> texts = new LinkedList();

        PDDocument document = PDDocument.load(bytes);

        PDFTextStripper pdfTextStripper = new PDFTextStripper();

        OCRProcess ocrProcess = new OCRProcess();

        ImageProcessing imageProcessing = new ImageProcessing();

        for(int i=1; i<=document.getNumberOfPages(); i++) {
            pdfTextStripper.setStartPage(i);
            pdfTextStripper.setEndPage(i);

            String text = pdfTextStripper.getText(document);

            if(StringUtils.isBlank(text)) {
                BufferedImage image = imageProcessing.pdfPageToImage(document, i);

                ocrProcess.setImage(image);

                text = ocrProcess.extractText();
            }

            texts.add(text);
        }

        document.close();

        return texts;
    }

    public List<String> extractTextV2WithoutOCR(byte[] bytes) throws IOException {
        List<String> texts = new LinkedList();

        PDDocument document = PDDocument.load(bytes);

        PDFTextStripper pdfTextStripper = new PDFTextStripper();

        for(int i=1; i<=document.getNumberOfPages(); i++) {
            pdfTextStripper.setStartPage(i);
            pdfTextStripper.setEndPage(i);

            String text = pdfTextStripper.getText(document);

            texts.add(text);
        }

        document.close();

        return texts;
    }

    public String extractAllText(byte[] bytes) throws IOException {
        PDDocument document = PDDocument.load(bytes);

        PDFTextStripper pdfTextStripper = new PDFTextStripper();

        String extractedText = pdfTextStripper.getText(document);

        document.close();

        return extractedText;
    }

    /**
     * Extract text dari pdf dengan mempertimbangkan ada file pdf yg berisi citra (mis. hasil scan)
     * @param bytes
     * @return
     */
    public List<String> extractTextV2(File file) throws Exception {
        List<String> texts = new LinkedList();

        PDDocument document = PDDocument.load(file);

        PDFTextStripper pdfTextStripper = new PDFTextStripper();

        OCRProcess ocrProcess = new OCRProcess();

        ImageProcessing imageProcessing = new ImageProcessing();

        for(int i=1; i<=document.getNumberOfPages(); i++) {
            pdfTextStripper.setStartPage(i);
            pdfTextStripper.setEndPage(i);

            String text = pdfTextStripper.getText(document);

            if(StringUtils.isBlank(text)) {
                BufferedImage image = imageProcessing.pdfPageToImage(document, i);

                ocrProcess.setImage(image);

                text = ocrProcess.extractText();
            }

            texts.add(text);
        }

        document.close();

        return texts;
    }

    public List<String> extractTextV2WithoutOCR(File file) throws IOException {
        List<String> texts = new LinkedList();

        PDDocument document = PDDocument.load(file);

        PDFTextStripper pdfTextStripper = new PDFTextStripper();

        for(int i=1; i<=document.getNumberOfPages(); i++) {
            pdfTextStripper.setStartPage(i);
            pdfTextStripper.setEndPage(i);

            String text = pdfTextStripper.getText(document);

            texts.add(text);
        }

        document.close();

        return texts;
    }

    public String extractAllText(File file) throws IOException {
        PDDocument document = PDDocument.load(file);

        PDFTextStripper pdfTextStripper = new PDFTextStripper();

        String extractedText = pdfTextStripper.getText(document);

        document.close();

        return extractedText;
    }

    public File pdfToTiff(File pdf) throws IOException {
        File tiffOutput = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString());

        ImageProcessing impro = new ImageProcessing();
        List<BufferedImage> images = impro.pdfToImages(pdf);
        impro.writeImagesToTiff(images, tiffOutput);

        return tiffOutput;
    }

    public File produceSearchablePdfFile(File src) throws Exception {
        String temp = System.getProperty("java.io.tmpdir")+"/"+UUID.randomUUID().toString();

        TessBaseAPI api = new TessBaseAPI();

        if (api.Init(TesseractDataInitializer.GetTessDataTempPath(), "eng") != 0) {
            throw new Exception("Could not initialize tesseract.");
        }

        TessResultRenderer renderer = tesseract.TessPDFRendererCreate(temp, TesseractDataInitializer.GetTessDataTempPath(), false);

        if(!api.ProcessPages(src.getPath(), null, 0, renderer)) {
            throw new Exception("Could not initialize tesseract.");
        }

        api.End();
        renderer.deallocate();

        tesseract.TessDeleteResultRenderer(renderer);

        File result = new File(temp.concat(".pdf"));

        return result;
    }

    public byte[] produceSearchablePdfBytes(File src) throws Exception {
        File pdf = produceSearchablePdfFile(src);

        byte[] bytes = IOUtils.toByteArray(src.toURI());

        Files.deleteIfExists(pdf.toPath());

        return bytes;
    }
}
