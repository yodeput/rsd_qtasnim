package com.qtasnim.eoffice.util;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.lang.annotation.Annotation;

public class BeanUtil {
    public static BeanManager getBeanManager() {
        try {
            InitialContext initialContext = new InitialContext();
            return (BeanManager) initialContext.lookup("java:comp/BeanManager");
        } catch (NamingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Object getBeanByName(String name) {
        BeanManager bm = getBeanManager();
        Bean bean = bm.getBeans(name).iterator().next();
        CreationalContext ctx = bm.createCreationalContext(bean);
        return bm.getReference(bean, bean.getClass(), ctx);
    }

    public static Object getBean(Class clazz) {
        try {
            return CDI.current().select(clazz).get();
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static Object getBean(Class clazz, Annotation... qualifiers) {
        return CDI.current().select( clazz, qualifiers).get();
    }
}
