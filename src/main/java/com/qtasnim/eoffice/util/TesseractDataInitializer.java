package com.qtasnim.eoffice.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class TesseractDataInitializer {

    private static String tessDataTempPath;

    static {
        try {
            File tesseractTessDataOri = new File(TesseractDataInitializer.class.getResource("/tess4j/tessdata").toURI());

            File tesseractTessDataTemp = new File(System.getProperty("java.io.tmpdir"), "tessdata");

            FileUtils.copyDirectory(tesseractTessDataOri, tesseractTessDataTemp);

            tessDataTempPath = tesseractTessDataTemp.getPath();
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    public static String GetTessDataTempPath() {
        return tessDataTempPath;
    }
}
