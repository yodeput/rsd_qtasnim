/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.util;

import java.io.*;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.helpers.TemplatingHelper;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTabStop;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTabJc;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import com.xandryex.WordReplacer;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acdwisu
 */
public class TemplatingProcessOld {
    private Map<String, String> pairMetaNameValue;
    private String pathTemplateRaw;
    
    private String keyPrefix;
    private String keyPostfix;

    private Map<String, String> pairKeyValue, pairKeyValueMulti;
    
    public TemplatingProcessOld(Map<String, String> pairMetaNameValue, String pathTemplateRaw) {
        this();
        
        this.pairMetaNameValue = pairMetaNameValue;

        this.pathTemplateRaw = pathTemplateRaw;
        
        this.pairKeyValue = new HashMap();
        this.pairKeyValueMulti = new HashMap();
        
        initPairKeyValue();
    }

    private TemplatingProcessOld() {
        this.keyPrefix = "${";
        this.keyPostfix = "}";
    }            

    public Map<String, String> getPairMetaNameValue() {
        return pairMetaNameValue;
    }

    public void setPairMetaNameValue(Map<String, String> pairMetaNameValue) {
        this.pairMetaNameValue = pairMetaNameValue;
    }

    public String getPathTemplateRaw() {
        return pathTemplateRaw;
    }

    public void setPathTemplateRaw(String pathTemplateRaw) {
        this.pathTemplateRaw = pathTemplateRaw;
    }
    
    public byte[] process() {
        try {
            XWPFDocument docx = new XWPFDocument(new FileInputStream(this.pathTemplateRaw));

            ByteArrayOutputStream out = new ByteArrayOutputStream();

            if(pairMetaNameValue.containsKey(TemplatingHelper.PENERIMA_SURAT_KEY)) {
                //templating penerima surat
                String keyPenerimaSurat = TemplatingHelper.toMetadataFormatName(TemplatingHelper.PENERIMA_SURAT_KEY);

                int jumlahPenerimaSurat = pairMetaNameValue.get(keyPenerimaSurat).split(TemplatingSeparator.SEPARATOR_NEW_ENTITY).length;

                templatingConditionalPenerimaSurat(docx, jumlahPenerimaSurat);
            }

            docx = this.templatingSingleValue(docx, pairKeyValue);
            docx = this.templatingMultiValue(docx, pairKeyValueMulti);

            docx.write(out);
            docx.close();

            out.close();

            Logger.getLogger(TemplatingProcessOld.class.getName()).log(Level.INFO, "templating success");

            return out.toByteArray();
        } catch (IOException | NumberFormatException ex) {
            Logger.getLogger(TemplatingProcessOld.class.getName()).log(Level.SEVERE, ex.getMessage());

            throw new InternalServerErrorException(null, ex);
        }
    }    
    
    private XWPFDocument templatingSingleValue(XWPFDocument docx, Map<String, String> pairKeyValue) {        
        WordReplacer replacer = new WordReplacer(docx);
        
        for(String key : pairKeyValue.keySet()) {
            String value = pairKeyValue.get(key);
            
            replacer.replaceWordsInText(key, value);
            replacer.replaceWordsInTables(key, value);
        }
        
        return replacer.getModdedXWPFDoc();
    }
    
    private XWPFDocument templatingMultiValue(XWPFDocument docx, Map<String, String> pairKeyValue) {
        List<XWPFParagraph> paragraphs = docx.getParagraphs();
        
        for(String key : pairKeyValue.keySet()) {
            String value = pairKeyValue.get(key);
            
            for (XWPFParagraph xwpfParagraph : paragraphs) {
                String runText = xwpfParagraph.getText();
                if (hasReplaceableItem(runText, key)) {
                    String replacedText = StringUtils.replace(xwpfParagraph.getText(), key, value);

                    removeAllRuns(xwpfParagraph);

                    insertReplacementRuns(xwpfParagraph, replacedText);
                }
            }
        }
        
        return docx;
    }
    
    private void insertReplacementRuns(XWPFParagraph paragraph, String replacedText) {
        String[] replacementTextSplitOnCarriageReturn = replacedText.split(TemplatingSeparator.SEPARATOR_NEW_LINE);

        for (int j = 0; j < replacementTextSplitOnCarriageReturn.length; j++) {
            String part = replacementTextSplitOnCarriageReturn[j];

            String[] subPart = part.split(TemplatingSeparator.SEPARATOR_TAB);

            XWPFRun newRun = paragraph.insertNewRun(j);

            CTTabStop tabStop = paragraph.getCTP().getPPr().addNewTabs().addNewTab();
            tabStop.setVal(STTabJc.LEFT);
            tabStop.setPos(BigInteger.valueOf(430));

            newRun.setText(subPart[0]);
            newRun.addTab();
            newRun.setText(subPart[1]);

            if (j+1 < replacementTextSplitOnCarriageReturn.length) {
                newRun.addCarriageReturn();
            }
        }       
    }

    private void removeAllRuns(XWPFParagraph paragraph) {
        int size = paragraph.getRuns().size();
        for (int i = 0; i < size; i++) {
            paragraph.removeRun(0);
        }
    }

    private boolean hasReplaceableItem(String runText, String searchValue) {
        return StringUtils.contains(runText, searchValue);
    }
    
    private void initPairKeyValue() {
        for(String metaName : this.pairMetaNameValue.keySet()) {
            String valueRaw = this.pairMetaNameValue.get(metaName);
            String[] valueSplitted = valueRaw.split(TemplatingSeparator.SEPARATOR_NEW_ENTITY);
                    
            String key = this.keyPrefix+metaName+this.keyPostfix;
            
            boolean isMultiValue = valueSplitted.length>1;
            
            if(isMultiValue) {
                String valueReconstruct = "";
                
                for(int i=0; i<valueSplitted.length; i++) {
                    valueReconstruct += (i+1)+"."+TemplatingSeparator.SEPARATOR_TAB+valueSplitted[i]+TemplatingSeparator.SEPARATOR_NEW_LINE;
                }
                pairKeyValueMulti.put(key, valueReconstruct);
            } else {
                pairKeyValue.put(key, valueSplitted[0]);
            }            
        }
    }

    private XWPFDocument templatingConditionalPenerimaSurat(XWPFDocument docx, int jumlahPenerima) {
        WordReplacer replacer = new WordReplacer(docx);
        List<XWPFParagraph> paragraphs = docx.getParagraphs();

        String[] penerimaSuratKondisionalKeys = TemplatingHelper.PENERIMA_SURAT_CONDITIONAL_KEY.split(",");

        String keyKondisi1 = this.keyPrefix+TemplatingHelper.toMetadataFormatName(penerimaSuratKondisionalKeys[0])+this.keyPostfix;
        String keyKondisi2 = this.keyPrefix+TemplatingHelper.toMetadataFormatName(penerimaSuratKondisionalKeys[1])+this.keyPostfix;

        if(jumlahPenerima > 3) {
            replacer.replaceWordsInText(keyKondisi1, "Para Penerima Terlampir");
            docx = replacer.getModdedXWPFDoc();

            Map<String, String> pairKeyValue = new HashMap<>();

            String value = "Penerima:"+TemplatingSeparator.SEPARATOR_NEW_LINE+
                    this.keyPrefix+TemplatingHelper.toMetadataFormatName("Penerima Surat")+this.keyPostfix;

            pairKeyValue.put(keyKondisi2, value);

            templatingMultiValue(docx, pairKeyValue);
        } else {
            Map<String, String> pairKeyValue = new HashMap<>();

            String value = this.keyPrefix+TemplatingHelper.toMetadataFormatName("Penerima Surat")+this.keyPostfix;

            pairKeyValue.put(keyKondisi2, value);

            templatingSingleValue(docx, pairKeyValue);

            for (XWPFParagraph xwpfParagraph : paragraphs) {
                String runText = xwpfParagraph.getText();
                if (hasReplaceableItem(runText, keyKondisi2)) {
//                    String replacedText = StringUtils.replace(xwpfParagraph.getText(), keyKondisi2, "");

                    removeAllRuns(xwpfParagraph);

//                    insertReplacementRuns(xwpfParagraph, replacedText);
                    break;
                }
            }
        }

        return docx;
    }
}
