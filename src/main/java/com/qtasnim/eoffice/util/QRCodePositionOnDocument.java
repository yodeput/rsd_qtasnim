package com.qtasnim.eoffice.util;

public enum QRCodePositionOnDocument {
    TOP_RIGHT,
    TOP_CENTER,
    TOP_LEFT,
    MIDDLE_RIGHT,
    MIDDLE_LEFT,
    BOTTOM_RIGHT,
    BOTTOM_CENTER,
    BOTTOM_LEFT
}
