package com.qtasnim.eoffice.util;

public class ExceptionUtil {
    private static final Integer MAX_RETRY = 10;

    public static Throwable getRealCause(Throwable e) {
        return getRealCause(e, 0);
    }

    private static Throwable getRealCause(Throwable e, Integer retry) {
        Throwable cause = e.getCause();

        if (cause == null || retry.equals(MAX_RETRY)) {
            return e;
        }

        if (!e.equals(cause)) {
            return getRealCause(cause, retry + 1);
        }

        return cause;
    }
}
