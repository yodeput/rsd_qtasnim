/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.util;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.codec.TiffImage;
import com.recognition.software.jdeskew.ImageDeskew;
import net.sourceforge.tess4j.util.ImageHelper;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.*;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acdwisu
 */
public class ImageProcessing {
    static final double MINIMUM_DESKEW_THRESHOLD = 0.05d;

    public BufferedImage binaryzeImage(BufferedImage image) {
        return ImageHelper.convertImageToBinary(image);
    }

    public BufferedImage deskewImage(BufferedImage image) {
        BufferedImage cloned = ImageHelper.cloneImage(image);

        ImageDeskew id = new ImageDeskew(cloned);

        double imageSkewAngle = id.getSkewAngle(); // determine skew angle

        if ((imageSkewAngle > MINIMUM_DESKEW_THRESHOLD || imageSkewAngle < -(MINIMUM_DESKEW_THRESHOLD))) {
            cloned = ImageHelper.rotateImage(cloned, -imageSkewAngle); // deskew image
        }

        return cloned;
    }
    
    public byte[] imageToByte(BufferedImage originalImage) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            ImageIO.write( originalImage, "jpeg", baos );
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();				
            return imageInByte;
        } catch (IOException ex) {
            Logger.getLogger(ImageProcessing.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public BufferedImage byteToImage(byte[] image) throws IOException {
        return ImageIO.read(new ByteArrayInputStream(image));
    }
    
    public void writeImage(String destPath, byte[] imageByte) {
        try (FileOutputStream fos = new FileOutputStream(destPath)) {
            fos.write(imageByte);
            
            fos.close();            
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }        
    }

    public List<BufferedImage> imagesFromTiff(InputStream fileInput) throws Exception {
        List<BufferedImage> extractedImages = new LinkedList();

        try (ImageInputStream iis = ImageIO.createImageInputStream(fileInput)) {

            ImageReader reader = getTiffImageReader();
            reader.setInput(iis);

            int pages = reader.getNumImages(true);
            for (int imageIndex = 0; imageIndex < pages; imageIndex++) {
                BufferedImage bufferedImage = reader.read(imageIndex);
                extractedImages.add(bufferedImage);
            }
        }

        return extractedImages;
    }

    public void writeImagesToTiff(List<BufferedImage> images, File outputFile) {
        // Obtain a TIFF writer
        ImageWriter writer = new ImageProcessing().getTiffImageWriter();

        try (ImageOutputStream output = ImageIO.createImageOutputStream(outputFile)) {
            writer.setOutput(output);

            ImageWriteParam params = writer.getDefaultWriteParam();
            params.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);

            // Compression: None, PackBits, ZLib, Deflate, LZW, JPEG and CCITT variants allowed
            // (different plugins may use a different set of compression type names)
            params.setCompressionType("JPEG");

            writer.prepareWriteSequence(null);

            for (BufferedImage image : images) {
                writer.writeToSequence(new IIOImage(image, null, null), params);
            }

            // We're done
            writer.endWriteSequence();
        } catch (IOException e) {
            e.printStackTrace();
        }

        writer.dispose();
    }

    public ImageReader getTiffImageReader() {
        Iterator<ImageReader> imageReaders = ImageIO.getImageReadersByFormatName("TIFF");
        if (!imageReaders.hasNext()) {
            throw new UnsupportedOperationException("No TIFF Reader found!");
        }
        return imageReaders.next();
    }

    public ImageWriter getTiffImageWriter() {
        Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByFormatName("TIFF");
        if (!imageWriters.hasNext()) {
            throw new UnsupportedOperationException("No TIFF Writer found!");
        }
        return imageWriters.next();
    }

    public byte[] multiImagesToPdf(List<BufferedImage> images) throws IOException, DocumentException {
        File temp = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID()+".pdf");

        Document TifftoPDF = new Document();
        PdfWriter pdfWriter = PdfWriter.getInstance(TifftoPDF, new FileOutputStream(temp));
        pdfWriter.setStrictImageSequence(true);
        TifftoPDF.open();

        for(Image image : images) {
            com.itextpdf.text.Image tempImage = com.itextpdf.text.Image.getInstance(image, null);

            com.itextpdf.text.Rectangle pageSize = new com.itextpdf.text.Rectangle(tempImage.getWidth(), tempImage.getHeight());
            TifftoPDF.setPageSize(pageSize);
            TifftoPDF.newPage();
            TifftoPDF.add(tempImage);
        }

        TifftoPDF.close();

        byte[] bytes = IOUtils.toByteArray(temp.toURI());

        Files.deleteIfExists(temp.toPath());

        return bytes;
    }

    public byte[] multiImagesToPdf(File tiffFile) throws IOException, DocumentException {
        File temp = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID()+".pdf");

        RandomAccessFileOrArray myTiffFile = new RandomAccessFileOrArray(tiffFile.getCanonicalPath());

        // Find number of images in Tiff file
        int numberOfPages = TiffImage.getNumberOfPages(myTiffFile);

        Document TifftoPDF = new Document();

        PdfWriter pdfWriter = PdfWriter.getInstance(TifftoPDF, new FileOutputStream(temp));
        pdfWriter.setStrictImageSequence(true);

        TifftoPDF.open();

        com.itextpdf.text.Image tempImage;

        // Run a for loop to extract images from Tiff file
        // into a Image object and add to PDF recursively
        for (int i = 1; i <= numberOfPages; i++) {
            tempImage = TiffImage.getTiffImage(myTiffFile, i);
            com.itextpdf.text.Rectangle pageSize = new  com.itextpdf.text.Rectangle(tempImage.getWidth(), tempImage.getHeight());
            TifftoPDF.setPageSize(pageSize);
            TifftoPDF.newPage();
            TifftoPDF.add(tempImage);
        }

        TifftoPDF.close();

        byte[] bytes = IOUtils.toByteArray(temp.toURI());

        Files.deleteIfExists(temp.toPath());

        return bytes;
    }

    public List<BufferedImage> pdfToImages(PDDocument document) throws IOException {
        List<BufferedImage> images = new LinkedList<>();

        PDFRenderer pdfRenderer = new PDFRenderer(document);
        for (int page = 0; page < document.getNumberOfPages(); ++page)
        {
            BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);

            images.add(bim);
        }

        return images;
    }

    public List<BufferedImage> pdfToImages(File pdfFile) throws IOException {
        final PDDocument document = PDDocument.load(pdfFile);

        List<BufferedImage> images = pdfToImages(document);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfToImages(File pdfFile, String password) throws IOException {
        final PDDocument document = PDDocument.load(pdfFile, password);

        List<BufferedImage> images = pdfToImages(document);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfToImages(InputStream pdfStream) throws IOException {
        final PDDocument document = PDDocument.load(pdfStream);

        List<BufferedImage> images = pdfToImages(document);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfToImages(InputStream pdfStream, String password) throws IOException {
        final PDDocument document = PDDocument.load(pdfStream, password);

        List<BufferedImage> images = pdfToImages(document);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfToImages(byte[] pdfBytes) throws IOException {
        final PDDocument document = PDDocument.load(pdfBytes);

        List<BufferedImage> images = pdfToImages(document);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfToImages(byte[] pdfBytes, String password) throws IOException {
        final PDDocument document = PDDocument.load(pdfBytes, password);

        List<BufferedImage> images = pdfToImages(document);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfPagesToImages(PDDocument document, List<Integer> pages) throws IOException {
        List<BufferedImage> images = new LinkedList<>();

        PDFRenderer pdfRenderer = new PDFRenderer(document);

        for(int page : pages) {
            BufferedImage bim = pdfRenderer.renderImageWithDPI(page-1, 300, ImageType.RGB);

            images.add(bim);
        }

        return images;
    }

    public List<BufferedImage> pdfPagesToImages(File pdf, List<Integer> pages) throws IOException {
        final PDDocument document = PDDocument.load(pdf);

        List<BufferedImage> images = pdfPagesToImages(document, pages);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfPagesToImages(File pdf, String password, List<Integer> pages) throws IOException {
        final PDDocument document = PDDocument.load(pdf, password);

        List<BufferedImage> images = pdfPagesToImages(document, pages);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfPagesToImages(InputStream pdf, List<Integer> pages) throws IOException {
        final PDDocument document = PDDocument.load(pdf);

        List<BufferedImage> images = pdfPagesToImages(document, pages);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfPagesToImages(InputStream pdf, String password, List<Integer> pages) throws IOException {
        final PDDocument document = PDDocument.load(pdf, password);

        List<BufferedImage> images = pdfPagesToImages(document, pages);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfPagesToImages(byte[] pdf, List<Integer> pages) throws IOException {
        final PDDocument document = PDDocument.load(pdf);

        List<BufferedImage> images = pdfPagesToImages(document, pages);

        document.close();

        return images;
    }

    public List<BufferedImage> pdfPagesToImages(byte[] pdf, String password, List<Integer> pages) throws IOException {
        final PDDocument document = PDDocument.load(pdf, password);

        List<BufferedImage> images = pdfPagesToImages(document, pages);

        document.close();

        return images;
    }

    public BufferedImage pdfPageToImage(PDDocument document, int page) throws IOException {
        PDFRenderer pdfRenderer = new PDFRenderer(document);

        return pdfRenderer.renderImageWithDPI(page-1, 300, ImageType.RGB);
    }

    public BufferedImage pdfPageToImage(File pdf, int page) throws IOException {
        final PDDocument document = PDDocument.load(pdf);

        return pdfPageToImage(pdf, page);
    }

    public BufferedImage pdfPageToImage(File pdf, String password, int page) throws IOException {
        final PDDocument document = PDDocument.load(pdf, password);

        return pdfPageToImage(pdf, page);
    }

    public BufferedImage pdfPageToImage(InputStream pdf, int page) throws IOException {
        final PDDocument document = PDDocument.load(pdf);

        return pdfPageToImage(pdf, page);
    }

    public BufferedImage pdfPageToImage(InputStream pdf, String password, int page) throws IOException {
        final PDDocument document = PDDocument.load(pdf, password);

        return pdfPageToImage(pdf, page);
    }

    public BufferedImage pdfPageToImage(byte[] pdf, int page) throws IOException {
        final PDDocument document = PDDocument.load(pdf);

        return pdfPageToImage(pdf, page);
    }

    public BufferedImage pdfPageToImage(byte[] pdf, String password, int page) throws IOException {
        final PDDocument document = PDDocument.load(pdf, password);

        return pdfPageToImage(pdf, page);
    }
}
