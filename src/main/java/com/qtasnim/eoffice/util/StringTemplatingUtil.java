package com.qtasnim.eoffice.util;

import com.qtasnim.eoffice.helpers.ReflectionHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.text.StringSubstitutor;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class StringTemplatingUtil {
    public String templating(String template, Object entity) {
        return templatingProcess(template, entity);
    }

    public String templating(String template) {
        return templatingProcess(template);
    }

    private Map<String, String> mappingFromEntity(String template, Object entity) {
        Map<String, String> mapped = new HashMap<>();

        String[] keys = StringUtils.substringsBetween(template, "${", "}");
        List<String> nullValue = new LinkedList<>();

        ExpressionUtil expressionUtil = new ExpressionUtil();

        for(String key : keys) {
            String resolved = expressionUtil.simpleResolve(entity, key);
            if(resolved == null) nullValue.add(key);
            else {
                String value = resolved;
                mapped.put(key, value);
            }
        }

        if(nullValue.size() > 0) {
            mapped.putAll(ReflectionHelper.reflectionResolve(entity,nullValue));
        }

        return mapped;
    }

    private Map<String, String> mappingFromEntity(String template) {
        Map<String, String> mapped = new HashMap<>();

        String[] keys = StringUtils.substringsBetween(template, "${", "}");

        for(String key : keys) {
            mapped.put(key, " - ");
        }

        return mapped;
    }

    private String templatingProcess(String template, Object entity) {
        String templated = "  ";
        Map<String,String> valuesMap;

        //create mapping key -> value from entity
        valuesMap = this.mappingFromEntity(template,entity);

        //templating whole template string
        StringSubstitutor substitutor = new StringSubstitutor(valuesMap);

        templated = substitutor.replace(template);

        return templated;
    }

    private String templatingProcess(String template) {
        String templated = "  ";
        Map<String,String> valuesMap;

        //create mapping key -> value from entity
        valuesMap = this.mappingFromEntity(template);

        //templating whole template string
        StringSubstitutor substitutor = new StringSubstitutor(valuesMap);

        templated = substitutor.replace(template);

        return templated;
    }
}
