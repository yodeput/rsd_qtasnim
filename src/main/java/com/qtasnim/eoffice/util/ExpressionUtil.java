package com.qtasnim.eoffice.util;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.jayway.jsonpath.JsonPath;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.workflows.OrganizationEntitySerializer;

public class ExpressionUtil {
    public Object resolve(Object entity, String expr) {
        try {
            String json = getObjectMapper().writeValueAsString(entity);
            return JsonPath.read(json, expr);
        } catch (Exception exception) {
            return null;
        }
    }


    /**
     * @param entity
     * @param fieldName nama field dari objek tanpa penanda apapun
     * @return
     */
    public String simpleResolve(Object entity, String fieldName) {
        String expr = "$."+fieldName;
        String resolved = (String) resolve(entity, expr);
        return resolved;
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
