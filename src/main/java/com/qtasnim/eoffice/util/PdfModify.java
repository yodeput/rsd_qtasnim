package com.qtasnim.eoffice.util;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

public class PdfModify {

    private final QRCodePositionOnDocument DEFAULT_QR_CODE_POSITION;

    public PdfModify() {
        DEFAULT_QR_CODE_POSITION = QRCodePositionOnDocument.BOTTOM_LEFT;
    }

    public byte[] addQRCode(byte[] pdfSrc, byte[] qrcode) {
        return addQRCode(pdfSrc, qrcode, DEFAULT_QR_CODE_POSITION);
    }

    public byte[] addQRCode(byte[] pdfSrc, byte[] qrcode, QRCodePositionOnDocument position) {
        byte[] result = new byte[]{};

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        PdfReader reader = null;
        try {
            reader = new PdfReader(pdfSrc);

            File temp = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID()+".png");

            ImageIO.write(new ImageProcessing().byteToImage(qrcode), "png", temp);

            PdfStamper stamper = new PdfStamper(reader, byteArrayOutputStream);

            for(int i=1; i<=reader.getNumberOfPages(); i++) {
                Image image = Image.getInstance(temp.getPath());

                PdfImage stream = new PdfImage(image, "", null);

                stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));

                PdfIndirectObject ref = stamper.getWriter().addToBody(stream);

                image.setDirectReference(ref.getIndirectReference());

                boolean portrait;

                Rectangle pageSize = reader.getPageSize(i);

                // silly assumption -_-
                if(pageSize.getHeight() > pageSize.getWidth()) portrait = true;
                else portrait = false;

                QRCodePositionOnDocument pos = position==null? DEFAULT_QR_CODE_POSITION:position;

                int x=0,y=0;

                if(portrait) {
                    switch (pos) {
                        case BOTTOM_LEFT:
                            x = 0;
                            y = 0;
                            break;
                        case BOTTOM_CENTER:
                            x = (int) ((pageSize.getWidth()/2) - (image.getWidth()/2));
                            y = 0;
                            break;
                        case BOTTOM_RIGHT:
                            x = (int) (pageSize.getWidth() - image.getWidth());
                            y = 0;
                            break;
                        case MIDDLE_LEFT:
                            x = 0;
                            y = (int) ((pageSize.getHeight()/2) - (image.getHeight()/2));
                            break;
                        case MIDDLE_RIGHT:
                            x = (int) (pageSize.getWidth() - image.getWidth());
                            y = (int) ((pageSize.getHeight()/2) - (image.getHeight()/2));
                            break;
                        case TOP_LEFT:
                            x = 0;
                            y = (int) ((pageSize.getHeight() - image.getHeight()));
                            break;
                        case TOP_CENTER:
                            x = (int) ((pageSize.getWidth()/2) - (image.getWidth()/2));
                            y = (int) ((pageSize.getHeight() - image.getHeight()));
                            break;
                        case TOP_RIGHT:
                            x = (int) (pageSize.getWidth() - image.getWidth());
                            y = (int) ((pageSize.getHeight() - image.getHeight()));
                            break;
                    }
                } else {
                    image.setRotationDegrees(90);

                    switch (pos) {
                        case BOTTOM_LEFT:
                            x = (int) (pageSize.getWidth() - image.getWidth());
                            y = 0;
                            break;
                        case BOTTOM_CENTER:
                            x = (int) (pageSize.getWidth() - image.getWidth());
                            y = (int) ((pageSize.getHeight()/2) - (image.getHeight()/2));
                            break;
                        case BOTTOM_RIGHT:
                            x = (int) (pageSize.getWidth() - image.getWidth());
                            y = (int) ((pageSize.getHeight() - image.getHeight()));
                            break;
                        case MIDDLE_LEFT:
                            x = (int) ((pageSize.getWidth()/2) - (image.getWidth()/2));
                            y = 0;
                            break;
                        case MIDDLE_RIGHT:
                            x = (int) ((pageSize.getWidth()/2) - (image.getWidth()/2));
                            y = (int) ((pageSize.getHeight() - image.getHeight()));
                            break;
                        case TOP_LEFT:
                            x = 0;
                            y = 0;
                            break;
                        case TOP_CENTER:
                            x = 0;
                            y = (int) ((pageSize.getHeight()/2) - (image.getHeight()/2));
                            break;
                        case TOP_RIGHT:
                            x = 0;
                            y = (int) ((pageSize.getHeight() - image.getHeight()));
                            break;
                    }
                }

                image.setAbsolutePosition(x, y);

                PdfContentByte over = stamper.getOverContent(i);
                over.addImage(image);
            }

            stamper.close();
            reader.close();

            result = byteArrayOutputStream.toByteArray();

            Files.deleteIfExists(temp.toPath());
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }

        return result;
    }

    public byte[] addQRCode(byte[] pdfSrc, byte[] qrcode, Point position) {
        byte[] result = new byte[]{};

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        PdfReader reader = null;
        try {
            reader = new PdfReader(pdfSrc);

            File temp = new File(System.getProperty("java.io.tmpdir"),UUID.randomUUID()+".png");

            ImageIO.write(new ImageProcessing().byteToImage(qrcode), "png", temp);

            PdfStamper stamper = new PdfStamper(reader, byteArrayOutputStream);

            for(int i=1; i<=reader.getNumberOfPages(); i++) {
                Image image = Image.getInstance(temp.getPath());

                PdfImage stream = new PdfImage(image, "", null);

                stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));

                PdfIndirectObject ref = stamper.getWriter().addToBody(stream);

                image.setDirectReference(ref.getIndirectReference());

                boolean portrait;

                Rectangle pageSize = reader.getPageSize(i);

                // silly assumption -_-
                if(pageSize.getHeight() > pageSize.getWidth()) portrait = true;
                else portrait = false;

                int x=0,y=0;

                if(portrait) {
                    x = position.x;
                    y = position.y;
                } else {
                    image.setRotationDegrees(90);

                    x = position.y;
                    y = position.x;
                }

                image.setAbsolutePosition(x, y);

                PdfContentByte over = stamper.getOverContent(i);
                over.addImage(image);
            }

            stamper.close();
            reader.close();

            result = byteArrayOutputStream.toByteArray();

            Files.deleteIfExists(temp.toPath());
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }

        return result;
    }
}
