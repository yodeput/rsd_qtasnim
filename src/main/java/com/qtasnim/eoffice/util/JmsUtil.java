package com.qtasnim.eoffice.util;

import javax.jms.JMSException;
import javax.jms.Message;

public class JmsUtil {
    public static String getStringProperty(Message message, String name) {
        try {
            return message.getStringProperty(name);
        } catch (JMSException e) {
            return null;
        }
    }
}
