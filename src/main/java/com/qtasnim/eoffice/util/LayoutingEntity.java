/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.util;

/**
 *
 * @author acdwisu
 */
public class LayoutingEntity {
    private String fieldKey;
    private String fieldName;
    private String fieldType;

    private final String prefixKey = "${";
    private final String postfixKey = "}";
    
    public LayoutingEntity(String fieldName, String fieldType) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        
        this.fieldKey = prefixKey+fieldName+postfixKey;
    }

    public String getFieldKey() {
        return fieldKey;
    }

    public void setFieldKey(String fieldKey) {
        this.fieldKey = fieldKey;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }
}
