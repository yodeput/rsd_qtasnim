package com.qtasnim.eoffice.util;

public interface MetadataValueType {
    public static final int SINGLE_VALUE = 1;
    public static final int MULTI_VALUE = 2;
}
