package com.qtasnim.eoffice.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Anotasi penanda field yang digunakan sebagai metadata untuk keperluan templating dan layouting | digunakan pada field dari class implementor IDynamicFormEntity
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FieldMetadata {
    /**
     * nama metadata, sebaiknya sama dengan nama field. format penamaan : Camel_Case
     */
    String nama();

    /**
     * format metadata (template), bagaimana value field direpresentasikan
     */
    String format() default "";

    /**
     * tipe metadata -single value atau multi value-; merujuk ke 'MetadataValueType'
     */
    int type() default MetadataValueType.SINGLE_VALUE;

//    int role();
}