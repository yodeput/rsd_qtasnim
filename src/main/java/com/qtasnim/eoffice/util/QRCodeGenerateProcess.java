package com.qtasnim.eoffice.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.servlets.demo.helpers.ConfigManager;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class QRCodeGenerateProcess {

    private final String DEFAULT_CONTENT;
    private final int DEFAULT_SIZE;
    private final BufferedImage DEFAULT_LOGO_IMAGE;

    private final int MINIMAL_QR_SIZE;
    private final int MINIMAL_QR_LOGO_SIZE;
    private final float PERCENTAGE_HEIGHT_QR_LOGO;

    private ImageProcessing impro;

    public QRCodeGenerateProcess(ApplicationConfig applicationConfig) {
        impro = new ImageProcessing();

        DEFAULT_LOGO_IMAGE = getDefaultLogoImage();
        DEFAULT_CONTENT = applicationConfig.getQrcodeContentDefault(); //ConfigManager.GetProperty("qrcode.content.default");
        DEFAULT_SIZE = Integer.parseInt(applicationConfig.getQrcodeSizeDefault());  //ConfigManager.GetProperty("qrcode.size.default"));

        MINIMAL_QR_SIZE = Integer.parseInt(applicationConfig.getQrcodeSizeMinimal());  //ConfigManager.GetProperty("qrcode.size.minimal"));
        PERCENTAGE_HEIGHT_QR_LOGO = Float.parseFloat(applicationConfig.getQrcodeSizeLogoHeightPercentage());  //ConfigManager.GetProperty("qrcode.size.logo.height_percentage"));
        MINIMAL_QR_LOGO_SIZE = minimalQrLogoSize();
    }

    public byte[] generateQRCode() {
        return generateQRCode(null);
    }

    public byte[] generateQRCode(String content) {
        return generateQRCode(content, null);
    }

    public byte[] generateQRCode(String content, Integer size) {
        QRCodeWriter qrWriter = new QRCodeWriter();
        BitMatrix matrix = null;

        int qrSize = (size == null? DEFAULT_SIZE : (size < MINIMAL_QR_SIZE? MINIMAL_QR_SIZE : size));

        try {
            matrix = qrWriter.encode(
                    content == null || content.isEmpty()? DEFAULT_CONTENT : content,
                    BarcodeFormat.QR_CODE,
                    qrSize,
                    qrSize);

            BufferedImage image = MatrixToImageWriter.toBufferedImage(matrix);

            return impro.imageToByte(image);
        } catch (WriterException e) {
            e.printStackTrace();

            return new byte[]{};
        }
    }

    public byte[] generateQRCodeWithLogo() throws IOException, WriterException {
        return generateQRCodeWithLogo(null, null, null);
    }

    public byte[] generateQRCodeWithLogo(String content) throws IOException, WriterException {
        return generateQRCodeWithLogo(content, null, null);
    }

    public byte[] generateQRCodeWithLogo(String content, Integer size) throws IOException, WriterException {
        return generateQRCodeWithLogo(content, null, size);
    }

    public byte[] generateQRCodeWithLogo(String content, byte[] logo) throws IOException, WriterException {
        return generateQRCodeWithLogo(content, logo, null);
    }

    public byte[] generateQRCodeWithLogo(String content, byte[] logo, Integer size) throws WriterException, IOException {
        Map hints = new HashMap();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.MARGIN, 0);

        QRCodeWriter qrWriter = new  QRCodeWriter();
        BitMatrix matrix = null;

        int qrSize = (size == null? DEFAULT_SIZE : (size < MINIMAL_QR_SIZE? MINIMAL_QR_SIZE : size));

        matrix = qrWriter.encode(
                content == null || content.isEmpty()? DEFAULT_CONTENT : content,
                BarcodeFormat.QR_CODE,
                qrSize,
                qrSize,
                hints);

        BufferedImage image = MatrixToImageWriter.toBufferedImage(matrix);
        BufferedImage logoImage = logo == null? DEFAULT_LOGO_IMAGE : impro.byteToImage(logo);

        if(logoImage.getHeight() < MINIMAL_QR_LOGO_SIZE) logoImage = DEFAULT_LOGO_IMAGE;

        logoImage = scaleLogo(image, logoImage);

        //Calculate the delta height and width
        int deltaHeight = image.getHeight() - logoImage.getHeight();
        int deltaWidth  = image.getWidth()  - logoImage.getWidth();

        //Draw the new image
        BufferedImage combined = new BufferedImage(qrSize, qrSize, BufferedImage.TYPE_3BYTE_BGR);

        Graphics2D g = (Graphics2D)combined.getGraphics();

        g.drawImage(image, 0, 0, null);
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        g.drawImage(logoImage, (int)Math.round(deltaWidth/2), (int)Math.round(deltaHeight/2), null);

        return impro.imageToByte(combined);
    }

    private BufferedImage getDefaultLogoImage() {
        try {
            return ImageIO.read(new File(this.getClass().getResource("/qrcode/kai-logo.png").toURI()));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            return new BufferedImage(MINIMAL_QR_LOGO_SIZE, MINIMAL_QR_LOGO_SIZE, BufferedImage.TYPE_3BYTE_BGR);
        }
    }

    private int minimalQrLogoSize() {
        return (int) (MINIMAL_QR_SIZE * PERCENTAGE_HEIGHT_QR_LOGO);
    }

    private BufferedImage scaleLogo(BufferedImage qrcode, BufferedImage logo) {
        float ratio = logo.getWidth()/logo.getHeight();

        int scaledHeight = Math.round(qrcode.getHeight() * PERCENTAGE_HEIGHT_QR_LOGO);
        int scaledWidth = Math.round(scaledHeight * ratio);

        BufferedImage imageBuff = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_3BYTE_BGR);
        Graphics g = imageBuff.createGraphics();
        g.drawImage(logo.getScaledInstance(scaledWidth, scaledHeight, BufferedImage.SCALE_SMOOTH), 0, 0, new Color(0,0,0), null);
        g.dispose();
        return imageBuff;
    }
}
