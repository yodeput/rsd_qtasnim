package com.qtasnim.eoffice.context;

import com.qtasnim.eoffice.config.ApplicationConfig;
import lombok.Data;

import javax.ejb.Singleton;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import java.util.List;

@Data
@Singleton
public class ApplicationContext {
    @Default
    @Inject
    private ApplicationConfig applicationConfig;

    private List<Class> taskResolvers;

    private List<Class> serviceResolvers;
}
