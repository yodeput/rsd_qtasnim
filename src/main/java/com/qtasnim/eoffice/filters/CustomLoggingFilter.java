package com.qtasnim.eoffice.filters;

import com.qtasnim.eoffice.context.RequestContext;
import com.qtasnim.eoffice.security.IRequestContext;
import com.qtasnim.eoffice.services.LogType;
import org.apache.commons.lang3.StringUtils;
import com.qtasnim.eoffice.services.Logger;

import javax.annotation.Priority;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@Priority(Integer.MAX_VALUE)
public class CustomLoggingFilter implements ContainerRequestFilter {
    @Context
    private HttpServletRequest servletRequest;

    @Inject
    private Logger logger;

    @Inject
    @IRequestContext
    private Event<RequestContext> requestContextEvent;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String transactionId = requestContext.getHeaderString("X-TransactionId");
        String log = "";

        if (StringUtils.isNotEmpty(transactionId)) {
            requestContextEvent.fire(new RequestContext(transactionId, servletRequest.getRemoteAddr()));
            log = String.format(" | %s", transactionId);
        }

        if (!requestContext.getMethod().equals("OPTIONS")) {
            logger.log(LogType.API, String.format("%s | %s", requestContext.getUriInfo().getRequestUri().toString(), log));
        }
    }
}
