package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.PersonalArsip;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.PersonalArsipService;
import com.qtasnim.eoffice.ws.dto.PersonalArsipDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("personal-arsip")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Personal Arsip Resource",
        description = "WS Endpoint untuk menghandle operasi Personal Arsip"
)
public class PersonalArsipResource {
    @Inject
    private PersonalArsipService personalArsipService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = PersonalArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kota list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<PersonalArsip> all = personalArsipService.getAll();
        List<PersonalArsipDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(PersonalArsip::getId))
                .map(PersonalArsipDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = PersonalArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kota list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{

        List<PersonalArsip> data = personalArsipService.getResultList(filter, sort, descending, skip, limit);
        long count = personalArsipService.count(filter);

        List<PersonalArsipDto> result = data.stream().map(q->new PersonalArsipDto(q)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Kota")
    public Response saveData(@Valid PersonalArsipDto dao) {
        try {
            personalArsipService.saveOrEdit(dao, null);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Kota")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid PersonalArsipDto dao) {
        try {
            personalArsipService.saveOrEdit(dao, id);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Kota")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            PersonalArsip model = personalArsipService.find(id);
            personalArsipService.remove(model);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}
