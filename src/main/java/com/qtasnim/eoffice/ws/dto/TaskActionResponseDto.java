package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TaskActionResponseDto {
    private String taskId;
    private String actionType;
    private String taskName;
    private String actionLabel;
    private String context;
    private Boolean isSecretary = false;
    private Long assigneeId;
    private List<FormFieldDto> forms;

    public TaskActionResponseDto() {
        forms = new ArrayList<>();
    }
}
