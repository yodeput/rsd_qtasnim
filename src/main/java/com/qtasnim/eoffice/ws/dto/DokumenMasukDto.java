package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SuratDisposisi;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DokumenMasukDto implements Serializable {

    private Long id;
    private String tanggalDiterima;
    private String tanggalDikirim;
    private String noSurat;
    private String perihal;
    private String sifatNaskah;
    private String klasifikasiDokumen;
    private String jenisDokumen;
    private boolean isKorespondensi;
    private List<PengirimDto> pengirim;
    private Boolean isRead=false;
    private Boolean isStarred=false;
    private Boolean isImportant=false;
    private Boolean isFinished=false;
    private Boolean disposisiStatus=false;
    private List<SuratDisposisiDto> suratDisposisi;

    private String delegasiTipe;
    private String delegasiUser;
    private String delegasiOrganisasi;

    public DokumenMasukDto(){}
}
