package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class ProfileUserDto {

    private String namaNip;
    private String jabatan;
    private String photo;

    public ProfileUserDto(){}

    public ProfileUserDto(String name,String jabatan,String foto){
        this.namaNip = name;
        this.jabatan = jabatan;
        this.photo = foto;
    }
}
