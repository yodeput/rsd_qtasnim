package com.qtasnim.eoffice.ws.dto.chat;

import com.qtasnim.eoffice.db.chat.Group;
import com.qtasnim.eoffice.ws.dto.MasterUserDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Setter
@Getter
@NoArgsConstructor
public class GroupMembersDto {
    private MasterUserDto owner;
    private List<MasterUserDto> list;

    public GroupMembersDto(Group group) {
        owner = Optional.ofNullable(group.getOwner()).map(t -> new MasterUserDto(group.getOwner())).orElse(null);
        list = group.getDetails().stream().map(t -> new MasterUserDto(t.getUser())).collect(Collectors.toList());
    }
}
