package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.DistribusiDokumenDetail;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

@Data
public class DistribusiDokumenDetailDto implements Serializable {

    private Long id;
    private Long idReference;
    private String referenceTable;

    private Long idDistribusi;
    
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public DistribusiDokumenDetailDto() {}

    public DistribusiDokumenDetailDto(DistribusiDokumenDetail model) {
        DateUtil dateUtil = new DateUtil();
        
        this.id = model.getId();
        this.idReference = model.getReferenceId();
        this.referenceTable = model.getReferenceTable();
        
        this.idDistribusi = Optional.ofNullable(model.getDistribusi()).map(q -> q.getId()).orElse(null);
        
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
    }
}
