package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PemeriksaBonNomor;
import com.qtasnim.eoffice.db.PenandatanganBonNomor;
import lombok.Data;

@Data
public class PemeriksaBonNomorDto {

    private Long id;
    private Long idOrgPemeriksa;

    public PemeriksaBonNomorDto(){}

    public PemeriksaBonNomorDto(PemeriksaBonNomor model){
        this.id = model.getId();
        this.idOrgPemeriksa = model.getOrganisasi().getIdOrganization();
    }
}
