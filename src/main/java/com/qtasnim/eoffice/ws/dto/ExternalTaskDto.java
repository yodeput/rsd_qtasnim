package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ExternalTaskDto {
    String activityId;
    String activityInstanceId;
    String errorMessage;
    String errorDetails;
    String executionId;
    String id;
    String lockExpirationTime;
    String processDefinitionId;
    String processDefinitionKey;
    String processDefinitionVersionTag;
    String processInstanceId;
    String retries;
    String suspended;
    String workerId;
    String topicName;
    String tenantId;
    String priority;
    String businessKey;

    public ExternalTaskDto() { }
}
