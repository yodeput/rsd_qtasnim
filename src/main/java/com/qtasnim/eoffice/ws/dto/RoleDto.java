package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterRole;
import com.qtasnim.eoffice.db.constraints.CompanyId;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.EnableUniqueValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterRole", fieldNames = {"roleCode"})
public class RoleDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long idRole;

    @NotNull(message = "roleCode tidak boleh null.")
    @NotEmpty(message = "roleCode harus diisi.")
    @Size(min = 1, max = 10, message = "panjang roleCode harus diantara {min} dan {max}")
    private String roleCode;

    @NotNull(message = "roleName tidak boleh null.")
    @NotEmpty(message = "roleName harus diisi.")
    @Size(min = 1, max = 255, message = "panjang roleName harus diantara {min} dan {max}")
    private String roleName;

    @NotNull(message = "description tidak boleh null.")
    @NotEmpty(message = "description harus diisi.")
    @Size(min = 1, max = 1000, message = "panjang description harus diantara {min} dan {max}")
    private String description;

    private Boolean isActive;
    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;
    private String endDate;

//    private List<Long> orgIds;
//    private List<MasterStrukturOrganisasiDto> orgList;

    @NotNull(message = "companyId tidak boleh null")
    @CompanyId
    private Long companyId;
    private Integer level;

    private CompanyCodeDto companyCode;

    @NotNull(message = "Modul tidak boleh null")
    @NotEmpty(message = "Modul tidak boleh kosong")
    private List<RoleModuleDto> roleModule;
    private List<RoleUserDto> roleUser;

    public RoleDto(){
//        orgList = new ArrayList<>();
//        orgIds = new ArrayList<>();
        roleModule = new ArrayList<>();
    }

    public RoleDto (MasterRole model){
        this(model,true);
    }

    public RoleDto(MasterRole model,Boolean isLoadAll) {
        this.idRole = model.getIdRole();
        this.roleCode = model.getRoleCode();
        this.roleName = model.getRoleName();
        this.description = model.getDescription();
        this.isActive = model.getIsActive();
        this.level = model.getLevel();

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);

        if(isLoadAll) {
            if(model.getRoleModuleList()!=null) {
                this.roleModule = model.getRoleModuleList().stream().map(q -> new RoleModuleDto(q)).collect(Collectors.toList());
            }
            if(model.getOrganisasiList()!=null) {
                this.roleUser = model.getOrganisasiList().stream().map(q -> new RoleUserDto(q)).collect(Collectors.toList());
            }
        }
    }
}
