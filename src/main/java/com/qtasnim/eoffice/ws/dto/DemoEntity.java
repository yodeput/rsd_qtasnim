package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.db.constraints.ISODateAfter;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Dominik Mathmann, GEDOPLAN
 */
@Data
@EnableISODateAfterValidator
public class DemoEntity {
  @NotNull(message = "id cannot be null")
  private Integer id;

  @NotNull(message = "message cannot be null")
  @Size(min = 1, max = 10, message = "message length range should be {min} to {max}")
  private String message;

  @Email(message = "email should be valid")
  private String email;

  @NotNull(message = "isoDate cannot be null")
  @ISODate(format = DateUtil.DATE_FORMAT, message = "isoDate should be valid format: {format}")
  private String isoDate;

//  @NotNull(message = "fromDate cannot be null")
  @ISODate(format = DateUtil.DATE_FORMAT, message = "fromDate should be valid format: {format}")
  private String fromDate;

//  @NotNull(message = "toDate cannot be null")
  @ISODate(format = DateUtil.DATE_FORMAT, message = "toDate should be valid format: {format}")
  @ISODateAfter(field = "fromDate", message = "toDate should be after fromDate")
  private String toDate;

  public DemoEntity(Integer id, String message) {
    this.id = id;
    this.message = message;
  }

  public DemoEntity() {

  }
}
