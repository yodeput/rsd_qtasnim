package com.qtasnim.eoffice.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DocActivityPayloadDto {
    private JenisDokumenDocLibDto data;
}
