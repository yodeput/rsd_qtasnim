package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PemeriksaSurat;
import lombok.Data;

@Data
public class PemeriksaSuratDto {

    private Long id;
    private MasterStrukturOrganisasiDto pemeriksa;
    private Long idPemeriksa;
    private Long userId;
    private MasterUserDto user;
    private Integer seq;
    private String delegasiType;

    public PemeriksaSuratDto(){}

    public PemeriksaSuratDto(PemeriksaSurat model){
        this(model,false);
    }

    public PemeriksaSuratDto(PemeriksaSurat model,boolean includeRelation){
        this.idPemeriksa = model.getOrganization().getIdOrganization();
        this.pemeriksa = new MasterStrukturOrganisasiDto(model.getOrganization());
        this.seq = model.getSeq();
        this.delegasiType = model.getDelegasiType();
        if(includeRelation) {
            bindUser(model);
        }
    }

    public void bindUser(PemeriksaSurat model){
        if (model.getUser() != null) {
            this.user = new MasterUserDto(model.getUser());
            this.userId = model.getUser().getId();
        }
    }
}
