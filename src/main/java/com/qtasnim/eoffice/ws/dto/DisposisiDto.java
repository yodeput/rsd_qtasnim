package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SuratDisposisi;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class DisposisiDto {
    private String tanggalEksekusi;
    private MasterStrukturOrganisasiDto assignee;
    private List<MasterStrukturOrganisasiDto> disposisi;
    private List<String> tindakan;
    private String komentar;
    private String statusEksekusi;

    public DisposisiDto(SuratDisposisi suratDisposisi) {
        if (suratDisposisi != null) {
            DateUtil dateUtil = new DateUtil();
            tanggalEksekusi = Optional.ofNullable(suratDisposisi.getExecutedDate()).map(dateUtil::getCurrentISODate).orElse(null);
            assignee = new MasterStrukturOrganisasiDto(suratDisposisi.getOrganization());
            disposisi = suratDisposisi.getDisposisi().stream().map(t -> new MasterStrukturOrganisasiDto(t.getOrganization())).collect(Collectors.toList());
            tindakan = suratDisposisi.getTindakanDisposisi().stream().map(t -> t.getTindakan().getNama()).collect(Collectors.toList());
            komentar = suratDisposisi.getComment();
            statusEksekusi = suratDisposisi.getStatus();
        }
    }
}
