package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterVendor;
import com.qtasnim.eoffice.db.PenandatanganSurat;
import lombok.Data;

import java.util.Optional;

@Data
public class PenandatanganSuratDto {
    private Long id;
    private MasterStrukturOrganisasiDto penandatangan;
    private Long idPenandatangan;
    private Integer seq;
    private MasterUserDto user;
    private Long userId;
    private MasterStrukturOrganisasiDto orgAN;
    private MasterUserDto usrAN;
    private Long idANOrg;
    private Long idANUser;
    private Long idSurat;
    private Long idVendor;
    private MasterVendorDto vendor;
    private String delegasiType;

    public PenandatanganSuratDto(){}

    public PenandatanganSuratDto(PenandatanganSurat model){
        this(model,false);
    }

    public PenandatanganSuratDto(PenandatanganSurat model,boolean includeRelation){
        this.id = model.getId();

        Optional.ofNullable(model.getOrganization()).ifPresent(org -> {
            this.penandatangan = new MasterStrukturOrganisasiDto(org);
            this.idPenandatangan = org.getIdOrganization();
        });

        this.seq = model.getSeq();
        this.delegasiType = model.getDelegasiType();

        if(model.getSurat()!=null) {
            this.idSurat =model.getSurat().getId();
        }

        Optional.ofNullable(model.getVendor()).ifPresent(vendor -> {
            this.idVendor = vendor.getId();
            this.vendor = new MasterVendorDto(vendor);
        });

        if(includeRelation){
            bindRelation(model);
        }
    }

    public void bindRelation(PenandatanganSurat model){
        if(model.getUserAN()!=null) {
            this.idANUser = model.getUserAN().getId();
            this.usrAN = new MasterUserDto(model.getUserAN());
        }

        if(model.getUser()!=null){
            this.user = new MasterUserDto(model.getUser());
            this.userId = model.getUser().getId();
        }

        if(model.getOrgAN()!=null) {
            this.idANOrg = model.getOrgAN().getIdOrganization();
            this.orgAN = new MasterStrukturOrganisasiDto(model.getOrgAN());
        }
    }
}
