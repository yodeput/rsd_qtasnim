package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SysChangeLog;
import com.qtasnim.eoffice.db.SysLog;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@EnableISODateAfterValidator
public class SysChangeLogDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String version;
    private String changeLogDate;
    private Boolean isWeb;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;
    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    private List<SysChangeLogDetailDto> detail;

    public SysChangeLogDto() {
        detail = new ArrayList<>();
    }
    
    private Long lastId;

    public SysChangeLogDto(SysChangeLog model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.version = model.getVersion();
        this.changeLogDate = dateUtil.getCurrentISODate(model.getChangeLogDate());
        this.isWeb = model.getIsWeb();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();

        this.detail = model.getChangeLogDetailList().stream().map(q -> new SysChangeLogDetailDto(q)).collect(Collectors.toList());
    }
}