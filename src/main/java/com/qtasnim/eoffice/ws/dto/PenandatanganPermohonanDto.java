package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PenandatanganPermohonan;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class PenandatanganPermohonanDto {

    private Long id;


    private Long idUser;
    private MasterUserDto user;

    private Long idPermohonan;
    private PermohonanDokumenDto permohonan;

    private Long idOrganisasi;
    private MasterStrukturOrganisasiDto organisasi;


    public PenandatanganPermohonanDto(){}

    public PenandatanganPermohonanDto(PenandatanganPermohonan model){
        DateUtil dateUtil = new DateUtil();
        this.id= model.getId();

        this.idUser = Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.idOrganisasi = Optional.ofNullable(model.getOrganisasi()).map(a->a.getIdOrganization()).orElse(null);
        this.organisasi = Optional.ofNullable(model.getOrganisasi()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idPermohonan = Optional.ofNullable(model.getPermohonan()).map(a->a.getId()).orElse(null);
        this.permohonan = Optional.ofNullable(model.getPermohonan()).map(PermohonanDokumenDto::new).orElse(null);

    }
}
