package com.qtasnim.eoffice.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoggerPayloadDto {
    private String id;
    private String log;
}
