package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PenerimaPara;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Data
public class PenerimaParaDto {

    private Long id;

    @NotNull(message = "Id Penerima tidak boleh null")
    private Long idPenerima;
    private PenerimaSuratDto penerimaSurat;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    @NotNull(message = "Id Organisasi tidak boleh null")
    private Long idOrganisasi;
    private MasterStrukturOrganisasiDto organisasi;

    private Long idUser;
    private MasterUserDto user;

    public PenerimaParaDto(){}

    public PenerimaParaDto(PenerimaPara model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.idPenerima = Optional.ofNullable(model.getPenerimaSurat()).map(a->a.getId()).orElse(null);
        this.penerimaSurat = Optional.ofNullable(model.getPenerimaSurat()).map(PenerimaSuratDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.idOrganisasi = Optional.ofNullable(model.getOrganisasi()).map(a->a.getIdOrganization()).orElse(null);
        this.organisasi= Optional.ofNullable(model.getOrganisasi()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idUser= Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
        this.user= Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
    }
}
