package com.qtasnim.eoffice.ws.dto;

import java.util.List;

public interface IWorkflowDto {
    String getWorkflowAction();
    void setWorkflowAction(String workflowAction);
    Object getWorkflowResponse();
    void setWorkflowResponse(Object workflowResponse);
    String getWorkflowFormName();
    void setWorkflowFormName(String workflowFormName);
    List<String> getWorkflowCurrentTasks();
    String getWorkflowNote();
    void setWorkflowNote(String workflowNote);
}
