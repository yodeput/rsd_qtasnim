package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterRole;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterUser;
import lombok.Data;

import java.io.Serializable;

@Data
public class RoleUserDto implements Serializable {

    private Long idUser;
    private String nipp;
    private String nama;
    private String jabatan;

    public RoleUserDto(){}

    public RoleUserDto(MasterStrukturOrganisasi org){
        if(org.getUser()!=null) {
            this.idUser = org.getUser().getId();
            this.nama = org.getUser().getNameFront() + " " + org.getUser().getNameMiddleLast();
            this.nipp = org.getUser().getEmployeeId();
        }
        this.jabatan = org.getOrganizationName();
    }
}
