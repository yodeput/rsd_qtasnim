package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class LampiranDto implements Serializable {
    private String id;
    private String namaFile;
    private Long ukuran;
    private String tipe;
    private String waktu;
    private String pengunggah;
    private String downloadLink;

    public LampiranDto(){}
}
