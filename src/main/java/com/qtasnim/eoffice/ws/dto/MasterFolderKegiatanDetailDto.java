package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterFolderKegiatan;
import com.qtasnim.eoffice.db.MasterFolderKegiatanDetail;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
public class MasterFolderKegiatanDetailDto implements Serializable {

    private Long id;
    private String folderName;
    private Long idParent;
    private MasterFolderKegiatanDetailDto parent;
    private Long idPersonal;
    private MasterFolderKegiatanDto personal;
    private Long idCompany;
    private CompanyCodeDto companyCode;
    private Long idUser;
    private MasterUserDto user;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public MasterFolderKegiatanDetailDto(){}

    public MasterFolderKegiatanDetailDto(MasterFolderKegiatanDetail model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.folderName = model.getFolderName();
        this.idPersonal = Optional.ofNullable(model.getPersonal()).map(a->a.getId()).orElse(null);
        this.personal = Optional.ofNullable(model.getPersonal()).map(MasterFolderKegiatanDto::new).orElse(null);
        this.idParent = Optional.ofNullable(model.getParent()).map(a->a.getId()).orElse(null);
        this.parent = Optional.ofNullable(model.getParent()).map(MasterFolderKegiatanDetailDto::new).orElse(null);
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
    }
}
