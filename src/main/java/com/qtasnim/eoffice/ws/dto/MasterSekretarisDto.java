package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterSekretaris;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
public class MasterSekretarisDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    //@NotNull(message = "tipePenerimaan tidak boleh null")
    //@Digits(integer = 10, fraction = 0, message = "tipePenerimaan harus berupa angka.")
    //@Size(min = 0, max = 1, message = "panjang tipePenerimaan maksimal {max}")
    private String tipePenerimaan;

//    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private MasterStrukturOrganisasiDto pejabat;
    private MasterStrukturOrganisasiDto sekretaris;
    private Long pejabatId;
    private Long sekretarisId;
    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    public MasterSekretarisDto(){}

    public MasterSekretarisDto(MasterSekretaris model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.pejabat = Optional.ofNullable(model.getPejabat()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.sekretaris = Optional.ofNullable(model.getSekretaris()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.pejabatId = Optional.ofNullable(model.getPejabat()).map(q->q.getIdOrganization()).orElse(null);
        this.sekretarisId = Optional.ofNullable(model.getSekretaris()).map(q->q.getIdOrganization()).orElse(null);
        this.tipePenerimaan = model.getTipePenerimaan();
        this.isActive = model.getIsActive();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }

}
