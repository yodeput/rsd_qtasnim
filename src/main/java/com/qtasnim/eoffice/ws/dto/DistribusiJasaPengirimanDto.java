package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
public class DistribusiJasaPengirimanDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String nomorDokumen;
    private String perihal;
    private String nomorPengiriman;
    private String nomorResi;
    private String status;
    private String alasanRetur;

    private String tglDokumen;
    private String tglPengiriman;
    private String tglDiterima;
    private String tglRetur;

    private Boolean isDeleted;
    private Boolean isInternal;

    @NotNull(message = "companyId tidak boleh null")
    private Long idCompany;
    private CompanyCodeDto companyCode;

    private Long idSurat;
    private SuratDto surat;

    private Long idKlasifikasiKeamanan;
    private MasterKlasifikasiKeamananDto klasifikasiKeamanan;

    private Long idTingkatUrgensi;
    private MasterTingkatUrgensiDto tingkatUrgensi;

    private Long idTingkatPerkembangan;
    private MasterTingkatPerkembanganDto tingkatPerkembangan;

//    private String namaPerusahaanPengiriman;
    private Long idPerusahaanPengiriman;
    private MasterPerusahaanPengirimanDto perusahaanPengiriman;

    @NotNull(message = "idPengirim tidak boleh null")
    private Long idPengirim;
    private MasterUserDto pengirim;

    private Long idTujuanInternal;
    private MasterStrukturOrganisasiDto tujuanInternal;

    private Long idTujuanEksternal;
    private MasterVendorDto tujuanEksternal;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;


    public DistribusiJasaPengirimanDto() { }

    public DistribusiJasaPengirimanDto(DistribusiJasaPengiriman model) {
        this();

        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.nomorDokumen = model.getNomorDokumen();
        this.perihal = model.getPerihal();
        this.nomorPengiriman = model.getNomorPengiriman();
        this.nomorResi = model.getNomorResi();
        this.status = model.getStatus();
        this.alasanRetur = model.getAlasanRetur();

        if (model.getTglDokumen() != null) {
            this.tglDokumen = Optional.ofNullable(dateUtil.getCurrentISODate(model.getTglDokumen())).orElse(null);
        }
        if (model.getTglPengiriman() != null) {
            this.tglPengiriman = Optional.ofNullable(dateUtil.getCurrentISODate(model.getTglPengiriman())).orElse(null);
        }
        if (model.getTglDiterima() != null) {
            this.tglDiterima = Optional.ofNullable(dateUtil.getCurrentISODate(model.getTglDiterima())).orElse(null);
        }
        if (model.getTglRetur() != null) {
            this.tglRetur = Optional.ofNullable(dateUtil.getCurrentISODate(model.getTglRetur())).orElse(null);
        }

        this.isDeleted = model.getIsDeleted();
        this.isInternal = model.getIsInternal();

        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(CompanyCode::getId).orElse(null);
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);

        this.idSurat = Optional.ofNullable(model.getSurat()).map(Surat::getId).orElse(null);
        this.surat = Optional.ofNullable(model.getSurat()).map(SuratDto::new).orElse(null);

        this.idKlasifikasiKeamanan = Optional.ofNullable(model.getKlasifikasiKeamanan()).map(MasterKlasifikasiKeamanan::getIdKlasifikasiKeamanan).orElse(null);
        this.klasifikasiKeamanan = Optional.ofNullable(model.getKlasifikasiKeamanan()).map(MasterKlasifikasiKeamananDto::new).orElse(null);

        this.idTingkatUrgensi = Optional.ofNullable(model.getTingkatUrgensi()).map(MasterTingkatUrgensi::getId).orElse(null);
        this.tingkatUrgensi = Optional.ofNullable(model.getTingkatUrgensi()).map(MasterTingkatUrgensiDto::new).orElse(null);

        this.idTingkatPerkembangan = Optional.ofNullable(model.getTingkatPerkembangan()).map(MasterTingkatPerkembangan::getId).orElse(null);
        this.tingkatPerkembangan = Optional.ofNullable(model.getTingkatPerkembangan()).map(MasterTingkatPerkembanganDto::new).orElse(null);

//        this.namaPerusahaanPengiriman = Optional.ofNullable(model.getPerusahaanPengiriman()).map(MasterPerusahaanPengiriman::getNama).orElse(null);
        this.idPerusahaanPengiriman = Optional.ofNullable(model.getPerusahaanPengiriman()).map(MasterPerusahaanPengiriman::getId).orElse(null);
        this.perusahaanPengiriman = Optional.ofNullable(model.getPerusahaanPengiriman()).map(MasterPerusahaanPengirimanDto::new).orElse(null);

        this.idPengirim = Optional.ofNullable(model.getPengirim()).map(MasterUser::getId).orElse(null);
        this.pengirim = Optional.ofNullable(model.getPengirim()).map(MasterUserDto::new).orElse(null);

        this.idTujuanInternal = Optional.ofNullable(model.getTujuanInternal()).map(MasterUser::getId).orElse(null);
        this.tujuanInternal = Optional.ofNullable(model.getTujuanInternal()).map(q -> new MasterStrukturOrganisasiDto(q.getOrganizationEntity())).orElse(null);

        this.idTujuanEksternal = Optional.ofNullable(model.getTujuanEksternal()).map(MasterVendor::getId).orElse(null);
        this.tujuanEksternal = Optional.ofNullable(model.getTujuanEksternal()).map(MasterVendorDto::new).orElse(null);

        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();

        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
    }
}
