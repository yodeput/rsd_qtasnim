package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.ParaDetail;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
public class ParaDetailDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long idParaDetail;
    private Long idPara;

    private Long idOrganization;
    private MasterStrukturOrganisasiDto orgList;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;
    private String endDate;

    private Long idUser;
    private MasterUserDto user;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    public ParaDetailDto(){}

    public ParaDetailDto(ParaDetail model) {
        this.idParaDetail = model.getIdParaDetail();
        this.idPara = Optional.ofNullable(model.getPara()).map(a->a.getIdPara()).orElse(null);

        this.idOrganization = Optional.ofNullable(model.getOrganisasi()).map(a->a.getIdOrganization()).orElse(null);
        this.orgList = Optional.ofNullable(model.getOrganisasi()).map(MasterStrukturOrganisasiDto::new).orElse(null);

        this.idUser = Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(q->new MasterUserDto(q)).orElse(null);

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}
