package com.qtasnim.eoffice.ws.dto.ng;

import com.qtasnim.eoffice.db.FormDefinition;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class FormDefinitionDto {
    private Long id;
    private String name;
    private Double version;
    private String activationDate;
    private String createdDate;
    private Boolean activated = false;
    private Boolean majorVersion = false;
    private List<FormFieldDto> fields;

    public FormDefinitionDto() {
        fields = new ArrayList<>();
    }

    public FormDefinitionDto(FormDefinition formDefinition) {
        if (formDefinition != null) {
            DateUtil dateUtil = new DateUtil();
            id = formDefinition.getId();
            name = formDefinition.getName();
            version = formDefinition.getVersion();
            createdDate = dateUtil.getCurrentISODate(formDefinition.getCreatedDate());
            activationDate = Optional.of(formDefinition.getStart()).map(dateUtil::getCurrentISODate).orElse(null);
            fields = formDefinition.getFormFields().stream().map(FormFieldDto::new).collect(Collectors.toList());

            if (version % 1 == 0) {
                majorVersion = true;
            }
        }
    }
}
