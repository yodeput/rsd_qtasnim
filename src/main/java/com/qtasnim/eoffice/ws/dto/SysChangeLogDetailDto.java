package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SysChangeLog;
import com.qtasnim.eoffice.db.SysChangeLogDetail;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class SysChangeLogDetailDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private Long idChangeLog;
    private String description;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    public SysChangeLogDetailDto() {
    }

    public SysChangeLogDetailDto(SysChangeLogDetail model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.idChangeLog = Optional.ofNullable(model.getChangeLog()).map(q -> q.getId()).orElse(null);
        ;
        this.description = model.getDescription();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
    }
}