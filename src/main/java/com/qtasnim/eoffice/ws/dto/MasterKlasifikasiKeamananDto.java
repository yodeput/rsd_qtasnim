package com.qtasnim.eoffice.ws.dto;

import java.io.Serializable;
import java.util.Optional;

import com.qtasnim.eoffice.db.MasterKlasifikasiKeamanan;
import com.qtasnim.eoffice.db.constraints.*;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterKlasifikasiKeamanan", fieldNames = {"kodeKlasifikasiKeamanan", "namaKlasifikasiKeamanan"})
public class MasterKlasifikasiKeamananDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long idKlasifikasiKeamanan;

    @NotNull(message = "kode klasifikasi tidak boleh null.")
    @NotEmpty(message = "kode klasifikasi harus diisi.")
    @Size(min = 1, max = 10, message = "panjang kode klasifikasi harus diantara {min} dan {max}")
    @Unique(fieldName = "kodeKlasifikasiKeamanan", entityName = "MasterKlasifikasiKeamanan", message = "Kode sudah ada.")
    private String kodeKlasifikasiKeamanan;
    
    @NotNull(message = "nama klasifikasi tidak boleh null.")
    @NotEmpty(message = "nama klasifikasi harus diisi.")
    @Size(min = 1, max = 255, message = "panjang nama klasifikasi harus diantara {min} dan {max}")
    @Unique(fieldName = "namaKlasifikasiKeamanan", entityName = "MasterKlasifikasiKeamanan", message = "Nama sudah ada.")
    private String namaKlasifikasiKeamanan;

    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    @CompanyId
    private Long companyId;

    public MasterKlasifikasiKeamananDto(){}

    public MasterKlasifikasiKeamananDto(MasterKlasifikasiKeamanan model) {
        this.idKlasifikasiKeamanan = model.getIdKlasifikasiKeamanan();
        this.kodeKlasifikasiKeamanan = model.getKodeKlasifikasiKeamanan();
        this.namaKlasifikasiKeamanan = model.getNamaKlasifikasiKeamanan();
        this.isActive = model.getIsActive();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}