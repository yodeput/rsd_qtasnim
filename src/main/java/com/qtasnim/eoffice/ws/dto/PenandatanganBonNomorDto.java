package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PemeriksaSurat;
import com.qtasnim.eoffice.db.PenandatanganBonNomor;
import lombok.Data;

@Data
public class PenandatanganBonNomorDto {

    private Long id;
    private Long idOrgPenandatangan;

    public PenandatanganBonNomorDto(){}

    public PenandatanganBonNomorDto(PenandatanganBonNomor model){
        this.id = model.getId();
        this.idOrgPenandatangan = model.getOrganisasi().getIdOrganization();
    }
}
