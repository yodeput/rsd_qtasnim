package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterJenisDokumen;
import com.qtasnim.eoffice.db.MasterKewenangan;
import com.qtasnim.eoffice.db.MasterKewenanganPenerima;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class MasterKewenanganPenerimaDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull(message = "grade penerima tidak boleh null.")
    @NotEmpty(message = "grade penerima harus diisi.")
    @Size(min = 1, max = 50, message = "panjang grade harus diantara {min} dan {max}")
    private String gradePenerima;

    @NotNull(message = "grade pengirim tidak boleh null.")
    @NotEmpty(message = "grade pengirim harus diisi.")
    @Size(min = 1, max = 50, message = "panjang grade harus diantara {min} dan {max}")
    private String gradePengirim;

//    @NotNull(message = "kewenangan tidak boleh null.")
//    @NotEmpty(message = "kewenangan harus diisi.")
    private String kewenangan;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    private Long idJenisDokumen;
    private MasterJenisDokumenDto jenisDokumen;

    private List<MasterGradeDto> listGradePengirim;
    private List<MasterGradeDto> listGradePenerima;
    private Boolean isDeleted;

    public MasterKewenanganPenerimaDto() {
        this.listGradePengirim = new ArrayList<>();
        this.listGradePenerima = new ArrayList<>();
    }

    public MasterKewenanganPenerimaDto(MasterKewenanganPenerima model) {
        this.id = model.getId();
        this.gradePenerima = model.getGradePenerima();
        this.gradePengirim = model.getGradePengirim();
        this.kewenangan = model.getKewenangan();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(CompanyCode::getId).orElse(null);

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());

        this.jenisDokumen = Optional.ofNullable(model.getJenisDokumen()).map(MasterJenisDokumenDto::new).orElse(null);
        this.idJenisDokumen = Optional.ofNullable(model.getJenisDokumen()).map(MasterJenisDokumen::getIdJenisDokumen).orElse(null);
        this.isDeleted = model.getIsDeleted();
    }
}