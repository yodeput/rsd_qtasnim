package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.TandaTangan;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

@Data
public class TandaTanganDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;
    private String kode;
//    private byte[] tandatangan;
    private String tandatangan;
    private MasterUserDto user;
    private Long idUser;
    private String tipe;
    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;
    private String startDate;
    private String endDate;
    private Boolean isDeleted;

    public TandaTanganDto(){}

    public TandaTanganDto(TandaTangan model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.kode = model.getKode();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.idUser = Optional.ofNullable(model.getUser()).map(q->q.getId()).orElse(null);
        this.tipe = model.getTipe();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        if(model.getTandatangan()!=null) {
            this.tandatangan = Base64.getEncoder().encodeToString(model.getTandatangan());
        }
        this.isDeleted = model.getIsDeleted();
    }
}
