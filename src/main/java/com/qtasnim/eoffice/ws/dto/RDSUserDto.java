package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterArea;
import com.qtasnim.eoffice.db.MasterGrade;
import com.qtasnim.eoffice.db.MasterKorsa;
import com.qtasnim.eoffice.db.MasterKota;
import com.qtasnim.eoffice.db.MasterPropinsi;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;
import org.apache.commons.text.WordUtils;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Data
public class RDSUserDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String username;
    private String employeeId;
    private String nameFront;
    private String nameMiddleLast;
    private String nameFull;
    private String email;
    private String plans;
    private String planText;
    private String businessArea;
    private String jabatan;
    private String kedudukan;
    private String gradeId;
    private String korsaId;
    private String unit;
    private String areaId;
    private String agama;
    private String kelamin;
    private String tempatLahir;
    private String tanggalLahir;
    private String mobilePhone;
    private String kota;
    private String provinsi;
    private Long organizationId;
    private Long userId;

    public RDSUserDto(MasterUser user){
        DateUtil dateUtil = new DateUtil();
        this.id = user.getId();
        this.username = user.getLoginUserName();
        this.employeeId = Optional.ofNullable(user.getEmployeeId()).orElse(null);
        this.userId = user.getId();
        this.nameFull =  WordUtils.capitalizeFully(String.format("%s %s", Optional.ofNullable(user.getNameFront()).orElse(""), Optional.ofNullable(user.getNameMiddleLast()).orElse("")).trim());
        this.email = Optional.ofNullable(user.getEmail()).orElse(null);
        this.plans = Optional.ofNullable(user.getOrganizationEntity()).map(MasterStrukturOrganisasi::getOrganizationCode).orElse(null);
        this.planText = Optional.ofNullable(user.getOrganizationEntity()).map(MasterStrukturOrganisasi::getOrganizationName).orElse(null);
        this.organizationId = Optional.ofNullable(user.getOrganizationEntity()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null);
        this.businessArea = Optional.ofNullable(user.getBusinessArea()).orElse(null);
        this.jabatan = Optional.ofNullable(user.getJabatan()).orElse(null);
        this.kedudukan = Optional.ofNullable(user.getKedudukan()).orElse(null);
        this.gradeId = Optional.ofNullable(user.getGrade()).map(MasterGrade::getId).orElse(null);
        this.korsaId = Optional.ofNullable(user.getKorsa()).map(MasterKorsa::getId).orElse(null);
        this.unit = Optional.ofNullable(user.getKorsa()).map(MasterKorsa::getNama).orElse(null);
        this.areaId = Optional.ofNullable(user.getArea()).map(MasterArea::getId).orElse(null);
        this.agama = Optional.ofNullable(user.getAgama()).orElse(null);
        this.kelamin = Optional.ofNullable(user.getKelamin()).orElse(null);
        this.tempatLahir = Optional.ofNullable(user.getTempatLahir()).orElse(null);
        this.tanggalLahir = Optional.ofNullable(user.getTanggalLahir()).map(dateUtil::getCurrentISODate).orElse(null);
        this.mobilePhone = Optional.ofNullable(user.getMobilePhone()).orElse(null);

        Optional.ofNullable(user.getArea()).filter(t -> t.getKota() != null).map(MasterArea::getKota).ifPresent(kota -> {
            this.kota = kota.getNamaKota();
            this.kota = Optional.ofNullable(kota.getPropinsi()).map(MasterPropinsi::getNama).orElse(null);
        });
    }

}
