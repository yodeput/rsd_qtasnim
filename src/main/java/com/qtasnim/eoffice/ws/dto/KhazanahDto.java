package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Khazanah;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class KhazanahDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String tipeKhazanah;

    @NotNull(message = "judul tidak boleh null")
    @NotEmpty(message = "judul tidak boleh kosong")
    private String judul;

    private String deskripsi;
    private Long idBahasa;
    private MasterBahasaDto bahasa;
    private Long idNegara;
    private MasterNegaraDto negara;
    private Long idKlasifikasiKhazanah;
    private MasterKlasifikasiKhazanahDto klasifikasiKhazanah;

    @NotNull(message = "idCompany tidak boleh null")
    private Long idCompany;
    private CompanyCodeDto company;
    private String penerbit;
    private Integer tahunTerbit;
    private String penulis;
    private Integer jmlHalaman;
    private String jenisBuku;
    private String lokasiSimpan;
    private String penulisSkenario;
    private String durasi;
    private String media;
    private String draft;
    private String status;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    private GlobalAttachmentDto cover;
    private GlobalAttachmentDto content;

    public KhazanahDto(){}

    public KhazanahDto(Khazanah model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.tipeKhazanah = model.getTipeKhazanah();
        this.judul = model.getJudul();
        this.deskripsi = model.getDeskripsi();
        this.idBahasa = Optional.ofNullable(model.getBahasa()).map(a->a.getId()).orElse(null);
        this.bahasa = Optional.ofNullable(model.getBahasa()).map(MasterBahasaDto::new).orElse(null);
        this.idNegara = Optional.ofNullable(model.getNegara()).map(a->a.getId()).orElse(null);
        this.negara = Optional.ofNullable(model.getNegara()).map(MasterNegaraDto::new).orElse(null);
        this.idKlasifikasiKhazanah = Optional.ofNullable(model.getKlasifikasiKhazanah()).map(a->a.getId()).orElse(null);
        this.klasifikasiKhazanah = Optional.ofNullable(model.getKlasifikasiKhazanah()).map(MasterKlasifikasiKhazanahDto::new).orElse(null);
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.penerbit = model.getPenerbit();
        this.tahunTerbit = Optional.ofNullable(model.getTahunTerbit()).map(a->a.intValue()).orElse(null);
        this.penulis = model.getPenulis();
        this.jmlHalaman = model.getJmlHalaman();
        this.jenisBuku = model.getJenisBuku();
        this.lokasiSimpan = model.getLokasiSimpan();
        this.penulisSkenario = model.getPenulisSkenario();
        if(model.getDurasi()!=null) {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            this.durasi = dateFormat.format(model.getDurasi());
        }
        this.media = model.getMedia();
        this.draft = model.getDraft();
        this.status = model.getStatus();
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
    }
}
