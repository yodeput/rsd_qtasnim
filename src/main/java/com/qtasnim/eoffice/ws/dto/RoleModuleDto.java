package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterModul;
import com.qtasnim.eoffice.db.MasterRole;
import com.qtasnim.eoffice.db.RoleModule;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

@Data
public class RoleModuleDto implements Serializable {

    private Long id;
    private Long idRole;
    private RoleDto role;
    private Long idModul;
    private Long idModulParent;
    private MasterModulDto module;
    private Boolean allowCreate;
    private Boolean allowDelete;
    private Boolean allowPrint;
    private Boolean allowRead;
    private Boolean allowUpdate;

    public RoleModuleDto(){}

    public RoleModuleDto(RoleModule model){
        this.id = model.getId();
        this.idRole = Optional.ofNullable(model.getRoles()).map(MasterRole::getIdRole).orElse(null);

        Optional.ofNullable(model.getModules()).ifPresent(masterModul -> {
            this.idModul = masterModul.getIdModul();
            this.module = new MasterModulDto(masterModul);
            this.idModulParent = Optional.ofNullable(masterModul.getParent()).map(MasterModul::getIdModul).orElse(null);
        });

        this.allowCreate = model.getAllowCreate();
        this.allowDelete = model.getAllowDelete();
        this.allowPrint = model.getAllowPrint();
        this.allowRead = model.getAllowRead();
        this.allowUpdate = model.getAllowUpdate();
    }
}
