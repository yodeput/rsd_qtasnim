package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PenerimaSurat;
import lombok.Data;

import java.util.Optional;

@Data
public class PenerimaSuratDto {
    private Long id;
//    private SuratDto surat;
    private Long idSurat;
    private MasterStrukturOrganisasiDto organization;
    private Long idOrganization;
    private ParaDto para;
    private Long idPara;
    private Integer seq;
    private Long idVendor;
    private MasterVendorDto vendor;
    private Long idUser;
    private MasterUserDto user;
    private String delegasiType;

    public PenerimaSuratDto(){}

    public PenerimaSuratDto(PenerimaSurat model){
        this(model,false);
    }

    public PenerimaSuratDto(PenerimaSurat model, boolean includeRelation){
        this.id = model.getId();
        this.idSurat = Optional.ofNullable(model.getSurat()).map(a->a.getId()).orElse(null);
        this.organization = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idOrganization = Optional.ofNullable(model.getOrganization()).map(a->a.getIdOrganization()).orElse(null);
        this.seq = model.getSeq();
        this.idVendor = Optional.ofNullable(model.getVendor()).map(a->a.getId()).orElse(null);
        this.vendor = Optional.ofNullable(model.getVendor()).map(MasterVendorDto::new).orElse(null);
        this.delegasiType = model.getDelegasiType();
        if(includeRelation){
            bindPara(model);
            bindUser(model);
        }
    }

    public void bindPara(PenerimaSurat model){
        this.para = Optional.ofNullable(model.getPara()).map(ParaDto::new).orElse(null);
        this.idPara = Optional.ofNullable(model.getPara()).map(a->a.getIdPara()).orElse(null);
    }

    public void bindUser(PenerimaSurat model){
        this.idUser = Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
    }
}
