package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PenerimaNomorManual;
import com.qtasnim.eoffice.db.PenerimaSurat;
import lombok.Data;

import java.util.Optional;

@Data
public class PenerimaNomorManualDto {
    private Long id;
    private Long idPermohonan;
    private MasterStrukturOrganisasiDto organization;
    private Long idOrganization;
    private Integer seq;
    private Long idVendor;
    private MasterVendorDto vendor;
    private Long idUser;
    private MasterUserDto user;
    private String delegasiType;

    public PenerimaNomorManualDto(){}

    public PenerimaNomorManualDto(PenerimaNomorManual model){
        this.id = model.getId();
        this.idPermohonan = Optional.ofNullable(model.getPermohonan()).map(a->a.getId()).orElse(null);
        this.organization = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idOrganization = Optional.ofNullable(model.getOrganization()).map(a->a.getIdOrganization()).orElse(null);
        this.seq = model.getSeq();
        this.idVendor = Optional.ofNullable(model.getVendor()).map(a->a.getId()).orElse(null);
        this.vendor = Optional.ofNullable(model.getVendor()).map(MasterVendorDto::new).orElse(null);
        this.idUser = Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.delegasiType = model.getDelegasiType();
    }
}
