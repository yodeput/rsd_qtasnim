/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Agie
 */
@Data
public class FolderBerkasDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private MasterFolderKegiatanDetailDto parent;
    private List<MasterFolderKegiatanDetailDto> children;
    private List<BerkasDto> berkases;
    private List<GlobalAttachmentDto> files;

    public FolderBerkasDto() {
        children = new ArrayList<>();
        berkases = new ArrayList<>();
        files = new ArrayList<>();
    }
}
