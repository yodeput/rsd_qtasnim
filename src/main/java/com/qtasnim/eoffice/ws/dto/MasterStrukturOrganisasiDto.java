package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
public class MasterStrukturOrganisasiDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long organizationId;

//    @NotNull(message = "kode tidak boleh null.")
//    @NotEmpty(message = "kode harus diisi.")
//    @Size(min = 1, max = 50, message = "panjang kode harus diantara {min} dan {max}")
    private String organizationCode;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Size(min = 1, max = 255, message = "panjang nama harus diantara {min} dan {max}")
    private String organizationName;

//    @NotNull(message = "isDeleted tidak boleh null")
    private Boolean isDeleted;

//    @NotNull(message = "level tidak boleh null")
//    @Digits(integer = 10, fraction = 0, message = "level harus berupa angka.")
    private Integer level;

    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private MasterStrukturOrganisasiDto parent;
    private Long parentId;
    private MasterUserDto user;
    private Long idUser;
    //    private List<RoleDto> roleList;
    private List<Long> roleIds;
    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    private String idPersa;
    private MasterAreaDto persa;

    private Boolean isHris;
    private String singkatan;

    private Boolean isFavorite;
    private Boolean isPosisi;
    private Boolean isChief;
    private String clazz;
    private String grade;
    private MasterKorsaDto korsa;

    public MasterStrukturOrganisasiDto(){}

    public MasterStrukturOrganisasiDto(MasterStrukturOrganisasi master) {
        DateUtil dateUtil = new DateUtil();
        this.organizationId = master.getIdOrganization();
        this.organizationCode = master.getOrganizationCode();
        this.organizationName = master.getOrganizationName();
        this.parent = Optional.ofNullable(master.getParent()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.parentId = Optional.ofNullable(master.getParent()).map(a->a.getIdOrganization()).orElse(null);
        this.isDeleted = master.getIsDeleted();
        this.isActive = master.getIsActive();
        this.level = master.getLevel();
        this.createdBy = master.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(master.getCreatedDate());
        this.modifiedBy = master.getModifiedBy();
        if(master.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(master.getModifiedDate());
        }
        this.startDate = dateUtil.getCurrentISODate(master.getStart());
        this.endDate = dateUtil.getCurrentISODate(master.getEnd());
        if(master.getUser()!=null) {
            if(!master.getUser().getIsDeleted().booleanValue() && master.getUser().getIsActive().booleanValue()) {
                this.user = new MasterUserDto(master.getUser(), false);
                this.idUser = master.getUser().getId();
            }else{
                this.user = null;
                this.idUser = null;
            }
        }else{
            this.user = null;
            this.idUser = null;
        }
//        this.roleList = master.getRoleList().stream().map(q->new RoleDto(q)).collect(Collectors.toList());
//        this.roleIds = master.getRoleList().stream().map(q -> q.getIdRole()).collect(Collectors.toList());
        this.companyCode = Optional.ofNullable(master.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(master.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.persa = Optional.ofNullable(master.getArea()).map(MasterAreaDto::new).orElse(null);
        this.idPersa = Optional.ofNullable(master.getArea()).map(q -> q.getId()).orElse(null);
        this.isHris = master.getIsHris();
        this.singkatan = master.getSingkatan();
        this.isPosisi = master.getIsPosisi();
        this.isChief = master.getIsChief();
        this.clazz = Optional.ofNullable(master.getClazz()).orElse(null);
        this.grade = Optional.ofNullable(master.getGrade()).map(a->a.getId()).orElse(null);
        this.korsa = Optional.ofNullable(master.getUnit()).map(MasterKorsaDto::new).orElse(null);
    }
}