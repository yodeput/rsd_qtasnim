package com.qtasnim.eoffice.ws.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilePreviewModelDto {
    private String fileId;
    private String fileName;
    private String type;
    private String mime;
}
