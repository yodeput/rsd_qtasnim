package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterArea;
import com.qtasnim.eoffice.db.MasterKorsa;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.db.constraints.ISODateAfter;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class MasterKorsaDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "id tidak boleh null.")
    @NotEmpty(message = "id harus diisi.")
    private String id;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Size(min = 1, max = 100, message = "panjang nama harus diantara {min} dan {max}")
    private String nama;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    @NotNull(message = "kode tidak boleh null.")
    @NotEmpty(message = "kode harus diisi.")
    @Size(min = 1, max = 50, message = "panjang nama harus diantara {min} dan {max}")
    private String kode;

    public MasterKorsaDto() {
    }

    public MasterKorsaDto(MasterKorsa model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.nama = model.getNama();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());

        this.kode = model.getKode();
    }
}