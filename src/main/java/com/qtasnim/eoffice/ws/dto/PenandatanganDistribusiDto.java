package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PenandatanganDistribusi;
import lombok.Data;

import java.util.Optional;
import javax.validation.constraints.NotNull;

@Data
public class PenandatanganDistribusiDto {
    private Long id;
    private Integer seq;
    
    private Long idDistribusi;
    
    @NotNull(message = "Penandatangan tidak boleh null")
    private Long idPenandatangan;
    private MasterStrukturOrganisasiDto penandatangan;
    
    private Long idUser;
    private MasterUserDto user;

    public PenandatanganDistribusiDto() {}

    public PenandatanganDistribusiDto(PenandatanganDistribusi model){
        this.id = model.getId();
        this.seq = model.getSeq();
        
        if (model.getDistribusi() != null) {
            this.idDistribusi = model.getDistribusi().getId();
        }
        
        Optional.ofNullable(model.getOrganization()).ifPresent(org -> {
            this.penandatangan = new MasterStrukturOrganisasiDto(org);
            this.idPenandatangan = org.getIdOrganization();
        });

        if(model.getUser() != null){
            this.user = new MasterUserDto(model.getUser());
            this.idUser = model.getUser().getId();
        }
    }
}
