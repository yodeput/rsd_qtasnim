package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.CardChecklist;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class CardChecklistDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    private String description;
    private Long cardId;
    private Boolean isDeleted;
    private Long companyId;
    private CompanyCodeDto company;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private List<CardChecklistDetailDto> checklistDetail;
    private Long checkItemsChecked;

    public CardChecklistDto () {}

    public CardChecklistDto (CardChecklist model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.title = model.getTitle();
        this.description = model.getDescription();
        this.cardId = Optional.ofNullable(model.getCard()).map(a->a.getId()).orElse(null);
        this.isDeleted = model.getIsDeleted();
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        if(model.getCheckListDetail()!=null && !model.getCheckListDetail().isEmpty()){
            List<CardChecklistDetailDto> checkDetail = new ArrayList<>();
            model.getCheckListDetail().stream().filter(q->!q.getIsDeleted()).forEach(a->{
                CardChecklistDetailDto card = new CardChecklistDetailDto(a);
                checkDetail.add(card);
            });
            this.checklistDetail = checkDetail;
            this.checkItemsChecked = checkDetail.stream().filter(w->w.getIsChecked()).count();
        }
    }
}
