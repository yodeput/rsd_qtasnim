package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SuratUndangan;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SuratUndanganDto {
    private String undanganStart;
    private String undanganEnd;
    private String zona;
    private Long id;

    public SuratUndanganDto(SuratUndangan model) {
        if (model != null) {
            DateUtil dateUtil = new DateUtil();
            this.undanganStart = dateUtil.getCurrentISODate(model.getStart());
            this.undanganEnd = dateUtil.getCurrentISODate(model.getEnd());
            this.zona = model.getZona();
            this.id = model.getId();
        }
    }
}
