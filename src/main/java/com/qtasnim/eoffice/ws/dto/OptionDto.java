package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class OptionDto {
    private String name;
    private String value;

    public OptionDto(Class clazz) {
        this.name = clazz.getSimpleName();
        this.value = clazz.getName();
    }
}
