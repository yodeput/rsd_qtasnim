package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import java.util.LinkedList;
import java.util.List;

@Data
public class TemplatingDto {
    @NotNull(message="Id Form Definition tidak boleh null")
    private Long idFormDefinition;
    private List<MetadataValueDto> metadata;
    @DefaultValue("")
    private String docId;
    private List<PenerimaSuratDto> penerimaSurat;
    private List<PenandatanganSuratDto> penandatanganSurat;
    private List<PemeriksaSuratDto> pemeriksaSurat;
    private List<TembusanSuratDto> tembusanSurat;

    public TemplatingDto() {
        metadata = new LinkedList<>();
    }

    public TemplatingDto(Long idFormDefinition, List<MetadataValueDto> metadata, String docId) {
        this.idFormDefinition = idFormDefinition;
        this.metadata = metadata;
        this.docId = docId;
    }
}
