package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ErrorReport;
import com.qtasnim.eoffice.db.HelpDocument;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class HelpDocumentDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @NotNull(message = "tipe tidak boleh null")
    private String tipe;
    @NotNull(message = "nama tidak boleh null")
    private String nama;
    @NotNull(message = "deskripsi tidak boleh null")
    private String deskripsi;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;
    private Long companyId;
    
    private List<GlobalAttachmentDto> files;

    public HelpDocumentDto() {
        files = new ArrayList<>();
    }

    public HelpDocumentDto(HelpDocument model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.tipe = model.getTipe();
        this.nama = model.getNama();
        this.deskripsi = model.getDeskripsi();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
    }
}