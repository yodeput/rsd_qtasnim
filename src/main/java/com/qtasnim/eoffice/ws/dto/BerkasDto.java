package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Arsip;
import com.qtasnim.eoffice.db.Berkas;
import com.qtasnim.eoffice.db.MasterLemariArsip;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class BerkasDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private CompanyCodeDto companyCode;
    @NotNull(message = "idCompany id tidak boleh null.")
    private Long idCompany;

    private MasterKorsaDto unit;
    @NotNull(message = "idUnit tidak boleh null.")
    private String idUnit;

    private MasterKlasifikasiMasalahDto masalah;
    @NotNull(message = "idMasalah tidak boleh null.")
    private Long idMasalah;

    private MasterStatusAkhirArsipDto penyusutanAkhir;
    @NotNull(message = "idPenyusutan tidak boleh null.")
    private Long idPenyusutan;

    private String nomor;
    @NotNull(message = "judul tidak boleh null.")
    private String judul;
    @NotNull(message = "jenis tidak boleh null.")
    private String jenis;
    @NotNull(message = "lokasi tidak boleh null.")
    private String lokasi;
    @NotNull(message = "deskripsi tidak boleh null.")
    private String deskripsi;

    private String status;
    @NotNull(message = "isAktif tidak boleh null.")
    private Boolean isAktif;

    private String jraAktif;
    private String jraInaktif;

    private String startKurun;
    private String endKurun;

    private String rencanaMusnahDate;
    private String closedDate;
    private String terakhirAksesDate;
    private String inaktifDate;
    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private MasterFolderKegiatanDto folderKegiatan;
    @NotNull(message = "idFolder tidak boleh null.")
    private Long idFolder;
    private String folderName;

    private BigDecimal biaya;
    private BigDecimal progress;

    private MasterRuangArsipDto ruangArsip;
    private Long idRuangArsip;

    private MasterLemariArsipDto lemariArsip;
    private Long idLemariArsip;

    private MasterTrapArsipDto trapArsip;
    private Long idTrapArsip;

    private MasterBoksArsipDto boksArsip;
    private Long idBoksArsip;

    private List<ArsipDto> arsipList;
    private List<Long> arsipIds;

    private Boolean isDeleted;

    public BerkasDto() {
        arsipList = new ArrayList<>();
        arsipIds = new ArrayList<>();
    }

    public BerkasDto(Berkas model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);

        this.unit = Optional.ofNullable(model.getUnit()).map(MasterKorsaDto::new).orElse(null);
        this.idUnit = Optional.ofNullable(model.getUnit()).map(q -> q.getId()).orElse(null);

        this.masalah = Optional.ofNullable(model.getKlasifikasiMasalah()).map(MasterKlasifikasiMasalahDto::new).orElse(null);
        this.idMasalah = Optional.ofNullable(model.getKlasifikasiMasalah()).map(q -> q.getIdKlasifikasiMasalah()).orElse(null);

        this.penyusutanAkhir = Optional.ofNullable(model.getStatusAkhirArsip()).map(MasterStatusAkhirArsipDto::new).orElse(null);
        this.idPenyusutan = Optional.ofNullable(model.getStatusAkhirArsip()).map(q -> q.getId()).orElse(null);

        this.nomor = model.getNomor();
        this.judul = model.getJudul();
        this.jenis = model.getJenis();
        this.lokasi = model.getLokasi();
        this.deskripsi = model.getDeskripsi();
        this.status = model.getStatus();
        this.isAktif = model.getIsAktif();

        if (model.getJraAktif() != null) {
            this.jraAktif = dateUtil.getCurrentISODate(model.getJraAktif());
        } else {
            this.jraAktif = null;
        }
        if (model.getJraInaktif() != null) {
            this.jraInaktif = dateUtil.getCurrentISODate(model.getJraInaktif());
        } else {
            this.jraInaktif = null;
        }
        if (model.getStartKurun() != null) {
            this.startKurun = dateUtil.getCurrentISODate(model.getStartKurun());

        } else {
            this.startKurun = null;
        }
        if (model.getEndKurun() != null) {
            this.endKurun = dateUtil.getCurrentISODate(model.getEndKurun());
        } else {
            this.endKurun = null;
        }
        if (model.getInaktifDate() != null) {
            this.inaktifDate = dateUtil.getCurrentISODate(model.getInaktifDate());
        } else {
            this.inaktifDate = null;
        }
        if (model.getRencanaMusnahDate() != null) {
            this.rencanaMusnahDate = dateUtil.getCurrentISODate(model.getRencanaMusnahDate());
        } else {
            this.rencanaMusnahDate = null;
        }
        if (model.getClosedDate() != null) {
            this.closedDate = dateUtil.getCurrentISODate(model.getClosedDate());
        } else {
            this.closedDate = null;
        }

        if (model.getTerakhirAksesDate() != null) {
            this.terakhirAksesDate = dateUtil.getCurrentISODate(model.getTerakhirAksesDate());
        } else {
            this.terakhirAksesDate = null;
        }

        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();

        //hide, for performance
//        if (model.getArsipList() != null) {
//            this.arsipList = model.getArsipList().stream().map(q -> new ArsipDto(q)).collect(Collectors.toList());
//            this.arsipIds = model.getArsipList().stream().map(q -> q.getId()).collect(Collectors.toList());
//        }

        this.folderKegiatan = Optional.ofNullable(model.getFolderKegiatan()).map(MasterFolderKegiatanDto::new).orElse(null);
        this.idFolder = Optional.ofNullable(model.getFolderKegiatan()).map(q -> q.getId()).orElse(null);
        this.folderName = model.getFolderName();
        this.progress = model.getProgress();
        this.biaya = model.getBiaya();


        if (model.getRuangArsip()!= null) {
            this.idRuangArsip = Optional.ofNullable(model.getRuangArsip()).map(q -> q.getId()).orElse(null);
            this.ruangArsip= Optional.ofNullable(model.getRuangArsip()).map(MasterRuangArsipDto::new).orElse(null);
        }
        if (model.getLemariArsip()!= null) {
            this.idLemariArsip = Optional.ofNullable(model.getLemariArsip()).map(q -> q.getId()).orElse(null);
            this.lemariArsip= Optional.ofNullable(model.getLemariArsip()).map(MasterLemariArsipDto::new).orElse(null);
        }
        if (model.getTrapArsip()!= null) {
            this.idTrapArsip = Optional.ofNullable(model.getTrapArsip()).map(q -> q.getId()).orElse(null);
            this.trapArsip= Optional.ofNullable(model.getTrapArsip()).map(MasterTrapArsipDto::new).orElse(null);
        }
        if (model.getBoksArsip()!= null) {
            this.idBoksArsip = Optional.ofNullable(model.getBoksArsip()).map(q -> q.getId()).orElse(null);
            this.boksArsip= Optional.ofNullable(model.getBoksArsip()).map(MasterBoksArsipDto::new).orElse(null);
        }

        this.isDeleted = model.getIsDeleted();
    }
}
