package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterKontakHelpdesk;
import com.qtasnim.eoffice.db.MasterTentangKami;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;

@Data
public class MasterKontakHelpdeskDto implements Serializable {
    private Long id;
    private String toka;
    private String email;
    private CompanyCodeDto company;
    private Long companyId;
    private String start;
    private String end;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public MasterKontakHelpdeskDto(){}

    public MasterKontakHelpdeskDto(MasterKontakHelpdesk model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.toka = model.getToka();
        this.email = model.getEmail();
        if(model.getCompanyCode()!=null) {
            this.company = new CompanyCodeDto(model.getCompanyCode());
            this.companyId = model.getCompanyCode().getId();
        }
        this.start = dateUtil.getCurrentISODate(model.getStart());
        this.end = dateUtil.getCurrentISODate(model.getEnd());
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
    }
}
