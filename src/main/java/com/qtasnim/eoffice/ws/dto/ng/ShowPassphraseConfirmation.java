package com.qtasnim.eoffice.ws.dto.ng;

import com.qtasnim.eoffice.ws.dto.WebSocketMessage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShowPassphraseConfirmation extends WebSocketMessage {
    private String actionType; //submit | actionSubmit

    public ShowPassphraseConfirmation(String payload, String actionType) {
        setType("[Dokumen] Show Passphrase Confirmation");
        setPayload(payload);
        setActionType(actionType);
    }
}
