package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ReferensiSurat;
import com.qtasnim.eoffice.db.ReferensiSuratDetail;
import lombok.Data;

import java.util.Optional;

@Data
public class ReferensiSuratDetailDto {
    private Long id;
    private SuratDto surat;
    private Long suratId;
    private ReferensiSuratDto referensiSurat;
    private Long refId;


    public ReferensiSuratDetailDto(){}

    public ReferensiSuratDetailDto(ReferensiSuratDetail model){
        this.id = model.getId();
        this.surat = Optional.ofNullable(model.getSurat()).map(SuratDto::new).orElse(null);
        this.suratId = Optional.ofNullable(model.getSurat()).map(a->a.getId()).orElse(null);
        this.referensiSurat = Optional.ofNullable(model.getReferensiSurat()).map(ReferensiSuratDto::new).orElse(null);
        this.refId = Optional.ofNullable(model.getReferensiSurat()).map(a->a.getId()).orElse(null);

    }
}
