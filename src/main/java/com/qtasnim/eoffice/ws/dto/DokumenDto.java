package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Data
public class DokumenDto  implements Serializable {
    private Long id;
    private String tanggal;
    private String perihal;
    private String jenisDokumen;
    private boolean isKorespondensi;
    private List<PengirimDto> pengirim;
    private String Aksi;

    public DokumenDto(){}
}
