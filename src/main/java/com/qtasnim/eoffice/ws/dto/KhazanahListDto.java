package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterKlasifikasiKhazanah;
import lombok.Data;

import java.util.List;

@Data
public class KhazanahListDto {
    private Long idKlasifikasi;
    private String namaKlasifikasi;
    private String namaParent;

    private List<KhazanahDto> children;
    private Long count;

    public KhazanahListDto() {}

    public KhazanahListDto(MasterKlasifikasiKhazanah master) {
        this.idKlasifikasi = master.getId();
        this.namaKlasifikasi = master.getNama();
        this.namaParent = master.getParent().getNama();
    }
}
