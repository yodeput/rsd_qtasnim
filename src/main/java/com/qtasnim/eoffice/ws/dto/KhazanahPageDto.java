package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class KhazanahPageDto {
    private Long idKlasifikasi;
    private Integer skip;
    private Integer limit;

    public KhazanahPageDto() {
        this.skip = 0;
        this.limit = 10;
    }
}
