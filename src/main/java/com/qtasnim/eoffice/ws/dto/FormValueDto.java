package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.FormDefinition;
import com.qtasnim.eoffice.db.FormValue;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Data
public class FormValueDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private FormFieldDto form;

    @NotNull(message = "formId tidak boleh null")
    private Long formId;

    private String value;
    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;
    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

    private String endDate;

    public FormValueDto(){}

    public FormValueDto(FormValue model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.form = Optional.ofNullable(model.getFormField()).map(FormFieldDto::new).orElse(null);
        this.formId = Optional.ofNullable(model.getFormField()).map(q->q.getId()).orElse(null);
        this.value = model.getValue();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }

}
