package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ReferensiSurat;
import lombok.Data;

import java.util.Optional;

@Data
public class ReferensiSuratFileDto {
    private String docId;
    private String namaFile;
    private Long ukuran;
    private String pengunggah;
    private String noSurat;
    private String tanggal;
    private String perihal;
    private String jenisDokumen;

    public ReferensiSuratFileDto(){}
}
