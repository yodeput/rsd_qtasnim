package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DokumenRiwayatDto implements Serializable {
    private Long id;
    private String noSurat;

    private List<DokumenRiwayatItemDto> konseptor;
    private List<DokumenRiwayatItemDto> pemeriksa;
    private List<DokumenRiwayatItemDto> penandatangan;

    private List<DokumenRiwayatItemDto> penerima;
    private List<DokumenRiwayatItemDto> tembusan;
    private List<DokumenRiwayatItemDto> disposisi;

    public DokumenRiwayatDto(){}
}
