package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterBahasa;
import com.qtasnim.eoffice.db.constraints.*;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterBahasa", fieldNames = {"nama"})
public class MasterBahasaDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "nama tidak boleh null")
    @Unique(fieldName = "nama", entityName = "MasterBahasa", message = "Nama sudah ada.")
    private String nama;

    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    @CompanyId
    private Long companyId;

    public MasterBahasaDto(MasterBahasa model) {
        DateUtil dateUtil = new DateUtil();

        //this.isActive = model.getIsActive();
        this.id = model.getId();
        this.nama = model.getNama();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }

    public MasterBahasaDto() {
    }

}