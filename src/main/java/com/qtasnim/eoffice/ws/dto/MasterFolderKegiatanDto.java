package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterFolderKegiatan;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
public class MasterFolderKegiatanDto implements Serializable {

    private Long id;

    private String folderName;

    private Long idParent;
    private MasterFolderKegiatanDto parent;

    private Boolean isPersonalFolder;
    private Boolean isAllowAdd;

    @NotNull(message = "companyId tidak boleh null")
    private Long idCompany;

    private CompanyCodeDto companyCode;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    private String start;
    private String end;

    public MasterFolderKegiatanDto(){}

    public MasterFolderKegiatanDto(MasterFolderKegiatan model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.folderName = model.getFolderName();
        this.idParent = Optional.ofNullable(model.getParent()).map(a->a.getId()).orElse(null);
        this.parent = Optional.ofNullable(model.getParent()).map(MasterFolderKegiatanDto::new).orElse(null);
//        this.isPersonalFolder = model.getIsPersonalFolder();
        this.isAllowAdd = model.getIsAllowAdd();
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.start = dateUtil.getCurrentISODate(model.getStart());
        this.end = dateUtil.getCurrentISODate(model.getEnd());
    }
}
