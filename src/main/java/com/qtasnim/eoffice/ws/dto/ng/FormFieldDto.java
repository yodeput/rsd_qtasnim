package com.qtasnim.eoffice.ws.dto.ng;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qtasnim.eoffice.db.FormField;
import com.qtasnim.eoffice.ws.dto.TaskActionDto;
import lombok.Data;

import java.util.function.Predicate;

@Data
public class FormFieldDto {
    private Long id;
    private Integer seq;
    private String label;
    private String name;
    private String inputType;
    private String defaultValue;
    private String type;
    private String otherData;
    private String value;
    private Boolean required = false;
    private Boolean disabled = false;
    private Boolean postToCamunda = true;
    private String requiredEl = null;
    @JsonIgnore
    private Predicate<TaskActionDto> requiredFn = taskActionDto -> required;
    
    public FormFieldDto() {}
    
    public FormFieldDto(FormField formField) {
        setId(formField.getId());
        setLabel(formField.getMetadata().getLabel());
        setName(formField.getMetadata().getNama());
        setDefaultValue(formField.getDefaultValue());
        setRequired(formField.getIsRequired());
        setDisabled(formField.getIsDisabled());
        setSeq(formField.getSeq());

        switch (formField.getType()) {
            case "password": {
                setType("input");
                setInputType("password");
                break;
            }
            case "text": {
                setType("input");
                setInputType("text");
                break;
            }
            case "email": {
                setType("input");
                setInputType("email");
                break;
            }
            default: {
                setType(formField.getType());
                setOtherData(formField.getData());
            }
        }
    }
}
