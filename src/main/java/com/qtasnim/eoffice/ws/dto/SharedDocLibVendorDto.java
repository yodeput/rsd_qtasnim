package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SharedDocLibVendor;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class SharedDocLibVendorDto {
    private MasterVendorDto vendor;
    private SuratDto surat;
    private String readDate;
    private String createdDate;
    private String createdBy;

    public SharedDocLibVendorDto(SharedDocLibVendor sharedDocLibVendor) {
        DateUtil dateUtil = new DateUtil();
        surat = Optional.ofNullable(new SuratDto(sharedDocLibVendor.getSurat())).orElse(null);
        vendor = new MasterVendorDto(sharedDocLibVendor.getVendor());
        readDate = dateUtil.getCurrentISODate(sharedDocLibVendor.getReadDate());
        createdDate = dateUtil.getCurrentISODate(sharedDocLibVendor.getCreatedDate());
        createdBy = sharedDocLibVendor.getCreatedBy();
    }
}
