package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterKategoriCard;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Data
public class KategoriCardDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String color;
    private Boolean isDeleted;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private Long idCompany;
    private CompanyCodeDto company;

    public KategoriCardDto(){}

    public KategoriCardDto(MasterKategoriCard model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.name = model.getName();
        this.color = model.getColor();
        this.isDeleted = model.getIsDeleted();
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
    }
}
