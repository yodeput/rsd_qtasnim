package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.UserTask;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Optional;

@Getter
@Setter
public class UserTaskDto {
    Long id;
    String taskId;
    String taskName;
    String executionStatus;

    String relatedEntity;
    String recordRefId;
    String nomorDokumen;
    String perihal;
    String jenisDokumen;
    String klasifikasiKeamanan;
    String status;
    String note;

    String createdTaskDate;
    String approvedDate;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String modifiedBy;

    MasterStrukturOrganisasiDto konseptor;
    MasterStrukturOrganisasiDto pengirim;
    MasterStrukturOrganisasiDto assignee;

    Boolean isParalelAssignee;

    public UserTaskDto() { }

    public UserTaskDto(UserTask task) {
        if (task != null) {
            DateUtil dateUtil = new DateUtil();

            this.id = task.getId();
            taskId = task.getTaskId();
            taskName = task.getTaskName();
            executionStatus = task.getExecutionStatus().toString();
            relatedEntity = task.getRelatedEntity();
            recordRefId = task.getRecordRefId();
            nomorDokumen = task.getNomorDokumen();
            perihal = task.getPerihal();
            jenisDokumen = task.getJenisDokumen();
            klasifikasiKeamanan = task.getKlasifikasiKeamanan();
            status = task.getStatus();
            note = task.getNote();

            createdTaskDate = dateUtil.getCurrentISODate(task.getCreatedTaskDate());
            if (approvedDate != null) approvedDate = dateUtil.getCurrentISODate(task.getApprovedDate());
            createdDate = dateUtil.getCurrentISODate(task.getCreatedDate());
            if (modifiedDate != null) modifiedDate = dateUtil.getCurrentISODate(task.getModifiedDate());

            createdBy = task.getCreatedBy();
            modifiedBy = task.getModifiedBy();
            isParalelAssignee = task.getIsParalelAssignee();

            konseptor = Optional.ofNullable(task.getKonseptor()).map(MasterStrukturOrganisasiDto::new).orElse(null);
            pengirim = Optional.ofNullable(task.getPengirim()).map(MasterStrukturOrganisasiDto::new).orElse(konseptor);
            assignee = Optional.ofNullable(task.getAssignee()).map(MasterStrukturOrganisasiDto::new).orElse(null);

        }
    }

}
