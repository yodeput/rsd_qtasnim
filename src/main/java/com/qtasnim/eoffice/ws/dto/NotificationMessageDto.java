package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class NotificationMessageDto extends WebSocketMessage {
    public NotificationMessageDto() {
        setType("Notification");
    }
}
