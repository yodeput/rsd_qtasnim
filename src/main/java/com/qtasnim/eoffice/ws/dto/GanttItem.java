package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GanttItem {
    private Long id;
    private String name;
    private MasterUser resource;
    private GanttItem depend;
    private GanttItem parent;
    private String caption;
    private String notes;
}
