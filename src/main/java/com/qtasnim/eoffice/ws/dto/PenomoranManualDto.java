package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class PenomoranManualDto implements IWorkflowDto {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String noDokumen;
    private String noPermohonan;
    private String status;
    private String lampiran;
    private String perihal;
    private String keterangan;
    private String tempatDiterima;
    private String tglDokumen;
    private String tglDiterima;

    @NotNull(message = "Status hapus tidak boleh null")
    private Boolean isDeleted;

    private MasterJenisDokumenDto jenisDokumen;
    @NotNull(message = "idJenisDokumen tidak boleh null.")
    private Long idJenisDokumen;

    private MasterKlasifikasiMasalahDto klasifikasiDokumen;
    @NotNull(message = "idKlasifikasiDokumen tidak boleh null.")
    private Long idKlasifikasiDokumen;

    private MasterKlasifikasiKeamananDto klasifikasiKeamanan;
    @NotNull(message = "idKlasifikasiKeamanan tidak boleh null.")
    private Long idKlasifikasiKeamanan;

    private MasterStrukturOrganisasiDto konseptor;
    @NotNull(message = "idKonseptor tidak boleh null.")
    private Long idKonseptor;

    private CompanyCodeDto companyCode;
    @NotNull(message = "Company id tidak boleh null.")
    private Long idCompany;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    private List<PemeriksaNomorManualDto> pemeriksa;
    private List<PenandatanganNomorManualDto> penandatangan;
    private List<PenerimaNomorManualDto> penerima;

    private String workflowAction;
    private String workflowFormName;
    private List<String> workflowCurrentTasks;
    private String activeTask;
    private Object workflowResponse;
    private String workflowNote;

    public PenomoranManualDto(){
        pemeriksa = new ArrayList<>();
        penandatangan = new ArrayList<>();
    }

    public PenomoranManualDto(PenomoranManual model) {
        this(model, true);
    }

    public PenomoranManualDto(PenomoranManual model, boolean includeRelation) {
        this();

        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();

        this.noDokumen = Optional.ofNullable(model.getNoDokumen()).orElse(null);
        this.noPermohonan = Optional.ofNullable(model.getNoPermohonan()).orElse(null);
        this.lampiran = Optional.ofNullable(model.getLampiran()).orElse(null);
        this.perihal = Optional.ofNullable(model.getPerihal()).orElse(null);
        this.keterangan = Optional.ofNullable(model.getKeterangan()).orElse(null);
        this.tempatDiterima = Optional.ofNullable(model.getTempatDiterima()).orElse(null);
        this.status = model.getStatus();

        if(model.getTglDokumen()!=null) {
            this.tglDokumen = Optional.ofNullable(dateUtil.getCurrentISODate(model.getTglDokumen())).orElse(null);
        }
        if(model.getTglDiterima()!=null) {
            this.tglDiterima = Optional.ofNullable(dateUtil.getCurrentISODate(model.getTglDiterima())).orElse(null);
        }

        this.isDeleted = model.getIsDeleted();

        this.konseptor = Optional.ofNullable(model.getKonseptor()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idKonseptor = Optional.ofNullable(model.getKonseptor()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null);

        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany =  Optional.ofNullable(model.getCompanyCode()).map(CompanyCode::getId).orElse(null);

        Optional.ofNullable(model.getJenisDok()).ifPresent(data -> {
            this.jenisDokumen = new MasterJenisDokumenDto(data);
            this.idJenisDokumen = data.getIdJenisDokumen();
        });

        Optional.ofNullable(model.getKlasDokumen()).ifPresent(data -> {
            this.klasifikasiDokumen = new MasterKlasifikasiMasalahDto(data);
            this.idJenisDokumen = data.getIdKlasifikasiMasalah();
        });

        Optional.ofNullable(model.getKlasKeamanan()).ifPresent(data -> {
            this.klasifikasiKeamanan = new MasterKlasifikasiKeamananDto(data);
            this.idKlasifikasiKeamanan = data.getIdKlasifikasiKeamanan();
        });

        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = Optional.ofNullable(dateUtil.getCurrentISODate(model.getModifiedDate())).orElse(null);

        if(includeRelation) {
            bindPenandatangan(model);
            bindPemeriksa(model);
            bindPenerima(model);
        }


    }

    public static PenomoranManualDto minimal(PenomoranManual model) {
        return new PenomoranManualDto(model, false);
    }

    public void bindPenandatangan(PenomoranManual model) {
        this.penandatangan = model.getPenandatangan().stream().map(PenandatanganNomorManualDto::new).collect(Collectors.toList());
    }

    public void bindPemeriksa(PenomoranManual model) {
        this.pemeriksa = model.getPemeriksa().stream().map(PemeriksaNomorManualDto::new).collect(Collectors.toList());
    }

    public void bindPenerima(PenomoranManual model) {
        this.penerima = model.getPenerima().stream().map(PenerimaNomorManualDto::new).collect(Collectors.toList());
    }

}
