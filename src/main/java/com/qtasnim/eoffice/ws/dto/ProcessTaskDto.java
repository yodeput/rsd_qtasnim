package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ProcessTask;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.util.List;
import java.util.Optional;

@Data
public class ProcessTaskDto {
    private Long id;
    private String taskType;
    private String response;
    private MasterUserDto assignee;
    private MasterStrukturOrganisasiDto orgAssignee;
    private String executionStatus;
    private String assignedDate;
    private String executedDate;
    private String note;
    private String taskName;
    private String jabatanDelegasi;
    private Boolean isHadir;
    private List<String> tindakkanDisposisi;

    public ProcessTaskDto(ProcessTask processTask) {
        if (processTask != null) {
            DateUtil dateUtil = new DateUtil();

            this.id = processTask.getId();
            this.taskType = processTask.getTaskType();
            this.response = processTask.getResponse();
            if(processTask.getAssignee().getUser()!=null) {
                this.assignee = new MasterUserDto(processTask.getAssignee().getUser());
            }else{
                this.orgAssignee = new MasterStrukturOrganisasiDto(processTask.getAssignee());
            }
            this.executionStatus = processTask.getExecutionStatus().toString();
            this.assignedDate = dateUtil.getCurrentISODate(processTask.getAssignedDate());
            this.executedDate = Optional.ofNullable(processTask.getExecutedDate()).map(dateUtil::getCurrentISODate).orElse(null);
            this.note = processTask.getNote();
            this.taskName = processTask.getTaskName();
            this.isHadir = processTask.getIsHadir();
        }
    }
}
