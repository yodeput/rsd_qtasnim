package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterNegara;
import com.qtasnim.eoffice.db.constraints.CompanyId;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.EnableUniqueValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.db.constraints.ISODateAfter;
import com.qtasnim.eoffice.db.constraints.Unique;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterNegara", fieldNames = {"nama"})
public class MasterNegaraDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Size(min = 1, max = 255, message = "panjang nama harus diantara {min} dan {max}")
    @Unique(fieldName = "nama", entityName = "MasterNegara", message = "Nama sudah ada.")
    private String nama;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;
    @CompanyId
    private Long companyId;
    private CompanyCodeDto companyCode;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    public MasterNegaraDto(){}

    public MasterNegaraDto(MasterNegara model) {
        this.id = model.getId();
        this.nama = model.getNama();

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());

        if (model.getCompanyCode() != null) {
            this.companyCode = new CompanyCodeDto(model.getCompanyCode());
            this.companyId = model.getCompanyCode().getId();
        }
    }
}
