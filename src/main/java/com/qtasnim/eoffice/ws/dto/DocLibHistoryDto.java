package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.DocLibHistory;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class DocLibHistoryDto {
    private Long id;
    private String version;
    private Long size;
    private String docId;
    private MasterUserDto user;
    private String date;

    public DocLibHistoryDto() {

    }

    public DocLibHistoryDto(DocLibHistory docLibHistory) {
        if (docLibHistory != null) {
            DateUtil dateUtil = new DateUtil();

            id = docLibHistory.getId();
            version = docLibHistory.getVersion();
            size = docLibHistory.getSize();
            docId = docLibHistory.getDocId();
            user = new MasterUserDto(docLibHistory.getUser());
            date = dateUtil.getCurrentISODate(docLibHistory.getCreatedDate());
        }
    }
}
