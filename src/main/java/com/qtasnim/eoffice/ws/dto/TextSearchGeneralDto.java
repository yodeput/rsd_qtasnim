package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class TextSearchGeneralDto {
    private String docName;

    private String nomorArsip;
    private String jenisDokumen;

    private String nomorBerkas;
    private String judulBerkas;
    private String folderName;

    private String noDokumen;
}
