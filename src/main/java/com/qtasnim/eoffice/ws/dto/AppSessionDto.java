package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.AppSession;
import com.qtasnim.eoffice.db.ImageRepository;
import com.qtasnim.eoffice.db.MasterRole;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.util.Optional;

@Data
public class AppSessionDto {
    private Long id;
    private String appName;
    private String token;
    private String startDate;
    private String endDate;

    public AppSessionDto(AppSession ses) {
        DateUtil dateUtil = new DateUtil();
        this.id = ses.getId();
        this.appName = ses.getAppName();
        this.token = ses.getToken();
        this.startDate = dateUtil.getCurrentISODate(ses.getStartDate());
        this.endDate = dateUtil.getCurrentISODate(ses.getEndDate());
    }
    public AppSessionDto(){}
}
