package com.qtasnim.eoffice.ws.dto;

public interface Paginated<T> {
    T currentPage();
    int currentPageIndex();
    int pageCount();
    int totalCount();
}