package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;
import org.apache.commons.text.WordUtils;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Data
public class MasterUserDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String username;
    private String passHash;
    private String passSalt;
    private String employeeId;
    private String nameFront;
    private String nameMiddleLast;
    private String nameFull;
    private String salutation;
    private String email;
    private String otherInformation;
    private String description;
    private String status;
    private Boolean isDeleted;
    private Boolean isActive;
    private Boolean isInternal;
    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;
    private String startDate;
    private String endDate;
    private String image;
    private List<RoleDto> roles;
    private Long organization;
    private Long parentOrganization;
    private MasterVendorDto vendor;
    private Long vendorId;
    private String businessArea;
    private String jabatan;
    private String kedudukan;
//    private byte[] photo;
    private String keyPattern;
    private String photo;
    private String fingerprint;

    private String gradeId;
    private MasterGradeDto grade;
    private String korsaId;
    private MasterKorsaDto korsa;
    private String areaId;
    private MasterAreaDto area;

    private String agama;
    private String kelamin;
    private String tempatLahir;
    private String tanggalLahir;

    private List<JabatanDto> jabatanList;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    private String mobilePhone;
    
    private String organizationName;
    private String singkatanOrg;
    private MasterStrukturOrganisasiDto pejabatSekretaris;
    private List<RoleModuleDto> roleModules;

    public MasterUserDto(){
    }

    public MasterUserDto(MasterUser model) {
        this(model, new ArrayList<>());
    }

    public MasterUserDto(MasterUser model, Boolean loadRole) {
        this(model, new ArrayList<>(), loadRole);
    }

    public MasterUserDto(MasterUser model, List<JabatanDto> jabatanList) {
        this(model, jabatanList, true);
    }

    public MasterUserDto(MasterUser model, List<JabatanDto> jabatanList, Boolean loadRole) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.username = model.getLoginUserName();
        this.passHash = model.getPasswordHash();
        this.passSalt = model.getPasswordSalt();
        this.employeeId = model.getEmployeeId();
        this.nameFront = model.getNameFront();
        this.nameMiddleLast = model.getNameMiddleLast();
        this.nameFull = WordUtils.capitalizeFully(String.format("%s %s", Optional.ofNullable(this.nameFront).orElse(""), Optional.ofNullable(this.nameMiddleLast).orElse("")).trim());
        this.salutation = model.getSalutation();
        this.email = model.getEmail();
        this.otherInformation = model.getOtherContactInformation();
        this.description = model.getDescription();
        this.status = model.getStatus();
        this.isDeleted = model.getIsDeleted();
        this.isActive = model.getIsActive();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.image = Optional.ofNullable(model.getImage()).map(ImageRepository::getGeneratedFileName).orElse(null);
        //this.roles = model.getRoles().stream().map(q -> new RoleDto(q)).collect(Collectors.toList());
        //this.organization = Optional.ofNullable(model.getOrganizationEntity()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        if(model.getOrganizationEntity()!=null) {
            this.organization = model.getOrganizationEntity().getIdOrganization();
            if(model.getOrganizationEntity().getParent()!=null){
                this.parentOrganization = model.getOrganizationEntity().getParent().getIdOrganization();
            }
        }
        this.isInternal = model.getIsInternal();
        this.vendor = Optional.ofNullable(model.getVendor()).map(MasterVendorDto::new).orElse(null);
        this.vendorId = Optional.ofNullable(model.getVendor()).map(q->q.getId()).orElse(null);
        this.businessArea = model.getBusinessArea();
        this.jabatan = model.getJabatan();
        this.kedudukan = model.getKedudukan();
//        this.photo = model.getPhoto();
        if(model.getPhoto()!=null) {
            this.photo = Base64.getEncoder().encodeToString(model.getPhoto());
        }
        this.keyPattern = model.getKeyPattern();
        this.fingerprint = model.getFingerprint();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        this.area = Optional.ofNullable(model.getArea()).map(MasterAreaDto::new).orElse(null);
        this.areaId = Optional.ofNullable(model.getArea()).map(q->q.getId()).orElse(null);

        this.korsa = Optional.ofNullable(model.getKorsa()).map(MasterKorsaDto::new).orElse(null);
        this.korsaId = Optional.ofNullable(model.getKorsa()).map(q->q.getId()).orElse(null);

        this.grade = Optional.ofNullable(model.getGrade()).map(MasterGradeDto::new).orElse(null);
        this.gradeId = Optional.ofNullable(model.getGrade()).map(q->q.getId()).orElse(null);

        this.agama = model.getAgama();
        this.kelamin = model.getKelamin();
        this.tempatLahir = model.getTempatLahir();
        if(model.getTanggalLahir()!=null){
            this.tanggalLahir = dateUtil.getCurrentISODate(model.getTanggalLahir());
        } else {
            this.tanggalLahir = null;
        }

        this.mobilePhone = model.getMobilePhone();
        this.jabatanList = jabatanList;
        this.organizationName = Optional.ofNullable(model.getOrganizationEntity()).map(MasterStrukturOrganisasi::getOrganizationName).orElse(null);
        this.singkatanOrg = Optional.ofNullable(model.getOrganizationEntity()).map(MasterStrukturOrganisasi::getSingkatan).orElse(null);

        if(loadRole && model.getOrganizationEntity()!=null){
            List<MasterRole> roless = model.getOrganizationEntity().getRoleList();
            List<RoleDto> roleList = new ArrayList<>();
            if(!roless.isEmpty()) {
                roless.forEach(r -> {
                    RoleDto roleDto = new RoleDto(r,false);
                    roleList.add(roleDto);
                });
                this.roles = roleList.stream().sorted(Comparator.comparing(RoleDto::getLevel).reversed()).collect(Collectors.toList());
            }else{
                this.roles = new ArrayList<>();
            }
        }else{
            this.roles = new ArrayList<>();
        }
    }
}