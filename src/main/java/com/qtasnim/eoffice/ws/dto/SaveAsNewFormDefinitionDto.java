package com.qtasnim.eoffice.ws.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class SaveAsNewFormDefinitionDto implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @NotNull(message = "Sumber Jenis Dokumen tidak boleh kosong.")
    private Long sourceDocumentId;

    @NotNull(message = "Tujuan Jenis Dokumen Baru tidak boleh kosong.")
    private Long newDocumentId;

    public SaveAsNewFormDefinitionDto() {}
}
