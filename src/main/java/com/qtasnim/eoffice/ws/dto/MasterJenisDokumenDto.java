package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterJenisDokumen;
import com.qtasnim.eoffice.db.constraints.*;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterJenisDokumen", fieldNames = {"kodeJenisDokumen", "namaJenisDokumen"})
public class MasterJenisDokumenDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long idJenisDokumen;

    @NotNull(message = "kode jenis dokumen tidak boleh null.")
    @NotEmpty(message = "kode jenis dokumen harus diisi.")
    @Size(min = 1, max = 50, message = "panjang kode jenis dokumen harus diantara {min} dan {max}")
    @Unique(fieldName = "kodeJenisDokumen", entityName = "MasterJenisDokumen", message = "Kode sudah ada.")
    private String kodeJenisDokumen;
    
    @NotNull(message = "nama jenis dokumen tidak boleh null.")
    @NotEmpty(message = "nama jenis dokumen harus diisi.")
    @Size(min = 1, max = 50, message = "panjang nama jenis dokumen harus diantara {min} dan {max}")
    @Unique(fieldName = "namaJenisDokumen", entityName = "MasterJenisDokumen", message = "Nama sudah ada.")
    private String namaJenisDokumen;


    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    @CompanyId
    private Long companyId;

    private Boolean isKorespondensi;

    private Long idGroup;

    private JenisDokumenDocLibDto jenisDokumenDocLibDto;
    private String baseCode;
    private MasterJenisDokumenDto parent;
    private Long parentId;
    private Boolean isHidden = false;
    private Boolean isDeletable = true;
    private Boolean isMultiPemeriksa = true;
    private Boolean isPemeriksaMandatory = false;
    private Boolean isRegulasi = false;
    private Boolean isIncludePejabatMengetahui = false;
    private Integer jumlahPenandatangan = 1;

    public MasterJenisDokumenDto(){}

    public MasterJenisDokumenDto(MasterJenisDokumen model) {
        this.idJenisDokumen = model.getIdJenisDokumen();
        this.kodeJenisDokumen = model.getKodeJenisDokumen();
        this.namaJenisDokumen = model.getNamaJenisDokumen();
        this.isActive = model.getIsActive();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.isKorespondensi = model.getIsKorespondensi();
        this.idGroup = Optional.ofNullable(model.getGroup()).map(MasterJenisDokumen::getIdJenisDokumen).orElse(null);
        this.parent = Optional.ofNullable(model.getParent()).map(MasterJenisDokumenDto::new).orElse(null);
        this.parentId = Optional.ofNullable(model.getParent()).map(MasterJenisDokumen::getIdJenisDokumen).orElse(null);
        this.baseCode = model.getBaseCode();
        this.isHidden = model.getIsHidden();
        this.isDeletable = model.getIsDeletable();
        this.isMultiPemeriksa = model.getIsMultiPemeriksa();
        this.isPemeriksaMandatory = model.getIsPemeriksaMandatory();
        this.isIncludePejabatMengetahui = model.getIsIncludePejabatMengetahui();
        this.isRegulasi = model.getIsRegulasi();
        this.jumlahPenandatangan = model.getJumlahPenandatangan();
    }
}
