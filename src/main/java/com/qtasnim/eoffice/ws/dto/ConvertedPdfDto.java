package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ProcessTask;
import com.qtasnim.eoffice.services.RiwayatService;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class ConvertedPdfDto {
    String tipe;
    String noIdDoc;
    List<DetailConvertedPdfDto> detail;

    public ConvertedPdfDto () {
        detail = new ArrayList<>();
    }

    public ConvertedPdfDto (String tipe, List<DetailConvertedPdfDto> detail) {
        this.tipe = tipe;
        this.detail = detail;
    }

    public ConvertedPdfDto (String tipe, String noIdDoc, List<DetailConvertedPdfDto> detail) {
        this.tipe = tipe;
        this.noIdDoc = noIdDoc;
        this.detail = detail;
    }
}
