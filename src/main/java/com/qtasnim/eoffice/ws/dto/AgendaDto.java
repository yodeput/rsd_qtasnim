package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Agenda;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.db.constraints.ISODateAfter;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class AgendaDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull(message = "Perihal tidak boleh null")
    @NotEmpty(message = "Perihal tidak boleh kosong")
    private String perihal;

    private String description;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String start;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String end;

    private String location;

    @NotNull(message = "Id company tidak boleh null")
    private Long idCompany;

    private CompanyCodeDto companyCode;
    private Long idOrganization;
    private MasterStrukturOrganisasiDto organization;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public  AgendaDto(){}

    public AgendaDto(Agenda model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.perihal = model.getPerihal();
        this.description = model.getDescription();
        this.start = dateUtil.getCurrentISODate(model.getStart());
        this.end = dateUtil.getCurrentISODate(model.getEnd());
        this.location = model.getLocation();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.organization = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idOrganization = Optional.ofNullable(model.getOrganization()).map(q->q.getIdOrganization()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
    }
}
