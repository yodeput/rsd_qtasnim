package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@EnableISODateAfterValidator
public class SuratForDistribusiDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String noDokumen;
    private String perihal;
    private String approvedDate;

    private CompanyCodeDto companyCode;
    private MasterKlasifikasiKeamananDto keamanan;
    private MasterTingkatUrgensiDto tingkatUrgensi;
    private MasterTingkatPerkembanganDto tingkatPerkembangan;


    private List<PenerimaSuratDto> penerimaSurat;

    public SuratForDistribusiDto() {
        penerimaSurat = new ArrayList<>();
    }

    public SuratForDistribusiDto(Surat model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.noDokumen = model.getNoDokumen();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        if (model.getApprovedDate() != null) {
            this.approvedDate = Optional.ofNullable(dateUtil.getCurrentISODate(model.getApprovedDate())).orElse(null);
        }
        if (model.getPenerimaSurat() != null){
            this.penerimaSurat = model.getPenerimaSurat().stream().map(PenerimaSuratDto::new).collect(Collectors.toList());
        }
    }
}
