package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PersonalArsip;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
public class PersonalArsipDto implements Serializable {

    private Long id;
    private Long idOrganisasi;
    private MasterStrukturOrganisasiDto organisasi;
    private Long idUser;
    private MasterUserDto user;
    private Long idFolder;
    private MasterFolderKegiatanDetailDto folder;
    private String docId;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public PersonalArsipDto(){}

    public PersonalArsipDto(PersonalArsip model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.idFolder = Optional.ofNullable(model.getFolder()).map(a->a.getId()).orElse(null);
        this.folder = Optional.ofNullable(model.getFolder()).map(MasterFolderKegiatanDetailDto::new).orElse(null);
        this.docId = model.getDocId();
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
    }
}
