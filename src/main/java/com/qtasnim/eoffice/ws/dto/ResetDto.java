package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResetDto implements Serializable {
    private String email;

    public  ResetDto(){}

    public ResetDto(String email){
        this.email= email;
    }
}
