package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class ResetPasswordDto {
    private String newPassword;

    public ResetPasswordDto(){}

    public ResetPasswordDto(String newPassword){
        this.newPassword = newPassword;
    }
}
