package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PenerimaDistribusi;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Data
public class PenerimaDistribusiDto {

    private Long id;
    private Integer seq;

    private Boolean isDownload;
    private Boolean isRead;

    private String downloadDate;
    private String readDate;

    private Long idDistribusi;

    @NotNull(message = "Penerima tidak boleh null")
    private Long idPenerima;
    private MasterStrukturOrganisasiDto organization;

    private Long idUser;
    private MasterUserDto user;

    public PenerimaDistribusiDto() { }

    public PenerimaDistribusiDto(PenerimaDistribusi model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.seq = model.getSeq();

        if (model.getDistribusi() != null) {
            this.idDistribusi = model.getDistribusi().getId();
        }

        Optional.ofNullable(model.getOrganization()).ifPresent(org -> {
            this.organization = new MasterStrukturOrganisasiDto(org);
            this.idPenerima = org.getIdOrganization();
        });

        if (model.getUser() != null) {
            this.user = new MasterUserDto(model.getUser());
            this.idUser = model.getUser().getId();
        }
        
        this.isDownload = model.getIsDownload();
        if (model.getDownloadDate()!= null) {
            this.downloadDate = dateUtil.getCurrentISODate(model.getDownloadDate());
        }
        
        this.isRead = model.getIsRead();
        if (model.getReadDate()!= null) {
            this.readDate = dateUtil.getCurrentISODate(model.getReadDate());
        }
    }
}
