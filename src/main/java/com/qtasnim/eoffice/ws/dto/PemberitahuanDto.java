package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Pemberitahuan;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class PemberitahuanDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @Size(min = 1, max = 255, message = "panjang perihal harus diantara {min} dan {max}")
    private String perihal;

    @NotNull(message = "konten tidak boleh null.")
    @NotEmpty(message = "konten harus diisi.")
    private String konten;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;


    private String publishedBy;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    public PemberitahuanDto() {
    }

    public PemberitahuanDto(Pemberitahuan model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.perihal = model.getPerihal();
        this.konten = model.getKonten();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}