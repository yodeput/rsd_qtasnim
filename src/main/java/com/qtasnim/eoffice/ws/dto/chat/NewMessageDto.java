package com.qtasnim.eoffice.ws.dto.chat;

import com.qtasnim.eoffice.ws.dto.WebSocketMessage;

public class NewMessageDto extends WebSocketMessage {
    public NewMessageDto() {
        setType("[Chat] New Message");
    }
}
