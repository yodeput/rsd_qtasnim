package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterArsiparis;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class MasterArsiparisDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    private MasterStrukturOrganisasiDto petugasArsiparis;

    @NotNull(message = "idPetugasArsiparis tidak boleh null")
    private Long idPetugasArsiparis;

    private List<ArsiparisDto> arsiparisList;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;
    private String endDate;
    private Boolean isDeleted;

    public MasterArsiparisDto(MasterArsiparis model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.petugasArsiparis = Optional.ofNullable(model.getPetugas()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idPetugasArsiparis = Optional.ofNullable(model.getPetugas()).map(q->q.getIdOrganization()).orElse(null);
        if(model.getArsiparisList()!=null){
            this.arsiparisList = model.getArsiparisList().stream().map(q->new ArsiparisDto(q)).collect(Collectors.toList());
        }
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.isDeleted = model.getIsDeleted();
    }

    public MasterArsiparisDto() {
    }

}