package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class MetadataValueDto {
    private String name;
    private String label;
    private String value;
    private Long fieldId;
    private String fieldData;
//    private String tableSource;

    public MetadataValueDto(String name, String label, String value) {
        this.name = name;
        this.label = label;
        this.value = value;
    }

    public MetadataValueDto() {}
}
