package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class ScanDocumentDto {
    private TextExtractionDto texts;
    private String docId;

    public ScanDocumentDto() {
        texts = new TextExtractionDto();
    }

    public ScanDocumentDto(TextExtractionDto texts, String docId) {
        this.texts = texts;
        this.docId = docId;
    }
}
