package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Para;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
public class ParaDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long idPara;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Size(min = 1, max = 255, message = "panjang nama harus diantara {min} dan {max}")
    private String namaPara;

    @NotNull(message = "isPosition tidak boleh null")
    private Boolean isPosition;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    private List<ParaDetailDto> paraDetails;
    private List<Long>detailIds;
    private Boolean isDeleted;

    public ParaDto(){}

    public ParaDto(Para model) {
        this.idPara = model.getIdPara();
        this.namaPara = model.getNamaPara();
        this.isPosition = model.getIsPosition();

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        this.paraDetails = model.getParaDetails().stream().map(ParaDetailDto::new).collect(Collectors.toList());
        this.isDeleted = model.getIsDeleted();
    }
}
