package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PemeriksaNomorManual;
import com.qtasnim.eoffice.db.PemeriksaSurat;
import lombok.Data;

import java.util.Optional;

@Data
public class PemeriksaNomorManualDto {

    private Long id;

    private Long idPermohonan;

    private MasterStrukturOrganisasiDto pemeriksa;
    private Long idPemeriksa;

    private Long userId;
    private MasterUserDto user;

    private Boolean isUnitDokumen;

    public PemeriksaNomorManualDto(){}

    public PemeriksaNomorManualDto(PemeriksaNomorManual model){
        this.id = model.getId();

        Optional.ofNullable(model.getOrganization()).ifPresent(org -> {
            this.pemeriksa = new MasterStrukturOrganisasiDto(org);
            this.idPemeriksa = org.getIdOrganization();
        });

        this.isUnitDokumen = model.getIsUnitDokumen();

        if(model.getUser()!=null){
            this.user = new MasterUserDto(model.getUser());
            this.userId = model.getUser().getId();
        }

        Optional.ofNullable(model.getPermohonan()).ifPresent(per -> {
            this.idPermohonan = per.getId();
        });
    }
}
