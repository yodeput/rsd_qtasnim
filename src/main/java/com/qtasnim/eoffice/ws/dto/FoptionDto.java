package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class FoptionDto {

    private String id;
    private String optionName;
    private String valueName;
    private String error;

    public FoptionDto(){}
}
