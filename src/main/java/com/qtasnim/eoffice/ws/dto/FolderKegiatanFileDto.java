/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Agie
 */
@Data
public class FolderKegiatanFileDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private MasterFolderKegiatanDetailDto parent;
    private List<GlobalAttachmentDto> personalFiles;

    public FolderKegiatanFileDto() {
        personalFiles = new ArrayList<>();
    }
}
