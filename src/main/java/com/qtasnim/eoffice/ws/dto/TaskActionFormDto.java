package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class TaskActionFormDto {
    private String fieldName;
    private String fieldValue;
}
