package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class NotificationTaskDto {
    private String name;
    private Integer positionCount;
    private Integer pymtCount;
    private Integer pelakharCount;
    private Integer teamCount;

    public NotificationTaskDto() {}
}
