package com.qtasnim.eoffice.ws.dto;

import lombok.Data;


@Data
public class ChartDashboardDto {
    String jenisDokumen;
    String name;
    Long value;

    public ChartDashboardDto () {}
}
