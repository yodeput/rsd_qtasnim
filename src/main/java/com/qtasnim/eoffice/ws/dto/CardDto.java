package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class CardDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String code;
    private String title;
    private String description;
    private Long bucketId;
    private String progress;
    private Long prioritasId;
    private MasterPrioritasDto prioritas;
    private Boolean isDeleted;
    private MasterStrukturOrganisasiDto cardOwner;
    private Long cardOwnerId;
    private String start;
    private String end;
    private Long companyId;
    private CompanyCodeDto company;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private List<CardMemberDto> cardMember;
    private List<CardChecklistDto> cardChecklist;
    private List<Long> idMembers;

    public CardDto() {
    }

    public CardDto(Card model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.code = model.getKode();
        this.title = model.getTitle();
        this.description = model.getDescription();
        this.bucketId = Optional.ofNullable(model.getBucket()).map(a -> a.getId()).orElse(null);
        this.progress = model.getProgress();
        this.prioritasId = Optional.ofNullable(model.getPrioritas()).map(a -> a.getId()).orElse(null);
        this.prioritas = Optional.ofNullable(model.getPrioritas()).map(MasterPrioritasDto::new).orElse(null);
        this.isDeleted = model.getIsDeleted();
        this.cardOwner = Optional.ofNullable(model.getCardOwner()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.cardOwnerId = Optional.ofNullable(model.getCardOwner()).map(a -> a.getIdOrganization()).orElse(null);
        this.start = model.getStart() != null ? dateUtil.getCurrentISODate(model.getStart()) : null;
        this.end = model.getEnd() != null ? dateUtil.getCurrentISODate(model.getEnd()) : null;
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(a -> a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = model.getModifiedDate()!=null ? dateUtil.getCurrentISODate(model.getModifiedDate()) : null;
        if (model.getMemberList() != null && !model.getMemberList().isEmpty()) {
            List<CardMemberDto> memberList = new ArrayList<>();
            List<Long> ids = new ArrayList<>();
            model.getMemberList().forEach(a -> {
                if (!a.getIsDeleted()) {
                    CardMemberDto card = new CardMemberDto(a);
                    memberList.add(card);
                    if (a.getOrganization() != null) {
                        ids.add(a.getOrganization().getIdOrganization());
                    }
                }
            });
            this.cardMember = memberList;
            this.idMembers = ids;
        }
        if (model.getCheckList() != null && !model.getCheckList().isEmpty()) {
            List<CardChecklistDto> checkTmp = new ArrayList<>();
            model.getCheckList().forEach(b -> {
                if (!b.getIsDeleted()) {
                    CardChecklistDto card = new CardChecklistDto(b);
                    checkTmp.add(card);
                }
            });
            this.cardChecklist = checkTmp;
        }
    }
}
