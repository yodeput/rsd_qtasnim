package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterGrade;
import com.qtasnim.eoffice.db.MasterRuangArsip;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.db.constraints.ISODateAfter;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class MasterRuangArsipDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private MasterKorsaDto unitKerja;

    @NotNull(message = "idUnitKerja tidak boleh null")
    private String idUnitKerja;

//    @Size(min = 1, max = 255, message = "kode jenis_lemari harus diantara {min} dan {max}")
    private String kode;


    @Size(min = 1, max = 255, message = "panjang foto harus diantara {min} dan {max}")
    private String lokasi;

//    @Size(min = 1, max = 255, message = "panjang foto harus diantara {min} dan {max}")
    private String foto;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

   private List<String> url;

    public MasterRuangArsipDto() {
    }

    public MasterRuangArsipDto(MasterRuangArsip model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.unitKerja = Optional.ofNullable(model.getUnitKerja()).map(MasterKorsaDto::new).orElse(null);
        this.idUnitKerja = Optional.ofNullable(model.getUnitKerja()).map(q->q.getId()).orElse(null);
        this.kode = model.getKode();
        this.lokasi = model.getLokasi();
        this.foto = model.getFoto();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}