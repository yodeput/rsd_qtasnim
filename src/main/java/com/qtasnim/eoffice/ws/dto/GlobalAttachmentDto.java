package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.db.SuratDocLib;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Data
public class GlobalAttachmentDto {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String docId;
    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long idCompany;

    @NotNull(message = "docGeneratedName tidak boleh null")
    @NotEmpty(message = "docGeneratedName tidak boleh kosong")
    private String docGeneratedName;

    @NotNull(message = "docName tidak boleh null")
    @NotEmpty(message = "docName tidak boleh kosong")
    private String docName;

    private Long version;

    @NotNull(message = "Size tidak boleh null")
    private Long size;

    @NotNull(message = "isDeleted tidak boleh null")
    private Boolean isDeleted;

    @NotNull(message = "isKonsep tidak boleh null")
    private Boolean isKonsep;

    @NotNull(message = "isPublished tidak boleh null")
    private Boolean isPublished;

    @NotNull(message = "mimeType tidak boleh null")
    @NotEmpty(message = "mimeType tidak boleh kosong")
    private String mimeType;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private String number;

    @NotNull(message = "refTable tidak boleh null")
    @NotEmpty(message = "refTable tidak boleh kosong")
    private String refTable;

    @NotNull(message = "refId tidak boleh null")
    private Long refId;
    private String editLink;
    private String downloadLink;
    private String viewLink;
    private String idDocument;

    public GlobalAttachmentDto(GlobalAttachment model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.docId = model.getDocId();
        this.number = model.getNumber();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany =  Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.docGeneratedName = model.getDocGeneratedName();
        this.docName = model.getDocName();
        this.version = model.getVersion();
        this.size = model.getSize();
        this.isKonsep = model.getIsKonsep();
        this.isPublished = model.getIsPublished();
        this.mimeType = model.getMimeType();
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.refId = model.getReferenceId();
        this.refTable = model.getReferenceTable();
        this.isDeleted = model.getIsDeleted();
        this.idDocument = Optional.ofNullable(model.getIdDocument()).orElse(null);
    }
}
