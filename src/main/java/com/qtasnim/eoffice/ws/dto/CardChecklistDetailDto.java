package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.CardChecklist;
import com.qtasnim.eoffice.db.CardChecklistDetail;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Data
public class CardChecklistDetailDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String itemName;
    private Boolean isChecked;
    private Boolean isDeleted;
    private Long checklistId;
    private Long companyId;
    private CompanyCodeDto company;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private Long progress;

    public CardChecklistDetailDto () {}

    public CardChecklistDetailDto (CardChecklistDetail model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.itemName = model.getItemName();
        this.isChecked = model.getIsChecked();
        this.isDeleted = model.getIsDeleted();
        this.checklistId = Optional.ofNullable(model.getChecklistCard()).map(a->a.getId()).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
//        this.progress = model.getProgress();
    }
}
