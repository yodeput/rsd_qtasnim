package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PemeriksaNomorManual;
import com.qtasnim.eoffice.db.PenandatanganNomorManual;
import lombok.Data;

import java.util.Optional;

@Data
public class PenandatanganNomorManualDto {

    private Long id;
    private Long idPermohonan;

    private MasterStrukturOrganisasiDto penandatangan;
    private Long idPenandatangan;

    private Long userId;
    private MasterUserDto user;

    private MasterStrukturOrganisasiDto orgAN;
    private Long idANOrg;

    private MasterUserDto usrAN;
    private Long idANUser;

    private Integer seq;

    public PenandatanganNomorManualDto(){}

    public PenandatanganNomorManualDto(PenandatanganNomorManual model){
        this.id = model.getId();
        this.seq = model.getSeq();

        Optional.ofNullable(model.getOrganization()).ifPresent(org -> {
            this.penandatangan = new MasterStrukturOrganisasiDto(org);
            this.idPenandatangan = org.getIdOrganization();
        });

        if(model.getUser()!=null){
            this.user = new MasterUserDto(model.getUser());
            this.userId = model.getUser().getId();
        }

        Optional.ofNullable(model.getPermohonan()).ifPresent(per -> {
            this.idPermohonan = per.getId();
        });

        if(model.getOrgAN()!=null) {
            this.idANOrg = model.getOrgAN().getIdOrganization();
            this.orgAN = new MasterStrukturOrganisasiDto(model.getOrgAN());
        }

        if(model.getUserAN()!=null) {
            this.idANUser = model.getUserAN().getId();
            this.usrAN = new MasterUserDto(model.getUserAN());
        }


    }
}
