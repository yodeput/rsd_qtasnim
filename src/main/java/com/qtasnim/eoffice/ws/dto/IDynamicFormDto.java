package com.qtasnim.eoffice.ws.dto;

import java.util.List;

public interface IDynamicFormDto {
    Long getIdFormDefinition();
    List<MetadataValueDto> getMetadata();
}
