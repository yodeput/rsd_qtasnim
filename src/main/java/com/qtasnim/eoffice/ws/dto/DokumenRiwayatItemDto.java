package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SuratInteraction;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Data
public class DokumenRiwayatItemDto implements Serializable {
    private Long id;
    private Boolean isRead;
    private Boolean isFinished;
    private String finishedDate;
    private Long idOrg;
    private String jabatan;
    private String nama;
    private String nipp;
    private String readDate;

    public DokumenRiwayatItemDto(SuratInteraction model) {
        DateUtil dateUtil = new DateUtil();
        this.idOrg = model.getOrganization().getIdOrganization();
        this.jabatan = model.getOrganization().getOrganizationName();
        this.nama = model.getOrganization().getUser().getNameFront() +" "+model.getOrganization().getUser().getNameMiddleLast();
        this.nipp = model.getOrganization().getUser().getEmployeeId();
        this.isRead = model.getIsRead();
        this.readDate = Optional.ofNullable(model.getReadDate()).map(dateUtil::getCurrentISODate).orElse(null);
        this.isFinished = model.getIsFinished();
        this.finishedDate = Optional.ofNullable(model.getFinishedDate()).map(dateUtil::getCurrentISODate).orElse(null);
    }
}
