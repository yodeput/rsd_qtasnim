package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class TextSearchAttachsDetailDto {
    private List<Integer> pages;

    public TextSearchAttachsDetailDto() {
        pages = new LinkedList<>();
    }
}
