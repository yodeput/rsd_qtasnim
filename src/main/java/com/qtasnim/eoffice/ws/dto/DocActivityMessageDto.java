package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class DocActivityMessageDto extends WebSocketMessage {
    public DocActivityMessageDto() {
        setType("[DocLib] DocLib Added");
    }
}
