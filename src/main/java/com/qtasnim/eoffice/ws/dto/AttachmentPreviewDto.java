package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class AttachmentPreviewDto {
    private String fileName;
    private String link;
    private Boolean requireOnlyOfficeViewer;
    private String mimeType;

    public AttachmentPreviewDto() {
    }

    public AttachmentPreviewDto(String fileName, String link, Boolean requireOnlyOfficeViewer, String mimeType) {
        this.fileName = fileName;
        this.link = link;
        this.requireOnlyOfficeViewer = requireOnlyOfficeViewer;
        this.mimeType = mimeType;
    }
}
