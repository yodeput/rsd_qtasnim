package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class FieldDataDto implements Serializable {
    private String name;
    private Boolean isReadOnly;
    private Boolean isKontakInternal;
    private Boolean isMySelf;
    private Boolean vMySelf;
    private Boolean isOneToOne;
    private Boolean vOneToOne;
    private Boolean isFromDatabase;
    private Boolean isDirectlyChief;
    private Boolean vDirectChief;
    private Boolean isEqualSo;
    private Boolean vEqualSo;
    private List<FoptionDto> options;

    public FieldDataDto(){}
}
