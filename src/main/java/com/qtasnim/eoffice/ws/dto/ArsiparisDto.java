package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Arsiparis;
import com.qtasnim.eoffice.db.PenandatanganSurat;
import lombok.Data;

import java.util.Optional;

@Data
public class ArsiparisDto {
    private Long id;

    private Long idMarsiparis;

    private Long idPejabat;
    private MasterStrukturOrganisasiDto pejabat;

    public ArsiparisDto(){}

    public ArsiparisDto(Arsiparis model){
        this.id = model.getId();

        Optional.ofNullable(model.getPejabat()).ifPresent(org -> {
            this.pejabat = new MasterStrukturOrganisasiDto(org);
            this.idPejabat = org.getIdOrganization();
        });

        if(model.getArsiparis()!=null){
            this.idMarsiparis = model.getArsiparis().getId();
        }
    }
}
