package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterTindakan;
import com.qtasnim.eoffice.db.PenerimaSurat;
import com.qtasnim.eoffice.db.TindakanDisposisi;
import lombok.Data;

import java.util.Optional;

@Data
public class TindakanDisposisiDto {
    private Long id;
    private Long idDisposisi;
    private MasterTindakanDto tindakan;
    private Long idTindakan;

    public TindakanDisposisiDto(){}

    public TindakanDisposisiDto(TindakanDisposisi model){
        this.id = model.getId();
        this.idDisposisi = Optional.ofNullable(model.getSuratDisposisi()).map(a->a.getId()).orElse(null);
        this.tindakan = Optional.ofNullable(model.getTindakan()).map(MasterTindakanDto::new).orElse(null);
        this.idTindakan = Optional.ofNullable(model.getTindakan()).map(a->a.getId()).orElse(null);
    }
}
