package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PermohonanDokumen;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@EnableISODateAfterValidator
public class PermohonanDokumenDto {

    private Long id;

    @NotNull(message = "Tipe Layanan harus diisi")
    private String tipeLayanan;

//    @NotNull(message = "idArsip tidak boleh null")
    private Long idArsip;
    private ArsipDto arsip;

    private String noRegistrasi;

    @ISODate(format = DateUtil.DATE_FORMAT, message = "Tanggal Pinjam harus mengikuti format: {format}")
    private String tglPinjam;

//    @ISODate(format = DateUtil.DATE_FORMAT, message = "Tanggal Pinjam harus mengikuti format: {format}")
    private String tglKembali;

    @NotNull(message = "Tanggal Target tidak boleh null")
    @NotEmpty(message = "Tanggal target tidak boleh kosong")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "Tanggal Target harus mengikuti format: {format}")
    private String tglTarget;

    private Integer lamaPinjam;

    private String idUnitPeminjam;
    private MasterKorsaDto unitPeminjam;

    private String idUnitTujuan;
    private MasterKorsaDto unitTujuan;

    private Long idUserPeminjam;
    private MasterUserDto userPeminjam;

    private Long idUserTujuan;
    private MasterUserDto userTujuan;

    @NotNull(message = "Perihal tidak boleh null")
    @NotEmpty(message = "Perihal tidak boleh kosong")
    private String perihal;

    @NotNull(message = "Keperluan tidak boleh null")
    @NotEmpty(message = "Keperluan tidak boleh kosong")
    private String keperluan;

    private String status;

    private Integer perpanjangan;

    @NotNull(message = "Company harus dipilih")
    private Long idCompany;
    private CompanyCodeDto company;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    private Boolean isDeleted;

    private Long idSurat;
    private SuratDto surat;

    private List<PermohonanDokumenPenerimaDto> penerima;
    private List<PermohonanDokumenTembusanDto> tembusan;
    private Boolean isDelegated;

    public PermohonanDokumenDto(){
        penerima = new ArrayList();
        tembusan = new ArrayList();
    }

    public PermohonanDokumenDto(PermohonanDokumen model){
        DateUtil dateUtil = new DateUtil();
        this.id= model.getId();
        this.tipeLayanan=model.getTipeLayanan();

        this.idArsip = Optional.ofNullable(model.getArsip()).map(a->a.getId()).orElse(null);
        this.arsip = Optional.ofNullable(model.getArsip()).map(ArsipDto::new).orElse(null);

        this.noRegistrasi = model.getNoRegistrasi();
        this.tglPinjam= dateUtil.getCurrentISODate(model.getTglPinjam());
        if(model.getTglKembali()!=null) {
            this.tglKembali = dateUtil.getCurrentISODate(model.getTglKembali());
        }
        this.lamaPinjam = model.getLamaPinjam();

        this.idUserPeminjam = Optional.ofNullable(model.getUserPeminjam()).map(a->a.getId()).orElse(null);
        this.userPeminjam = Optional.ofNullable(model.getUserPeminjam()).map(MasterUserDto::new).orElse(null);
        this.idUserTujuan = Optional.ofNullable(model.getUserTujuan()).map(a->a.getId()).orElse(null);
        this.userTujuan = Optional.ofNullable(model.getUserTujuan()).map(MasterUserDto::new).orElse(null);

        this.idUnitPeminjam = Optional.ofNullable(model.getUnitPeminjam()).map(a->a.getId()).orElse(null);
        this.unitPeminjam = Optional.ofNullable(model.getUnitPeminjam()).map(MasterKorsaDto::new).orElse(null);
        this.idUnitTujuan = Optional.ofNullable(model.getUnitTujuan()).map(a->a.getId()).orElse(null);
        this.unitTujuan = Optional.ofNullable(model.getUnitTujuan()).map(MasterKorsaDto::new).orElse(null);

        this.perihal = model.getPerihal();
        this.keperluan = model.getKeperluan();
        this.status = model.getStatus();
        this.perpanjangan = model.getPerpanjangan();
        this.idCompany =  Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.isDeleted = model.getIsDeleted();
        this.idSurat = Optional.ofNullable(model.getSurat()).map(a->a.getId()).orElse(null);
        this.surat = Optional.ofNullable(model.getSurat()).map(SuratDto::new).orElse(null);
        if(model.getTglTarget()!=null) {
            this.tglTarget = dateUtil.getCurrentISODate(model.getTglTarget());
        }

        this.penerima = model.getPenerima().stream().map(q->new PermohonanDokumenPenerimaDto(q)).collect(Collectors.toList());
        this.tembusan = model.getTembusan().stream()
                .sorted((s1, s2) -> s1.getSeq().compareTo(s2.getSeq()))
                .map(q->new PermohonanDokumenTembusanDto(q)).collect(Collectors.toList());

    }
}
