package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ImageRepository;
import com.qtasnim.eoffice.db.MasterRole;
import com.qtasnim.eoffice.db.MasterUser;
import lombok.Data;

import java.util.Optional;

@Data
public class UserDto {
    private String username;

    private String nameFront;

    private String nameMiddleLast;

    private String image;

    private String email;

    private String[] roles;

    public UserDto(MasterUser user) {
        this.username = user.getLoginUserName();
        this.nameFront = user.getNameFront();
        this.nameMiddleLast = user.getNameMiddleLast();
        this.email = user.getEmail();
        this.image = Optional.ofNullable(user.getImage()).map(ImageRepository::getGeneratedFileName).orElse(null);
        this.roles = user.getOrganizationEntity().getRoleList().stream().map(MasterRole::getRoleName).toArray(String[]::new);
    }
}
