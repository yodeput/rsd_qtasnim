package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class DistribusiManualDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String nomorDokumen;
    @NotNull(message = "perihal tidak boleh null")
    private String perihal;
    @NotNull(message = "kurir tidak boleh null")
    private String kurir;
//    @NotNull(message = "Status hapus tidak boleh null")
    private Boolean isDeleted;

    private String jabatanPenerima;
    private String namaPenerima;
    private String nippPenerima;

    @NotNull(message = "companyId tidak boleh null")
    private Long idCompany;
    private CompanyCodeDto companyCode;

    @NotNull(message = "idPengirim tidak boleh null")
    private Long idPengirim;
    private MasterUserDto pengirim;
    
    private Long idPenerima;
    private MasterStrukturOrganisasiDto penerima;
    
    private Long idSurat;
    private SuratDto surat;

    @NotNull(message = "idKlasifikasiKeamanan tidak boleh null")
    private Long idKlasifikasiKeamanan;
    private MasterKlasifikasiKeamananDto klasifikasiKeamanan;

//    @NotNull(message = "idTingkatUrgensi tidak boleh null")
    private Long idTingkatUrgensi;
    private MasterTingkatUrgensiDto tingkatUrgensi;

    @NotNull(message = "idTingkatPerkembangan tidak boleh null")
    private Long idTingkatPerkembangan;
    private MasterTingkatPerkembanganDto tingkatPerkembangan;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    private String tglDokumen;
    private String tglDiterima;

    private List<TujuanDistribusiManualDto> tujuan;

    public DistribusiManualDto() { tujuan = new ArrayList<>(); }

    public DistribusiManualDto(DistribusiManual model) { this(model, true); }

    public DistribusiManualDto(DistribusiManual model, boolean includeRelation) {
        this();

        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.nomorDokumen = model.getNomorDokumen();
        this.perihal = model.getPerihal();
        this.jabatanPenerima = model.getJabatanPenerima();
        this.namaPenerima = model.getNamaPenerima();
        this.nippPenerima = model.getNippPenerima();
        this.kurir = model.getKurir();
        this.isDeleted = model.getIsDeleted();

        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(CompanyCode::getId).orElse(null);
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);

        this.idSurat = Optional.ofNullable(model.getSurat()).map(Surat::getId).orElse(null);
        this.surat = Optional.ofNullable(model.getSurat()).map(SuratDto::new).orElse(null);

        this.idPengirim = Optional.ofNullable(model.getPengirim()).map(MasterUser::getId).orElse(null);
        this.pengirim = Optional.ofNullable(model.getPengirim()).map(MasterUserDto::new).orElse(null);

        this.idPenerima = Optional.ofNullable(model.getPenerima()).map(MasterUser::getId).orElse(null);
        this.penerima = Optional.ofNullable(model.getPenerima()).map(q -> new MasterStrukturOrganisasiDto(q.getOrganizationEntity())).orElse(null);

        this.idKlasifikasiKeamanan = Optional.ofNullable(model.getKlasifikasiKeamanan()).map(MasterKlasifikasiKeamanan::getIdKlasifikasiKeamanan).orElse(null);
        this.klasifikasiKeamanan = Optional.ofNullable(model.getKlasifikasiKeamanan()).map(MasterKlasifikasiKeamananDto::new).orElse(null);

        this.idTingkatUrgensi = Optional.ofNullable(model.getTingkatUrgensi()).map(MasterTingkatUrgensi::getId).orElse(null);
        this.tingkatUrgensi = Optional.ofNullable(model.getTingkatUrgensi()).map(MasterTingkatUrgensiDto::new).orElse(null);

        this.idTingkatPerkembangan = Optional.ofNullable(model.getTingkatPerkembangan()).map(MasterTingkatPerkembangan::getId).orElse(null);
        this.tingkatPerkembangan = Optional.ofNullable(model.getTingkatPerkembangan()).map(MasterTingkatPerkembanganDto::new).orElse(null);
        
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();

        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }

        if (model.getTglDokumen() != null) {
            this.tglDokumen = Optional.ofNullable(dateUtil.getCurrentISODate(model.getTglDokumen())).orElse(null);
        }
        if (model.getTglDiterima() != null) {
            this.tglDiterima = Optional.ofNullable(dateUtil.getCurrentISODate(model.getTglDiterima())).orElse(null);
        }

        if (includeRelation) {
            bindTujuan(model);
        }
    }

    public static DistribusiManualDto minimal(DistribusiManual model) {
        return new DistribusiManualDto(model, false);
    }

    public void bindTujuan(DistribusiManual model) {
        this.tujuan = model.getTujuan().stream().map(TujuanDistribusiManualDto::new).collect(Collectors.toList());
    }
}
