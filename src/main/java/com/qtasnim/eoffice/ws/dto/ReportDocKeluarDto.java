package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PenerimaSurat;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ReportDocKeluarDto implements Serializable {
    private Long id;
    private String noSurat;
    private String tanggal;
    private String perihal;
    private List<PengirimDto> pengirim;
    private List<PenerimaSuratDto> penerima;
    private String area;
    private String unit;
    private String klasifikasiArsip;
    private String noAgenda;
    private String jenisDokumen;

    public ReportDocKeluarDto(){}
}
