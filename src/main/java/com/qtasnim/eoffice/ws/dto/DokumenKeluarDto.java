package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DokumenKeluarDto implements Serializable {
    private Long id;
    private String noSurat;
    private String tanggal;
    private String perihal;
    private List<PengirimDto> pengirim;
    private String jenisDokumen;
    private boolean isKorespondensi;

    public DokumenKeluarDto(){}
}
