package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SyncLog;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SyncLogDto {
    private Long id;
    private String type;
    private Long duration;
    private String executionDate;
    private String completedDate;
    private String status;

    public SyncLogDto(SyncLog model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.type = model.getType();
        this.executionDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.completedDate = dateUtil.getCurrentISODate(model.getCompletedDate());
        this.status = model.getStatus().toString();
        this.duration = model.getDuration();
    }
}
