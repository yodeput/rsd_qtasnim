package com.qtasnim.eoffice.ws.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GanttGroup extends GanttItem {
    private Boolean open;
}
