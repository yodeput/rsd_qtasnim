package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class WebSocketMessage {
    private String type = "Default";
    private Object payload;
}
