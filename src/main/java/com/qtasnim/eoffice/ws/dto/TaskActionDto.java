package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class TaskActionDto {
    @NotNull(message = "recordRefId tidak boleh null")
    @NotEmpty(message = "recordRefId tidak boleh kosong")
    private String taskId;
    private String passphrase;
    private Boolean isSecretary = false;

    private List<FormFieldDto> forms;

    public TaskActionDto() {
        forms = new ArrayList<>();
    }
}
