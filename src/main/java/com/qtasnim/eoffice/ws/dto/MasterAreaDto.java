package com.qtasnim.eoffice.ws.dto;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.MasterArea;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
public class MasterAreaDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "id tidak boleh null.")
    @NotEmpty(message = "id harus diisi.")
    private String id;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Size(min = 1, max = 100, message = "panjang nama harus diantara {min} dan {max}")
    private String nama;

    private Long idKota;
    private MasterKotaDto kota;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

    //    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    private Long companyId;

    private String singkatan;

    public MasterAreaDto() {
    }

    public MasterAreaDto(MasterArea model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.nama = model.getNama();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());

        this.kota = Optional.ofNullable(model.getKota()).map(MasterKotaDto::new).orElse(null);
        this.idKota = Optional.ofNullable(model.getKota()).map(a -> a.getIdKota()).orElse(null);

        this.singkatan = model.getSingkatan();
    }
}