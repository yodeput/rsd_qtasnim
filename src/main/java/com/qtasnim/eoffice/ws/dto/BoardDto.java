package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Board;
import com.qtasnim.eoffice.db.BoardMember;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class BoardDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    private Boolean isDeleted;
    private Long companyId;
    private CompanyCodeDto company;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private List<Long> idsBucket;
    private List<BucketDto> lists;
    private List<Long> idsMember;
    private List<BoardMemberDto> members;

    public BoardDto (){}

    public BoardDto(Board model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.title = model.getTitle();
        this.isDeleted = model.getIsDeleted();
        this.companyId = model.getCompanyCode().getId();
        this.company = new CompanyCodeDto(model.getCompanyCode());
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());

        if(model.getBucketList()!=null){
            this.idsBucket = model.getBucketList().stream().filter(q->!q.getIsDeleted()).map(a->a.getId()).collect(Collectors.toList());
            this.lists = model.getBucketList().stream().filter(q->!q.getIsDeleted()).map(BucketDto::new).collect(Collectors.toList());
        }

        if(model.getMembers() != null){
            this.idsMember = model.getMembers().stream().filter(q->!q.getIsDeleted()).map(a->a.getId()).collect(Collectors.toList());
            this.members = model.getMembers().stream().filter(q->!q.getIsDeleted()).map(BoardMemberDto::new).collect(Collectors.toList());
        }
    }
}
