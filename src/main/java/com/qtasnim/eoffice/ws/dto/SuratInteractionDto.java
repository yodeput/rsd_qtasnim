package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SuratDisposisi;
import com.qtasnim.eoffice.db.SuratInteraction;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
public class SuratInteractionDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @NotNull(message = "suratId tidak boleh null")
    private Long suratId;
    @NotNull(message = "organzationId tidak boleh null")
    private Long organzationId;
    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;
    @NotNull(message = "isRead tidak boleh null")
    private Boolean isRead;
    @NotNull(message = "isStarred tidak boleh null")
    private Boolean isStarred;
    @NotNull(message = "isImportant tidak boleh null")
    private Boolean isImportant;
    @NotNull(message = "isFinished tidak boleh null")
    private Boolean isFinished;

    private String finishedDate;
    private String createdDate;
    private String readDate;
    private String createdBy;
    private SuratDto surat;
    private MasterStrukturOrganisasiDto organisasi;
    private CompanyCodeDto company;
    private MasterUserDto user;
    private Long userId;


    public SuratInteractionDto() {
    }

    public SuratInteractionDto(SuratInteraction model) {
        this(model, false);
    }

    public SuratInteractionDto(SuratInteraction model, Boolean isMinimal) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.isRead = model.getIsRead();
        this.isImportant = model.getIsImportant();
        this.isStarred = model.getIsStarred();
        this.isFinished = model.getIsFinished();
        this.finishedDate = Optional.ofNullable(model.getFinishedDate()).map(dateUtil::getCurrentISODate).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.readDate = Optional.ofNullable(model.getReadDate()).map(dateUtil::getCurrentISODate).orElse(null);
        this.createdBy = model.getCreatedBy();

        if (!isMinimal) {
            this.suratId = Optional.ofNullable(model.getSurat()).map(q -> q.getId()).orElse(null);
            this.organzationId = Optional.ofNullable(model.getOrganization()).map(q -> q.getIdOrganization()).orElse(null);
            this.surat = Optional.ofNullable(model.getSurat()).map(SuratDto::new).orElse(null);
            this.organisasi = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
            this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
            this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
            this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
            this.userId = Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
        }
    }

}
