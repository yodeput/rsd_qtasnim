/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.RegulasiTerkait;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

/**
 *
 * @author THINKPAD-PC
 */
@Data
public class RegulasiTerkaitDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private Long regulasiId;
    private RegulasiDto regulasi;
    private Long regulasiTerkaitId;
    private RegulasiDto regulasiTerkait;

    private Boolean isDeleted;

    private Long companyId;
    private CompanyCodeDto company;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public RegulasiTerkaitDto(){}

    public RegulasiTerkaitDto(RegulasiTerkait model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.regulasi = Optional.ofNullable(model.getRegulasi()).map(RegulasiDto::new).orElse(null);
        this.regulasiId = Optional.ofNullable(model.getRegulasi()).map(q -> q.getId()).orElse(null);
        this.regulasiTerkait = Optional.ofNullable(model.getRegulasiTerkait()).map(RegulasiDto::new).orElse(null);
        this.regulasiTerkaitId = Optional.ofNullable(model.getRegulasiTerkait()).map(q -> q.getId()).orElse(null);
        this.isDeleted = model.getIsDeleted();
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());

    }
}
