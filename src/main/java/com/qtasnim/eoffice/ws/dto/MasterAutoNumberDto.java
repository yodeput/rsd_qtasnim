package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterAutoNumber;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.EnableUniqueValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterAutoNumber",fieldNames = {"title"})
public class MasterAutoNumberDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    
    @NotNull(message = "Title harus diisi")
    @NotEmpty(message = "Title harus diisi")
    private String title;

    // Counter nilai yg sudah di gunakan.
    // nilai 'value' = counter nomor dokumen terakhir.
    @NotNull(message = "Value harus diisi")
    private Integer value;

    @NotNull(message = "Format harus diisi")
    @Size(min = 1, max = 100, message = "Panjang Format harus diantara {min} dan {max}")
    private String format;

    @NotNull(message = "Reset Period harus diisi")
    @NotEmpty(message = "Reset Period harus diisi.")
    @Size(max = 5, message = "Panjang Reset Period maximal {max} Karakter")
    // resetPeriod = "YEAR" || "MONTH"
    private String resetPeriod;

    @NotNull(message = "Next Reset harus diisi.")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "Next Reset harus mengikuti format: {format}")
    private String nextReset;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public MasterAutoNumberDto(){}

    public MasterAutoNumberDto(MasterAutoNumber model) {
        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.nextReset = dateUtil.getCurrentISODate(model.getNextReset());


        this.id = model.getId();
        this.title = model.getTitle();
        this.value = model.getValue();
        this.format = model.getFormat();
        this.resetPeriod = model.getResetPeriod();
    }
}