/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Agie
 */
@Getter
@Setter
public class HelpDocumentMediaDto {
    private String fileType;
    private String fileName;
    private String docId;
    private String viewUrl;
    private String downloadUrl;
}
