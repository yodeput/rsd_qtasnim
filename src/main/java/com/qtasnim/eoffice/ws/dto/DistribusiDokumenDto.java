package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import java.io.Serializable;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@Data
public class DistribusiDokumenDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String perihal;
    @NotNull(message = "Jenis Distribusi tidak boleh null")
    private String jenis;
    private String passHash;
    private String passSalt;
    private String status;

    @NotNull(message = "Status hapus tidak boleh null")
    private Boolean isDeleted;

    @NotNull(message = "companyId tidak boleh null")
    private Long idCompany;
    private CompanyCodeDto companyCode;
    
    private MasterStrukturOrganisasiDto konseptor;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private String komentar;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String start;
    private String end;
    private String approvedDate;

    private List<DistribusiDokumenDetailDto> detail;
    private List<PenerimaDistribusiDto> penerima;
    private List<PenandatanganDistribusiDto> penandatangan;
    private List<PemeriksaDistribusiDto> pemeriksa;

    public DistribusiDokumenDto() {
        detail = new ArrayList<>();
        pemeriksa = new ArrayList<>();
        penandatangan = new ArrayList<>();
        penerima = new ArrayList<>();
    }

    public DistribusiDokumenDto(DistribusiDokumen model) {
        this(model, true);
    }

    public DistribusiDokumenDto(DistribusiDokumen model, boolean includeRelation) {
        this();

        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.perihal = model.getPerihal();
        this.jenis = model.getJenis();
        this.passHash = model.getPasswordHash();
        this.passSalt = model.getPasswordSalt();
        this.isDeleted = model.getIsDeleted();
        this.status = model.getStatus();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);

        this.konseptor = Optional.ofNullable(model.getKonseptor()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }

        this.start = dateUtil.getCurrentISODate(model.getStart());
        this.end = dateUtil.getCurrentISODate(model.getEnd());

        if (model.getApprovedDate() != null) {
            this.approvedDate = Optional.ofNullable(dateUtil.getCurrentISODate(model.getApprovedDate())).orElse(null);
        }

        if(includeRelation) {
            bindPenandatangan(model);
            bindPemeriksa(model);
            bindPenerima(model);
            bindDetail(model);
        }
    }

    public static DistribusiDokumenDto minimal(DistribusiDokumen model) {
        return new DistribusiDokumenDto(model, false);
    }

    public void bindPenandatangan(DistribusiDokumen model) {
        this.penandatangan = model.getPenandatangan().stream().map(PenandatanganDistribusiDto::new).collect(Collectors.toList());
    }

    public void bindPemeriksa(DistribusiDokumen model) {
        this.pemeriksa = model.getPemeriksa().stream().map(PemeriksaDistribusiDto::new).collect(Collectors.toList());
    }

    public void bindPenerima(DistribusiDokumen model) {
        this.penerima = model.getPenerima().stream().map(PenerimaDistribusiDto::new).collect(Collectors.toList());
    }

    public void bindDetail(DistribusiDokumen model) {
        this.detail = model.getDetail().stream().map(DistribusiDokumenDetailDto::new).collect(Collectors.toList());
    }
}
