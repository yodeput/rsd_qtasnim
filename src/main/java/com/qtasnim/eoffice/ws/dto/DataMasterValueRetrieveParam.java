package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.ws.rs.QueryParam;
import java.util.LinkedList;
import java.util.List;

@Data
public class DataMasterValueRetrieveParam {
    @QueryParam("tableSourceName")
    @NotNull
    private String tableSourceName;

    @QueryParam("selectedSourceField")
    private List<String> selectedSourceField;

    @QueryParam("selectedSourceValue")
    private String selectedSourceValue;

    public DataMasterValueRetrieveParam() {
        selectedSourceField = new LinkedList<>();
    }

    public DataMasterValueRetrieveParam(String tableSourceName, List<String> selectedSourceField, String selectedSourceValue) {
        this.tableSourceName = tableSourceName;
        this.selectedSourceField = selectedSourceField;
        this.selectedSourceValue = selectedSourceValue;
    }
}
