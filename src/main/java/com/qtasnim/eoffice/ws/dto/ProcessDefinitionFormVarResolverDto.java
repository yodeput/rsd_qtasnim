package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ProcessDefinitionFormVarResolver;
import lombok.Data;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;

@Data
public class ProcessDefinitionFormVarResolverDto {
    private Long id;
    private String name;
    private String expr;
    private List<OptionDto> options;

    public ProcessDefinitionFormVarResolverDto() {
        this.options = new ArrayList<>();
    }

    public ProcessDefinitionFormVarResolverDto(ProcessDefinitionFormVarResolver processDefinitionFormVarResolver) {
        this();
        if (processDefinitionFormVarResolver != null) {
            id = processDefinitionFormVarResolver.getId();
            name = processDefinitionFormVarResolver.getVarName();
            expr = processDefinitionFormVarResolver.getValueResolver();
        }
    }
}
