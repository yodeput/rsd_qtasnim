package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.DistribusiManual;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.TujuanDistribusiManual;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

@Data
public class TujuanDistribusiManualDto implements Serializable {

    private Long id;
    private Long idDistribusi;

    private Long organizationId;
    private MasterStrukturOrganisasiDto organization;
    
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public TujuanDistribusiManualDto() {}

    public TujuanDistribusiManualDto(TujuanDistribusiManual model) {
        DateUtil dateUtil = new DateUtil();
        
        this.id = model.getId();
        this.idDistribusi = Optional.ofNullable(model.getDistribusi()).map(DistribusiManual::getId).orElse(null);

        this.organizationId = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null);
        this.organization = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);

        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
    }
}
