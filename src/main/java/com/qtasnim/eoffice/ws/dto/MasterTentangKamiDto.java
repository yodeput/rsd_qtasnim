package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterTentangKami;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Base64;

@Data
public class MasterTentangKamiDto implements Serializable {

    private Long id;
    private String perihal;
    private String konten;
    private String jabatan;
    private String instansi;
    private String photo;
    private Boolean isLeftSide;
    private CompanyCodeDto company;
    private Long companyId;
    private String start;
    private String end;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private Long sort;

    public MasterTentangKamiDto() {
    }

    public MasterTentangKamiDto(MasterTentangKami model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.perihal = model.getPerihal();
        this.konten = model.getKonten();
        this.jabatan = model.getJabatan();
        this.instansi = model.getInstansi();
        if (model.getPhoto() != null) {
            this.photo = Base64.getEncoder().encodeToString(model.getPhoto());
        }
        this.isLeftSide = model.getIsLeftSide();
        if (model.getCompanyCode() != null) {
            this.company = new CompanyCodeDto(model.getCompanyCode());
            this.companyId = model.getCompanyCode().getId();
        }
        this.start = dateUtil.getCurrentISODate(model.getStart());
        this.end = dateUtil.getCurrentISODate(model.getEnd());
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.sort = model.getSort();
    }
}
