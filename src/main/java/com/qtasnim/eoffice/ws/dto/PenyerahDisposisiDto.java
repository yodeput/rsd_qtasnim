package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PenyerahDisposisi;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
public class PenyerahDisposisiDto implements Serializable {

    private Long id;

    private Long idParent;
    private PenyerahDisposisiDto parent;

    private Long idPermohonan;
    private PermohonanDokumenDto permohonan;

    private Long idOrganization;
    private MasterStrukturOrganisasiDto organization;

    private Long idTindakan;
    private MasterTindakanDto tindakan;

    @NotNull(message = "Komentar tidak boleh null")
    @NotEmpty(message = "Komentar tidak boleh kosong")
    private String comment;

    @NotNull(message = "Status is Read tidak boleh null")
    private Boolean isRead = false;

    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
    private String assignedDate;

    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
    private String executedDate;

    private String status;

    public PenyerahDisposisiDto(){}

    public PenyerahDisposisiDto(PenyerahDisposisi model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.idParent = Optional.ofNullable(model.getParent()).map(a->a.getId()).orElse(null);
        this.parent = Optional.ofNullable(model.getParent()).map(PenyerahDisposisiDto::new).orElse(null);
        this.idPermohonan = Optional.ofNullable(model.getPermohonan()).map(a->a.getId()).orElse(null);
        this.permohonan=Optional.ofNullable(model.getPermohonan()).map(PermohonanDokumenDto::new).orElse(null);
        this.idOrganization=Optional.ofNullable(model.getOrganization()).map(a->a.getIdOrganization()).orElse(null);
        this.organization=Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idTindakan=Optional.ofNullable(model.getTindakan()).map(a->a.getId()).orElse(null);
        this.tindakan=Optional.ofNullable(model.getTindakan()).map(MasterTindakanDto::new).orElse(null);
        this.comment=model.getComment();
        this.isRead = model.getIsRead();
        if(model.getAssignedDate()!=null) {
            this.assignedDate = dateUtil.getCurrentISODate(model.getAssignedDate());
        }
        if(model.getExecutedDate()!=null) {
            this.executedDate = dateUtil.getCurrentISODate(model.getExecutedDate());
        }
        this.status = model.getStatus();
    }
}
