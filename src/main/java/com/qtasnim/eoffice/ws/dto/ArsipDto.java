package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Arsip;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
public class ArsipDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private CompanyCodeDto companyCode;
    @NotNull(message = "idCompany id tidak boleh null.")
    private Long idCompany;

    @NotNull(message = "idBerkas tidak boleh null.")
    private Long idBerkas;

    private MasterStrukturOrganisasiDto organisasi;
    private Long idOrganisasi;

    private MasterKorsaDto unit;
    @NotNull(message = "idUnit tidak boleh null.")
    private String idUnit;

    private MasterKlasifikasiMasalahDto masalah;
    @NotNull(message = "idMasalah tidak boleh null.")
    private Long idMasalah;

    private MasterKlasifikasiKeamananDto keamanan;
    @NotNull(message = "idKeamanan tidak boleh null.")
    private Long idKeamanan;

    private MasterJenisDokumenDto jenisDokumen;
    @NotNull(message = "idJenisDokumen tidak boleh null.")
    private Long idJenisDokumen;

    private MasterBahasaDto bahasa;
    @NotNull(message = "idBahasa tidak boleh null.")
    private Long idBahasa;

    private MasterSatuanDto satuan;
    private Long idSatuan;

    private MasterTingkatPerkembanganDto tingkatPerkembangan;
    @NotNull(message = "idTingkatPerkembangan tidak boleh null.")
    private Long idTingkatPerkembangan;

    private MasterTingkatUrgensiDto tingkatUrgensi;
    @NotNull(message = "idTingkatUrgensia tidak boleh null.")
    private Long idTingkatUrgensi;

    private MasterKategoriArsipDto kategoriArsip;
    @NotNull(message = "idKategoriArsip tidak boleh null.")
    private Long idKategoriArsip;

    private MasterTingkatAksesPublikDto tingkatAkses;
    @NotNull(message = "idTingkatAkses tidak boleh null.")
    private Long idTingkatAkses;

    private MasterMediaArsipDto mediaArsip;
    @NotNull(message = "idMediaArsip tidak boleh null.")
    private Long idMediaArsip;

    private MasterPenciptaArsipDto penciptaArsipDto;
    @NotNull(message = "idPenciptaArsip tidak boleh null.")
    private Long idPenciptaArsip;

    @NotNull(message = "status tidak boleh null.")
    private String status;
    @NotNull(message = "perihal tidak boleh null.")
    private String perihal;
    @NotNull(message = "deskripsi tidak boleh null.")
    private String deskripsi;
//    @NotNull(message = "nomor tidak boleh null.")
    private String nomor;
    @NotNull(message = "isAsset tidak boleh null.")
    private Boolean isAsset;
    @NotNull(message = "isVital tidak boleh null.")
    private Boolean isVital;

    @NotNull(message = "jumlah tidak boleh null.")
    private int jumlah;

    private String tglNaskah;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private Boolean isDeleted;

    private String jraAktif;
    private String jraInaktif;

    public ArsipDto() {
    }

    public ArsipDto(Arsip model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();

        //this.berkas = Optional.ofNullable(model.getBerkas().getId()).orElse(null);
        this.idBerkas = Optional.ofNullable(model.getBerkas()).map(q -> q.getId()).orElse(null);

        this.organisasi = Optional.ofNullable(model.getOrganisasi()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idOrganisasi = Optional.ofNullable(model.getOrganisasi()).map(q -> q.getIdOrganization()).orElse(null);

        this.unit = Optional.ofNullable(model.getUnit()).map(MasterKorsaDto::new).orElse(null);
        this.idUnit = Optional.ofNullable(model.getUnit()).map(q -> q.getId()).orElse(null);

        this.jenisDokumen = Optional.ofNullable(model.getJenisDokumen()).map(MasterJenisDokumenDto::new).orElse(null);
        this.idJenisDokumen = Optional.ofNullable(model.getJenisDokumen()).map(q -> q.getIdJenisDokumen()).orElse(null);

        this.bahasa = Optional.ofNullable(model.getBahasa()).map(MasterBahasaDto::new).orElse(null);
        this.idBahasa = Optional.ofNullable(model.getBahasa()).map(q -> q.getId()).orElse(null);

        this.masalah = Optional.ofNullable(model.getKlasifikasiMasalah()).map(MasterKlasifikasiMasalahDto::new).orElse(null);
        this.idMasalah = Optional.ofNullable(model.getKlasifikasiMasalah()).map(q -> q.getIdKlasifikasiMasalah()).orElse(null);

        this.keamanan = Optional.ofNullable(model.getKlasifikasiKeamanan()).map(MasterKlasifikasiKeamananDto::new).orElse(null);
        this.idKeamanan = Optional.ofNullable(model.getKlasifikasiKeamanan()).map(q -> q.getIdKlasifikasiKeamanan()).orElse(null);

        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);

        this.satuan = Optional.ofNullable(model.getSatuan()).map(MasterSatuanDto::new).orElse(null);
        this.idSatuan = Optional.ofNullable(model.getSatuan()).map(q -> q.getId()).orElse(null);

        this.tingkatPerkembangan = Optional.ofNullable(model.getTingkatPerkembangan()).map(MasterTingkatPerkembanganDto::new).orElse(null);
        this.idTingkatPerkembangan = Optional.ofNullable(model.getTingkatPerkembangan()).map(q -> q.getId()).orElse(null);

        this.tingkatUrgensi = Optional.ofNullable(model.getTingkatUrgensi()).map(MasterTingkatUrgensiDto::new).orElse(null);
        this.idTingkatUrgensi = Optional.ofNullable(model.getTingkatUrgensi()).map(q -> q.getId()).orElse(null);

        this.kategoriArsip = Optional.ofNullable(model.getKategoriArsip()).map(MasterKategoriArsipDto::new).orElse(null);
        this.idKategoriArsip = Optional.ofNullable(model.getKategoriArsip()).map(q -> q.getId()).orElse(null);

        this.tingkatAkses = Optional.ofNullable(model.getTingkatAkses()).map(MasterTingkatAksesPublikDto::new).orElse(null);
        this.idTingkatAkses = Optional.ofNullable(model.getTingkatAkses()).map(q -> q.getId()).orElse(null);

        this.mediaArsip = Optional.ofNullable(model.getMediaArsip()).map(MasterMediaArsipDto::new).orElse(null);
        this.idMediaArsip = Optional.ofNullable(model.getMediaArsip()).map(q -> q.getId()).orElse(null);

        this.penciptaArsipDto = Optional.ofNullable(model.getPenciptaArsip()).map(MasterPenciptaArsipDto::new).orElse(null);
        this.idPenciptaArsip = Optional.ofNullable(model.getPenciptaArsip()).map(q -> q.getId()).orElse(null);

        this.status = model.getStatus();
        this.perihal = model.getPerihal();
        this.deskripsi = model.getDeskripsi();
        this.nomor = model.getNomor();

        this.isAsset = model.getIsAsset();
        this.isVital = model.getIsVital();

        this.jumlah = model.getJumlah();

        if (model.getTglNaskah() != null) {
            this.tglNaskah = dateUtil.getCurrentISODate(model.getTglNaskah());
        } else {
            this.tglNaskah = null;
        }
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();

        this.isDeleted = model.getIsDeleted();

        if (model.getJraAktif() != null) {
            this.jraAktif = dateUtil.getCurrentISODate(model.getJraAktif());
        } else {
            this.jraAktif = null;
        }
        if (model.getJraInaktif() != null) {
            this.jraInaktif = dateUtil.getCurrentISODate(model.getJraInaktif());
        } else {
            this.jraInaktif = null;
        }
    }
}
