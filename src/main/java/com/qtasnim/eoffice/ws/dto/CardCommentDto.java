package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.CardComment;
import com.qtasnim.eoffice.db.CardMember;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

@Data
public class CardCommentDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String comment;
    private Long cardId;
    private Long userId;
    private MasterUserDto user;
    private Boolean isDeleted;
    private MasterStrukturOrganisasiDto organization;
    private Long organizationId;
    private Long companyId;
    private CompanyCodeDto company;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public CardCommentDto() {}

    public CardCommentDto(CardComment model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.comment = model.getComment();
        this.cardId = Optional.ofNullable(model.getCard()).map(a->a.getId()).orElse(null);
        this.userId = Optional.of(model.getUser()).map(a->a.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.isDeleted = model.getIsDeleted();
        this.organization = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.organizationId = Optional.ofNullable(model.getOrganization()).map(a->a.getIdOrganization()).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
    }
}
