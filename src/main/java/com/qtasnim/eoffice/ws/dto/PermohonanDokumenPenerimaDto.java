package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.PermohonanDokumenPenerima;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import lombok.Data;

import java.util.Optional;

@Data
public class PermohonanDokumenPenerimaDto {

    private Long id;
    private Long permohonanDokumenId;
    private Long idOrganization;
    private Integer seq;
    private MasterStrukturOrganisasiDto organization;
    private MasterUserDto userApprover;

    public PermohonanDokumenPenerimaDto(){}

    public PermohonanDokumenPenerimaDto(PermohonanDokumenPenerima model){
        id = model.getId();
        permohonanDokumenId = model.getPermohonanDokumen().getId();
        organization = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        idOrganization = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null);
        userApprover = Optional.ofNullable(model.getUserApprover()).map(MasterUserDto::new).orElse(null);
        seq = model.getSeq();
    }
}
