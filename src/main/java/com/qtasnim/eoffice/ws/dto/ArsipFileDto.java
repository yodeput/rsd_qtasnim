/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Agie
 */
@Data
public class ArsipFileDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private ArsipDto parent;
    private List<GlobalAttachmentDto> files;
    
    public ArsipFileDto() {
        files = new ArrayList<>();
    }
}
