package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterEmailContent;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MasterEmailContentDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String type;
    private String body;
    private String subject;
    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    public MasterEmailContentDto(){}

    public MasterEmailContentDto(MasterEmailContent model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.type = model.getType();
        this.body = model.getBody();
        this.subject = model.getSubject();
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
    }
}