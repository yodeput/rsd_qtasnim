package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.KontakFavorit;
import com.qtasnim.eoffice.db.MasterKorsa;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.db.constraints.ISODateAfter;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class KontakFavoritDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

//    @NotNull(message = "idUser tidak boleh null")
    private Long idUser;
    private MasterUserDto user;

    @NotNull(message = "idOrg tidak boleh null")
    private Long idOrg;
    private MasterStrukturOrganisasiDto organisasi;

    public KontakFavoritDto() {
    }

    public KontakFavoritDto(KontakFavorit model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.idOrg = Optional.ofNullable(model.getOrganisasi()).map(a->a.getIdOrganization()).orElse(null);
        this.organisasi = Optional.ofNullable(model.getOrganisasi()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idUser = Optional.ofNullable(model.getUserOwn()).map(a->a.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUserOwn()).map(MasterUserDto::new).orElse(null);

        if(model.getCreatedDate()!=null) {
            this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        }
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
    }
}