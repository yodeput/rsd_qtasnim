/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.RegulasiHistory;
import com.qtasnim.eoffice.db.RegulasiPengganti;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

/**
 *
 * @author THINKPAD-PC
 */
@Data
public class RegulasiPenggantiDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private Long regulasiId;
    private RegulasiDto regulasi;
    private Long regulasiPenggantiId;
    private RegulasiDto regulasiPengganti;

    private Boolean isRenewal;

    private Long companyId;
    private CompanyCodeDto company;
    private String deskripsi;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;


    public RegulasiPenggantiDto(){}

    public RegulasiPenggantiDto(RegulasiPengganti model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.regulasi = Optional.ofNullable(model.getRegulasi()).map(RegulasiDto::new).orElse(null);
        this.regulasiId = Optional.ofNullable(model.getRegulasi()).map(q -> q.getId()).orElse(null);
        this.regulasiPengganti = Optional.ofNullable(model.getRegulasiPengganti()).map(RegulasiDto::new).orElse(null);
        this.regulasiPenggantiId = Optional.ofNullable(model.getRegulasiPengganti()).map(q -> q.getId()).orElse(null);
        this.deskripsi = model.getDeskripsi();
        this.isRenewal = model.getIsRenewal();
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());

    }
}
