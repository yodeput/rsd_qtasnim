package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.TembusanPara;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Data
public class TembusanParaDto {

    private Long id;

    private Long tembusanSuratId;
    private TembusanSuratDto tembusanSurat;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    @NotNull(message="Id Organisasi tidak boleh null")
    private Long idOrganization;

    private MasterStrukturOrganisasiDto organisasi;

    private Long idUser;
    private MasterUserDto user;

    public TembusanParaDto(){}

    public TembusanParaDto(TembusanPara model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.idOrganization = Optional.ofNullable(model.getOrganisasi()).map(a->a.getIdOrganization()).orElse(null);
        this.organisasi = Optional.ofNullable(model.getOrganisasi()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idUser = Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.tembusanSuratId = Optional.ofNullable(model.getTembusanSurat()).map(a->a.getId()).orElse(null);
        this.tembusanSurat = Optional.ofNullable(model.getTembusanSurat()).map(TembusanSuratDto::new).orElse(null);
    }
}
