package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterModul;
import com.qtasnim.eoffice.db.constraints.*;

import java.io.Serializable;
import java.util.List;

import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
//@EnableUniqueValidator(entityName = "MasterModul", fieldNames = {"namaModul"})
public class MasterModulDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long idModul;

    @NotNull(message = "namaModul tidak boleh null.")
    @NotEmpty(message = "namaModul harus diisi.")
    @Size(min = 1, max = 255, message = "panjang namaModul harus diantara {min} dan {max}")
//    @Unique(fieldName = "namaModul", entityName = "MasterModul", message = "Nama sudah ada.")
    private String namaModul;

    @NotNull(message = "tipe tidak boleh null.")
    @NotEmpty(message = "tipe harus diisi.")
    @Size(min = 1, max = 20, message = "panjang tipe harus diantara {min} dan {max}")
    private String tipe;

    @Size(min = 1, max = 255, message = "panjang translate harus diantara {min} dan {max}")
    private String translate;

//    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private String url;
    private String description;
    private String keyModul;
    private String iconClass;
    private Integer seq;

    private MasterModulDto parent;
    private Long idParent;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;
    private CompanyCodeDto companyCode;

    private List<RoleDto> roles;
    private List<Long> idRoles;

    public MasterModulDto(){}

    public MasterModulDto(MasterModul model) {
        this.idModul = model.getIdModul();
        this.namaModul = model.getNamaModul();
        this.parent =  Optional.ofNullable(model.getParent()).map(MasterModulDto::new).orElse(null);
        this.idParent = Optional.ofNullable(model.getParent()).map(q->q.getIdModul()).orElse(null);
        this.translate = model.getTranslate();
        this.url = model.getUrl();
        this.tipe = model.getTipe();
        this.description = model.getDescription();
        this.isActive = model.getIsActive();
//        this.roles = model.getRoleModuleList().stream().map(q->new RoleDto(q.getRoles())).collect(Collectors.toList());
        this.idRoles = model.getRoleModuleList().stream().map(q->q.getRoles().getIdRole()).collect(Collectors.toList());
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.keyModul = model.getKeyModul();
        this.iconClass = model.getIconClass();
        this.seq = model.getSeq();
    }
}
