package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Data
public class MengetahuiSuratDto {

    private Long id;

    @NotNull(message = "Surat tidak boleh null.")
    private Long idSurat;
    private Integer seq;

    private Long idOrganization;
    private MasterStrukturOrganisasiDto organization;

    private Long userId;
    private MasterUserDto user;

    private Long idVendor;
    private MasterVendorDto vendor;

    private Long idPara;
    private ParaDto para;

    public MengetahuiSuratDto(){}

    public MengetahuiSuratDto(MengetahuiSurat model){
        this.id = model.getId();
        this.seq = model.getSeq();
        this.idSurat = Optional.ofNullable(model.getSurat()).map(Surat::getId).orElse(null);

        Optional.ofNullable(model.getUser()).ifPresent(user -> {
            this.user = new MasterUserDto(user);
            this.userId = user.getId();
        });

        Optional.ofNullable(model.getPara()).ifPresent(para -> {
            this.para = new ParaDto(para);
            this.idPara = para.getIdPara();
        });

        Optional.ofNullable(model.getOrganization()).ifPresent(masterStrukturOrganisasi -> {
            this.organization = new MasterStrukturOrganisasiDto(masterStrukturOrganisasi);
            this.idOrganization = masterStrukturOrganisasi.getIdOrganization();
        });

        Optional.ofNullable(model.getVendor()).ifPresent(masterVendor -> {
            this.vendor = new MasterVendorDto(masterVendor);
            this.idVendor = masterVendor.getId();
        });
    }
}
