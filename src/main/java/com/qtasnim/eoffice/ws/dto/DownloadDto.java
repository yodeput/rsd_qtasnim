package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class DownloadDto {
    private String link;
    private String username;
    private String password;
    private String activeDate;

    public DownloadDto(){}
}
