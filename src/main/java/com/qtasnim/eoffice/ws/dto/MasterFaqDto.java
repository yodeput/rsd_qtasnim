package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ErrorReport;
import com.qtasnim.eoffice.db.MasterFaq;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class MasterFaqDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull(message = "pertanyaan tidak boleh null")
    private String pertanyaan;
    @NotNull(message = "jawaban tidak boleh null")
    private String jawaban;
    private String kategori;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;
    private Long companyId;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;
    private String endDate;

    public MasterFaqDto() {
    }

    public MasterFaqDto(MasterFaq model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.pertanyaan = model.getPertanyaan();
        this.jawaban = model.getJawaban();
        this.jawaban = model.getKategori();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}
