package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterJenisRegulasi;
import com.qtasnim.eoffice.db.MasterKota;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterJenisRegulasi", fieldNames = {"namaJenisRegulasi"})
public class MasterJenisRegulasiDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long idJenisRegulasi;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Size(min = 1, max = 100, message = "panjang nama harus diantara {min} dan {max}")
    // @Unique(fieldName = "namaJenisRegulasi", entityName = "MasterJenisRegulasi", message = "Nama sudah ada.")
    private String namaJenisRegulasi;

    @NotNull(message = "Format harus diisi")
    @Size(min = 1, max = 100, message = "Panjang Format harus diantara {min} dan {max}")
    private String deskripsiJenisRegulasi;

    @NotNull(message = "Seq harus diisi")
    private Integer seq;

    private CompanyCodeDto companyCode;
    @NotNull(message = "companyId tidak boleh null")
    @CompanyId
    private Long companyId;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")

    private String startDate;
    private String endDate;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    //    @NotNull(message = "endDate tidak boleh null")
    //    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
    //    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")

    public MasterJenisRegulasiDto(){}

    public MasterJenisRegulasiDto(MasterJenisRegulasi model) {

        this.idJenisRegulasi = model.getIdJenisRegulasi();
        this.namaJenisRegulasi = model.getNamaJenisRegulasi();
        this.deskripsiJenisRegulasi = model.getDeskripsiJenisRegulasi();
        this.seq = model.getSeq();

        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        DateUtil dateUtil = new DateUtil();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
    }
}
