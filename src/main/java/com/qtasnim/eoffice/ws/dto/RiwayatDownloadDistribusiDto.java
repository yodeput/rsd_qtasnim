package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PenerimaDistribusi;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

@Data
public class RiwayatDownloadDistribusiDto {
    private MasterStrukturOrganisasiDto organization;
    private String date;
    private String action;

    public RiwayatDownloadDistribusiDto(PenerimaDistribusi model) {
        DateUtil dateUtil = new DateUtil();

        this.organization = new MasterStrukturOrganisasiDto(model.getOrganization());
        this.date = dateUtil.getCurrentISODate(model.getDownloadDate());
        this.action = "Download";
    }
}
