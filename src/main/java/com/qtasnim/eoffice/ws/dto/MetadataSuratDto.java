package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class MetadataSuratDto {
    private SuratDto surat;
    private List<String> namaMetadata;
    private List<String> pengirim;
    private List<String> pemeriksa;
    private List<String> tembusan;
    private List<String> penerima;

    public MetadataSuratDto(){
        namaMetadata = new LinkedList<>();
        pengirim = new LinkedList<>();
        pemeriksa = new LinkedList<>();
        tembusan = new LinkedList<>();
        penerima = new LinkedList<>();
    }
}
