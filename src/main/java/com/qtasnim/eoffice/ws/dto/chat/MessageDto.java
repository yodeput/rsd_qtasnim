package com.qtasnim.eoffice.ws.dto.chat;

import com.qtasnim.eoffice.db.chat.Message;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.MasterUserDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class MessageDto {
    private Long id;
    private String message;
    private String date;
    private String chatId;
    private MasterUserDto from;

    public MessageDto(Message model) {
        DateUtil dateUtil = new DateUtil();

        id = model.getId();
        message = model.getMsg();
        chatId = model.getChatId();
        date = Optional.ofNullable(model.getAddTime()).map(dateUtil::getCurrentISODate).orElse(null);
        from = new MasterUserDto(model.getFrom());
    }
}
