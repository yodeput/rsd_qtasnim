package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Para;
import com.qtasnim.eoffice.db.ParaDetail;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class ParaResultDto {

    private Long idPara;
    private String namaPara;
    private Boolean isPositon;
    private List<String> pejabat;
    private List<String> users;
    private String start;
    private String end;

    public ParaResultDto(Para model){
        DateUtil dateUtil = new DateUtil();
        this.idPara = model.getIdPara();
        this.namaPara = model.getNamaPara();
        this.start = dateUtil.getCurrentISODate(model.getStart());
        this.end = dateUtil.getCurrentISODate(model.getEnd());
        this.isPositon = Optional.ofNullable(model.getIsPosition()).orElse(null);
        if(isPositon!=null && isPositon.booleanValue()) {
            this.pejabat = new ArrayList<>();
            if (!model.getParaDetails().isEmpty()) {
                for (ParaDetail detail : model.getParaDetails()) {
                    MasterStrukturOrganisasiDto org = Optional.ofNullable(detail.getOrganisasi()).map(MasterStrukturOrganisasiDto::new).orElse(null);
                    if (org != null) {
                        MasterUserDto user = Optional.ofNullable(org.getUser()).orElse(null);
                        if (user != null) {
                            String jabatan = org.getOrganizationName();
                            String nama = user.getNameFront() + " " + user.getNameMiddleLast();
                            String result = jabatan + " | " + nama + " | " + user.getEmployeeId();
                            pejabat.add(result);
                        }
                    }
                }
            }
        }else{
            if(!isPositon.booleanValue()){
                this.users = new ArrayList<>();
                if(!model.getParaDetails().isEmpty()){
                    for (ParaDetail detail : model.getParaDetails()) {
                        String nama = detail.getUser().getNameFront()+ ' ' + detail.getUser().getNameMiddleLast();
                        users.add(nama);
                    }
                }
            }
        }
    }
}
