package com.qtasnim.eoffice.ws.dto.chat;

import com.qtasnim.eoffice.ws.dto.WebSocketMessage;

public class NewContactDto extends WebSocketMessage {
    public NewContactDto() {
        setType("[Chat] Contact Added");
    }
}