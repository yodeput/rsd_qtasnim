package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterKategoriEksternal;
import com.qtasnim.eoffice.db.MasterPropinsi;
import com.qtasnim.eoffice.db.MasterVendor;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterVendor", fieldNames = {"nama"})
public class MasterVendorDto implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    private Long id;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Unique(fieldName = "nama", entityName = "MasterVendor", message = "Nama sudah ada.")
    private String nama;

    private String email;
    private String alamat;
    private String phone;
    private String pic;

    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;
    @CompanyId
    private Long companyId;

    private MasterKategoriEksternalDto kategoriEksternal;
    private Long idKategoriEksternal;

    private String namaUnit;
    private String website;
    private String kodePos;
    private String fax;

    private MasterPropinsiDto propinsi;
    private Long idPropinsi;

    private MasterKotaDto kota;
    private Long idKota;

    private List<UserDto> user;
    private List<Long> idUser;

    public MasterVendorDto(MasterVendor model) {
        this.isActive = model.getIsActive();
        this.id = model.getId();
        this.nama = model.getNama();
        this.email = model.getEmail();
        this.alamat = model.getAlamat();
        this.phone = model.getPhone();
        this.pic = model.getPic();
        this.namaUnit = model.getNamaUnit();
        this.kodePos = model.getKodePos();
        this.website = model.getWebsite();
        this.fax = model.getFax();


        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.kategoriEksternal = Optional.ofNullable(model.getKategoriEksternal()).map(MasterKategoriEksternalDto::new)
                .orElse(null);
        this.idKategoriEksternal = Optional.ofNullable(model.getKategoriEksternal()).map(a->a.getId()).orElse(null);
        this.propinsi = Optional.ofNullable(model.getPropinsi()).map(MasterPropinsiDto::new).orElse(null);
        this.idPropinsi = Optional.ofNullable(model.getPropinsi()).map(a->a.getId()).orElse(null);
        this.kota = Optional.ofNullable(model.getKota()).map(MasterKotaDto::new).orElse(null);
        this.idKota = Optional.ofNullable(model.getKota()).map(a->a.getIdKota()).orElse(null);

        if(model.getUser()!=null){
            this.idUser = model.getUser().stream().map(q->q.getId()).collect(Collectors.toList());
        }
    }

    public MasterVendorDto() {
    }

}