package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterPerusahaanPengiriman;
import com.qtasnim.eoffice.db.constraints.*;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterPerusahaanPengiriman", fieldNames = {"nama"})
public class MasterPerusahaanPengirimanDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "nama tidak boleh null")
    @Unique(fieldName = "nama", entityName = "MasterPerusahaanPengiriman", message = "Nama sudah ada.")
    private String nama;

    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

    //    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    @CompanyId
    private Long companyId;

    public MasterPerusahaanPengirimanDto(MasterPerusahaanPengiriman model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.nama = model.getNama();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(CompanyCode::getId).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        if (model.getEnd() != null) {
            this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        }
    }

    public MasterPerusahaanPengirimanDto() {
    }

}