package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.services.RiwayatService;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DetailConvertedPdfDto {
    String fileName;
    Integer page;
    String encodedImg;

    public DetailConvertedPdfDto () { }

    public DetailConvertedPdfDto (String fileName, Integer page, String base64) {
        this.fileName = fileName;
        this.page = page;
        encodedImg = base64;
    }
}
