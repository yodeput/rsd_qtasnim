package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SyncLog;
import com.qtasnim.eoffice.services.IService;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class BackgroundServiceDto {
    private String name;
    private String type;
    private Boolean isRun;
    private String status;
    private String lastExecutionDate;

    public BackgroundServiceDto() {

    }

    public BackgroundServiceDto(String type, IService service) {
        this(type, service, null);
    }

    public BackgroundServiceDto(String type, IService service, SyncLog syncLog) {
        this.name = service.getName();
        this.isRun = service.isRun();
        this.status = isRun ? "RUNNING" : "STOPPED";
        this.type = type;

        Optional.ofNullable(syncLog).ifPresent(s -> {
            DateUtil dateUtil = new DateUtil();

            this.lastExecutionDate = dateUtil.getCurrentISODate(s.getCreatedDate());
        });
    }
}
