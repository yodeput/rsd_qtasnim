package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.FormField;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Data
public class FormFieldDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private MasterMetadataDto metadata;
    // private FormDefinitionDto formDefinition;

    @NotNull(message = "Id metadata tidak boleh null")
    private Long metadataId;

    @NotNull(message = "Id form definition tidak boleh null")
    private Long definitionId;

    private String data;
    private String defaultValue;

    @NotNull(message = "Tipe Field tidak boleh null.")
    @NotEmpty(message = "Tipe Field harus diisi.")
    private String type;

    private String placeholder;
    private String tableSourceName;
    private String selectedSourceField;
    private String selectedSourceValue;

    @NotNull(message = "Sequence tidak boleh null")
    private Integer seq;

    @NotNull(message = "isActive tidak boleh null")
    private Boolean isRequired;

    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    private Boolean disabled = false;
    private Boolean isDeleted;
    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;
//    private String startDate;
//    private String endDate;

    private DataMasterValueRetrieve valueDataMaster;

    public FormFieldDto(){
        isDeleted = false;
        valueDataMaster = new DataMasterValueRetrieve();
    }

    public FormFieldDto(FormField model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.metadata = Optional.ofNullable(model.getMetadata()).map(MasterMetadataDto::new).orElse(null);
        // this.formDefinition = Optional.ofNullable(model.getDefinition()).map(FormDefinitionDto::new).orElse(null);
        this.metadataId = Optional.ofNullable(model.getMetadata()).map(q->q.getId()).orElse(null);
        this.definitionId = Optional.ofNullable(model.getDefinition()).map(q->q.getId()).orElse(null);
        this.data = model.getData();
        this.defaultValue = model.getDefaultValue();
        this.type = model.getType();
        this.placeholder = model.getPlaceholder();
        this.tableSourceName = model.getTableSourceName();
        this.selectedSourceField = model.getSelectedSourceField();
        this.selectedSourceValue = model.getSelectedSourceValue();
        this.seq = model.getSeq();
        this.isRequired = model.getIsRequired();
        this.isActive = model.getIsActive();
        this.disabled = model.getIsDisabled();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
//        this.startDate = dateUtil.getCurrentISODate(model.getStart());
//        this.endDate = dateUtil.getCurrentISODate(model.getEnd());

        valueDataMaster = new DataMasterValueRetrieve();
    }

}
