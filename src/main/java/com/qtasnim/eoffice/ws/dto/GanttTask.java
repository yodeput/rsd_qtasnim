package com.qtasnim.eoffice.ws.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class GanttTask extends GanttItem {
    private Date start;
    private Date end;
    private Integer completion;
    private GanttStatus status = GanttStatus.NOTSTARTED;
}
