package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TaskActionRequestDto {
    @NotNull(message = "recordRefId tidak boleh null")
    @NotEmpty(message = "recordRefId tidak boleh kosong")
    private String recordRefId;
}
