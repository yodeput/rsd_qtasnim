package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class TaskListDto implements Serializable {
    private String noDokumen;
    private String perihal;
    private Long id;
    private String jenisDokumen;
    private List<PengirimDto> pengirim;
    private List<String> unitPengirim;
    private String tglDiterima;
    private String tglDikirim;
    private String Aksi;

    // klasifikasi task
    private String taskClassification;
    private String path;

    public TaskListDto(){}
}
