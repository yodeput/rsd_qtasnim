package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class RefreshTokenDto {
    private String token;
    private String validUntil;
}
