package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterKota;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterKota", fieldNames = {"kodeKota", "namaKota"})
public class MasterKotaDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long idKota;

    @NotNull(message = "kode tidak boleh null.")
    @NotEmpty(message = "kode harus diisi.")
    @Size(min = 1, max = 10, message = "panjang kode harus diantara {min} dan {max}.")
    // @Unique(fieldName = "kodeKota", entityName = "MasterKota", message = "Kode sudah ada.")
    private String kodeKota;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Size(min = 1, max = 100, message = "panjang nama harus diantara {min} dan {max}")
    // @Unique(fieldName = "namaKota", entityName = "MasterKota", message = "Nama sudah ada.")
    private String namaKota;

    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    @CompanyId
    private Long companyId;

    private MasterPropinsiDto propinsi;
    private Long idPropinsi;

    public MasterKotaDto(){}

    public MasterKotaDto(MasterKota model) {
        this.idKota = model.getIdKota();
        this.kodeKota = model.getKodeKota();
        this.namaKota = model.getNamaKota();
        this.isActive = model.getIsActive();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.propinsi = Optional.ofNullable(model.getPropinsi()).map(MasterPropinsiDto::new).orElse(null);
        this.idPropinsi = Optional.ofNullable(model.getPropinsi()).map(a->a.getId()).orElse(null);
    }
}
