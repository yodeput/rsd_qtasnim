package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.db.SuratDisposisi;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class SuratDisposisiDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private Long idParent;
    private SuratDisposisiDto parent;
    @NotNull(message = "suratId tidak boleh null")
    private Long suratId;
    @NotNull(message = "organzationId tidak boleh null")
    private Long organizationId;
    @NotNull(message = "comment tidak boleh null")
    private String comment;
    @NotNull(message = "isRead tidak boleh null")
    private Boolean isRead;

    private String assignedDate;
    private String executedDate;
    private String targetDate;
    private Boolean isHadir;

    private String executionStatus;
    private SuratDto surat;
    private MasterStrukturOrganisasiDto organisasi;
    private List<TindakanDisposisiDto> tindakanDisposisi;
    private MasterUserDto user;
    private Long idUser;

    public SuratDisposisiDto() {
        tindakanDisposisi = new ArrayList<>();
    }

    public SuratDisposisiDto(SuratDisposisi model) {
        this(model, true);
    }

    public SuratDisposisiDto(SuratDisposisi model, Boolean includeSurat) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.idParent = model.getParent() != null ? model.getParent().getId() : null;

        if (includeSurat) {
            this.surat = Optional.ofNullable(model.getSurat()).map(SuratDto::new).orElse(null);
            this.suratId = Optional.ofNullable(model.getSurat()).map(Surat::getId).orElse(null);
        }

        this.organisasi = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.organizationId = Optional.ofNullable(model.getOrganization()).map(q -> q.getIdOrganization()).orElse(null);
        this.comment = model.getComment();
        this.isRead = model.getIsRead();

        if (model.getAssignedDate() != null) {
            this.assignedDate = dateUtil.getCurrentISODate(model.getAssignedDate());
        }

        if (model.getExecutedDate() != null) {
            this.executedDate = dateUtil.getCurrentISODate(model.getExecutedDate());

        }

        if (model.getTargetDate() != null) {
            this.targetDate = dateUtil.getCurrentISODate(model.getTargetDate());
        }

        this.executionStatus = Optional.ofNullable(model.getStatus()).orElse(null);
        this.isHadir = model.getIsHadir();

        if (model.getTindakanDisposisi() != null) {
            this.tindakanDisposisi = model.getTindakanDisposisi().stream().map(TindakanDisposisiDto::new).collect(Collectors.toList());
        }

        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.idUser = Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
    }

}
