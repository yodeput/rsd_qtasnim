package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.CardCategoryDetail;
import com.qtasnim.eoffice.db.CardComment;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

@Data
public class CardCategoryDetailDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private Long kategoriId;
    private KategoriCardDto kategori;
    private Long cardId;
    private Boolean isDeleted;
    private Long companyId;
    private CompanyCodeDto company;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public CardCategoryDetailDto() {}

    public CardCategoryDetailDto(CardCategoryDetail model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.kategoriId = Optional.ofNullable(model.getKategori()).map(a->a.getId()).orElse(null);
        this.kategori = Optional.ofNullable(model.getKategori()).map(KategoriCardDto::new).orElse(null);
        this.cardId = Optional.ofNullable(model.getCard()).map(a->a.getId()).orElse(null);
        this.isDeleted = model.getIsDeleted();
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
    }
}
