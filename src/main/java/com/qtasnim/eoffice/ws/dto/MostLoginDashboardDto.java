package com.qtasnim.eoffice.ws.dto;

import lombok.Data;


@Data
public class MostLoginDashboardDto {
    String jabatan;
    String unit;
    String nama;
    Long value;

    public MostLoginDashboardDto() {}
}
