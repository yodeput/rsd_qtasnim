package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PermohonanDokumenTembusan;
import lombok.Data;

import java.util.Optional;

@Data
public class PermohonanDokumenTembusanDto {

    private Long id;
    private Long permohonanDokumenId;
    private Long idOrganization;
    private Integer seq;
    private MasterStrukturOrganisasiDto organization;

    public PermohonanDokumenTembusanDto(){}

    public PermohonanDokumenTembusanDto(PermohonanDokumenTembusan model){
        id = model.getId();
        permohonanDokumenId = model.getPermohonanDokumen().getId();
        organization = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        idOrganization = Optional.ofNullable(model.getOrganization()).map(q -> q.getIdOrganization()).orElse(null);
        this.seq = model.getSeq();
    }
}
