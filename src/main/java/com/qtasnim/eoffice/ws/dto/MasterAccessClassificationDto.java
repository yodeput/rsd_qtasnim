package com.qtasnim.eoffice.ws.dto;

import java.io.Serializable;

import com.qtasnim.eoffice.db.MasterAccessClassification;

import lombok.Data;

@Data
public class MasterAccessClassificationDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private Boolean is_deleted;

    public MasterAccessClassificationDto(){}

    public MasterAccessClassificationDto(MasterAccessClassification model) {
        this.id = model.getId();
        this.name = model.getName();
        this.is_deleted = model.getIsDeleted();
    }
}