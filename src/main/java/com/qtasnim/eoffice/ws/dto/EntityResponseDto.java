package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class EntityResponseDto {
    private String jenisDokumen;
    private String taskResponse;
    private String taskExecutor;
    private String perihal;
    private String klasifikasiKeamanan;

    public EntityResponseDto(){}
}
