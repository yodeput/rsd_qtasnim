package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterLokasiKerja;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "MasterLokasiKerja", fieldNames = {"kodeLoker", "namaLoker"})
public class MasterLokasiKerjaDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long idLoker;

    @NotNull(message = "kode tidak boleh null.")
    @NotEmpty(message = "kode harus diisi.")
    @Size(min = 1, max = 50, message = "panjang kode harus diantara {min} dan {max}")
    @Unique(fieldName = "kodeLoker", entityName = "MasterLokasiKerja", message = "Kode sudah ada.")
    private String kodeLoker;

    @NotNull(message = "singkatan tidak boleh null.")
    @NotEmpty(message = "singkatan harus diisi.")
    @Size(min = 1, max = 50, message = "panjang singkatan harus diantara {min} dan {max}")
    private String singkatan;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Size(min = 1, max = 255, message = "panjang nama harus diantara {min} dan {max}")
    @Unique(fieldName = "namaLoker", entityName = "MasterLokasiKerja", message = "Nama sudah ada.")
    private String namaLoker;

    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private MasterKotaDto kota;
    private Long idKota;
    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    @CompanyId
    private Long companyId;

    public MasterLokasiKerjaDto(){}

    public MasterLokasiKerjaDto(MasterLokasiKerja model) {
        this.idLoker = model.getIdLoker();
        this.kodeLoker = model.getKodeLoker();
        this.singkatan = model.getSingkatan();
        this.namaLoker = model.getNamaLoker();
        this.kota = Optional.ofNullable(model.getKota()).map(MasterKotaDto::new).orElse(null);
        this.idKota = Optional.ofNullable(model.getKota()).map(q->q.getIdKota()).orElse(null);
        this.isActive = model.getIsActive();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}
