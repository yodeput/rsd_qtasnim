package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Bucket;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class BucketDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    private Boolean isDeleted;
    private Long boardId;
    private Long companyId;
    private CompanyCodeDto company;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private List<CardDto> cardList;
    private Long sort;

    public BucketDto(){}

    public BucketDto(Bucket model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.title = model.getTitle();
        this.isDeleted = model.getIsDeleted();
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.boardId = Optional.ofNullable(model.getBoard()).map(a->a.getId()).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());

        if(model.getCardList()!=null && !model.getCardList().isEmpty()){
            List<CardDto> cards = new ArrayList<>();
            model.getCardList().forEach(a->{
                if(a.getIsDeleted().booleanValue()==false) {
                    cards.add(new CardDto(a));
                }
            });
            this.cardList = cards;
        }

        this.sort = model.getSort();
    }
}
