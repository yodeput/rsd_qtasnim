package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

import static com.qtasnim.eoffice.ws.dto.LinkPagination.PAGE_QUERY_PARAM;

@Data
public class QueryParams {
    @QueryParam("q") String question;

    @Min(value = 1, message = "page start at 1")
    @QueryParam(PAGE_QUERY_PARAM) @DefaultValue("1") Integer page;
}
