package com.qtasnim.eoffice.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NgGantt {
    @JsonProperty("pID")
    private Long id;
    @JsonProperty("pName")
    private String name;
    @JsonProperty("pStart")
    private String start;
    @JsonProperty("pEnd")
    private String end;
    @JsonProperty("pClass")
    private String clazz;
    @JsonProperty("pLink")
    private String link;
    @JsonProperty("pMile")
    private Integer mile;
    @JsonProperty("pRes")
    private String resource = "";
    @JsonProperty("pComp")
    private Integer completion;
    @JsonProperty("pGroup")
    private Integer group;
    @JsonProperty("pParent")
    private Long parent;
    @JsonProperty("pOpen")
    private Integer open;
    @JsonProperty("pDepend")
    private Long depend;
    @JsonProperty("pCaption")
    private String caption;
    @JsonProperty("pNotes")
    private String notes;


    public static NgGantt sample() {
        NgGantt ganttProcessDto = new NgGantt();
        ganttProcessDto.setId(1L);
        ganttProcessDto.setName("Define Chart API");
        ganttProcessDto.setClazz("ggroupblack");
        ganttProcessDto.setStart("2017-02-20");
        ganttProcessDto.setEnd("2017-02-25");
        ganttProcessDto.setMile(0);
        ganttProcessDto.setResource("Brian");
        ganttProcessDto.setCompletion(0);
        ganttProcessDto.setGroup(1);
        ganttProcessDto.setParent(0L);
        ganttProcessDto.setOpen(1);
        ganttProcessDto.setNotes("Some Notes text");

        return ganttProcessDto;
    }
}
