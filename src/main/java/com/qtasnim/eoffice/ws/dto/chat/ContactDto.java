package com.qtasnim.eoffice.ws.dto.chat;

import com.qtasnim.eoffice.db.chat.Contact;
import com.qtasnim.eoffice.ws.dto.MasterUserDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ContactDto {
    private Long id;
    private String status;
    private MasterUserDto to;
    private Long unread;

    public ContactDto(Contact contact) {
        id = contact.getId();
        to = new MasterUserDto(contact.getTo());
        status = "online";
        unread = contact.getUnread();
    }
}
