package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.KhazanahActionHistory;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
public class KhazanahActionHistoryDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @NotNull(message = "khazanahId tidak boleh null")
    private Long khazanahId;
    @NotNull(message = "organzationId tidak boleh null")
    private Long organizationId;
    @NotNull(message = "userId tidak boleh null")
    private Long userId;
    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;
    @NotNull(message = "isViewed tidak boleh null")
    private Boolean isViewed;
    @NotNull(message = "isDownloaded tidak boleh null")
    private Boolean isDownloaded;

    private String viewedDate;
    private String downloadedDate;
    private KhazanahDto khazanah;
    private MasterStrukturOrganisasiDto organisasi;
    private CompanyCodeDto company;
    private MasterUserDto user;


    public KhazanahActionHistoryDto() {
    }

    public KhazanahActionHistoryDto(KhazanahActionHistory model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.khazanah = Optional.ofNullable(model.getKhazanah()).map(KhazanahDto::new).orElse(null);
        this.khazanahId = Optional.ofNullable(model.getKhazanah()).map(q -> q.getId()).orElse(null);
        this.organisasi = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.organizationId = Optional.ofNullable(model.getOrganization()).map(q -> q.getIdOrganization()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.userId = Optional.ofNullable(model.getUser()).map(q -> q.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.isViewed = model.getIsViewed();
        this.isDownloaded = model.getIsDownloaded();
        if (model.getViewedDate() != null) {
            this.viewedDate = dateUtil.getCurrentISODate(model.getViewedDate());
        }
        if (model.getDownloadedDate() != null) {
            this.downloadedDate = dateUtil.getCurrentISODate(model.getDownloadedDate());
        }
    }

}
