package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.db.SuratCatatanSekretaris;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class SuratCatatanSekretarisDto {
    private Long id;
    private Long idSurat;
    private Long idPejabat;
    private Long idSekretaris;
    private MasterStrukturOrganisasiDto pejabat;
    private MasterStrukturOrganisasiDto sekretaris;
    private String note;
    private String createdDate;

    public SuratCatatanSekretarisDto(SuratCatatanSekretaris model) {
        if (model != null) {
            DateUtil dateUtil = new DateUtil();

            id = model.getId();
            idSurat = Optional.ofNullable(model.getSurat()).map(Surat::getId).orElse(null);
            idPejabat = Optional.ofNullable(model.getPejabat()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null);
            idSekretaris = Optional.ofNullable(model.getSekretaris()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null);
            pejabat = new MasterStrukturOrganisasiDto(model.getPejabat());
            sekretaris = new MasterStrukturOrganisasiDto(model.getSekretaris());
            note = model.getNote();
            createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        }
    }
}
