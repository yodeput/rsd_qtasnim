/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

/**
 *
 * @author Agie
 */
@Data
public class ReportArsipDto {
    private Long id;
    
    private String unit;
    private String klasifikasiArsip;
    private String judulArsip;
    private String nomorArsip;
    private String jenisDokumen;
    private String namaBerkas;

    private String tglNaskah;
    private String tglRetensiAktif;
    private String tglRetensiInAktif;
    private String tglCloseFolder;
}

