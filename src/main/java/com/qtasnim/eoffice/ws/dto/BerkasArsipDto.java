/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Agie
 */
@Data
public class BerkasArsipDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private BerkasDto parent;
    private List<ArsipDto> arsips;
    
    public BerkasArsipDto() {
        arsips = new ArrayList<>();
    }
}
