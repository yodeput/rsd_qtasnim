package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ProcessDefinition;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class ProcessDefinitionDto {
    private Long id;
    private String definitionId;
    private String deploymentId;
    private String definition;
    private String definitionName;
    private String formName;
    private Double version;
    private String activationDate;
    private Long workflowProviderId;
    private Boolean activated = false;
    private Boolean majorVersion = false;
    private MasterJenisDokumenDto jenisDokumen;
    private Long idJenisDokumen;
    private List<ProcessDefinitionFormVarResolverDto> varResolvers;

    public ProcessDefinitionDto() {
        varResolvers = new ArrayList<>();
    }

    public ProcessDefinitionDto(ProcessDefinition processDefinition) {
        if (processDefinition != null) {
            id = processDefinition.getId();
            definitionId = processDefinition.getDefinitionId();
            deploymentId = processDefinition.getDeploymentId();
            definitionName = processDefinition.getDefinitionName();
            formName = processDefinition.getFormName();
            version = processDefinition.getVersion();
            workflowProviderId = processDefinition.getWorkflowProvider().getId();
            varResolvers = processDefinition.getFormVars().stream().map(ProcessDefinitionFormVarResolverDto::new).collect(Collectors.toList());
            definition = processDefinition.getDefinition();

            Optional.ofNullable(processDefinition.getJenisDokumen()).ifPresent(jenisDokumen -> {
                this.jenisDokumen = new MasterJenisDokumenDto(jenisDokumen);
                this.idJenisDokumen = jenisDokumen.getIdJenisDokumen();
            });

            if (processDefinition.getActivationDate() != null) {
                DateUtil dateUtil = new DateUtil();
                activationDate = dateUtil.getCurrentISODate(processDefinition.getActivationDate());
            }

            if (version % 1 == 0) {
                majorVersion = true;
            }
        }
    }
}
