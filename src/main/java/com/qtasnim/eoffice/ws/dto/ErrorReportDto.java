package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ErrorReport;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class ErrorReportDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    @NotNull(message = "kategori tidak boleh null")
    private String kategori;
    @NotNull(message = "uraian tidak boleh null")
    private String uraian;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;
    private Long companyId;

    private String docId;
    private List<ErrorReportImageDto> files;

    public ErrorReportDto() {
    }

    public ErrorReportDto(ErrorReport model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.kategori = model.getKategori();
        this.uraian = model.getUraian();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();

        this.docId = Optional.ofNullable(model.getDocId()).orElse(null);
    }
}
