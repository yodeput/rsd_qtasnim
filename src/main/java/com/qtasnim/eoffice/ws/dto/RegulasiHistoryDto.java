/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Regulasi;
import com.qtasnim.eoffice.db.RegulasiHistory;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author THINKPAD-PC
 */
@Data
public class RegulasiHistoryDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private Long regulasiId;
    private RegulasiDto regulasi;

    private String status;

    private Long companyId;
    private CompanyCodeDto company;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;


    public RegulasiHistoryDto(){}

    public RegulasiHistoryDto(String status, RegulasiDto regulasi, CompanyCodeDto company) {
        this.regulasi = regulasi;
        this.status = status;
        this.company = company;
    }

    public RegulasiHistoryDto(RegulasiHistory model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.regulasi = Optional.ofNullable(model.getRegulasi()).map(RegulasiDto::new).orElse(null);
        this.regulasiId = Optional.ofNullable(model.getRegulasi()).map(q -> q.getId()).orElse(null);
        this.status = model.getStatus();
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());

    }
}
