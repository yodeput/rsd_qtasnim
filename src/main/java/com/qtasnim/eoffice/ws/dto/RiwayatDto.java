package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SuratInteraction;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

@Data
public class RiwayatDto {
    private MasterStrukturOrganisasiDto organization;
    private MasterUserDto user;
    private String date;
    private String action;
    private String delegasiType;

    public RiwayatDto(SuratInteraction model) {
        DateUtil dateUtil = new DateUtil();

        this.organization = new MasterStrukturOrganisasiDto(model.getOrganization());
        this.date = dateUtil.getCurrentISODate(model.getReadDate());
        this.action = "Read";
    }

    public RiwayatDto(){}
}
