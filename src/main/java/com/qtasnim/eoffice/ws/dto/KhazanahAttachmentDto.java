package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class KhazanahAttachmentDto {
    private String tipe;
    private String fileName;
    private String mimeType;

    public KhazanahAttachmentDto() {}
}
