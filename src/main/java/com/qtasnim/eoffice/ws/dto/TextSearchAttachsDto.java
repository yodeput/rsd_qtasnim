package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class TextSearchAttachsDto {
    private List<GlobalAttachmentDto> attachments;

    public TextSearchAttachsDto() {
        attachments = new LinkedList<>();
    }
}
