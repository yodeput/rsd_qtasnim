package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Base64;
import java.util.Optional;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
@EnableUniqueValidator(entityName = "CompanyCode", fieldNames = {"code","name"})
public class CompanyCodeDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "code tidak boleh null.")
    @NotEmpty(message = "code harus diisi.")
    private String code;

    @NotNull(message = "name tidak boleh null.")
    @NotEmpty(message = "name harus diisi.")
    private String name;

    @NotNull(message = "url tidak boleh null.")
    @NotEmpty(message = "url harus diisi.")
    private String url;

    @NotNull(message = "address tidak boleh null.")
    @NotEmpty(message = "address harus diisi.")
    private String address;

    @NotNull(message = "email tidak boleh null.")
    @NotEmpty(message = "email harus diisi.")
    @Email(message = "email tidak valid")
    private String email;

    @NotNull(message = "description tidak boleh null.")
    @NotEmpty(message = "description harus diisi.")
    private String description;

    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate( format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String bg;
    private String logo;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;    

    public CompanyCodeDto() {
    }

    public CompanyCodeDto(CompanyCode model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.code = model.getCode();
        this.name = model.getName();
        this.url = model.getUrl();
        this.address = model.getAddress();
        this.description = model.getDescription();
        this.email = model.getEmail();
        if(model.getLogo()!=null) {
            this.logo = Base64.getEncoder().encodeToString(model.getLogo());
        }
        if(model.getBg()!=null) {
            this.bg = Base64.getEncoder().encodeToString(model.getBg());
        }
        this.isActive = model.getIsActive();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());

    }
}