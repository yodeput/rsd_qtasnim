package com.qtasnim.eoffice.ws.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationContextDto {
    private String timezone;
    private String tzOffset;
    private String plugginScanningTwainKey;
    private Boolean plugginScanningTwainIsTrial;
    private String applicationName;
}
