package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.BoardMember;
import com.qtasnim.eoffice.db.CardMember;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Data
public class BoardMemberDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private Long userId;
    private MasterUserDto user;
    private Boolean isDeleted;
    private MasterStrukturOrganisasiDto organization;
    private List<Long> organizationId;
    private MasterKorsaDto unit;
    private String idUnit;
    private Long boardId;
    private Long companyId;
    private CompanyCodeDto company;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public BoardMemberDto() {}

    public BoardMemberDto(BoardMember model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.userId = Optional.of(model.getUser()).map(a->a.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.isDeleted = model.getIsDeleted();
        this.organization = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.boardId = Optional.ofNullable(model.getBoard()).map(a->a.getId()).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        this.unit = Optional.ofNullable(model.getUnit()).map(MasterKorsaDto::new).orElse(null);
        this.idUnit = Optional.ofNullable(model.getUnit()).map(a->a.getId()).orElse(null);
    }
}
