package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Data
public class DataMasterSelectedSourceDto {
    private String selectedSourceValue;
    private Map<String,String> selectedSourceField;
    private List<Object> parent;

    public DataMasterSelectedSourceDto() {
        selectedSourceField = new HashMap<>();
        parent = new LinkedList<>();
    }
}
