package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.FormValue;
import com.qtasnim.eoffice.db.ReferensiSuratDetail;
import com.qtasnim.eoffice.db.Surat;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class DokumenReferensiDto implements Serializable {

    private Long id;
    private Long suratDetailId;
    private Long suratId;
    private String noDokumen;
    private String perihal;
    private String jenisDokumen;
    private List<String> jabatanPengirim;
    private List<String> namaPengirim;
    private Boolean isEksternal=false;
    private String tanggalDokumen;
    private String namaFile;
    private String downloadLink;

    public DokumenReferensiDto(){}

    public DokumenReferensiDto(ReferensiSuratDetail q){
        if(q.getReferensiSurat().getSurat()!=null) {
            Surat suratRef = q.getReferensiSurat().getSurat();
            setId(q.getReferensiSurat().getId());
            setSuratId(suratRef.getId());
            setSuratDetailId(q.getId());
            setNoDokumen(suratRef.getNoDokumen());
            setJenisDokumen(suratRef.getFormDefinition().getJenisDokumen().getNamaJenisDokumen());
            setJabatanPengirim(suratRef.getPenandatanganSurat().stream().map(a -> a.getOrganization().getOrganizationName()).collect(Collectors.toList()));
            setNamaPengirim(suratRef.getPenandatanganSurat().stream().map(a -> a.getOrganization().getUser().getNameFront() + " " + a.getOrganization().getUser().getNameMiddleLast()).collect(Collectors.toList()));
            FormValue fv1 = suratRef.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal")).findFirst().orElse(null);

            if (fv1 != null) {
                setPerihal(fv1.getValue());
            }

            FormValue fv2 = suratRef.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("tanggal dokumen")).findFirst().orElse(null);

            if (fv2 != null) {
                setTanggalDokumen(fv2.getValue());
            }
        }
    }
}
