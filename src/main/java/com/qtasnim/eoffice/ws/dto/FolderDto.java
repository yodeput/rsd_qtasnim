package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.db.MasterFolderKegiatan;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FolderDto implements Serializable {

    private Long id;
    private Long idCompany;
    private CompanyCodeDto companyCode;
    private Long idParent;
    private MasterFolderKegiatanDto parent;
    private String folderName;
    private Boolean isPersonalFolder;
    private Boolean isAllowToAdd;
    private String createdDate;
    private String start;
    private String end;

    public FolderDto(Long id, String name, Boolean personalFolder, Boolean isAllowToAdd, Date createdDate, Date start, Date end, Long parent, CompanyCode companyCode){
        DateUtil dateUtil = new DateUtil();
        this.id = id;
        this.folderName = name;
        this.isPersonalFolder = personalFolder;
        this.isAllowToAdd = isAllowToAdd;
        if(createdDate!=null){
            this.createdDate = dateUtil.getCurrentISODate(createdDate);
        }
        if(start!=null){
            this.start = dateUtil.getCurrentISODate(start);
        }
        if(end!=null){
            this.end = dateUtil.getCurrentISODate(end);
        }
        this.idParent =  parent;
        this.companyCode = new CompanyCodeDto(companyCode);
        this.idCompany = companyCode!=null ? companyCode.getId() : null;
    }
}
