package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.OrganisasiHistory;
import com.qtasnim.eoffice.db.PosisiHistory;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.util.Date;
import java.util.Optional;

@Data
public class PosisiHistoryDto {

    private Long id;

    private String plans_ex;
    private String pstxt_ex;
    private String begda;
    private String endda;
    private String plans_pr;
    private String pstxt_pr;
    private String begda_prev;
    private String endda_prev;

    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private CompanyCodeDto companyCode;
    private Long companyId;

    public PosisiHistoryDto(PosisiHistory model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.plans_ex = model.getPlans_ex();
        this.pstxt_ex = model.getPstxt_ex();
        this.begda = dateUtil.getCurrentISODate(model.getBegda());
        this.endda = dateUtil.getCurrentISODate(model.getEndda());
        this.plans_pr = model.getPlans_pr();
        this.pstxt_pr = model.getPstxt_pr();
        this.begda_prev = dateUtil.getCurrentISODate(model.getBegda_prev());
        this.endda_prev = dateUtil.getCurrentISODate(model.getEndda_prev());
        this.createdBy = model.getCreatedBy();
        this.createdDate = model.getCreatedDate();
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = model.getModifiedDate();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

    }

}