package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class PengirimDto {
    private String jabatan;
    private String nama;
    private String nipp;
}
