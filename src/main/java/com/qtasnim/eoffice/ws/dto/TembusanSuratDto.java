package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterVendor;
import com.qtasnim.eoffice.db.TembusanSurat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Data
public class TembusanSuratDto {

    private Long id;

    @NotNull(message = "Surat tidak boleh null.")
    private Long idSurat;
//    private SuratDto surat;
    private Integer seq;

    private Long idOrganization;
    private MasterStrukturOrganisasiDto organization;

    private Long userId;
    private MasterUserDto user;

    private Long idVendor;
    private MasterVendorDto vendor;

    private Long idPara;
    private ParaDto para;
    private String delegasiType;

    public TembusanSuratDto(){}

    public TembusanSuratDto(TembusanSurat model){
        this(model,false);
    }

    public TembusanSuratDto(TembusanSurat model, boolean includeRelation){
        this.id = model.getId();
        this.idSurat = Optional.ofNullable(model.getSurat()).map(a->a.getId()).orElse(null);;
        this.seq = model.getSeq();
        this.organization = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idOrganization = Optional.ofNullable(model.getOrganization()).map(a->a.getIdOrganization()).orElse(null);
        this.idVendor = Optional.ofNullable(model.getVendor()).map(a->a.getId()).orElse(null);
        this.vendor = Optional.ofNullable(model.getVendor()).map(MasterVendorDto::new).orElse(null);
        this.delegasiType = model.getDelegasiType();
        if(includeRelation){
            bindPara(model);
            bindUser(model);
        }
    }

    public void bindUser(TembusanSurat model){
        this.para = Optional.ofNullable(model.getPara()).map(ParaDto::new).orElse(null);
        this.idPara = Optional.ofNullable(model.getPara()).map(a->a.getIdPara()).orElse(null);
    }

    public void bindPara(TembusanSurat model){
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.userId = Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
    }
}
