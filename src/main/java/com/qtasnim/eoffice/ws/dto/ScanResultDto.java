package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

@Data
public class ScanResultDto {
    private TextExtractionDto text;
    private byte[] pdf;
    private boolean isScannedTiff;

    public ScanResultDto() {
        text = new TextExtractionDto();
        pdf = null;
        isScannedTiff = true;
    }
}
