package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class CardEntity {
    private String title;
    private String cardOwner;
    private String formattedStart;
    private String formattedEnd;
    private String prioritas;
    private String desciption;
    private String pageLink;
    
    public CardEntity() {
        this.pageLink = "/apps/tasks/others";
    }
}
