package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterKlasifikasiKhazanah;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class MasterKlasifikasiKhazanahDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Size(min = 1, max = 255, message = "panjang nama harus diantara {min} dan {max}")
//    @Unique(fieldName = "nama", entityName = "MasterKlasifikasiKhazanah", message = "Nama sudah ada.")
    private String nama;

    private Long idParent;
    private  MasterKlasifikasiKhazanahDto parent;

    @NotNull(message = "idCompany tidak boleh null")
    private Long idCompany;
    private CompanyCodeDto companyCode;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    public MasterKlasifikasiKhazanahDto(){}

    public MasterKlasifikasiKhazanahDto(MasterKlasifikasiKhazanah model) {
        this.id = model.getId();
        this.nama = model.getNama();
        this.idParent = Optional.ofNullable(model.getParent()).map(a->a.getId()).orElse(null);
        this.parent = Optional.ofNullable(model.getParent()).map(MasterKlasifikasiKhazanahDto::new).orElse(null);
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}
