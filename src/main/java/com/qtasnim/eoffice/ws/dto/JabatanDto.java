package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class JabatanDto {
    private Long id;
    private String jabatan;
    private String tipe;

    public JabatanDto() {}

    public JabatanDto(Long id, String jabatan, String tipe) {
        this.id = id;
        this.jabatan = jabatan;
        this.tipe = tipe;
    }
}
