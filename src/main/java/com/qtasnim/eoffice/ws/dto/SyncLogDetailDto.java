package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SyncLogDetail;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SyncLogDetailDto {
    private String type;
    private String timestamp;
    private String message;

    public SyncLogDetailDto(SyncLogDetail model) {
        DateUtil dateUtil = new DateUtil();

        this.type = model.getType();
        this.timestamp = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.message = model.getContent();
    }
}
