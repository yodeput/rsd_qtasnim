package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterDivision;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;

import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
//@EnableUniqueValidator(entityName = "MasterDivision", fieldNames = {"kode", "nama"})
public class MasterDivisionDto implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    private Long id;

    @NotNull(message = "kode tidak boleh null.")
    @NotEmpty(message = "kode harus diisi.")
    @Size(min = 1, max = 50, message = "panjang kode harus diantara {min} dan {max}")
//    @Unique(fieldName = "kode", entityName = "MasterDivision", message = "Nama sudah ada.")
    private String kode;

    @NotNull(message = "kodeHris tidak boleh null.")
    @NotEmpty(message = "kodeHris harus diisi.")
    @Size(min = 1, max = 50, message = "panjang kodeHris harus diantara {min} dan {max}")
    private String kodeHris;

    @NotNull(message = "nama tidak boleh null.")
    @NotEmpty(message = "nama harus diisi.")
    @Size(min = 1, max = 255, message = "panjang nama harus diantara {min} dan {max}")
//    @Unique(fieldName = "nama", entityName = "MasterDivision", message = "Nama sudah ada.")
    private String nama;

    @NotNull(message = "level tidak boleh null")
    @Digits(integer = 10, fraction = 0, message = "level harus berupa angka.")
    private Integer level;

    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private MasterDivisionDto parent;
    private Long idParent;
    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    public MasterDivisionDto() {
    }

    public MasterDivisionDto(MasterDivision model) {
        DateUtil dateUtil = new DateUtil();

        this.isActive = model.getIsActive();
        this.level = model.getLevel();
        this.id = model.getId();
        this.parent = Optional.ofNullable(model.getIdParent()).map(MasterDivisionDto::new).orElse(null);
        this.idParent = Optional.ofNullable(model.getIdParent()).map(q->q.getId()).orElse(null);
        this.kode = model.getKode();
        this.nama = model.getNama();
        this.kodeHris = model.getKodeHris();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}