package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterLemariArsip;
import com.qtasnim.eoffice.db.MasterTrapArsip;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.db.constraints.ISODateAfter;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class MasterTrapArsipDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private MasterLemariArsipDto lemariArsip;
    private Long idLemariArsip;

//    @NotNull(message = "kode tidak boleh null")
    private String kode;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    public MasterTrapArsipDto() {
    }

    public MasterTrapArsipDto(MasterTrapArsip model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.lemariArsip = Optional.ofNullable(model.getLemariArsip()).map(MasterLemariArsipDto::new).orElse(null);
        this.idLemariArsip = Optional.ofNullable(model.getLemariArsip()).map(q->q.getId()).orElse(null);
        this.kode = model.getKode();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}