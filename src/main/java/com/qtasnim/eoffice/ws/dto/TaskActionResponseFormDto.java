package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class TaskActionResponseFormDto {
    private String fieldName;
    private String fieldText;
    private String fieldType;
    private Integer fieldIndex;
    private Boolean isOptional;
    private String fieldValue;
}
