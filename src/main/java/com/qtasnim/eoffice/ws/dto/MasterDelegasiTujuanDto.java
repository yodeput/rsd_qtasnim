package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterDelegasi;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.db.constraints.ISODateAfter;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class MasterDelegasiTujuanDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    @NotNull(message = "tipe tidak boleh null.")
    @NotEmpty(message = "tipe harus diisi.")
    @Size(min = 1, max = 10, message = "panjang tipe harus diantara {min} dan {max}")
    private String tipe;

    private MasterStrukturOrganisasiDto to;
    private Long organisasi_to;

    public MasterDelegasiTujuanDto() {
    }

    public MasterDelegasiTujuanDto(Long id, String tipe, MasterStrukturOrganisasiDto to, Long organisasi_to) {
        this.id = id;
        this.tipe = tipe;
        this.to = to;
        this.organisasi_to = organisasi_to;
    }
}
