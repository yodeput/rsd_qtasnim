package com.qtasnim.eoffice.ws.dto;

import java.io.Serializable;
import java.util.Optional;

import com.qtasnim.eoffice.db.MasterTingkatPerkembangan;
import com.qtasnim.eoffice.db.constraints.CompanyId;
import com.qtasnim.eoffice.db.constraints.EnableUniqueValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.db.constraints.Unique;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@EnableUniqueValidator(entityName = "MasterTingkatPerkembangan", fieldNames = {"name"})
public class MasterTingkatPerkembanganDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "nama tidak boleh null")
    @NotEmpty(message = "nama tidak boleh kosong")
    @Unique(fieldName = "name", entityName = "MasterTingkatPerkembangan", message = "Nama sudah ada.")
    private String name;

    @NotNull(message = "companyId tidak boleh null")
    @CompanyId
    private Long idCompany;
    private CompanyCodeDto companyCode;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;
    private String endDate;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public MasterTingkatPerkembanganDto(){}

    public MasterTingkatPerkembanganDto(MasterTingkatPerkembangan model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.name = model.getName();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}