package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.services.PemeriksaSuratService;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.inject.Inject;
import javax.swing.text.html.Option;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@Data
public class SuratDto implements IDynamicFormDto, IWorkflowDto {
    private static final long serialVersionUID = 1L;

    private Long id;
    private CompanyCodeDto companyCode;
    @NotNull(message = "Company id tidak boleh null.")
    private Long idCompany;
    private FormDefinitionDto formDefinition;
    @NotNull(message = "Jenis Dokumen tidak boleh null.")
    private Long idFormDefinition;
    private SuratDto parent;
    private Long idParent;
    private String noDokumen;
    private String noAgenda;
    private String status;
    private MasterStrukturOrganisasiDto konseptor;
    @NotNull(message = "idKonseptor tidak boleh null.")
    private Long idKonseptor;
    private String submittedDate;
    @NotNull(message = "Status hapus tidak boleh null")
    private Boolean isDeleted;
    private Boolean isPemeriksaParalel;
    private Boolean isNeedPublishByUnitDokumen = false;
    private Boolean isMultiPemeriksa = true;
    private Boolean isPemeriksaMandatory = false;
    private Boolean isIncludePejabatMengetahui = false;
    private Boolean isRegulasi = false;
    private Integer jumlahPenandatangan = 1;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private List<FormValueDto> formValueList;
    private List<Long> formValueIds;
    private List<Long> referensiId;
    private List<PemeriksaSuratDto> pemeriksaSurat;
    private List<PenandatanganSuratDto> penandatanganSurat;
    private List<TembusanSuratDto> tembusanSurat;
    private List<PenerimaSuratDto> penerimaSurat;
    private List<SuratDisposisiDto> suratDisposisi;
    private List<MetadataValueDto> metadata;
    private List<MengetahuiSuratDto> mengetahuiSurat;

    private String workflowAction;
    private String workflowFormName;
    private List<String> workflowCurrentTasks;
    private String activeTask;
    private Object workflowResponse;
    private String workflowNote;
    private String folderArsip;
    private Long idArsip;
    private Long idSuratBalas;
    private Long idJabatan;
    private Boolean isKonsepEdited = false;
    private String approvedDate;
    private List<SuratUndanganDto> undangans;
    private String statusNavigasi = Constants.STATUS_NAVIGASI_SIMPAN_OLEH_KONSEPTOR;
    private Boolean isPinjam;
    private String passphrase;
    private MasterUserDto konseptorUser;
    private String delegasiType;
    private Boolean bonNomor;

    private SuratInteractionDto interaction;

    public SuratDto(){
        formValueList = new ArrayList<>();
        formValueIds = new ArrayList<>();
        pemeriksaSurat = new ArrayList<>();
        penandatanganSurat = new ArrayList<>();
        tembusanSurat = new ArrayList<>();
        mengetahuiSurat = new ArrayList<>();
        penerimaSurat = new ArrayList<>();
        suratDisposisi = new ArrayList<>();
        undangans = new ArrayList<>();
        metadata = new ArrayList<>();
    }

    public SuratDto(Surat model) {
        this(model, true);
    }

    public SuratDto(Surat model, boolean includeRelation) {
        this();

        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany =  Optional.ofNullable(model.getCompanyCode()).map(CompanyCode::getId).orElse(null);
        this.formDefinition = Optional.ofNullable(model.getFormDefinition()).map(FormDefinitionDto::new).orElse(null);
        this.idFormDefinition = Optional.ofNullable(model.getFormDefinition()).map(FormDefinition::getId).orElse(null);
        this.parent = Optional.ofNullable(model.getParent()).map(SuratDto::new).orElse(null);
        this.idParent = Optional.ofNullable(model.getParent()).map(Surat::getId).orElse(null);
        this.noDokumen = Optional.ofNullable(model.getNoDokumen()).orElse(null);
        this.noAgenda = Optional.ofNullable(model.getNoAgenda()).orElse(null);
        this.status = model.getStatus();
        this.konseptor = Optional.ofNullable(model.getKonseptor()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idKonseptor = Optional.ofNullable(model.getKonseptor()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null);
        this.konseptorUser = Optional.ofNullable(model.getUserKonseptor()).map(MasterUserDto::new).orElse(null);

        if(model.getSubmittedDate()!=null) {
            this.submittedDate = Optional.ofNullable(dateUtil.getCurrentISODate(model.getSubmittedDate())).orElse(null);
        }

        this.isDeleted = model.getIsDeleted();
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = Optional.ofNullable(dateUtil.getCurrentISODate(model.getModifiedDate())).orElse(null);

        if (includeRelation) {
            this.formValueList = model.getFormValue().stream().map(FormValueDto::new).collect(Collectors.toList());
            this.formValueIds = model.getFormValue().stream().map(FormValue::getId).collect(Collectors.toList());

            this.undangans = model.getUndangans().stream().map(SuratUndanganDto::new).collect(Collectors.toList());
            this.mengetahuiSurat = model.getMengetahuiSurat().stream().map(MengetahuiSuratDto::new).collect(Collectors.toList());

            bindMetadata(model);
            bindPenandatangan(model,includeRelation);
            bindPemeriksa(model,includeRelation);
            bindPenerima(model,includeRelation);
            bindTembusan(model,includeRelation);
            bindDisposisi(model);
        }

        this.isPemeriksaParalel = Optional.ofNullable(model.getIsPemeriksaParalel()).orElse(false);
        this.isNeedPublishByUnitDokumen = Optional.ofNullable(model.getIsNeedPublishByUnitDokumen()).orElse(false);
        this.isMultiPemeriksa = Optional.ofNullable(model.getIsMultiPemeriksa()).orElse(true);
        this.isPemeriksaMandatory = Optional.ofNullable(model.getIsPemeriksaMandatory()).orElse(false);
        this.isIncludePejabatMengetahui = Optional.ofNullable(model.getIsIncludePejabatMengetahui()).orElse(false);
        this.isRegulasi = Optional.ofNullable(model.getIsRegulasi()).orElse(false);
        this.jumlahPenandatangan = Optional.ofNullable(model.getJumlahPenandatangan()).orElse(1);
        this.folderArsip = model.getFolderArsip();
        this.idArsip = model.getIdArsip();
        this.isKonsepEdited = model.getIsKonsepEdited();
        this.approvedDate = Optional.ofNullable(model.getApprovedDate()).map(dateUtil::getCurrentISODate).orElse(null);
        this.statusNavigasi = model.getStatusNavigasi();
        this.isPinjam = Optional.ofNullable(model.getIsPinjam()).orElse(null);
        this.delegasiType = model.getDelegasiType();
        this.bonNomor = Optional.ofNullable(model.getBonNomor()).orElse(null);
    }

    public static SuratDto minimal(Surat model) {
        return new SuratDto(model, false);
    }

    public void bindPenandatangan(Surat model,boolean loadRelation) {
        this.penandatanganSurat = model.getPenandatanganSurat().stream().map(t -> new PenandatanganSuratDto(t,loadRelation)).collect(Collectors.toList());
    }

    public void bindDisposisi(Surat model) {
        this.suratDisposisi = model.getSuratDisposisi().stream().map(t -> new SuratDisposisiDto(t, false)).collect(Collectors.toList());
    }

    public void bindMetadata(Surat model) {
        this.metadata = model.getFormValue().stream().map(q -> {
            MetadataValueDto valueDto = new MetadataValueDto();
            valueDto.setName(q.getFormField().getMetadata().getNama());
            valueDto.setLabel(q.getFormField().getMetadata().getLabel());
            valueDto.setValue(q.getValue());
            return valueDto;
        }).collect(Collectors.toList());
    }

    public void bindPemeriksa(Surat model,boolean loadRelation){
        if(model.getPemeriksaSurat()!=null){
            this.pemeriksaSurat = model.getPemeriksaSurat().stream().map(t-> new PemeriksaSuratDto(t,loadRelation)).collect(Collectors.toList());
        }
    }

    public void bindPenerima(Surat model,boolean loadRelation){
        if(model.getPenerimaSurat()!=null){
            this.penerimaSurat = model.getPenerimaSurat().stream().map(t -> new PenerimaSuratDto(t,loadRelation)).collect(Collectors.toList());
        }
    }

    public void bindTembusan(Surat model,boolean loadRelation){
        if(model.getTembusanSurat()!=null){
            this.tembusanSurat = model.getTembusanSurat().stream().map(t -> new TembusanSuratDto(t,loadRelation)).collect(Collectors.toList());
        }
    }
}
