package com.qtasnim.eoffice.ws.dto.chat;

import com.qtasnim.eoffice.db.chat.Group;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GroupDto {
    private Long id;
    private String groupname;
    private byte[] avatar;

    public GroupDto(Group group) {
        id = group.getId();
        groupname = group.getName();
        avatar = group.getHeadphoto();
    }
}
