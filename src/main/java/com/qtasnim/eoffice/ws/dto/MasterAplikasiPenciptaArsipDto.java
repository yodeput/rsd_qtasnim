package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterAplikasiPenciptaArsip;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

@Data
public class MasterAplikasiPenciptaArsipDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private Long idCompany;
    private CompanyCodeDto companyCode;
    private String start;
    private String end;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public MasterAplikasiPenciptaArsipDto(){}

    public MasterAplikasiPenciptaArsipDto(MasterAplikasiPenciptaArsip model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.name = model.getName();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany = Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.start = dateUtil.getCurrentISODate(model.getStart());
        this.end = dateUtil.getCurrentISODate(model.getEnd());
    }
}