package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Data
public class ListStringDto {
    public ListStringDto() {
        values = new LinkedList<>();
    }

    public ListStringDto(List<String> values) {
        this.values = values;
    }

    private List<String> values;
}
