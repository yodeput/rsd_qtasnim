package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.util.List;

@Data
public class MessageDto {
    private String message;
    private String status;
    private List<FieldErrorDto> fields;

    public MessageDto(String status, String message) {
        this.message = message;
        this.status = status;
    }
}