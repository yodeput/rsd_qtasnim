package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PenerimaDistribusi;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

@Data
public class RiwayatReadDistribusiDto {
    private MasterStrukturOrganisasiDto organization;
    private String date;
    private String action;

    public RiwayatReadDistribusiDto(PenerimaDistribusi model) {
        DateUtil dateUtil = new DateUtil();

        this.organization = new MasterStrukturOrganisasiDto(model.getOrganization());
        this.date = dateUtil.getCurrentISODate(model.getReadDate());
        this.action = "Read";
    }

    public RiwayatReadDistribusiDto(){}
}
