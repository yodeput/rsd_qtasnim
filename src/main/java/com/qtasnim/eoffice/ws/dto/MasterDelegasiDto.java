package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterDelegasi;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
public class MasterDelegasiDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

//    @NotNull(message = "tipe tidak boleh null.")
//    @NotEmpty(message = "tipe harus diisi.")
//    @Size(min = 1, max = 10, message = "panjang tipe harus diantara {min} dan {max}")
    private String tipe;

    @NotNull(message = "tgl mulai tidak boleh null.")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "tgl mulai harus mengikuti format: {format}")
    private String mulai;

    @NotNull(message = "tgl selesai tidak boleh null.")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "tgl selesai harus mengikuti format: {format}")
    @ISODateAfter(field = "mulai", message = "tgl selesai tidak boleh sebelum tgl mulai")
    private String selesai;

    @NotNull(message = "alasan tidak boleh null.")
    private String alasan;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;
    private Boolean isDeleted;

    private MasterStrukturOrganisasiDto from;
    private MasterStrukturOrganisasiDto to;
    private Long organisasi_from;
    private Long organisasi_to;
    private CompanyCodeDto companyCode;
    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;
    private List<MasterDelegasiTujuanDto> tujuan;


    public MasterDelegasiDto() {
        tujuan = new ArrayList<>();
    }

    public MasterDelegasiDto(MasterDelegasi model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.from = Optional.ofNullable(model.getFrom()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.to = Optional.ofNullable(model.getTo()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.organisasi_from = Optional.ofNullable(model.getFrom()).map(q -> q.getIdOrganization()).orElse(null);
        this.organisasi_to = Optional.ofNullable(model.getTo()).map(q -> q.getIdOrganization()).orElse(null);
        this.tipe = model.getTipe();
        this.mulai = dateUtil.getCurrentISODate(model.getMulai());
        this.selesai = dateUtil.getCurrentISODate(model.getSelesai());
        this.alasan = model.getAlasan();
        this.isDeleted = model.getIsDeleted();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if (model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
    }


}
