package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

@Data
public class LoginDto {
    private String token;
    private String refreshToken;
    private String validUntil;
    private String refreshTokenValidUntil;
}
