package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterRuangArsip;
import com.qtasnim.eoffice.db.SysLog;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class SysLogDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String category;
    private String content;
    private String executorIp;
    private String executorId;
    private String docId;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;
    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    public SysLogDto() {
    }

    public SysLogDto(SysLog model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.category = model.getCategory();
        this.content = model.getContent();
        this.executorIp = model.getExecutorIp();
        this.executorId = model.getExecutorId();
        this.docId = model.getDocId();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
    }
}