package com.qtasnim.eoffice.ws.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class BerkasSidebarDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String date;
    private Long idKlas;
    private String kodeKlas;
    private String namaKlas;

    public BerkasSidebarDto() {
    }
}
