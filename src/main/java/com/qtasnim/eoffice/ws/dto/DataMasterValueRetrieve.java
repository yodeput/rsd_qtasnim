package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class DataMasterValueRetrieve {
    public DataMasterValueRetrieve() {
        values = new LinkedList<>();
    }

    public DataMasterValueRetrieve(List values) {
        this.values = values;
    }

    private List<DataMasterSelectedSourceDto> values;
}
