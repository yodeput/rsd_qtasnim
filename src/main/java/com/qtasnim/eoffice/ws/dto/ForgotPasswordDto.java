package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ForgotPassword;
import lombok.Data;

@Data
public class ForgotPasswordDto {
    private Long id;
    private String uuid;
    private Long userId;
    private Boolean isValid;

    public ForgotPasswordDto(){}

    public ForgotPasswordDto(ForgotPassword model){
        this.id = model.getId();
        this.uuid = model.getUuid();
        this.userId = model.getUserId();
        this.isValid = model.getIsValid();
    }
}
