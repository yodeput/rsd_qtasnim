package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SuratInteraction;
import com.qtasnim.eoffice.db.SuratInteractionHistory;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@Data
public class SuratInteractionHistoryDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @NotNull(message = "suratId tidak boleh null")
    private Long suratId;
    @NotNull(message = "organzationId tidak boleh null")
    private Long organzationId;
    @NotNull(message = "userId tidak boleh null")
    private Long userId;
    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;
    @NotNull(message = "isRead tidak boleh null")
    private Boolean isRead;
    @NotNull(message = "isStarred tidak boleh null")
    private Boolean isStarred;
    @NotNull(message = "isImportant tidak boleh null")
    private Boolean isImportant;
    @NotNull(message = "isFinished tidak boleh null")
    private Boolean isFinished;

    private String finishedDate;
    private String modifiedDate;
    private SuratDto surat;
    private MasterStrukturOrganisasiDto organisasi;
    private CompanyCodeDto company;
    private MasterUserDto user;


    public SuratInteractionHistoryDto() {
    }

    public SuratInteractionHistoryDto(SuratInteractionHistory model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.surat = Optional.ofNullable(model.getSurat()).map(SuratDto::new).orElse(null);
        this.suratId = Optional.ofNullable(model.getSurat()).map(q -> q.getId()).orElse(null);
        this.organisasi = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.organzationId = Optional.ofNullable(model.getOrganization()).map(q -> q.getIdOrganization()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.userId = Optional.ofNullable(model.getUser()).map(q -> q.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.isRead = model.getIsRead();
        this.isImportant = model.getIsImportant();
        this.isStarred = model.getIsStarred();
        this.isFinished = model.getIsFinished();
        this.finishedDate = dateUtil.getCurrentISODate(model.getFinishedDate());
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
    }

}
