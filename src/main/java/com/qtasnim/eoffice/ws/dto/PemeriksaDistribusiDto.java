package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PemeriksaDistribusi;
import java.util.Optional;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class PemeriksaDistribusiDto {

    private Long id;
    private Integer seq;
    
    private Long idDistribusi;
    
    @NotNull(message = "Pemeriksa tidak boleh null")
    private Long idPemeriksa;
    private MasterStrukturOrganisasiDto pemeriksa;
    
    private Long userId;
    private MasterUserDto user;

    public PemeriksaDistribusiDto() { }

    public PemeriksaDistribusiDto(PemeriksaDistribusi model) {
        this.id = model.getId();
        this.seq = model.getSeq();
        
        if (model.getDistribusi() != null) {
            this.idDistribusi = model.getDistribusi().getId();
        }
        
        Optional.ofNullable(model.getOrganization()).ifPresent(org -> {
            this.pemeriksa = new MasterStrukturOrganisasiDto(org);
            this.idPemeriksa = org.getIdOrganization();
        });
        
        if (model.getUser() != null) {
            this.user = new MasterUserDto(model.getUser());
            this.userId = model.getUser().getId();
        }
    }
}
