package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class KewenanganPenerimaDto implements Serializable {

    private List<Long> idsPenandatangan;
    private Long idJenisDokumen;

    public KewenanganPenerimaDto(){}
}
