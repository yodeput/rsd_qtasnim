package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.SuratDocLib;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Data
public class SuratDocLibDto {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String docId;
    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long idCompany;
    private SuratDto surat;
    private Long idSurat;
    private String docGeneratedName;
    private String docName;
    private Long version;
    private Long size;
    private Boolean isKonsep;
    private Boolean isPublished;
    private String mimeType;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private String number;
    private String editLink;
    private String viewLink;

    public SuratDocLibDto(SuratDocLib model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.docId = model.getDocId();
        this.number = model.getNumber();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany =  Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.surat =  Optional.ofNullable(model.getSurat()).map(SuratDto::new).orElse(null);
        this.idSurat = Optional.ofNullable(model.getSurat()).map(q->q.getId()).orElse(null);;
        this.docGeneratedName = model.getDocGeneratedName();
        this.docName = model.getDocName();
        this.version = model.getVersion();
        this.size = model.getSize();
        this.isKonsep = model.getIsKonsep();
        this.isPublished = model.getIsPublished();
        this.mimeType = model.getMimeType();
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
    }
}
