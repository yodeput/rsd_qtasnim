/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Regulasi;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author THINKPAD-PC
 */
@Data
public class RegulasiDto implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;
    private String nomorRegulasi;
    private String namaRegulasi;

    private Long regulasiPenggantiId;
    private RegulasiDto regulasiPengganti;

    private Long jenisRegulasiId;
    private MasterJenisRegulasiDto jenisRegulasi;

    private String deskripsi;
    private Boolean isRegulasiPerusahaan;
    private String tanggalMulaiBerlaku;
    private String tanggalSelesaiBerlaku;
    private String tanggalTerbit;

    private Long klasifikasiDokumenId;
    private MasterKlasifikasiMasalahDto klasifikasiDokumen;

    private String status;
    private Boolean isPublished;
    private Boolean isDeleted;

    private Long companyId;
    private CompanyCodeDto company;

    private String unitKonseptorId;
    private MasterKorsaDto unitKonseptor;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private Boolean isStarred;
    
    private List<Long> idIndukTerkaits;
    private List<RegulasiDto> regulasiIndukTerkaitList;
    
    public RegulasiDto(){}
    
    public RegulasiDto(Regulasi model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.nomorRegulasi = model.getNomorRegulasi();
        this.namaRegulasi = model.getNamaRegulasi();
        this.regulasiPengganti = Optional.ofNullable(model.getRegulasiPengganti()).map(RegulasiDto::new).orElse(null);
        this.regulasiPenggantiId = Optional.ofNullable(model.getRegulasiPengganti()).map(q -> q.getId()).orElse(null);
        this.jenisRegulasi = Optional.ofNullable(model.getJenisRegulasi()).map(MasterJenisRegulasiDto::new).orElse(null);
        this.jenisRegulasiId = Optional.ofNullable(model.getJenisRegulasi()).map(q -> q.getIdJenisRegulasi()).orElse(null);
        this.deskripsi = model.getDeskripsi();
        this.isRegulasiPerusahaan = model.getIsRegulasiPerusahaan();
        this.tanggalMulaiBerlaku = dateUtil.getCurrentISODate(model.getTanggalMulaiBerlaku());
        this.tanggalSelesaiBerlaku = dateUtil.getCurrentISODate(model.getTanggalSelesaiBerlaku());
        this.tanggalTerbit = dateUtil.getCurrentISODate(model.getTanggalTerbit());
        this.klasifikasiDokumen = Optional.ofNullable(model.getKlasifikasiDokumen()).map(MasterKlasifikasiMasalahDto::new).orElse(null);
        this.klasifikasiDokumenId = Optional.ofNullable(model.getKlasifikasiDokumen()).map(q -> q.getIdKlasifikasiMasalah()).orElse(null);
        this.status = model.getStatus();
        this.isPublished = model.getIsPublished();
        this.unitKonseptor = Optional.ofNullable(model.getUnitKonseptor()).map(MasterKorsaDto::new).orElse(null);
        this.unitKonseptorId = Optional.ofNullable(model.getUnitKonseptor()).map(q -> q.getId()).orElse(null);
        this.isDeleted = model.getIsDeleted();
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q -> q.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        this.isStarred = model.getIsStarred();
    }
}
