package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterKlasifikasiMasalah;
import com.qtasnim.eoffice.db.constraints.*;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
//@EnableUniqueValidator(entityName = "MasterKlasifikasiMasalah", fieldNames = {"kodeKlasifikasiMasalah", "namaKlasifikasiMasalah", "parent"})
public class MasterKlasifikasiMasalahDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long idKlasifikasiMasalah;

    @NotNull(message = "kode klasifikasi tidak boleh null.")
    @NotEmpty(message = "kode klasifikasi harus diisi.")
    @Size(min = 1, max = 10, message = "panjang kode klasifikasi harus diantara {min} dan {max}")
//    @Unique(fieldName = "kodeKlasifikasiMasalah", entityName = "MasterKlasifikasiMasalah", message = "Nama sudah ada.")
    private String kodeKlasifikasiMasalah;

    @NotNull(message = "nama klasifikasi tidak boleh null.")
    @NotEmpty(message = "nama klasifikasi harus diisi.")
    @Size(min = 1, max = 255, message = "panjang nama klasifikasi harus diantara {min} dan {max}")
//    @Unique(fieldName = "namaKlasifikasiMasalah", entityName = "MasterKlasifikasiMasalah", message = "Nama sudah ada.")
    private String namaKlasifikasiMasalah;

//    @ISODate(format = DateUtil.DATE_FORMAT, message = "tanggal retensi aktif harus mengikuti format: {format}")
    private String tglRetensiAktif;

//    @ISODate(format = DateUtil.DATE_FORMAT, message = "tanggal retensi inAktif harus mengikuti format: {format}")
//    @ISODateAfter(field = "tglRetensiAktif", message = "tgl retensi inAktif tidak boleh sebelum tgl retensi aktif")
    private String tglRetensiInaktif;

    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private MasterKlasifikasiMasalahDto parent;
    private Long idParent;
    private CompanyCodeDto companyCode;
    private Long companyId;

    private Long idStatusAkhirArsip;
    private MasterStatusAkhirArsipDto statusAkhir;
    private Integer retensiAktifYear;
    private Integer retensiInaktifYear;

    private Boolean hasChild;

    public MasterKlasifikasiMasalahDto(){}

    public MasterKlasifikasiMasalahDto(MasterKlasifikasiMasalah model) {
        DateUtil dateUtil = new DateUtil();
        this.idKlasifikasiMasalah = model.getIdKlasifikasiMasalah();
        this.kodeKlasifikasiMasalah = model.getKodeKlasifikasiMasalah();
        this.namaKlasifikasiMasalah = model.getNamaKlasifikasiMasalah();
        this.parent = Optional.ofNullable(model.getParent()).map(MasterKlasifikasiMasalahDto::new).orElse(null);
        this.idParent = Optional.ofNullable(model.getParent()).map(q->q.getIdKlasifikasiMasalah()).orElse(null);
        this.tglRetensiAktif = dateUtil.getCurrentISODate(model.getTglRetensiAktif());
        this.tglRetensiInaktif = dateUtil.getCurrentISODate(model.getTglRetensiInaktif());
        this.isActive = model.getIsActive();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.statusAkhir = Optional.ofNullable(model.getStatusAkhir()).map(MasterStatusAkhirArsipDto::new).orElse(null);
        this.idStatusAkhirArsip = Optional.ofNullable(model.getStatusAkhir()).map(q->q.getId()).orElse(null);
        this.retensiAktifYear = model.getRetensiAktifYear();
        this.retensiInaktifYear = model.getRetensiInaktifYear();
    }
}
