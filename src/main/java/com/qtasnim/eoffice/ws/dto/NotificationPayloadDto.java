package com.qtasnim.eoffice.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NotificationPayloadDto {
    private String date;
    private String type;
    private String title;
    private String message;
    private String transactionId;
}
