package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.services.FileService;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Optional;

@Data
public class JenisDokumenDocLibDto {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String docId;
    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long idCompany;
    private Long idAuthor;
    private MasterJenisDokumenDto masterJenisDokumenDto;
    private MasterUserDto author;
    private Long idJenisDokumen;
    private String docGeneratedName;
    private String docName;
    private Double version;
    private String cmisVersion;
    private Long size;
    private String mimeType;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private String number;
    private String editLink;
    private String viewLink;
    private String downloadLink;
    private String start;
    private String activationDate;
    private String end;
    private Boolean isActive = true;

    public JenisDokumenDocLibDto() {

    }

    public JenisDokumenDocLibDto(JenisDokumenDocLib model){
        this(model, null);
    }

    public JenisDokumenDocLibDto(JenisDokumenDocLib model, FileService fileService){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.docId = model.getDocId();
        this.number = model.getNumber();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.author = Optional.ofNullable(model.getAuthor()).map(MasterUserDto::new).orElse(null);
        this.idAuthor = Optional.ofNullable(model.getAuthor()).map(MasterUser::getId).orElse(null);
        this.idCompany =  Optional.ofNullable(model.getCompanyCode()).map(CompanyCode::getId).orElse(null);
        this.masterJenisDokumenDto =  Optional.ofNullable(model.getJenisDokumen()).map(MasterJenisDokumenDto::new).orElse(null);
        this.idJenisDokumen = Optional.ofNullable(model.getJenisDokumen()).map(MasterJenisDokumen::getIdJenisDokumen).orElse(null);;
        this.docGeneratedName = model.getDocGeneratedName();
        this.docName = model.getDocName();
        this.version = model.getVersion();
        this.cmisVersion = model.getCmisVersion();
        this.size = model.getSize();
        this.mimeType = model.getMimeType();
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        this.start = Optional.ofNullable(model.getStart()).map(dateUtil::getCurrentISODate).orElse(null);
        this.end = Optional.ofNullable(model.getEnd()).map(dateUtil::getCurrentISODate).orElse(null);
        this.activationDate = this.start;
        this.isActive = model.getIsActive();

        if(model.getModifiedDate() != null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }

        if (fileService != null) {
            String extension = Optional.of(docName).filter(t -> t.contains(".")).map(FileUtility::GetFileExtension).orElse("docx");

            this.editLink = fileService.getLayoutingLink(Optional.ofNullable(this.idJenisDokumen).map(Object::toString).orElse("-1"), this.cmisVersion);
            this.viewLink = fileService.getViewLink(docId, extension, this.cmisVersion);
            this.downloadLink = fileService.getViewLink(docId, false, this.cmisVersion);
        }
    }
}
