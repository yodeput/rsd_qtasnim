package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.OrganisasiHistory;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
public class OrganisasiHistoryDto {

    private Long id;
    private Integer level;
    private String parnt;
    private String orgeh;
    private String ouabv;
    private String outxt;
    private String persa;
    private String btrtl;
    private String begda_o;
    private String endda_o;
    private String plans;
    private String psabv;
    private String pstxt;
    private String trfgb;
    private String trfg1;
    private String trgs1;
    private String chief;
    private String sttus;

    private String begda_s;
    private String endda_s;

    private String pernr;
    private String cname;
    private String begda_p;
    private String endda_p;

    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;

    public OrganisasiHistoryDto(OrganisasiHistory model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.level = model.getLevel();
        this.parnt = model.getParnt();
        this.orgeh = model.getOrgeh();
        this.ouabv = model.getOuabv();
        this.outxt = model.getOutxt();
        this.persa = model.getPersa();
        this.btrtl = model.getBtrtl();
        this.begda_o = dateUtil.getCurrentISODate(model.getBegda_o());
        this.endda_o = dateUtil.getCurrentISODate(model.getEndda_o());
        this.plans = model.getPlans();
        this.psabv = model.getPsabv();
        this.pstxt = model.getPstxt();
        this.trfgb = model.getTrfgb();
        this.trfg1 = model.getTrfg1();
        this.trgs1 = model.getTrgs1();
        this.chief = model.getChief();
        this.sttus = model.getSttus();
        this.begda_s = dateUtil.getCurrentISODate(model.getBegda_s());
        this.endda_s = dateUtil.getCurrentISODate(model.getEndda_s());
        this.pernr = model.getPernr();
        this.cname = model.getCname();
        this.begda_p = dateUtil.getCurrentISODate(model.getBegda_p());
        this.endda_p = dateUtil.getCurrentISODate(model.getEndda_p());
        this.createdBy = model.getCreatedBy();
        this.createdDate = model.getCreatedDate();
        this.modifiedBy = model.getModifiedBy();
        this.modifiedDate = model.getModifiedDate();
    }

}