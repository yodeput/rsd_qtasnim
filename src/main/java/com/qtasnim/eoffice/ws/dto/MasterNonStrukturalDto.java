package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterLemariArsip;
import com.qtasnim.eoffice.db.MasterNonStruktural;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.db.constraints.ISODateAfter;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Optional;

@Data
@EnableISODateAfterValidator
public class MasterNonStrukturalDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private MasterUserDto userDto;

    @NotNull(message = "userId tidak boleh null.")
    @NotEmpty(message = "userId harus diisi.")
    private Long userId;

    private MasterStrukturOrganisasiDto organisasiDto;

    @NotNull(message = "organisasiId tidak boleh null.")
    @NotEmpty(message = "organisasiId harus diisi.")
    private Long organisasiId;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;


    public MasterNonStrukturalDto() {
    }

    public MasterNonStrukturalDto(MasterNonStruktural model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();

        this.userDto = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.userId = Optional.ofNullable(model.getUser()).map(q->q.getId()).orElse(null);
        this.organisasiDto = Optional.ofNullable(model.getOrganisasi()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.organisasiId = Optional.ofNullable(model.getOrganisasi()).map(q->q.getIdOrganization()).orElse(null);
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
    }
}