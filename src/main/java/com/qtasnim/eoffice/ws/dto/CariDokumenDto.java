package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class CariDokumenDto implements IDynamicFormDto, IWorkflowDto {
    private static final long serialVersionUID = 1L;

    private Long id;
    private CompanyCodeDto companyCode;
    @NotNull(message = "Company id tidak boleh null.")
    private Long idCompany;
    private FormDefinitionDto formDefinition;
    @NotNull(message = "Jenis Dokumen tidak boleh null.")
    private Long idFormDefinition;
    private CariDokumenDto parent;
    private Long idParent;
    private String noDokumen;
    private String noAgenda;
    private String status;
    private MasterStrukturOrganisasiDto konseptor;
    private MasterUserDto konseptorUser;
    private Long idKonseptorUser;
    private String delegasiType;
    @NotNull(message = "idKonseptor tidak boleh null.")
    private Long idKonseptor;
    private String submittedDate;
    @NotNull(message = "Status hapus tidak boleh null")
    private Boolean isDeleted;
    private Boolean isPemeriksaParalel;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private List<FormValueDto> formValueList;
    private List<Long> formValueIds;
    private List<Long> referensiId;
    private List<PemeriksaSuratDto> pemeriksaSurat;
    private List<PenandatanganSuratDto> penandatanganSurat;
    private List<TembusanSuratDto> tembusanSurat;
    private List<PenerimaSuratDto> penerimaSurat;
    private List<MetadataValueDto> metadata;

    private String workflowAction;
    private String workflowFormName;
    private List<String> workflowCurrentTasks;
    private String activeTask;
    private Object workflowResponse;
    private String workflowNote;
    private String folderArsip;
    private Long idArsip;
    private Long idSuratBalas;
    private Long idJabatan;
    private Boolean isKonsepEdited = false;
    private String approvedDate;
    private List<SuratUndanganDto> undangans;
    private String statusNavigasi = Constants.STATUS_NAVIGASI_SIMPAN_OLEH_KONSEPTOR;
    private Boolean isPinjam;
    private Boolean isOtherUnit;
    private MasterKorsaDto korsaOther;

    private SuratInteractionDto interaction;

    public CariDokumenDto(){
        formValueList = new ArrayList<>();
        formValueIds = new ArrayList<>();
        pemeriksaSurat = new ArrayList<>();
        penandatanganSurat = new ArrayList<>();
        tembusanSurat = new ArrayList<>();
        penerimaSurat = new ArrayList<>();
        undangans = new ArrayList<>();
    }

    public CariDokumenDto(Surat model) {
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.idCompany =  Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.formDefinition = Optional.ofNullable(model.getFormDefinition()).map(FormDefinitionDto::new).orElse(null);
        this.idFormDefinition = Optional.ofNullable(model.getFormDefinition()).map(a->a.getId()).orElse(null);
        this.parent = Optional.ofNullable(model.getParent()).map(CariDokumenDto::new).orElse(null);
        this.idParent = Optional.ofNullable(model.getParent()).map(q->q.getId()).orElse(null);
        this.noDokumen = Optional.ofNullable(model.getNoDokumen()).orElse(null);
        this.noAgenda = Optional.ofNullable(model.getNoAgenda()).orElse(null);
        this.status = model.getStatus();
        this.konseptor = Optional.ofNullable(model.getKonseptor()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idKonseptor = Optional.ofNullable(model.getKonseptor()).map(q->q.getIdOrganization()).orElse(null);
        this.konseptorUser = Optional.ofNullable(model.getUserKonseptor()).map(MasterUserDto::new).orElse(null);
        this.idKonseptorUser = Optional.ofNullable(model.getUserKonseptor()).map(q->q.getId()).orElse(null);
        this.delegasiType = model.getDelegasiType();

        if(model.getSubmittedDate()!=null) {
            this.submittedDate = Optional.ofNullable(dateUtil.getCurrentISODate(model.getSubmittedDate())).orElse(null);
        }

        this.isDeleted = model.getIsDeleted();
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.formValueList = model.getFormValue().stream().map(q->new FormValueDto(q)).collect(Collectors.toList());
        this.formValueIds = model.getFormValue().stream().map(q->q.getId()).collect(Collectors.toList());
        this.metadata = model.getFormValue().stream().map(q -> {
            MetadataValueDto valueDto = new MetadataValueDto();
            valueDto.setName(q.getFormField().getMetadata().getNama());
            valueDto.setLabel(q.getFormField().getMetadata().getLabel());
            valueDto.setValue(q.getValue());
            return valueDto;
        }).collect(Collectors.toList());

        if(model.getPenandatanganSurat()!=null){
            this.penandatanganSurat = model.getPenandatanganSurat().stream().map(q->new PenandatanganSuratDto(q)).collect(Collectors.toList());
        }

        if(model.getPemeriksaSurat()!=null){
            this.pemeriksaSurat = model.getPemeriksaSurat().stream().map(q->new PemeriksaSuratDto(q,true)).collect(Collectors.toList());
        }

        if(model.getPenerimaSurat()!=null){
            this.penerimaSurat = model.getPenerimaSurat().stream().map(q->new PenerimaSuratDto(q)).collect(Collectors.toList());
        }

        if(model.getTembusanSurat()!=null){
            this.tembusanSurat = model.getTembusanSurat().stream().map(q->new TembusanSuratDto(q)).collect(Collectors.toList());
        }

        this.isPemeriksaParalel = model.getIsPemeriksaParalel();

        this.folderArsip = model.getFolderArsip();
        this.idArsip = model.getIdArsip();
        this.isKonsepEdited = model.getIsKonsepEdited();
        this.approvedDate = Optional.ofNullable(model.getApprovedDate()).map(dateUtil::getCurrentISODate).orElse(null);

        this.undangans = model.getUndangans().stream().map(SuratUndanganDto::new).collect(Collectors.toList());
        this.statusNavigasi = model.getStatusNavigasi();
        this.isPinjam = Optional.ofNullable(model.getIsPinjam()).orElse(null);
    }
}
