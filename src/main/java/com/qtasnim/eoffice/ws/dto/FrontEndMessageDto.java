package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.util.List;

@Data
public class FrontEndMessageDto {
    private List<String> recipients;
    private String message;
}
