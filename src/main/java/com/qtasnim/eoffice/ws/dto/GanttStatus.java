package com.qtasnim.eoffice.ws.dto;

public enum GanttStatus {
    NOTSTARTED, ONPROGRESS, COMPLETED, LATE
}
