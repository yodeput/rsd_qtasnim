package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class TextExtractionDto {
    private static final long serialVersionUID = 1L;

    private String fileName;
    private Integer pageCount;
    private String mimeType;
    private List<String> texts;

    public TextExtractionDto() {
        fileName = "";
        pageCount = 0;
        mimeType = "";
        texts = new LinkedList();
    }
}
