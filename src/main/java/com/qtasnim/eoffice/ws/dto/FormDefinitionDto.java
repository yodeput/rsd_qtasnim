package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.FormDefinition;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
public class FormDefinitionDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    
    @NotNull(message = "nama tidak boleh null.")
    private String name;

    @NotNull(message = "description tidak boleh null.")
    private String description;

    @NotNull(message = "version tidak boleh null")
    @Digits(integer = 10, fraction = 3, message = "version harus berupa angka.")
    private Double version;

    @NotNull(message = "isActive tidak boleh null")
    private Boolean isActive;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    private MasterJenisDokumenDto jenisDokumen;

    @NotNull(message = "jenisDokumenId tidak boleh null")
    private Long jenisDokumenId;
    
    private List<FormFieldDto> fields;
    private List<Long> fieldIds;

    public FormDefinitionDto(){
        fields = new ArrayList<>();
        fieldIds = new ArrayList<>();
    }

    public FormDefinitionDto(FormDefinition model) {
        DateUtil dateUtil = new DateUtil();

        this.version = model.getVersion();
        this.id = model.getId();
        this.name = model.getName();
        this.jenisDokumen = Optional.ofNullable(model.getJenisDokumen()).map(MasterJenisDokumenDto::new).orElse(null);
        this.jenisDokumenId = Optional.ofNullable(model.getJenisDokumen()).map(q->q.getIdJenisDokumen()).orElse(null);
        this.description = model.getDescription();
        this.isActive = model.getIsActive();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());

        if(model.getFormFields()!=null){
            this.fields = model.getFormFields().stream().map(q -> new FormFieldDto(q)).collect(Collectors.toList());
            this.fieldIds = model.getFormFields().stream().map(q -> q.getId()).collect(Collectors.toList());
        }

    }

}
