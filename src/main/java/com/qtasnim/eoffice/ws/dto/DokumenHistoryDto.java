package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.DokumenHistory;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

@Data
public class DokumenHistoryDto implements Serializable {
    private Long id;
    private String referenceTable;
    private Long referenceId;
    private String status;
    private String actionDate;
    private Long idOrganisasi;
    private MasterStrukturOrganisasiDto organisasi;
    private Long idUser;
    private MasterUserDto user;
    private String delegasiType;

    public DokumenHistoryDto(DokumenHistory model){
        DateUtil dateUtil = new DateUtil();
        this.id = model.getId();
        this.actionDate = dateUtil.getCurrentISODate(model.getActionDate());
        this.referenceTable = model.getReferenceTable();
        this.referenceId = model.getReferenceId();
        this.status = model.getStatus();
        this.idOrganisasi = Optional.ofNullable(model.getOrganization()).map(a->a.getIdOrganization()).orElse(null);
        this.organisasi = Optional.ofNullable(model.getOrganization()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idUser = Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.delegasiType = model.getDelegasiType();
    }

    public DokumenHistoryDto(){}
}
