package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.PermohonanBonNomor;
import com.qtasnim.eoffice.db.PermohonanDokumen;
import com.qtasnim.eoffice.db.constraints.EnableISODateAfterValidator;
import com.qtasnim.eoffice.db.constraints.ISODate;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@EnableISODateAfterValidator
public class PermohonanBonNomorDto {

    private Long id;

    private String noRegistrasi;

    @ISODate(format = DateUtil.DATE_FORMAT, message = "tglTarget harus mengikuti format: {format}")
    private String tglTarget;

    private Long idOrgKonseptor;
    private MasterStrukturOrganisasiDto orgKoseptor;

    private Long idUserKonseptor;
    private MasterUserDto userKonseptor;

    private Long idJenisDokumen;
    private MasterJenisDokumenDto jenisDokumen;

    @NotNull(message = "Perihal tidak boleh null")
    @NotEmpty(message = "Perihal tidak boleh kosong")
    private String perihal;

    private Integer status;

    @NotNull(message = "Company harus dipilih")
    private Long idCompany;
    private CompanyCodeDto company;

    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    private Boolean isDeleted;

    private List<MasterStrukturOrganisasiDto> pemeriksaList;
    private List<Long>pemeriksaIds;
    private List<MasterStrukturOrganisasiDto> penandatanganList;
    private List<Long> penandatanganIds;

    private List<PemeriksaBonNomorDto> pemeriksaBon;
    private List<PenandatanganBonNomorDto> penandatanganBon;

    public PermohonanBonNomorDto(){
        pemeriksaBon = new ArrayList<>();
        penandatanganBon = new ArrayList<>();

        pemeriksaList = new ArrayList<>();
        pemeriksaIds = new ArrayList<>();
        penandatanganList = new ArrayList<>();
        penandatanganIds = new ArrayList<>();
    }

    public PermohonanBonNomorDto(PermohonanBonNomor model){
        DateUtil dateUtil = new DateUtil();
        this.id= model.getId();
        this.noRegistrasi = model.getNoRegistrasi();
        if(model.getTglTarget()!=null) {
            this.tglTarget = dateUtil.getCurrentISODate(model.getTglTarget());
        }
        this.idOrgKonseptor = Optional.ofNullable(model.getOrgKonseptor()).map(a->a.getIdOrganization()).orElse(null);
        this.orgKoseptor = Optional.ofNullable(model.getOrgKonseptor()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.idUserKonseptor = Optional.ofNullable(model.getUserKonseptor()).map(a->a.getId()).orElse(null);
        this.userKonseptor = Optional.ofNullable(model.getUserKonseptor()).map(MasterUserDto::new).orElse(null);
        this.idJenisDokumen = Optional.ofNullable(model.getJenisDokumen()).map(a->a.getIdJenisDokumen()).orElse(null);
        this.jenisDokumen = Optional.ofNullable(model.getJenisDokumen()).map(MasterJenisDokumenDto::new).orElse(null);
        this.perihal = model.getPerihal();
        this.status = model.getStatus();
        this.idCompany =  Optional.ofNullable(model.getCompanyCode()).map(a->a.getId()).orElse(null);
        this.company = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.createdBy = model.getCreatedBy();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedBy = model.getModifiedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.isDeleted = model.getIsDeleted();

        if(model.getPemeriksaList()!=null) {
            this.pemeriksaList = model.getPemeriksaList().stream().map(q -> new MasterStrukturOrganisasiDto(q)).collect(Collectors.toList());
            this.pemeriksaIds = model.getPemeriksaList().stream().map(q -> q.getIdOrganization()).collect(Collectors.toList());
        }

        if(model.getPenandatanganList()!=null) {
            this.penandatanganList = model.getPenandatanganList().stream().map(q -> new MasterStrukturOrganisasiDto(q)).collect(Collectors.toList());
            this.penandatanganIds = model.getPenandatanganList().stream().map(q -> q.getIdOrganization()).collect(Collectors.toList());
        }

    }
}
