package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.DistribusiComment;
import com.qtasnim.eoffice.db.DistribusiDokumen;
import com.qtasnim.eoffice.util.DateUtil;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

@Data
public class DistribusiCommentDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String komentar;
    private DistribusiDokumenDto distribusi;
    private Long idDistribusi;
    private MasterUserDto user;
    private Boolean isDeleted;
    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    public DistribusiCommentDto() {
    }

    public DistribusiCommentDto(DistribusiComment model) {
        DateUtil dateUtil = new DateUtil();

        this.id = model.getId();
        this.komentar = model.getKomentar();
        this.distribusi = Optional.ofNullable(model.getDistribusi()).map(DistribusiDokumenDto::new).orElse(null);
        this.idDistribusi = Optional.ofNullable(model.getDistribusi()).map(q->q.getId()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.isDeleted = model.getIsDeleted();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        this.createdBy = model.getCreatedBy();
        this.modifiedBy = model.getModifiedBy();
    }
}