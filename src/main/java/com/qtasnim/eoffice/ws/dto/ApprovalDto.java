package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ApprovalDto {

    @NotNull(message = "id tidak boleh null")
    private Long id;
    @NotNull(message = "action tidak boleh null.")
    @NotEmpty(message = "action harus diisi.")
    private String action;
    private String note;

    public ApprovalDto(){}
}
