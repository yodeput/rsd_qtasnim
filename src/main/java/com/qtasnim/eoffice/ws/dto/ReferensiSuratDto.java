package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.ReferensiSurat;
import lombok.Data;

import java.util.Optional;

@Data
public class ReferensiSuratDto {
    private Long id;
    private SuratDto surat;
    private Long suratId;
    private MasterStrukturOrganisasiDto organization;
    private Long organizationId;
    private MasterUserDto user;
    private Long userId;


    public ReferensiSuratDto(){}

    public ReferensiSuratDto(ReferensiSurat model){
        this.id = model.getId();
        this.surat = Optional.ofNullable(model.getSurat()).map(SuratDto::new).orElse(null);
        this.suratId = Optional.ofNullable(model.getSurat()).map(a->a.getId()).orElse(null);
        this.organization = Optional.ofNullable(model.getOrganisasi()).map(MasterStrukturOrganisasiDto::new).orElse(null);
        this.organizationId = Optional.ofNullable(model.getOrganisasi()).map(a->a.getIdOrganization()).orElse(null);
        this.user = Optional.ofNullable(model.getUser()).map(MasterUserDto::new).orElse(null);
        this.userId = Optional.ofNullable(model.getUser()).map(a->a.getId()).orElse(null);
    }
}
