package com.qtasnim.eoffice.ws.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.ws.rs.QueryParam;

@Data
public class LoginParam {
  @QueryParam("username")
  @NotNull
  private String username;

  @QueryParam("password")
  @NotNull
  private String password;
}
