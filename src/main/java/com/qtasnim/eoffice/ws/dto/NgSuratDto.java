package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.Surat;
import lombok.Data;

import java.util.Optional;

@Data
public class NgSuratDto extends SuratDto {
    private static final long serialVersionUID = 1L;
    private String editLink;
    private String viewLink;

    public NgSuratDto() {}

    public NgSuratDto(Surat surat) {
        super(surat);
    }

    public NgSuratDto(SuratDto suratDto) {
        setId(suratDto.getId());
        setCompanyCode(suratDto.getCompanyCode());
        setIdCompany(suratDto.getIdCompany());
        setFormDefinition(suratDto.getFormDefinition());
        setIdFormDefinition(suratDto.getIdFormDefinition());
        setParent(suratDto.getParent());
        setIdParent(suratDto.getIdParent());
        setNoDokumen(suratDto.getNoDokumen());
        setStatus(suratDto.getStatus());
        setKonseptor(suratDto.getKonseptor());
        setIdKonseptor(suratDto.getIdKonseptor());
        setSubmittedDate(suratDto.getSubmittedDate());
        setIsDeleted(suratDto.getIsDeleted());
        setIsPemeriksaParalel(Optional.ofNullable(suratDto.getIsPemeriksaParalel()).orElse(false));
        setCreatedBy(suratDto.getCreatedBy());
        setCreatedDate(suratDto.getCreatedDate());
        setModifiedBy(suratDto.getModifiedBy());
        setModifiedDate(suratDto.getModifiedDate());
        setFormValueList(suratDto.getFormValueList());
        setFormValueIds(suratDto.getFormValueIds());
//        setPemeriksaList(suratDto.getPemeriksaList());
//        setPemeriksaIds(suratDto.getPemeriksaIds());
//        setPenandatanganList(suratDto.getPenandatanganList());
//        setPenandatanganIds(suratDto.getPenandatanganIds());
//        setTembusanList(suratDto.getTembusanList());
//        setPenerimaList(suratDto.getPenerimaList());
//        setTembusanIds(suratDto.getTembusanIds());
        setPemeriksaSurat(suratDto.getPemeriksaSurat());
        setPenandatanganSurat(suratDto.getPenandatanganSurat());
        setTembusanSurat(suratDto.getTembusanSurat());
        setPenerimaSurat(suratDto.getPenerimaSurat());
        setMetadata(suratDto.getMetadata());
        if(suratDto.getBonNomor() != null && suratDto.getBonNomor().booleanValue()){
            setApprovedDate(suratDto.getApprovedDate());
            setBonNomor(suratDto.getBonNomor());
        }
    }
}
