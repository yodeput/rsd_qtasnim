package com.qtasnim.eoffice.ws.dto;

import com.qtasnim.eoffice.db.MasterKewenangan;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.db.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
@EnableISODateAfterValidator
public class MasterKewenanganDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull(message = "grade tidak boleh null.")
    @NotEmpty(message = "grade harus diisi.")
    @Size(min = 1, max = 50, message = "panjang grade harus diantara {min} dan {max}")
    private String grade;

    @NotNull(message = "kewenangan tidak boleh null.")
    @NotEmpty(message = "kewenangan harus diisi.")
    private String kewenangan;

    @NotNull(message = "startDate tidak boleh null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "startDate harus mengikuti format: {format}")
    private String startDate;

//    @NotNull(message = "endDate tidak boleh null")
//    @ISODate(format = DateUtil.DATE_FORMAT, message = "endDate harus mengikuti format: {format}")
//    @ISODateAfter(field = "startDate", message = "endDate tidak boleh sebelum startDate")
    private String endDate;

    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    private CompanyCodeDto companyCode;

    @NotNull(message = "companyId tidak boleh null")
    private Long companyId;

    private Boolean isDeleted;

    public MasterKewenanganDto() {
    }

    public MasterKewenanganDto(MasterKewenangan model) {
        this.id = model.getId();
        this.grade = model.getGrade();
        this.kewenangan = model.getKewenangan();
        this.companyCode = Optional.ofNullable(model.getCompanyCode()).map(CompanyCodeDto::new).orElse(null);
        this.companyId = Optional.ofNullable(model.getCompanyCode()).map(q->q.getId()).orElse(null);

        DateUtil dateUtil = new DateUtil();
        this.createdDate = dateUtil.getCurrentISODate(model.getCreatedDate());
        this.createdBy = model.getCreatedBy();
        if(model.getModifiedDate()!=null) {
            this.modifiedDate = dateUtil.getCurrentISODate(model.getModifiedDate());
        }
        this.modifiedBy = model.getModifiedBy();
        this.startDate = dateUtil.getCurrentISODate(model.getStart());
        this.endDate = dateUtil.getCurrentISODate(model.getEnd());
        this.isDeleted = model.getIsDeleted();
    }
}