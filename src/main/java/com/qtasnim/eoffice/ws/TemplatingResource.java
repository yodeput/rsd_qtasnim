/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.TemplatingService;
import com.qtasnim.eoffice.ws.dto.TemplatingDto;
import id.kai.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author acdwisu
 */

@Path("templating")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Templating Dokumen",
        description = "WS Endpoint untuk menghandle templating dokumen"
)
public class TemplatingResource {
    @Inject
    private TemplatingService templatingService;

    @Context
    private UriInfo uriInfo;

    @POST
    @Path("temp-doc")
    @ApiOperation(
            value = "Generate and modify temporary document",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            consumes = MediaType.APPLICATION_JSON,
            notes = "Return docId dari generated temporary dokumen")
    public Response modifyTempDoc(TemplatingDto dto) {
        try {
            Map<String, String> result = new HashMap();

            String generatedDocId = templatingService.directTemplating(dto);

            result.put("docId", generatedDocId);

            if (!StringUtils.isBlank(generatedDocId)) {
                return Response
                        .ok(result)
                        .build();
            } else {
                return Response.status(Response.Status.NO_CONTENT).build();
            }

        } catch(Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path("release-temp-doc")
    @ApiOperation(
            value = "Delete temporary document",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            consumes = MediaType.APPLICATION_JSON,
            notes = "Delete temporary document ")
    public Response releaseTempDoc(@DefaultValue("") @QueryParam("docId") String docId) {
        try {
            templatingService.deleteTemporaryGeneratedDocAlfresco(docId);

            return Response.status(Response.Status.OK).build();
        } catch(Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
