package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterNonStruktural;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterNonStrukturalService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterNonStrukturalDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-non-struktural")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Non Struktural Resource",
        description = "WS Endpoint untuk menghandle operasi Non Struktural"
)
public class MasterNonStrukturalResource {
    @Inject
    private MasterNonStrukturalService masterNonStrukturalService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data NonStruktural",
            response = MasterNonStruktural[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return NonStruktural list")
    public Response getNonStruktural(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<MasterNonStruktural> all = masterNonStrukturalService.getAll();
        List<MasterNonStrukturalDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterNonStruktural::getId))
                .map(MasterNonStrukturalDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterNonStrukturalDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return NonStruktural list")
    public Response getFilteredNonStruktural(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterNonStrukturalDto> result = masterNonStrukturalService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterNonStrukturalDto::new).collect(Collectors.toList());
        long count = masterNonStrukturalService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Add NonStruktural")
    public Response saveNonStruktural(@Valid MasterNonStrukturalDto dao) {
        try {
            masterNonStrukturalService.save(null, dao);
            notificationService.sendInfo("Info", "Data berhasil disimpan");

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit NonStruktural")
    public Response editNonStruktural(@NotNull @PathParam("id") Long id, @Valid MasterNonStrukturalDto dao) {
        try {
            masterNonStrukturalService.save(id, dao);
            notificationService.sendInfo("Info", "Data berhasil diubah");

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete NonStruktural")
    public Response deleteNonStruktural(@NotNull @PathParam("id") Long id) {
        try {
            masterNonStrukturalService.delete(id);
            notificationService.sendInfo("Info", "Data berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}