package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("surat")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Surat Management",
        description = "WS Endpoint untuk menghandle operasi Surat"
)
public class SuratDisposisiResource {

    @Inject
    private SuratDisposisiService suratDisposisiService;

    @Inject
    private SuratService suratService;

    @Inject
    private MasterTindakan masterTindakan;


    @GET
    @Path("disposisi")
    @ApiOperation(
            value = "Get All Disposisi",
            response = SuratDisposisiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Disposisi list")
    public Response getAll( @DefaultValue("0") @QueryParam("skip") int skip,
                            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                            @DefaultValue("") @QueryParam("filter") String filter,
                            @QueryParam("sort") String sort,
                            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<SuratDisposisiDto> result = suratDisposisiService.getResultList(filter, sort, descending, skip, limit).stream().
                map(SuratDisposisiDto::new).collect(Collectors.toList());
        long count = suratDisposisiService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("disposisi")
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Disposisi")
    public Response saveDisposisi(@Valid SuratDisposisiDto dao) {
        try{
            suratDisposisiService.save(null,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("disposisi/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Disposisi")
    public Response editDisposisi(@NotNull @PathParam("id") Long id, @Valid SuratDisposisiDto dao) {
        try{
            suratDisposisiService.save(id,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("disposisi/markAsRead/{id}")
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Bahasa")
    public Response markAsRead(@NotNull @PathParam("id")Long id) {
        try{
            suratDisposisiService.markAsRead(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("disposisi/addTindakan")
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Bahasa")
    public Response addTindakan(@NotNull @QueryParam("idSuratDisposisi")Long idSuratDisposisi, @NotNull @QueryParam("idMasterTindakan")Long idMasterTindakan) {
        try{
            suratDisposisiService.addTindakan(idSuratDisposisi,idMasterTindakan);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

}
