package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("permohonan-bon-nomor")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Permohonan Bon Nomor",
        description = "WS Endpoint untuk Permohonan Bon Nomor"
)
public class PermohonanBonNomorResource {
    @Inject
    private PermohonanBonNomorService permohonanBonNomorService;

    @Inject
    private PemeriksaBonNomorService pemeriksaBonNomorService;

    @Inject
    private PenandatanganBonNomorService penandatanganBonNomorService;


    @GET
    @ApiOperation(
            value = "Get Filtered Data",
            response = PermohonanBonNomorDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Permohonan Bon Nomor")
    public Response get(@DefaultValue("0") @QueryParam("skip") int skip,
                        @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                        @DefaultValue("") @QueryParam("filter") String filter,
                        @QueryParam("sort") String sort,
                        @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

        List<PermohonanBonNomorDto> result = permohonanBonNomorService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(PermohonanBonNomorDto::new).collect(Collectors.toList());
        long count = permohonanBonNomorService.count(filter);
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }


    @POST
    @ApiOperation(
            value = "Add",
            response = PermohonanBonNomorDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Permohonan Bon Nomor")
    public Response saveDokumen(@Valid PermohonanBonNomorDto dao) {
        try {
            permohonanBonNomorService.saveOrEdit(null, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Permohonan Bon Nomor")
    public Response editDokumen(@NotNull @PathParam("id") Long id, @Valid PermohonanBonNomorDto dao) {
        try {
            permohonanBonNomorService.saveOrEdit(id, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Permohonan Bon Nomor")
    public Response deleteDokumen(@NotNull @PathParam("id") Long id) {
        try {
            permohonanBonNomorService.delete(id);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @GET
    @Path("ListPemeriksa")
    @ApiOperation(
            value = "Get All Pemeriksa",
            response = PemeriksaBonNomorDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Pemeriksa List")
    public Response getPemeriksa(@DefaultValue("0") @QueryParam("skip") int skip,
                                 @DefaultValue("10") @QueryParam("limit") int limit,
                                 @DefaultValue("") @QueryParam("idPermohonan") Long id) {
        List<PemeriksaBonNomorDto> result = pemeriksaBonNomorService.getAllBy(id).stream().map(PemeriksaBonNomorDto::new).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("ListPenandatangan")
    @ApiOperation(
            value = "Get All Pemeriksa",
            response = PenandatanganBonNomorDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Pemeriksa List")
    public Response getPenandatangan(@DefaultValue("0") @QueryParam("skip") int skip,
                                 @DefaultValue("10") @QueryParam("limit") int limit,
                                 @DefaultValue("") @QueryParam("idPermohonan") Long id) {
        List<PenandatanganBonNomorDto> result = penandatanganBonNomorService.getAllBy(id).stream().map(PenandatanganBonNomorDto::new).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("approve/{id}")
    @ApiOperation(
            value = "Approve Permohonan Bon Nomor",
            response = PermohonanBonNomorDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Approve Permohonan Bon Nomor")
    public Response approve(@DefaultValue("0") @QueryParam("skip") int skip,
                                     @DefaultValue("10") @QueryParam("limit") int limit,
                                     @NotNull @PathParam("id") Long id) {
        PermohonanBonNomorDto result = permohonanBonNomorService.approve(id);

        return Response
                .ok(result)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }



}