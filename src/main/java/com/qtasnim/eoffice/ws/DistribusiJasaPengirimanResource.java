package com.qtasnim.eoffice.ws;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.db.DistribusiJasaPengiriman;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.DistribusiJasaPengirimanService;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.DistribusiJasaPengirimanDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;


@Path("distribusi-jasa-pengiriman")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Distribusi Jasa Pengiriman Resource",
        description = "WS Endpoint untuk menghandle operasi DistribusiDocument melalui Jasa Pengiriman"
)
public class DistribusiJasaPengirimanResource {

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject private Logger logger;

    @Inject private NotificationService notificationService;

    @Inject private DistribusiJasaPengirimanService service;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = DistribusiJasaPengirimanDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Distribusi Document list")
    public Response getDistribusi(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<DistribusiJasaPengirimanDto> result = service.getResultList(filter, sort, descending, skip, limit).stream().
                map(DistribusiJasaPengirimanDto::new).collect(Collectors.toList());
        long count = service.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    @POST
    @Secured
    @ApiOperation(
            value = "Add Distribusi Jasa Pengiriman",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Distribusi JasaPengiriman")
    public Response saveDistribusi(@Valid DistribusiJasaPengirimanDto dto) {
        try {
            service.saveOrEdit(null, dto);
//            notificationService.sendInfo("Info", String.format("Data berhasil disimpan,", dto.getNomorDokumen()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save : %s", dto.getNomorDokumen(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Secured
    @ApiOperation(
            value = "Edit Distribusi JasaPengiriman",
            consumes = MediaType.APPLICATION_JSON,
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Distribusi JasaPengiriman")
    public Response editDistribusi(@NotNull @PathParam("id") Long id, @Valid DistribusiJasaPengirimanDto dto) {
        try {
            service.saveOrEdit(id, dto);
//            notificationService.sendInfo("Info", String.format("Data berhasil disimpan,", dto.getNomorDokumen()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit : %s", dto.getNomorDokumen(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @Secured
    @ApiOperation(
            value = "Delete Distribusi JasaPengiriman",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Distribusi JasaPengiriman")
    public Response deleteDistribusi(@NotNull @PathParam("id") Long id) {
        try {
            DistribusiJasaPengiriman model = service.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil dihapus", model.getNomorDokumen()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
