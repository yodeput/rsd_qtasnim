package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.TextSearchService;
import com.qtasnim.eoffice.ws.dto.TextSearchAttachsDetailDto;
import com.qtasnim.eoffice.ws.dto.TextSearchAttachsDto;
import com.qtasnim.eoffice.ws.dto.TextSearchGeneralDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("text-search")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Text Search",
        description = "WS Endpoint untuk mengcari text dari file-file terlampir"
)
public class TextSearchResource {
    @Context
    private UriInfo uriInfo;

    @Inject
    private TextSearchService textSearchService;

    @GET
    @Path("text-search/general")
    @ApiOperation(
            value = "Search text from attached files",
            response = TextSearchGeneralDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Search Results from Attachments Contains Queried Text")
    public Response textSearchGeneral(@DefaultValue("") @QueryParam("query") String query) {
        try {
            List<TextSearchGeneralDto> result = textSearchService.findTextInArsip(query);

            long count = result.size();

            if (result != null) {
                return Response
                        .ok(result)
                        .header("X-Total-Count", count)
                        .header("Access-Control-Expose-Headers", "X-Total-Count")
                        .build();
            } else {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("text-search/attachs")
    @ApiOperation(
            value = "Search text from attached files",
            response = TextSearchAttachsDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List All Attachments Contains Queried Text")
    public Response textSearchAttachs(@DefaultValue("") @QueryParam("query") String query) {
        try {
            TextSearchAttachsDto result = textSearchService.findTextInAttachments(query);

            long count = result.getAttachments().size();

            if (result != null) {
                return Response
                        .ok(result)
                        .header("X-Total-Count", count)
                        .header("Access-Control-Expose-Headers", "X-Total-Count")
                        .build();
            } else {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("text-search/attachs-detail")
    @ApiOperation(
            value = "Search text location(page) in attached file",
            response = TextSearchAttachsDetailDto.class,
            produces = MediaType.APPLICATION_JSON,
            consumes = MediaType.APPLICATION_JSON,
            notes = "Return List Pages Number Contains Queried Text In Specified attached file")
    public Response textSearchAttachsDetail(@DefaultValue("0") @QueryParam("idAttachment") Long idAttachment, @DefaultValue("") @QueryParam("query") String query) {
        try {
            TextSearchAttachsDetailDto result = textSearchService.findTextInPage(idAttachment, query);

            long count = result.getPages().size();

            if (result != null) {
                return Response
                        .ok(result)
                        .header("X-Total-Count", count)
                        .header("Access-Control-Expose-Headers", "X-Total-Count")
                        .build();
            } else {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch(Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
