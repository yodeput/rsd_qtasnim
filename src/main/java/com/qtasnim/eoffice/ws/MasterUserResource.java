package com.qtasnim.eoffice.ws;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.JerseyClient;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ForbiddenException;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.PasswordHash;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.*;
import id.kai.ws.dto.KAIResetPasswordDto;
import id.kai.ws.dto.KAIValidateChangeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;
import org.json.simple.JSONObject;

import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.*;
import java.util.stream.Collectors;

@Path("master-user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "User Management",
        description = "WS Endpoint untuk menghandle operasi User/Pengguna"
)
public class MasterUserResource {
    @Inject
    private MasterUserService masterUserService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private MasterDelegasiService delegasiService;

    @Inject
    private MasterNonStrukturalService nonStrukturalService;

    @Inject
    private MasterSekretarisService masterSekretarisService;

    @Inject
    private MasterRoleService masterRoleService;

    @Inject
    @ISessionContext
    private Session user;

    @Inject
    @JerseyClient
    private Client client;

    @Inject
    private ApplicationConfig applicationConfig;

    @Inject
    private PasswordHash passwordHash;

    @GET
    @Path("me")
    @Secured
    @ApiOperation(
            value = "Me",
            response = MasterUserDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return logged in user data")
    public MasterUserDto getMe() throws ForbiddenException {
        return Optional.ofNullable(user).filter(t -> t.getUserSession() != null).map(t -> {
                    List<JabatanDto> jabatanList = new ArrayList<>();
                    if (t.getUserSession().getUser().getOrganizationEntity() != null) {
                        MasterStrukturOrganisasi utama = t.getUserSession().getUser().getOrganizationEntity();
                        jabatanList.add(new JabatanDto(utama.getIdOrganization(), utama.getOrganizationName(), "Utama"));
                    }
                    List<MasterDelegasi> delegasiList = delegasiService.getByToNoTipe();
                    if (delegasiList != null && delegasiList.size() > 0) {
                        for (MasterDelegasi delegasi : delegasiList) {
                            MasterStrukturOrganisasi from = delegasi.getFrom();
                            jabatanList.add(new JabatanDto(from.getIdOrganization(), from.getOrganizationName(), delegasi.getTipe()));
                        }
                    }
                    MasterNonStruktural nonStruktural = nonStrukturalService.getByUserLogin();
                    if (nonStruktural != null) {
                        MasterStrukturOrganisasi org = nonStruktural.getOrganisasi();
                        jabatanList.add(new JabatanDto(org.getIdOrganization(), org.getOrganizationName(), "Non Struktural"));
                    }
                    MasterUserDto result = new MasterUserDto(t.getUserSession().getUser(), jabatanList);
                    MasterStrukturOrganisasiDto pejabatSekretaris = masterSekretarisService.getPejabatBySekretaris(result.getOrganization()).stream().findFirst().map(MasterStrukturOrganisasiDto::new).orElse(null);
                    List<MasterRole> validRoles = t.getUserSession().getUser().getOrganizationEntity().getRoleList().stream()
                            .filter(v -> v.getLevel() != null && v.getLevel() > 0)
                            .collect(Collectors.toList());
                    MasterRole defaultRole = masterRoleService.defaultRole();

//                    if (defaultRole != null) {
//                        validRoles.add(defaultRole);
//                    }
//
//                    List<RoleModuleDto> roleModules = validRoles
//                            .stream()
//                            .flatMap(v -> v.getRoleModuleList().stream())
//                            .collect(Collectors.groupingBy(v -> v.getModules().getIdModul(), Collectors.toList()))
//                            .entrySet()
//                            .stream()
//                            .map(v -> {
//                                RoleModuleDto masterRoleModule = new RoleModuleDto(v.getValue().stream().findAny().orElse(null));
//                                v.getValue()
//                                        .stream()
//                                        .collect(Collectors.groupingBy(w -> w.getRoles().getLevel(), Collectors.toList()))
//                                        .entrySet()
//                                        .stream()
//                                        .sorted(Comparator.comparing(Map.Entry::getKey))
//                                        .forEach(w -> {
//                                            RoleModule roleModule = w.getValue().stream().findFirst().orElse(null);
//
//                                            if (roleModule != null) {
//                                                if (roleModule.getAllowUpdate() != null) {
//                                                    masterRoleModule.setAllowUpdate(roleModule.getAllowUpdate());
//                                                }
//
//                                                if (roleModule.getAllowRead() != null) {
//                                                    masterRoleModule.setAllowRead(roleModule.getAllowRead());
//                                                }
//
//                                                if (roleModule.getAllowPrint() != null) {
//                                                    masterRoleModule.setAllowPrint(roleModule.getAllowPrint());
//                                                }
//
//                                                if (roleModule.getAllowCreate() != null) {
//                                                    masterRoleModule.setAllowCreate(roleModule.getAllowCreate());
//                                                }
//
//                                                if (roleModule.getAllowDelete() != null) {
//                                                    masterRoleModule.setAllowDelete(roleModule.getAllowDelete());
//                                                }
//                                            }
//                                        });
//
//                                return masterRoleModule;
//                            })
//                            .collect(Collectors.toList());

                    List<RoleModuleDto> roleModules = new ArrayList<>();
                    List<RoleModule> rModules = validRoles.stream().flatMap(v -> v.getRoleModuleList().stream())
                            .collect(Collectors.toList());
                    for(RoleModule a : defaultRole.getRoleModuleList()){
                        RoleModuleDto roleModule = new RoleModuleDto(a);
                        if(a.getAllowCreate().booleanValue() == false && rModules.stream().anyMatch(b->b.getModules().getIdModul().equals(a.getModules().getIdModul()) && b.getAllowCreate() != null && b.getAllowCreate().booleanValue())){
                            roleModule.setAllowCreate(true);
                        }
                        if(a.getAllowRead().booleanValue() == false && rModules.stream().anyMatch(b->b.getModules().getIdModul().equals(a.getModules().getIdModul()) && b.getAllowRead() != null && b.getAllowRead().booleanValue())){
                            roleModule.setAllowRead(true);
                        }
                        if(a.getAllowUpdate().booleanValue() == false && rModules.stream().anyMatch(b->b.getModules().getIdModul().equals(a.getModules().getIdModul()) && b.getAllowUpdate() != null && b.getAllowUpdate().booleanValue())){
                            roleModule.setAllowUpdate(true);
                        }
                        if(a.getAllowDelete().booleanValue() == false && rModules.stream().anyMatch(b->b.getModules().getIdModul().equals(a.getModules().getIdModul()) && b.getAllowDelete() != null && b.getAllowDelete().booleanValue())){
                            roleModule.setAllowDelete(true);
                        }
                        if(a.getAllowPrint().booleanValue() == false && rModules.stream().anyMatch(b->b.getModules().getIdModul().equals(a.getModules().getIdModul()) && b.getAllowPrint() != null && b.getAllowPrint().booleanValue())){
                            roleModule.setAllowPrint(true);
                        }
                        roleModules.add(roleModule);
                    }

                    result.setPejabatSekretaris(pejabatSekretaris);
                    result.setRoleModules(roleModules);

                    return result;
                }
        ).orElseThrow(() -> new ForbiddenException("User not logged in"));
    }

    @GET
    @Path("profile")
    @Secured
    @ApiOperation(
            value = "Profile",
            response = ProfileUserDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return logged in user profile")
    public ProfileUserDto getProfile() throws ForbiddenException {
        try {
            ProfileUserDto profile = masterUserService.getProfile(user.getUserSession().getUser());
            return profile;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ProfileUserDto();
        }
    }

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterUserDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return user list")
    public Response getAll(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit) throws InternalServerErrorException {
        JPAJinqStream<MasterUser> all = masterUserService.getAllValid();
        List<MasterUserDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterUser::getId))
                .map(MasterUserDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter")
    @ApiOperation(
            value = "Get All",
            response = MasterUserDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return user list")
    public Response getAll(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @DefaultValue("")
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

        List<MasterUserDto> result = masterUserService.getResultList(filter, sort, descending, skip, limit)
                .stream().map(q->{
                    MasterUserDto obj = new MasterUserDto(q,true);
                    List<RoleDto> roles = obj.getRoles();
                    if(roles !=null && !roles.isEmpty()) {
                        if (roles.stream().noneMatch(a -> a.getRoleName().toLowerCase().equals("user"))) {
                            RoleDto user = new RoleDto(masterRoleService.defaultRole(), false);
                            roles.add(user);
                        }
                    }else{
                        RoleDto user = new RoleDto(masterRoleService.defaultRole(), false);
                        roles.add(user);
                    }
                    obj.setRoles(roles);
                    return obj;
                }).collect(Collectors.toList());

        long count = masterUserService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter2")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterUserDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return user list filtered")
    public Response getDataFiltered(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("") @QueryParam("start") String start,
            @DefaultValue("") @QueryParam("end") String end,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

        HashMap<String, Object> temp = masterUserService.getFiltered(filter, start, end, skip, limit, descending, sort);
        List<MasterUserDto> result = (List<MasterUserDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save a new user")
    public Response saveData(MasterUserDto dao) {
        try {
            masterUserService.saveOrEdit2(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan",dao.getUsername()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data '%s' gagal disimpan",dao.getUsername()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit an user")
    public Response editData(@PathParam("id") Long id, MasterUserDto dao) {
        try {
            masterUserService.saveOrEdit2(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil diubah", dao.getUsername()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data '%s' Gagal diubah", String.format("%s", dao.getUsername()).trim()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete an user")
    public Response deleteData(@PathParam("id") Long id) {
        try {
            MasterUser deleted = masterUserService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil dihapus", String.format("%s %s", deleted.getNameFront(), deleted.getNameMiddleLast()).trim()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", "Data gagal dihapus");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("/pattern/{userId}")
    @ApiOperation(
            value = "Save Key Pattern",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save key pattern from mobile apps")
    public Response savePattern(@PathParam("userId") Long id, MasterUserDto dao) {
        try {
            masterUserService.savePattern(id, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("/finger/{userId}")
    @ApiOperation(
            value = "Save Fingerprint",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save fingerprint from mobile apps")
    public Response saveFinger(@PathParam("userId") Long id, MasterUserDto dao) {
        try {
            masterUserService.saveFinger(id, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("changePassword")
    @Secured
    @ApiOperation(
            value = "Save New Password Changed",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save New Password Changed")
    public Response saveChangedPassword(ResetPasswordDto dao) {
        try {
            MasterUser obj = user.getUserSession().getUser();
            if (obj.getIsInternal().booleanValue() == false) {
                String hash = passwordHash.getHash(dao.getNewPassword(), obj.getPasswordSalt());
                obj.setPasswordHash(hash);
                masterUserService.edit(obj);
                notificationService.sendInfo("Info", "Password Berhasil Diubah");

                return Response
                        .ok(new MessageDto("info", "Password Berhasil Diubah"))
                        .status(Response.Status.OK)
                        .build();
            } else {
                /*Logic untuk reset/change password internal*/
                try {
                    WebTarget webResourceReset = client.target(UriBuilder.fromUri(applicationConfig.getResetPasswordUrl()).toString());
                    JSONObject userBody = new JSONObject();
                    userBody.put("username", obj.getEmployeeId());
                    Response responseReset = webResourceReset
                            .request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", String.format("Bearer %s", applicationConfig.getSyncAuthBearer()))
                            .post(Entity.entity(userBody, MediaType.APPLICATION_JSON_TYPE));
                    String resultReset = responseReset.readEntity(String.class);
                    KAIResetPasswordDto kaiResetPasswordDto = getObjectMapper().readValue(resultReset, new TypeReference<KAIResetPasswordDto>() {
                    });

                    WebTarget webResourceValidate = client.target(UriBuilder.fromUri(applicationConfig.getValidateCodeUrl()).toString());
                    JSONObject validateBody = new JSONObject();
                    validateBody.put("code", kaiResetPasswordDto.getMessage().getCode());
                    Response responseValidate = webResourceValidate
                            .request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", String.format("Bearer %s", applicationConfig.getSyncAuthBearer()))
                            .post(Entity.entity(validateBody, MediaType.APPLICATION_JSON_TYPE));
                    String resultValidate = responseValidate.readEntity(String.class);
                    KAIValidateChangeDto kaiValidateChangeDto = getObjectMapper().readValue(resultValidate, new TypeReference<KAIValidateChangeDto>() {
                    });

                    WebTarget webResourceChange = client.target(UriBuilder.fromUri(applicationConfig.getChangePasswordUrl()).toString());
                    JSONObject passwordBody = new JSONObject();
                    passwordBody.put("password", dao.getNewPassword());
                    passwordBody.put("signature", kaiValidateChangeDto.getSignature());
                    Response responseChanged = webResourceChange
                            .request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", String.format("Bearer %s", applicationConfig.getSyncAuthBearer()))
                            .post(Entity.entity(passwordBody, MediaType.APPLICATION_JSON_TYPE));

                    notificationService.sendInfo("Info", "Password Berhasil Diubah");
                    return Response.ok(new MessageDto("info", "Password Berhasil Diubah"))
                            .status(Response.Status.OK).build();
                } catch (Exception ex) {
                    throw new InternalServerErrorException(ex.getMessage(), ex);
                }
            }
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal Merubah Password : %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("/eksternal")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save an eksternal user")
    public Response saveEksternal(@FormDataParam("data") MasterUserDto dao, @FormDataParam("data") String json) {
        try {
            Map<String, Object> values = getObjectMapper().readValue(json, new TypeReference<HashMap<String, Object>>() {
            });
            String password = values.get("password").toString();
            masterUserService.saveEksternal(null, dao, password);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", String.format("%s %s", dao.getNameFront(), dao.getNameMiddleLast()).trim()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data '%s' gagal disimpan", String.format("%s %s", dao.getNameFront(), dao.getNameMiddleLast()).trim()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("/filter/by-organization")
    @ApiOperation(value = "Get By Organization",
            response = MasterUserDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return user list by organization")
    public Response getByOrganization(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("") @QueryParam("userFilter") String userFilter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterStrukturOrganisasiDto> structures = masterStrukturOrganisasiService
                .getResultList(filter, sort, descending, skip, limit).stream()
                .filter(q -> q.getIsActive() && !q.getIsDeleted())
                .map(MasterStrukturOrganisasiDto::new)
                .collect(Collectors.toList());

        List<MasterStrukturOrganisasi> ids = new ArrayList<>();
        structures.forEach(q -> {
            Long id = q.getOrganizationId();
            if (id != null) {
                MasterStrukturOrganisasi organisasi = masterStrukturOrganisasiService.getByIdOrganisasi(id);
                ids.add(organisasi);
            }
        });

        List<MasterUserDto> result = masterUserService
                .getResultList(userFilter, sort, descending, skip, limit)
                .stream().filter(q -> !q.getIsDeleted() && q.getIsActive() && ids.contains(q.getOrganizationEntity()))
                .map(MasterUserDto::new)
                .collect(Collectors.toList());

        long count = masterUserService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }


    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    @GET
    @Path("validate")
    @ApiOperation(
            value = "Validasi Username,email dan nomor telepon",
            produces = MediaType.APPLICATION_JSON)
    public Response validation(
            @DefaultValue("") @QueryParam("id") Long id,
            @DefaultValue("") @QueryParam("username") String username,
            @DefaultValue("") @QueryParam("email") String email,
            @DefaultValue("") @QueryParam("phone") String phone) {
        try {
            boolean isValid = masterUserService.validate(id, username, email, phone);
            return Response.ok(isValid).status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }

    @POST
    @Path("/eksternal/{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save an eksternal user")
    public Response saveEksternal(@PathParam("id") Long id, @FormDataParam("data") MasterUserDto dao, @FormDataParam("data") String json) {
        try {
            Map<String, Object> values = getObjectMapper().readValue(json, new TypeReference<HashMap<String, Object>>() {
            });
            String password = values.get("password").toString();
            masterUserService.saveEksternal(id, dao, password);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil diubah", String.format("%s %s", dao.getNameFront(), dao.getNameMiddleLast()).trim()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data %s gagal diubah : %s", dao.getUsername(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("findUser")
    @Secured
    public Response findByUsername(@QueryParam("username") String username){
        RDSUserDto result = new RDSUserDto(masterUserService.findUser(username));
        return Response
               .ok(result).header("Access-Control-Expose-Headers", "X-Total-Count")
               .build();
    }
}