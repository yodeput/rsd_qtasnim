package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.PosisiHistory;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.PosisiHistoryService;
import com.qtasnim.eoffice.ws.dto.PosisiHistoryDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.stream.Collectors;

@Path("posisi-history")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Posisi History Resource",
        description = "WS Endpoint untuk menghandle operasi Posisi History"
)
public class PosisiHistoryResource {
    @Inject
    private PosisiHistoryService posisiHistoryService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data Posisi History",
            response = PosisiHistory[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Posisi History list")
    public Response get(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<PosisiHistoryDto> all = posisiHistoryService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(PosisiHistoryDto::new).collect(Collectors.toList());
        long count = posisiHistoryService.count(filter);
        return Response
                .ok(all)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Posisi History")
    public Response delete(@NotNull @PathParam("id") Long id) {
        try {
            posisiHistoryService.delete(id);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}