package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterSekretaris;
import com.qtasnim.eoffice.security.ForbiddenException;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterSekretarisService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterSekretarisDto;
import com.qtasnim.eoffice.ws.dto.MasterStrukturOrganisasiDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-sekretaris")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = " Pengaturan Penunjukan Sekretaris",
        description = "WS Endpoint untuk menghandle Penunjukan Sekretaris"
)
public class MasterSekretarisResource {
    @Inject
    private MasterSekretarisService masterSekretarisService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = MasterSekretaris[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Sekretaris list")
    public Response getCountries(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit) throws InternalServerErrorException {
        JPAJinqStream<MasterSekretaris> all = masterSekretarisService.getAll();
        List<MasterSekretarisDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterSekretaris::getId))
                .map(MasterSekretarisDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterSekretaris[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kota list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @DefaultValue("") @QueryParam("pejabat") String pejabat,
                                @DefaultValue("") @QueryParam("sekretaris") String sekretaris,
                                @DefaultValue("") @QueryParam("start") String start,
                                @DefaultValue("") @QueryParam("end") String end,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        if(Strings.isNullOrEmpty(filter)){
            filter = "isDeleted==false";
        }else{
            filter = filter+";isDeleted==false";
        }
        List<MasterSekretarisDto> result = masterSekretarisService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterSekretarisDto::new).collect(Collectors.toList());
        long count = masterSekretarisService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Sekretaris")
    public Response saveSekretaris(@Valid MasterSekretarisDto dao) {

        try{
            masterSekretarisService.save(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getTipePenerimaan()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getTipePenerimaan(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Sekretaris")
    public Response editSekretaris(@NotNull @PathParam("id") Long id, @Valid MasterSekretarisDto dao) {

        try{
            masterSekretarisService.save(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getTipePenerimaan()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getTipePenerimaan(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Sekretaris")
    public Response deleteSekretaris(@NotNull @PathParam("id") Long id) {

        try{
            MasterSekretaris deleted = masterSekretarisService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getTipePenerimaan()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("globalFilter")
    @ApiOperation(
            value = "Get Data Filtered Sekretaris",
            response = MasterSekretaris[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Sekretaris list")
    public Response getFilteredData(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @DefaultValue("") @QueryParam("start") String start,
                                @DefaultValue("") @QueryParam("end") String end,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterSekretarisDto> result = masterSekretarisService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterSekretarisDto::new).collect(Collectors.toList());
        long count = masterSekretarisService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("jabatan-by-sekretaris/{sekretarisId}")
    public Response getJabatanBySekretaris(
            @PathParam("sekretarisId") Long sekretarisId
    ) throws ForbiddenException {
        List<MasterStrukturOrganisasiDto> result = masterSekretarisService
                .getPejabatBySekretaris(sekretarisId)
                .stream()
                .map(MasterStrukturOrganisasiDto::new)
                .collect(Collectors.toList());

        return Response.ok(result).build();
    }
}