package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.*;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("registrasi-dokumen")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = " Registrasi Dokumen Eksternal",
        description = "WS Endpoint untuk Registrasi Dokumen Eksternal"
)
public class RegistrasiDokumenEksternalResource {
    @Inject
    private RegistrasiDokumenEksternalService registrasiDokumenEksternalService;

    @Inject
    private MasterJenisDokumenService masterJenisDokumenService;

    @Inject
    private SuratService suratService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @Inject
    private MasterKlasifikasiMasalahService klasifikasiMasalahService;

    @Inject
    private MasterKlasifikasiKeamananService klasifikasiKeamananService;

    @Inject
    private FormFieldService formFieldService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private MasterVendorService vendorService;

    @GET
    @Path("getDraft")
    @ApiOperation(
            value = "Get Draft Registrasi Dokumen",
            response = SuratDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Registrasi Dokumen Status Draft")
    public Response getDraftReg(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        JPAJinqStream<Surat> all = registrasiDokumenEksternalService.getDraft();
        List<DokumenDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Surat::getCreatedDate).reversed())
                .map(q -> {
                    DokumenDto obj = new DokumenDto();
                    DateUtil dateUtil = new DateUtil();
                    obj.setId(q.getId());
                    obj.setTanggal(dateUtil.getCurrentISODate(q.getCreatedDate()));
                    FormValue fv = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                            .findFirst().orElse(null);
                    if (fv != null) {
                        obj.setPerihal(fv.getValue());
                    }
                    FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("dari"))
                            .findFirst().orElse(null);
                    List<PengirimDto> pObj = new ArrayList<>();
                    if (fv2 != null) {
                        PengirimDto pengirim = new PengirimDto();
                        MasterVendor vendor = vendorService.find(Long.parseLong(fv2.getValue()));
                        pengirim.setNama(vendor.getNama());
                        pObj.add(pengirim);
                    }
                    obj.setPengirim(pObj);
                    return obj;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("draft")
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Registrasi Dokumen Eksternal")
    public Response saveDokumen(@Valid SuratDto dao) {
        try{
            dao.setStatus("DRAFT");
            SuratDto surat = registrasiDokumenEksternalService.saveOrEdit(null,dao);
            return Response.ok(surat).status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("editDraft/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Registrasi Dokumen Eksternal")
    public Response editDokumen(@NotNull @PathParam("id") Long id, @Valid SuratDto dao) {
        try {
            registrasiDokumenEksternalService.saveOrEdit(id, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("deleteDraft/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Registrasi Dokumen Eksternal")
    public Response deleteDokumen(@NotNull @PathParam("id") Long id) {
        try{
            registrasiDokumenEksternalService.delete(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @GET
    @Path("getJenisDokumen")
    @ApiOperation(
            value = "Get All Jenis Dokumen",
            response = MasterJenisDokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Jenis Dokumen list")
    public Response getAllJenisDokumen(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterJenisDokumen> all = masterJenisDokumenService.getAll(false);
        List<MasterJenisDokumenDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterJenisDokumen::getIdJenisDokumen))
                .map(MasterJenisDokumenDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path(value = "upload/{idReg}")
    @Secured
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Upload",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response upload(
            @PathParam("idReg") Long idReg,
            @FormDataParam(value = "file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value = "file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body
    ) throws IOException {
        try {
            suratService.uploadAttachment(idReg, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil diunggah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("File '%s' gagal diunggah: %s", fileDisposition.getFileName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("detail/{idReg}")
    @ApiOperation(
            value = "Get Dokumen Registrasi By Id",
            response = SuratDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Dokumen Registrasi")
    public Response getByIdSurat(@PathParam("idReg") Long idReg) {
        SuratDto result = new SuratDto();
        Surat surat = suratService.getByIdSurat(idReg);
        if (surat != null) {
            result = new SuratDto(surat);

            List<FormFieldDto> formFieldDtos = result.getFormDefinition().getFields();

            for (FormFieldDto formFieldDto : formFieldDtos) {
                String selectedFields = formFieldDto.getSelectedSourceField();

                formFieldDto.setValueDataMaster(formFieldService.fillValueFromDataMaster(
                        new DataMasterValueRetrieveParam(
                                formFieldDto.getTableSourceName(),
                                selectedFields == null ? null : Arrays.asList(selectedFields.split(",")),
                                formFieldDto.getSelectedSourceValue())
                ));
            }

            result.getMetadata().forEach((a)->{
                if(a.getName().contains("Klasifikasi Keamanan")){
                    if(!Strings.isNullOrEmpty(a.getValue())) {
                        Long idKeamanan = Long.parseLong(a.getValue());
                        MasterKlasifikasiKeamanan keamanan = klasifikasiKeamananService.find(idKeamanan);
                        a.setLabel(keamanan.getNamaKlasifikasiKeamanan());
                    }
                }

                if(a.getName().contains("Klasifikasi Dokumen")){
                    if(!Strings.isNullOrEmpty(a.getValue())) {
                        Long idMasalah = Long.parseLong(a.getValue());
                        MasterKlasifikasiMasalah masalah = klasifikasiMasalahService.find(idMasalah);
                        a.setLabel(masalah.getKodeKlasifikasiMasalah() + " " + masalah.getNamaKlasifikasiMasalah());
                    }
                }

                if(a.getName().contains("Dari")){
                    if(!Strings.isNullOrEmpty(a.getValue())) {
                        Long idVendor = Long.parseLong(a.getValue());
                        MasterVendor vendor = vendorService.find(idVendor);
                        a.setLabel(vendor.getNama());
                    }
                }
            });
        }
        return Response.ok(result).build();
    }

    @GET
    @Path("getAttach")
    @ApiOperation(
            value = "Get Surat Attachment Files",
            response = LampiranDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat Attachment File List")
    public Response getAttachmentFile(
            @DefaultValue("") @QueryParam("idReg") Long idReg,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getAttachmentDataByRef("t_surat", idReg);
        List<LampiranDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(q -> {
                    LampiranDto obj = new LampiranDto();
                    DateUtil dateUtil = new DateUtil();
                    obj.setId(q.getDocId());
                    obj.setNamaFile(q.getDocName());
                    obj.setUkuran(q.getSize());
                    obj.setTipe(q.getMimeType());
                    obj.setWaktu(dateUtil.getCurrentISODate(q.getCreatedDate()));
                    obj.setPengunggah(q.getCreatedBy());
                    return obj;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("submit")
    @ApiOperation(
            value = "Submit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Submit Registrasi Dokumen Eksternal")
    public Response submitDokumen(@Valid SuratDto dao) {
        try{
            dao.setStatus("SUBMITTED");
            SuratDto surat = registrasiDokumenEksternalService.submit(dao);
            return Response.ok(surat).status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}