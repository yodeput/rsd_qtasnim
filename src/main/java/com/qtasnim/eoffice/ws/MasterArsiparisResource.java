package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterArsiparis;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterArsiparisService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.FieldErrorDto;
import com.qtasnim.eoffice.ws.dto.MasterArsiparisDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-arsiparis")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Arsiparis Resource",
        description = "WS Endpoint untuk menghandle operasi Arsiparis"
)
public class MasterArsiparisResource {
    @Inject
    private MasterArsiparisService masterArsiparisService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data Arsiparis",
            response = MasterArsiparis[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsiparis list")
    public Response getArsiparis(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<MasterArsiparis> all = masterArsiparisService.getAll();
        List<MasterArsiparisDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterArsiparis::getId))
                .map(MasterArsiparisDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterArsiparis[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kota list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        
        List<MasterArsiparisDto> result = masterArsiparisService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterArsiparisDto::new).collect(Collectors.toList());
        long count = masterArsiparisService.count(filter);
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Arsiparis")
    public Response saveArsiparis(MasterArsiparisDto dao) {
        String hasil = masterArsiparisService.validate(null,dao);
        if(!Strings.isNullOrEmpty(hasil)){
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {
            try {
                masterArsiparisService.save(null, dao);
                notificationService.sendInfo("Info", "Data berhasil disimpan");

                return Response
                        .ok(new MessageDto("info", "Data berhasil disimpan"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Save: %s", ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Arsiparis")
    public Response editArsiparis(@PathParam("id") Long id, MasterArsiparisDto dao) {
        String hasil = masterArsiparisService.validate(id,dao);
        if(!Strings.isNullOrEmpty(hasil)){
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {
            try {
                masterArsiparisService.save(id, dao);
                notificationService.sendInfo("Info", "Data berhasil diubah");

                return Response
                        .ok(new MessageDto("info", "Data berhasil diubah"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Edit: %s", ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Arsiparis")
    public Response deleteArsiparis(@PathParam("id") Long id) {

        try{
            masterArsiparisService.delete(id);
            notificationService.sendInfo("Info", "Data berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("data/{id}")
    @ApiOperation(
            value = "Get Data",
            response = MasterArsiparisDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Data")
    public Response getData(@NotNull @PathParam("id") Long id) {
        MasterArsiparisDto result = masterArsiparisService.getById(id);
        return Response
                .ok(result)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filterBy")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterArsiparis[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kota list")
    public Response filterByCB(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending,
                                @DefaultValue("") @QueryParam("start") String start,
                                @DefaultValue("") @QueryParam("end") String end) throws InternalServerErrorException {

        HashMap<String,Object> temp = masterArsiparisService.getFilteredByCriteriaB(filter,sort,descending,skip,limit,start,end);
        List<MasterArsiparisDto> result = (List<MasterArsiparisDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    private MessageDto prepareMessage(String field) {
        String msg;
        switch (field) {
            case "start":
                msg = "Tanggal Mulai berada pada rentang Penunjukkan Arsiparis sebelumnya.";
                break;
            case "end":
                msg = "Tanggal Selesai berada pada rentang Penunjukkan Arsiparis sebelumnya.";
                break;
            default:
                msg = "Periode Penujukkan Arsiparis sudah dibuat sebelumnya.";
                break;
        }
        MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();

        FieldErrorDto fieldErrorDto = new FieldErrorDto();
        fieldErrorDto.setField(field);
        fieldErrorDto.setMessage(msg);
        fieldErrorDtos.add(fieldErrorDto);

        messageDto.setFields(fieldErrorDtos);

        return messageDto;
    }
}