package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterGrade;
import com.qtasnim.eoffice.db.MasterKewenanganPenerima;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterGradeService;
import com.qtasnim.eoffice.services.MasterKewenanganPenerimaService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.FieldErrorDto;
import com.qtasnim.eoffice.ws.dto.MasterGradeDto;
import com.qtasnim.eoffice.ws.dto.MasterKewenanganPenerimaDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import liquibase.util.StringUtils;

@Path("master-kewenangan-penerima")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Pengaturan Kewenangan Penerima Surat",
        description = "WS Endpoint untuk menghandle Pengaturan Kewenangan Penerima Surat"
)
public class MasterKewenanganPenerimaResource {
    @Inject
    private MasterKewenanganPenerimaService kewenanganPenerimaService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Inject private MasterGradeService gradeService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = MasterKewenanganPenerimaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kewenangan Penerima list")
    public Response getData(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit) throws InternalServerErrorException {
        JPAJinqStream<MasterKewenanganPenerima> all = kewenanganPenerimaService.getAll();
        List<MasterKewenanganPenerimaDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterKewenanganPenerima::getId))
                .map(MasterKewenanganPenerimaDto::new).collect(Collectors.toList());
        this.mappingToDto(result);
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter")
    @ApiOperation(
            value = "Get Data",
            response = MasterKewenanganPenerimaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kewenangan Penandatangan list")
    public Response getData(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterKewenanganPenerimaDto> result = kewenanganPenerimaService.getResultList(filter, sort, descending, skip, limit)
                .stream().map(MasterKewenanganPenerimaDto::new).collect(Collectors.toList());

        this.mappingToDto(result);

        long count = kewenanganPenerimaService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Kewenangan Penerima")
    public Response saveData(@Valid MasterKewenanganPenerimaDto dao) {
        boolean hasil = kewenanganPenerimaService.validate(null, dao.getGradePengirim(), dao.getIdJenisDokumen(), dao.getCompanyId());
        if (!hasil) {
            MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
            List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();
            FieldErrorDto fieldErrorDto = new FieldErrorDto();
            fieldErrorDto.setField("gradePengirim");
            fieldErrorDto.setMessage("Grade pengirim sudah di gunakan.");
            fieldErrorDtos.add(fieldErrorDto);
            messageDto.setFields(fieldErrorDtos);
            return Response.status(Response.Status.BAD_REQUEST).entity(messageDto).build();
        } else {
            try {
                kewenanganPenerimaService.saveOrEdit(null, dao);
                notificationService.sendInfo("Info", "Data berhasil disimpan");

                return Response
                        .ok(new MessageDto("info", "Data berhasil disimpan"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Save: %s", ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Kewenangan Penerima")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterKewenanganPenerimaDto dao) {

        boolean hasil = kewenanganPenerimaService.validate(id, dao.getGradePengirim(), dao.getIdJenisDokumen(), dao.getCompanyId());
        if (!hasil) {
            MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
            List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();
            FieldErrorDto fieldErrorDto = new FieldErrorDto();
            fieldErrorDto.setField("gradePengirim");
            fieldErrorDto.setMessage("Grade pengirim sudah di gunakan.");
            fieldErrorDtos.add(fieldErrorDto);
            messageDto.setFields(fieldErrorDtos);
            return Response.status(Response.Status.BAD_REQUEST).entity(messageDto).build();
        } else {
            try {
                kewenanganPenerimaService.saveOrEdit(id, dao);
                notificationService.sendInfo("Info", "Data berhasil diubah");

                return Response
                        .ok(new MessageDto("info", "Data berhasil diubah"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Edit: %s", ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Kewenangan Penerima")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try{
            MasterKewenanganPenerima model = kewenanganPenerimaService.find(id);
            model.setIsDeleted(true);
            kewenanganPenerimaService.edit(model);
            notificationService.sendInfo("Info", "Data berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private void mappingToDto(List<MasterKewenanganPenerimaDto> all) {
        all.stream().map(dto -> {
            dto.setListGradePenerima(extractFromGrade(dto.getGradePenerima()));
            dto.setListGradePengirim(extractFromGrade(dto.getGradePengirim()));

            return dto;
        }).collect(Collectors.toList());
    }
    
    private List<MasterGradeDto> extractFromGrade(String grade) {
        List<MasterGradeDto> result = new ArrayList<>();
        
        if (!StringUtils.isEmpty(grade)) {
            List<String> strList = Stream.of(grade.split(";")).collect(Collectors.toList());
            List<MasterGrade> list = gradeService.getMasterGradeListByNames(strList);
            if (list.size() > 0) {
                result = list.stream().map(MasterGradeDto::new).collect(Collectors.toList());
            }
        }
        
        return result;
    }
}