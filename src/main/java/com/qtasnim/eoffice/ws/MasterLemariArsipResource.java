package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterLemariArsip;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterLemariArsipService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterLemariArsipDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-lokasi-arsip")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Lokasi Arsip Resource",
        description = "WS Endpoint untuk menghandle operasi Lokasi Arsip"
)
public class MasterLemariArsipResource {
    @Inject
    private MasterLemariArsipService masterLemariArsipService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @Path("lemari")
    @ApiOperation(
            value = "Get Data LemariArsip",
            response = MasterLemariArsip[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return LemariArsip list")
    public Response getLemariArsip(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<MasterLemariArsip> all = masterLemariArsipService.getAll();
        List<MasterLemariArsipDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterLemariArsip::getId))
                .map(MasterLemariArsipDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("lemari/filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterLemariArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return LemariArsip list")
    public Response getFilteredLemariArsip(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterLemariArsipDto> result = masterLemariArsipService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterLemariArsipDto::new).collect(Collectors.toList());
        long count = masterLemariArsipService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("lemari")
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Add LemariArsip")
    public Response saveLemariArsip(@Valid MasterLemariArsipDto dao) {
        try {
            masterLemariArsipService.save(null, dao);
            notificationService.sendInfo("Info", String.format("Data berhasil disimpan", dao.getJenisLemari()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getJenisLemari(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("lemari/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit LemariArsip")
    public Response editLemariArsip(@NotNull @PathParam("id") Long id, @Valid MasterLemariArsipDto dao) {
        try {
            masterLemariArsipService.save(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil diubah", dao.getKode()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getJenisLemari(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("lemari/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete LemariArsip")
    public Response deleteLemariArsip(@NotNull @PathParam("id") Long id) {
        try {
            MasterLemariArsip deleted = masterLemariArsipService.delete(id);
            notificationService.sendInfo("Info", String.format("Data berhasil dihapus", deleted.getJenisLemari()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus, Data sudah digunakan"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("lemari/all")
    @ApiOperation(
            value = "Get All Data",
            response = MasterLemariArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return LemariArsip list")
    public Response getAllLemariArsip(
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterLemariArsipDto> result = masterLemariArsipService.getResultList(filter, sort, descending).stream()
                .map(MasterLemariArsipDto::new).collect(Collectors.toList());
        long count = masterLemariArsipService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}