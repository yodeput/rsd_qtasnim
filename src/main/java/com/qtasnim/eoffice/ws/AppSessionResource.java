package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.AppSessionService;
import com.qtasnim.eoffice.services.SysLogService;
import com.qtasnim.eoffice.ws.dto.AppSessionDto;
import com.qtasnim.eoffice.ws.dto.SysLogDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.stream.Collectors;

@Path("app-session")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Application Session Resource",
        description = "WS Endpoint untuk menghandle operasi Generate Token WS API untuk aplikasi lain dari RDS"
)
public class AppSessionResource {
    @Inject
    private AppSessionService appSessionService;

    @Context
    private UriInfo uriInfo;
    
    
    @GET
    @ApiOperation(
            value = "Get Data",
            response = AppSessionDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return SysLog list")
    public Response getData(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<AppSessionDto> result = appSessionService.getResultList(filter, sort, descending, skip, limit).stream().
                map(AppSessionDto::new).collect(Collectors.toList());
        long count = appSessionService.count(filter);

        return Response
            .ok(result)
            .header("X-Total-Count", count)
            .header("Access-Control-Expose-Headers", "X-Total-Count")
            .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Authentifikasi")
    public Response AddData(AppSessionDto dao) {
        try{
            appSessionService.save(null,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}