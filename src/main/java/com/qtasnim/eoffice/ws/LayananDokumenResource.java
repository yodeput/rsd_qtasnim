package com.qtasnim.eoffice.ws;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.ExclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.FlowElement;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.TaskEventHandler;
import com.qtasnim.eoffice.workflows.WorkflowException;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaQuery;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("layanan-dokumen")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Layanan Dokumen Resource",
        description = "WS Endpoint untuk menghandle operasi layanan dokumen"
)
public class LayananDokumenResource {
    @Inject
    private PermohonanDokumenService permohonanDokumenService;
    @Inject
    private PenyerahDisposisiService disposisiService;
    @Inject
    private NotificationService notificationService;
    @Inject
    private ProcessTaskService processTaskService;
    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;
    @Inject
    private SuratService suratService;
    @Inject
    private MasterDelegasiService delegasiService;

    @Inject
    private Logger logger;

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = PermohonanDokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Layanan Dokumen list")
    public Response getFilteredArea(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("") @QueryParam("tipeLayanan") String tipeLayanan,
            @DefaultValue("") @QueryParam("idOrganisasi") Long idOrganisasi,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        CriteriaQuery<PermohonanDokumen> rawQuery = permohonanDokumenService.getAllQuery(filter, tipeLayanan, sort, descending, idOrganisasi);
        List<PermohonanDokumenDto> result = permohonanDokumenService.execCriteriaQuery(rawQuery, skip, limit)
                .stream().map(PermohonanDokumenDto::new)
                .collect(Collectors.toList());
        long count = permohonanDokumenService.countCriteriaQuery(rawQuery);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("pending-tasks")
    @ApiOperation(
            value = "Get Need Approval Layanan",
            response = PermohonanDokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Layanan Dokumen list")
    public Response getTasks(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("") @QueryParam("idOrganisasi") Long idOrganisasi,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

        List<PermohonanDokumenDto> result;
        try {
            result = permohonanDokumenService.getPendingTasksResult(filter, sort, descending, skip, limit, idOrganisasi).stream()
                        .map(a -> {
                            PermohonanDokumenDto obj = new PermohonanDokumenDto(a);
                            List<ProcessTask> tasks = processTaskService.getMyTaskByRecordRefId("Layanan Dokumen",a.getId().toString());
                            Long idPenerima = obj.getPenerima().get(0).getIdOrganization();
                            List<String>tmp = new ArrayList<>();
                            tmp.add(idPenerima.toString());
                            Long idAssignee = tasks.get(0).getAssignee().getIdOrganization();
                            List<MasterDelegasi> del = delegasiService.getByPejabatCodeList(tmp);
                            if(!idAssignee.equals(idPenerima)){
                                obj.setIsDelegated(true);
                            }
                            return obj;
                        }).collect(Collectors.toList());
        } catch (Exception ex) {
            ex.printStackTrace();
            result = new ArrayList<>();
        }
        long count = permohonanDokumenService.getTypedQueryCount(filter, sort, descending, idOrganisasi);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("pending-tasks/count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response needAttentionDraftCount(
            @QueryParam("idOrganization") Long idOrganization
    ) {
        try {
            CriteriaQuery<PermohonanDokumen> rawQuery = permohonanDokumenService.getAllQuery(idOrganization);
            long count = permohonanDokumenService.countCriteriaQuery(rawQuery);

            return Response
                    .ok(count)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @ApiOperation(
            response = PermohonanDokumenDto.class,
            value = "Add Layanan Dokumen",
            produces = MediaType.APPLICATION_JSON,
            notes = "Menyimpan Layanan Dokumen")
    public Response add(@Valid PermohonanDokumenDto dao) {
        boolean isValid = permohonanDokumenService.validate(null,dao);
        if(isValid) {
            try {
                Surat surat = suratService.getByIdSurat(dao.getIdSurat());
                if(!surat.getIsPinjam() && dao.getStatus().equals("SUBMITTED")) {
                    PermohonanDokumen obj = permohonanDokumenService.addData(null, dao);
                    permohonanDokumenService.startWorkflow(obj);

                    String info = dao.getStatus().equals("SUBMITTED") ? "berhasil dikirim" : "berhasil disimpan";
                    notificationService.sendInfo("Info", String.format("Data '%s' '%s'", dao.getPerihal(), info));

                    return Response
                            .ok(new MessageDto("info", "Data berhasil disimpan"))
                            .status(Response.Status.OK)
                            .build();
                }else{
                    if(!surat.getIsPinjam() && dao.getStatus().equals("DRAFT")){
                        PermohonanDokumen obj = permohonanDokumenService.addData(null, dao);
                        permohonanDokumenService.startWorkflow(obj);

                        String info = dao.getStatus().equals("SUBMITTED") ? "berhasil dikirim" : "berhasil disimpan";
                        notificationService.sendInfo("Info", String.format("Data '%s' '%s'", dao.getPerihal(), info));

                        return Response
                                .ok(new MessageDto("info", "Data berhasil disimpan"))
                                .status(Response.Status.OK)
                                .build();
                    }else {
                        String ex = "sedang dalam peminjaman";
                        notificationService.sendError("Error", String.format("Dokumen dengan nomor '%s': %s", dao.getSurat().getNoDokumen(), ex));
                        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                    }
                }
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getPerihal(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }else{
            MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
            List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();
            FieldErrorDto fieldErrorDto = new FieldErrorDto();
            fieldErrorDto.setField("tipeLayanan");
            fieldErrorDto.setMessage("Pengajuan Layanan sudah dilakukan sebelumnya.");
            fieldErrorDtos.add(fieldErrorDto);
            messageDto.setFields(fieldErrorDtos);
            return Response.status(Response.Status.BAD_REQUEST).entity(messageDto).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            response = PermohonanDokumenDto.class,
            value = "Edit Layanan Dokumen",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Layanan Dokumen")
    public Response edit(@PathParam ("id") Long id, @Valid PermohonanDokumenDto dao) {
        boolean isValid = permohonanDokumenService.validate(id,dao);
        if(isValid) {
            try {
                Surat surat = suratService.getByIdSurat(dao.getIdSurat());
                if(!surat.getIsPinjam() && dao.getStatus().equals("SUBMITTED")) {
                    PermohonanDokumen obj = permohonanDokumenService.addData(id, dao);
                    permohonanDokumenService.startWorkflow(obj);

                    String info = dao.getStatus().equals("SUBMITTED") ? "berhasil dikirim" : "berhasil disimpan";
                    notificationService.sendInfo("Info", String.format("Data '%s' '%s'", dao.getPerihal(), info));

                    return Response
                            .ok(new MessageDto("info", "Data berhasil disimpan"))
                            .status(Response.Status.OK)
                            .build();
                }else{
                    if(!surat.getIsPinjam() && dao.getStatus().equals("DRAFT")){
                        PermohonanDokumen obj = permohonanDokumenService.addData(id, dao);
                        permohonanDokumenService.startWorkflow(obj);

                        String info = dao.getStatus().equals("SUBMITTED") ? "berhasil dikirim" : "berhasil disimpan";
                        notificationService.sendInfo("Info", String.format("Data '%s' '%s'", dao.getPerihal(), info));

                        return Response
                                .ok(new MessageDto("info", "Data berhasil disimpan"))
                                .status(Response.Status.OK)
                                .build();
                    }else {
                        String ex = "sedang dalam peminjaman";
                        notificationService.sendError("Error", String.format("Dokumen dengan nomor '%s': %s", dao.getSurat().getNoDokumen(), ex));
                        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
                    }
                }
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getPerihal(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }else{
            MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
            List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();
            FieldErrorDto fieldErrorDto = new FieldErrorDto();
            fieldErrorDto.setField("tipeLayanan");
            fieldErrorDto.setMessage("Pengajuan Layanan sudah dilakukan sebelumnya.");
            fieldErrorDtos.add(fieldErrorDto);
            messageDto.setFields(fieldErrorDtos);
            return Response.status(Response.Status.BAD_REQUEST).entity(messageDto).build();
        }
    }

    @POST
    @Path("approve/{id}")
    @ApiOperation(
            value = "Post Approve Permohonan",
            produces = MediaType.APPLICATION_JSON,
            notes = "Post Approve Permohonan")
    public Response approve(@PathParam ("id") Long id) {
        try{
            permohonanDokumenService.approvePermohonan(id);
            notificationService.sendInfo("Info", String.format("Data berhasil diubah"));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save : %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("perpanjangan/{id}")
    @ApiOperation(
            value = "Pepanjangan Permohonan",
            produces = MediaType.APPLICATION_JSON,
            notes = "Pepanjangan Permohonan")
    public Response extend(@PathParam ("id") Long id,@Valid PermohonanDokumenDto dao) {
        try{
            PermohonanDokumen obj = permohonanDokumenService.perpanjangan(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diperpanjang", obj.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal disimpan"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete (Konsep) Peminjaman/Salinan Dokumen",
            produces = MediaType.APPLICATION_JSON,
            notes = "Menghapus (konsep) permohonan peminjaman/salinan dokumen")
    public Response delete(@PathParam ("id") Long id) {
        try{
            PermohonanDokumen dok = permohonanDokumenService.del(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", dok.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        }catch (Exception ex){
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("kembalikan/{id}")
    @ApiOperation(
            value = "Kembalikan Peminjaman Dokumen",
            produces = MediaType.APPLICATION_JSON,
            notes = "Pengembalian  peminjaman dokumen")
    public Response kembalikan(@PathParam ("id") Long id) {
        try{
            PermohonanDokumen permohonanDokumen = permohonanDokumenService.find(id);
            Surat surat = permohonanDokumen.getSurat();
            if(surat!=null) {
                surat.setIsPinjam(false);
                suratService.edit(surat);
                permohonanDokumen.setStatus("RETURNED");
                permohonanDokumenService.edit(permohonanDokumen);
                notificationService.sendInfo("Info", String.format("Pengembalian dokumen dengan nomor : '%s'  berhasil dilakukan", surat.getNoDokumen()));
                return Response.status(Response.Status.OK).build();
            }else{
                notificationService.sendError("Error", String.format("Dokumen tidak ditemukan, pengembalian gagal dilakukan"));
                return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
            }
        }catch (Exception ex){
            notificationService.sendError("Error", String.format("Pengembalian gagal dilakukan, %s", ex.getMessage()));
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("disposisiPenyerah")
    @ApiOperation(
            value = "Disposisi Penyerah Dokumen",
            produces = MediaType.APPLICATION_JSON,
            notes = "Menyimpan disposisi penyerah dokumen")
    public Response disposisiPenyerah(@Valid PenyerahDisposisiDto dao) {
        try{
            disposisiService.saveOrEdit(null,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }


    @POST
    @Path("task-actions/submit")
    @ApiOperation(
            value = "Task Action Submit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task history")
    public Response taskAction(
            TaskActionDto dto) {
        try {
            try {
                ProcessTask processTask = processTaskService.getTaskId(dto.getTaskId());

                if (processTask == null) {
                    return Response
                            .ok(new MessageDto("error", "Invalid recordRefId"))
                            .build();
                }

                TaskActionResponseDto validTask = generateTaskActions(processTask);

                //INVALID FIELD NAME
                List<String> invalidFieldNames = dto.getForms().stream()
                        .filter(t -> validTask.getForms().stream().noneMatch(f -> f.getName().equals(t.getName())))
                        .map(FormFieldDto::getName)
                        .collect(Collectors.toList());

                if (invalidFieldNames.size() > 0) {
                    throw new WorkflowException(String.format("Error when submit task actions: Invalid field name(s): %s", String.join(", ", invalidFieldNames)), "QT-ERR-CMDA-11");
                }

                //INVALID REQUIRED FIELD
                List<String> invalidRequiredFields = validTask.getForms().stream()
                        .filter(t -> t.getRequiredFn().test(dto) && dto.getForms().stream().noneMatch(f -> f.getName().equals(t.getName()) && StringUtils.isNotEmpty(f.getValue())))
                        .map(t -> String.format("%s: %s", t.getName(), "'Should not be null or empty'"))
                        .collect(Collectors.toList());

                if (invalidRequiredFields.size() > 0) {
                    throw new WorkflowException(String.format("Error when submit task actions: Invalid field value: %s", String.join(", ", invalidFieldNames)), "QT-ERR-CMDA-11");
                }

                Map<String, Object> userResponse = dto.getForms()
                        .stream()
                        .filter(t -> validTask.getForms().stream().anyMatch(v -> v.getPostToCamunda() && v.getName().equals(t.getName())))
                        .collect(HashMap::new, (m, v)->m.put(v.getName(), (Object)v.getValue()), HashMap::putAll);
                MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
                IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();

                final IWorkflowEntity entity = permohonanDokumenService.getEntity(processTask);
                TaskEventHandler taskEventHandler = new TaskEventHandler();
                taskEventHandler
                        .onCompleted((processInstance, status) -> permohonanDokumenService.onWorkflowCompleted(status, processInstance, entity, workflowProvider, null, null));

                workflowProvider.execute(processTask, entity, userResponse, taskEventHandler);

                //#region SEND MAIL NOTIFICATION
                // SEND MAIL TO KONSEPTOR
//                workflowProvider.infoTaskNotification(processTask, entity, null);

                // SEND MAIL TO NEXT APPROVER
//                ProcessInstance processInstance = processTask.getProcessInstance();
//                List<ProcessTask> activeTasks = workflowProvider.getActiveTasks(processInstance);
//
//                for (ProcessTask t : activeTasks) {
//                    workflowProvider.newTaskNotification(t, entity, null);
//                }
                //#endregion

                notificationService.sendInfo("Info", "Persetujuan Layanan berhasil dikirim");

                return Response
                        .ok()
                        .build();
            } catch (Exception e) {
                throw new WorkflowException("Error when submit task actions", "QT-ERR-CMDA-11", e);
            }
        } catch (Exception ex) {
            String errorMessage = Optional.ofNullable(ex.getCause()).map(Throwable::getMessage).orElse("Unknown Error");
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Tugas gagal dikirim: %s", errorMessage));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private TaskActionResponseDto generateTaskActions(ProcessTask processTask) throws IOException {
        TaskActionResponseDto response = new TaskActionResponseDto();
        ProcessInstance processInstance = Optional.ofNullable(processTask).map(ProcessTask::getProcessInstance).orElse(null);
        BpmnModelInstance modelInstance;

        try (InputStream inputStream = new ByteArrayInputStream(processInstance.getProcessDefinition().getDefinition().getBytes(Charset.forName("UTF-8")))) {
            modelInstance = Bpmn.readModelFromStream(inputStream);
        }

        response.setTaskName(processTask.getTaskName());
        response.setActionType(processTask.getTaskType());
        response.setTaskId(processTask.getTaskId());
        response.setAssigneeId(Optional.ofNullable(processTask.getAssignee()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null));

        List<String> approvalActions = modelInstance
                .getModelElementsByType(modelInstance.getModel().getType(UserTask.class))
                .stream()
                .filter(t -> ((UserTask) t).getId().equals(processTask.getTaskName()))
                .flatMap(t -> ((UserTask) t).getOutgoing().stream().map(SequenceFlow::getTarget))
                .filter(t -> t instanceof ExclusiveGateway)
                .flatMap(t -> t.getOutgoing().stream())
                .map(FlowElement::getName)
                .sorted()
                .collect(Collectors.toList());
        FormFieldDto approvalForm = new FormFieldDto();
        approvalForm.setName(processTask.getResponseVar());
        approvalForm.setLabel("Approval");
        approvalForm.setType("select");
        approvalForm.setOtherData(getObjectMapper().writeValueAsString(new OptionsDto(approvalActions)));
        approvalForm.setRequired(true);

        response.getForms().add(approvalForm);

        FormFieldDto commentForm = new FormFieldDto();
        commentForm.setName("Comment");
        commentForm.setLabel("Comment");
        commentForm.setType("input");
        commentForm.setInputType("text");
        commentForm.setRequired(null);
        commentForm.setRequiredFn(taskActionDto -> taskActionDto.getForms().stream().filter(t -> t.getName().equals(approvalForm.getName())).findFirst().map(t -> !t.getValue().equals("Setujui")).orElse(false));
        commentForm.setRequiredEl(String.format("forms[.name == '%s'].value != 'Setujui'", approvalForm.getName()));

        response.getForms().add(commentForm);
        response.setActionLabel(String.format("el: forms[.name == '%s'].value", approvalForm.getName()));

        return response;
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

}
