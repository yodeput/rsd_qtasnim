package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.PenandatanganPermohonan;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.PenandatanganPermohonanService;
import com.qtasnim.eoffice.ws.dto.PenandatanganPermohonanDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("penandatangan-permohonan")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Penandatangan Permohonan Resource",
        description = "WS Endpoint untuk menghandle operasi Penandatangan Permohonan"
)
public class PenandatanganPermohonanResource {
    @Inject
    private PenandatanganPermohonanService penandatanganPermohonanService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Filtered Data",
            response = PenandatanganPermohonanDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Penandatangan Permohonan list")
    public Response get(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<PenandatanganPermohonanDto> result = penandatanganPermohonanService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(PenandatanganPermohonanDto::new).collect(Collectors.toList());
        long count = penandatanganPermohonanService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Penandatangan Permohonan")
    public Response save(@Valid PenandatanganPermohonanDto dao) {
        try {
            penandatanganPermohonanService.save(null, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Penandatangan Permohonan")
    public Response edit(@NotNull @PathParam("id") Long id, @Valid PenandatanganPermohonanDto dao) {
        try {
            penandatanganPermohonanService.save(id, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Penandatangan Permohonan")
    public Response delete(@NotNull @PathParam("id") Long id) {
        try {
            penandatanganPermohonanService.delete(id);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}