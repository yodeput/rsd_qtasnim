package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.jms.JenisDokumenDocLibMessage;
import com.qtasnim.eoffice.rabbitmq.JenisDokumenDocLibExchange;
import com.qtasnim.eoffice.rabbitmq.JenisDokumenDocLibQueue;
import com.qtasnim.eoffice.util.JmsUtil;
import org.apache.commons.lang3.StringUtils;
import com.qtasnim.eoffice.services.Logger;

import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Singleton
@ServerEndpoint("/ws/jenisDokumen/{id-jenis-dokumen}")
public class JenisDokumenWebSocket {
    private static final Map<String, List<Session>> peers = Collections.synchronizedMap(new HashMap<>());

    @Inject
    private Logger logger;

    @OnOpen
    public void onOpen(Session session, @PathParam("id-jenis-dokumen") String idJenisDokumen) {
        Map.Entry<String, List<Session>> entry = peers.entrySet().stream().filter(t -> t.getKey().equals(idJenisDokumen)).findFirst().orElse(null);

        if (entry != null) {
            List<Session> current = entry.getValue();
            current.add(session);

            entry.setValue(current);
        } else {
            peers.put(idJenisDokumen, new ArrayList<>(Collections.singletonList(session)));
        }
    }

    @OnClose
    public void onClose(Session session, @PathParam("id-jenis-dokumen") String idJenisDokumen) {
        Map.Entry<String, List<Session>> entry = peers.entrySet().stream().filter(t -> t.getKey().equals(idJenisDokumen)).findFirst().orElse(null);
        if (entry != null) {
            List<Session> current = entry.getValue().stream().filter(t -> !t.getId().equals(session.getId())).collect(Collectors.toList());

            entry.setValue(current);
        }
    }

    public void onJMSMessage(@Observes @JenisDokumenDocLibMessage Message msg) {
        try {
            String idJenisDokumen = JmsUtil.getStringProperty(msg, "idJenisDokumen");

            if (StringUtils.isNotEmpty(idJenisDokumen)) {
                for (Map.Entry<String, List<Session>> sessionMap: peers.entrySet().stream().filter(t -> t.getKey().equals(idJenisDokumen)).collect(Collectors.toList())) {
                    for (Session session: sessionMap.getValue()) {
                        if (session.isOpen()) {
                            session.getBasicRemote().sendText(msg.getBody(String.class));
                        } else {
                            List<Session> current = sessionMap.getValue().stream().filter(t -> !t.getId().equals(session.getId())).collect(Collectors.toList());
                            sessionMap.setValue(current);
                        }
                    }
                }
            }
        } catch (IOException | JMSException ex) {
            logger.error(null, ex);
        }
    }

    public void onAmqpMessage(@Observes JenisDokumenDocLibQueue event) {
        try {
            String recipients = event.getIdJenisDokumen();

            if (StringUtils.isNotEmpty(recipients)) {
                List<String> recipientList = Arrays.asList(recipients.split(","));

                for (Map.Entry<String, List<Session>> sessionMap: peers.entrySet().stream().filter(t -> recipientList.stream().anyMatch(r -> r.equals(t.getKey()))).collect(Collectors.toList())) {
                    for (Session session: sessionMap.getValue()) {
                        if (session.isOpen()) {
                            session.getBasicRemote().sendText(event.getMessage());
                        } else {
                            List<Session> current = sessionMap.getValue().stream().filter(t -> !t.getId().equals(session.getId())).collect(Collectors.toList());
                            sessionMap.setValue(current);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            logger.error(null, ex);
        }
    }
}
