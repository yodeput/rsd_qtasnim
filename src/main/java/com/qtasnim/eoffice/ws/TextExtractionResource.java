package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.services.TextExtractionService;
import com.qtasnim.eoffice.ws.dto.ListStringDto;
import com.qtasnim.eoffice.ws.dto.ScanDocumentDto;
import com.qtasnim.eoffice.ws.dto.ScanResultDto;
import com.qtasnim.eoffice.ws.dto.TextExtractionDto;
import io.swagger.annotations.*;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.InputStream;

@Path("text-extract")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Text Extaction",
        description = "WS Endpoint untuk meng-ekstrak text dari file (file office & file citra)"
)
public class TextExtractionResource {
    @Context
    private UriInfo uriInfo;

    @Inject
    private TextExtractionService textExtractionService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @POST
    @Path(value = "extract-text")
    @Secured
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Extract Text From File",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response extractText(
            @FormDataParam(value="file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value="file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body
    ) {
        try {
            TextExtractionDto result = textExtractionService.parsingFile(inputStream, fileDisposition, body);

            return Response
                    .ok(result)
                    .build();
        }catch(Exception ex){
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }

    @GET
    @Path("text-extractable-files")
    @ApiOperation(
            value = "Get List Format File Text-extractable",
            response = ListStringDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List All Format File Text-extractable")
    public Response textExtractableFiles() {
        try {
            ListStringDto result = textExtractionService.getTextExtractableFiles();

            long count = result.getValues().size();

            if (result != null) {
                return Response
                        .ok(result)
                        .header("X-Total-Count", count)
                        .header("Access-Control-Expose-Headers", "X-Total-Count")
                        .build();
            } else {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("convert-ocr-pdf")
    @Produces({"application/pdf"})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response connvertOcr(
            @FormDataParam(value="file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value="file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body) {
        try {
            String fileName = fileDisposition.getFileName();
            String fileNameShort = FileUtility.GetFileNameWithoutExtension(fileName);
            String fileFormat = FileUtility.GetFileExtension(fileName);
            ScanResultDto result = textExtractionService.parsingFileFromScan(inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", "Konversi OCR Berhasil");

            return Response
                    .ok(result.getPdf())
                    .header("Content-Disposition", String.format("attachment; filename=%s-ocr.pdf", fileNameShort))
                    .build();
        } catch (Exception e) {
            logger.error(null, e);
            notificationService.sendError("Error", String.format("Konversi OCR Gagal: %s", e.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
