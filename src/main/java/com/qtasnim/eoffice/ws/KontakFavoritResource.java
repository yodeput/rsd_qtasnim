package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.KontakFavorit;
import com.qtasnim.eoffice.db.MasterKorsa;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.KontakFavoritService;
import com.qtasnim.eoffice.services.MasterKorsaService;
import com.qtasnim.eoffice.ws.dto.KontakFavoritDto;
import com.qtasnim.eoffice.ws.dto.MasterKorsaDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Path("kontak-favorit")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Kontak Favorit Resource",
        description = "WS Endpoint untuk menghandle operasi Kontak Favorit"
)
public class KontakFavoritResource {

    @Inject
    private KontakFavoritService kontakFavoritService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data Kontak Favorit",
            response = KontakFavoritDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kontak Favorit list by User Session")
    public Response getAll(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("")
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        HashMap<String, Object> temp = kontakFavoritService.getAll(filter, skip, limit, descending, sort);
        List<KontakFavoritDto> result = (List<KontakFavoritDto>) temp.get("data");
        long count = (long) temp.get("length");
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

//    @GET
//    @Path("filter")
//    @ApiOperation(
//            value = "Get Filtered Data",
//            response = KontakFavoritDto[].class,
//            produces = MediaType.APPLICATION_JSON,
//            notes = "Return Kontak Favorit list Filtered")
//    public Response getFiltered(
//            @DefaultValue("0") @QueryParam("skip") int skip,
//            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
//            @DefaultValue("") @QueryParam("filter") String filter,
//            @QueryParam("sort") String sort,
//            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
//        List<KontakFavoritDto> result = kontakFavoritService.getResultList(filter, sort, descending, skip, limit).stream()
//                .map(KontakFavoritDto::new).collect(Collectors.toList());
//        long count = kontakFavoritService.count(filter);
//        List<KontakFavoritDto> result = kontakFavoritService.getAll(filter,skip,limit);
//        long count = result.size();
//
//        return Response
//                .ok(result)
//                .header("X-Total-Count", count)
//                .header("Access-Control-Expose-Headers", "X-Total-Count")
//                .build();
//    }
    @POST
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Favorite")
    public Response insertFavorite(@Valid KontakFavoritDto dao) {
        try {
            kontakFavoritService.saveOrEdit(null, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Kontak Favorit")
    public Response editKorsa(@NotNull @PathParam("id") Long id, @Valid KontakFavoritDto dao) {
        try {
            kontakFavoritService.saveOrEdit(id, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("toggledFavourite/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Kontak Favorit")
    public Response editKorsa(@NotNull @PathParam("id") Long idOrganisasi) {
        try {
            boolean result = kontakFavoritService.toggledFavourite(idOrganisasi);
            return Response.ok(result).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Kontak Favorite")
    public Response delete(@NotNull @PathParam("id") Long id) {
        try {
            KontakFavorit kontak = kontakFavoritService.find(id);
            kontakFavoritService.remove(kontak);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("fav/{idOrg}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Kontak Favorite by Organization")
    public Response deleteByOrg(@NotNull @PathParam("idOrg") Long idOrg) {
        try {
            KontakFavorit kontak = kontakFavoritService.getByIdOrg(idOrg);
            if (kontak != null) {
                kontakFavoritService.remove(kontak);
            }
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}
