package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterDelegasi;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterDelegasiService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.FieldErrorDto;
import com.qtasnim.eoffice.ws.dto.MasterDelegasiDto;
import com.qtasnim.eoffice.ws.dto.MasterDelegasiTujuanDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-delegasi")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = " Pengaturan Penunjukan PYMT dan Pelakhar",
        description = "WS Endpoint untuk menghandle Pengaturan Delegasi"
)
public class MasterDelegasiResource {
    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get Data Delegasi",
            response = MasterDelegasi[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Delegasi list")
    public Response getArea(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit) throws InternalServerErrorException {
        JPAJinqStream<MasterDelegasi> all = masterDelegasiService.getAll();
        List<MasterDelegasiDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterDelegasi::getId))
                .map(MasterDelegasiDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterDelegasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Delegasi list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
//        if (Strings.isNullOrEmpty(filter)) {
//            filter = "isDeleted==false";
//        } else {
//            filter = filter + ";isDeleted==false";
//        }
        List<MasterDelegasiDto> result = masterDelegasiService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterDelegasiDto::new).collect(Collectors.toList());

        long count = masterDelegasiService.count(filter);
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter2")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterDelegasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Delegasi list")
    public Response getFiltered2(@DefaultValue("0") @QueryParam("skip") int skip,
                                 @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                 @DefaultValue("") @QueryParam("filter") String filter,
                                 @QueryParam("sort") String sort,
                                 @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        if (Strings.isNullOrEmpty(filter)) {
            filter = "isDeleted==false";
        } else {
            filter = filter + ";isDeleted==false";
        }
        List<MasterDelegasiDto> result = masterDelegasiService.getResultList(filter, sort, descending, skip, limit*2).stream()
                .map(MasterDelegasiDto::new).collect(Collectors.toList());
        List<MasterDelegasiDto> resultFinal = new ArrayList<>();
        if (result.size() > 0) {
            for (int i = 0; i < result.size(); i++) {
                MasterDelegasiDto obj1 = result.get(i);
                MasterDelegasiDto newObj = new MasterDelegasiDto();
                newObj.setAlasan(obj1.getAlasan());
                newObj.setOrganisasi_from(obj1.getOrganisasi_from());
                newObj.setFrom(obj1.getFrom());
                newObj.setMulai(obj1.getMulai());
                newObj.setSelesai(obj1.getSelesai());
                newObj.setCreatedDate(obj1.getCreatedDate());
                newObj.setModifiedDate(obj1.getModifiedDate());
                newObj.setCompanyCode(obj1.getCompanyCode());
                MasterDelegasiDto obj2 = masterDelegasiService.getByFromTipe(obj1);
                if (obj2 != null) {
                    MasterDelegasiTujuanDto tujuan1 = new MasterDelegasiTujuanDto(obj1.getId(), obj1.getTipe(), obj1.getTo(), obj1.getOrganisasi_to());
                    MasterDelegasiTujuanDto tujuan2 = new MasterDelegasiTujuanDto(obj2.getId(), obj2.getTipe(), obj2.getTo(), obj2.getOrganisasi_to());
                    List<MasterDelegasiTujuanDto> tujuanList = new ArrayList<>();
                    tujuanList.add(tujuan1);
                    tujuanList.add(tujuan2);
                    newObj.setTujuan(tujuanList);
                    resultFinal.add(newObj);
                }
            }
        }

        if (resultFinal.size() > 0) {
            for (int i = 0; i < resultFinal.size(); i++) {
                MasterDelegasiDto obj1 = resultFinal.get(i);
                for (int x = 0; x < resultFinal.size(); x++) {
                    MasterDelegasiDto objx = resultFinal.get(x);
                    if (obj1.getOrganisasi_from().equals(objx.getOrganisasi_from()) && obj1.getSelesai().equals(objx.getSelesai()) && i > x) {
                        resultFinal.remove(objx);
                    }
                }
            }
        }

        long count = resultFinal.size();
        return Response
                .ok(resultFinal)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Delegasi")
    public Response saveDelegasi(@Valid MasterDelegasiDto dao) {
        String hasil = masterDelegasiService.validate(null, dao);
        if (!Strings.isNullOrEmpty(hasil)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {
            try {
                masterDelegasiService.save(null, dao);
                notificationService.sendInfo("Info", "Data berhasil disimpan");

                return Response
                        .ok(new MessageDto("info", "Data berhasil disimpan"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Save: %s", ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Delegasi")
    public Response editDelegasi(@NotNull @PathParam("id") Long id, @Valid MasterDelegasiDto dao) {
        String hasil = masterDelegasiService.validate(id, dao);
        if (!Strings.isNullOrEmpty(hasil)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {
            try {
                masterDelegasiService.save(id, dao);
                notificationService.sendInfo("Info", "Data berhasil diubah");

                return Response
                        .ok(new MessageDto("info", "Data berhasil diubah"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Edit: %s", ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @GET
    @Path("active-by-org-id/{pejabatId}")
    public Response getActiveDelegasi(
            @NotNull @PathParam("pejabatId") Long pejabatId
    ) {
        MasterDelegasi masterDelegasi = masterDelegasiService.findByPejabat(pejabatId);

        if (masterDelegasi != null) {
            return Response.ok(new MasterDelegasiDto(masterDelegasi)).build();
        }

        return Response.ok().build();
    }

    @DELETE
    @Path("{id1}/{id2}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Delegasi")
    public Response deleteDelegasi(@NotNull @PathParam("id1") Long id1,
                                   @NotNull @PathParam("id2") Long id2) {

        try {
            masterDelegasiService.delete2(id1, id2);
            notificationService.sendInfo("Info", "Data berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private MessageDto prepareMessage(String field) {
        String msg;
        switch (field) {
            case "start":
                msg = "Tanggal Mulai berada pada rentang Penunjukkan PYMT/Pelakhar sebelumnya.";
                break;
            case "end":
                msg = "Tanggal Selesai berada pada rentang Penunjukkan PYMT/Pelakhar sebelumnya.";
                break;
            default:
                msg = "Periode Penujukkan PYMT/Pelakhar sudah dibuat sebelumnya.";
                break;
        }
        MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();

        FieldErrorDto fieldErrorDto = new FieldErrorDto();
        fieldErrorDto.setField(field);
        fieldErrorDto.setMessage(msg);
        fieldErrorDtos.add(fieldErrorDto);

        messageDto.setFields(fieldErrorDtos);

        return messageDto;
    }
}