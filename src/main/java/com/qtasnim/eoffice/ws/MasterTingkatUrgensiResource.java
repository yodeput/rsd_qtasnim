package com.qtasnim.eoffice.ws;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterTingkatUrgensi;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterTingkatUrgensiService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterTingkatUrgensiDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

@Path("master-tingkat-urgensi")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Tingkat Urgensi Resource",
        description = "WS Endpoint untuk menghandle operasi Tingkat Urgensi"
)
public class MasterTingkatUrgensiResource {
    @Inject
    private MasterTingkatUrgensiService service;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get",
            response = MasterTingkatUrgensiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterTingkatUrgensi> all = service.getAll();
        List<MasterTingkatUrgensiDto> result = all
//                .filter(q -> !q.getIsDeleted())
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterTingkatUrgensi::getId))
                .map(MasterTingkatUrgensiDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered",
            response = MasterTingkatUrgensiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Tingkat Urgensi list Filtered")
    public Response getFiltered(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterTingkatUrgensiDto> result = service
                .getResultList(filter, sort, descending, skip, limit)
                .stream()
                .map(MasterTingkatUrgensiDto::new)
                .collect(Collectors.toList());
        long count = service.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            response = MasterTingkatUrgensiDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Save as new")
    public Response saveData(@Valid MasterTingkatUrgensiDto dao) {
        try {
            MasterTingkatUrgensiDto obj = service.saveOrEdit(null,dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            response = MasterTingkatUrgensiDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Save an update")
    public Response editData(@PathParam("id") Long id,@Valid MasterTingkatUrgensiDto dao) {
        try {
            MasterTingkatUrgensiDto obj = service.saveOrEdit(id,dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
//notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getName(), ex.getMessage()));
            notificationService.sendError("Error", String.format("'%s' gagal diubah", dao.getName()));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete")
    public Response deleteData(@PathParam("id") Long id) {
        try {
            MasterTingkatUrgensi obj = service.find(id);
            service.remove(obj);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", obj.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}