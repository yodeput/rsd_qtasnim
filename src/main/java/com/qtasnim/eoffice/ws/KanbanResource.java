package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("kanban")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Kanban Resource",
        description = "WS Endpoint untuk menghandle operasi Kanban"
)
public class KanbanResource {
    @Inject
    private BoardService boardService;

    @Inject
    private BucketService bucketService;

    @Inject
    private CardService cardService;

    @Inject
    private BoardMemberService boardMemberService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @Inject
    private CardChecklistService checklistService;

    @Inject
    private CardChecklistDetailService checkDetailService;

    @Inject
    private MasterKategoriCardService kategoriCardService;

    @GET
    @Path("board")
    @ApiOperation(
            value = "Get Boards",
            response = BoardDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Board list")
    public Response getBoards(@DefaultValue("0") @QueryParam("skip") int skip,
                              @DefaultValue("10") @QueryParam("limit") int limit) throws javax.ws.rs.InternalServerErrorException {

        JPAJinqStream<Board> all = boardService.getBoards();
        List<BoardDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Board::getId))
                .map(BoardDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("board/{id}")
    @ApiOperation(
            value = "Get Boards",
            response = BoardDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Board list")
    public Response getBoards(@Valid @PathParam("id") Long id) throws javax.ws.rs.InternalServerErrorException {
        BoardDto result = new BoardDto(boardService.getBoard(id));
        if(result!=null) {
            return Response
                    .ok(result)
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        }else{
            notificationService.sendError("Error","Gagal mengambil Board");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("board")
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Board")
    public Response saveBoard(@Valid BoardDto dao) throws InternalServerErrorException {
        try{
            Board model = boardService.saveOrEdit(dao,null);
            notificationService.sendInfo("Info", "Board Berhasil ditambahkan");

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error","Gagal menambahkan Board");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("board/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Board")
    public Response editBoard(
            @Valid BoardDto dao,
            @Valid @PathParam("id") Long id
    ) throws InternalServerErrorException {
        try{
            Board model = boardService.saveOrEdit(dao,id);
            notificationService.sendInfo("Info", String.format("Board '%s'  berhasil diubah", model.getTitle()));

            return Response
                    .ok(new MessageDto("info", "Board berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal mengubah Board '%s': %s", dao.getTitle(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("board/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete an Modul")
    public Response deleteBoard(@Valid @PathParam("id") Long id) {
        try {
            Board model = boardService.deleteBoard(id);
            notificationService.sendInfo("Info", String.format("Board '%s'  berhasil dihapus", model.getTitle()));

            return Response
                    .ok(new MessageDto("info", "Board berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Board gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("list")
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Bucket")
    public Response saveBucket(@Valid BucketDto dao) throws InternalServerErrorException {
        try{
            BucketDto model = new BucketDto(bucketService.saveOrEdit(dao,null));
            notificationService.sendInfo("Info", "List Berhasil ditambahkan");

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error","Gagal menambahkan List");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("list/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Bucket")
    public Response editBucket(
            @Valid BucketDto dao,
            @Valid @PathParam("id") Long id) throws InternalServerErrorException {
        try{
            BucketDto model = new BucketDto(bucketService.saveOrEdit(dao,id));
            notificationService.sendInfo("Info", String.format("List '%s'  berhasil diubah", model.getTitle()));

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal mengubah List '%s': %s", dao.getTitle(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("list/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete an Modul")
    public Response deleteBucket(@Valid @PathParam("id") Long id) {
        try {
            Bucket model = bucketService.deleteBucket(id);
            notificationService.sendInfo("Info", String.format("List '%s'  berhasil dihapus", model.getTitle()));

            return Response
                    .ok(new MessageDto("info", "List berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("List gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("card")
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Card")
    public Response saveCard(@Valid CardDto dao) throws InternalServerErrorException {
        try{
            CardDto model = new CardDto(cardService.saveOrEdit2(dao,null));
            notificationService.sendInfo("Info", "Card Berhasil ditambahkan");

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error","Gagal menambahkan Card");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("card/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Card")
    public Response editCard(
            @Valid CardDto dao,
            @Valid @PathParam("id") Long id) throws InternalServerErrorException {
        try{
            CardDto model = new CardDto(cardService.saveOrEdit2(dao,id));
            notificationService.sendInfo("Info", String.format("Card '%s'  berhasil diubah", model.getTitle()));

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal mengubah Card '%s': %s", dao.getTitle(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("card/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete an Card")
    public Response deleteCard(@Valid @PathParam("id") Long id) {
        try {
            Card model = cardService.deleteCard(id);
            notificationService.sendInfo("Info", String.format("Card berhasil dihapus"));

            return Response
                    .ok(new MessageDto("info", "Card berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Card gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("checklist")
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Checklist")
    public Response saveChecklist(@Valid CardChecklistDto dao) throws InternalServerErrorException {
        try{
            CardChecklistDto model = new CardChecklistDto(checklistService.saveOrEdit(dao,null));
            notificationService.sendInfo("Info", "Checklist Berhasil ditambahkan");

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error","Gagal menambahkan Checklist");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("checklist/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Checklist")
    public Response editChecklist(
            @Valid CardChecklistDto dao,
            @Valid @PathParam("id") Long id) throws InternalServerErrorException {
        try{
            CardChecklistDto model = new CardChecklistDto(checklistService.saveOrEdit(dao,id));
            notificationService.sendInfo("Info", String.format("Checklist '%s'  berhasil diubah", model.getTitle()));

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal mengubah Checklist '%s': %s", dao.getTitle(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("checklist/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete an Checklist")
    public Response deleteChecklist(@Valid @PathParam("id") Long id) {
        try {
            CardChecklist model = checklistService.deleteCheckList(id);
            notificationService.sendInfo("Info", String.format("Checklist berhasil dihapus"));

            return Response
                    .ok(new MessageDto("info", "Checklist berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Checklist gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("checkitem")
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Check Item")
    public Response saveCheckItem(@Valid CardChecklistDetailDto dao) throws InternalServerErrorException {
        try{
            CardChecklistDetailDto model = new CardChecklistDetailDto(checkDetailService.saveOrEdit(dao,null));
            notificationService.sendInfo("Info", "Check Item Berhasil ditambahkan");

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error","Gagal menambahkan Check Item");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("checkitem/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Check Item")
    public Response editChecklist(
            @Valid CardChecklistDetailDto dao,
            @Valid @PathParam("id") Long id) throws InternalServerErrorException {
        try{
            CardChecklistDetailDto model = new CardChecklistDetailDto(checkDetailService.saveOrEdit(dao,id));
            notificationService.sendInfo("Info", String.format("Check Item '%s'  berhasil diubah", model.getItemName()));

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal mengubah Check Item '%s': %s", dao.getItemName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("checkitem/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete an Check Item")
    public Response deleteCheckItem(@Valid @PathParam("id") Long id) {
        try {
            CardChecklistDetail model = checkDetailService.deleteCheckListDetail(id);
            notificationService.sendInfo("Info", String.format("Check Item berhasil dihapus"));

            return Response
                    .ok(new MessageDto("info", "Check Item berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Check Item gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("kategori")
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Master Label")
    public Response saveMkategoriCard(@Valid KategoriCardDto dao) throws InternalServerErrorException {
        try{
            KategoriCardDto model = new KategoriCardDto(kategoriCardService.saveOrEdit(dao,null));
            notificationService.sendInfo("Info", "Master Label Berhasil ditambahkan");

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error","Gagal menambahkan Master Label");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("kategori/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Check Item")
    public Response editMkategoriCard(
            @Valid KategoriCardDto dao,
            @Valid @PathParam("id") Long id) throws InternalServerErrorException {
        try{
            KategoriCardDto model = new KategoriCardDto(kategoriCardService.saveOrEdit(dao,id));
            notificationService.sendInfo("Info", String.format("Master Label '%s'  berhasil diubah", model.getName()));

            return Response
                    .ok(model)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal mengubah Master Label '%s': %s", dao.getName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("labels")
    @ApiOperation(
            value = "Get Labels",
            response = KategoriCardDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return labels")
    public Response getLabels(@DefaultValue("0") @QueryParam("skip") int skip,
                              @DefaultValue("10") @QueryParam("limit") int limit) throws javax.ws.rs.InternalServerErrorException {

        JPAJinqStream<MasterKategoriCard> all = kategoriCardService.getAll();
        List<KategoriCardDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterKategoriCard::getId))
                .map(KategoriCardDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("board/member")
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Board Member")
    public Response addBoardMember(
            @Valid BoardMemberDto dao) throws InternalServerErrorException {
        try{
            boardMemberService.saveOrEdit(dao,null);
            notificationService.sendInfo("Info", String.format("Member Berhasil Ditambahkan"));

            return Response
                    .ok()
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal Menambahkan Member"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("board/member/{id}")
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Board Member")
    public Response updateBoardMember(
            @Valid BoardMemberDto dao,@Valid @PathParam("id") Long id) throws InternalServerErrorException {
        try{
            boardMemberService.saveOrEdit(dao,id);
            notificationService.sendInfo("Info", String.format("Member Berhasil Diubah"));

            return Response
                    .ok()
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal Mengubah Member"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
