package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.FormField;
import com.qtasnim.eoffice.db.FormField;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.FormFieldService;
import com.qtasnim.eoffice.ws.dto.FormFieldDto;
import com.qtasnim.eoffice.ws.dto.FormFieldDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("form-field")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Pembuatan Form Field",
        description = "WS Endpoint untuk menghandle Pembuatan Form Field"
)
public class FormFieldResource {
    @Inject
    private FormFieldService formFieldService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data Area",
            response = FormField[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Area list")
    public Response getArea(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<FormField> all = formFieldService.getAll();
        List<FormFieldDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(FormField::getId))
                .map(FormFieldDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Data",
            response = FormFieldDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Form Field list")
    public Response getCountries(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<FormFieldDto> result = formFieldService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(FormFieldDto::new).collect(Collectors.toList());
        long count = formFieldService.count(filter);

        return Response
            .ok(result)
            .header("X-Total-Count", count)
            .header("Access-Control-Expose-Headers", "X-Total-Count")
            .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Form Field")
    public Response saveMetadata(FormFieldDto dao) {

        try{
            // formFieldService.save(null,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Form Field")
    public Response editMetadata(@PathParam("id") Long id, FormFieldDto dao) {

        try{
            // formFieldService.save(id,dao); //pras
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Form Field")
    public Response deleteMetadata(@PathParam("id") Long id) {
        try{
            formFieldService.delete(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}