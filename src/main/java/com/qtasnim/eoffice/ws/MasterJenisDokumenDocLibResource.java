package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.JenisDokumenDocLib;
import com.qtasnim.eoffice.db.MasterJenisDokumen;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.FileService;
import com.qtasnim.eoffice.services.JenisDokumenDocLibService;
import com.qtasnim.eoffice.services.MasterJenisDokumenService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.JenisDokumenDocLibDto;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("master-jenis-dokumen-doclib")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@LocalBean
@Stateless
public class MasterJenisDokumenDocLibResource {
    @Inject
    private JenisDokumenDocLibService jenisDokumenDocLibService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @Inject
    private MasterJenisDokumenService masterJenisDokumenService;

    @Inject
    private FileService fileService;

    @GET
    @Path("{idJenisDokumen}")
    public Response get(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @QueryParam("limit") int limit,
            @PathParam("idJenisDokumen") Long idJenisDokumen) {

        JenisDokumenDocLib activated = jenisDokumenDocLibService.getPublished(idJenisDokumen).findFirst().orElse(null);
        JPAJinqStream<JenisDokumenDocLib> all = jenisDokumenDocLibService.getHistory(idJenisDokumen);
        Long count = all.count();
        List<JenisDokumenDocLibDto> result = all
                .skip(skip)
                .limit(limit)
                .map(t -> {
                    JenisDokumenDocLibDto dto = new JenisDokumenDocLibDto(t, fileService);
                    dto.setIsActive(activated != null && t.getId().equals(activated.getId()));

                    return dto;
                })
                .collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    public Response filter(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending)throws InternalServerErrorException {
        List<com.qtasnim.eoffice.ws.dto.JenisDokumenDocLibDto> result = jenisDokumenDocLibService
                .getResultList(filter, sort, descending, skip, limit)
                .stream()
                .map(t -> new JenisDokumenDocLibDto(t, fileService))
                .collect(Collectors.toList());
        long count = jenisDokumenDocLibService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("published/{idJenisDokumen}")
    public Response getPublished(
            @PathParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @QueryParam("limit") int limit) {

        JPAJinqStream<JenisDokumenDocLib> all = jenisDokumenDocLibService.getPublished(idJenisDokumen);
        Long count = all.count();
        List<JenisDokumenDocLibDto> result = all
                .skip(skip)
                .limit(limit)
                .map(t -> new JenisDokumenDocLibDto(t, fileService))
                .collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("latest/{idJenisDokumen}")
    public Response getLatest(@PathParam("idJenisDokumen") Long idJenisDokumen) {
        JenisDokumenDocLib latest = jenisDokumenDocLibService.getLatest(idJenisDokumen);
        List<JenisDokumenDocLibDto> result = new ArrayList<>();

        if (latest != null) {
            result = Stream.of(latest).map(t -> new JenisDokumenDocLibDto(t, fileService)).collect(Collectors.toList());
        }

        return Response
                .ok(result)
                .header("X-Total-Count", 1)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("save/{idJenisDokumen}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response save(
            @PathParam("idJenisDokumen") Long idJenisDokumen,
            @FormDataParam("dto") JenisDokumenDocLibDto dto,
            @FormDataParam("file") InputStream stream,
            @FormDataParam("file") FormDataBodyPart body) {
        return save(idJenisDokumen, dto, stream, Optional.ofNullable(body).map(t -> t.getFormDataContentDisposition().getFileName()).orElse(null), Optional.ofNullable(body).map(t -> t.getMediaType().toString()).orElse(null));
    }

    public Response save(
            Long idJenisDokumen,
            JenisDokumenDocLibDto dto,
            InputStream stream,
            String fileName,
            String mediaType) {
        MasterJenisDokumen masterJenisDokumen = masterJenisDokumenService.find(idJenisDokumen);

        try {
            JenisDokumenDocLib activated = jenisDokumenDocLibService.getLatest(idJenisDokumen);
            double nextVersion = 0.1;

            if (activated != null) {
                String versionString = activated.getVersion().toString();
                Integer leftPart = Integer.valueOf(versionString.substring(0, versionString.lastIndexOf(".")));
                Integer rightPart = Integer.valueOf(versionString.substring(versionString.lastIndexOf(".") + 1));

                rightPart++;

                nextVersion = Double.valueOf(String.format("%s.%s", leftPart, rightPart));
            }

            JenisDokumenDocLib formDefinition = jenisDokumenDocLibService.upload(idJenisDokumen, dto, nextVersion, stream, fileName, mediaType);
            notificationService.sendInfo("Info", String.format("Form '%s' berhasil disimpan", masterJenisDokumen.getNamaJenisDokumen()));

            return Response
                    .ok(new JenisDokumenDocLibDto(formDefinition, fileService))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save Form '%s': %s", masterJenisDokumen.getNamaJenisDokumen(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("submit/{idJenisDokumen}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response submit(
            @PathParam("idJenisDokumen") Long idJenisDokumen,
            @FormDataParam("dto") JenisDokumenDocLibDto dto,
            @FormDataParam("file") InputStream stream,
            @FormDataParam("file") FormDataBodyPart body) throws InternalServerErrorException {
        return submit(idJenisDokumen, dto, stream, Optional.ofNullable(body).map(t -> t.getFormDataContentDisposition().getFileName()).orElse(null), Optional.ofNullable(body).map(t -> t.getMediaType().toString()).orElse(null));
    }

    public Response submit(
            Long idJenisDokumen,
            JenisDokumenDocLibDto dto,
            InputStream stream,
            String fileName,
            String mediaType) throws InternalServerErrorException {
        MasterJenisDokumen masterJenisDokumen = masterJenisDokumenService.find(idJenisDokumen);

        try {
            JenisDokumenDocLib activated = jenisDokumenDocLibService.getLatest(idJenisDokumen);
            double nextVersion = 1d;

            if (activated != null) {
                nextVersion = Math.floor(activated.getVersion()) + 1;
            }

            DateUtil dateUtil = new DateUtil();
            JenisDokumenDocLib formDefinition = jenisDokumenDocLibService.upload(idJenisDokumen, dto, nextVersion, dateUtil.getDateFromISOString(dto.getActivationDate()), stream, fileName, mediaType);
            notificationService.sendInfo("Info", String.format("Form '%s' berhasil diterbitkan", masterJenisDokumen.getNamaJenisDokumen()));

            return Response
                    .ok(new JenisDokumenDocLibDto(formDefinition, fileService))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Form gagal diterbitkan '%s': %s", masterJenisDokumen.getNamaJenisDokumen(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
