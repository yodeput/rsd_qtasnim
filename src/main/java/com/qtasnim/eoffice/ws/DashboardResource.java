package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterDelegasi;
import com.qtasnim.eoffice.db.MasterNonStruktural;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.ChartDashboardDto;
import com.qtasnim.eoffice.ws.dto.MostLoginDashboardDto;
import com.qtasnim.eoffice.ws.dto.NotificationDto;
import com.qtasnim.eoffice.ws.dto.NotificationTaskDto;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Path("dashboard")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class DashboardResource {

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private SuratService suratService;

    @Inject
    private BerkasService berkasService;

    @Inject
    private ArsipService arsipService;

    @Inject
    private MasterDelegasiService delegasiService;

    @Inject
    private MasterNonStrukturalService nonStrukturalService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private SysLogService sysLogService;

    @GET
    @Path("notification1")
    @ApiOperation(
            value = "Get Notification Document Count",
            response = NotificationTaskDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Notification Document Count list")
    public Response notification1() {
        List<NotificationTaskDto> result = new ArrayList<>();
        Integer tempCountPymt = 0;
        Integer tempCountPosition = 0;
        Integer tempCountPelakhar = 0;
        Integer tempCountTim = 0;
        Integer totalcount = 0;
        Integer countRejected = 0;
        MasterUser user = userSession.getUserSession().getUser();
        if (user.getIsInternal().booleanValue()) {
            /* == Surat Masuk == */

            /*count as user position*/
            tempCountPosition = suratService.suratMasukNeedAttCount(user.getOrganizationEntity().getIdOrganization()).intValue();

            /*count as PYMT*/
            MasterDelegasi pymt = delegasiService.getByTo("PYMT");
            if (pymt != null) {
                tempCountPymt = suratService.suratMasukNeedAttCount(pymt.getFrom().getIdOrganization()).intValue();
            }

            /*count as pelakhar*/
            MasterDelegasi pelakhar = delegasiService.getByTo("Pelakhar");
            if (pelakhar != null) {
                tempCountPelakhar = suratService.suratMasukNeedAttCount(pelakhar.getFrom().getIdOrganization()).intValue();
            }

            /*count as Tim*/
            MasterNonStruktural nonStruktural = nonStrukturalService.getByUserLogin();
            if (nonStruktural != null) {
                tempCountTim = suratService.suratMasukNeedAttCount(nonStruktural.getOrganisasi().getIdOrganization()).intValue();
            }

            NotificationTaskDto dokumenMasuk = new NotificationTaskDto();
            dokumenMasuk.setName("Dokumen Masuk");
            dokumenMasuk.setPelakharCount(tempCountPelakhar);
            dokumenMasuk.setPositionCount(tempCountPosition);
            dokumenMasuk.setPymtCount(tempCountPymt);
            dokumenMasuk.setTeamCount(tempCountTim);
            result.add(dokumenMasuk);

            totalcount = totalcount + tempCountPelakhar + tempCountPosition + tempCountPymt + tempCountTim;

            /*reset value*/
            tempCountPelakhar = 0;
            tempCountPosition = 0;
            tempCountPymt = 0;
            tempCountTim = 0;

            /* == Setujui == */
            tempCountPosition = suratService.suratSetujuNeedAttCount(user.getOrganizationEntity().getIdOrganization()).intValue();

            /*count as PYMT*/
            MasterDelegasi pymt2 = delegasiService.getByTo("PYMT");
            if (pymt2 != null) {
                tempCountPymt = suratService.suratSetujuNeedAttCount(pymt2.getFrom().getIdOrganization()).intValue();
            }

            /*count as pelakhar*/
            MasterDelegasi pelakhar2 = delegasiService.getByTo("Pelakhar");
            if (pelakhar2 != null) {
                tempCountPelakhar = suratService.suratSetujuNeedAttCount(pelakhar2.getFrom().getIdOrganization()).intValue();
            }

            /*count as Tim*/
            MasterNonStruktural nonStruktural2 = nonStrukturalService.getByUserLogin();
            if (nonStruktural2 != null) {
                tempCountTim = suratService.suratSetujuNeedAttCount(nonStruktural2.getOrganisasi().getIdOrganization()).intValue();
            }

            NotificationTaskDto setujui = new NotificationTaskDto();
            setujui.setName("Tugas");
            setujui.setPelakharCount(tempCountPelakhar);
            setujui.setPositionCount(tempCountPosition);
            setujui.setPymtCount(tempCountPymt);
            setujui.setTeamCount(tempCountTim);
            result.add(setujui);

//            NotificationTaskDto task = new NotificationTaskDto();
//            task.setName("Task");
//            task.setPelakharCount(tempCountPelakhar);
//            task.setPositionCount(tempCountPosition);
//            task.setPymtCount(tempCountPymt);
//            task.setTeamCount(tempCountTim);
//            result.add(task);

            /*== Kembalikan ke konseptor ==*/
            countRejected = suratService.countRejectedToPemeriksa(user.getOrganizationEntity().getIdOrganization()).intValue();
            NotificationTaskDto kembali = new NotificationTaskDto();
            kembali.setName("Kembalikan ke Konseptor");
            kembali.setPositionCount(countRejected);
            kembali.setPelakharCount(0);
            kembali.setPymtCount(0);
            kembali.setTeamCount(0);
            result.add(kembali);

            totalcount = totalcount + tempCountPelakhar + tempCountPosition + tempCountPymt + tempCountTim + countRejected;
        }


        return Response
                .ok(result)
                .header("X-Total-Count", totalcount)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("notification2")
    @ApiOperation(
            value = "Get Notification User Count",
            response = NotificationDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Notification User Count list")
    public Response notification2() throws InternalServerErrorException {

//        return Response
//                .ok("[" +
//                        "                {" +
//                        "                    \"name\": \"Chat\"," +
//                        "                    \"count\": null" +
//                        "                }," +
//                        "                {" +
//                        "                    \"name\": \"Regulasi\"," +
//                        "                    \"count\": \"1\"" +
//                        "                }," +
//                        "                {" +
//                        "                    \"name\": \"Khazanah\"," +
//                        "                    \"count\": \"3\"" +
//                        "                }," +
//                        "                {" +
//                        "                    \"name\": \"Pengaturan Tanda Tangan\"," +
//                        "                    \"count\": \"1\"" +
//                        "                }," +
//                        "                {" +
//                        "                    \"name\": \"Pengaturan Arsiparis\"," +
//                        "                    \"count\": \"1\"" +
//                        "                }," +
//                        "                {" +
//                        "                    \"name\": \"Dokumen Belum diberkaskan\"," +
//                        "                    \"count\": \"100\"" +
//                        "                }," +
//                        "                {" +
//                        "                    \"name\": \"Folder arsip belum close\"," +
//                        "                    \"count\": \"45\"" +
//                        "                }" +
//                        "            ]")
//                .build();

        List<NotificationDto> obj = new ArrayList<>();
//        obj.add(new NotificationDto("Chat",0));
//        obj.add(new NotificationDto("Regulasi",0));
//        obj.add(new NotificationDto("Khazanah",0));
//        obj.add(new NotificationDto("Pengaturan Tanda Tangan",0));
//        obj.add(new NotificationDto("Pengaturan Arsiparis",0));


        MasterUser user = userSession.getUserSession().getUser();
        if (user.getIsInternal().booleanValue()) {
            obj.add(new NotificationDto("Dokumen Belum diberkaskan", suratService.getCountSuratBelumDiarsipkan()));
            obj.add(new NotificationDto("Folder arsip belum close", berkasService.getCountBerkasBelumClose()));
        }

        int size = suratService.getCountSuratBelumDiarsipkan() + berkasService.getCountBerkasBelumClose();

        return Response
                .ok(obj)
                .header("X-Total-Count", size)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }


    @GET
    @Path("notification3")
    @ApiOperation(
            value = "Get Notification Count",
            response = NotificationDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Notification Count list")
    public Response getNotif() throws InternalServerErrorException {
        List<NotificationDto> obj = new ArrayList<>();

        obj.add(new NotificationDto("Dokumen Belum diberkaskan", suratService.getCountSuratBelumDiarsipkan()));
        obj.add(new NotificationDto("Folder arsip belum close", berkasService.getCountBerkasBelumClose()));

        return Response
                .ok(obj)
                .header("X-Total-Count", obj.size())
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("chart/outgoing-doc")
    @ApiOperation(
            value = "Get Outgoing Document Count for Chart",
            response = ChartDashboardDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Datasource for Outgoing Document Chart")
    public Response getOutgoingData(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idArea") String idArea,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @QueryParam("idKArsip") Long idKArsip,
            @QueryParam("year") String year,
            @QueryParam("month") String month,
            @QueryParam("date") String date
    ) throws InternalServerErrorException {
        Integer yr = Strings.isNullOrEmpty(year) ? null : Integer.parseInt(year);
        Integer mo = Strings.isNullOrEmpty(month) ? null : Integer.parseInt(month);
        Integer dt = Strings.isNullOrEmpty(date) ? null : Integer.parseInt(date);
        List<ChartDashboardDto> result = suratService.getChartDocKeluar(idJenisDokumen, idArea, idUnit, idKArsip, yr, mo, dt);


        return Response
                .ok(result)
                .header("X-Total-Count", result.size())
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("chart/created-doc")
    @ApiOperation(
            value = "Get Created Document for Chart",
            response = ChartDashboardDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Datasource for Created Document Chart")
    public Response getCreatedDocData(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @QueryParam("start") String start,
            @QueryParam("end") String end
    ) {
        try {
            List<ChartDashboardDto> result = suratService.getChartCreatedDoc(idJenisDokumen, idUnit, start, end);

            return Response
                    .ok(result)
                    .header("X-Total-Count", result.size())
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Terdapat kesalahan."));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("chart/received-doc")
    @ApiOperation(
            value = "Get Incoming Document for Chart",
            response = ChartDashboardDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Datasource for Incoming Document Chart")
    public Response getIncomingDocData(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @QueryParam("start") String start,
            @QueryParam("end") String end
    ) {
        try {
            List<ChartDashboardDto> result = suratService.getChartIncomingDoc(idJenisDokumen, idUnit, start, end);

            return Response
                    .ok(result)
                    .header("X-Total-Count", result.size())
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Terdapat kesalahan."));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("chart/archived-doc")
    @ApiOperation(
            value = "Get Archived Document for Chart",
            response = ChartDashboardDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Datasource for Archived Document Chart")
    public Response getArchivedDocData(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @QueryParam("start") String start,
            @QueryParam("end") String end
    ) {
        try {
            List<ChartDashboardDto> result = suratService.getChartArchivedDoc(idJenisDokumen, idUnit, start, end);

            return Response
                    .ok(result)
                    .header("X-Total-Count", result.size())
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Terdapat kesalahan."));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("chart/destroyed-doc")
    @ApiOperation(
            value = "Get Destroyed Document for Chart",
            response = ChartDashboardDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Datasource for Destroyed Document Chart")
    public Response getDestroyedDocData(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @QueryParam("start") String start,
            @QueryParam("end") String end
    ) {
        try {
            List<ChartDashboardDto> result = arsipService.getChartDestroyedDoc(idJenisDokumen, idUnit, start, end);

            return Response
                    .ok(result)
                    .header("X-Total-Count", result.size())
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Terdapat kesalahan."));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("list/most-login-user")
    @ApiOperation(
            value = "Get List Most Login User",
            response = MostLoginDashboardDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Datasource for List Most Login User")
    public Response getMostLoginUserData(
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @QueryParam("start") String start,
            @QueryParam("end") String end,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending
    ) {
        try {
            HashMap<String, Object> temp = sysLogService.getChartMostLoginUser(idUnit, start, end, skip, limit, sort, descending);
            List<MostLoginDashboardDto> result = (List<MostLoginDashboardDto>) temp.get("data");
            int count = (int) temp.get("length");

            return Response
                    .ok(result)
                    .header("X-Total-Count", count)
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Terdapat kesalahan."));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("list/neglected-inbox")
    @ApiOperation(
            value = "Get Neglected Inbox for List",
            response = MostLoginDashboardDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Datasource for List Neglected Inbox")
    public Response getNeglectedInboxData(
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @QueryParam("start") String start,
            @QueryParam("end") String end,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending
    ) {
        try {
            HashMap<String, Object> temp = suratService.getListNeglectedInbox(idUnit, start, end, skip, limit, sort, descending);
            List<MostLoginDashboardDto> result = (List<MostLoginDashboardDto>) temp.get("data");
            int count = (int) temp.get("length");

            return Response
                    .ok(result)
                    .header("X-Total-Count", count)
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Terdapat kesalahan."));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
