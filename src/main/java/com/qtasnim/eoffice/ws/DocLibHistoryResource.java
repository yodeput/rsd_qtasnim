package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.DocLibHistory;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.FileService;
import com.qtasnim.eoffice.ws.dto.DocLibHistoryDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("doclib-history")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Doc Lib History",
        description = "WS Endpoint untuk kebutuhan manajemen history doc lib"
)
public class DocLibHistoryResource {
    @Inject
    private FileService fileService;

    @Path("{docId}")
    @GET
    @ApiOperation(
            value = "Get All History",
            response = DocLibHistoryDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return History List")
    public Response getAll(
            @PathParam("docId") String docId,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<DocLibHistory> all = fileService.getHistory(docId);
        List<DocLibHistoryDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(DocLibHistory::getVersion).reversed())
                .map(DocLibHistoryDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}
