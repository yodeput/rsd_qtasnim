package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.SysLogService;
import com.qtasnim.eoffice.ws.dto.SysLogDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("syslog")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "SysLog Resource",
        description = "WS Endpoint untuk menghandle operasi Sys-Log"
)
public class SysLogResource {
    @Inject
    private SysLogService sysLogService;

    @Context
    private UriInfo uriInfo;
    
    
    @GET
    @ApiOperation(
            value = "Get Data",
            response = SysLogDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return SysLog list")
    public Response getCountries(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<SysLogDto> result = sysLogService.getResultList(filter, sort, descending, skip, limit).stream().
                map(SysLogDto::new).collect(Collectors.toList());
        long count = sysLogService.count(filter);

        return Response
            .ok(result)
            .header("X-Total-Count", count)
            .header("Access-Control-Expose-Headers", "X-Total-Count")
            .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add SysLog")
    public Response saveSysLog(@Valid SysLogDto dao) {
        try{
            sysLogService.save(null,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    /*@POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit SysLog")
    public Response editSysLog(@NotNull @PathParam("id") Long id, @Valid SysLogDto dao) {

        try{
            sysLogService.save(id,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }*/

    /*@DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete SysLog")
    public Response deleteSysLog(@NotNull @PathParam("id") Long id) {

        try{
            sysLogService.delete(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }*/
}