package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.jms.FrontEndMessage;
import com.qtasnim.eoffice.rabbitmq.FrontEndExchange;
import com.qtasnim.eoffice.rabbitmq.FrontEndQueue;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.util.JmsUtil;
import com.qtasnim.eoffice.ws.dto.WebSocketMessageMap;
import org.apache.commons.lang3.StringUtils;
import com.qtasnim.eoffice.services.Logger;

import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

@Singleton
@ServerEndpoint("/ws/{client-id}")
public class WebSocket {
    private static final Map<String, List<Session>> peers = Collections.synchronizedMap(new HashMap<>());
    private static final Map<String, List<WebSocketMessageMap>> messageMaps = Collections.synchronizedMap(new HashMap<>());

    @Inject
    private Logger logger;

    @Inject
    private ApplicationConfig applicationConfig;

    @OnMessage
    public void onMessage(String message, Session session) {
        if ("ping".equals(message)) {
            try {
                if (session.isOpen()) {
                    session.getBasicRemote().sendText("pong");
                }
            } catch (IOException e) {
                logger.error(null, e);
            }
        }
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("client-id") String username) {
        try {
            DateUtil dateUtil = new DateUtil();
            Map<String, String> queryMap = getQueryMap(session.getQueryString());
            Map.Entry<String, List<Session>> messageEntry = getSessions(username);

            if (messageEntry != null) {
                List<Session> current = messageEntry.getValue();
                current.add(session);

                messageEntry.setValue(current);
            } else {
                peers.put(username, new ArrayList<>(Collections.singletonList(session)));
            }

            Map.Entry<String, List<WebSocketMessageMap>> messagesMapEntry = messageMaps.entrySet().stream().filter(t -> t.getKey().equals(username)).findFirst().orElse(null);
            if (messagesMapEntry != null) {
                Date startDate = queryMap.entrySet().stream().filter(t -> t.getKey().equals("date")).findFirst().filter(t -> StringUtils.isNotEmpty(t.getValue())).map(t -> dateUtil.getDateFromISOString(t.getValue())).orElse(null);
                List<WebSocketMessageMap> unsents = messagesMapEntry.getValue().stream().filter(t -> startDate != null && t.getDate().compareTo(startDate) > 0).sorted(Comparator.comparing(WebSocketMessageMap::getDate)).collect(Collectors.toList());
                for (WebSocketMessageMap unsent: unsents) {
                    try {
                        session.getBasicRemote().sendText(unsent.getMessage());
                    } catch (IOException e) {
                        logger.error(null, e);
                    }
                }

                messagesMapEntry.setValue(new ArrayList<>());
            } else {
                messageMaps.put(username, new ArrayList<>());
            }
        } catch (Exception e) {
            logger.error(null, e);
        }
    }

    @OnClose
    public void onClose(Session session, @PathParam("client-id") String clientId) {
        try {
            Map.Entry<String, List<Session>> entry = getSessions(clientId);

            if (entry != null) {
                List<Session> current = entry.getValue().stream().filter(t -> !t.getId().equals(session.getId())).collect(Collectors.toList());

                entry.setValue(current);
            }
        } catch (Exception e) {
            logger.error(null, e);
        }
    }

    public void onJMSMessage(@Observes @FrontEndMessage Message msg) {
        try {
            String recipients = JmsUtil.getStringProperty(msg, "recipients");
            String messageBody = msg.getBody(String.class);

            sendMessage(messageBody, recipients);
        } catch (IOException | JMSException ex) {
            logger.error(null, ex);
        }
    }

    public void onAmqpMessage(@Observes FrontEndQueue event) {
        try {
            String recipients = event.getRecipients();

            sendMessage(event.getMessage(), recipients);
        } catch (IOException | JMSException ex) {
            logger.error(null, ex);
        }
    }

    private Map.Entry<String, List<Session>> getSessions(String username) {
        return peers.entrySet().stream().filter(t -> t.getKey().equals(username)).findFirst().orElse(null);
    }

    private Map<String, String> getQueryMap(String query) {
        Map<String, String> map = new HashMap<>();
        if (query != null) {
            String[] params = query.split("&");
            for (String param : params) {
                String[] nameval = param.split("=");
                map.put(nameval[0], nameval[1]);
            }
        }
        return map;
    }

    private void sendMessage(String messageBody, String recipients) throws IOException, JMSException {
//        try {
//            String fileName = String.format("rabbitmq-debug%s.log", Optional.ofNullable(System.getProperty("rds.servername")).filter(StringUtils::isNotEmpty).map(t -> "-" + t).orElse(""));
//            String targetPath = applicationConfig.getTempDir() + File.separator + fileName;
//            String logBody = "GET" + " | " + new Date().toString() + " | " + messageBody;
//            Files.write(Paths.get(targetPath), (logBody + System.lineSeparator()).getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
//        } catch (Exception e) {
//
//        }

        if (StringUtils.isNotEmpty(recipients)) {
            List<String> recipientList = Arrays.asList(recipients.split(","));
            List<Map.Entry<String, List<Session>>> entries = peers.entrySet().stream().filter(t -> recipientList.stream().anyMatch(r -> r.equals(t.getKey()))).collect(Collectors.toList());

            for (Map.Entry<String, List<Session>> sessionMap: entries) {
                for (Session session: sessionMap.getValue()) {
                    if (session.isOpen()) {
                        session.getBasicRemote().sendText(messageBody);
                    } else {
                        List<Session> current = sessionMap.getValue().stream().filter(t -> !t.getId().equals(session.getId())).collect(Collectors.toList());
                        sessionMap.setValue(current);

                        recipientList.forEach(recipient -> {
                            messageMaps.entrySet().stream().filter(t -> t.getKey().equals(recipient)).findFirst().ifPresent(entry -> {
                                entry.getValue().add(new WebSocketMessageMap(new Date(), messageBody));
                            });
                        });
                    }
                }
            }

            if (entries.size() == 0) {
                recipientList.forEach(recipient -> {
                    messageMaps.entrySet().stream().filter(t -> t.getKey().equals(recipient)).findFirst().ifPresent(entry -> {
                        entry.getValue().add(new WebSocketMessageMap(new Date(), messageBody));
                    });
                });
            }
        }
    }
}
