package com.qtasnim.eoffice.ws;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.JerseyClient;
import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.PasswordHash;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.UserNotFoundException;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import id.kai.ws.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author seno@qtasnim.com
 */
@Path("auth")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api(
        value = "Auth Resource",
        description = "WS Endpoint untuk menghandle operasi otentifikasi"
)
public class AuthResource {
    @Inject
    private ApplicationConfig applicationConfig;

    @Context
    private HttpServletRequest httpServletRequest;

    @Inject
    private JWTService jwtService;

    @Inject
    private LoginService loginService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    private UserSessionService userSessionService;

    @Inject
    private ResetPasswordService resetPasswordService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private PasswordHash passwordHash;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    @JerseyClient
    private Client client;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private ImageRepositoryService imageRepositoryService;

    @Inject
    private Logger logger;

    @POST
    @Path("login")
    @ApiOperation(
            value = "Login",
            response = LoginDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return security token")
    public Response login(@HeaderParam("user-agent") String userAgent, LoginParam login) throws InternalServerErrorException {
//        MasterUser masterUser = masterUserService.findUserByUsername(login.getUsername());
//        if (masterUser != null) {
//            loginService.login(masterUser, login.getPassword());
//            LoginDto loginDto = createToken(userAgent, masterUser);
//            return Response.status(200).entity(loginDto).build();
//        }
//        throw new UserNotFoundException(String.format("User %s not registered", login.getUsername()));
        MasterUser masterUser = null;
        DateUtil dateUtil = new DateUtil();
        Date current = new Date();
        if (!applicationConfig.getSkipKaiLogin()) {
            try {
                Form form = new Form();
                form.param("username", login.getUsername());
                form.param("password", login.getPassword());

                LoginResponseDto dto = null;
                String result = null;

                try {
                    WebTarget webResource = client.target(UriBuilder.fromUri(applicationConfig.getLoginUrl()).toString());
                    Response response = webResource.request(MediaType.MULTIPART_FORM_DATA_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(Entity.form(form));
                    result = response.readEntity(String.class);
                    dto = getObjectMapper().readValue(result, new TypeReference<LoginResponseDto>(){});
                } catch (Exception e) {
                    logger.error("Error connect to KAI WS", e);
                }

                if (dto != null) {
                    if (dto.getStatus().equals("success")) {
                        LoginSuccessDto successDto = getObjectMapper().readValue(result, new TypeReference<LoginSuccessDto>(){});
                        masterUser = masterUserService.findUser(login.getUsername());

                        if (masterUser == null){
                            ITWSEmployeeDto kaiUser = getKaiUser(login.getUsername());

                            CompanyCode companyCode = companyCodeService.getByCode(kaiUser.getIdBukrs());
                            if(companyCode==null){
                                companyCode = new CompanyCode();
                                companyCode.setName(kaiUser.getTextBukrs());
                                companyCode.setCode(kaiUser.getIdBukrs());
                                companyCode.setAddress("-");
                                companyCode.setEmail("-");
                                companyCode.setUrl("-");
                                companyCode.setDescription("-");
                                companyCode.setIsActive(true);
                                companyCode.setStart(current);
                                companyCode.setEnd(dateUtil.getDefValue());
                                companyCodeService.create(companyCode);
                            }

                                MasterStrukturOrganisasi entity = null;
                                if(!kaiUser.getPlans().equals("99999999")) {
                                    entity = masterStrukturOrganisasiService.getByCode(kaiUser.getPlans());
                                    if (entity == null) {
                                        entity = new MasterStrukturOrganisasi();
                                        entity.setOrganizationCode(kaiUser.getPlans());
                                        entity.setOrganizationName(kaiUser.getPosText());
                                        entity.setIsActive(true);
                                        entity.setIsDeleted(false);
                                        entity.setLevel(0);
                                        entity.setStart(current);
                                        entity.setEnd(dateUtil.getDefValue());
                                        entity.setIsPosisi(true);
                                        entity.setIsChief(false);
                                        masterStrukturOrganisasiService.create(entity);
                                    }
                                }

                            String[] nameParts = kaiUser.getName().split(" ");

                            masterUser = new MasterUser();
                            masterUser.setEmail(kaiUser.getEmailKorporate());
                            masterUser.setEmployeeId(kaiUser.getPernr().toString());
                            masterUser.setLoginUserName(kaiUser.getPernr().toString());
                            masterUser.setNameFront(nameParts[0]);
                            masterUser.setNameMiddleLast(String.join(" ", Arrays.stream(nameParts).skip(1).collect(Collectors.toList())));
                            masterUser.setSalutation(kaiUser.getGenderKey().equals("1") ? "Mrs." : "Mr.");
                            masterUser.setPasswordSalt("cf4365595f6b6f29449d222d9b7157");
                            String hash = passwordHash.getHash(login.getPassword(), masterUser.getPasswordSalt());
                            masterUser.setPasswordHash(hash);
                            masterUser.setCreatedBy("system");
                            masterUser.setModifiedBy("system");
                            masterUser.setCreatedDate(new Date());
                            masterUser.setModifiedDate(new Date());
                            if(entity!=null) {
                                masterUser.setOrganizationEntity(entity);
                                masterUser.setJabatan(entity.getOrganizationName());
                                masterUser.setKedudukan(entity.getOrganizationName());
                            }
//                            masterUser.setPersonalArea(kaiUser.getPsubareaText());
                            masterUser.setBusinessArea(kaiUser.getTextBusArea());
//                            masterUser.setGrade(kaiUser.getGrade());

                            masterUser.setAgama(kaiUser.getAgama());
                            masterUser.setKelamin(kaiUser.getGender());
                            masterUser.setTempatLahir(kaiUser.getBirthplace());
                            masterUser.setTanggalLahir(dateUtil.getBirthDate(kaiUser.getBirthdate()));
                            masterUser.setIsInternal(true);

                            if(kaiUser.getPlans().equals("99999999")){
                                masterUser.setIsActive(false);
                                masterUser.setIsDeleted(true);
                            }else{
                                masterUser.setIsActive(true);
                                masterUser.setIsDeleted(false);
                            }

                            if(StringUtils.isNotEmpty(dto.getProfileUrl())){
                                masterUser.setOtherContactInformation(dto.getProfileUrl());
                            }

                            if (StringUtils.isNotEmpty(successDto.getProfileUrl()) && successDto.getProfileUrl().toLowerCase().endsWith(".jpg")) {
                                try {
                                    ImageRepository uploaded = imageRepositoryService.upload(String.format("%s.jpg", login.getUsername()), new URL(successDto.getProfileUrl()));
                                    masterUser.setImage(uploaded);
                                } catch (Exception e) {

                                }
                            }
                            masterUser.setStart(current);
                            masterUser.setEnd(dateUtil.getDefValue());
                            masterUserService.create(masterUser);
                        } else {
//                        if (masterUser.getGrade() == null || masterUser.getGrade().isEmpty()) {
//                            EmployeeDto kaiUser = getKaiUser(login.getUsername());
//                            masterUser.setGrade(kaiUser.getGrade());
//
//                            masterUserService.edit(masterUser);
//                        }

                            if(masterUser.getIsActive().booleanValue()==false){
                                String msg = "Akun tidak aktif";
                                throw new UserNotFoundException(msg);
                            }else{
                                if(Strings.isNullOrEmpty(masterUser.getOtherContactInformation()) && StringUtils.isNotEmpty(dto.getProfileUrl())) {
                                    masterUser.setOtherContactInformation(dto.getProfileUrl());
                                    masterUserService.edit(masterUser);
                                }
                            }
                        }
                    } else if (dto.getStatus().equals("fail")) {
                        masterUser = masterUserService.findUserByUsername(login.getUsername());
                        if (masterUser != null) {
                            try {
                                loginService.login(masterUser, login.getPassword());
                                LoginDto loginDto = createToken(userAgent, masterUser);
                                logger.log(LogType.LOGIN, String.format("User %s successfully Login", masterUser.getLoginUserName()));
                                return Response.status(200).entity(loginDto).build();
                            }catch (NotAuthorizedException na){
                                throw new NotAuthorizedException(na.getMessage());
                            }
                        }else{
                            throw new UserNotFoundException(String.format("Username/password belum terdaftar"));
                        }
                    }
                }
            } catch (IOException | KeyManagementException | NoSuchAlgorithmException e) {
                throw new InternalServerErrorException("Error when read data from KAI WS", "QT-ERR-WS-02", e);
            }
        } else {
            masterUser = masterUserService.findUserByUsername(login.getUsername());
        }

        if (masterUser != null) {
            if (applicationConfig.getSkipKaiLogin()) {
                try {
                    loginService.login(masterUser, login.getPassword());
                }catch (NotAuthorizedException na){
                    throw new NotAuthorizedException(na.getMessage());
                }
            }
            if(masterUser.getOrganizationEntity()!=null) {
                LoginDto loginDto = createToken(userAgent, masterUser);
                logger.log(LogType.LOGIN, String.format("User %s successfully Login", masterUser.getLoginUserName()));
                return Response.status(200).entity(loginDto).build();
            }else{
                throw new UserNotFoundException(String.format("User tidak memiliki jabatan"));
            }
        }else{
            throw new UserNotFoundException(String.format("Username/password belum terdaftar"));
        }
    }

    @GET
    @Path("logout")
    @Secured
    @ApiOperation(
            value = "Logout",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Invalidate token")
    public MessageDto logout() {
        String username = Optional.ofNullable(userSession).filter(t -> t.getUserSession() != null).map(t -> t.getUserSession().getUser().getLoginUserName()).orElse(null);

        doLogout();

        if (StringUtils.isNotEmpty(username)) {
            logger.log(LogType.LOGIN, String.format("User %s berhasil logged out", username));
        }

        return new MessageDto("info", "User berhasil logged out");
    }

    @POST
    @Path("refresh-token")
    @ApiOperation(
            value = "Refresh token",
            response = RefreshTokenDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Refresh token")
    public Response refresh(@QueryParam("token") String refreshToken) {
        RefreshTokenDto loginDto = refreshToken(refreshToken);

        return Response.status(200).entity(loginDto).build();
    }

    private void doLogout() {
        if (userSession.getUserSession() != null) {
            UserSession session = userSessionService.findByToken(userSession.getUserSession().getToken());
            session.setTokenValidUntil(new Date());
            userSessionService.edit(session);
        }
    }

    private LoginDto createToken(String userAgent, MasterUser masterUser) {
        UserSession userSession = new UserSession();

        if(masterUser.getIsInternal()) {
            userSession = jwtService.generateJWTToken(masterUser, applicationContext.getApplicationConfig());
            userSession.setIpAddress(httpServletRequest.getRemoteAddr());
            userSession.setClient(userAgent);
            userSessionService.create(userSession);
        }else{
            userSession = jwtService.generateJWTToken2(masterUser, applicationContext.getApplicationConfig());
            userSession.setIpAddress(httpServletRequest.getRemoteAddr());
            userSession.setClient(userAgent);
            userSessionService.create(userSession);
        }

        DateUtil dateUtil = new DateUtil();
        LoginDto loginDto = new LoginDto();
        loginDto.setToken(userSession.getToken());
        loginDto.setRefreshToken(userSession.getRefreshToken());
        loginDto.setValidUntil(dateUtil.formatDate(userSession.getTokenValidUntil(), DateUtil.DATE_FORMAT_BIASA));
        loginDto.setRefreshTokenValidUntil(dateUtil.formatDate(userSession.getRefreshTokenValidUntil(), DateUtil.DATE_FORMAT_BIASA));
        return loginDto;
    }

    private RefreshTokenDto refreshToken(String refreshToken) {
        UserSession userSession = userSessionService.findValidRefreshToken(refreshToken);
        jwtService.refreshJWTToken(userSession, applicationContext.getApplicationConfig());

        userSessionService.edit(userSession);

        DateUtil dateUtil = new DateUtil();
        RefreshTokenDto loginDto = new RefreshTokenDto();
        loginDto.setToken(userSession.getToken());
        loginDto.setValidUntil(dateUtil.getCurrentISODate(userSession.getTokenValidUntil()));
        return loginDto;
    }


    @POST
    @Path("forgot-password")
    @ApiOperation(
            value = "Lupa Password",
            produces = MediaType.APPLICATION_JSON,
            notes = "Lupa Password")
    public Response forgotPassword(
            @QueryParam("email") String email,
            @QueryParam("callback") String callback
    ) {
        try{
            MasterUserDto user = new MasterUserDto();
            if(!Strings.isNullOrEmpty(email)){
                user = masterUserService.getByEmailAdd(email);
                if (user!=null) {
                    if(user.getIsInternal().booleanValue()==false) {
                        UUID rndmId = UUID.randomUUID();
                        String subject = "Permintaan Reset Password";
                        String messageBody = Optional.ofNullable(callback).filter(StringUtils::isNotEmpty).orElse(applicationConfig.getFrontEndUrl() + "/login#token=") + rndmId.toString();
                        notificationService.sendEmail(subject, messageBody, masterUserService.find(user.getId()));
                        /*saved data to tabel reset password*/
                        ForgotPassword obj = new ForgotPassword();
                        obj.setUserId(user.getId());
                        obj.setIsValid(true);
                        obj.setUuid(rndmId.toString());
                        resetPasswordService.create(obj);
                        return Response.ok(new MessageDto("info", "Permintaan Reset Password berhasil")).build();
                    }else{
                        /*Logic untuk reset/change password internal*/
                        try{
                            WebTarget webResource = client.target(UriBuilder.fromUri(applicationConfig.getForgotPasswordUrl()).toString());
                            JSONObject userBody = new JSONObject();
                            userBody.put("username",user.getEmployeeId());
                            Response response = webResource
                                    .request(MediaType.MULTIPART_FORM_DATA_TYPE).accept(MediaType.APPLICATION_JSON_TYPE)
                                    .header("Authorization", String.format("Bearer %s", applicationConfig.getSyncAuthBearer()))
                                    .post(Entity.entity(userBody,MediaType.APPLICATION_JSON_TYPE));
                            return Response.ok(new MessageDto("info", "Permintaan Reset Password berhasil")).build();
                        }catch(Exception ex){
                            throw new InternalServerErrorException(ex.getMessage(), ex);
                        }
                    }
                } else{
                    throw new InternalServerErrorException("Email tidak valid");
                }
            }else{
                throw new InternalServerErrorException("Email tidak boleh kosong");
            }
        } catch (Exception ex){
            throw new InternalServerErrorException(ex.getMessage(), ex);
        }
    }

    @POST
    @Path("reset-password")
    @ApiOperation(
            value = "Reset Password",
            produces = MediaType.APPLICATION_JSON,
            notes = "Reset Password")
    public Response resetPassword(
            ResetPasswordDto dao,
            @QueryParam("id") String id
    ) {
        try{
            ForgotPasswordDto data = resetPasswordService.getDataByUUID(id);
            if(data!=null) {
                MasterUser user = masterUserService.find(data.getUserId());
                String hash = passwordHash.getHash(dao.getNewPassword(), user.getPasswordSalt());
                user.setPasswordHash(hash);
                masterUserService.edit(user);
                resetPasswordService.destroyValidate(data);
                return Response.ok(new MessageDto("info", "Reset Password berhasil")).build();
            }else{
                throw new InternalServerErrorException("Token tidak valid");
            }
        } catch (Exception ex){
            throw new InternalServerErrorException(ex.getMessage(), ex);
        }

    }

    @POST
    @Path("login-pattern")
    @ApiOperation(
            value = "Login Pattern",
            response = LoginDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return security token")
    public Response loginPattern(@HeaderParam("user-agent") String userAgent, @QueryParam("username") String username,
                                 @QueryParam("pattern") String pattern) throws InternalServerErrorException {
        MasterUser masterUser = masterUserService.findUserByUsername(username);
        if (masterUser != null && !Strings.isNullOrEmpty(pattern)) {
            loginService.patternAuth(masterUser, pattern);
            LoginDto loginDto = createToken(userAgent, masterUser);
            return Response.status(200).entity(loginDto).build();
        }
        throw new UserNotFoundException(String.format("User %s not registered", username));
    }

    private ITWSEmployeeDto getKaiUser(String nipp) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        UriBuilder baseUri = UriBuilder
                .fromUri(applicationConfig.getHrisUrl())
                .path("get-employee-by");
        baseUri.queryParam("nipp", nipp);
        WebTarget webResource = client.target(baseUri.toString());
        Response response = webResource
                .request()
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + applicationConfig.getHrisAuthBearer())
                .get();
        String result = response.readEntity(String.class);

        ITWSEmployeeSearchDto employeeDto = getObjectMapper().readValue(result, new TypeReference<ITWSEmployeeSearchDto>(){});

        return employeeDto.getData().stream().findFirst().orElse(null);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

        return objectMapper;
    }
}
