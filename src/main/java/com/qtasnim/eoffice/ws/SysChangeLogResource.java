package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.SysChangeLog;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.services.SysChangeLogDetailService;
import com.qtasnim.eoffice.services.SysChangeLogService;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import com.qtasnim.eoffice.ws.dto.SysChangeLogDetailDto;
import com.qtasnim.eoffice.ws.dto.SysChangeLogDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

@Path("syschangelog")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "SysChangeLog Resource",
        description = "WS Endpoint untuk menghandle operasi SysChangeLog"
)
public class SysChangeLogResource {

    @Inject
    private SysChangeLogService service;

    @Inject
    private SysChangeLogDetailService serviceDetail;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = SysChangeLogDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return SysChangeLog list")
    public Response getCountries(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

        List<SysChangeLog> all = service.getAll().collect(Collectors.toList());
        if (!filter.isEmpty()) {
            all = service.getAll().where(q -> q.getVersion().contains(filter)).collect(Collectors.toList());
        }
        List<SysChangeLogDto> result = service.mapToDto(all).stream().skip(skip).limit(limit).collect(Collectors.toList());
        long count = all.stream().count();
//        List<SysChangeLogDto> result = service.getResultList(filter, sort, descending, skip, limit).stream().
//                map(SysChangeLogDto::new).collect(Collectors.toList());
//        long count = service.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter")
    @ApiOperation(
            value = "Get Data Filter",
            response = SysChangeLogDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return SysChangeLog list")
    public Response getSysChangeLogFilter(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {

        List<SysChangeLogDto> result = service.getResultList(filter, sort, descending, skip, limit).stream().
                map(SysChangeLogDto::new).collect(Collectors.toList());
        long count = service.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
//    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add SysChangeLog")
    public Response saveSysLog(@Valid SysChangeLogDto dao) {
        try {
            service.saveOrEdit(null, dao, dao.getDetail(), null);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getVersion()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("'%s' gagal disimpan", dao.getVersion()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Edit",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Edit SysChangeLog")
    public Response editSysLog(@NotNull @PathParam("id") Long id, @Valid @FormDataParam("data") SysChangeLogDto dao, @FormDataParam("deletedDetailIds") List<Long> deletedDetailsId) {

        try {
            service.saveOrEdit(id, dao, dao.getDetail(), deletedDetailsId);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getVersion()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getVersion(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete SysChangeLog")
    public Response deleteSysLog(@NotNull @PathParam("id") Long id) {

        try {
            SysChangeLog deleted = service.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getVersion()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("detail/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete SysChangeLog")
    public Response deleteSysLogDetail(@NotNull @PathParam("id") Long id) {

        try {
            serviceDetail.delete(id);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}
