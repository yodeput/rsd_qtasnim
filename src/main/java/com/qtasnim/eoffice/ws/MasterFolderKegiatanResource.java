package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.db.MasterFolderKegiatan;
import com.qtasnim.eoffice.db.MasterFolderKegiatanDetail;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.*;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-folder-kegiatan")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Folder Kegiatan Resource",
        description = "WS Endpoint untuk menghandle operasi Folder Kegiatan"
)
public class MasterFolderKegiatanResource {
    @Inject
    private MasterFolderKegiatanService folderKegiatanService;

    @Inject
    private MasterFolderKegiatanDetailService kegiatanDetailService;

    @Inject
    private PersonalArsipService personalArsipService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterFolderKegiatanDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Folder Kegiatan List")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterFolderKegiatan> all = folderKegiatanService.getAll();
        List<MasterFolderKegiatanDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterFolderKegiatan::getId))
                .map(MasterFolderKegiatanDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterFolderKegiatanDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Folder Kegiatan Filtered")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{

        List<MasterFolderKegiatanDto> result = folderKegiatanService.getResultList(filter, sort, descending, skip, limit).stream().
                map(MasterFolderKegiatanDto::new).collect(Collectors.toList());
        long count = folderKegiatanService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }


    @GET
    @Path("detail/filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterFolderKegiatanDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Folder Kegiatan Filtered")
    public Response getDeltailFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{

        List<MasterFolderKegiatanDetailDto> result = kegiatanDetailService.getResultList(filter, sort, descending, skip, limit).stream().
                map(MasterFolderKegiatanDetailDto::new).collect(Collectors.toList());
        long count = folderKegiatanService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Data Folder Kegiatan")
    public Response saveData(@Valid MasterFolderKegiatanDto dao) {
        try {
            folderKegiatanService.saveOrEdit(dao, null);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getFolderName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal disimpan '%s': %s", dao.getFolderName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Data Folder Kegiatan")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterFolderKegiatanDto dao) {
        try {
            folderKegiatanService.saveOrEdit(dao, id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getFolderName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal diubah '%s': %s", dao.getFolderName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Data Folder Kegiatan")
    public Response deleteData(
            @NotNull @PathParam("id") Long id,
            @NotNull @DefaultValue("false") @QueryParam("isPersonalFolder") boolean personalFolder) {
        try {
            folderKegiatanService.deleteFolder(id, personalFolder);
            notificationService.sendInfo("Info", "Data berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"+", "+ex.getMessage()));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new MessageDto("Error", ex.getMessage())).build();
        }
    }

    @GET
    @Path("parent")
    @ApiOperation(
            value = "Get Data By Parent",
            response = FolderDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Folder Kegiatan list By Parent")
    public Response getByParentId(
            @QueryParam("search") String search,
            @QueryParam("idParent") Long idParent,
            @DefaultValue("false") @QueryParam("isPersonal") Boolean isPersonal,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {
        List<FolderDto> result = new ArrayList<>();
        if(idParent==null){
            List<MasterFolderKegiatanDetail>detailKegiatan;
            if(isPersonal){
                detailKegiatan = kegiatanDetailService.getByParentId(idParent,sort,descending, search).collect(Collectors.toList());
                for(MasterFolderKegiatanDetail dt:detailKegiatan){
                    result.add(new FolderDto(dt.getId(),dt.getFolderName(),true,true, dt.getCreatedDate(),null,null, dt.getParent()!=null? dt.getParent().getId() : null, dt.getCompanyCode()));
                }
            }else {
                List<MasterFolderKegiatan> folderKegiatan = folderKegiatanService.getByParentId(idParent, sort, descending).collect(Collectors.toList());
                for (MasterFolderKegiatan fk : folderKegiatan) {
                    result.add(new FolderDto(fk.getId(), fk.getFolderName(), false, fk.getIsAllowAdd(), fk.getCreatedDate(), fk.getStart(), fk.getEnd(), fk.getParent() != null ? fk.getParent().getId() : null, fk.getCompanyCode()));
                }
            }
        }else{
            List<MasterFolderKegiatanDetail>detailKegiatan;
           if(isPersonal){
               detailKegiatan = kegiatanDetailService.getByParentId(idParent,sort,descending, search).collect(Collectors.toList());
               for(MasterFolderKegiatanDetail dt:detailKegiatan){
                   result.add(new FolderDto(dt.getId(),dt.getFolderName(),true,true, dt.getCreatedDate(),null,null, dt.getParent()!=null? dt.getParent().getId() : null, dt.getCompanyCode()));
               }
           }else{
               MasterFolderKegiatan fld = folderKegiatanService.find(idParent);
               if(fld.getFolderName().toLowerCase().equals("personal")){
                   detailKegiatan = kegiatanDetailService.getByParentId(null,sort,descending, search).collect(Collectors.toList());
                   for(MasterFolderKegiatanDetail dt:detailKegiatan){
                       result.add(new FolderDto(dt.getId(),dt.getFolderName(),true,true,dt.getCreatedDate(),null,null, dt.getParent()!=null? dt.getParent().getId() : null, dt.getCompanyCode()));
                   }
               }else{
                   List<MasterFolderKegiatan> folderKegiatan = folderKegiatanService.getByParentId(idParent,sort,descending).collect(Collectors.toList());
                   for(MasterFolderKegiatan fk:folderKegiatan){
                       result.add(new FolderDto(fk.getId(),fk.getFolderName(),false,fk.getIsAllowAdd(),fk.getCreatedDate(),fk.getStart(),fk.getEnd(), fk.getParent()!=null? fk.getParent().getId() : null , fk.getCompanyCode()));
                   }
               }
           }
        }

        long count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path(value = "upload/{idFolder}")
    @Secured
    @Produces({ MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Upload File Personal Arsip",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response upload(
            @PathParam("idFolder") Long idFolder,
            @FormDataParam(value="file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value="file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body
    ) {
        try {
            personalArsipService.uploadAttachment(idFolder, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil diunggah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Upload '%s': %s", fileDisposition.getFileName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path(value = "save-scanned-document")
    @Secured
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Scan ",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response saveScannedDocument(@QueryParam("idFolder") @NotNull Long idFolder,
                                        @FormDataParam(value="file") InputStream inputStream,
                                        @ApiParam(hidden = true) @FormDataParam(value="file") FormDataContentDisposition fileDisposition,
                                        @FormDataParam("file") FormDataBodyPart body
    ) {
        try {
            ScanDocumentDto result = personalArsipService.scanDocument(idFolder, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("Save Scan '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(result)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save Scan '%s': %s", fileDisposition.getFileName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("getPersonalFileByIdPersonalArsip")
    @ApiOperation(
            value = "Get Personal Arsip Files",
            response = GlobalAttachmentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Personal Arsip File List")
    public Response getArsipFileByIdPersonalArsip(
            @DefaultValue("") @QueryParam("idPersonalArsip") Long idPersonalArsip,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit){
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getAttachmentDataByRef("t_personal_arsip",idPersonalArsip);
        List<GlobalAttachmentDto> result =all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(t -> globalAttachmentService.getLink(t))
                .collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getPersonalFile")
    @ApiOperation(
            value = "Get Personal Arsip Files",
            response = GlobalAttachmentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Personal Arsip File List")
    public Response getArsipFile(
            @QueryParam("search") String search,
            @DefaultValue("") @QueryParam("idFolder") Long idFolder,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit){
        List<Long> idPersonalArsips = personalArsipService.getByIdFolder(idFolder);

        List<GlobalAttachmentDto> result = new LinkedList<>();

        long count = 0;
        for(Long idPersonalArsip : idPersonalArsips) {
            JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getAttachmentDataByRefWithSearch("t_personal_arsip", idPersonalArsip, search);

            result.addAll(all
                    .skip(skip)
                    .limit(limit)
                    .sorted(Comparator.comparing(GlobalAttachment::getId))
                    .map(t -> globalAttachmentService.getLink(t))
                    .collect(Collectors.toList()));

            count+=all.count();
        }

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @DELETE
    @Path("file/{id}")
    @ApiOperation(
            value = "Delete",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete File Personal")
    public Response deleteFile(
            @NotNull @PathParam("id") Long idGlobal) {
        try {
            GlobalAttachment deleted = folderKegiatanService.deleteFile(idGlobal);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil dihapus", deleted.getDocName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete File: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("File gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new MessageDto("Error", ex.getMessage())).build();
        }
    }

    @GET
    @Path("getFolder")
    @ApiOperation(
            value = "Get Object Folder",
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Object Folder")
    public Response getFolder(
            @NotNull @QueryParam("idFolder") Long idFolder,
            @NotNull @QueryParam("isPersonal") boolean isPersonal){
        if(!isPersonal){
            MasterFolderKegiatanDto result=new MasterFolderKegiatanDto(folderKegiatanService.find(idFolder));
            return Response.ok(result).build();
        }else{
            MasterFolderKegiatanDetailDto result = new MasterFolderKegiatanDetailDto(kegiatanDetailService.find(idFolder));
            return Response.ok(result).build();
        }
    }
}
