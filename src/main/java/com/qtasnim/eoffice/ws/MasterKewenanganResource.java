package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterKewenangan;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterKewenanganService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.FieldErrorDto;
import com.qtasnim.eoffice.ws.dto.MasterKewenanganDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-kewenangan")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Pengaturan Kewenangan Penandatangan",
        description = "WS Endpoint untuk menghandle Pengaturan Kewenangan Penandatangan"
)
public class MasterKewenanganResource {
    @Inject
    private MasterKewenanganService masterKewenanganService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = MasterKewenangan[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kewenangan Penandatangan list")
    public Response getData(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit) throws InternalServerErrorException {
        JPAJinqStream<MasterKewenangan> all = masterKewenanganService.getAll();
        List<MasterKewenanganDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterKewenangan::getId))
                .map(MasterKewenanganDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter")
    @ApiOperation(
            value = "Get Data",
            response = MasterKewenangan[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kewenangan Penandatangan list")
    public Response getData(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterKewenanganDto> result = masterKewenanganService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterKewenanganDto::new).collect(Collectors.toList());
        long count = masterKewenanganService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Kewenangan Penandatangan")
    public Response saveData(@Valid MasterKewenanganDto dao) {
        boolean isValid = masterKewenanganService.validate(null,dao.getGrade(),dao.getCompanyId());
        if(!isValid){
            MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
            List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();
            FieldErrorDto fieldErrorDto = new FieldErrorDto();
            fieldErrorDto.setField("grade");
            fieldErrorDto.setMessage("Grade sudah di gunakan.");
            fieldErrorDtos.add(fieldErrorDto);
            messageDto.setFields(fieldErrorDtos);
            return Response.status(Response.Status.BAD_REQUEST).entity(messageDto).build();
        } else{
            try {
                masterKewenanganService.saveOrEdit(null, dao);
                notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getKewenangan()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil disimpan"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getKewenangan(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Kewenangan Penandatangan")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterKewenanganDto dao) {
        boolean isValid = masterKewenanganService.validate(id,dao.getGrade(),dao.getCompanyId());
        if(!isValid){
            MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
            List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();
            FieldErrorDto fieldErrorDto = new FieldErrorDto();
            fieldErrorDto.setField("grade");
            fieldErrorDto.setMessage("Grade sudah di gunakan.");
            fieldErrorDtos.add(fieldErrorDto);
            messageDto.setFields(fieldErrorDtos);
            return Response.status(Response.Status.BAD_REQUEST).entity(messageDto).build();
        } else {
            try {
                masterKewenanganService.saveOrEdit(id, dao);
                notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getKewenangan()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil diubah"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getKewenangan(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Kewenangan Penandatangan")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try{
            MasterKewenangan model = masterKewenanganService.find(id);
//            masterKewenanganService.remove(model);
            model.setIsDeleted(true);
            masterKewenanganService.edit(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getKewenangan()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}