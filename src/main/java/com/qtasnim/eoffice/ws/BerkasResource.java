package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.Berkas;
import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.util.StringUtil;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.*;
import liquibase.util.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Path("berkas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Berkas Resource",
        description = "WS Endpoint untuk menghandle operasi Berkas"
)
public class BerkasResource {
    @Inject
    private BerkasService berkasService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private Logger logger;

    @Context
    private UriInfo uriInfo;

    @Inject
    @ISessionContext
    private Session userSession;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = BerkasDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Berkas list")
    public Response getBerkas(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        HashMap<String,Object> temp = berkasService.getAll(filter, sort, descending, skip, limit);
        List<BerkasDto> result = (List<BerkasDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
            .ok(result)
            .header("X-Total-Count", count)
            .header("Access-Control-Expose-Headers", "X-Total-Count")
            .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Data",
            response = BerkasDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Berkas list")
    public Response getBerkasFilter(
            @DefaultValue("false") @QueryParam("isAdmin") boolean isAdmin,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        MasterUser user  = userSession.getUserSession().getUser();
        if(!isAdmin){
            if(Strings.isNullOrEmpty(filter)){
                filter = "unit.id=="+user.getKorsa().getId();
            } else {
                filter = filter + " and unit.id=="+user.getKorsa().getId();
            }
        }
        List<BerkasDto> result = berkasService.getResultList(filter, sort, descending, skip, limit)
                .stream().map(BerkasDto::new).collect(Collectors.toList());
        long count = berkasService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("sidebar")
    @ApiOperation(
            value = "Get Data",
            response = BerkasDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Berkas list")
    public Response getBerkasSidebar(
            @DefaultValue("false") @QueryParam("isAdmin") boolean isAdmin,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        MasterUser user  = userSession.getUserSession().getUser();

        if(!isAdmin){
            filter = "unit.id=="+user.getKorsa().getId();
        }
        List<BerkasSidebarDto> result = berkasService.getResultList(filter, sort, descending, 0, 0)
                .stream().map(q -> {
                    BerkasSidebarDto obj = new BerkasSidebarDto();
                    DateUtil dateUtil = new DateUtil();
                    obj.setId(q.getId());
                    obj.setDate(dateUtil.getCurrentISODate(q.getCreatedDate()));
                    obj.setKodeKlas(q.getKlasifikasiMasalah().getKodeKlasifikasiMasalah());
                    obj.setIdKlas(q.getKlasifikasiMasalah().getIdKlasifikasiMasalah());
                    obj.setNamaKlas(q.getKlasifikasiMasalah().getNamaKlasifikasiMasalah());

                    return obj;
                }).collect(Collectors.toList());
        long count = berkasService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filterByDate")
    @ApiOperation(
            value = "Filtered By Date",
            response = BerkasDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Berkas list")
    public Response getBerkasByDate(
            @QueryParam("start") String start,
            @QueryParam("end") String end,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<BerkasDto> result = berkasService.getByDate(start, end ,filter, sort, descending, skip, limit).stream().
                map(BerkasDto::new).collect(Collectors.toList());
        long count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("byIdBerkas/{id}")
    @ApiOperation(
            value = "Berkas By Date",
            response = BerkasDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Berkas")
    public Response getBerkasById(
            @NotNull @PathParam("id") Long id) throws InternalServerErrorException {
        List<BerkasDto> trow = new ArrayList<>();
        BerkasDto result = new BerkasDto(berkasService.find(id));
        long count = 1;

        trow.add(result);

        return Response
                .ok(trow)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("close")
    @ApiOperation(
            value = "Get close Berkas",
            response = BerkasDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Berkas list dengan status Close")
    public Response getCloseBerkas(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

//        String strFilter = String.format("isDeleted==%s and closedDate!=%s and isAktif==%s", false, null, true);
//        if (!Strings.isNullOrEmpty(filter)) {
//            strFilter = String.format("%s and %s", strFilter, filter);
//        }

        List<BerkasDto> result = berkasService.getResultList(filter, sort, descending, skip, limit)
                .stream().map(BerkasDto::new).collect(Collectors.toList());
        long count = berkasService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("inactive")
    @ApiOperation(
            value = "Inactive Berkas",
            response = BerkasDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Berkas list dengan Status Inactive")
    public Response getInactiveBerkas(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<BerkasDto> result = berkasService.getResultList(filter,sort,skip,limit).stream().map(BerkasDto::new).collect(Collectors.toList());
        long count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("musnah")
    @ApiOperation(
            value = "Berkas Musnah",
            response = BerkasDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Berkas list dengan status Musnah")
    public Response getBerkasMusnah(
            @DefaultValue("") @QueryParam("nomor") String nomor,
            @DefaultValue("") @QueryParam("judul") String judul,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<BerkasDto> result = berkasService.getBerkasMusnah(nomor, judul ,filter, sort, descending, skip, limit).stream().map(BerkasDto::new).collect(Collectors.toList());
        long count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Berkas")
    public Response saveBerkas(@Valid BerkasDto dao) {
        try {
            Berkas berkas = berkasService.save(null, dao);
            notificationService.sendInfo("Info", String.format("Berkas '%s' berhasil disimpan", berkas.getNomor()));

            return Response
                    .ok(new MessageDto("info", "Berkas berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Berkas '%s' gagal disimpan: %s", dao.getJudul(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Berkas")
    public Response editBerkas(@PathParam("id") Long id, @Valid BerkasDto dao) {
        try {
            Berkas berkas = berkasService.save(id, dao);
            notificationService.sendInfo("Info", String.format("Berkas '%s' berhasil diubah", berkas.getNomor()));

            return Response
                    .ok(new MessageDto("info", "Berkas berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Berkas '%s' gagal diubah: %s", dao.getNomor(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete Berkas",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Berkas")
    public Response deleteData(@NotNull @PathParam("id") Long id) {

        try{
            Berkas deleted = berkasService.deleteBerkas(id);
            notificationService.sendInfo("Info", String.format("Berkas '%s' berhasil dihapus", deleted.getJudul()));

            return Response
                    .ok(new MessageDto("info", "Berkas berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete Berkas: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Berkas gagal dihapus"+", "+ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("musnah/{id}")
    @ApiOperation(
            value = "Berkas Musnah",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Berkas, Arsip, ArsipDocLib")
    public Response deleteBerkas(@NotNull @PathParam("id") Long id) {

        try{
            Berkas destroyed = berkasService.deleteMusnah(id);
            notificationService.sendInfo("Info", String.format("Berkas '%s' berhasil dihancurkan", destroyed.getNomor()));

            return Response
                    .ok(new MessageDto("info", "Berkas berhasil dihancurkan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Berkas gagal dihancurkan: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("close/{id}")
    @ApiOperation(
            value = "Close Berkas",
            produces = MediaType.APPLICATION_JSON,
            notes = "Update Close Date pada Berkas")
    public Response close(@NotNull @PathParam("id") Long id) {
        try{
            BerkasDto closed = berkasService.closeBerkas(id);
            notificationService.sendInfo("Info", String.format("Berkas '%s' berhasil ditutup", closed.getNomor()));

            return Response
                    .ok(new MessageDto("info", "Berkas berhasil ditutup"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Berkas gagal ditutup: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("setInActive/{id}")
    @ApiOperation(
            value = "Set Inactive",
            produces = MediaType.APPLICATION_JSON,
            notes = "Update Status Active & Inactive Date pada Berkas")
    public Response setInActive(@NotNull @PathParam("id") Long id) {

        try{
            BerkasDto inactive = berkasService.setInActive(id);
            notificationService.sendInfo("Info", String.format("Berkas '%s' berhasil menjadi tidak aktif", inactive.getNomor()));

            return Response
                    .ok(new MessageDto("info", "Berkas berhasil menjadi tidak aktif"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Berkas gagal menjadi tidak aktif: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("setStatis/{id}")
    @ApiOperation(
            value = "Set Status Berkas Statis",
            produces = MediaType.APPLICATION_JSON,
            notes = "Update Status Berkas menjadi Statis")
    public Response setStatis(@NotNull @PathParam("id") Long id) {

        try{
            Berkas statis = berkasService.setStatis(id);
            notificationService.sendInfo("Info", String.format("Berkas '%s' berhasil ditandai ke statis", statis.getNomor()));

            return Response
                    .ok(new MessageDto("info", "Berkas berhasil ditandai ke statis"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Berkas gagal diubah: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("setStatusAkhirArsipStatis/{id}")
    @ApiOperation(
            value = "Set Status Akhir Arsip Statis",
            produces = MediaType.APPLICATION_JSON,
            notes = "Update Status Akhir Arsip menjadi Statis")
    public Response setStatusAkhir(@NotNull @PathParam("id") Long id) {

        try{
            berkasService.setStatusAkhirArsipStatis(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("updateLastAccess/{id}")
    @ApiOperation(
            value = "Set Tanggal Terakhir Akses",
            produces = MediaType.APPLICATION_JSON,
            notes = "Update Tanggal Terakhir Akses")
    public Response setTglAkses(@NotNull @PathParam("id") Long id) {

        try{
            berkasService.updateLastAccess(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @GET
    @Path("folderKegiatan")
    @ApiOperation(
            value = "Get Berkas by id Folder Kegiatan",
            response = BerkasDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Berkas list dengan parameter id Folder Kegiatan")
    public Response getBerkasByFolder(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("0") @QueryParam("idFolder") Long idFolder) throws InternalServerErrorException {
        JPAJinqStream<Berkas> all = berkasService.getByFolder(idFolder);
        List<BerkasDto> result = all
                .skip(skip)
                .limit(limit)
                .map(BerkasDto::new).collect(Collectors.toList());
        long count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }


    @POST
    @Path(value = "upload/{idBerkas}")
    @Secured
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Upload File Berkas",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response upload(
            @PathParam("idBerkas") Long idBerkas,
            @FormDataParam(value = "file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value = "file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body
    ) {
        try {
            berkasService.uploadAttachment(idBerkas, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil diunggah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal diunggah '%s': %s", fileDisposition.getFileName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("getFileBerkas")
    @ApiOperation(
            value = "Get Arsip Dokumen Files",
            response = GlobalAttachmentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip File List")
    public Response getArsipFile(
            @DefaultValue("") @QueryParam("idBerkas") Long idBerkas,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getAllDataByRef("t_berkas", idBerkas);
        List<GlobalAttachmentDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(t -> globalAttachmentService.getLink(t))
                .collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @DELETE
    @Path("file/{id}")
    @ApiOperation(
            value = "Delete",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete File Personal")
    public Response deleteFile(
            @NotNull @PathParam("id") Long idGlobal) {
        try {
            GlobalAttachment deleted = berkasService.deleteFile(idGlobal);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil dihapus", deleted.getDocName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete File: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("File gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path(value = "save-scanned-document")
    @Secured
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Scan ",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response saveScannedDocument(@QueryParam("idBerkas") @NotNull Long idBerkas,
                                        @FormDataParam(value = "file") InputStream inputStream,
                                        @ApiParam(hidden = true) @FormDataParam(value = "file") FormDataContentDisposition fileDisposition,
                                        @FormDataParam("file") FormDataBodyPart body
    ) {
        try {
            ScanDocumentDto result = berkasService.scanDocument(idBerkas, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("File Scan '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(new MessageDto("info", "File Scan berhasil diunggah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("File Scan gagal diunggah '%s': %s", fileDisposition.getFileName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


}