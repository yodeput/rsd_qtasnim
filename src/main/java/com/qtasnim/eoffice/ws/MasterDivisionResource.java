package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterDivision;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterDivisionService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.FieldErrorDto;
import com.qtasnim.eoffice.ws.dto.MasterDivisionDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;

@Path("master-division")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Division Resource",
        description = "WS Endpoint untuk menghandle operasi Division"
)
public class MasterDivisionResource {

    @Inject
    private MasterDivisionService masterDivisionService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data Division",
            response = MasterDivision[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Division list")
    public Response getDivision(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<MasterDivision> all = masterDivisionService.getAll();
        List<MasterDivisionDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterDivision::getId))
                .map(MasterDivisionDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterDivisionDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Division list")
    public Response getDivision(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterDivisionDto> result = masterDivisionService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterDivisionDto::new).collect(Collectors.toList());
        long count = masterDivisionService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Division")
    public Response saveDivision(@Valid MasterDivisionDto dao) {

        List<MasterDivision> list = masterDivisionService.findAll().stream().filter(q->q.getCompanyCode().getId()
                .equals(dao.getCompanyId())).collect(Collectors.toList());
        String hasil ="";
        if(!list.isEmpty()) {
            hasil = this.validate(null, dao.getNama(), dao.getKode(), dao.getParent(), list);
        }

        if (!StringUtils.isEmpty(hasil)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {

            try {
                masterDivisionService.save(null, dao);
                notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNama()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil disimpan"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                //notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNama(), ex.getMessage()));
                notificationService.sendError("Error", String.format("'%s' gagal disimpan", dao.getNama()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Division")
    public Response editDivision(@NotNull @PathParam("id") Long id, @Valid MasterDivisionDto dao) {

        List<MasterDivision> list = masterDivisionService.findAll().stream().filter(q->q.getCompanyCode().getId()
                .equals(dao.getCompanyId())).collect(Collectors.toList());
        String hasil ="";
        if(!list.isEmpty()) {
            hasil = this.validate(id, dao.getNama(), dao.getKode(), dao.getParent(), list);
        }

        if (!StringUtils.isEmpty(hasil)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {

            try {
                masterDivisionService.save(id, dao);
                notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNama()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil diubah"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNama(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Division")
    public Response deleteDivision(@NotNull @PathParam("id") Long id) {

        try {
            MasterDivision deleted = masterDivisionService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("/parent")
    @ApiOperation(
            value = "Get Data By Parent",
            response = MasterDivisionDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Division Data By Parent")
    public Response getDataByParent(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @QueryParam("idParent") Long idParent,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {
        JPAJinqStream<MasterDivision> all = masterDivisionService.getByParent(idParent, sort, descending);
        List<MasterDivisionDto> result = all
                .skip(skip)
                .limit(limit)
                .map(MasterDivisionDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    private String validate(Long id, String nama, String kode, MasterDivisionDto parent, List<MasterDivision> list) {
        List<MasterDivision> dataList;
        
        if (parent == null) {
            dataList = list.stream().filter(t -> t.getIdParent() == null).collect(Collectors.toList());
        } else {
            dataList = list.stream().filter(t -> t.getIdParent() != null && Objects.equals(t.getIdParent().getId(), parent.getId())).collect(Collectors.toList());
        }
        
        if (id != null) {
            dataList = dataList.stream().filter(q -> !Objects.equals(q.getId(), id)).collect(Collectors.toList());
        }
        
        if (dataList != null) {
            boolean notValidNama = dataList.stream().anyMatch(t -> nama.equals(t.getNama()));
            boolean notValidKode = dataList.stream().anyMatch(t -> kode.equals(t.getKode()));
            boolean notValidAll = dataList.stream().anyMatch(t -> nama.equals(t.getNama()) && kode.equals(t.getKode()));
            return notValidAll ? "all" : notValidKode ? "kode" : notValidNama ? "nama" : "" ;
        }
        
        return "";
    }

    
    private MessageDto prepareMessage(String field) {
        String msg;
        switch (field) {
            case "nama":
                msg = "Nama";
                break;
            case "kode":
                msg = "Kode";
                break;
            default:
                msg = "Nama dan Kode";
                break;
        }
        MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();

        FieldErrorDto fieldErrorDto = new FieldErrorDto();
        fieldErrorDto.setField(field);
        fieldErrorDto.setMessage(msg + " Divisi sudah di gunakan.");
        fieldErrorDtos.add(fieldErrorDto);

        messageDto.setFields(fieldErrorDtos);

        return messageDto;
    }
}
