package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterTindakan;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterTindakanService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterTindakanDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-tindakan")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Tindakan Resource",
        description = "WS Endpoint untuk menghandle operasi Tindakan"
)
public class MasterTindakanResource {
    @Inject
    private MasterTindakanService masterTindakanService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = MasterTindakan[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Tindakan list")
    public Response getCountries(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit) throws InternalServerErrorException {
        JPAJinqStream<MasterTindakan> all = masterTindakanService.getAll();
        List<MasterTindakanDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterTindakan::getId))
                .map(MasterTindakanDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter")
    @ApiOperation(
            value = "Get Data",
            response = MasterTindakanDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Tindakan list")
    public Response getCountries(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterTindakanDto> result = masterTindakanService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterTindakanDto::new)
                .collect(Collectors.toList());
        long count = masterTindakanService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/getByCompanyId/{id}")
    @ApiOperation(
            value = "Get Data",
            response = MasterTindakanDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Tindakan list")
    public Response getByCompanyId(@PathParam("id") Long id,
                                   @DefaultValue("0")
                                   @QueryParam("skip") int skip,
                                   @DefaultValue("10")
                                   @Max(50)
                                   @QueryParam("limit") int limit) throws InternalServerErrorException {
        JPAJinqStream<MasterTindakan> all = masterTindakanService.getByCompanyId(id);
        List<MasterTindakanDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterTindakan::getId))
                .map(MasterTindakanDto::new).collect(Collectors.toList());
        long count = all.count();


        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Tindakan")
    public Response saveTindakan(@Valid MasterTindakanDto dao) {

        try {
            masterTindakanService.save(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNama(), ex.getMessage()));
            notificationService.sendError("Error", String.format("'%s' gagal disimpan", dao.getNama()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Tindakan")
    public Response editTindakan(@NotNull @PathParam("id") Long id, @Valid MasterTindakanDto dao) {

        try {
            masterTindakanService.save(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNama(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Tindakan")
    public Response deleteTindakan(@NotNull @PathParam("id") Long id) {

        try {
            MasterTindakan deleted = masterTindakanService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}