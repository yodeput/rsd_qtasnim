package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.db.Arsip;
import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.ArsipService;
import com.qtasnim.eoffice.services.BerkasService;
import com.qtasnim.eoffice.services.GlobalAttachmentService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.*;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Path("arsip")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Arsip Resource",
        description = "WS Endpoint untuk menghandle operasi Arsip"
)
public class ArsipResource {
    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @Inject
    private ArsipService arsipService;

    @Context
    private UriInfo uriInfo;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    @ISessionContext
    private Session userSession;

    @GET
    @ApiOperation(
            value = "Get Arsip Data",
            response = ArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip list")
    public Response getArsip(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        MasterUser user  = userSession.getUserSession().getUser();
        filter = filter + " and berkas.unit.id=="+user.getKorsa().getId();
        List<ArsipDto> result = arsipService.getResultList(filter, sort, descending, skip, limit)
                .stream().filter(q -> !q.getIsDeleted()).map(ArsipDto::new).collect(Collectors.toList());

        long count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("byIdArsip/{id}")
    @ApiOperation(
            value = "Arsip By Id",
            response = ArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip")
    public Response getArsipById(
            @NotNull @PathParam("id") Long id) throws InternalServerErrorException {
        List<ArsipDto> trow = new ArrayList<>();
        ArsipDto result = new ArsipDto(arsipService.find(id));
        long count = 1;

        trow.add(result);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Arsip")
    public Response saveArsip(@Valid ArsipDto dao) {
        try {
            Arsip arsip = arsipService.save(null, dao);
            notificationService.sendInfo("Info", String.format("Arsip '%s' berhasil disimpan", arsip.getNomor()));

            return Response
                    .ok(new MessageDto("info", "Arsip berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save Arsip '%s': %s", dao.getNomor(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit Arsip",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Arsip")
    public Response editArsip(@PathParam("id") Long id, @Valid ArsipDto dao) {
        try {
            Arsip arsip = arsipService.save(id, dao);
            notificationService.sendInfo("Info", String.format("Arsip '%s' berhasil diubah", arsip.getNomor()));

            return Response
                    .ok(new MessageDto("info", "Arsip berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Unable to Edit Arsip '%s': %s", dao.getNomor(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Arsip")
    public Response deleteArsip(@NotNull @PathParam("id") Long id) {

        try{
            Arsip deleted = arsipService.delete(id);
            notificationService.sendInfo("Info", String.format("Arsip '%s' berhasil dihapus", deleted.getNomor()));

            return Response
                    .ok(new MessageDto("info", "Arsip berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete Arsip: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Arsip gagal dihapus"+", "+ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path(value = "upload/{idArsip}")
    @Secured
    @Produces({ MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Upload File Arsip",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response upload(
            @PathParam("idArsip") Long idArsip,
            @FormDataParam(value="file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value="file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body
    ) {
        try {
            arsipService.uploadAttachment(idArsip, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil diunggah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal diunggah '%s': %s", fileDisposition.getFileName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("getFileArsip")
    @ApiOperation(
            value = "Get Arsip Dokumen Files",
            response = GlobalAttachmentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip File List")
    public Response getArsipFile(
            @DefaultValue("") @QueryParam("idArsip") Long idArsip,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit){
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getAllDataByRef("t_arsip",idArsip);
        List<GlobalAttachmentDto> result =all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(t -> globalAttachmentService.getLink(t))
                .collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path(value = "save-scanned-document")
    @Secured
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Scan ",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response saveScannedDocument(@QueryParam("idArsip") @NotNull Long idArsip,
                                        @FormDataParam(value="file") InputStream inputStream,
                                        @ApiParam(hidden = true) @FormDataParam(value="file") FormDataContentDisposition fileDisposition,
                                        @FormDataParam("file") FormDataBodyPart body
    ) {
        try {
            ScanDocumentDto result = arsipService.scanDocument(idArsip, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("File Scan '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(new MessageDto("info", "File Scan berhasil diunggah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("File Scan gagal diunggah '%s': %s", fileDisposition.getFileName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("file/{id}")
    @ApiOperation(
            value = "Delete",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete File Personal")
    public Response deleteFile(
            @NotNull @PathParam("id") Long idGlobal) {
        try {
            GlobalAttachment deleted = arsipService.deleteFile(idGlobal);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil dihapus", deleted.getDocName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete File: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("File gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}