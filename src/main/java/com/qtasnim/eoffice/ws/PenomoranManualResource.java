package com.qtasnim.eoffice.ws;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.TaskEventHandler;
import com.qtasnim.eoffice.workflows.WorkflowException;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.ExclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.FlowElement;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

@Path("penomoran-manual")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Penomoran Manual Resource",
        description = "WS Endpoint untuk menghandle operasi Penomoran Manual"
)
public class PenomoranManualResource {

    @Inject
    private PenomoranManualService service;

    @Inject
    private CompanyCodeService companyService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private NotificationService notificationService;
    @Inject
    private ProcessTaskService processTaskService;
    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;
    @Inject
    private GlobalAttachmentService globalAttachmentService;
    @Inject
    private FileService fileService;
    @Inject
    private DistribusiCommentService distribusiCommentService;

    @Inject
    private Logger logger;

    @Inject
    private DokumenHistoryService dokHistoryService;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = PenomoranManualDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Penomoran Manual list")
    public Response getdata(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<PenomoranManualDto> result = service.getResultList(filter, sort, descending, skip, limit).stream().map(PenomoranManualDto::new).collect(Collectors.toList());
        long count = service.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Add",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Add Penomoran Manual")
    public Response save(@Valid @FormDataParam("data") PenomoranManualDto dao, @FormDataParam("file1") FormDataBodyPart file1, @FormDataParam("file2") FormDataBodyPart file2) {
        String message = "disimpan";
        try {
            service.saveOrEditTransaction(null, dao, file1, file2, null, null, null);
            if(dao.getStatus().equals("SUBMITTED"))
                message = "dikirim";
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil "+message, dao.getPerihal()));


            return Response
                    .ok(new MessageDto("info", "Data berhasil "+message))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data '%s' gagal "+message, dao.getPerihal()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Edit",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Edit Area")
    public Response editArea(
            @NotNull @PathParam("id") Long id,
            @Valid @FormDataParam("data") PenomoranManualDto dao,
            @FormDataParam("file1") FormDataBodyPart file1,
            @FormDataParam("file2") FormDataBodyPart file2,
            @FormDataParam("deletedPemeriksaIds") List<Long> deletedPemeriksaIds,
            @FormDataParam("deletedPenandatanganIds") List<Long> deletedPenandatanganIds,
            @FormDataParam("deletedPenerimaIds") List<Long> deletedPenerimaIds) {
        String message = "diubah";
        try {
            service.saveOrEditTransaction(id, dao, file1, file2, deletedPemeriksaIds, deletedPenandatanganIds, deletedPenerimaIds);
            if(dao.getStatus().equals("SUBMITTED"))
                message = "dikirim";
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil "+message, dao.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil "+message))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Data '%s' gagal diubah: %s", dao.getPerihal(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Penomoran Manual")
    public Response deleteDistribusi(@NotNull @PathParam("id") Long id) {

        try {
            PenomoranManual deleted = service.find(id);
            deleted.setIsDeleted(true);
            service.edit(deleted);
            notificationService.sendInfo("Info", "Data berhasil dihapus");
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("task-actions/submit")
    @ApiOperation(
            value = "Task Action Submit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task history")
    public Response taskAction(
            TaskActionDto dto) {
        try {
            try {
                ProcessTask processTask = processTaskService.getTaskId(dto.getTaskId());

                if (processTask == null) {
                    return Response
                            .ok(new MessageDto("error", "Invalid recordRefId"))
                            .build();
                }

                TaskActionResponseDto validTask = generateTaskActions(processTask);

                //INVALID FIELD NAME
                List<String> invalidFieldNames = dto.getForms().stream()
                        .filter(t -> validTask.getForms().stream().noneMatch(f -> f.getName().equals(t.getName())))
                        .map(FormFieldDto::getName)
                        .collect(Collectors.toList());

                if (invalidFieldNames.size() > 0) {
                    throw new WorkflowException(String.format("Error when submit task actions: Invalid field name(s): %s", String.join(", ", invalidFieldNames)), "QT-ERR-CMDA-11");
                }

                //INVALID REQUIRED FIELD
                List<String> invalidRequiredFields = validTask.getForms().stream()
                        .filter(t -> t.getRequiredFn().test(dto) && dto.getForms().stream().noneMatch(f -> f.getName().equals(t.getName()) && StringUtils.isNotEmpty(f.getValue())))
                        .map(t -> String.format("%s: %s", t.getName(), "'Should not be null or empty'"))
                        .collect(Collectors.toList());

                if (invalidRequiredFields.size() > 0) {
                    throw new WorkflowException(String.format("Error when submit task actions: Invalid field value: %s", String.join(", ", invalidFieldNames)), "QT-ERR-CMDA-11");
                }

                Map<String, Object> userResponse = dto.getForms()
                        .stream()
                        .filter(t -> validTask.getForms().stream().anyMatch(v -> v.getPostToCamunda() && v.getName().equals(t.getName())))
                        .collect(HashMap::new, (m, v) -> m.put(v.getName(), (Object) v.getValue()), HashMap::putAll);
                MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
                IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();

                final IWorkflowEntity entity = service.getEntity(processTask);
                TaskEventHandler taskEventHandler = new TaskEventHandler();
                taskEventHandler
                        .onCompleted((processInstance, status) -> service.onWorkflowCompleted(status, processInstance, entity, workflowProvider,null,null))
                        .onTaskExecuted((task, userRsp) -> service.onTaskExecuted(dto, workflowProvider, entity, task, taskEventHandler, userResponse));

                workflowProvider.execute(processTask, entity, userResponse, taskEventHandler);

                //#region SEND MAIL NOTIFICATION
                // SEND MAIL TO KONSEPTOR
                service.infoTaskNotification(processTask, entity, null);

                // SEND MAIL TO NEXT APPROVER
                ProcessInstance processInstance = processTask.getProcessInstance();
                List<ProcessTask> activeTasks = workflowProvider.getActiveTasks(processInstance);

                for (ProcessTask t : activeTasks) {
                    service.newTaskNotification(t, entity, null);
                }
                //#endregion

                notificationService.sendInfo("Info", "Persetujuan Layanan berhasil dikirim");

                return Response
                        .ok()
                        .build();
            } catch (Exception e) {
                throw new WorkflowException("Error when submit task actions", "QT-ERR-CMDA-11", e);
            }
        } catch (Exception ex) {
            String errorMessage = Optional.ofNullable(ex.getCause()).map(Throwable::getMessage).orElse("Unknown Error");
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Tugas gagal dikirim: %s", errorMessage));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private TaskActionResponseDto generateTaskActions(ProcessTask processTask) throws IOException {
        TaskActionResponseDto response = new TaskActionResponseDto();
        ProcessInstance processInstance = Optional.ofNullable(processTask).map(ProcessTask::getProcessInstance).orElse(null);
        BpmnModelInstance modelInstance;

        try (InputStream inputStream = new ByteArrayInputStream(processInstance.getProcessDefinition().getDefinition().getBytes(Charset.forName("UTF-8")))) {
            modelInstance = Bpmn.readModelFromStream(inputStream);
        }

        response.setTaskName(processTask.getTaskName());
        response.setActionType(processTask.getTaskType());
        response.setTaskId(processTask.getTaskId());
        response.setAssigneeId(Optional.ofNullable(processTask.getAssignee()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null));

        List<String> approvalActions = modelInstance
                .getModelElementsByType(modelInstance.getModel().getType(UserTask.class))
                .stream()
                .filter(t -> ((UserTask) t).getId().equals(processTask.getTaskName()))
                .flatMap(t -> ((UserTask) t).getOutgoing().stream().map(SequenceFlow::getTarget))
                .filter(t -> t instanceof ExclusiveGateway)
                .flatMap(t -> t.getOutgoing().stream())
                .map(FlowElement::getName)
                .sorted()
                .collect(Collectors.toList());
        FormFieldDto approvalForm = new FormFieldDto();
        approvalForm.setName(processTask.getResponseVar());
        approvalForm.setLabel("Approval");
        approvalForm.setType("select");
        approvalForm.setOtherData(getObjectMapper().writeValueAsString(new OptionsDto(approvalActions)));
        approvalForm.setRequired(true);

        response.getForms().add(approvalForm);

        FormFieldDto commentForm = new FormFieldDto();
        commentForm.setName("Comment");
        commentForm.setLabel("Comment");
        commentForm.setType("input");
        commentForm.setInputType("text");
        commentForm.setRequired(null);
        commentForm.setRequiredFn(taskActionDto -> taskActionDto.getForms().stream().filter(t -> t.getName().equals(approvalForm.getName())).findFirst().map(t -> !t.getValue().equals("Setujui")).orElse(false));
        commentForm.setRequiredEl(String.format("forms[.name == '%s'].value != 'Setujui'", approvalForm.getName()));

        response.getForms().add(commentForm);
        response.setActionLabel(String.format("el: forms[.name == '%s'].value", approvalForm.getName()));

        return response;
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    @GET
    @Path("task-history/{id}")
    @ApiOperation(
            value = "Get Task History",
            response = ProcessTaskDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task history")
    public Response getTaskHistory(@NotNull @PathParam("id") String id) {
        try {
            List<ProcessTask> myTasks = processTaskService.getTaskHistoryByRecordRefId("Penomoran Manual Dokumen", id);

            List<ProcessTaskDto> result = myTasks
                    .stream()
                    .sorted(Comparator.comparing(ProcessTask::getId).reversed())
                    .map(ProcessTaskDto::new)
                    .collect(Collectors.toList());

            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            throw new WorkflowException("Error when get distibusi task history", "QT-ERR-CMDA-08", e);
        }
    }

    @POST
    @Path("read")
    @ApiOperation(
            value = "Read Penomoran Manual",
            produces = MediaType.APPLICATION_JSON,
            notes = "Read to Dokumen History")
    public Response readDistribusi(
            @NotNull @QueryParam("id") Long id) throws InternalServerErrorException {
        try {
            DokumenHistoryDto history = new DokumenHistoryDto();
            history.setReferenceTable("t_distribusi_dokumen");
            history.setReferenceId(id);
            history.setStatus("READ");

            MasterUser usr = userSession.getUserSession().getUser();
            history.setIdOrganisasi(usr.getOrganizationEntity().getIdOrganization());
            history.setIdUser(usr.getId());
            dokHistoryService.saveHistory(history);

            return Response
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            throw new InternalServerErrorException("Gagal mengupdate tanggal.");
        }
    }

   /* @POST
    @Path(value = "upload/{idPenomoran}")
    @Secured
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Upload",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response upload(
            @PathParam("idPenomoran") Long idPenomoran,
            @FormDataParam(value = "file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value = "file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body
    ) throws IOException {
        try {
            service.uploadAttachment(idPenomoran, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil diunggah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("File '%s' gagal diunggah: %s", fileDisposition.getFileName(), ExceptionUtil.getRealCause(ex).getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }*/
}
