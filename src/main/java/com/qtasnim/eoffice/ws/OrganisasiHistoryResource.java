package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.OrganisasiHistory;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.OrganisasiHistoryService;
import com.qtasnim.eoffice.ws.dto.OrganisasiHistoryDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("organisasi-history")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Organisasi History Resource",
        description = "WS Endpoint untuk menghandle operasi Organisasi History"
)
public class OrganisasiHistoryResource {
    @Inject
    private OrganisasiHistoryService organisasiHistoryService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data Organisasi History",
            response = OrganisasiHistory[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Organisasi History list")
    public Response get(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<OrganisasiHistoryDto> all = organisasiHistoryService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(OrganisasiHistoryDto::new).collect(Collectors.toList());
        long count = organisasiHistoryService.count(filter);
        return Response
                .ok(all)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Organisasi History")
    public Response delete(@NotNull @PathParam("id") Long id) {
        try {
            organisasiHistoryService.delete(id);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}