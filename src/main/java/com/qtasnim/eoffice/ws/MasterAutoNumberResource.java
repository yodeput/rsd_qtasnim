package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterAutoNumberDto;
import com.qtasnim.eoffice.ws.dto.MasterKategoriArsipDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-auto-number")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Auto Number",
        description = "WS Endpoint untuk menghandle operasi Auto Number"
)
public class MasterAutoNumberResource {
    @Inject
    private MasterAutoNumberService service;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Inject
    private FormDefinitionService formService;

    @Inject
    private SuratService suratService;

    @GET
    @Path("test")
    public Response test(@QueryParam("id") int id) {
//        FormDefinition fd = formService.find(9L);    // nota dinas
        Surat surat = suratService.find(new Long(id));

        // FORMAT: {MasterJenisDokumen}.{Jabatan}/{MasterKlasifikasiMasalah}/{rom_month:MM}/{value:0}{MasterKlasifikasiKeamanan}/KA-{now:yyyy}
        String number = service.from(Surat.class).next(t -> t.getNoDokumen(), surat);
        surat.setNoDokumen(number);
        suratService.edit(surat);
        System.out.println(number);

        return Response
                .ok("")
                .build();
    }

    @GET
    @ApiOperation(
            value = "Get Data",
            response = MasterAutoNumberDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Auto Number list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterAutoNumber> all = service.getAll();
        List<MasterAutoNumberDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterAutoNumber::getId))
                .map(MasterAutoNumberDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterAutoNumberDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Auto Number list Filtered")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {


        List<MasterAutoNumber> data = service.getResultList(filter, sort, descending, skip, limit);
        long count = service.count(filter);

        List<MasterAutoNumberDto> result = data.stream().map(q->new MasterAutoNumberDto(q)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    public Response saveData(@Valid MasterAutoNumberDto dao) {
        try{
            service.saveOrEdit(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getTitle()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getTitle(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Data Auto Number")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterAutoNumberDto dao) {

        try {
            service.saveOrEdit(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getTitle()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getTitle(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Data Auto Number")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        
        try{
            MasterAutoNumber model = service.find(id);
            service.remove(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getTitle()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}