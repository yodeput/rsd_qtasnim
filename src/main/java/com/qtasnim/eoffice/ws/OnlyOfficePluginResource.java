/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qtasnim.eoffice.ws;

import com.google.gson.JsonObject;
import com.qtasnim.eoffice.services.TemplatingService;
import com.qtasnim.eoffice.util.LayoutingEntity;
import com.qtasnim.eoffice.ws.dto.SuratDto;
import id.kai.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 *
 * @author acdwisu
 */

@Path("o-f-plugin")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api(
        value = "Only Office Plugin",
        description = "WS Endpoint untuk menghandle plugin onlyoffice"
)
public class OnlyOfficePluginResource {
    @Inject
    private TemplatingService templatingService;

    @Context
    private UriInfo uriInfo;
    
    @GET
    @Path("layouting-key/{idJenisDokumen}")
    @ApiOperation(
            value = "Get Layouting Key",
            response = LayoutingEntity[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Layouting Key Jenis Dokumen List berdasarkan idJenisDokumen")
    public Response getLayoutingKey(@PathParam("idJenisDokumen") Long idJenisDokumen) {
        try {
            List<LayoutingEntity> result = templatingService.getLayoutingEntityJenisDokumen(idJenisDokumen);
            long count = result.size();

            if (result != null) {
                return Response
                        .ok(result)
                        .header("X-Total-Count", count)
                        .header("Access-Control-Expose-Headers", "X-Total-Count")
                        .build();
            } else {
                System.out.println("Result getLayoutingKey null");
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch(Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
