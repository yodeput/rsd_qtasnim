package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.config.ApplicationConfig;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.ws.dto.ApplicationContextDto;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.TimeZone;

@Path("application-context")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ApplicationContextResource {
    @Inject
    private ApplicationConfig applicationConfig;

    @GET
    public Response get() {
        ApplicationContextDto dto = new ApplicationContextDto();
        TimeZone currentTimezone = TimeZone.getDefault();
        ZonedDateTime hereAndNow = Instant.now().atZone(currentTimezone.toZoneId());

        dto.setTimezone(currentTimezone.getID());
        dto.setTzOffset(String.format("%tz", hereAndNow));
        dto.setPlugginScanningTwainKey(applicationConfig.getPlugginScanningTwainKey());
        dto.setPlugginScanningTwainIsTrial(applicationConfig.getPlugginScanningTwainIsTrial());
        dto.setApplicationName(applicationConfig.getApplicationName());

        return Response
                .ok(dto)
                .build();
    }
}
