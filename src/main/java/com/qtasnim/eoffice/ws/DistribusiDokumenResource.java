package com.qtasnim.eoffice.ws;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.TaskEventHandler;
import com.qtasnim.eoffice.workflows.WorkflowException;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.ExclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.FlowElement;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

@Path("distribusi-dokumen")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Distribusi Document Resource",
        description = "WS Endpoint untuk menghandle operasi DistribusiDocument"
)
public class DistribusiDokumenResource {

    @Inject
    private DistribusiDokumenService service;

    @Inject
    private CompanyCodeService companyService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private NotificationService notificationService;
    @Inject
    private ProcessTaskService processTaskService;
    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;
    @Inject
    private MasterFolderKegiatanService masterFolderKegiatanService;
    @Inject
    private PersonalArsipService personalArsipService;
    @Inject
    private MasterFolderKegiatanDetailService kegiatanDetailService;
    @Inject
    private BerkasService berkasService;
    @Inject
    private ArsipService arsipService;
    @Inject
    private GlobalAttachmentService globalAttachmentService;
    @Inject
    private FileService fileService;
    @Inject
    private DistribusiCommentService distribusiCommentService;

    @Inject
    private Logger logger;

    @Inject
    private DokumenHistoryService dokHistoryService;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = DistribusiDokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Distribusi Document list")
    public Response getDistribusi(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending,
            @DefaultValue("true") @QueryParam("loadAll") boolean loadAll) throws InternalServerErrorException {
        List<DistribusiDokumenDto> result = service.getResultList(filter, sort, descending, skip, limit).stream().
                map(a->{
                    if(loadAll){
                        return new DistribusiDokumenDto(a,loadAll);
                    }else{
                        DistribusiDokumenDto surat = DistribusiDokumenDto.minimal(a);
                        surat.bindPenandatangan(a);
//                        surat.bindDetail(a);

                        return surat;
                    }
                }).collect(Collectors.toList());
        long count = service.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Add",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Add Distribusi Document")
    public Response saveDistribusi(@Valid @FormDataParam("data") DistribusiDokumenDto dao, @FormDataParam("data") String json) {
        String message = "disimpan";
        try {
            Map<String, Object> values = getObjectMapper().readValue(json, new TypeReference<HashMap<String, Object>>() {
            });
            String password = values.get("password") == null ? "" : values.get("password").toString();
            service.saveOrEditTransaction(null, dao, password, null, null, null, null);
            if(dao.getStatus().equals("SUBMITTED"))
                message = "dikirim";
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil "+message, dao.getPerihal()));


            return Response
                    .ok(new MessageDto("info", "Data berhasil "+message))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data '%s' gagal "+message, dao.getPerihal()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Edit",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Edit Area")
    public Response editArea(
            @NotNull @PathParam("id") Long id,
            @Valid @FormDataParam("data") DistribusiDokumenDto dao,
            @FormDataParam("data") String json,
            @FormDataParam("deletedDetailIds") List<Long> deletedDetailIds,
            @FormDataParam("deletedPemeriksaIds") List<Long> deletedPemeriksaIds,
            @FormDataParam("deletedPenandatanganIds") List<Long> deletedPenandatanganIds,
            @FormDataParam("deletedPenerimaIds") List<Long> deletedPenerimaIds) {
        String message = "diubah";
        try {
            Map<String, Object> values = getObjectMapper().readValue(json, new TypeReference<HashMap<String, Object>>() {
            });
            String password = values.get("password") != null ? values.get("password").toString() : "";
            service.saveOrEditTransaction(id, dao, password, deletedDetailIds, deletedPemeriksaIds, deletedPenandatanganIds, deletedPenerimaIds);
            if(dao.getStatus().equals("SUBMITTED"))
                message = "dikirim";
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil "+message, dao.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil "+message))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Data '%s' gagal diubah: %s", dao.getPerihal(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Distribusi Document")
    public Response deleteDistribusi(@NotNull @PathParam("id") Long id) {

        try {
            DistribusiDokumen deleted = service.find(id);
            deleted.setIsDeleted(true);
            service.edit(deleted);
            notificationService.sendInfo("Info", "Data berhasil dihapus");
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("task-actions/submit")
    @ApiOperation(
            value = "Task Action Submit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task history")
    public Response taskAction(
            TaskActionDto dto) {
        try {
            try {
                ProcessTask processTask = processTaskService.getTaskId(dto.getTaskId());

                if (processTask == null) {
                    return Response
                            .ok(new MessageDto("error", "Invalid recordRefId"))
                            .build();
                }

                TaskActionResponseDto validTask = generateTaskActions(processTask);

                //INVALID FIELD NAME
                List<String> invalidFieldNames = dto.getForms().stream()
                        .filter(t -> validTask.getForms().stream().noneMatch(f -> f.getName().equals(t.getName())))
                        .map(FormFieldDto::getName)
                        .collect(Collectors.toList());

                if (invalidFieldNames.size() > 0) {
                    throw new WorkflowException(String.format("Error when submit task actions: Invalid field name(s): %s", String.join(", ", invalidFieldNames)), "QT-ERR-CMDA-11");
                }

                //INVALID REQUIRED FIELD
                List<String> invalidRequiredFields = validTask.getForms().stream()
                        .filter(t -> t.getRequiredFn().test(dto) && dto.getForms().stream().noneMatch(f -> f.getName().equals(t.getName()) && StringUtils.isNotEmpty(f.getValue())))
                        .map(t -> String.format("%s: %s", t.getName(), "'Should not be null or empty'"))
                        .collect(Collectors.toList());

                if (invalidRequiredFields.size() > 0) {
                    throw new WorkflowException(String.format("Error when submit task actions: Invalid field value: %s", String.join(", ", invalidFieldNames)), "QT-ERR-CMDA-11");
                }

                Map<String, Object> userResponse = dto.getForms()
                        .stream()
                        .filter(t -> validTask.getForms().stream().anyMatch(v -> v.getPostToCamunda() && v.getName().equals(t.getName())))
                        .collect(HashMap::new, (m, v) -> m.put(v.getName(), (Object) v.getValue()), HashMap::putAll);
                MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
                IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();

                final IWorkflowEntity entity = service.getEntity(processTask);
                TaskEventHandler taskEventHandler = new TaskEventHandler();
                taskEventHandler
                        .onCompleted((processInstance, status) -> service.onWorkflowCompleted(status, processInstance, entity, workflowProvider, null, null))
                        .onTaskExecuted((task, userRsp) -> service.onTaskExecuted(dto, workflowProvider, entity, task, taskEventHandler, userResponse));

                workflowProvider.execute(processTask, entity, userResponse, taskEventHandler);

                //#region SEND MAIL NOTIFICATION
                // SEND MAIL TO KONSEPTOR
                service.infoTaskNotification(processTask, entity, null);

                // SEND MAIL TO NEXT APPROVER
                ProcessInstance processInstance = processTask.getProcessInstance();
                List<ProcessTask> activeTasks = workflowProvider.getActiveTasks(processInstance);

                for (ProcessTask t : activeTasks) {
                    service.newTaskNotification(t, entity, null);
                }
                //#endregion

                notificationService.sendInfo("Info", "Persetujuan Layanan berhasil dikirim");

                return Response
                        .ok()
                        .build();
            } catch (Exception e) {
                throw new WorkflowException("Error when submit task actions", "QT-ERR-CMDA-11", e);
            }
        } catch (Exception ex) {
            String errorMessage = Optional.ofNullable(ex.getCause()).map(Throwable::getMessage).orElse("Unknown Error");
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Tugas gagal dikirim: %s", errorMessage));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private TaskActionResponseDto generateTaskActions(ProcessTask processTask) throws IOException {
        TaskActionResponseDto response = new TaskActionResponseDto();
        ProcessInstance processInstance = Optional.ofNullable(processTask).map(ProcessTask::getProcessInstance).orElse(null);
        BpmnModelInstance modelInstance;

        try (InputStream inputStream = new ByteArrayInputStream(processInstance.getProcessDefinition().getDefinition().getBytes(Charset.forName("UTF-8")))) {
            modelInstance = Bpmn.readModelFromStream(inputStream);
        }

        response.setTaskName(processTask.getTaskName());
        response.setActionType(processTask.getTaskType());
        response.setTaskId(processTask.getTaskId());
        response.setAssigneeId(Optional.ofNullable(processTask.getAssignee()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null));

        List<String> approvalActions = modelInstance
                .getModelElementsByType(modelInstance.getModel().getType(org.camunda.bpm.model.bpmn.instance.UserTask.class))
                .stream()
                .filter(t -> ((org.camunda.bpm.model.bpmn.instance.UserTask) t).getId().equals(processTask.getTaskName()))
                .flatMap(t -> ((UserTask) t).getOutgoing().stream().map(SequenceFlow::getTarget))
                .filter(t -> t instanceof ExclusiveGateway)
                .flatMap(t -> t.getOutgoing().stream())
                .map(FlowElement::getName)
                .sorted()
                .collect(Collectors.toList());
        FormFieldDto approvalForm = new FormFieldDto();
        approvalForm.setName(processTask.getResponseVar());
        approvalForm.setLabel("Approval");
        approvalForm.setType("select");
        approvalForm.setOtherData(getObjectMapper().writeValueAsString(new OptionsDto(approvalActions)));
        approvalForm.setRequired(true);

        response.getForms().add(approvalForm);

        FormFieldDto commentForm = new FormFieldDto();
        commentForm.setName("Comment");
        commentForm.setLabel("Comment");
        commentForm.setType("input");
        commentForm.setInputType("text");
        commentForm.setRequired(null);
        commentForm.setRequiredFn(taskActionDto -> taskActionDto.getForms().stream().filter(t -> t.getName().equals(approvalForm.getName())).findFirst().map(t -> !t.getValue().equals("Setujui")).orElse(false));
        commentForm.setRequiredEl(String.format("forms[.name == '%s'].value != 'Setujui'", approvalForm.getName()));

        response.getForms().add(commentForm);
        response.setActionLabel(String.format("el: forms[.name == '%s'].value", approvalForm.getName()));

        return response;
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    @GET
    @Path("task-history/{id}")
    @ApiOperation(
            value = "Get Task History",
            response = ProcessTaskDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task history")
    public Response getTaskHistory(@NotNull @PathParam("id") String id) {
        try {
            List<ProcessTask> myTasks = processTaskService.getTaskHistoryByRecordRefId("Distribusi Dokumen", id);

            List<ProcessTaskDto> result = myTasks
                    .stream()
                    .sorted(Comparator.comparing(ProcessTask::getId).reversed())
                    .map(ProcessTaskDto::new)
                    .collect(Collectors.toList());

            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            throw new WorkflowException("Error when get distibusi task history", "QT-ERR-CMDA-08", e);
        }
    }

    @GET
    @Path("getFolderBerkas")
    @ApiOperation(
            value = "Get Folder Berkas By Parent",
            response = FolderBerkasDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List Folder Berkas by Parent")
    public Response getFolderBerkas(@NotNull @QueryParam("idFolder") Long idFolder) throws InternalServerErrorException {
        MasterFolderKegiatanDetail parent = kegiatanDetailService.find(idFolder);
        JPAJinqStream<MasterFolderKegiatanDetail> children = kegiatanDetailService.getByParentId2(idFolder);
        JPAJinqStream<Berkas> berkases = berkasService.getByFolder(idFolder);
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getAttachmentDataByRef("t_personal_arsip",idFolder);
        List<Long> idPersonalArsips = personalArsipService.getByIdFolder(idFolder);

        List<GlobalAttachmentDto> resultFile = new LinkedList<>();

        for(Long idPersonalArsip : idPersonalArsips) {
            JPAJinqStream<GlobalAttachment> attachment = globalAttachmentService.getAttachmentDataByRefWithSearch("t_personal_arsip", idPersonalArsip, "");

            resultFile.addAll(attachment
                    .skip(0)
                    .limit(0)
                    .sorted(Comparator.comparing(GlobalAttachment::getId))
                    .map(t -> globalAttachmentService.getLink(t))
                    .collect(Collectors.toList()));

        }
        FolderBerkasDto result = new FolderBerkasDto();
        result.setParent(new MasterFolderKegiatanDetailDto(parent));
        result.setChildren(children.map(MasterFolderKegiatanDetailDto::new).collect(Collectors.toList()));
        result.setBerkases(berkases.map(BerkasDto::new).collect(Collectors.toList()));
        result.setFiles(resultFile);

        return Response
                .ok(result)
                .build();
    }

    @GET
    @Path("getArsips")
    @ApiOperation(
            value = "Get All Arsip Data",
            response = BerkasArsipDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return All Arsip list")
    public Response getAllArsips(
            @NotNull @QueryParam("idBerkas") Long idBerkas) throws InternalServerErrorException {
        Berkas parent = berkasService.find(idBerkas);
        JPAJinqStream<Arsip> arsips = arsipService.getByBerkas(idBerkas);

        BerkasArsipDto result = new BerkasArsipDto();
        result.setParent(new BerkasDto(parent));
        result.setArsips(arsips.map(ArsipDto::new).collect(Collectors.toList()));

        long count = arsips.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getFilesArsipFirst")
    @ApiOperation(
            value = "Get Arsip Dokumen Files",
            response = ArsipFileDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip File List")
    public Response getFilesArsipFirst(
            @DefaultValue("") @QueryParam("idArsip") Long idArsip) throws InternalServerErrorException {
        Arsip parent = arsipService.find(idArsip);
        List<GlobalAttachmentDto> files = globalAttachmentService.getAllDataByRef("t_arsip", idArsip).map(GlobalAttachmentDto::new).collect(Collectors.toList());
        files.stream().map(attach -> {
            if (attach.getDocName().lastIndexOf(".") > 0) {
                attach.setViewLink(fileService.getViewLink(attach.getDocId(), FileUtility.GetFileExtension(attach.getDocName())));
            } else {
                attach.setViewLink(fileService.getViewLink(attach.getDocId()));
            }
            return attach;
        }).collect(Collectors.toList());

        long count = files.size();

        return Response
                .ok(files)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getFilesArsip")
    @ApiOperation(
            value = "Get Arsip Dokumen Files",
            response = ArsipFileDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip File List")
    public Response getArsipFile(
            @DefaultValue("") @QueryParam("idArsip") Long idArsip) throws InternalServerErrorException {
        Arsip parent = arsipService.find(idArsip);
        List<GlobalAttachmentDto> files = globalAttachmentService.getAllDataByRef("t_arsip", idArsip).map(GlobalAttachmentDto::new).collect(Collectors.toList());

        ArsipFileDto result = new ArsipFileDto();
        result.setParent(new ArsipDto(parent));
        result.setFiles(files.stream().map(attach -> {
            if (attach.getDocName().lastIndexOf(".") > 0) {
                attach.setViewLink(fileService.getViewLink(attach.getDocId(), FileUtility.GetFileExtension(attach.getDocName())));
            } else {
                attach.setViewLink(fileService.getViewLink(attach.getDocId()));
            }
            return attach;
        }).collect(Collectors.toList()));

        long count = files.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getFilesPersonal")
    @ApiOperation(
            value = "Get Arsip Dokumen Files",
            response = ArsipFileDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip File List")
    public Response getFilesPersonal(
            @DefaultValue("") @QueryParam("idReferensi") Long idRef) throws InternalServerErrorException {
        JPAJinqStream<GlobalAttachment> files = globalAttachmentService.getAllDataByRef("t_personal_arsip", idRef);
        List<GlobalAttachmentDto> resultFile = new LinkedList<>(files
                .skip(0)
                .limit(0)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(t -> globalAttachmentService.getLink(t))
                .collect(Collectors.toList()));
        FolderKegiatanFileDto result = new FolderKegiatanFileDto();
        result.setParent(new MasterFolderKegiatanDetailDto(kegiatanDetailService.find(idRef)));
        result.setPersonalFiles(resultFile);

        long count = files.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getSingleFilePersonal")
    @ApiOperation(
            value = "Get Arsip Dokumen Files",
            response = GlobalAttachmentDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip File List")
    public Response getSingleFilePersonal(
            @DefaultValue("") @QueryParam("idReferensi") Long idRef) throws InternalServerErrorException {
        JPAJinqStream<GlobalAttachment> files = globalAttachmentService.getAllDataByRef("t_personal_arsip", idRef);
        List<GlobalAttachmentDto> resultFile = new LinkedList<>(files
                .skip(0)
                .limit(0)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(t -> globalAttachmentService.getLink(t))
                .collect(Collectors.toList()));

        long count = files.count();

        return Response
                .ok(resultFile)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("cek-password")
    @ApiOperation(
            value = "Cek Password Distribusi",
            produces = MediaType.APPLICATION_JSON,
            notes = "Cek Password ketika Download Shared File")
    public Response cekPassword(
            @NotNull @QueryParam("idDistribusi") Long idDistribusi, 
            @NotNull @QueryParam("idPenerima") Long idPenerima, 
            @QueryParam("password") String password) throws InternalServerErrorException {
        try {
            DistribusiDokumen model = service.find(idDistribusi);
            if (!Strings.isNullOrEmpty(password)) {
                try {
                    service.cekPassword(model, password, idPenerima);
                } catch (NotAuthorizedException exc) {
                    logger.error(null, exc);
                    throw new InternalServerErrorException("Password salah.");
                }
            } else {
                try {
                    service.updateDownloadDate(idPenerima);
                } catch (InternalServerErrorException exc) {
                    logger.error(null, exc);
                    throw new InternalServerErrorException("Gagal menyimpan tanggal download.");
                }
            }
            
            return Response
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            throw new InternalServerErrorException(ex.getMessage());
        }
    }

    @POST
    @Path("readDistribusi")
    @ApiOperation(
            value = "Read Distribusi",
            produces = MediaType.APPLICATION_JSON,
            notes = "Update Read Date ketika Read Shared File")
    public Response readDistribusi(
            @NotNull @QueryParam("idDistribusi") Long idDistribusi, 
            @NotNull @QueryParam("idPenerima") Long idPenerima) throws InternalServerErrorException {
        try {
            DistribusiDokumen model = service.find(idDistribusi);
            service.readDistribusi(model, idPenerima);
            
            return Response
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            throw new InternalServerErrorException("Gagal mengupdate tanggal.");
        }
    }

    @POST
    @Path("read")
    @ApiOperation(
            value = "Read Distribusi",
            produces = MediaType.APPLICATION_JSON,
            notes = "Read to Dokumen History")
    public Response readDistribusi(
            @NotNull @QueryParam("idDistribusi") Long idDistribusi) throws InternalServerErrorException {
        try {
            DokumenHistoryDto history = new DokumenHistoryDto();
            history.setReferenceTable("t_distribusi_dokumen");
            history.setReferenceId(idDistribusi);
            history.setStatus("READ");

            MasterUser usr = userSession.getUserSession().getUser();
            history.setIdOrganisasi(usr.getOrganizationEntity().getIdOrganization());
            history.setIdUser(usr.getId());
            dokHistoryService.saveHistory(history);

            return Response
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            throw new InternalServerErrorException("Gagal mengupdate tanggal.");
        }
    }
    
    @GET
    @Path("interaction/read/{idDistribusi}")
    public Response riwayatRead(
            @PathParam("idDistribusi") Long idDistribusi) throws InternalServerErrorException {
//        List<PenerimaDistribusi> penerima = service.getPenerimaRead(idDistribusi);
        List<DokumenHistory> penerima = dokHistoryService.getHistory("t_distribusi_dokumen",idDistribusi).collect(Collectors.toList());
        List<RiwayatReadDistribusiDto> result = penerima
                .stream()
                .sorted(Comparator.comparing(q -> q.getActionDate()))
                .map(q -> {
                    RiwayatReadDistribusiDto r = new RiwayatReadDistribusiDto();
                    DateUtil dateUtil = new DateUtil();
                    r.setOrganization(new MasterStrukturOrganisasiDto(q.getOrganization()));
                    r.setDate(dateUtil.getCurrentISODate(q.getActionDate()));
                    r.setAction(q.getStatus());
                    return r;
                }).collect(Collectors.toList());
        long count = penerima.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
    
    @GET
    @Path("interaction/download/{idDistribusi}")
    public Response riwayatDownload(
            @PathParam("idDistribusi") Long idDistribusi) throws InternalServerErrorException {
        List<PenerimaDistribusi> penerima = service.getPenerimaDownload(idDistribusi);
        List<RiwayatDownloadDistribusiDto> result = penerima
                .stream()
                .sorted(Comparator.comparing(q -> q.getDownloadDate()))
                .map(RiwayatDownloadDistribusiDto::new)
                .collect(Collectors.toList());
        long count = penerima.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("comment/{id}")
    public Response getKomentar(
            @PathParam("id") Long idDistribusi,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit
    ) throws InternalServerErrorException {
        JPAJinqStream<DistribusiComment> komentar = distribusiCommentService.getAll(idDistribusi);
        List<DistribusiCommentDto> result = komentar
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(DistribusiComment::getCreatedDate))
                .map(DistribusiCommentDto::new).collect(Collectors.toList());
        long count = komentar.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("comment")
    @ApiOperation(
            value = "Save Comment",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save New Comment")
    public Response saveComment(
            @Valid DistribusiCommentDto dao) throws InternalServerErrorException {
        try {
            distribusiCommentService.saveOrEdit(null,dao);
            notificationService.sendInfo("Info", "Komentar Berhasil Ditambahkan");

            return Response
                    .ok(new MessageDto("info", "Komentar Berhasil Ditambahkan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", "Gagal Menambahkan Komentar");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("comment/{id}")
    @ApiOperation(
            value = "Update Comment",
            produces = MediaType.APPLICATION_JSON,
            notes = "Update New Comment")
    public Response updateComment(
            @NotNull @PathParam("id") Long id,
            @Valid DistribusiCommentDto dao
    ) throws InternalServerErrorException {
        try {
            distribusiCommentService.saveOrEdit(id,dao);
            notificationService.sendInfo("Info", "Komentar Berhasil Diubah");

            return Response
                    .ok(new MessageDto("info", "Komentar Berhasil Diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", "Gagal Mengubah Komentar");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("comment/{id}")
    @ApiOperation(
            value = "Delete Comment",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete New Comment")
    public Response deleteComment(
            @NotNull @PathParam("id") Long id
    ) throws InternalServerErrorException {
        try {
            distribusiCommentService.removeComment(id);
            notificationService.sendInfo("Info", "Komentar Berhasil Dihapus");

            return Response
                    .ok(new MessageDto("info", "Komentar Berhasil Dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", "Gagal Menghapus Komentar");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
