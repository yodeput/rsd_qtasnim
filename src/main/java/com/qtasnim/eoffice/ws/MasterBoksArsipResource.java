package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterBoksArsip;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterBoksArsipService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterBoksArsipDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-lokasi-arsip")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Lokasi Arsip Resource",
        description = "WS Endpoint untuk menghandle operasi Lokasi Arsip"
)
public class MasterBoksArsipResource {
    @Inject
    private MasterBoksArsipService masterBoksArsipService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @Path("boks")
    @ApiOperation(
            value = "Get Data BoksArsip",
            response = MasterBoksArsip[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return BoksArsip list")
    public Response getBoksArsip(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<MasterBoksArsip> all = masterBoksArsipService.getAll();
        List<MasterBoksArsipDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterBoksArsip::getId))
                .map(MasterBoksArsipDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("boks/filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterBoksArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return BoksArsip list")
    public Response getFilteredBoksArsip(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterBoksArsipDto> result = masterBoksArsipService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterBoksArsipDto::new).collect(Collectors.toList());
        long count = masterBoksArsipService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("boks")
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Add BoksArsip")
    public Response saveBoksArsip(@Valid MasterBoksArsipDto dao) {
        try {
            masterBoksArsipService.save(null, dao);
            notificationService.sendInfo("Info", String.format("Data berhasil disimpan", dao.getKode()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save : %s", dao.getKode(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("boks/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit BoksArsip")
    public Response editBoksArsip(@NotNull @PathParam("id") Long id, @Valid MasterBoksArsipDto dao) {
        try {
            masterBoksArsipService.save(id, dao);

            if(!Strings.isNullOrEmpty(dao.getKode())) {
                notificationService.sendInfo("Info", String.format("Data %s berhasil diubah", dao.getKode()));
            }else{
                notificationService.sendInfo("Info", String.format("Data berhasil diubah"));
            }

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getKode(), ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal diubah"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("boks/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete BoksArsip")
    public Response deleteBoksArsip(@NotNull @PathParam("id") Long id) {
        try {
            MasterBoksArsip deleted = masterBoksArsipService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil dihapus", deleted.getKode()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus, Data sudah digunakan"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("boks/all")
    @ApiOperation(
            value = "Get All Data",
            response = MasterBoksArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return BoksArsip list")
    public Response getAllBoksArsip(
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterBoksArsipDto> result = masterBoksArsipService.getResultList(filter, sort, descending).stream()
                .map(MasterBoksArsipDto::new).collect(Collectors.toList());
        long count = masterBoksArsipService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}