package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Pemberitahuan;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterUserService;
import com.qtasnim.eoffice.services.PemberitahuanService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.CompanyCodeDto;
import com.qtasnim.eoffice.ws.dto.PemberitahuanDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("pemberitahuan")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Pemberitahuan Resource",
        description = "WS Endpoint untuk menghandle operasi Pemberitahuan"
)
public class PemberitahuanResource {
    @Inject
    private PemberitahuanService pemberitahuanService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @Inject
    private MasterUserService userService;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = Pemberitahuan[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Pemberitahuan list")
    public Response getPemberitahuan(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<Pemberitahuan> all = pemberitahuanService.getAll();
        List<PemberitahuanDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Pemberitahuan::getId))
                .map(PemberitahuanDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
    
    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Data with Filter",
            response = PemberitahuanDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Pemberitahuan list")
    public Response getPemberitahuan(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<PemberitahuanDto> result = pemberitahuanService.getResultList(filter, sort, descending, skip, limit).stream().
                map(q->{
                    DateUtil dateUtil = new DateUtil();
                    PemberitahuanDto pemberitahuan = new PemberitahuanDto();
                    pemberitahuan.setId(q.getId());
                    pemberitahuan.setPerihal(q.getPerihal());
                    pemberitahuan.setKonten(q.getKonten());
                    pemberitahuan.setStartDate(dateUtil.getCurrentISODate(q.getStart()));
                    pemberitahuan.setEndDate(dateUtil.getCurrentISODate(q.getEnd()));
                    pemberitahuan.setCompanyCode(new CompanyCodeDto(q.getCompanyCode()));
                    pemberitahuan.setCompanyId(q.getCompanyCode().getId());
                    pemberitahuan.setCreatedBy(q.getCreatedBy());
                    pemberitahuan.setCreatedDate(dateUtil.getCurrentISODate(q.getCreatedDate()));
                    if(q.getModifiedDate()!=null){
                        pemberitahuan.setModifiedDate(dateUtil.getCurrentISODate(q.getModifiedDate()));
                    }
                    if(q.getModifiedBy()!=null){
                        pemberitahuan.setModifiedBy(q.getModifiedBy());
                    }
                    MasterUser user = userService.findUserByUsername(q.getCreatedBy());
                    if(user!=null){
                        String published = user.getLoginUserName()+" | "+user.getJabatan()+" | "+user.getNameFront()+" "+user.getNameMiddleLast();
                        pemberitahuan.setPublishedBy(published);
                    }
                    return pemberitahuan;
                }).collect(Collectors.toList());
        long count = pemberitahuanService.count(filter);

        return Response
            .ok(result)
            .header("X-Total-Count", count)
            .header("Access-Control-Expose-Headers", "X-Total-Count")
            .build();
    }

    @POST
    @ApiOperation(
            value = "Post Data",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Pemberitahuan")
    public Response savePemberitahuan(@Valid PemberitahuanDto dao) {
        try{
            pemberitahuanService.save(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getPerihal(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Post Edit Data",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Pemberitahuan")
    public Response editPemberitahuan(@NotNull @PathParam("id") Long id, @Valid PemberitahuanDto dao) {

        try{
            pemberitahuanService.save(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getPerihal(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete Data",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Pemberitahuan")
    public Response deletePemberitahuan(@NotNull @PathParam("id") Long id) {

        try{
            Pemberitahuan deleted = pemberitahuanService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}