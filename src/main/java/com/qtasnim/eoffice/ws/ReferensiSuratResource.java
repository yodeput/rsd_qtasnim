package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterVendor;
import com.qtasnim.eoffice.db.ReferensiSurat;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.services.ReferensiSuratService;
import com.qtasnim.eoffice.services.SuratInteractionService;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import com.qtasnim.eoffice.ws.dto.ReferensiSuratDto;
import com.qtasnim.eoffice.ws.dto.SuratInteractionDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("referensi")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Surat Management",
        description = "WS Endpoint untuk menghandle operasi Surat"
)
public class ReferensiSuratResource {

    @Inject
    private ReferensiSuratService referensiSuratService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @GET
    @ApiOperation(
            value = "Get",
            response = ReferensiSuratDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get All Referensi Surat")
    public Response getAll( @DefaultValue("0") @QueryParam("skip") int skip,
                            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                            @DefaultValue("") @QueryParam("filter") String filter,
                            @QueryParam("sort") String sort,
                            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<ReferensiSuratDto> result = referensiSuratService.getResultList(filter, sort, descending, skip, limit).stream().
                map(ReferensiSuratDto::new).collect(Collectors.toList());
        long count = referensiSuratService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("bySurat/{id}")
    @ApiOperation(
            value = "Get Referensi Surat by Surat",
            response = SuratInteractionDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Interaction History by Id Surat")
    public Response bySurat(@PathParam("id") Long id) throws InternalServerErrorException {
        List<ReferensiSuratDto> result = referensiSuratService.getBySurat(id)
                .map(ReferensiSuratDto::new).collect(Collectors.toList());
        long count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("byOrganization/{id}")
    @ApiOperation(
            value = "Get Referensi by Id Surat",
            response = SuratInteractionDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Referensi by Id Surat")
    public Response getByIdOrg(
            @PathParam("id") Long id,
            @QueryParam("idSurat") Long idSurat,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit
    ) throws InternalServerErrorException {
        JPAJinqStream<ReferensiSurat> all = referensiSuratService.getByOrg(id,idSurat);
        List<ReferensiSuratDto> result = all
                .sorted(Comparator.comparing(ReferensiSurat::getId))
                .map(ReferensiSuratDto::new).collect(Collectors.toList());

        long count  = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Surat Referenece")
    public Response save(@Valid ReferensiSuratDto dao) {
        try {
            referensiSuratService.save(null, dao);
            notificationService.sendInfo("Info", "Referensi berhasil ditambahkan");

            return Response
                    .ok(new MessageDto("info", "Referensi berhasil ditambahkan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Referensi gagal ditambahkan: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Surat Interaction")
    public Response editDisposisi(@NotNull @PathParam("id") Long id, @Valid ReferensiSuratDto dao) {
        try{
            referensiSuratService.save(id,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Vendor")
    public Response deleteVendor(@NotNull @PathParam("id") Long id) {

        try{
            ReferensiSurat model = referensiSuratService.find(id);
            referensiSuratService.remove(model);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("deleteBySuratId/{suratId}")
    @ApiOperation(
            value = "Delete by Surat Id",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete by Surat Id")
    public Response deleteBySuratId(@NotNull @PathParam("suratId") Long suratId) {

        try{
            referensiSuratService.deleteBySuratId(suratId);
            notificationService.sendInfo("Info", "Referensi berhasil dihapus");

            return Response
                    .ok()
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Referensi gagal dihapus: %s", ExceptionUtil.getRealCause(ex).getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}
