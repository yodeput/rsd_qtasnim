package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.SuratInteractionHistoryService;
import com.qtasnim.eoffice.ws.dto.SuratInteractionDto;
import com.qtasnim.eoffice.ws.dto.SuratInteractionHistoryDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("surat")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Surat Interaction History Management",
        description = "WS Endpoint untuk menghandle operasi Histori Interaksi Surat"
)
public class SuratInteractionHistoryResource {

    @Inject
    private SuratInteractionHistoryService suratInteractionHistoryService;

    @GET
    @Path("interaction/history")
    @ApiOperation(
            value = "Get",
            response = SuratInteractionHistoryDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get All Surat Interaction")
    public Response getAll( @DefaultValue("0") @QueryParam("skip") int skip,
                            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                            @DefaultValue("") @QueryParam("filter") String filter,
                            @QueryParam("sort") String sort,
                            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<SuratInteractionHistoryDto> result = suratInteractionHistoryService.getResultList(filter, sort, descending, skip, limit).stream().
                map(SuratInteractionHistoryDto::new).collect(Collectors.toList());
        long count = suratInteractionHistoryService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("interaction/history/bySuratId/{idSurat}")
    @ApiOperation(
            value = "Get Interaction History by Id Surat",
            response = SuratInteractionHistoryDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Interaction History by Id Surat")
    public Response getAllBySuratId(@PathParam("idSurat") Long idSurat, @DefaultValue("0") @QueryParam("skip") int skip,
                            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                            @DefaultValue("") @QueryParam("filter") String filter,
                            @QueryParam("sort") String sort,
                            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<SuratInteractionHistoryDto> result = suratInteractionHistoryService.getByIdSurat(idSurat, filter, sort, descending, skip, limit).stream().
                map(SuratInteractionHistoryDto::new).collect(Collectors.toList());
        long count = suratInteractionHistoryService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("interaction/history")
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Surat Interaction")
    public Response saveDisposisi(@Valid SuratInteractionHistoryDto dao) {
        try{
            suratInteractionHistoryService.save(null,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("interaction/history/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Surat Interaction")
    public Response editDisposisi(@NotNull @PathParam("id") Long id, @Valid SuratInteractionHistoryDto dao) {
        try{
            suratInteractionHistoryService.save(id,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("interaction/history/setStar/{id}")
    @ApiOperation(
            value = "Set as Stared",
            produces = MediaType.APPLICATION_JSON,
            notes = "Set as Stared")
    public Response setStared(@NotNull @PathParam("id") Long id) {
        try{
            suratInteractionHistoryService.setStared(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("interaction/history/setFinish/{id}")
    @ApiOperation(
            value = "Set as Finished",
            produces = MediaType.APPLICATION_JSON,
            notes = "Set as Finished")
    public Response setFinished(@NotNull @PathParam("id") Long id) {
        try{
            suratInteractionHistoryService.setFinished(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("interaction/history/setImportant/{id}")
    @ApiOperation(
            value = "Set as Important",
            produces = MediaType.APPLICATION_JSON,
            notes = "Set as Important")
    public Response setImportant(@NotNull @PathParam("id") Long id) {
        try{
            suratInteractionHistoryService.setImportant(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("interaction/history/setRead/{id}")
    @ApiOperation(
            value = "Set as Read",
            produces = MediaType.APPLICATION_JSON,
            notes = "Set as Read")
    public Response setIsRead(@NotNull @PathParam("id") Long id) {
        try{
            suratInteractionHistoryService.setIsRead(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

}
