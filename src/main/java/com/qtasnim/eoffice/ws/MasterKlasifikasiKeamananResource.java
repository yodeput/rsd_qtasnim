package com.qtasnim.eoffice.ws;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qtasnim.eoffice.db.MasterKlasifikasiKeamanan;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterKlasifikasiKeamananService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterKlasifikasiKeamananDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

@Path("master-klasifikasi-keamanan")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Klasifikasi Keamanan Resource",
        description = "WS Endpoint untuk menghandle CRUD Klasifikasi Keamanan"
)
public class MasterKlasifikasiKeamananResource {
    @Inject private MasterKlasifikasiKeamananService service;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterKlasifikasiKeamananDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List Klasifikasi Keamanan"
    )
    public Response getAll(
        @DefaultValue("0") @QueryParam("skip") int skip,
        @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<MasterKlasifikasiKeamanan> all = service.getAll();
        List<MasterKlasifikasiKeamananDto> result = all           
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterKlasifikasiKeamanan::getIdKlasifikasiKeamanan))
                .map(MasterKlasifikasiKeamananDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered",
            response = MasterKlasifikasiKeamananDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List Klasifikasi Keamanan yang telah di-Filter"
    )
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{
        List<MasterKlasifikasiKeamanan> data = service.getResultList(filter, sort, descending, skip, limit);
        long count = service.count(filter);

        List<MasterKlasifikasiKeamananDto> result = data.stream().map(q->new MasterKlasifikasiKeamananDto(q)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
   }
    
    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Klasifikasi Keamanan Baru"
    )
    public Response saveData(@Valid MasterKlasifikasiKeamananDto dao){
        try{
            service.saveOrEdit(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNamaKlasifikasiKeamanan()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNamaKlasifikasiKeamanan(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Klasifikasi Keamanan Terpilih"
    )
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterKlasifikasiKeamananDto dao) {
        try{
            service.saveOrEdit(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNamaKlasifikasiKeamanan()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNamaKlasifikasiKeamanan(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Klasifikasi Keamanan Terpilih"
    )
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try{
            MasterKlasifikasiKeamanan model = service.find(id);
            service.remove(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getNamaKlasifikasiKeamanan()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}