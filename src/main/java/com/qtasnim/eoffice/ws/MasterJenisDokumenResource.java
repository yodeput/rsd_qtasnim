package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.db.JenisDokumenDocLib;
import com.qtasnim.eoffice.db.MasterJenisDokumen;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.JenisDokumenDocLibService;
import com.qtasnim.eoffice.services.MasterJenisDokumenService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.JenisDokumenDocLibDto;
import com.qtasnim.eoffice.ws.dto.MasterJenisDokumenDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.*;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-jenis-dokumen")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Jenis Dokumen Resource",
        description = "WS Endpoint untuk menghandle operasi Jenis Dokumen"
)
public class MasterJenisDokumenResource {
    @Inject
    private MasterJenisDokumenService masterJenisDokumenService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Inject
    private JenisDokumenDocLibService docLibService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterJenisDokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Jenis Dokumen Korespondensi list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterJenisDokumen> all = masterJenisDokumenService.getAll(true);
        List<MasterJenisDokumenDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterJenisDokumen::getIdJenisDokumen))
                .map(q->{
                    MasterJenisDokumenDto obj = new MasterJenisDokumenDto(q);
                    JenisDokumenDocLibDto docLib = new JenisDokumenDocLibDto(docLibService.getByJenisDokumenId(obj.getIdJenisDokumen()));
                    if(docLib!=null) {
                        obj.setJenisDokumenDocLibDto(docLib);
                    }
                    return obj;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getRegDocType")
    @ApiOperation(
            value = "Get All Registration Doc Type",
            response = MasterJenisDokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Jenis Dokumen Registrasi list")
    public Response getAllReg(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterJenisDokumen> all = masterJenisDokumenService.getAll(false);
        List<MasterJenisDokumenDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterJenisDokumen::getIdJenisDokumen))
                .map(q->{
                    MasterJenisDokumenDto obj = new MasterJenisDokumenDto(q);
                    JenisDokumenDocLibDto docLib = new JenisDokumenDocLibDto(docLibService.getByJenisDokumenId(obj.getIdJenisDokumen()));
                    if(docLib!=null) {
                        obj.setJenisDokumenDocLibDto(docLib);
                    }
                    return obj;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterJenisDokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Jenis Dokumen list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{
        List<MasterJenisDokumen> data = masterJenisDokumenService.getResultList(filter, sort, descending, skip, limit);
        long count = masterJenisDokumenService.count(filter);

        List<MasterJenisDokumenDto> result = data.stream().map(q->{
            MasterJenisDokumenDto obj = new MasterJenisDokumenDto(q);
            JenisDokumenDocLib docLib = docLibService.getByJenisDokumenId(obj.getIdJenisDokumen());
            if(docLib!=null) {
                obj.setJenisDokumenDocLibDto(new JenisDokumenDocLibDto(docLib));
            }
            return obj;
        }).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
   }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save a new Jenis Dokumen")
    public Response saveData(@Valid MasterJenisDokumenDto dao) {
        try {
            MasterJenisDokumenDto jenisDokumenDto = masterJenisDokumenService.saveOrEdit(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNamaJenisDokumen()));

            return Response
                    .ok(jenisDokumenDto)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNamaJenisDokumen(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit an Jenis Dokumen")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterJenisDokumenDto dao) {
        try {
            MasterJenisDokumenDto jenisDokumenDto = masterJenisDokumenService.saveOrEdit(id,dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNamaJenisDokumen()));

            return Response
                    .ok(jenisDokumenDto)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNamaJenisDokumen(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete an Jenis Dokumen")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            MasterJenisDokumen model = masterJenisDokumenService.find(id);
            model.setIsActive(false);
            masterJenisDokumenService.edit(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getNamaJenisDokumen()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("force/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete an Jenis Dokumen")
    public Response forceDeleteData(@NotNull @PathParam("id") Long id) {
        try {
            MasterJenisDokumen model = masterJenisDokumenService.find(id);
            masterJenisDokumenService.forceDelete(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getNamaJenisDokumen()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path(value = "upload/{idJenisDoc}")
    @Secured
    @Produces({ MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Upload Template Document",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response upload(
            @PathParam("idJenisDoc") Long idJenisDoc,
            @FormDataParam(value="file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value="file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body
    ) throws IOException {
        try {
            masterJenisDokumenService.uploadAttachment(idJenisDoc, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil diunggah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Upload File '%s': %s", fileDisposition.getFileName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("getByGroup")
    @ApiOperation(
            value = "Get By Group",
            response = MasterJenisDokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Jenis Dokumen By Group to list")
    public Response getByGroup(@DefaultValue("0") @QueryParam("skip") int skip,
                              @DefaultValue("10") @QueryParam("limit") int limit,
                              @QueryParam("idGroup") Long idGroup) {

        JPAJinqStream<MasterJenisDokumen> all = masterJenisDokumenService.getByGroupId(idGroup);
        List<MasterJenisDokumenDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterJenisDokumen::getIdJenisDokumen))
                .map(MasterJenisDokumenDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}
