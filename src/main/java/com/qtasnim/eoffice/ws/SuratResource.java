package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.*;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Path("surat")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Surat Management",
        description = "WS Endpoint untuk menghandle operasi Surat"
)
public class SuratResource {
    @Inject
    private FormDefinitionService formDefinitionService;

    @Inject
    private FormFieldService formFieldService;

    @Inject
    private MasterJenisDokumenService masterJenisDokumenService;

    @Inject
    private MasterKlasifikasiKeamananService klasifikasiKeamananService;

    @Inject
    private MasterKlasifikasiMasalahService klasifikasiMasalahService;

    @Inject
    private MasterNonStrukturalService nonStrukturalService;

    @Inject
    private SuratService suratService;

    @Inject
    private PemeriksaSuratService pemeriksaSuratService;

    @Inject
    private PenandatanganSuratService penandatanganSuratService;

    @Inject
    private TembusanSuratService tembusanSuratService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private FileService fileService;

    @Inject
    private SuratInteractionService interactionService;

    @Inject
    private SuratDisposisiService disposisiService;

    @Inject
    private MasterDelegasiService delegasiService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @Inject
    private MasterVendorService vendorService;

    @Inject
    private ProcessTaskService processTaskService;

    @GET
    @ApiOperation(
            value = "Get All Surat",
            response = SuratDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<Surat> all = suratService.getAll();
        List<SuratDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Surat::getId))
                .map(SuratDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter")
    public Response filter(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false")
            @QueryParam("descending") boolean descending) {
        List<SuratDto> result = suratService
                .getResultList(filter, sort, descending, skip, limit)
                .stream()
                .map(SuratDto::new)
                .collect(Collectors.toList());

        long count = suratService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/getJenisDokumen")
    @ApiOperation(
            value = "Get All Jenis Dokumen",
            response = MasterJenisDokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Jenis Dokumen list")
    public Response getAllJenisDokumen(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterJenisDokumen> all = masterJenisDokumenService.getAll(true);
        List<MasterJenisDokumenDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterJenisDokumen::getIdJenisDokumen))
                .map(MasterJenisDokumenDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/getFormField")
    @ApiOperation(
            value = "Get Form Field",
            response = FormFieldDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Form Field")
    public Response getFormField(
            @DefaultValue("0") @QueryParam("idJenisDokumen") Long idJenisDokumen) {

        List<FormDefinition> def = formDefinitionService.getByIdJenisDokumen(idJenisDokumen);
        List<FormFieldDto> result = formFieldService.getByDefinitionList(def);
        long count = result.size();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("/penomoranSurat/{id}")
    @ApiOperation(
            value = "Penomoran Surat",
            produces = MediaType.APPLICATION_JSON,
            notes = "Penomoran surat berdasarkan parameter id surat")
    public Response penomoranSurat(@NotNull @PathParam("id") Long id) {
        try{
			Long idSurat = suratService.generateNumber(id);
            notificationService.sendInfo("Info", "Nomor Dokumen berhasil disimpan");

            return Response
                    .ok(new MessageDto("info", "Nomor Dokumen berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Generate Document Number: %s", ExceptionUtil.getRealCause(ex).getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("/saveDraft")
    @ApiOperation(
            response = SuratDto.class,
            value = "Save Surat Draft",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Surat Draft")
    public Response saveDraft(@Valid SuratDto dto, @QueryParam("docId") @NotNull String docId) {
        try {
            dto.setStatus("DRAFT");
            SuratDto surat = suratService.saveOrEditDraft(null, dto, docId);
            notificationService.sendInfo("Info", "Document berhasil disimpan to draft");

            return Response
                    .ok(surat)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save Draft Document: %s", ExceptionUtil.getRealCause(ex).getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path(value = "upload/{idSurat}")
    @Secured
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Upload",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response upload(
            @PathParam("idSurat") Long idSurat,
            @FormDataParam(value = "file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value = "file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body
    ) throws IOException {
        try {
            suratService.uploadAttachment(idSurat, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil diunggah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("File '%s' gagal diunggah: %s", fileDisposition.getFileName(), ExceptionUtil.getRealCause(ex).getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("editDraft/{idSurat}")
    @ApiOperation(
            value = "Edit Surat Draft",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Surat Draft")
    public Response EditDraft(
            @PathParam("idSurat") Long idSurat,
            @Valid SuratDto dto) {
        try {
            dto.setStatus("DRAFT");
            SuratDto surat = suratService.saveOrEditDraft(idSurat, dto, null);
            notificationService.sendInfo("Info", "Dokumen berhasil diubah");

            return Response
                    .ok(surat)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit Draft Document: %s", ExceptionUtil.getRealCause(ex).getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("deleteDraft/{idSurat}")
    @ApiOperation(
            value = "Delete",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Surat")
    public Response deleteDraft(@PathParam("idSurat") long idSurat) {
        try {
            Surat obj = suratService.find(idSurat);
            obj.setIsDeleted(true);
            suratService.edit(obj);
            notificationService.sendInfo("Info", "Dokumen berhasil deleted");

            return Response
                    .ok(new MessageDto("info", "Dokumen berhasil deleted"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ExceptionUtil.getRealCause(ex).getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("deleteAttachment/{idSurat}")
    public Response deleteAttachment(
            @PathParam("idSurat") long idSurat,
            @QueryParam("docId") String docId
    ) {
        try {
            suratService.deleteAttachment(idSurat, docId);
            notificationService.sendInfo("Info", "Lampiran berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Lampiran berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Lampiran gagal dihapus: %s", ExceptionUtil.getRealCause(ex).getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("/kirimDraftSurat")
    @ApiOperation(
            response = SuratDto.class,
            value = "Kirim Surat",
            produces = MediaType.APPLICATION_JSON,
            notes = "Kirim Surat")
    public Response kirimDraftSurat(
            @Valid SuratDto dto,
            @QueryParam("docId") @NotNull String docId
    ) {
        try {
            dto.setStatus("SUBMITTED");
            SuratDto surat = suratService.kirimSurat(dto, docId);
            notificationService.sendInfo("Info", "Dokumen berhasil dikirim");

            return Response
                    .ok(surat)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Send Document: %s", ExceptionUtil.getRealCause(ex).getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("ListPemeriksa")
    @ApiOperation(
            value = "Get All Pemeriksa",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Pemeriksa List")
    public Response getPemeriksa(@DefaultValue("0") @QueryParam("skip") int skip,
                                 @DefaultValue("10") @QueryParam("limit") int limit,
                                 @DefaultValue("") @QueryParam("idSurat") Long idSurat) {
        List<PemeriksaSuratDto> result = pemeriksaSuratService.getAllBy(idSurat).stream().map(t -> new PemeriksaSuratDto(t,true)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("ListPenandatangan")
    @ApiOperation(
            value = "Get All Penandatangan",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Penandatangan List")
    public Response getPenandatangan(@DefaultValue("0") @QueryParam("skip") int skip,
                                     @DefaultValue("10") @QueryParam("limit") int limit,
                                     @DefaultValue("") @QueryParam("idSurat") Long idSurat) {
        List<PenandatanganSuratDto> result = penandatanganSuratService.getAllBy(idSurat).stream().map(PenandatanganSuratDto::new).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("ListTembusan")
    @ApiOperation(
            value = "Get All Tembusan",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Tembusan List")
    public Response getTembusan(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("idSurat") Long idSurat) {
        List<TembusanSuratDto> result = tembusanSuratService.getAllBy(idSurat).stream().map(TembusanSuratDto::new).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("ListDraft")
    @ApiOperation(
            value = "Get All Draft Dokumen",
            response = DokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Draft List")
    public Response getDraft(@DefaultValue("0") @QueryParam("skip") int skip,
                             @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<Surat> all = suratService.getDraft();
        List<DokumenDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Surat::getCreatedDate).reversed())
                .map(q -> {
                    DokumenDto obj = new DokumenDto();
                    DateUtil dateUtil = new DateUtil();
                    obj.setId(q.getId());
                    obj.setTanggal(dateUtil.getCurrentISODate(q.getCreatedDate()));
                    FormValue fv = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                            .findFirst().orElse(null);
                    if (fv != null) {
                        obj.setPerihal(fv.getValue());
                    }
                    MasterJenisDokumen jenisDokumen = q.getFormDefinition().getJenisDokumen();
                    obj.setJenisDokumen(jenisDokumen.getKodeJenisDokumen()+" "+jenisDokumen.getNamaJenisDokumen());
                    obj.setKorespondensi(jenisDokumen.getIsKorespondensi());
                    if(obj.isKorespondensi()){
                        List<PengirimDto> pObj = q.getPenandatanganSurat().stream().map(b -> {
                            PengirimDto pengirim = new PengirimDto();
                            pengirim.setJabatan(b.getOrganization().getOrganizationName());
                            pengirim.setNama(b.getOrganization().getUser().getNameFront()+" "+b.getOrganization().getUser().getNameMiddleLast());
                            pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                            return pengirim;
                        }).collect(Collectors.toList());
                        obj.setPengirim(pObj);
                    }else{
                        List<PengirimDto> pObj = new ArrayList<>();
                        FormValue fv2 = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("dari"))
                                .findFirst().orElse(null);
                        if (fv2 != null) {
                           MasterVendor vendor = vendorService.find(Long.parseLong(fv2.getValue()));
                           PengirimDto po = new PengirimDto();
                           po.setJabatan(vendor.getNama());
                           pObj.add(po);
                        }
                        obj.setPengirim(pObj);
                    }
                    return obj;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("ListDraft/filter")
    @ApiOperation(
            value = "Get All Draft Dokumen",
            response = DokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Draft List")
    public Response getDraftFilter(@DefaultValue("0") @QueryParam("skip") int skip,
                                   @DefaultValue("10") @QueryParam("limit") int limit,
                                   @DefaultValue("") @QueryParam("filter") String filter,
                                   @QueryParam("sort") String sort,
                                   @QueryParam("descending") Boolean descending) {

        HashMap<String,Object> temp = suratService.getDraftFilter(filter,sort,descending,skip,limit);
        List<DokumenDto> result = (List<DokumenDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();

    }

    @GET
    @Path("ListKoreksi")
    @ApiOperation(
            value = "Get All Dokumen Koreksi",
            response = DokumenDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Dokumen Koreksi List")
    public Response getKoreksi(@DefaultValue("0") @QueryParam("skip") int skip,
                               @DefaultValue("10") @QueryParam("limit") int limit) {

        List<DokumenDto> result = suratService.getKoreksi(skip, limit);
        long count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("ListProses")
    @ApiOperation(
            value = "Get Dokumen Dalam Proses Approval",
            response = DokumenDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Dokumen Dalam Proses Approval")
    public Response getExecuted(@DefaultValue("0") @QueryParam("skip") int skip,
                               @DefaultValue("10") @QueryParam("limit") int limit) {

        List<DokumenDto> result = suratService.getExecuted(skip, limit);
        long count = result.size();

//        HashMap<String,Object> temp = suratService.getExecuted(skip,limit);
//        List<DokumenDto> result = (List<DokumenDto>) temp.get("data");
//        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("ListTandatangan")
    @ApiOperation(
            value = "Get All Dokumen Tanda Tangan",
            response = DokumenDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Dokumen Tanda Tangan List")
    public Response getTandaTangan(@DefaultValue("0") @QueryParam("skip") int skip,
                                   @DefaultValue("10") @QueryParam("limit") int limit) {

        List<DokumenDto> result = suratService.getKoreksi(skip, limit);
        long count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("batal/{id}")
    @ApiOperation(
            value = "Membatalkan Surat",
            produces = MediaType.APPLICATION_JSON,
            notes = "Membatalkan Surat")
    public Response BatalSurat(@PathParam("id") Long id) {
        try {
            Surat surat = suratService.find(id);
            if (surat.getStatus().equals("DRAFT")) {
                surat.setStatus("BATAL");
                suratService.edit(surat);
            }
            notificationService.sendInfo("Info", "Dokumen berhasil canceled");

            return Response
                    .ok(new MessageDto("info", "Dokumen berhasil canceled"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Cancel: %s", ExceptionUtil.getRealCause(ex).getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GET
    @Path("penerima")
    @ApiOperation(
            value = "Get All Dokumen Penerima",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Dokumen  Penerima")
    public Response getByPenerima(@DefaultValue("0") @QueryParam("skip") int skip,
                                  @DefaultValue("10") @QueryParam("limit") int limit) {

        HashMap<String,Object> temp = suratService.getByPenerima(skip,limit);
        List<SuratDto> result = (List<SuratDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }


    @GET
    @Path("penerima/filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = SuratDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Penerima list Filtered")
    public Response getByPenerimaFilter(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

        HashMap<String,Object> temp = suratService.getByPenerimaFilter(filter,sort,descending,skip,limit);
        List<SuratDto> result = (List<SuratDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("detail/{idSurat}")
    @ApiOperation(
            value = "Get Surat By Id",
            response = SuratDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat")
    public Response getByIdSurat(@PathParam("idSurat") Long idSurat) {
        SuratDto result = new SuratDto();
        Surat surat = suratService.getByIdSurat(idSurat);
        if (surat != null) {
            result = new SuratDto(surat);

            List<FormFieldDto> formFieldDtos = result.getFormDefinition().getFields();

            for (FormFieldDto formFieldDto : formFieldDtos) {
                String selectedFields = formFieldDto.getSelectedSourceField();

                formFieldDto.setValueDataMaster(formFieldService.fillValueFromDataMaster(
                        new DataMasterValueRetrieveParam(
                                formFieldDto.getTableSourceName(),
                                selectedFields == null ? null : Arrays.asList(selectedFields.split(",")),
                                formFieldDto.getSelectedSourceValue())
                ));
            }

            result.getMetadata().forEach((a)->{
                if(a.getName().contains("Klasifikasi Keamanan")){
                    if(!Strings.isNullOrEmpty(a.getValue())) {
                        Long idKeamanan = Long.parseLong(a.getValue());
                        MasterKlasifikasiKeamanan keamanan = klasifikasiKeamananService.find(idKeamanan);
                        a.setLabel(keamanan.getNamaKlasifikasiKeamanan());
                    }
                }

                if(a.getName().contains("Klasifikasi Dokumen")){
                    if(!Strings.isNullOrEmpty(a.getValue())) {
                        Long idMasalah = Long.parseLong(a.getValue());
                        MasterKlasifikasiMasalah masalah = klasifikasiMasalahService.find(idMasalah);
                        a.setLabel(masalah.getKodeKlasifikasiMasalah() + " " + masalah.getNamaKlasifikasiMasalah());
                    }
                }

                if(a.getName().contains("Dari")){
                    if(!Strings.isNullOrEmpty(a.getValue())) {
                        Long idVendor = Long.parseLong(a.getValue());
                        MasterVendor vendor = vendorService.find(idVendor);
                        a.setLabel(vendor.getNama());
                    }
                }
            });
        }
        return Response.ok(result).build();
    }

    @GET
    @Path("getDataDoc")
    @ApiOperation(
            value = "Get Surat Dokumen Files",
            response = GlobalAttachmentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat Dokumen File List")
    public Response getDocAttachment(
            @DefaultValue("") @QueryParam("idSurat") Long idSurat,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getDocDataByRef("t_surat", idSurat);
        List<GlobalAttachmentDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(t -> globalAttachmentService.getLink(t))
                .collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getDocDraft")
    @ApiOperation(
            value = "Get Konsep Surat Draft Files",
            response = GlobalAttachmentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Konsep Surat Draft File List")
    public Response getDocDraft(
            @DefaultValue("") @QueryParam("idSurat") Long idSurat,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getDraftDocDataByRef("t_surat", idSurat);
        List<GlobalAttachmentDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(t -> globalAttachmentService.getLink(t))
                .collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getDocIdDraft")
    @ApiOperation(
            value = "Get Konsep Surat Draft Files",
            response = GlobalAttachmentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Konsep Surat Draft File List")
    public Response getDocIdDraft(
            @DefaultValue("") @QueryParam("idSurat") Long idSurat) {
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getDraftDocDataByRef("t_surat", idSurat);

        String docId = all.toList().get(0).getDocId();

        Map<String, String> result = new HashMap<>();

        result.put("docId", docId);

        return Response
                .ok(result)
                .build();
    }

//    @GET
//    @Path("preview/{idDoc}")
//    @ApiOperation(
//            value = "Get Attachment Dokumen Preview",
//            response = GlobalAttachmentDto.class,
//            produces = MediaType.APPLICATION_JSON,
//            notes = "Return Attachment with Preview Link")
//    public Response getDocAttachment(@PathParam("idDoc") Long idDoc){
//        GlobalAttachmentDto result = globalAttachmentService.getLink(idDoc);
//        return Response.ok(result).build();
//    }

    @GET
    @Path("preview-drafts")
    @ApiOperation(
            value = "Get Link Draft Dokumen Preview",
            response = ListStringDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Draft Document Preview Link")
    public Response getDraftPreviewLink(@QueryParam("idSurat") @NotNull Long idSurat) throws CMISProviderException {
        ListStringDto result = new ListStringDto(suratService.getDraftLinkPreview(idSurat));

        return Response.ok(result).build();
    }

    @GET
    @Path("preview-documents")
    @ApiOperation(
            value = "Get Link Dokumen Preview",
            response = ListStringDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Document Preview Link")
    public Response getDocumentPreviewLink(@QueryParam("idSurat") @NotNull Long idSurat) throws CMISProviderException {
        ListStringDto result = new ListStringDto(suratService.getDocumentLinkPreview(idSurat));

        return Response.ok(result).build();
    }

    @GET
    @Path("preview-attachments")
    @ApiOperation(
            value = "Get Link Dokumen Attachments Preview",
            response = List.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Document Attachments Preview Link")
    public Response getAttachmentPreviewLink(@QueryParam("idSurat") @NotNull Long idSurat) throws CMISProviderException {
        List<AttachmentPreviewDto> result = suratService.getAttachmentLinkPreview(idSurat);

        return Response.ok(result).build();
    }

    @GET
    @Path("getAttach")
    @ApiOperation(
            value = "Get Surat Attachment Files",
            response = LampiranDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat Attachment File List")
    public Response getAttachmentFile(
            @DefaultValue("") @QueryParam("idSurat") Long idSurat,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getAttachmentDataByRef("t_surat", idSurat);
        List<LampiranDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(q -> {
                    LampiranDto obj = new LampiranDto();
                    DateUtil dateUtil = new DateUtil();
                    obj.setId(q.getDocId());
                    obj.setNamaFile(q.getDocName());
                    obj.setUkuran(q.getSize());
                    obj.setTipe(q.getMimeType());
                    obj.setWaktu(dateUtil.getCurrentISODate(q.getCreatedDate()));
                    obj.setPengunggah(q.getCreatedBy());
                    obj.setDownloadLink(fileService.getViewLink(q.getDocId(), false));

                    return obj;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("downloadFile")
    @ApiOperation(
            value = "Download File",
            notes = "Return File from Alfresco")
    public Response getDownloadFile(
            @DefaultValue("") @QueryParam("idDoc") String idDoc) {
        GlobalAttachmentDto obj = globalAttachmentService.findByDocId(idDoc);
        StreamingOutput fileStream = new StreamingOutput() {
            @Override
            public void write(java.io.OutputStream output) throws IOException, WebApplicationException {
                try {
                    byte[] download = fileService.download(idDoc);
                    output.write(download);
                    output.flush();
                } catch (Exception e) {
                    throw new WebApplicationException("File Not Found !!");
                }
            }
        };
        return Response
                .ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
                .header("content-disposition", "attachment; filename = " + obj.getDocName() + "." + obj.getMimeType())
                .build();
    }

    @GET
    @Path("getReferensiFiles")
    @ApiOperation(
            value = "Get Surat Referensi File List",
            response = ReferensiSuratFileDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat Referensi File List")
    public Response getReferenceFiles(
            @DefaultValue("") @QueryParam("idSurat") Long idSurat,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        // Long idParent = suratService.find(idSurat).getParent().getId();
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getDocDataByRef("t_surat", idSurat);
        List<ReferensiSuratFileDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(q -> {
                    ReferensiSuratFileDto obj = new ReferensiSuratFileDto();
                    DateUtil dateUtil = new DateUtil();
                    Surat surat = suratService.find(q.getReferenceId());
                    obj.setDocId(q.getDocId());
                    obj.setNamaFile(q.getDocName());
                    obj.setUkuran(q.getSize());
                    obj.setPengunggah(q.getCreatedBy());
                    obj.setNoSurat(surat.getNoDokumen());
                    obj.setTanggal(dateUtil.getCurrentISODate(q.getCreatedDate()));
                    FormValue fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                            .findFirst().orElse(null);
                    if (fv != null) {
                        obj.setPerihal(fv.getValue());
                    }
                    obj.setJenisDokumen(surat.getFormDefinition().getJenisDokumen().getNamaJenisDokumen());
                    return obj;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getMetadataSurat")
    @ApiOperation(
            value = "Get Surat Metadata List",
            response = MetadataSuratDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat Metadata List")
    public Response getMetadataSurat(
            @DefaultValue("") @QueryParam("idSurat") Long idSurat,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<Surat> all = suratService.getMetadataSurat(idSurat);
        List<MetadataSuratDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Surat::getId))
                .map(q -> {
                    MetadataSuratDto obj = new MetadataSuratDto();
                    DateUtil dateUtil = new DateUtil();
                    obj.setSurat(new SuratDto(q));
                    obj.setNamaMetadata(q.getFormValue().stream().map(a -> a.getFormField().getMetadata().getNama())
                            .collect(Collectors.toList()));
                    obj.setPengirim(q.getPenandatanganSurat().stream().map(a -> a.getOrganization().getOrganizationName())
                            .collect(Collectors.toList()));
                    obj.setPemeriksa(q.getPemeriksaSurat().stream().map(a -> a.getOrganization().getOrganizationName())
                            .collect(Collectors.toList()));
                    obj.setTembusan(q.getTembusanSurat().stream().map(a -> a.getOrganization().getOrganizationName())
                            .collect(Collectors.toList()));
                    obj.setPenerima(q.getPenerimaSurat().stream().map(a -> a.getOrganization().getOrganizationName())
                            .collect(Collectors.toList()));
                    obj.getSurat().getMetadata().forEach((a)->{
                       if(a.getName().contains("Klasifikasi Keamanan")){
                           if(!Strings.isNullOrEmpty(a.getValue())) {
                               Long idKeamanan = Long.parseLong(a.getValue());
                               MasterKlasifikasiKeamanan keamanan = klasifikasiKeamananService.find(idKeamanan);
                               a.setLabel(keamanan.getNamaKlasifikasiKeamanan());
                           }
                       }

                       if(a.getName().contains("Klasifikasi Dokumen")){
                           if(!Strings.isNullOrEmpty(a.getValue())) {
                               Long idMasalah = Long.parseLong(a.getValue());
                               MasterKlasifikasiMasalah masalah = klasifikasiMasalahService.find(idMasalah);
                               a.setLabel(masalah.getKodeKlasifikasiMasalah() + " " + masalah.getNamaKlasifikasiMasalah());
                           }
                       }
                    });
                   return obj;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getDokumenKeluar")
    @ApiOperation(
            value = "Get Dokumen Keluar List",
            response = DokumenKeluarDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Dokumen Keluar List")
    public Response getDokumenKeluar(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        HashMap<String,Object> temp = suratService.getDokumenKeluar(skip,limit);
        List<DokumenKeluarDto> result = (List<DokumenKeluarDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("listBatal")
    @ApiOperation(
            value = "Get All Dokumen Batal",
            response = DokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get List Dokumen Batal")
    public Response getBatalDoc(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<Surat> all = suratService.getDokumenBatal();
        List<DokumenDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Surat::getModifiedDate).reversed())
                .map(q -> {
                    DokumenDto obj = new DokumenDto();
                    DateUtil dateUtil = new DateUtil();
                    obj.setId(q.getId());
                    if (q.getSubmittedDate() != null) {
                        obj.setTanggal(dateUtil.getCurrentISODate(q.getCreatedDate()));
                    }
                    FormValue fv = q.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                            .findFirst().orElse(null);
                    if (fv != null) {
                        obj.setPerihal(fv.getValue());
                    }
                    List<PengirimDto> pObj = q.getPenandatanganSurat().stream().map(b -> {
                        PengirimDto pengirim = new PengirimDto();
                        pengirim.setJabatan(b.getOrganization().getOrganizationName());
                        pengirim.setNama(b.getOrganization().getUser().getNameFront()+" "+b.getOrganization().getUser().getNameMiddleLast());
                        pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                        return pengirim;
                    }).collect(Collectors.toList());
                    obj.setPengirim(pObj);
                    MasterJenisDokumen jenisDokumen = q.getFormDefinition().getJenisDokumen();
                    obj.setJenisDokumen(jenisDokumen.getKodeJenisDokumen()+" "+jenisDokumen.getNamaJenisDokumen());
                    obj.setKorespondensi(jenisDokumen.getIsKorespondensi());
                    return obj;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("ListFavorit")
    @ApiOperation(
            value = "Get Favorit by User Login",
            response = DokumenMasukDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat list")
    public Response getFavoritList(
            @DefaultValue("") @QueryParam("tipe") String tipe,
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") Boolean descending) throws InternalServerErrorException {

        MasterStrukturOrganisasi current = userSession.getUserSession().getUser().getOrganizationEntity();
        JPAJinqStream<SuratInteraction> list = interactionService.getFavoritByUser(current);
        List<ProcessTask> processTasks = processTaskService.getExecutedTaskByOrgId(current.getIdOrganization()).toList();
        List<DokumenMasukDto> result = list
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(SuratInteraction::getCreatedDate)).map(
                        q->{
                            DokumenMasukDto obj = new DokumenMasukDto();
                            DateUtil dateUtil = new DateUtil();

                            ProcessTask task = new ProcessTask();
                            if(!processTasks.isEmpty()) {
                                task = processTasks.get(processTasks.size() - 1);
                            }else{
                                task = null;
                            }

                            obj.setId(q.getSurat().getId());
                            MasterJenisDokumen jenisDokumen = q.getSurat().getFormDefinition().getJenisDokumen();
                            obj.setJenisDokumen(jenisDokumen.getKodeJenisDokumen()+" "+jenisDokumen.getNamaJenisDokumen());
                            obj.setKorespondensi(jenisDokumen.getIsKorespondensi());
                            if(obj.isKorespondensi()){
                                List<PengirimDto> pObj = q.getSurat().getPenandatanganSurat().stream().map(b -> {
                                    PengirimDto pengirim = new PengirimDto();
                                    pengirim.setJabatan(b.getOrganization().getOrganizationName());
                                    pengirim.setNama(b.getOrganization().getUser().getNameFront()+" "+b.getOrganization().getUser().getNameMiddleLast());
                                    pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                                    return pengirim;
                                }).collect(Collectors.toList());
                                obj.setPengirim(pObj);
                            }else{
                                List<PengirimDto> pObj = new ArrayList<>();
                                FormValue form = q.getSurat().getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("dari"))
                                        .findFirst().orElse(null);
                                if (form != null) {
                                    MasterVendor vendor = vendorService.find(Long.parseLong(form.getValue()));
                                    PengirimDto po = new PengirimDto();
                                    po.setJabatan(vendor.getNama());
                                    pObj.add(po);
                                }
                                obj.setPengirim(pObj);
                            }
                            obj.setNoSurat(q.getSurat().getNoDokumen());
                            obj.setTanggalDikirim(dateUtil.getCurrentISODate(q.getSurat().getSubmittedDate()));
                            //TODO
                            //seharusnya ambil dari tabel process_task (asisgned_date)
                            if(task!=null) {
                                obj.setTanggalDiterima(dateUtil.getCurrentISODate(task.getAssignedDate()));
                            }
                            SuratInteraction interaction = Optional.ofNullable(interactionService.getByUser(q.getId())).orElse(null);
                            if (interaction != null) {
                                obj.setIsRead(interaction.getIsRead());
                                obj.setIsImportant(interaction.getIsImportant());
                                obj.setIsStarred(interaction.getIsStarred());
                                obj.setIsFinished(interaction.getIsFinished());
                            }
                            List<SuratDisposisiDto> disposisi = disposisiService.findByIdSurat(q.getId()).stream().map(SuratDisposisiDto::new).collect(Collectors.toList());
                            if (!disposisi.isEmpty()) {
                                obj.setDisposisiStatus(true);
                                obj.setSuratDisposisi(disposisi);
                            }

                            FormValue fv1 = q.getSurat().getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                                    .findFirst().orElse(null);
                            if (fv1 != null) {
                                obj.setPerihal(fv1.getValue());
                            }

                            FormValue fv2 = q.getSurat().getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("sifat naskah"))
                                    .findFirst().orElse(null);
                            if (fv2 != null) {
                                obj.setSifatNaskah(fv2.getValue());
                            }

                            FormValue fv3 = q.getSurat().getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("klasifikasi masalah"))
                                    .findFirst().orElse(null);
                            if (fv3 != null) {
                                obj.setKlasifikasiDokumen(fv3.getValue());
                            }

                            return obj;

                }).collect(Collectors.toList());

        int count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("inbox")
    @ApiOperation(
            value = "Get Inbox by User Login",
            response = DokumenMasukDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat list")
    public Response getInboxList(
            @DefaultValue("") @QueryParam("tipe") String tipe,
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") Boolean descending) throws InternalServerErrorException {

        HashMap<String,Object> temp = suratService.getInboxList(tipe, sort, descending, skip, limit);
        List<DokumenMasukDto> result = (List<DokumenMasukDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("disposisi/{idSurat}")
    @ApiOperation(
            value = "Get All Surat",
            response = DisposisiDto[].class,
            notes = "Return disposisi list")
    public Response getDisposisi(
            @PathParam("idSurat") Long idSurat,
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @QueryParam("limit") int limit) {
        JPAJinqStream<SuratDisposisi> findAll = disposisiService.getAll()
                .where(t -> t.getExecutedDate() != null && t.getSurat().getId().equals(idSurat));
        List<DisposisiDto> result = findAll
                .leftOuterJoinFetchList(SuratDisposisi::getDisposisi)
                .sortedBy(SuratDisposisi::getExecutedDate)
                .skip(skip)
                .limit(limit)
                .map(DisposisiDto::new)
                .collect(Collectors.toList());
        long count = findAll.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    /*@GET
    @Path("status")
    @ApiOperation(
            value = "Get List Status Surat",
            response = DokumenMasukDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat list")
    public Response getListStatus(
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

        JPAJinqStream<Surat> all = suratService.getSuratByContainOrg();
    *//*List<SuratDto> result2 = all
            .skip(skip)
            .limit(limit)
            .sorted(Comparator.comparing(Surat::getId))
            .map(SuratDto::new).collect(Collectors.toList());*//*

        List<DokumenStatusDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Surat::getId))
                .map(q-> {
                    DokumenStatusDto obj = new DokumenStatusDto();
                    Surat surat = suratService.getByIdSurat(q.getId());

                    obj.setId(surat.getId());
                    obj.setNoSurat(surat.getNoDokumen());
                    List<SuratInteraction> riwayatPemeriksa = new ArrayList<>();
                    List<SuratInteraction> riwayatPenendatangan = new ArrayList<>();
                    List<SuratInteraction> riwayatPenerima = new ArrayList<>();
                    for(PemeriksaSurat org:q.getPemeriksaSurat()){
                        riwayatPemeriksa.add(interactionService.getBySuratIdOrCreateByOrg(surat, org.getOrganization().getIdOrganization()));
                    }
                    obj.setStatusPemeriksa(riwayatPemeriksa.stream().map(DokumenRiwayatItemDto::new).collect(Collectors.toList()));
                    for(PenandatanganSurat org:q.getPenandatanganSurat()){
                        riwayatPenendatangan.add(interactionService.getBySuratIdOrCreate(surat, org.getOrganization().getIdOrganization()));
                    }
                    obj.setStatusPenendatangan(riwayatPenendatangan.stream().map(DokumenRiwayatItemDto::new).collect(Collectors.toList()));

                    for(PenerimaSurat org:q.getPenerimaSurat()){
                        riwayatPenerima.add(interactionService.getBySuratIdOrCreate(surat, org.getOrganization().getIdOrganization()));
                    }
                    obj.setStatusPenerima(riwayatPenerima.stream().map(DokumenRiwayatItemDto::new).collect(Collectors.toList()));

                    return obj;
                }).collect(Collectors.toList());

        Long count = all.count();


        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }*/

    @GET
    @Path("riwayat/pembuatan/{id}")
    @ApiOperation(
            value = "Get Riwayat Surat by id surat",
            response = DokumenRiwayatDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Riwayat pembuatan Surat list")
    public Response getRiwayatPembuatan(
            @NotNull @PathParam("id") Long id
    ) throws InternalServerErrorException {

        Surat surat = suratService.getByIdSurat(id);
        DokumenRiwayatDto dto = new DokumenRiwayatDto();
        List<DokumenRiwayatItemDto> konseptor = new ArrayList<>();
        List<DokumenRiwayatItemDto> pemeriksa = new ArrayList<>();
        List<DokumenRiwayatItemDto> penandatangan = new ArrayList<>();

        DokumenRiwayatItemDto aa = new DokumenRiwayatItemDto(interactionService.getBySuratIdOrCreateByOrg(surat, surat.getKonseptor().getIdOrganization()));
        MasterDelegasi delegasi = delegasiService.getByIdOrg(surat.getKonseptor().getIdOrganization());
        if (delegasi != null) {
            aa.setJabatan(delegasi.getTipe() + " " + delegasi.getFrom().getOrganizationName());
        }
        konseptor.add(aa);
        dto.setKonseptor(konseptor);

        for (PemeriksaSurat pemeriksaSurat : surat.getPemeriksaSurat()) {
            DokumenRiwayatItemDto bb = new DokumenRiwayatItemDto(interactionService.getBySuratIdOrCreateByOrg(surat, pemeriksaSurat.getOrganization().getIdOrganization()));
            MasterDelegasi delegasi2 = delegasiService.getByIdOrg(pemeriksaSurat.getOrganization().getIdOrganization());
            if (delegasi2 != null) {
                bb.setJabatan(delegasi2.getTipe() + " " + delegasi2.getFrom().getOrganizationName());
            }
            pemeriksa.add(bb);
        }
        dto.setPemeriksa(pemeriksa);


        for (PenandatanganSurat penandatanganSurat : surat.getPenandatanganSurat()) {
            DokumenRiwayatItemDto cc = new DokumenRiwayatItemDto(interactionService.getBySuratIdOrCreateByOrg(surat, penandatanganSurat.getOrganization().getIdOrganization()));
            MasterDelegasi delegasi3 = delegasiService.getByIdOrg(penandatanganSurat.getOrganization().getIdOrganization());
            if (delegasi3 != null) {
                cc.setJabatan(delegasi3.getTipe() + " " + delegasi3.getFrom().getOrganizationName());
            }
            penandatangan.add(cc);
        }
        dto.setPenandatangan(penandatangan);

        List<DokumenRiwayatItemDto> penerima = new ArrayList<>();
        for (PenerimaSurat penerimaSurat : surat.getPenerimaSurat()) {
            DokumenRiwayatItemDto dd = new DokumenRiwayatItemDto(interactionService.getBySuratIdOrCreateByOrg(surat, penerimaSurat.getOrganization().getIdOrganization()));
            MasterDelegasi delegasi4 = delegasiService.getByIdOrg(penerimaSurat.getOrganization().getIdOrganization());
            if (delegasi4 != null) {
                aa.setJabatan(delegasi4.getTipe() + " " + delegasi4.getFrom().getOrganizationName());
            }
            penerima.add(aa);
        }
        dto.setPenerima(penerima);

        return Response
                .ok(dto)
                .header("X-Total-Count", 1)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("riwayat/penerimaan/{id}")
    @ApiOperation(
            value = "Get Riwayat Surat by id surat",
            response = DokumenRiwayatDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Riwayat penerimaan Surat list")
    public Response getRiwayatPenerimaan(
            @NotNull @PathParam("id") Long id
    ) throws InternalServerErrorException {

        Surat surat = suratService.getByIdSurat(id);
        DokumenRiwayatDto dto = new DokumenRiwayatDto();
        List<DokumenRiwayatItemDto> penerima = new ArrayList<>();
        List<DokumenRiwayatItemDto> tembusan = new ArrayList<>();
        List<DokumenRiwayatItemDto> disposisi = new ArrayList<>();
        for (PenerimaSurat penerimaSurat : surat.getPenerimaSurat()) {
            DokumenRiwayatItemDto aa = new DokumenRiwayatItemDto(interactionService.getBySuratIdOrCreateByOrg(surat, penerimaSurat.getOrganization().getIdOrganization()));
            MasterDelegasi delegasi = delegasiService.getByIdOrg(penerimaSurat.getOrganization().getIdOrganization());
            if (delegasi != null) {
                aa.setJabatan(delegasi.getTipe() + " " + delegasi.getFrom().getOrganizationName());
            }
            penerima.add(aa);
        }
        dto.setPenerima(penerima);
        if(surat.getTembusanSurat().size()>0) {
            for (TembusanSurat tembusanSurat : surat.getTembusanSurat()) {
                DokumenRiwayatItemDto bb = new DokumenRiwayatItemDto(interactionService.getBySuratIdOrCreateByOrg(surat, tembusanSurat.getOrganization().getIdOrganization()));
                MasterDelegasi delegasi = delegasiService.getByIdOrg(tembusanSurat.getOrganization().getIdOrganization());
                if (delegasi != null) {
                    bb.setJabatan(delegasi.getTipe() + " " + delegasi.getFrom().getOrganizationName());
                }
                tembusan.add(bb);
            }
            dto.setTembusan(tembusan);
        }
        if(surat.getSuratDisposisi().size()>0) {
            for (SuratDisposisi disposisiSurat : surat.getSuratDisposisi()) {
                DokumenRiwayatItemDto cc = new DokumenRiwayatItemDto(interactionService.getBySuratIdOrCreateByOrg(surat, disposisiSurat.getOrganization().getIdOrganization()));
                MasterDelegasi delegasi = delegasiService.getByIdOrg(disposisiSurat.getOrganization().getIdOrganization());
                if (delegasi != null) {
                    cc.setJabatan(delegasi.getTipe() + " " + delegasi.getFrom().getOrganizationName());
                }
                disposisi.add(cc);
            }
            dto.setDisposisi(disposisi);
        }


        return Response
                .ok(dto)
                .header("X-Total-Count", 1)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getByNoDokumen/{id}")
    @ApiOperation(
            value = "Get Surat by Nomor Dokumen",

            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat by Nomor Dokumen")
    public Response getSuratByNoDokumen(
            @NotNull @PathParam("id") String id
    ) throws InternalServerErrorException {

        Surat surat = suratService.getByNoDokumen(id);
        SuratDto result = new SuratDto();
        if (surat != null) {
            result = new SuratDto(surat);
            return Response
                    .ok(result)
                    .header("X-Total-Count", 1)
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        }

        return Response
                .ok(new MessageDto("0", "Data Not Found"))
                .header("X-Total-Count", 0)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();


    }

    @GET
    @Path("getReferensi/{id}")
    @ApiOperation(
            value = "Get Referensi Surat by Id Surat",
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat by Id Surat")
    public Response getRef(
            @NotNull @PathParam("id") Long id
    ) throws InternalServerErrorException {
        List<DokumenReferensiDto> refferences = suratService.getRef(id);

        return Response
                .ok(refferences)
                .header("X-Total-Count", 1)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();

    }

    @POST
    @Path("arsipkan/{id}")
    @ApiOperation(
            value = "Get Referensi Surat by Id Surat",
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat by Id Surat")
    public Response arsipkan(
            @NotNull @PathParam("id") Long id,
            @NotNull @QueryParam("idArsip") Long idArsip) throws InternalServerErrorException {
        try {
            Surat surat = suratService.arsipkan(id, idArsip);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil diarsipkan", id));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal mengarsipkan %s': %s", idArsip, id));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }

    @GET
    @Path("generatePublishDoc/{idSurat}")
    @ApiOperation(
            value = "Generate pdf dari draft dokumen",
            produces = MediaType.APPLICATION_JSON,
            notes = "Return DocId pdf Dokumen")
    public Response generatePublishDoc(
            @NotNull @PathParam("idSurat") Long idSurat) {

        try {
            String docId = suratService.generatePublishDoc(idSurat);

            Map result = new HashMap();

            result.put("docId", docId);

            notificationService.sendInfo("Info", String.format("Surat (id:%s), publish pdf generated", idSurat));

            return Response
                    .ok(result)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("searchDoc")
    @ApiOperation(
            value = "Get Dokumen By Search",
            response = SuratDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Dokumen Searched")
    public Response searchDokumen(
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("") @QueryParam("sent") String tglDikirim,
            @DefaultValue("") @QueryParam("idArea") String idArea,
            @DefaultValue("") @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idKKeamanan") Long idKKeamanan,
            @DefaultValue("") @QueryParam("idPrioritas") Long idPrioritas,
            @DefaultValue("") @QueryParam("idKonseptor") Long idKonseptor,
            @DefaultValue("") @QueryParam("idPengirim") Long idPengirim,
            @DefaultValue("") @QueryParam("idPenerima") Long idPenerima,
            @DefaultValue("false") @QueryParam("otherUnit") Boolean otherUnit){

        if(!Strings.isNullOrEmpty(filter) || !Strings.isNullOrEmpty(tglDikirim) || !Strings.isNullOrEmpty(idArea)
                || idJenisDokumen != null || idPrioritas != null || idKonseptor != null || idPenerima !=null || otherUnit.booleanValue() != false) {
            HashMap<String, Object> temp = suratService.searchDokumen(filter, tglDikirim, idArea, idJenisDokumen, idKKeamanan, idPrioritas, idKonseptor, idPengirim, idPenerima, otherUnit);
            List<CariDokumenDto> result = (List<CariDokumenDto>) temp.get("data");
            long count = (long) temp.get("length");

            return Response
                    .ok(result)
                    .header("X-Total-Count", count)
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        }else{

            long count = 0;

            return Response
                    .ok(new ArrayList<>())
                    .header("X-Total-Count", count)
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        }
    }

    @GET
    @Path("/distribusi")
    @ApiOperation(
            value = "Get Surat for Distribusi",
            response = SuratForDistribusiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat for Distribusi")
    public Response getSuratForDistribusi(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false")
            @QueryParam("descending") boolean descending) {
        List<Surat> all = new ArrayList<>(suratService.getResultList(filter, sort, descending, skip, limit));
        List<SuratForDistribusiDto> result = suratService.mapToSuratForDistribusiDto(all);

        long count = suratService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}
