package com.qtasnim.eoffice.ws;


import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.ws.dto.DemoEntity;
import io.swagger.annotations.*;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.Date;

/**
 * JWT-geschützte Demo-Rest-Resource
 *
 * @author Dominik Mathmann, GEDOPLAN
 */
@Path("demo")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api(
        value = "Demo",
        description = "Demo"
)
public class DemoResource {

  @GET
  @Secured
  public DemoEntity getHello() {
    return new DemoEntity(1, "Hello World");
  }

  @POST
  @Secured
  @ApiOperation(
          value = "Demo",
          produces = MediaType.APPLICATION_JSON,
          notes = "Demo")
  public Response post(
          @Valid DemoEntity demoEntity
  ) {
    return Response.ok().build();
  }

  @POST
  @Path(value = "upload")
  @Secured
  @Produces({ MediaType.APPLICATION_JSON})
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @ApiOperation(
          value = "Upload",
          produces = MediaType.APPLICATION_JSON)
  @ApiImplicitParams({
          @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
  })
  public Response upload(
          @FormDataParam(value="file") InputStream inputStream,
          @ApiParam(hidden = true) @FormDataParam(value="file") FormDataContentDisposition fileDisposition
  ) {
    return Response.ok().build();
  }

  @POST
  @Path(value = "demo2")
  @Secured
  @ApiOperation(
          value = "Demo2",
          produces = MediaType.APPLICATION_JSON,
          notes = "Demo2")
  public Response post2(
          @NotNull @FormParam(value = "name") String name
  ) {
    return Response.ok().build();
  }

  @GET
  @Path("message")
  public String getMessage() {
    return "Hello world from WS - " + new Date();
  }
}
