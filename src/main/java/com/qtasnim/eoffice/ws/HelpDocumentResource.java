package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.HelpDocument;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.CompanyCodeService;
import com.qtasnim.eoffice.services.HelpDocumentService;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.CompanyCodeDto;
import com.qtasnim.eoffice.ws.dto.HelpDocumentDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.io.InputStream;
import java.util.Comparator;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

@Path("help-document")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Help Document Resource",
        description = "WS Endpoint untuk menghandle operasi HelpDocument"
)
public class HelpDocumentResource {

    @Inject
    private HelpDocumentService service;

    @Inject
    CompanyCodeService companyService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = HelpDocumentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Help Document list")
    public Response getHelp(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<HelpDocumentDto> result = service.getResultList(filter, sort, descending, skip, limit).stream().
                map(HelpDocumentDto::new).collect(Collectors.toList());
        long count = service.count(filter);

        service.setFiles(result);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("media")
    @ApiOperation(
            value = "Get Data",
            response = HelpDocumentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Help Document list")
    public Response getHelpDocument(
            @QueryParam("tipe") String tipe,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) throws InternalServerErrorException {
        JPAJinqStream<HelpDocument> all = service.getAll().where(q -> q.getTipe().equals(tipe));
        List<HelpDocumentDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(HelpDocument::getId).reversed())
                .map(HelpDocumentDto::new)
                .collect(Collectors.toList());
        long count = all.count();

        service.setFiles(result);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Add",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Add Help Document")
    public Response saveHelp(
            @Valid @FormDataParam(value = "dto") HelpDocumentDto dao,
            @FormDataParam(value = "file") InputStream stream,
            @FormDataParam("file") FormDataBodyPart body) {
        try {
            service.saveHelpDocument(dao, stream, body);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("'%s' gagal disimpan", dao.getNama()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Help Document")
    public Response deleteHelp(@NotNull @PathParam("id") Long id, @NotNull @QueryParam("fileId") Long fileId) {

        try {
            service.delete(id, fileId);
            notificationService.sendInfo("Info", "Data berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("helpdesk")
    @ApiOperation(
            value = "Get Data",
            response = CompanyCodeDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Helpdesk Information")
    public Response getHelpDesk() throws InternalServerErrorException {

        CompanyCodeDto result = Optional.of(userSession.getUserSession().getUser().getCompanyCode()).map(CompanyCodeDto::new).get();

        return Response
                .ok(result)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}
