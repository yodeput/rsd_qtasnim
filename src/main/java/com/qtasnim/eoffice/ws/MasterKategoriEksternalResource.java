package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.MasterKategoriEksternal;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterKategoriEksternalService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterKategoriEksternalDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-kategori-eksternal")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Kategori Eksternal Resource",
        description = "WS Endpoint untuk menghandle operasi Kategori Eksternal"
)
public class MasterKategoriEksternalResource {
    @Inject
    private MasterKategoriEksternalService kategoriEksternalService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get",
            response = MasterKategoriEksternal[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterKategoriEksternal> all = kategoriEksternalService.getAll();
        List<MasterKategoriEksternalDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterKategoriEksternal::getId))
                .map(MasterKategoriEksternalDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered",
            response = MasterKategoriEksternalDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{

        List<MasterKategoriEksternal> data = kategoriEksternalService.getResultList(filter, sort, descending, skip, limit);
        long count = kategoriEksternalService.count(filter);

        List<MasterKategoriEksternalDto> result = data.stream().map(q->new MasterKategoriEksternalDto(q)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save as New")
    public Response saveData(@Valid MasterKategoriEksternalDto dao) {
        try {
            kategoriEksternalService.saveOrEdit(dao, null);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNama(), ex.getMessage()));
            notificationService.sendError("Error", String.format("'%s' gagal disimpan", dao.getNama()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save an update")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterKategoriEksternalDto dao) {
        try {
            kategoriEksternalService.saveOrEdit(dao, id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNama(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            MasterKategoriEksternal model = kategoriEksternalService.find(id);
            kategoriEksternalService.remove(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
