package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.Card;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.CardService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterAreaDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.ws.dto.CardDto;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;

@Path("card")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Card Resource",
        description = "WS Endpoint untuk menghandle operasi Card"
)
public class CardResource {
    @Inject
    private CardService cardService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterAreaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Division list")
    public Response getFilteredData(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("") @QueryParam("progress") String progress,
            @QueryParam("idPrioritas") Long idPrioritas,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        CriteriaQuery<Card> query = cardService.getAllQuery(filter, sort, descending, progress, idPrioritas);
        List<CardDto> result = cardService.execCriteriaQuery(query, skip, limit)
                .stream().map(CardDto::new)
                .collect(Collectors.toList());
        long count = cardService.countCriteriaQuery(query);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Card")
    public Response saveData(@Valid CardDto dao) {
        try {
            cardService.saveOrEdit(dao, null);
            notificationService.sendInfo("Info", String.format("Tugas '%s' berhasil disimpan", dao.getTitle()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNama(), ex.getMessage()));
            notificationService.sendError("Error", String.format("'%s' gagal disimpan", dao.getTitle()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Card")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid CardDto dao) {
        try {
            cardService.saveOrEdit(dao, id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getTitle()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getTitle(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Card")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            Card deleted = cardService.deleteCard(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getTitle()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @GET
    @Path("needAttention/count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response needAttentionCount(@QueryParam("idOrganization") Long idOrganization) {
        try {
            CriteriaQuery<Card> query = cardService.getCountQuery(idOrganization);
            long count = cardService.countCriteriaQuery(query);
            
            return Response
                    .ok(count)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}