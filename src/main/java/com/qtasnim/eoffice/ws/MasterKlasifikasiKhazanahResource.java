package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterKlasifikasiKhazanah;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterKlasifikasiKhazanahService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.FieldErrorDto;
import com.qtasnim.eoffice.ws.dto.MasterKlasifikasiKhazanahDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;

@Path("master-klasifikasi-khazanah")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Klasifikasi Khazanah Resource",
        description = "WS Endpoint untuk menghandle operasi Klasifikasi Khazanah"
)
public class MasterKlasifikasiKhazanahResource {

    @Inject
    private MasterKlasifikasiKhazanahService klasifikasiKhazanahService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterKlasifikasiKhazanahDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Klasifikasi Masalah list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<MasterKlasifikasiKhazanah> all = klasifikasiKhazanahService.getAll();
        List<MasterKlasifikasiKhazanahDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterKlasifikasiKhazanah::getId))
                .map(MasterKlasifikasiKhazanahDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterKlasifikasiKhazanahDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Klasifikasi Masalah list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterKlasifikasiKhazanah> data = klasifikasiKhazanahService.getResultList(filter, sort, descending, skip, limit);
        long count = klasifikasiKhazanahService.count(filter);

        List<MasterKlasifikasiKhazanahDto> result = data.stream().map(q -> new MasterKlasifikasiKhazanahDto(q)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("parent")
    @ApiOperation(
            value = "Get Data By Parent",
            response = MasterKlasifikasiKhazanahDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Klasifikasi Masalah list By Parent")
    public Response getByParentId(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @QueryParam("idParent") Long idParent,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {

        JPAJinqStream<MasterKlasifikasiKhazanah> all = klasifikasiKhazanahService.getByParent(idParent, sort, descending);
        List<MasterKlasifikasiKhazanahDto> result = all
                .skip(skip)
                .limit(limit)
                .map(MasterKlasifikasiKhazanahDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("parent-name")
    @ApiOperation(
            value = "Get Data By Parent Name",
            response = MasterKlasifikasiKhazanahDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Klasifikasi Masalah list By Parent Name")
    public Response getByParentName(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @QueryParam("parent") String parent,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {

        JPAJinqStream<MasterKlasifikasiKhazanah> all = klasifikasiKhazanahService.getByParentName(parent, filter, sort, descending);
        List<MasterKlasifikasiKhazanahDto> result = all
                .skip(skip)
                .limit(limit)
                .map(MasterKlasifikasiKhazanahDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Klasifikasi Masalah")
    public Response saveData(@Valid MasterKlasifikasiKhazanahDto dao) throws InternalServerErrorException {

        List<MasterKlasifikasiKhazanah> list = klasifikasiKhazanahService.findAll().stream().filter(q->q.getCompanyCode().getId().equals(dao.getIdCompany())).collect(Collectors.toList());
        String hasil="";
        if(!list.isEmpty()) {
            hasil = this.validate(null, dao.getNama(), dao.getParent(), list);
        }

        if (!StringUtils.isEmpty(hasil)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {

            try {
                klasifikasiKhazanahService.saveOrEdit(dao, null);
                notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNama()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil disimpan"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                //notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNama(), ex.getMessage()));
                notificationService.sendError("Error", String.format("'%s' gagal disimpan", dao.getNama()));

                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Klasifikasi Masalah")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterKlasifikasiKhazanahDto dao) throws InternalServerErrorException {

        List<MasterKlasifikasiKhazanah> list = klasifikasiKhazanahService.findAll().stream().filter(q->q.getCompanyCode().getId().equals(dao.getIdCompany())).collect(Collectors.toList());
        String hasil ="";
        if(!list.isEmpty()) {
            hasil = this.validate(id, dao.getNama(), dao.getParent(), list);
        }

        if (!StringUtils.isEmpty(hasil)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {

            try {
                klasifikasiKhazanahService.saveOrEdit(dao, id);
                notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNama()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil diubah"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                //notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNama(), ex.getMessage()));
                notificationService.sendError("Error", String.format("'%s' gagal diubah", dao.getNama()));

                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Klasifikasi Masalah")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            MasterKlasifikasiKhazanah model = klasifikasiKhazanahService.find(id);
            klasifikasiKhazanahService.remove(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private String validate(Long id, String nama, MasterKlasifikasiKhazanahDto parent, List<MasterKlasifikasiKhazanah> list) {
        List<MasterKlasifikasiKhazanah> dataList;
        
        if (parent == null) {
            dataList = list.stream().filter(t -> t.getParent() == null).collect(Collectors.toList());
        } else {
            dataList = list.stream().filter(t -> t.getParent() != null && Objects.equals(t.getParent().getId(), parent.getId())).collect(Collectors.toList());
        }
        
        if (id != null) {
            dataList = dataList.stream().filter(q -> !Objects.equals(q.getId(), id)).collect(Collectors.toList());
        }
        
        if (dataList != null) {
            boolean notValid = dataList.stream().anyMatch(t -> nama.equals(t.getNama()));
            return notValid ? "nama" : "" ;
        }
        
        return "";
    }

    private MessageDto prepareMessage(String field) {
        String msg;
        switch (field) {
            case "nama":
                msg = "Nama";
                break;
            case "kode":
                msg = "Kode";
                break;
            default:
                msg = "Nama dan Kode";
                break;
        }
        MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();

        FieldErrorDto fieldErrorDto = new FieldErrorDto();
        fieldErrorDto.setField(field);
        fieldErrorDto.setMessage(msg + " Klasifikasi Khazanah sudah di gunakan.");
        fieldErrorDtos.add(fieldErrorDto);

        messageDto.setFields(fieldErrorDtos);

        return messageDto;
    }
}
