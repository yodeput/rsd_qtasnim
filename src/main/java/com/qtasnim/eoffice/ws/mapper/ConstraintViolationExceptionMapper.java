package com.qtasnim.eoffice.ws.mapper;

import com.qtasnim.eoffice.ws.dto.FieldErrorDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterators;
import java.util.stream.StreamSupport;

@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
    @Override
    public Response toResponse(final ConstraintViolationException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(prepareMessage(exception))
                .build();
    }

    private MessageDto prepareMessage(ConstraintViolationException exception) {
        MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();

        for (ConstraintViolation<?> cv : exception.getConstraintViolations()) {
            FieldErrorDto fieldErrorDto = new FieldErrorDto();
            Iterator iterator = cv.getPropertyPath().iterator();

            while (iterator.hasNext()) {
                fieldErrorDto.setField(iterator.next().toString());
            }

            fieldErrorDto.setMessage(cv.getMessage());
            fieldErrorDtos.add(fieldErrorDto);
        }

        messageDto.setFields(fieldErrorDtos);

        return messageDto;
    }
}
