package com.qtasnim.eoffice.ws.mapper;


import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.security.NotAuthorizedException;
import com.qtasnim.eoffice.ws.dto.MessageDto;

import javax.swing.text.html.Option;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Optional;

@Provider
public class InternalServerErrorExceptionMapper implements ExceptionMapper<InternalServerErrorException> {
    @Override
    public Response toResponse(InternalServerErrorException exception) {
        return Response
                .ok(new MessageDto(Optional.ofNullable(exception.getCause()).map(Throwable::getMessage).orElse(exception.getMessage()), "error"))
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .header("X-Reason", Optional.ofNullable(exception.getCause()).map(Throwable::getMessage).orElse(exception.getMessage()))
                .header("Access-Control-Expose-Headers", "X-Reason")
                .build();
    }
}
