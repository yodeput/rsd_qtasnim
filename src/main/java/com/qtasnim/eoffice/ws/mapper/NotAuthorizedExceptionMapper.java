package com.qtasnim.eoffice.ws.mapper;


import com.qtasnim.eoffice.security.NotAuthorizedException;
import com.qtasnim.eoffice.security.UserNotFoundException;
import com.qtasnim.eoffice.ws.dto.MessageDto;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Optional;

@Provider
public class NotAuthorizedExceptionMapper implements ExceptionMapper<UserNotFoundException> {
    @Override
    public Response toResponse(UserNotFoundException exception) {
        return Response
                .ok(new MessageDto(Optional.ofNullable(exception.getCause()).map(Throwable::getMessage).orElse(exception.getMessage()), "error"))
                .status(Response.Status.UNAUTHORIZED)
                .header("X-Reason", exception.getMessage())
                .header("Access-Control-Expose-Headers", "X-Reason")
                .build();
    }
}
