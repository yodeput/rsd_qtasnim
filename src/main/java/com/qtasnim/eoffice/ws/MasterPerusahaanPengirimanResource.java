package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterPerusahaanPengiriman;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.services.MasterPerusahaanPengirimanService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterPerusahaanPengirimanDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-perusahaan-pengiriman")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Perusahaan Pengiriman Resource",
        description = "WS Endpoint untuk menghandle operasi Perusahaan Pengiriman"
)
public class MasterPerusahaanPengirimanResource {
    @Inject
    private MasterPerusahaanPengirimanService service;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data Area",
            response = MasterPerusahaanPengirimanDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Area list")
    public Response getArea(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<MasterPerusahaanPengiriman> all = service.getAll();
        List<MasterPerusahaanPengirimanDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterPerusahaanPengiriman::getId))
                .map(MasterPerusahaanPengirimanDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
    
    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Data",
            response = MasterPerusahaanPengirimanDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return PerusahaanPengiriman list")
    public Response getCountries(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterPerusahaanPengirimanDto> result = service.getResultList(filter, sort, descending, skip, limit).stream().
                map(MasterPerusahaanPengirimanDto::new).collect(Collectors.toList());
        long count = service.count(filter);

        return Response
            .ok(result)
            .header("X-Total-Count", count)
            .header("Access-Control-Expose-Headers", "X-Total-Count")
            .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add PerusahaanPengiriman")
    public Response savePerusahaanPengiriman(@Valid MasterPerusahaanPengirimanDto dao) {
        try{
            service.save(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNama(), ex.getMessage()));
            notificationService.sendError("Error", String.format("'%s' gagal disimpan", dao.getNama()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit PerusahaanPengiriman")
    public Response editPerusahaanPengiriman(@NotNull @PathParam("id") Long id, @Valid MasterPerusahaanPengirimanDto dao) {

        try{
            service.save(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNama(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete PerusahaanPengiriman")
    public Response deletePerusahaanPengiriman(@NotNull @PathParam("id") Long id) {

        try{
            MasterPerusahaanPengiriman deleted = service.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getNama()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}