package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.services.ProcessTaskService;
import com.qtasnim.eoffice.services.SuratService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.ArrayList;

@Path("monitoring")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class MonitoringResource {
    @Inject
    private SuratService suratService;
    @Inject
    private ProcessTaskService taskService;

    @Inject
    @ISessionContext
    private Session userSession;

    @GET
    @Path("{moduleName}")
    public Response getFilteredByName(
        @PathParam("moduleName") String moduleName,
        @DefaultValue("") @QueryParam("filterValue") String filterValue,
        @DefaultValue("") @QueryParam("status") String status,
        @DefaultValue("") @QueryParam("startDate") String startDate,
        @DefaultValue("") @QueryParam("endDate") String endDate,
        @DefaultValue("0") @QueryParam("skip") int skip,
        @DefaultValue("10") @QueryParam("limit") int limit) {

        long count = 0;
        
        // switch(moduleName) {

        //     case "itsr":
        //     case "itcr":
        //         boolean isServiceRequest = moduleName.equalsIgnoreCase("itsr");
        //         status = Strings.isNullOrEmpty(status) ? "APPROVED" : status;
        //         JPAJinqStream<SuratService> all = suratService.getFiltered(isServiceRequest, filterValue, status, startDate, endDate);

        //         count = all.count();
        //         List<SuratDto> result = all.skip(skip).limit(limit)
        //                 .sorted(Comparator.comparing(Surat::getId))
        //                 .map(SuratDto::new)
        //                 .collect(Collectors.toList());
                
        //         return Response.ok(result)
        //                 .header("X-Total-Count", count)
        //                 .header("Access-Control-Expose-Headers", "X-Total-Count").build();


        //     //------------------- END itsr/cr


        //     case "itwo":
        //     case "itwo-eksternal":
        //         // boolean isInternal = moduleName.equalsIgnoreCase("itwo");
        //         status = Strings.isNullOrEmpty(status) ? "APPROVED" : status;
        //         JPAJinqStream<WorkOrder> woList = workOrderService.getFiltered(filterValue, status, startDate, endDate);	// internal && eksternal

        //         count = woList.count();
        //         List<WorkOrderDto> resultWorkOrder = woList.skip(skip).limit(limit)
        //                 .sorted(Comparator.comparing(WorkOrder::getId))
        //                 .map(WorkOrderDto::new)
        //                 .collect(Collectors.toList());

	// 			List<String> ids = resultWorkOrder.stream().map(t -> t.getId().toString()).collect(Collectors.toList());
	// 			List<ProcessTask> processTasks = taskService.getByRecordRefIds(Stream.of("itwo-eksternal", "itwo").collect(Collectors.toList()), ids);
        //         resultWorkOrder.forEach(t -> {

	// 				String formId = (t.getIsEksternalProses() && !t.getIsInternal()) ? "itwo-eksternal" : "itwo";

	// 				processTasks.stream()
	// 					.filter(pt -> pt.getProcessInstance().getRecordRefId().equals(t.getId().toString())
	// 							&& pt.getProcessInstance().getProcessDefinition().getFormName().equals(formId))
	// 					.forEach(processTask -> {
	// 						// t.getAssignees().add(new MasterUserDto(processTask.getAssignee().getUser()));
	// 						t.getWorkflowCurrentTasks().add(processTask.getTaskName());
	// 					});

        //         });
                
        //         return Response.ok(resultWorkOrder)
        //                 .header("X-Total-Count", count)
        //                 .header("Access-Control-Expose-Headers", "X-Total-Count").build();


        //     //------------------- END itwo/eksternal


        //     case "itwoc":
        //         status = Strings.isNullOrEmpty(status) ? "APPROVED" : status;
        //         JPAJinqStream<WorkOrderCompletion> wocList = wocService.getFiltered(filterValue, status, startDate, endDate);

        //         count = wocList.count();
        //         List<WorkOrderCompletionDto> resultWoc = wocList.skip(skip).limit(limit)
        //                 .sorted(Comparator.comparing(WorkOrderCompletion::getId))
        //                 .map(WorkOrderCompletionDto::new)
        //                 .collect(Collectors.toList());
                
        //         return Response.ok(resultWoc)
        //                 .header("X-Total-Count", count)
        //                 .header("Access-Control-Expose-Headers", "X-Total-Count").build();

        //     //------------------- END itwoc                        

        //     case "bast":
        //     case "basto":
        //         boolean isBast = moduleName.equalsIgnoreCase("bast");
        //         status = Strings.isNullOrEmpty(status) ? "APPROVED" : status;
        //         JPAJinqStream<Bast> bastList = bastService.getFiltered(isBast, filterValue, status, startDate, endDate);

        //         count = bastList.count();
        //         List<BastDto> resultBast = bastList.skip(skip).limit(limit)
        //                 .sorted(Comparator.comparing(Bast::getId))
        //                 .map(BastDto::new)
        //                 .collect(Collectors.toList());
                
        //         return Response.ok(resultBast)
        //                 .header("X-Total-Count", count)
        //                 .header("Access-Control-Expose-Headers", "X-Total-Count").build();

        //     default:
        //         return Response.ok(new ArrayList<>())
        //                 .header("X-Total-Count", count)
        //                 .header("Access-Control-Expose-Headers", "X-Total-Count").build();

        // }

        return Response.ok(new ArrayList<>())
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count").build();
    }

}
