package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterModul;
import com.qtasnim.eoffice.db.MasterRole;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterModulService;
import com.qtasnim.eoffice.services.MasterUserService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.services.RoleService;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("role")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "User Management Role",
        description = "WS Endpoint untuk menghandle operasi Role"
)
public class RoleResouce {
    @Inject
    private RoleService roleService;
    
    @Inject
    private MasterUserService masterUserService;
    
    @Inject
    private MasterModulService modulService;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get All Role",
            response = RoleDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Role list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterRole> all = roleService.getAll();
        List<RoleDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterRole::getIdRole))
                .map(RoleDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = RoleDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Role list Filtered")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending,
                                @DefaultValue("false") @QueryParam("loadAll") boolean loadAll) throws InternalServerErrorException {

        List<MasterRole> data = roleService.getResultList(filter, sort, descending, skip, limit);
        long count = roleService.count(filter);

        List<RoleDto> result = data.stream().map(q->new RoleDto(q,loadAll)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Role")
    public Response saveData(@Valid RoleDto dao) {
        try {
//            boolean isValid = roleService.checkLevel(dao);
//            if(isValid) {
                roleService.saveOrEdit(dao, null);
                notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getRoleName()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil disimpan"))
                        .status(Response.Status.OK)
                        .build();
//            }else{
//                throw  new Exception(String.format("Role dengan level : %s sudah ada",dao.getLevel()));
//            }
        } catch (Exception e) {
            if(!Strings.isNullOrEmpty(e.getMessage())) {
                notificationService.sendError("Error",e.getMessage());
                return Response
                        .status(Response.Status.GONE)
                        .entity(e.getMessage())
                        .build();
            }else{
                return Response.status(Response.Status.NOT_MODIFIED).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Role")
    public Response editData(@NotNull @PathParam("id") Long id,@Valid RoleDto dao) {
        try {
//            boolean isValid = roleService.checkLevel(dao);
//            if(isValid) {
                roleService.saveOrEdit(dao, id);
                notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getRoleName()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil disimpan"))
                        .status(Response.Status.OK)
                        .build();
//            }else{
//                throw  new Exception(String.format("Role dengan level : %s sudah ada",dao.getLevel()));
//            }
        } catch (Exception e) {
            if(!Strings.isNullOrEmpty(e.getMessage())) {
                notificationService.sendError("Error",e.getMessage());
                return Response
                        .status(Response.Status.GONE)
                        .entity(e.getMessage())
                        .build();
            }else{
                return Response.status(Response.Status.NOT_MODIFIED).build();
            }
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Role")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            roleService.deleteRole(id);
            notificationService.sendInfo("Info", String.format("Data berhasil dihapus"));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception e) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
    
    @GET
    @Path("listModule")
    @ApiOperation(
            value = "Get Module By Parent",
            response = RoleModuleDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Module List By Parent")
    public Response getModules(
            @QueryParam("idParent") Long idParent,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit){
        JPAJinqStream<MasterModul> all = modulService.getByParent(idParent,sort,descending);
        List<RoleModuleDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterModul::getIdModul))
                .map(q->{
                    RoleModuleDto rm = new RoleModuleDto();
                    rm.setIdModul(q.getIdModul());
                    rm.setModule(new MasterModulDto(q));
                    return rm;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("{idOrg}")
    @ApiOperation(
            value = "Get Role By Id Organization",
            response = RoleDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Role list by Id Organization")
    public Response getByIdOrg(@PathParam("idOrg") Long idOrg) {

        JPAJinqStream<MasterRole> all = roleService.getByIdOrg(idOrg);
        List<RoleDto> result = all.sorted(Comparator.comparing(MasterRole::getIdRole))
                .map(RoleDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("detail/{idRole}")
    @ApiOperation(
            value = "Get Role",
            response = RoleDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Role Object")
    public Response getDetail(@NotNull @PathParam("idRole") Long idRole) {
        RoleDto role = roleService.getByIdRole(idRole);
        return Response
                .ok(role)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getUsers")
    @ApiOperation(
            value = "Get User",
            response = MasterUserDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return User by Name")
    public Response getUserByName(@NotEmpty @QueryParam("nama") String nama) {

        JPAJinqStream<MasterUser> all = masterUserService.getByName(nama);
        List<MasterUserDto> result = all.sorted(Comparator.comparing(MasterUser::getId)).map(MasterUserDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("all")
    @ApiOperation(
            value = "Get All Role",
            response = RoleDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return All Role")
    public Response getAllRole() {
        JPAJinqStream<MasterRole> all = roleService.getAll();
        List<RoleDto> result = all
                .sorted(Comparator.comparing(MasterRole::getIdRole))
                .map(RoleDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}
