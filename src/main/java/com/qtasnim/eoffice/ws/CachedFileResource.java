package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.caching.FileCache;
import com.qtasnim.eoffice.security.Secured;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.UUID;

@Path("cached-file")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class CachedFileResource {
    @Inject
    private FileCache fileCache;

    @POST
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response upload(
            @FormDataParam(value = "file") InputStream inputStream) {
        try {
            String cacheId = UUID.randomUUID().toString();
            fileCache.put(cacheId, IOUtils.toByteArray(inputStream));

            return Response.ok(cacheId).status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response delete(
            @PathParam(value = "id") String id) {
        try {
            fileCache.remove(id);

            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}
