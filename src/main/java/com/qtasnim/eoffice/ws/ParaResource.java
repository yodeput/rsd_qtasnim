package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Para;
import com.qtasnim.eoffice.db.ParaDetail;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Path("para")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Para Resource",
        description = "WS Endpoint untuk menghandle operasi Para/Group Para"
)
public class ParaResource {

    @Inject
    private ParaService paraService;

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    private ParaDetailService paraDetailService;

    @Inject
    private KontakFavoritService kontakFavoritService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @Inject
    private FileService fileService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = ParaResultDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Para list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<Para> all = paraService.getAll();
        List<ParaResultDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Para::getIdPara))
                .map(ParaResultDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = ParaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Para list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("") @QueryParam("start") String start,
            @DefaultValue("") @QueryParam("end") String end,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

//        List<Para> data = paraService.getResultList(filter, sort, descending, skip, limit);
//        long count = paraService.count(filter);
//
//        List<ParaResultDto> result = data.stream().map(q->new ParaResultDto(q)).collect(Collectors.toList());
        HashMap<String, Object> temp = paraService.getFiltered(filter, start, end, skip, limit, descending, sort);
        List<ParaDto> result = (List<ParaDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Para")
    public Response saveData(@Valid ParaDto dao) {
        boolean isValid = paraService.validate(null,dao.getNamaPara(),dao.getCompanyId());
        if(!isValid){
            MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
            List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();
            FieldErrorDto fieldErrorDto = new FieldErrorDto();
            fieldErrorDto.setField("namaPara");
            fieldErrorDto.setMessage("Nama Group para sudah di gunakan.");
            fieldErrorDtos.add(fieldErrorDto);
            messageDto.setFields(fieldErrorDtos);
            return Response.status(Response.Status.BAD_REQUEST).entity(messageDto).build();
        }else {
            try {
                paraService.saveOrEdit(dao, null);
                notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNamaPara()));

                return Response
                        .ok()
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNamaPara(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Para")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid ParaDto dao) {
        boolean isValid = paraService.validate(id,dao.getNamaPara(),dao.getCompanyId());
        if(!isValid){
            MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
            List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();
            FieldErrorDto fieldErrorDto = new FieldErrorDto();
            fieldErrorDto.setField("namaPara");
            fieldErrorDto.setMessage("Nama Group para sudah di gunakan.");
            fieldErrorDtos.add(fieldErrorDto);
            messageDto.setFields(fieldErrorDtos);
            return Response.status(Response.Status.BAD_REQUEST).entity(messageDto).build();
        }else {
            try {
                paraService.saveOrEdit(dao, id);
                notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNamaPara()));

                return Response
                        .ok()
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNamaPara(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Data")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            paraService.deleteData(id);
            notificationService.sendInfo("Info", "Data berhasil dihapus");
            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("paraDetail/{idPara}")
    @ApiOperation(
            value = "Save Para Detail",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Para Detail")
    public Response saveParaDetail(List<Long> ordIds, @NotNull @PathParam("idPara") Long idPara) {
        try {
            paraDetailService.saveOrEdit(ordIds, idPara);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @GET
    @Path("{idPara}")
    @ApiOperation(
            value = "Get Para Data by Id",
            response = ParaDetailDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Para data by Id")
    public Response getDataPara(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @PathParam("idPara") Long idPara) {
        JPAJinqStream<Para> all = paraService.getDataById(idPara);
        List<ParaDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Para::getIdPara))
                .map(ParaDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getStruktur")
    @ApiOperation(
            value = "Get Struktur",
            response = MasterStrukturOrganisasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Struktur Tree List")
    public Response getStruktur(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterStrukturOrganisasi> all = masterStrukturOrganisasiService.getAllValid();
        List<MasterStrukturOrganisasiDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterStrukturOrganisasi::getIdOrganization))
                .map(MasterStrukturOrganisasiDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("getStrukturByParent")
    @ApiOperation(
            value = "Get Struktur",
            response = MasterStrukturOrganisasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Struktur Tree List")
    public Response getStrukturByParent(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @QueryParam("idParent") Long idParent,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending,
            KewenanganPenerimaDto dao) {
        try {
            List<MasterStrukturOrganisasiDto> result = new ArrayList<>();
            JPAJinqStream<MasterStrukturOrganisasi> all = masterStrukturOrganisasiService.getByParent(idParent, sort, descending);
            List<MasterStrukturOrganisasi> kontakList = kontakFavoritService.getOrganisasi();
            if (dao == null) {
                result = all
                        .skip(skip)
                        .limit(limit)
                        .map(q -> {
                            MasterStrukturOrganisasiDto obj = new MasterStrukturOrganisasiDto();
                            if (kontakList.stream().anyMatch(a -> a.getIdOrganization().equals(q.getIdOrganization()))) {
                                obj.setIsFavorite(true);
                            } else {
                                obj.setIsFavorite(false);
                            }
                            DateUtil dateUtil = new DateUtil();
                            obj.setOrganizationId(q.getIdOrganization());
                            obj.setOrganizationCode(q.getOrganizationCode());
                            obj.setOrganizationName(q.getOrganizationName());
                            if (q.getParent() != null) {
                                obj.setParent(new MasterStrukturOrganisasiDto(q.getParent()));
                                obj.setParentId(q.getParent().getIdOrganization());
                            }
                            obj.setIsDeleted(q.getIsDeleted());
                            obj.setIsActive(q.getIsActive());
                            obj.setLevel(q.getLevel());
                            obj.setCreatedBy(q.getCreatedBy());
                            obj.setCreatedDate(dateUtil.getCurrentISODate(q.getCreatedDate()));
                            obj.setModifiedBy(q.getModifiedBy());
                            if (q.getModifiedDate() != null) {
                                obj.setModifiedDate(dateUtil.getCurrentISODate(q.getModifiedDate()));
                            }
                            obj.setStartDate(dateUtil.getCurrentISODate(q.getStart()));
                            obj.setEndDate(dateUtil.getCurrentISODate(q.getEnd()));
                            if (q.getUser() != null) {
                                obj.setUser(new MasterUserDto(q.getUser()));
                                obj.setIdUser(q.getUser().getId());
                            }
                            if (q.getRoleList() != null && !q.getRoleList().isEmpty()) {
                                obj.setRoleIds(q.getRoleList().stream().map(a -> a.getIdRole()).collect(Collectors.toList()));
                            }
                            if (q.getCompanyCode() != null) {
                                obj.setCompanyCode(new CompanyCodeDto(q.getCompanyCode()));
                                obj.setCompanyId(q.getCompanyCode().getId());
                            }
                            if (q.getArea() != null) {
                                obj.setPersa(new MasterAreaDto(q.getArea()));
                                obj.setIdPersa(q.getArea().getId());
                            }
                            obj.setIsHris(q.getIsHris());
                            obj.setSingkatan(q.getSingkatan());
                            return obj;
                        }).collect(Collectors.toList());
                long count = all.count();

                return Response
                        .ok(result)
                        .header("X-Total-Count", count)
                        .header("Access-Control-Expose-Headers", "X-Total-Count")
                        .build();
            } else {
                List<MasterStrukturOrganisasiDto> tmp = all
                        .skip(skip)
                        .limit(limit)
                        .map(MasterStrukturOrganisasiDto::new).collect(Collectors.toList());
                result = masterStrukturOrganisasiService.validateGrade(tmp, dao);
                return Response.ok(result)
                        .header("X-Total-Count", result.size())
                        .header("Access-Control-Expose-Headers", "X-Total-Count").build();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).entity(ex.getMessage()).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @GET
    @Path("getStrukturBy")
    @ApiOperation(
            value = "Get Struktur",
            response = MasterStrukturOrganisasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Struktur By Nama/Nipp")
    public Response getStrukturBy(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("nama") String nama,
            @DefaultValue("") @QueryParam("nipp") String nipp,
            @DefaultValue("") @QueryParam("posisi") String posisi,
            @DefaultValue("") @QueryParam("idKorsa") String idKorsa,
            @DefaultValue("") @QueryParam("idPersa") String idPersa,
            @DefaultValue("") @QueryParam("idGrade") String idGrade) {

        JPAJinqStream<MasterStrukturOrganisasi> all = masterStrukturOrganisasiService
                .getOrgBy(nama, nipp, idGrade, idKorsa, posisi, idPersa);

        long count = all.count();
        List<MasterStrukturOrganisasiDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterStrukturOrganisasi::getIdOrganization))
                .map(MasterStrukturOrganisasiDto::new).collect(Collectors.toList());

        getStarredContact(result);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("users")
    @ApiOperation(
            value = "Get Users",
            response = MasterUserDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return User List")
    public Response getUsers(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<MasterUser> all = masterUserService.getAllValid();
        List<MasterUserDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterUser::getId))
                .map(MasterUserDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/user/{idOrganization}")
    @ApiOperation(
            value = "Get Data By Organization Id",
            response = MasterUserDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return User By Organization Id")
    public Response getUserByIdOrg(@PathParam("idOrganization") Long idOrganization) {
        MasterUserDto user = masterUserService.getByOrgId(idOrganization);

        return Response
                .ok(user)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/organization/{idUser}")
    @ApiOperation(
            value = "Get Data Organization By User Id",
            response = MasterStrukturOrganisasiDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return User By Organization Id")
    public Response getOrgByUsrId(@PathParam("idUser") Long idUser) {
        MasterStrukturOrganisasiDto organisasi = masterStrukturOrganisasiService.getByUserId(idUser);

        return Response
                .ok(organisasi)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("globalFilter")
    @ApiOperation(
            value = "Get Data Filtered Para",
            response = ParaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Para list")
    public Response getFilteredData(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("") @QueryParam("start") String start,
            @DefaultValue("") @QueryParam("end") String end,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<ParaDto> result = paraService.getResultList(filter, sort, descending, skip, limit).stream().map(ParaDto::new).collect(Collectors.toList());
        long count = paraService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    private void getStarredContact(List<MasterStrukturOrganisasiDto> result) {
        result.forEach(q -> {
            boolean isFavorit = kontakFavoritService.getIsFavoritBy(q.getOrganizationId());
            q.setIsFavorite(isFavorit);
        });
    }

    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Upload Data",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Upload Data")
    public Response uploadData(@NotNull @FormDataParam("isPosition") boolean isPosition,
                               @FormDataParam("file") FormDataBodyPart body) {
        try {
            paraService.uploadData(isPosition,body);
            notificationService.sendInfo("Info", String.format("Data berhasil disimpan"));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal disimpan, %s",ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("template")
    @ApiOperation(
            value = "Download Template",
            notes = "Return Template File")
    public Response getDownloadFile() {
        String filename="Template-Grup-Para";
        String docId="7a44fcb6-d4ad-46b1-af65-c60a0724ffd2";
        StreamingOutput fileStream = new StreamingOutput() {
            @Override
            public void write(java.io.OutputStream output) throws IOException, WebApplicationException {
                try {
                    byte[] download = fileService.download(docId);
                    output.write(download);
                    output.flush();
                } catch (Exception e) {
                    throw new WebApplicationException("File Not Found !!");
                }
            }
        };
        return Response
                .ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
                .header("content-disposition", "attachment; filename = " + filename + ".xlsx")
                .build();
    }
}
