package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.Card;
import com.qtasnim.eoffice.db.CardChecklistDetail;
import com.qtasnim.eoffice.db.CardMember;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.CardService;
import com.qtasnim.eoffice.services.GanttService;
import com.qtasnim.eoffice.ws.dto.GanttGroup;
import com.qtasnim.eoffice.ws.dto.GanttItem;
import com.qtasnim.eoffice.ws.dto.GanttStatus;
import com.qtasnim.eoffice.ws.dto.GanttTask;
import me.xdrop.jrand.JRand;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Path("gantt")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class GanttResource {
    @Inject
    private GanttService ganttService;

    @Inject
    private CardService cardService;

    @GET
    public Response get() {
        return Response.ok("ok").status(Response.Status.OK).build();
    }

    @GET
    @Path("{boardId}")
    public Response get(@PathParam("boardId") Long boardId) {
        Calendar cal = Calendar.getInstance();
        List<GanttItem> items = new ArrayList<>();

        cardService.findByBoard(boardId).sortedBy(Card::getStart).toList().forEach(card -> {
            List<CardChecklistDetail> allCheckList = card.getCheckList().stream().flatMap(t -> t.getCheckListDetail().stream()).collect(Collectors.toList());
            long completed = allCheckList.stream().filter(CardChecklistDetail::getIsChecked).count();

            GanttTask task = new GanttTask();
            task.setId(card.getId());
            task.setName(card.getTitle());
            task.setNotes(card.getDescription());
            task.setCompletion(new Double((double) completed / (double) allCheckList.size() * 100).intValue());
            task.setResource(card.getMemberList().stream().findFirst().map(CardMember::getUser).orElse(null));
            task.setStart(Optional.ofNullable(card.getStart()).orElse(cal.getTime()));
            task.setEnd(Optional.ofNullable(card.getEnd()).orElse(cal.getTime()));

            boolean isLate = cal.getTime().after(task.getEnd());

            if (isLate) {
                task.setStatus(GanttStatus.LATE);
            } else {
                switch (task.getStatus()) {
                    case COMPLETED: {
                        task.setStatus(GanttStatus.COMPLETED);
                        break;
                    }
                    case NOTSTARTED: {
                        task.setStatus(GanttStatus.NOTSTARTED);
                        break;
                    }
                    case ONPROGRESS: {
                        task.setStatus(GanttStatus.ONPROGRESS);
                        break;
                    }
                }
            }

            items.add(task);
        });

        return Response
                .ok(ganttService.compile(items))
                .status(Response.Status.OK)
                .build();
    }

    private void generateDummyData(List<GanttItem> items) {
        Calendar cal = Calendar.getInstance();
        GanttGroup group = new GanttGroup();
        group.setId(1L);
        group.setOpen(true);
        group.setName(JRand.sentence().words(2, 5).gen());
        group.setNotes(JRand.sentence().words(2, 3).gen());
        items.add(group);

        final AtomicLong previous = new AtomicLong(-1);

        for (int i = 2; i <= 15; i++) {
            Integer duration = JRand.natural().range(1, 20).gen();

            GanttTask task = new GanttTask();
            task.setId((long) i);
            task.setName(JRand.sentence().words(2, 5).gen());
            task.setNotes(JRand.sentence().words(2, 3).gen());
            task.setParent(group);
            task.setStatus(GanttStatus.NOTSTARTED);
            task.setCompletion(JRand.natural().min(0).max(100).gen());
            task.setResource(new MasterUser(){
                {
                    setNameFront(JRand.firstname().gen());
                    setNameMiddleLast(JRand.lastname().gen());
                }
            });
            task.setStart(cal.getTime());

            if (previous.get() != -1) {
                task.setDepend(items.stream().filter(t -> t.getId().equals(previous.get())).findFirst().orElse(null));
            }

            cal.add(Calendar.DATE, duration);
            task.setEnd(cal.getTime());

            previous.set(task.getId());

            items.add(task);
        }
    }
}
