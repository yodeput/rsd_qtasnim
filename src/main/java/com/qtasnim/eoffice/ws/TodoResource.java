package com.qtasnim.eoffice.ws;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.TaskEventHandler;
import com.qtasnim.eoffice.workflows.WorkflowException;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.ExclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.FlowElement;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaQuery;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

@Path("todo")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "User Task Resource",
        description = "WS Endpoint untuk menghandle operasi tugas pengguna"
)
public class TodoResource {
    @Inject
    private UserTaskService userTaskService;
    @Inject
    private SuratService suratService;
    @Inject
    private PermohonanDokumenService permohonanDokumenService;
    @Inject
    private NgSuratService ngSuratService;
    @Inject
    private DistribusiDokumenService distribusiDokumenService;
    @Inject
    private NotificationService notificationService;
    @Inject
    private ProcessTaskService processTaskService;
    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;
    @Inject
    private PenomoranManualService penomoranManualService;
    @Inject
    private Logger logger;

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = UserTaskDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Todo list")
    public Response getFilteredData(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("") @QueryParam("jenisDokumen") String jenisDokumen,
            @QueryParam("sort") String sort,
            @DefaultValue("true") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        CriteriaQuery<UserTask> rawQuery = userTaskService.getAllQuery(filter, sort, descending, jenisDokumen);
        List<UserTaskDto> result = userTaskService.execCriteriaQuery(rawQuery, skip, limit)
                .stream().map(UserTaskDto::new)
                .collect(Collectors.toList());
        long count = userTaskService.countCriteriaQuery(rawQuery);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("get-related-entity")
    @ApiOperation(
            value = "Get Related Entity Data",
            response = Object.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Related Entity")
    public Response getFilteredData(
            @DefaultValue("0") @QueryParam("id") Long id,
            @DefaultValue("") @QueryParam("relatedEntity") String relatedEntity) throws InternalServerErrorException {

        switch (relatedEntity) {
            case "com.qtasnim.eoffice.db.Surat": {
                Surat surat = suratService.find(id);

                return Response.ok(new SuratDto(surat)).build();
            }
            case "com.qtasnim.eoffice.db.PermohonanDokumen": {
                PermohonanDokumen layanan = permohonanDokumenService.find(id);

                return Response.ok(new PermohonanDokumenDto(layanan)).build();
            }
            case "com.qtasnim.eoffice.db.DistribusiDokumen": {
                DistribusiDokumen distribusi = distribusiDokumenService.find(id);

                return Response.ok(new DistribusiDokumenDto(distribusi)).build();
            }
            case "com.qtasnim.eoffice.db.PenomoranManual": {
                PenomoranManual manual = penomoranManualService.find(id);

                return Response.ok(new PenomoranManualDto(manual)).build();
            }
        }

        return Response.ok(null).build();
    }

    @POST
    @Path("task-actions/submit")
    @ApiOperation(
            value = "Task Action Submit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task history")
    public Response taskAction(
            TaskActionDto dto) {
        try {
            try {
                ProcessTask processTask = processTaskService.getTaskId(dto.getTaskId());

                if (processTask == null) {
                    return Response
                            .ok(new MessageDto("error", "Invalid recordRefId"))
                            .build();
                }

                TaskActionResponseDto validTask = generateTaskActions(processTask);

                //INVALID FIELD NAME
                List<String> invalidFieldNames = dto.getForms().stream()
                        .filter(t -> validTask.getForms().stream().noneMatch(f -> f.getName().equals(t.getName())))
                        .map(FormFieldDto::getName)
                        .collect(Collectors.toList());

                if (invalidFieldNames.size() > 0) {
                    throw new WorkflowException(String.format("Error when submit task actions: Invalid field name(s): %s", String.join(", ", invalidFieldNames)), "QT-ERR-CMDA-11");
                }

                //INVALID REQUIRED FIELD
                List<String> invalidRequiredFields = validTask.getForms().stream()
                        .filter(t -> t.getRequiredFn().test(dto) && dto.getForms().stream().noneMatch(f -> f.getName().equals(t.getName()) && StringUtils.isNotEmpty(f.getValue())))
                        .map(t -> String.format("%s: %s", t.getName(), "'Should not be null or empty'"))
                        .collect(Collectors.toList());

                if (invalidRequiredFields.size() > 0) {
                    throw new WorkflowException(String.format("Error when submit task actions: Invalid field value: %s", String.join(", ", invalidFieldNames)), "QT-ERR-CMDA-11");
                }

                Map<String, Object> userResponse = dto.getForms()
                        .stream()
                        .filter(t -> validTask.getForms().stream().anyMatch(v -> v.getPostToCamunda() && v.getName().equals(t.getName())))
                        .collect(HashMap::new, (m, v)->m.put(v.getName(), (Object)v.getValue()), HashMap::putAll);
                MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
                IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();

                IWorkflowEntity entityObject = new PermohonanDokumen();
                switch (processTask.getProcessInstance().getRelatedEntity()) {
                    case "com.qtasnim.eoffice.db.PermohonanDokumen": {
                        entityObject = permohonanDokumenService.getEntity(processTask);
                        break;
                    }
                    case "com.qtasnim.eoffice.db.DistribusiDokumen": {
                        entityObject = distribusiDokumenService.getEntity(processTask);
                        break;
                    }
                }

                final IWorkflowEntity entity = entityObject;
                TaskEventHandler taskEventHandler = new TaskEventHandler();
                taskEventHandler
                        .onCompleted((processInstance, status) -> {
                            switch (entity.getClassName()) {
                                case "com.qtasnim.eoffice.db.PermohonanDokumen": {
                                    permohonanDokumenService.onWorkflowCompleted(status, processInstance, entity, workflowProvider, null, null);
                                    break;
                                }
                                case "com.qtasnim.eoffice.db.DistribusiDokumen": {
                                    distribusiDokumenService.onWorkflowCompleted(status, processInstance, entity, workflowProvider, null, null);
                                    break;
                                }
                            }
                        })
                        .onTaskExecuted((task, userRsp) -> userTaskService.saveOrEdit(task))
                        .onTasksCreated((prevTask,processTasks) -> {
                            switch (entity.getClassName()) {
                                case "com.qtasnim.eoffice.db.DistribusiDokumen": {
                                    distribusiDokumenService.onTasksCreated(entity, prevTask, processTasks);
                                    break;
                                }
                            }
                        });

                workflowProvider.execute(processTask, entity, userResponse, taskEventHandler);

                //#region SEND MAIL NOTIFICATION
                // SEND MAIL TO KONSEPTOR
                switch (entity.getClassName()) {
                    case "com.qtasnim.eoffice.db.PermohonanDokumen": {
                        permohonanDokumenService.infoTaskNotification(processTask, entity, null);
                        break;
                    }
                    case "com.qtasnim.eoffice.db.DistribusiDokumen": {
                        distribusiDokumenService.infoTaskNotification(processTask, entity, null);
                        break;
                    }
                }

                // SEND MAIL TO NEXT APPROVER
                ProcessInstance processInstance = processTask.getProcessInstance();
                List<ProcessTask> activeTasks = workflowProvider.getActiveTasks(processInstance);

                for (ProcessTask t : activeTasks) {
                    switch (entity.getClassName()) {
                        case "com.qtasnim.eoffice.db.PermohonanDokumen": {
                            permohonanDokumenService.newTaskNotification(t, entity, null);
                            break;
                        }
                        case "com.qtasnim.eoffice.db.DistribusiDokumen": {
                            distribusiDokumenService.newTaskNotification(t, entity, null);
                            break;
                        }
                    }
                }
                //#endregion

                notificationService.sendInfo("Info", "Persetujuan Layanan berhasil dikirim");

                return Response
                        .ok()
                        .build();
            } catch (Exception e) {
                throw new WorkflowException("Error when submit task actions", "QT-ERR-CMDA-11", e);
            }
        } catch (Exception ex) {
            String errorMessage = Optional.ofNullable(ex.getCause()).map(Throwable::getMessage).orElse("Unknown Error");
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Tugas gagal dikirim: %s", errorMessage));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private TaskActionResponseDto generateTaskActions(ProcessTask processTask) throws IOException {
        TaskActionResponseDto response = new TaskActionResponseDto();
        ProcessInstance processInstance = Optional.ofNullable(processTask).map(ProcessTask::getProcessInstance).orElse(null);
        BpmnModelInstance modelInstance;

        try (InputStream inputStream = new ByteArrayInputStream(processInstance.getProcessDefinition().getDefinition().getBytes(Charset.forName("UTF-8")))) {
            modelInstance = Bpmn.readModelFromStream(inputStream);
        }

        response.setTaskName(processTask.getTaskName());
        response.setActionType(processTask.getTaskType());
        response.setTaskId(processTask.getTaskId());
        response.setAssigneeId(Optional.ofNullable(processTask.getAssignee()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null));

        List<String> approvalActions = modelInstance
                .getModelElementsByType(modelInstance.getModel().getType(org.camunda.bpm.model.bpmn.instance.UserTask.class))
                .stream()
                .filter(t -> ((org.camunda.bpm.model.bpmn.instance.UserTask) t).getId().equals(processTask.getTaskName()))
                .flatMap(t -> ((org.camunda.bpm.model.bpmn.instance.UserTask) t).getOutgoing().stream().map(SequenceFlow::getTarget))
                .filter(t -> t instanceof ExclusiveGateway)
                .flatMap(t -> t.getOutgoing().stream())
                .map(FlowElement::getName)
                .sorted()
                .collect(Collectors.toList());
        FormFieldDto approvalForm = new FormFieldDto();
        approvalForm.setName(processTask.getResponseVar());
        approvalForm.setLabel("Approval");
        approvalForm.setType("select");
        approvalForm.setOtherData(getObjectMapper().writeValueAsString(new OptionsDto(approvalActions)));
        approvalForm.setRequired(true);

        response.getForms().add(approvalForm);

        FormFieldDto commentForm = new FormFieldDto();
        commentForm.setName("Comment");
        commentForm.setLabel("Comment");
        commentForm.setType("input");
        commentForm.setInputType("text");
        commentForm.setRequired(null);
        commentForm.setRequiredFn(taskActionDto -> taskActionDto.getForms().stream().filter(t -> t.getName().equals(approvalForm.getName())).findFirst().map(t -> !t.getValue().equals("Setujui")).orElse(false));
        commentForm.setRequiredEl(String.format("forms[.name == '%s'].value != 'Setujui'", approvalForm.getName()));

        response.getForms().add(commentForm);
        response.setActionLabel(String.format("el: forms[.name == '%s'].value", approvalForm.getName()));

        return response;
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
    
    @GET
    @Path("needAttention/count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response needAttentionCount(@QueryParam("idOrganization") Long idOrganization) {
        try {
            Long count = userTaskService.tugasNeedAttCount(idOrganization);
            
            return Response
                    .ok(count)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}
