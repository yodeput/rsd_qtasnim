package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.MasterModul;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterModulService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.FieldErrorDto;
import com.qtasnim.eoffice.ws.dto.MasterModulDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;

@Path("master-modul")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Modul Resource",
        description = "WS Endpoint untuk menghandle operasi Modul"
)
public class MasterModulResource {

    @Inject
    private MasterModulService masterModulService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterModulDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Modul list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterModul> all = masterModulService.getAll();
        List<MasterModulDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterModul::getIdModul))
                .map(MasterModulDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterModulDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Modul list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterModul> data = masterModulService.getResultList(filter, sort, descending, skip, 0);
        long count = masterModulService.count(filter);

        List<MasterModulDto> result = data.stream().map(q -> new MasterModulDto(q)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save a new Modul")
    public Response saveData(@Valid MasterModulDto dao) {

        List<MasterModul> list = masterModulService.findAll().stream().filter(q->q.getCompanyCode().getId().equals(dao.getCompanyId()))
                .collect(Collectors.toList());
        String hasil = "";
        if(!list.isEmpty()) {
            hasil = this.validate(null, dao.getNamaModul(), dao.getParent(), list);
        }

        if (!StringUtils.isEmpty(hasil)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {

            try {
                masterModulService.saveOrEdit(dao, null);
                notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNamaModul()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil disimpan"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNamaModul(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit an Modul")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterModulDto dao) {

        List<MasterModul> list = masterModulService.findAll().stream().filter(q->q.getCompanyCode().getId().equals(dao.getCompanyId()))
                .collect(Collectors.toList());
        String hasil ="";
        if(!list.isEmpty()) {
            hasil = this.validate(id, dao.getNamaModul(), dao.getParent(), list);
        }

        if (!StringUtils.isEmpty(hasil)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {

            try {
                masterModulService.saveOrEdit(dao, id);
                notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNamaModul()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil diubah"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNamaModul(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete an Modul")
    public Response deleteData(@Valid @PathParam("id") Long id) {
        try {
            MasterModul model = masterModulService.find(id);
            masterModulService.remove(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getNamaModul()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("/parent")
    @ApiOperation(
            value = "Get Data By Parent",
            response = MasterModulDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Modul Data By Parent")
    public Response getDataByParent(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @QueryParam("idParent") Long idParent,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {
        JPAJinqStream<MasterModul> all = masterModulService.getByParent(idParent, sort, descending);
        List<MasterModulDto> result = all
                .skip(skip)
                .limit(limit)
                .map(MasterModulDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    private String validate(Long id, String nama, MasterModulDto parent, List<MasterModul> list) {
        List<MasterModul> dataList;
        
        if (parent == null) {
            dataList = list.stream().filter(t -> t.getParent() == null).collect(Collectors.toList());
        } else {
            dataList = list.stream().filter(t -> t.getParent() != null && Objects.equals(t.getParent().getIdModul(), parent.getIdModul())).collect(Collectors.toList());
        }
        
        if (id != null) {
            dataList = dataList.stream().filter(q -> !Objects.equals(q.getIdModul(), id)).collect(Collectors.toList());
        }
        
        if (dataList != null) {
            boolean notValid = dataList.stream().anyMatch(t -> nama.equals(t.getNamaModul()));
            return notValid ? "nama" : "" ;
        }
        
        return "";
    }

    private MessageDto prepareMessage(String field) {
        String msg;
        switch (field) {
            case "nama":
                msg = "Nama";
                break;
            case "kode":
                msg = "Kode";
                break;
            default:
                msg = "Nama dan Kode";
                break;
        }
        MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();

        FieldErrorDto fieldErrorDto = new FieldErrorDto();
        fieldErrorDto.setField(field);
        fieldErrorDto.setMessage(msg + " Modul sudah di gunakan.");
        fieldErrorDtos.add(fieldErrorDto);

        messageDto.setFields(fieldErrorDtos);

        return messageDto;
    }
    
    @GET
    @Path("full")
    @ApiOperation(
            value = "Get Full Modules",
            response = MasterModulDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Modul list")
    public Response getFull() {

        JPAJinqStream<MasterModul> all = masterModulService.getAll();
        List<MasterModulDto> result = all
                .sorted(Comparator.comparing(MasterModul::getIdModul))
                .map(MasterModulDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}
