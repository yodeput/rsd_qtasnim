package com.qtasnim.eoffice.ws;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.ClassUtil;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.workflows.IWorkflowProvider;
import com.qtasnim.eoffice.workflows.IWorkflowService;
import com.qtasnim.eoffice.workflows.TaskEventHandler;
import com.qtasnim.eoffice.workflows.WorkflowException;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.camunda.bpm.model.bpmn.instance.*;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaFormData;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaFormField;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("workflow")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Workflow Resource",
        description = "WS Endpoint untuk menghandle operasi Workflow"
)
@Stateless
@LocalBean
public class WorkflowResource implements Serializable {
    public static final String DEFAULT_FORM_NAME = "Disposisi";

    @Inject
    private ProcessDefinitionService processDefinitionService;

    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private ProcessDefinitionFormVarResolverService processDefinitionFormVarResolverService;

    @Inject
    private ProcessTaskService processTaskService;

    @Inject
    private MasterJenisDokumenService jenisDokumenService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @Inject
    @ISessionContext
    private Session user;

    @GET
    @Path("history")
    @ApiOperation(
            value = "Get History",
            response = ProcessDefinitionDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of workflow history")
    public Response get(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @QueryParam("limit") int limit,
            @QueryParam("idJenisDokumen") Long idJenisDokumen) {
        ProcessDefinition activated;
        JPAJinqStream<ProcessDefinition> all;

        if (idJenisDokumen != null) {
            activated = processDefinitionService.getActivated(idJenisDokumen);
            all = processDefinitionService.getHistory(idJenisDokumen);
        } else {
            activated = processDefinitionService.getActivated(DEFAULT_FORM_NAME);
            all = processDefinitionService.getHistory(DEFAULT_FORM_NAME);
        }

        Long count = all.count();
        List<ProcessDefinitionDto> result = all
                .skip(skip)
                .limit(limit)
                .map(t -> {
                    ProcessDefinitionDto dto = new ProcessDefinitionDto(t);
                    dto.setActivated(activated != null && t.getId().equals(activated.getId()));

                    return dto;
                })
                .collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @ApiOperation(
            value = "Get Active Workflow",
            response = ProcessDefinitionDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return a workflow definition")
    public Response get(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @QueryParam("version") String version
    ) {
        ProcessDefinition activated;

        if (version != null) {
            if (idJenisDokumen != null) {
                activated = processDefinitionService.getByVersion(idJenisDokumen, Double.valueOf(version));
            } else {
                activated = processDefinitionService.getByVersion(DEFAULT_FORM_NAME, Double.valueOf(version));
            }

            if (activated == null) {
                return Response
                        .ok(new MessageDto("warning", "Workflow tidak ditemukan"))
                        .build();
            }
        } else {
            if (idJenisDokumen != null) {
                activated = processDefinitionService.getActivated(idJenisDokumen);
            } else {
                activated = processDefinitionService.getActivated(DEFAULT_FORM_NAME);
            }

            if (activated == null) {
                return Response
                        .ok(new MessageDto("warning", "Workflow tidak ditemukan. silahkan aktifkan terlebih dahulu"))
                        .build();
            }
        }

        List<OptionDto> taskResolver = applicationContext.getTaskResolvers().stream().map(OptionDto::new).collect(Collectors.toList());

        ProcessDefinitionDto dto = new ProcessDefinitionDto(activated);
        dto.getVarResolvers().forEach(t -> t.setOptions(taskResolver));

        return Response.ok(dto).build();
    }

    @GET
    @Path("get")
    @ApiOperation(
            value = "Get Active Workflow By Form Name",
            response = ProcessDefinitionDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return a workflow definition")
    public Response getBy(
            @QueryParam("formName") String formName,
            @QueryParam("version") String version
    ) {
        ProcessDefinition activated;

        if (version != null) {
            activated = processDefinitionService.getByVersion(formName, Double.valueOf(version));

            if (activated == null) {
                return Response
                        .ok(new MessageDto("warning", "Workflow tidak ditemukan"))
                        .build();
            }
        } else {

            if (Strings.isNullOrEmpty(formName)) {
                return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Form Name tidak boleh kosong")
                    .build();
            }

            activated = processDefinitionService.getActivated(formName);

            if (activated == null) {
                return Response
                        .ok(new MessageDto("warning", "Workflow tidak ditemukan. silahkan aktifkan terlebih dahulu"))
                        .build();
            }
        }

        List<OptionDto> taskResolver = applicationContext.getTaskResolvers().stream().map(OptionDto::new).collect(Collectors.toList());

        ProcessDefinitionDto dto = new ProcessDefinitionDto(activated);
        dto.getVarResolvers().forEach(t -> t.setOptions(taskResolver));

        return Response.ok(dto).build();
    }

    @GET
    @Path("get-latest")
    public Response getLatestBy(
            @QueryParam("formName") String formName
    ) {
        ProcessDefinition activated;

        if (Strings.isNullOrEmpty(formName)) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Form Name tidak boleh kosong")
                    .build();
        } 

        activated = processDefinitionService.getLastest(formName);

        if (activated == null) {
            return Response
                    .ok(new MessageDto("warning", "Workflow tidak ditemukan. silahkan simpan/aktifkan terlebih dahulu"))
                    .build();
        }

        List<OptionDto> taskResolver = applicationContext.getTaskResolvers().stream().map(OptionDto::new).collect(Collectors.toList());

        ProcessDefinitionDto dto = new ProcessDefinitionDto(activated);
        dto.getVarResolvers().forEach(t -> t.setOptions(taskResolver));

        return Response.ok(dto).build();
    }

    @GET
    @Path("latest")
    public Response getLatest(
            @QueryParam("idJenisDokumen") Long idJenisDokumen
    ) {
        ProcessDefinition activated;

        if (idJenisDokumen != null) {
            activated = processDefinitionService.getLastest(idJenisDokumen);
        } else {
            activated = processDefinitionService.getLastest(DEFAULT_FORM_NAME);
        }

        if (activated == null) {
            return Response
                    .ok(new MessageDto("warning", "Workflow tidak ditemukan. silahkan simpan/aktifkan terlebih dahulu"))
                    .build();
        }

        List<OptionDto> taskResolver = applicationContext.getTaskResolvers().stream().map(OptionDto::new).collect(Collectors.toList());

        ProcessDefinitionDto dto = new ProcessDefinitionDto(activated);
        dto.getVarResolvers().forEach(t -> t.setOptions(taskResolver));

        return Response.ok(dto).build();
    }

    @GET
    @Path("diagram")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(
            value = "Get BPMN",
            response = ProcessDefinitionDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return a workflow bpmn")
    public Response getDiagram(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @QueryParam("version") String version
    ) {
        ProcessDefinition activated;

        if (version != null) {
            if (idJenisDokumen != null) {
                activated = processDefinitionService.getByVersion(idJenisDokumen, Double.valueOf(version));
            } else {
                activated = processDefinitionService.getByVersion(DEFAULT_FORM_NAME, Double.valueOf(version));
            }
        } else {
            if (idJenisDokumen != null) {
                activated = processDefinitionService.getActivated(idJenisDokumen);
            } else {
                activated = processDefinitionService.getActivated(DEFAULT_FORM_NAME);
            }
        }

        if (activated == null) {
            return Response
                    .ok(new MessageDto("warning", "Workflow tidak ditemukan"))
                    .build();
        }

        return Response
                .ok(activated.getDefinition().getBytes())
                .header("Content-Disposition", "attachment; filename=\"" + activated.getDefinitionName() + ".bpmn\"")
                .build();
    }

    @POST
    @Path("parse-diagram")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Parse Diagram",
            response = ProcessDefinitionDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return workflow definition from a bpmn")
    public Response parseDiagram(
            @FormDataParam("file") InputStream file
    ) throws InternalServerErrorException {
        try {
            try {
                String diagramString = IOUtils.toString(file, StandardCharsets.UTF_8);
                List<CamundaFormField> startEventFormFields;

                try (InputStream inputStream = new ByteArrayInputStream(diagramString.getBytes(Charset.forName("UTF-8")))) {
                    BpmnModelInstance modelInstance = Bpmn.readModelFromStream(inputStream);

                    Bpmn.validateModel(modelInstance);

                    startEventFormFields = modelInstance
                            .getModelElementsByType(modelInstance.getModel().getType(ExtensionElements.class))
                            .stream()
                            .filter(t -> t.getParentElement() instanceof StartEvent)
                            .flatMap(t -> t.getChildElementsByType(modelInstance.getModel().getType(CamundaFormData.class)).stream())
                            .flatMap(t -> t.getChildElementsByType(modelInstance.getModel().getType(CamundaFormField.class)).stream())
                            .map(t -> (CamundaFormField) t)
                            .collect(Collectors.toList());
                }

                List<OptionDto> taskResolver = applicationContext.getTaskResolvers().stream().map(OptionDto::new).collect(Collectors.toList());

                ProcessDefinitionDto dto = new ProcessDefinitionDto();
                dto.setDefinition(diagramString);
                dto.setVarResolvers(startEventFormFields.stream().map(t -> new ProcessDefinitionFormVarResolverDto() {
                    {
                        setName(t.getCamundaId());
                        setOptions(taskResolver);
                    }
                }).collect(Collectors.toList()));

                return Response
                        .ok(dto)
                        .build();
            } catch (IOException ex) {
                throw new InternalServerErrorException("Invalid I/O operation", "QT-ERR-GRL-01", ex);
            }
        } catch (Exception ex) {
            logger.error(null, ex);
            String errorMessage = Optional.ofNullable(ex.getCause()).map(Throwable::getMessage).orElse("Unknown Error");
            notificationService.sendError("Error", String.format("Gagal membaca Diagram: %s", errorMessage));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("save")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Save Workflow",
            response = ProcessDefinitionDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return workflow definition")
    public Response save(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @FormDataParam("data") ProcessDefinitionDto dto,
            @FormDataParam("file") InputStream file,
            @DefaultValue("") @QueryParam("formName") String formName
    ) throws InternalServerErrorException {
        try {
            ProcessDefinition activated;
            MasterJenisDokumen masterJenisDokumen = null;

            if (idJenisDokumen != null) {
                activated = processDefinitionService.getLastest(idJenisDokumen);
                masterJenisDokumen = jenisDokumenService.find(idJenisDokumen);
            } else {
                if (!Strings.isNullOrEmpty(formName)) {
                    activated = processDefinitionService.getLastest(formName);
                } else {
                    activated = processDefinitionService.getLastest(DEFAULT_FORM_NAME);
                }
            }

            double nextVersion = 0.1;

            if (activated != null) {
                String versionString = activated.getVersion().toString();
                Integer leftPart = Integer.valueOf(versionString.substring(0, versionString.lastIndexOf(".")));
                Integer rightPart = Integer.valueOf(versionString.substring(versionString.lastIndexOf(".") + 1));

                rightPart++;

                nextVersion = Double.valueOf(String.format("%s.%s", leftPart, rightPart));
            }

            MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");

            ProcessDefinition processDefinition = new ProcessDefinition();
            processDefinition.setJenisDokumen(masterJenisDokumen);
            if (!Strings.isNullOrEmpty(formName)) {
                processDefinition.setFormName(formName);
            } else {
                processDefinition.setFormName(masterJenisDokumen.getNamaJenisDokumen());
            }
            processDefinition.setWorkflowProvider(masterWorkflowProvider);
            processDefinition.setVersion(nextVersion);
            List<ProcessDefinitionFormVarResolver> formVars = dto.getVarResolvers().stream().map(t -> {
                ProcessDefinitionFormVarResolver processDefinitionFormVarResolver = new ProcessDefinitionFormVarResolver();

                processDefinitionFormVarResolver.setVarName(t.getName());
                processDefinitionFormVarResolver.setValueResolver(t.getExpr());

                return processDefinitionFormVarResolver;
            }).collect(Collectors.toList());

            saveOrSubmit(processDefinition, formVars, file);

            notificationService.sendInfo("Info", "Workflow berhasil disimpan");

            return Response
                    .ok(new ProcessDefinitionDto(processDefinition))
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            String errorMessage = Optional.ofNullable(ex.getCause()).map(Throwable::getMessage).orElse("Unknown Error");
            notificationService.sendError("Error", String.format("Gagal membaca Diagram: %s", errorMessage));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("submit")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Submit/Publish Workflow",
            response = ProcessDefinitionDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return workflow definition")
    public Response submit(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @FormDataParam("data") ProcessDefinitionDto dto,
            @FormDataParam("file") InputStream file,
            @DefaultValue("") @QueryParam("formName") String formName
    ) throws InternalServerErrorException {
        try {
            ProcessDefinition activated;
            ProcessDefinition processDefinition = new ProcessDefinition();

            if (idJenisDokumen != null) {
                activated = processDefinitionService.getLastest(idJenisDokumen);
                MasterJenisDokumen masterJenisDokumen = jenisDokumenService.find(idJenisDokumen);
                processDefinition.setJenisDokumen(masterJenisDokumen);
                processDefinition.setFormName(masterJenisDokumen.getNamaJenisDokumen());
            } else {
                if (!Strings.isNullOrEmpty(formName)) {
                    activated = processDefinitionService.getLastest(formName);
                    processDefinition.setFormName(formName);
                } else {
                    activated = processDefinitionService.getLastest(DEFAULT_FORM_NAME);
                    processDefinition.setFormName(DEFAULT_FORM_NAME);
                }
                
            }

            double nextVersion = 1d;

            if (activated != null) {
                nextVersion = Math.floor(activated.getVersion()) + 1;
            }

            DateUtil dateUtil = new DateUtil();
            MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");

            processDefinition.setWorkflowProvider(masterWorkflowProvider);
            processDefinition.setVersion(nextVersion);
            processDefinition.setActivationDate(dateUtil.getDateFromISOString(dto.getActivationDate()));
            List<ProcessDefinitionFormVarResolver> formVars = dto.getVarResolvers().stream().map(t -> {
                ProcessDefinitionFormVarResolver processDefinitionFormVarResolver = new ProcessDefinitionFormVarResolver();

                processDefinitionFormVarResolver.setVarName(t.getName());
                processDefinitionFormVarResolver.setValueResolver(t.getExpr());

                return processDefinitionFormVarResolver;
            }).collect(Collectors.toList());

            saveOrSubmit(processDefinition, formVars, file);

            IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();
            workflowProvider.deploy(processDefinition);

            notificationService.sendInfo("Info", "Workflow berhasil diterbitkan");

            return Response
                    .ok(new ProcessDefinitionDto(processDefinition))
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            String errorMessage = Optional.ofNullable(ex.getCause()).map(Throwable::getMessage).orElse(ex.getMessage());
            notificationService.sendError("Error", String.format("Gagal membaca Diagram: %s", errorMessage));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("task-actions")
    @ApiOperation(
            value = "Get Available Task Actions",
            response = TaskActionResponseDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task action")
    public Response getTaskActions(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            TaskActionRequestDto requestDto) {
        try {
            List<ProcessTask> myTasks;

            if (idJenisDokumen != null) {
                myTasks = processTaskService.getMyTaskByRecordRefId(idJenisDokumen, requestDto.getRecordRefId(), user.getUserSession().getUser().getOrganizationEntity().getOrganizationCode());
            } else {
                myTasks = processTaskService.getMyTaskByRecordRefId(DEFAULT_FORM_NAME, requestDto.getRecordRefId(), user.getUserSession().getUser().getOrganizationEntity().getOrganizationCode());
            }

            ProcessTask processTask = myTasks.stream().findFirst().orElse(null);

            if (processTask == null) {
                return Response
                        .ok()
                        .build();
            }

            TaskActionResponseDto response = generateTaskActions(processTask);

            return Response
                    .ok(response)
                    .build();
        } catch (Exception e) {
            logger.error(null, e);
            throw new WorkflowException("Error when get task actions", "QT-ERR-CMDA-10", e);
        }
    }

    /**
     *
     * @param idJenisDokumen
     *      Related ID Jenis Dokumen
     * @param idsPathSegments
     *      Value must be semicolon delimeted
     * @param requestDto
     *      Request DTO
     * @return
     *      Task actions if any
     */
    @POST
    @Path("task-actions/by-jabatan/{pejabatIds}")
    @ApiOperation(
            value = "Get Available Task Actions",
            response = TaskActionResponseDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task action")
    public Response getTaskActionsByJabatan(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @PathParam("pejabatIds") String idsPathSegments,
            TaskActionRequestDto requestDto) {
        List<Long> ids;

        try {
            ids = Stream.of(idsPathSegments.split(",")).map(Long::parseLong).collect(Collectors.toList());
        } catch (Exception e) {
            return Response
                    .ok()
                    .build();
        }

        if (ids.size() == 0) {
            return Response
                    .ok()
                    .build();
        }

        try {
            List<ProcessTask> myTasks;

            if (idJenisDokumen != null) {
                myTasks = processTaskService.getMyTaskByRecordRefId(idJenisDokumen, requestDto.getRecordRefId(), ids);
            } else {
                myTasks = processTaskService.getMyTaskByRecordRefId(DEFAULT_FORM_NAME, requestDto.getRecordRefId(), ids);
            }

            ProcessTask processTask = myTasks.stream().findFirst().orElse(null);

            if (processTask == null) {
                return Response
                        .ok()
                        .build();
            }

            Long assigneeId = processTask.getAssignee().getIdOrganization();
            Long loggedInUserId = user.getUserSession().getUser().getOrganizationEntity().getIdOrganization();
            List<MasterDelegasi> delegasiAll = masterDelegasiService.getByPenggantiIdList(Collections.singletonList(user.getUserSession().getUser().getOrganizationEntity().getOrganizationCode()));
            Boolean isPelakharPymt = delegasiAll.stream().anyMatch(t -> t.getFrom().getIdOrganization().equals(assigneeId));
            Boolean isSecretary = !assigneeId.equals(loggedInUserId) && !isPelakharPymt;

            TaskActionResponseDto response = generateTaskActions(processTask, isSecretary);

            return Response
                    .ok(response)
                    .build();
        } catch (Exception e) {
            logger.error(null, e);
            throw new WorkflowException("Error when get task actions", "QT-ERR-CMDA-10", e);
        }
    }

    @POST
    @Path("task-actions-formname")
    @ApiOperation(
            value = "Get Available Task Actions",
            response = TaskActionResponseDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task action")
    public Response getTaskActionsByFormName(
            @QueryParam("formName") String formName,
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            TaskActionRequestDto requestDto) {
        try {
            List<ProcessTask> myTasks;

            if (idJenisDokumen != null) {
                myTasks = processTaskService.getMyTaskByRecordRefId(idJenisDokumen, requestDto.getRecordRefId(), user.getUserSession().getUser().getOrganizationEntity().getOrganizationCode());
            } else {
                myTasks = processTaskService.getMyTaskByRecordRefId(formName, requestDto.getRecordRefId(), user.getUserSession().getUser().getOrganizationEntity().getOrganizationCode());
            }

            ProcessTask processTask = myTasks.stream().findFirst().orElse(null);

            if (processTask == null) {
                return Response
                        .ok()
                        .build();
            }

            TaskActionResponseDto response = generateTaskActions(processTask);

            return Response
                    .ok(response)
                    .build();
        } catch (Exception e) {
            logger.error(null, e);
            throw new WorkflowException("Error when get task actions", "QT-ERR-CMDA-10", e);
        }
    }

    @GET
    @Path("task-action")
    @ApiOperation(
            value = "Get Available Task Actions",
            response = TaskActionResponseDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task action")
    public Response getTaskActionsByFormName(
            @QueryParam("taskId") String taskId) {
        try {

            ProcessTask processTask = processTaskService.getTaskId(taskId, user.getUserSession().getUser().getOrganizationEntity().getOrganizationCode());

            if (processTask == null) {
                return Response
                        .ok()
                        .build();
            }

            TaskActionResponseDto response = generateTaskActions(processTask);

            return Response
                    .ok(response)
                    .build();
        } catch (Exception e) {
            logger.error(null, e);
            throw new WorkflowException("Error when get task actions", "QT-ERR-CMDA-10", e);
        }
    }

    private TaskActionResponseDto generateTaskActions(ProcessTask processTask) throws IOException, InstantiationException, IllegalAccessException {
        return generateTaskActions(processTask, false);
    }

    private TaskActionResponseDto generateTaskActions(ProcessTask processTask, Boolean isSecretary) throws IOException, IllegalAccessException, InstantiationException {
        TaskActionResponseDto response = new TaskActionResponseDto();
        ProcessInstance processInstance = Optional.ofNullable(processTask).map(ProcessTask::getProcessInstance).orElse(null);
        BpmnModelInstance modelInstance;

        try (InputStream inputStream = new ByteArrayInputStream(processInstance.getProcessDefinition().getDefinition().getBytes(Charset.forName("UTF-8")))) {
            modelInstance = Bpmn.readModelFromStream(inputStream);
        }

        response.setIsSecretary(isSecretary);
        response.setTaskName(processTask.getTaskName());
        response.setActionType(processTask.getTaskType());
        response.setTaskId(processTask.getTaskId());
        response.setAssigneeId(Optional.ofNullable(processTask.getAssignee()).map(MasterStrukturOrganisasi::getIdOrganization).orElse(null));

        if (processTask.getTaskType().equals("Approval")) {
            List<String> approvalActions = modelInstance
                    .getModelElementsByType(modelInstance.getModel().getType(UserTask.class))
                    .stream()
                    .filter(t -> ((UserTask) t).getId().equals(processTask.getTaskName()))
                    .flatMap(t -> ((UserTask) t).getOutgoing().stream().map(SequenceFlow::getTarget))
                    .filter(t -> t instanceof ExclusiveGateway)
                    .flatMap(t -> t.getOutgoing().stream())
                    .map(FlowElement::getName)
                    .sorted()
                    .collect(Collectors.toList());
            FormFieldDto approvalForm = new FormFieldDto();
            approvalForm.setName(processTask.getResponseVar());
            approvalForm.setLabel("Approval");
            approvalForm.setType("select");
            approvalForm.setOtherData(getObjectMapper().writeValueAsString(new OptionsDto(approvalActions)));
            approvalForm.setRequired(true);

            response.getForms().add(approvalForm);

            FormFieldDto commentForm = new FormFieldDto();
            commentForm.setName("Comment");
            commentForm.setLabel("Comment");
            commentForm.setType("input");
            commentForm.setInputType("text");
            commentForm.setRequired(null);
            commentForm.setRequiredFn(taskActionDto -> taskActionDto.getForms().stream().filter(t -> t.getName().equals(approvalForm.getName())).findFirst().map(t -> !t.getValue().equals("Setujui")).orElse(false));
            commentForm.setRequiredEl(String.format("forms[.name == '%s'].value != 'Setujui'", approvalForm.getName()));

            response.getForms().add(commentForm);
            response.setActionLabel(String.format("el: forms[.name == '%s'].value", approvalForm.getName()));
        } else if (processTask.getTaskType().equals("Disposisi")) {
            FormFieldDto disposisiForm = new FormFieldDto();
            disposisiForm.setName(processTask.getResponseVar());
            disposisiForm.setLabel("Penerima Disposisi");
            disposisiForm.setType("disposisi");
            disposisiForm.setRequiredEl("true");

            response.getForms().add(disposisiForm);

            FormFieldDto tindakanForm = new FormFieldDto();
            tindakanForm.setName("Tindakan");
            tindakanForm.setLabel("Disposisi");
            tindakanForm.setType("master-tindakan");
            tindakanForm.setPostToCamunda(false);
            tindakanForm.setRequiredFn(taskActionDto -> taskActionDto.getForms().stream().filter(t -> t.getName().equals(disposisiForm.getName())).findFirst().map(t -> t.getValue() != null && !t.getValue().equals("[]")).orElse(false));
            tindakanForm.setRequiredEl(String.format("forms[.name == '%s'].value && forms[.name == '%s'].value != '[]'", disposisiForm.getName(), disposisiForm.getName()));

            response.getForms().add(tindakanForm);

            FormFieldDto commentForm = new FormFieldDto();
            commentForm.setName("Comment");
            commentForm.setLabel("Rincian Disposisi");
            commentForm.setType("text-area");
            commentForm.setRequiredFn(taskActionDto -> taskActionDto.getForms().stream().filter(t -> t.getName().equals(disposisiForm.getName())).findFirst().map(t -> t.getValue() != null && !t.getValue().equals("[]")).orElse(false));
            commentForm.setRequiredEl(String.format("forms[.name == '%s'].value && forms[.name == '%s'].value != '[]'", disposisiForm.getName(), disposisiForm.getName()));

            response.getForms().add(commentForm);

            response.setActionLabel("DOKUMEN.DISPOSISI");
        }

        IWorkflowService workflowService = ((IWorkflowEntity) ClassUtil.getClassByName(processTask.getProcessInstance().getRelatedEntity()).newInstance()).getWorkflowService();
        workflowService.onTaskActionGenerated(processTask, response);

        return response;
    }


    @POST
    @Path("task-actions/submit")
    @ApiOperation(
            value = "Task Action Submit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task history")
    @Transactional(Transactional.TxType.NOT_SUPPORTED)
    public Response taskAction(
            TaskActionDto dto) {
        try {
            try {
                ProcessTask processTask = processTaskService.getTaskId(dto.getTaskId());

                if (processTask == null) {
                    return Response
                            .ok(new MessageDto("error", "Invalid recordRefId"))
                            .build();
                }

                TaskActionResponseDto validTask = generateTaskActions(processTask, dto.getIsSecretary());

                //INVALID FIELD NAME
                List<String> invalidFieldNames = dto.getForms().stream()
                        .filter(t -> validTask.getForms().stream().noneMatch(f -> f.getName().equals(t.getName())))
                        .map(FormFieldDto::getName)
                        .collect(Collectors.toList());

                if (invalidFieldNames.size() > 0) {
                    throw new WorkflowException(String.format("Error when submit task actions: Invalid field name(s): %s", String.join(", ", invalidFieldNames)), "QT-ERR-CMDA-11");
                }

                //INVALID REQUIRED FIELD
                List<String> invalidRequiredFields = validTask.getForms().stream()
                        .filter(t -> t.getRequiredFn().test(dto) && dto.getForms().stream().noneMatch(f -> f.getName().equals(t.getName()) && f.getValue() != null))
                        .map(t -> String.format("%s: %s", t.getName(), "'Should not be null'"))
                        .collect(Collectors.toList());

                if (invalidRequiredFields.size() > 0) {
                    throw new WorkflowException(String.format("Error when submit task actions: Invalid field value: %s", String.join(", ", invalidFieldNames)), "QT-ERR-CMDA-11");
                }

                Map<String, Object> userResponse = dto.getForms()
                        .stream()
                        .filter(t -> validTask.getForms().stream().anyMatch(v -> v.getPostToCamunda() && v.getName().equals(t.getName())))
                        .collect(HashMap::new, (m, v)->m.put(v.getName(), (Object)v.getValue()), HashMap::putAll);
                MasterWorkflowProvider masterWorkflowProvider = masterWorkflowProviderService.getByProviderName("camunda");
                IWorkflowProvider workflowProvider = masterWorkflowProvider.getWorkflowProvider();
                IWorkflowService workflowService = ((IWorkflowEntity) ClassUtil.getClassByName(processTask.getProcessInstance().getRelatedEntity()).newInstance()).getWorkflowService();

                if (workflowService.isTaskInvalid(dto, processTask, userResponse)) {
                    return Response
                            .ok()
                            .build();
                }

                final IWorkflowEntity entity = workflowService.getEntity(processTask);
                TaskEventHandler taskEventHandler = new TaskEventHandler();
                taskEventHandler
                        .onCompleted((processInstance, status) -> workflowService.onWorkflowCompleted(status, processInstance, entity, workflowProvider, Optional.ofNullable(processTask.getAssignee().getUser()).map(MasterUser::getLoginUserName).orElse(null) , dto.getPassphrase()))
                        .onTaskExecuted((task, resps) -> workflowService.onTaskExecuted(dto, workflowProvider, entity, task, taskEventHandler, resps))
                        .onTasksCreated((prevTask, processTasks) -> workflowService.onTasksCreated(entity, prevTask, processTasks))
                        .onExecuting(workflowService::onTaskExecuting);

                workflowProvider.execute(processTask, entity, userResponse, taskEventHandler);

                return Response
                        .ok()
                        .build();
            } catch (Exception e) {
                throw new WorkflowException("Error when submit task actions", "QT-ERR-CMDA-11", e);
            }
        } catch (Exception ex) {
            Throwable realCause = ExceptionUtil.getRealCause(ex);
            String errorMessage = Optional.ofNullable(realCause).map(Throwable::getMessage).orElse("Unknown Error");
            logger.error(null, realCause);
            notificationService.sendError("Error", String.format("Tugas gagal dikirim: %s", errorMessage));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private Optional<Object> getValue(Map<String, Object> map, String key) {
        return map.entrySet().stream().filter(t -> t.getKey().equals(key)).findFirst().map(Map.Entry::getValue);
    }

    @POST
    @Path("task-history")
    @ApiOperation(
            value = "Get Task History",
            response = ProcessTaskDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list of task history")
    public Response getTaskHistory(
            @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @QueryParam("relatedEntity") String relatedEntity,
            TaskActionRequestDto requestDto) {
        try {
            List<ProcessTask> myTasks;
            List<ProcessTaskDto> result = new ArrayList<>();

            if (idJenisDokumen != null) {
                myTasks = processTaskService.getTaskHistoryByRecordRefId(idJenisDokumen, DEFAULT_FORM_NAME, requestDto.getRecordRefId());
            } else if (Strings.isNullOrEmpty(relatedEntity)){
                myTasks = processTaskService.getTaskHistoryByRecordRefId(DEFAULT_FORM_NAME, requestDto.getRecordRefId());
            } else {
                String formName = Objects.equals(relatedEntity, "com.qtasnim.eoffice.db.DistribusiDokumen") ? "Distribusi Dokumen" :
                        Objects.equals(relatedEntity, "com.qtasnim.eoffice.db.PermohonanDokumen") ? "Layanan Dokumen" :
                                Objects.equals(relatedEntity, "com.qtasnim.eoffice.db.PenomoranManual") ? "Penomoran Manual"
                                : DEFAULT_FORM_NAME;
                myTasks = processTaskService.getTaskHistoryByRecordRefId(formName, requestDto.getRecordRefId());
            }

            ProcessTask firstTask = myTasks.stream().findFirst().orElse(null);

            if (firstTask != null) {
                IWorkflowService workflowService = ((IWorkflowEntity) ClassUtil.getClassByName(firstTask.getProcessInstance().getRelatedEntity()).newInstance()).getWorkflowService();
                result = workflowService.onTaskHistoryRequested(myTasks);
            }

            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            throw new WorkflowException("Error when get task history", "QT-ERR-CMDA-08", e);
        }
    }

    private void saveOrSubmit(ProcessDefinition newWorkflowDefinition, List<ProcessDefinitionFormVarResolver> formVars, InputStream file) throws InternalServerErrorException {
        try {
            newWorkflowDefinition.setDefinition(IOUtils.toString(file, StandardCharsets.UTF_8));

            file.close();
        } catch (IOException ex) {
            throw new InternalServerErrorException("Invalid I/O operation", "QT-ERR-GRL-01", ex);
        }

        newWorkflowDefinition.setDefinitionType("bpmn 2.0");
        newWorkflowDefinition.setDefinitionName(UUID.randomUUID().toString());
        processDefinitionService.create(newWorkflowDefinition);

        formVars.forEach(t -> {
            t.setProcessDefinition(newWorkflowDefinition);

            processDefinitionFormVarResolverService.create(t);
        });
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
