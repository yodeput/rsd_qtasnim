package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.services.ProcessTaskService;
import com.qtasnim.eoffice.ws.dto.SuratDto;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("process-task")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProcessTaskResource {
    @Inject
    private ProcessTaskService processTaskService;

    @GET
    @Path("/surat")
    public Response filter(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false")
            @QueryParam("descending") boolean descending) {
        CriteriaQuery query = processTaskService
                .getCriteriaQuery(filter, sort, descending, (criteriaQuery, root) -> {
                    criteriaQuery.select(root.get("processInstance").get("surat")).distinct(true);
                });

        List<SuratDto> result = ((List<Surat>) processTaskService.getResultList(query, skip, limit))
                .stream()
                .map(SuratDto::new)
                .collect(Collectors.toList());

        long count = processTaskService.count(query);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}
