package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.*;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.*;
import java.util.stream.Collectors;

@Path("report")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Report Resource",
        description = "WS Endpoint untuk menghandle kebutuhan Report "
)
public class ReportResource {

    @Inject
    private SuratService suratService;

    @Inject
    private BerkasService berkasService;

    @Inject
    private ArsipService arsipService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private SuratInteractionService interactionService;

    @Inject
    private SuratDisposisiService disposisiService;

    @Inject
    private MasterDelegasiService delegasiService;

    @Inject
    private MasterNonStrukturalService nonStrukturalService;

    @Inject
    private PermohonanBonNomorService bonNomorService;

    @Inject
    private PermohonanDokumenService dokumenService;

    @Context
    private UriInfo uriInfo;

    @GET
    @Path("arsip/{status}")
    @ApiOperation(
            value = "Get Arsip by Status (aktif / inaktif",
            response = ArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip list")
    public Response getArsipAktif(
            @PathParam("status") String status,
            @DefaultValue("") @QueryParam("tglAwal") String startDate,
            @DefaultValue("") @QueryParam("tglAkhir") String endDate,
            @DefaultValue("") @QueryParam("namaBerkas") String namaBerkas,
            @DefaultValue("") @QueryParam("judulArsip") String judulArsip,
            @DefaultValue("") @QueryParam("noArsip") String noArsip,
            @DefaultValue("") @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @DefaultValue("") @QueryParam("idKArsip") Long idKArsip,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") Boolean descending) throws InternalServerErrorException {

//        JPAJinqStream<Arsip> all = arsipService.getArsipByStatus(status);
//        List<ArsipDto> result = all
//                .skip(skip)
//                .limit(limit)
//                .sorted(Comparator.comparing(Arsip::getId))
//                .map(ArsipDto::new).collect(Collectors.toList());
//
//        long count = all.count();
        HashMap<String, Object> temp = arsipService.getFilteredArsipByStatus(status, startDate, endDate, namaBerkas, judulArsip, noArsip, idJenisDokumen, idUnit, idKArsip, skip, limit, sort, descending);
        List<ReportArsipDto> result = (List<ReportArsipDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("arsip/pindahInaktif")
    @ApiOperation(
            value = "Get Arsip yang harus dipindah menjadi Inaktif",
            response = ArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip list")
    public Response getArsipAktifToInaktif(
            @DefaultValue("") @QueryParam("tglAwal") String startDate,
            @DefaultValue("") @QueryParam("tglAkhir") String endDate,
            @DefaultValue("") @QueryParam("namaBerkas") String namaBerkas,
            @DefaultValue("") @QueryParam("judulArsip") String judulArsip,
            @DefaultValue("") @QueryParam("noArsip") String noArsip,
            @DefaultValue("") @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @DefaultValue("") @QueryParam("idKArsip") Long idKArsip,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") Boolean descending) throws InternalServerErrorException {

//        JPAJinqStream<Arsip> all = arsipService.getArsipAktifToInaktif();
//        List<ArsipDto> result = all
//                .skip(skip)
//                .limit(limit)
//                .sorted(Comparator.comparing(Arsip::getId))
//                .map(ArsipDto::new).collect(Collectors.toList());
//
//        long count = all.count();
        HashMap<String, Object> temp = arsipService.getArsipSoonInaktif(startDate, endDate, namaBerkas, judulArsip, noArsip, idJenisDokumen, idUnit, idKArsip, skip, limit, sort, descending);
        List<ReportArsipDto> result = (List<ReportArsipDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("arsip/habisRetensi")
    @ApiOperation(
            value = "Get Arsip Habis Masa Retensi",
            response = ArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip list")
    public Response getArsipHabisRetensi(
            @DefaultValue("") @QueryParam("tglAwal") String startDate,
            @DefaultValue("") @QueryParam("tglAkhir") String endDate,
            @DefaultValue("") @QueryParam("namaBerkas") String namaBerkas,
            @DefaultValue("") @QueryParam("judulArsip") String judulArsip,
            @DefaultValue("") @QueryParam("noArsip") String noArsip,
            @DefaultValue("") @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @DefaultValue("") @QueryParam("idKArsip") Long idKArsip,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") Boolean descending) throws InternalServerErrorException {

//        JPAJinqStream<Arsip> all = arsipService.getArsipHabisRetensi();
//        List<ArsipDto> result = all
//                .skip(skip)
//                .limit(limit)
//                .sorted(Comparator.comparing(Arsip::getId))
//                .map(ArsipDto::new).collect(Collectors.toList());
//
//        long count = all.count();
        HashMap<String, Object> temp = arsipService.getArsipInaktifRetensi(startDate, endDate, namaBerkas, judulArsip, noArsip, idJenisDokumen, idUnit, idKArsip, skip, limit, sort, descending);
        List<ReportArsipDto> result = (List<ReportArsipDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("arsip/akanMusnah")
    @ApiOperation(
            value = "Get Arsip yang akan dimusnahkan",
            response = ArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip list")
    public Response getArsipAkanMusnah(
            @DefaultValue("") @QueryParam("tglAwal") String startDate,
            @DefaultValue("") @QueryParam("tglAkhir") String endDate,
            @DefaultValue("") @QueryParam("namaBerkas") String namaBerkas,
            @DefaultValue("") @QueryParam("judulArsip") String judulArsip,
            @DefaultValue("") @QueryParam("noArsip") String noArsip,
            @DefaultValue("") @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @DefaultValue("") @QueryParam("idKArsip") Long idKArsip,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") Boolean descending) throws InternalServerErrorException {

//        JPAJinqStream<Arsip> all = arsipService.getArsipAkanMusnah();
//        List<ArsipDto> result = all
//                .skip(skip)
//                .limit(limit)
//                .sorted(Comparator.comparing(Arsip::getId))
//                .map(ArsipDto::new).collect(Collectors.toList());
//
//        long count = all.count();
        HashMap<String, Object> temp = arsipService.getArsipInaktifMusnah(startDate, endDate, namaBerkas, judulArsip, noArsip, idJenisDokumen, idUnit, idKArsip, skip, limit, sort, descending);
        List<ReportArsipDto> result = (List<ReportArsipDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("surat/dokumenMasuk")
    @ApiOperation(
            value = "Get Surat Masuk by User Login",
            response = DokumenMasukDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Surat list")
    public Response getReportDokumenMasuk(
            @DefaultValue("") @QueryParam("startDate") String startDate,
            @DefaultValue("") @QueryParam("endDate") String endDate,
            @DefaultValue("") @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idArea") String idArea,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @DefaultValue("") @QueryParam("idKArsip") Long idKArsip,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") Boolean descending) throws InternalServerErrorException {

        HashMap<String, Object> temp = suratService.getRDocMasuk(startDate, endDate, idJenisDokumen, idArea, idUnit, idKArsip, sort, descending, skip, limit);
        List<ReportDokumenMasukDto> result = (List<ReportDokumenMasukDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("surat/dokumenKeluar")
    @ApiOperation(
            value = "Get Dokumen Keluar List",
            response = ReportDocKeluarDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Dokumen Keluar List")
    public Response dokumenKeluar(
            @DefaultValue("") @QueryParam("startDate") String start,
            @DefaultValue("") @QueryParam("endDate") String end,
            @DefaultValue("") @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idArea") String idArea,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @DefaultValue("") @QueryParam("idKArsip") Long idKArsip,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") Boolean descending) {

        HashMap<String, Object> temp = suratService.getRDocKeluar(start, end, idJenisDokumen, idArea, idUnit, idKArsip, sort, descending, skip, limit);
        List<ReportDocKeluarDto> result = (List<ReportDocKeluarDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("bon-nomor/konseptor")
    @ApiOperation(
            value = "Get Permohonan Bon Nomor",
            response = PermohonanBonNomorDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Permohonan Bon Nomor By Konseptor")
    public Response getBonNomor(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) throws InternalServerErrorException {

        JPAJinqStream<PermohonanBonNomor> all = bonNomorService.getByKonseptor();
        List<PermohonanBonNomorDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(PermohonanBonNomor::getModifiedDate).reversed())
                .map(PermohonanBonNomorDto::new).collect(Collectors.toList());

        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("bon-nomor/pemeriksa")
    @ApiOperation(
            value = "Get Permohonan Bon Nomor",
            response = PermohonanBonNomorDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Permohonan Bon Nomor By Pemeriksa")
    public Response getBonNomor2(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) throws InternalServerErrorException {

        JPAJinqStream<PermohonanBonNomor> all = bonNomorService.getByPemeriksa();
        List<PermohonanBonNomorDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(PermohonanBonNomor::getModifiedDate).reversed())
                .map(PermohonanBonNomorDto::new).collect(Collectors.toList());

        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("bon-nomor/penandatangan")
    @ApiOperation(
            value = "Get Permohonan Bon Nomor",
            response = PermohonanBonNomorDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Permohonan Bon Nomor By Penandatangan")
    public Response getBonNomor3(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) throws InternalServerErrorException {

        JPAJinqStream<PermohonanBonNomor> all = bonNomorService.getByPenandatangan();
        List<PermohonanBonNomorDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(PermohonanBonNomor::getModifiedDate).reversed())
                .map(PermohonanBonNomorDto::new).collect(Collectors.toList());

        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("permohonan-dokumen")
    @ApiOperation(
            value = "Get Permohonan Dokumen",
            response = PermohonanDokumenDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Permohonan Dokumen")
    public Response getPermohonanDokumen(
            @DefaultValue("") @QueryParam("tglPinjam") String tglPinjam,
            @DefaultValue("") @QueryParam("tglKembali") String tglKembali,
            @DefaultValue("") @QueryParam("idJenisDokumen") Long idJenisDokumen,
            @DefaultValue("") @QueryParam("idUnit") String idUnit,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") Boolean descending) throws InternalServerErrorException {

//        JPAJinqStream<PermohonanDokumen> all = dokumenService.getPeminjamanDokumen();
//        List<PermohonanDokumenDto> result = all
//                .skip(skip)
//                .limit(limit)
//                .sorted(Comparator.comparing(PermohonanDokumen::getId))
//                .map(PermohonanDokumenDto::new).collect(Collectors.toList());
//
//        long count = all.count();
        HashMap<String, Object> temp = dokumenService.getRPeminjamanDokumen(tglPinjam, tglKembali, idJenisDokumen, idUnit, sort, descending, skip, limit);
        List<PermohonanDokumenDto> result = (List<PermohonanDokumenDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

}
