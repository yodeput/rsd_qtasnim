package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.ImageRepository;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.ImageRepositoryService;
import org.apache.commons.compress.utils.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

@Path("image")
public class ImageUploadResource {

    @Inject
    private ImageRepositoryService imageRepositoryService;

    /**
     * The editor (eg CKEditor) invokes this to display an image.
     */
    @GET
    @Path("{name}")
    public Response getImage(
            @PathParam("name") String name,
            @QueryParam("size") String size
    ) throws IOException {
        ImageRepository imageRepository = imageRepositoryService.getByGeneratedName(name);
        ByteArrayInputStream bais = new ByteArrayInputStream(imageRepository.getImageSmall());
        return Response.ok(bais, MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment; filename=\"" + name ).build();
    }

    /**
     * CK3 specific image upload
     */
    @POST
    @Consumes("multipart/form-data")
    @Produces("text/html")
    @Path("ck")
    public Response ck5ImageUpload(
            @Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @FormDataParam("upload") InputStream imageInputStream,
            @FormDataParam("upload") FormDataContentDisposition imageFileDetail
    ) {

        try {
            String fileName = imageFileDetail.getFileName();
            ImageRepository uploaded = imageRepositoryService.upload(fileName, imageInputStream);
            String url = response.encodeURL("/api/image/" + uploaded.getGeneratedFileName());

            return Response.ok(String.format("{\"url\": \"%s\"}", url)).build();
        } catch (Exception e) {
            e.printStackTrace();

            return Response.ok("{ \"error\": { \"message\": \" " + Optional.ofNullable(e.getMessage()).orElse("Could not upload file") + " \" } }").build();
        }

    }

    /**
     * header sample
     * {
     * 	Content-Type=[image/png],
     * 	Content-Disposition=[form-data; name="file"; filename="filename.extension"]
     * }
     **/
    private String getFileName(String name) {

        String finalFileName = name.trim().replaceAll("\"", "");

        if (finalFileName.lastIndexOf(".")==-1) {
            return finalFileName;
        } else {
            return finalFileName.substring(0, finalFileName.lastIndexOf(".") );
        }
    }
}
