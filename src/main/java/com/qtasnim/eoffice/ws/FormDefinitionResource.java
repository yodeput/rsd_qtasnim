package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.FormDefinitionService;
import com.qtasnim.eoffice.services.FormFieldService;
import com.qtasnim.eoffice.services.MasterJenisDokumenService;
import com.qtasnim.eoffice.services.MasterMetadataService;
import com.qtasnim.eoffice.ws.dto.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("form-definition")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Pembuatan Form Definition",
        description = "WS Endpoint untuk menghandle Pembuatan Form Definition"
)
public class FormDefinitionResource {
    @Inject
    private FormDefinitionService formDefinitionService;

    @Inject
    private MasterJenisDokumenService jenisDokumenService;

    @Inject
    private MasterMetadataService masterMetaDataService;

    @Inject
    private FormFieldService formFieldService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Form Definitions",
            response = FormDefinition[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Area list")
    public Response getArea(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<FormDefinition> all = formDefinitionService.getAll();
        List<FormDefinitionDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(FormDefinition::getId))
                .map(FormDefinitionDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Data",
            response = FormDefinitionDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Form Definition list")
    public Response getCountries(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending)throws InternalServerErrorException {
        List<FormDefinitionDto> result = formDefinitionService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(FormDefinitionDto::new).collect(Collectors.toList());
        long count = formDefinitionService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("byJenisDokumen/{idJenisDokumen}")
    @ApiOperation(
            value = "Get Form Definition By idJenisDokumen",
            response = FormDefinitionDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Form Definition list by idJenisDokumen")
    public Response getByIdSurat(@PathParam("idJenisDokumen") Long idJenisDokumen){
        List<FormDefinition> found = formDefinitionService.getByIdJenisDokumen(idJenisDokumen);

        if(found != null) {
            List<FormDefinitionDto> result = found.stream()
                    .map(FormDefinitionDto::new).collect(Collectors.toList());

            for(FormDefinitionDto formDefinitionDto : result) {
                List<FormFieldDto> formFieldDtos = formDefinitionDto.getFields();

                for(FormFieldDto formFieldDto : formFieldDtos) {
                    String selectedFields = formFieldDto.getSelectedSourceField();

                    formFieldDto.setValueDataMaster(formFieldService.fillValueFromDataMaster(
                            new DataMasterValueRetrieveParam(
                                    formFieldDto.getTableSourceName(),
                                    selectedFields==null?null:Arrays.asList(selectedFields.split(",")),
                                    formFieldDto.getSelectedSourceValue())
                    ));
                }
            }

            long count = formDefinitionService.count();

            return Response
                    .ok(result)
                    .header("X-Total-Count", count)
                    .header("Access-Control-Expose-Headers", "X-Total-Count")
                    .build();
        } else {
            return Response.status(Response.Status.NO_CONTENT).build();
        }
    }

    @POST
    @Path("save")
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Template Dokumen")
    public Response save(@Valid FormDefinitionDto dto) {
        try {
            FormDefinition activated = formDefinitionService.getLatest(dto.getJenisDokumenId());
            double nextVersion = 0.1;

            if (activated != null) {
                String versionString = activated.getVersion().toString();
                Integer leftPart = Integer.valueOf(versionString.substring(0, versionString.lastIndexOf(".")));
                Integer rightPart = Integer.valueOf(versionString.substring(versionString.lastIndexOf(".") + 1));

                rightPart++;

                nextVersion = Double.valueOf(String.format("%s.%s", leftPart, rightPart));
            }

            nextVersion = 0;    // sementara versioning template diskip dulu. kalo mau di implements tinggal di uncomment
            FormDefinition formDefinition = formDefinitionService.saveOrSubmit(dto.getJenisDokumenId(), dto, dto.getFields(), nextVersion);

            return Response.ok(formDefinition).build();

        }  catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @POST
    @Path("saveas")
    @ApiOperation(
            value = "Save As New Template Dokumen",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save As New Template Dokumen")
    public Response saveAs(@Valid SaveAsNewFormDefinitionDto dto) {
        try {
            FormDefinition found = formDefinitionService.getLatest(dto.getSourceDocumentId());
            double nextVersion = 0.1;

            // if (found != null) {
            //     String versionString = found.getVersion().toString();
            //     Integer leftPart = Integer.valueOf(versionString.substring(0, versionString.lastIndexOf(".")));
            //     Integer rightPart = Integer.valueOf(versionString.substring(versionString.lastIndexOf(".") + 1));

            //     rightPart++;

            //     nextVersion = Double.valueOf(String.format("%s.%s", leftPart, rightPart));
            // }

            nextVersion = 0;    // sementara versioning template diskip dulu. kalo mau di implements tinggal di uncomment
            FormDefinition formDefinition = formDefinitionService.saveAsNewTemplate(found, dto.getNewDocumentId(), nextVersion);

            return Response.ok(formDefinition).build();

        }  catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @POST
    @Path("edit/{formDefinitionId}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Template Dokumen")
    public Response save(@PathParam("formDefinitionId") Long formDefinitionId, @Valid FormDefinitionDto dto) {
        try {
            FormDefinition activated = formDefinitionService.getLatest(dto.getJenisDokumenId());
            double nextVersion = 0.1;

            if (activated != null) {
                String versionString = activated.getVersion().toString();
                Integer leftPart = Integer.valueOf(versionString.substring(0, versionString.lastIndexOf(".")));
                Integer rightPart = Integer.valueOf(versionString.substring(versionString.lastIndexOf(".") + 1));

                rightPart++;

                nextVersion = Double.valueOf(String.format("%s.%s", leftPart, rightPart));
            }

            nextVersion = 0;    // sementara versioning template diskip dulu. kalo mau di implements tinggal di uncomment
            FormDefinition formDefinition = formDefinitionService.saveOrSubmit(dto.getJenisDokumenId(), dto, dto.getFields(), nextVersion);

            return Response.ok(formDefinition).build();

        }  catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
        }
    }

    // @POST
    // @Path("submit/{jenisDokumenId}")
    // public Response submit(@PathParam("jenisDokumenId") Long jenisDokumenId, @Valid FormDefinitionDto dto) throws InternalServerErrorException {
    //     try {
    //         FormDefinition activated = formDefinitionService.getLatest(jenisDokumenId);
    //         double nextVersion = 1d;

    //         if (activated != null) {
    //             nextVersion = Math.floor(activated.getVersion()) + 1;
    //         }

    //         nextVersion = 0;    // sementara versioning template diskip dulu. kalo mau di implements tinggal di uncomment
    //         FormDefinition formDefinition = formDefinitionService.saveOrSubmit(jenisDokumenId, dto, dto.getFields(), nextVersion);

    //         return Response.ok(formDefinition).build();

    //     }  catch (Exception e) {
    //         e.printStackTrace();
    //         return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
    //         // return Response.status(Response.Status.METHOD_NOT_ALLOWED).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
    //     }
    // }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Form Definition")
    public Response saveMetadata(@Valid FormDefinitionDto dao) {

        try{
            formDefinitionService.save(null,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Form Definition")
    public Response editMetadata(@NotNull @PathParam("id") Long id, @Valid FormDefinitionDto dao) {
        try{
            formDefinitionService.save(id,dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Form Definition")
    public Response deleteMetadata(@NotNull @PathParam("id") Long id) {
        try{
            formDefinitionService.delete(id);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}