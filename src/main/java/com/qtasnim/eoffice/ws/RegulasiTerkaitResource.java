package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.db.Regulasi;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.GlobalAttachmentDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import com.qtasnim.eoffice.ws.dto.RegulasiTerkaitDto;
import io.swagger.annotations.*;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("regulasi-terkait")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Regulasi Terkait Resource",
        description = "WS Endpoint untuk menghandle operasi Regulasi Terkait"
)
public class RegulasiTerkaitResource {
    @Inject
    private RegulasiTerkaitService terkaitService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @Inject
    private GlobalAttachmentService globalAttachmentService;
    
    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Data",
            response = RegulasiTerkaitDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Regulasi list")
    public Response getFilter(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<RegulasiTerkaitDto> result = terkaitService.getResultList(filter, sort, descending, skip, limit).stream().
                map(RegulasiTerkaitDto::new).collect(Collectors.toList());
        long count = terkaitService.count(filter);

        return Response
            .ok(result)
            .header("X-Total-Count", count)
            .header("Access-Control-Expose-Headers", "X-Total-Count")
            .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Regulasi Terkait")
    public Response saveRegulasi(@Valid RegulasiTerkaitDto dao) {
        try{
            terkaitService.save(null, dao);
            notificationService.sendInfo("Info","berhasil disimpan");

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNama(), ex.getMessage()));
            notificationService.sendError("Error", "gagal disimpan");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Regulasi Terkait")
    public Response editRegulasi(@NotNull @PathParam("id") Long id, @Valid RegulasiTerkaitDto dao) {

        try{
            terkaitService.save(id, dao);
            notificationService.sendInfo("Info", "Data berhasil diubah");

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", "Data berhasil diubah");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Regulasi Terkait")
    public Response deleteRegulasi(@NotNull @PathParam("id") Long id) {

        try{
            terkaitService.delete(id);
            notificationService.sendInfo("Info", "Data berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", "Data gagal dihapus");

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}