package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.ErrorReport;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.ErrorReportService;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.ErrorReportDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.stream.Collectors;

@Path("error-report")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Error Report Resource",
        description = "WS Endpoint untuk menghandle operasi ErrorReport"
)
public class ErrorReportResource {

    @Inject
    private ErrorReportService service;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = ErrorReportDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Error Report list")
    public Response getReport(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<ErrorReportDto> result = service.getResultList(filter, sort, descending, skip, limit).stream().
                map(ErrorReportDto::new).collect(Collectors.toList());
        long count = service.count(filter);

        service.setDocumentURLs(result);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Add Error Report")
    public Response saveReport(@Valid @FormDataParam("dto") ErrorReportDto dao,
            @FormDataParam("file") List<FormDataBodyPart> body) {
        try {
//            ErrorReport obj = service.save(null, dao);
//            service.uploadAttachment(obj.getId(), body);

            service.saveOrEdit(null, dao, body, null);
            notificationService.sendInfo("Info", String.format("Laporan berhasil dikirimkan"));
            return Response.ok().status(Response.Status.OK).build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Laporan gagal dikirimkan"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Edit",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Edit Error Report")
    public Response editReport(@NotNull @PathParam("id") Long id, @Valid @FormDataParam("dto") ErrorReportDto dao,
            @FormDataParam("file") List<FormDataBodyPart> body, @FormDataParam("deletedFilesId") List<String> deletedFilesId) {

        try {
//            service.save(id, dao);
//            service.uploadAttachment(id, body);
//            service.deleteAttachment(id, deletedFilesId);

            service.saveOrEdit(id, dao, body, deletedFilesId);
            notificationService.sendInfo("Info", String.format("Laporan berhasil diubah"));
            return Response.ok().status(Response.Status.OK).build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Laporan gagal diubah"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Error Report")
    public Response deleteReport(@NotNull @PathParam("id") Long id) {

        try {
//            service.deleteAttachment(id);
            service.delete(id);
            notificationService.sendInfo("Info", String.format("Laporan berhasil dihapus"));
            return Response.ok().status(Response.Status.OK).build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Laporan gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
