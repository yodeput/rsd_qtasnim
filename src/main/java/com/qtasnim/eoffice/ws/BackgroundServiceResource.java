package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.context.ApplicationContext;
import com.qtasnim.eoffice.db.SyncLog;
import com.qtasnim.eoffice.services.IService;
import com.qtasnim.eoffice.services.SyncLogDetailService;
import com.qtasnim.eoffice.services.SyncLogService;
import com.qtasnim.eoffice.util.BeanUtil;
import com.qtasnim.eoffice.ws.dto.BackgroundServiceDto;
import com.qtasnim.eoffice.ws.dto.SyncLogDetailDto;
import com.qtasnim.eoffice.ws.dto.SyncLogDto;
import org.apache.commons.lang3.StringUtils;
import org.jinq.orm.stream.JinqStream;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Path("background-service")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BackgroundServiceResource {
    @Resource
    private ManagedExecutorService executor;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private SyncLogService syncLogService;

    @Inject
    private SyncLogDetailService syncLogDetailService;

    @Context
    private UriInfo uriInfo;

    @GET
    public Response get() throws InternalServerErrorException {
        List<BackgroundServiceDto> result = applicationContext.getServiceResolvers()
                .stream()
                .collect(Collectors.toMap(Class::getName, BeanUtil::getBean))
                .entrySet()
                .stream()
                .filter(t -> t.getValue() != null)
                .map(t -> new BackgroundServiceDto(t.getKey(), (IService) t.getValue(), syncLogService.findLatestLog(t.getKey())))
                .collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", result.size())
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("start/{id}")
    public Response start(
            @PathParam("id") String id
    ) throws InternalServerErrorException {
        if (StringUtils.isEmpty(id)) {
            throw new InternalServerErrorException("invalid service id");
        }

        IService iService = applicationContext.getServiceResolvers()
                .stream()
                .collect(Collectors.toMap(Class::getName, BeanUtil::getBean))
                .entrySet()
                .stream()
                .filter(t -> t.getKey().equals(id))
                .findFirst()
                .map(t -> (IService) t.getValue())
                .orElse(null);

        if (iService == null) {
            throw new InternalServerErrorException("invalid service id");
        }

        executor.submit(iService::run);

        return Response
                .ok()
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("service-history/{id}")
    public Response history(
            @PathParam("id") String serviceId,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit
    ) throws InternalServerErrorException {
        if (StringUtils.isEmpty(serviceId)) {
            throw new InternalServerErrorException("invalid service id");
        }

        JinqStream<SyncLog> all = syncLogService.findByType(serviceId);
        List<SyncLogDto> result = all
                .skip(skip)
                .limit(limit)
                .toList()
                .stream()
                .map(SyncLogDto::new)
                .collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("log/{id}")
    public Response log(
            @PathParam("id") Long syncLogId
    ) throws InternalServerErrorException {
        if (syncLogId == null) {
            throw new InternalServerErrorException("invalid service id");
        }

        List<SyncLogDetailDto> collect = syncLogDetailService
                .getLog(syncLogId)
                .stream()
                .map(SyncLogDetailDto::new)
                .collect(Collectors.toList());

        return Response
                .ok(collect)
                .build();
    }

    @GET
    @Path("log-by-type/{id}")
    public Response logByType(
            @PathParam("id") String serviceId
    ) throws InternalServerErrorException {
        if (StringUtils.isEmpty(serviceId)) {
            throw new InternalServerErrorException("invalid service id");
        }

        SyncLog latestLog = syncLogService.findLatestLog(serviceId);
        List<SyncLogDetailDto> collect = new ArrayList<>();

        if (latestLog != null) {
            collect = syncLogDetailService
                    .getLog(latestLog.getId())
                    .stream()
                    .map(SyncLogDetailDto::new)
                    .collect(Collectors.toList());
        }

        return Response
                .ok(collect)
                .build();
    }
}