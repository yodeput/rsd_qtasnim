package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.Agenda;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.Session;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.AgendaService;
import com.qtasnim.eoffice.ws.dto.AgendaDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("agenda")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Agenda Management",
        description = "WS Endpoint untuk menghandle operasi Agenda"
)
public class AgendaResource {

    @Inject
    private AgendaService agendaService;
    @Inject
    @ISessionContext
    private Session userSession;

    @GET
    @ApiOperation(
            value = "Get All Agenda",
            response = AgendaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Agenda list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        MasterUser user = userSession.getUserSession().getUser();
        Long idOrg = user.getOrganizationEntity().getIdOrganization();
        Long idCompany = user.getCompanyCode().getId();
        
        JPAJinqStream<Agenda> all = agendaService.getAll().where(q -> q.getOrganization().getIdOrganization().equals(idOrg) && q.getCompanyCode().getId().equals(idCompany));
        List<AgendaDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Agenda::getId))
                .map(AgendaDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = AgendaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Agenda list with Filter")
    public Response getFiltered(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

        MasterUser user = userSession.getUserSession().getUser();
        Long idOrg = user.getOrganizationEntity().getIdOrganization();
        Long idCompany = user.getCompanyCode().getId();

        String additional = "(organization.idOrganization=='"+idOrg+"' and companyCode.id=='"+idCompany+"')";
        List<AgendaDto> result = agendaService.getResultList(Strings.isNullOrEmpty(filter) ? additional : additional + filter, sort, descending, skip, limit).stream()
                .map(AgendaDto::new).collect(Collectors.toList());
        long count = agendaService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Agenda")
    public Response saveAgenda(@Valid AgendaDto dao) {
        try {
            agendaService.saveOrEdit(null, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Agenda")
    public Response editAgenda(@NotNull @PathParam("id") Long id, @Valid AgendaDto dao) {
        try {
            agendaService.saveOrEdit(id, dao);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Agenda")
    public Response deleteAgenda(@NotNull @PathParam("id") Long id) {
        try {
            Agenda obj = agendaService.find(id);
            agendaService.remove(obj);
            return Response.status(Response.Status.OK).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}
