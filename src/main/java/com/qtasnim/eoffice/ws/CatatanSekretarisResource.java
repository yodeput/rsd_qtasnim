package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.Berkas;
import com.qtasnim.eoffice.db.MasterKlasifikasiKeamanan;
import com.qtasnim.eoffice.db.SuratCatatanSekretaris;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.BerkasService;
import com.qtasnim.eoffice.services.Logger;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.services.SuratCatatanSekretarisService;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.BerkasDto;
import com.qtasnim.eoffice.ws.dto.MasterKlasifikasiKeamananDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import com.qtasnim.eoffice.ws.dto.SuratCatatanSekretarisDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Path("surat-catatan-sekretaris")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class CatatanSekretarisResource {
    @Inject
    private BerkasService berkasService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private SuratCatatanSekretarisService suratCatatanSekretarisService;

    @Inject
    private Logger logger;

    @Context
    private UriInfo uriInfo;

    @POST
    public Response saveData(@Valid SuratCatatanSekretarisDto dao){
        try{
            SuratCatatanSekretaris suratCatatanSekretaris = suratCatatanSekretarisService.saveOrEdit(null, dao);
            notificationService.sendInfo("Info", "Catatan berhasil ditambahkan");

            return Response
                    .ok(new SuratCatatanSekretarisDto(suratCatatanSekretaris))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save Catatan: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid SuratCatatanSekretarisDto dao) {
        try{
            SuratCatatanSekretaris suratCatatanSekretaris = suratCatatanSekretarisService.saveOrEdit(id, dao);
            notificationService.sendInfo("Info", "Catatan berhasil diubah");

            return Response
                    .ok(new SuratCatatanSekretarisDto(suratCatatanSekretaris))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit Catatan: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try{
            SuratCatatanSekretaris model = suratCatatanSekretarisService.find(id);
            suratCatatanSekretarisService.remove(model);
            notificationService.sendInfo("Info", "Catatan berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Catatan berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Catatan gagal dihapus: %s", ex.getMessage()));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}