package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterAplikasiPenciptaArsip;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterAplikasiPenciptaArsipService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterAplikasiPenciptaArsipDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-app-pencipta-arsip")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Aplikasi Pencipta Arsip Resource",
        description = "WS Endpoint untuk menghandle operasi Aplikasi Pencipta Arsip"
)
public class MasterAplikasiPenciptaArsipResource {
    @Inject
    private MasterAplikasiPenciptaArsipService service;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get",
            response = MasterAplikasiPenciptaArsip[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List")
    public Response getAll(@DefaultValue("0")
                           @QueryParam("skip") int skip,
                           @DefaultValue("10")
                           @Max(50)
                           @QueryParam("limit") int limit,
                           @DefaultValue("")
                           @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<MasterAplikasiPenciptaArsip> all = service.getAll();
        List<MasterAplikasiPenciptaArsipDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterAplikasiPenciptaArsip::getId))
                .map(MasterAplikasiPenciptaArsipDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered",
            response = MasterAplikasiPenciptaArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Filtered list")
    public Response getFiltered(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterAplikasiPenciptaArsipDto> result = service.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterAplikasiPenciptaArsipDto::new).collect(Collectors.toList());
        long count = service.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            response = MessageDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Save a new")
    public Response saveData(MasterAplikasiPenciptaArsipDto dao) {
        try {
            MasterAplikasiPenciptaArsip model = new MasterAplikasiPenciptaArsip();
            model.setName(dao.getName());

            service.create(model);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            response = MessageDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Save an update")
    public Response editCountry(@PathParam("id") Long id, MasterAplikasiPenciptaArsipDto dao) {
        try {
            MasterAplikasiPenciptaArsip model = service.find(id);
            model.setName(dao.getName());

            service.edit(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
//notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getName(), ex.getMessage()));
            notificationService.sendError("Error", String.format("'%s' gagal diubah", dao.getName()));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @ApiOperation(
            value = "Delete",
            response = MessageDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete")
    @Path("{id}")
    public Response deleteCountry(@PathParam("id") Long id) {
        try {
            MasterAplikasiPenciptaArsip model = service.find(id);
            model.setEnd(new Date());

            service.edit(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}