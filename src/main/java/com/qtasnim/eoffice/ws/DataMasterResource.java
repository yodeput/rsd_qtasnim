package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.DataMasterService;
import com.qtasnim.eoffice.ws.dto.ListStringDto;
import com.qtasnim.eoffice.ws.dto.DataMasterValueRetrieve;
import com.qtasnim.eoffice.ws.dto.DataMasterValueRetrieveParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("master-data")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Data",
        description = "WS Endpoint untuk mendapatkan data master, field-field, serta data"
)
public class DataMasterResource {
    @Inject
    private DataMasterService dataMasterService;

    @GET
    @Path("list-master-data")
    @ApiOperation(
            value = "Get List Master Data",
            response = ListStringDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List All Master Data")
    public Response getAllDataMaster() {
        try {
            ListStringDto result = dataMasterService.getAllDataMaster();

            long count = result.getValues().size();

            if (result != null) {
                return Response
                        .ok(result)
                        .header("X-Total-Count", count)
                        .header("Access-Control-Expose-Headers", "X-Total-Count")
                        .build();
            } else {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @GET
    @Path("list-field/{data-master}")
    @ApiOperation(
            value = "Get List Field dari Master Data Terpilih",
            response = ListStringDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List All Fields dari Master Data Terpilih")
    public Response getAllField(@NotNull @PathParam("data-master") String datamaster) throws InternalServerErrorException {
        try {
            ListStringDto result = dataMasterService.getAllFieldOfMasterData(datamaster);

            long count = result.getValues().size();

            if(result != null) {
                return Response
                        .ok(result)
                        .header("X-Total-Count", count)
                        .header("Access-Control-Expose-Headers", "X-Total-Count")
                        .build();
            } else {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("values")
    @ApiOperation(
            value = "Get Values dari Master Data & Field-field Terpilih",
            response = DataMasterValueRetrieve[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List Values dari Master Data & Field-field Terpilih")
    public Response getValues(DataMasterValueRetrieveParam param) throws InternalServerErrorException {
        try {
            DataMasterValueRetrieve result = dataMasterService.getValueOfSelectedMasterDataFields(param);
            long count = result.getValues().size();

            if(result != null) {
                return Response
                        .ok(result)
                        .header("X-Total-Count", count)
                        .header("Access-Control-Expose-Headers", "X-Total-Count")
                        .build();
            } else {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}
