package com.qtasnim.eoffice.ws;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.qtasnim.eoffice.db.MasterAccessClassification;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterAccessClassificationService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterAccessClassificationDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

@Path("master-access-classification")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class MasterAccessClassificationResource {
    @Inject
    private MasterAccessClassificationService service;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterAccessClassification> all = service.getAll();
        List<MasterAccessClassificationDto> result = all
                .filter(q -> !q.getIsDeleted())
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterAccessClassification::getId))
                .map(MasterAccessClassificationDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    public Response saveData(MasterAccessClassificationDto dao) {
        try {
            MasterAccessClassification model = new MasterAccessClassification();
            model.setName(dao.getName());
            model.setIsDeleted(false);

            service.create(model);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    public Response editCountry(@PathParam("id") Long id, MasterAccessClassificationDto dao) {
        try {
            MasterAccessClassification model = service.find(id);

            model.setName(dao.getName());
            service.edit(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getName(), ex.getMessage()));
            notificationService.sendError("Error", String.format("'%s' gagal diubah", dao.getName()));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteCountry(@PathParam("id") Long id) {
        try {
            MasterAccessClassification model = service.find(id);

            model.setIsDeleted(true);
            service.edit(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}