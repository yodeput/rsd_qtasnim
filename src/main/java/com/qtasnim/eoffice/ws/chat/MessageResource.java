package com.qtasnim.eoffice.ws.chat;

import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.chat.Contact;
import com.qtasnim.eoffice.db.chat.Message;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterUserService;
import com.qtasnim.eoffice.services.chat.ContactService;
import com.qtasnim.eoffice.services.chat.MessageService;
import com.qtasnim.eoffice.ws.dto.chat.MessageDto;
import com.qtasnim.eoffice.ws.dto.chat.ContactDto;

import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("chat/message")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class MessageResource {
    @Inject
    private MessageService messageService;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    private ContactService contactService;

    @POST
    @Path("addFriend")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response applyAddFriend(
            @FormParam("uid") Long uid,
            @FormParam("toUid") Long toUid) {
        if (uid != null && toUid != null) {
            MasterUser fromUser = masterUserService.find(uid);
            MasterUser toUser = masterUserService.find(toUid);
            Contact contact = contactService.insertContact(fromUser, toUser);

            return Response
                    .ok(new ContactDto(contact))
                    .build();
        }

        return Response
                .ok()
                .build();
    }

    @POST
    @Path("sendMessage")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response sendMessage(
            @FormParam("uid") Long uid,
            @FormParam("toUid") Long toUid,
            @FormParam("message") String message,
            @FormParam("chatId") String chatId) {
        if (uid != null && toUid != null) {
            MasterUser fromUser = masterUserService.find(uid);
            MasterUser toUser = masterUserService.find(toUid);
            messageService.insertMessage(fromUser, toUser, message, chatId);
        }

        return Response
                .ok()
                .build();
    }

    @POST
    @Path("openChat")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response openChat(
            @FormParam("uid") Long uid,
            @FormParam("toUid") Long toUid) {
        if (uid != null && toUid != null) {
            MasterUser fromUser = masterUserService.find(uid);
            MasterUser toUser = masterUserService.find(toUid);
            contactService.openChat(fromUser, toUser);
        }

        return Response
                .ok()
                .build();
    }

    @POST
    @Path("loadChats")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response getMsgbox(
            @FormParam("uid") Long uid,
            @FormParam("toUid") Long toUid,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit) {
        List<Object> result = messageService.getMessages(uid, toUid)
                .sortedDescendingBy(Message::getAddTime)
                .skip(skip)
                .limit(limit)
                .toList()
                .stream()
                .map(MessageDto::new)
                .collect(Collectors.toList());

        return Response
                .ok(result)
                .build();
    }
}
