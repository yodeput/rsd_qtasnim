package com.qtasnim.eoffice.ws.chat;

import com.qtasnim.eoffice.db.chat.Group;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.chat.ContactService;
import com.qtasnim.eoffice.services.chat.GroupService;
import com.qtasnim.eoffice.ws.dto.chat.ContactDto;
import com.qtasnim.eoffice.ws.dto.chat.GroupMembersDto;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("chat")
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class ChatResource {
    @Inject
    private ContactService contactService;

    @Inject
    private GroupService groupService;

    @POST
    @Path("friends")
    public Response init(
            @FormParam("id") Long id,
            @FormParam("key") String key) {
        if (id != null) {
            List<ContactDto> result = contactService.getContacts(id).stream().map(ContactDto::new).collect(Collectors.toList());

            return Response
                    .ok(result)
                    .build();
        }

        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .build();
    }

    @POST
    @Path("group-member")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response member(
            @FormParam("id") Long id) {
        if (id != null) {
            Group group = groupService.find(id);
            GroupMembersDto groupMembers = groupService.getGroupMembers(group);

            return Response
                    .ok(groupMembers)
                    .build();
        }

        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .build();
    }
}
