package com.qtasnim.eoffice.ws;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qtasnim.eoffice.Constants;
import com.qtasnim.eoffice.RequirePassphraseException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.office.FileUtility;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.ng.ShowPassphraseConfirmation;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;
import org.jinq.orm.stream.JinqStream;

import javax.inject.Inject;
import javax.management.Query;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Path("ng-surat")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class NgSuratResource {

    @Inject
    private SuratService suratService;

    @Inject
    private NgSuratService ngSuratService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @Inject
    private FileService fileService;

    @Inject
    private SuratInteractionService interactionService;

    @Inject
    private SuratCatatanSekretarisService suratCatatanSekretarisService;

    @Inject
    private MasterDelegasiService masterDelegasiService;

    @Inject
    private GlobalPushEventService globalPushEventService;

    @Inject
    private ReferensiSuratService referensiSuratService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private DokumenHistoryService docHistoryService;

    @GET
    @Path("/filter")
    public Response filter(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false")
            @QueryParam("descending") boolean descending,
            @DefaultValue("true")
            @QueryParam("loadAll") boolean loadAll
    ) {
        List<SuratDto> result = suratService
                .getResultList(filter, sort, descending, skip, limit)
                .stream()
                .map(t -> {
                    if (loadAll) {
                        return new SuratDto(t);
                    } else {
                        SuratDto surat = SuratDto.minimal(t);
                        surat.bindMetadata(t);
                        surat.bindPenandatangan(t,false);
                        surat.bindDisposisi(t);

                        return surat;
                    }
                })
                .collect(Collectors.toList());

        if (result.size() > 0) {
            MasterUser user = userSession.getUserSession().getUser();
            List<Long> allID = result.stream().map(SuratDto::getId).collect(Collectors.toList());
            interactionService.getByIdSurats(allID)
                    .forEach(t
                            -> result.stream()
                            .filter(v -> v.getId().equals(t.getSurat().getId()) && t.getOrganization().getIdOrganization().equals(user.getOrganizationEntity().getIdOrganization()))
                            .findFirst()
                            .ifPresent(suratDto -> suratDto.setInteraction(new SuratInteractionDto(t, true))));
        }

        List<PenandatanganSuratDto> allPenandatangans = result.stream()
                .filter(t -> t.getStatus().equals("DRAFT") || t.getStatus().equals("SUBMITTED") || t.getStatus().equals("REJECTED"))
                .flatMap(t -> t.getPenandatanganSurat().stream())
                .filter(t -> t.getPenandatangan() != null)
                .collect(Collectors.toList());

        if (allPenandatangans.size() > 0) {
            List<String> allCodes = allPenandatangans.stream().map(t -> t.getPenandatangan().getOrganizationCode()).distinct().collect(Collectors.toList());
            masterDelegasiService.getByPejabatCodeList(allCodes)
                    .stream()
                    .filter(t -> t.getTipe().equals("PYMT"))
                    .collect(Collectors.toList())
                    .forEach(masterDelegasi -> {
                        allPenandatangans.stream().filter(t -> t.getPenandatangan().getOrganizationCode().equals(masterDelegasi.getFrom().getOrganizationCode())).forEach(p -> {
                            p.setUser(new MasterUserDto(masterDelegasi.getTo().getUser()));
                            p.setDelegasiType(masterDelegasi.getTipe());
                        });
                    });
        }

        long count = suratService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("/saveDraft")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response saveDraft(
            @Valid @FormDataParam("surat") SuratDto dto,
            @FormDataParam("file") InputStream stream,
            @FormDataParam("file") FormDataBodyPart body
    ) {
        try {
            //Create Surat
            dto.setStatus("DRAFT");
            NgSuratDto surat = ngSuratService.saveOrEditDraft(null, dto, stream, body);
            notificationService.sendInfo("Info", "Document berhasil disimpan to draft");

            return Response
                    .ok(surat)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save Draft Document: %s", Optional.ofNullable(ExceptionUtil.getRealCause(ex)).map(Throwable::getMessage).orElse(ex.getMessage())));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("editDraft/{idSurat}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response EditDraft(
            @PathParam("idSurat") Long idSurat,
            @Valid @FormDataParam("surat") SuratDto dto,
            @FormDataParam("file") InputStream stream,
            @FormDataParam("file") FormDataBodyPart body) {
        try {
            dto.setStatus(Optional.ofNullable(dto.getStatus()).orElse("DRAFT"));
            NgSuratDto surat = ngSuratService.saveOrEditDraft(idSurat, dto, stream, body);
            notificationService.sendInfo("Info", "Dokumen berhasil diubah");

            return Response
                    .ok(surat)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit Draft Document: %s", Optional.ofNullable(ExceptionUtil.getRealCause(ex)).map(Throwable::getMessage).orElse(ex.getMessage())));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("/kirimDraftSurat")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response kirimDraftSurat(
            @Valid @FormDataParam("surat") SuratDto dto,
            @FormDataParam("file") InputStream stream,
            @FormDataParam("file") FormDataBodyPart body
    ) {
        String prevStatus = dto.getStatus();

        try {
            dto.setStatus("SUBMITTED");
            NgSuratDto surat = ngSuratService.kirimSurat(dto, stream, body);

            DokumenHistoryDto history = new DokumenHistoryDto();
            history.setReferenceTable("t_surat");
            history.setReferenceId(surat.getId());
            history.setStatus("SEND");

            MasterUser usr = userSession.getUserSession().getUser();
            boolean isPYMT = masterDelegasiService.isPYMT(dto.getIdKonseptor(),usr.getOrganizationEntity().getIdOrganization());
            history.setIdOrganisasi(dto.getIdKonseptor());
            history.setIdUser(usr.getId());
            if(isPYMT) {
                history.setDelegasiType("PYMT");
            }
            docHistoryService.saveHistory(history);

            return Response
                    .ok(surat)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            Throwable realCause = ExceptionUtil.getRealCause(ex);

            if (realCause instanceof RequirePassphraseException) {
                dto.setStatus(prevStatus);

                try {
                    globalPushEventService.push(new ShowPassphraseConfirmation(getObjectMapper().writeValueAsString(dto), "submit"), userSession.getUserSession().getUser());
                    notificationService.sendInfo("Info", "Passphrase Confirmation");
                } catch (Exception e) {

                }

                return Response
                        .ok(dto)
                        .status(Response.Status.OK)
                        .build();
            } else {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Send Document: %s", Optional.ofNullable(realCause).map(Throwable::getMessage).orElse(ex.getMessage())));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @GET
    @Path("getKonsep")
    public Response getDocDraft(
            @DefaultValue("") @QueryParam("idSurat") Long idSurat,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        Surat surat = suratService.find(idSurat);
        JPAJinqStream<GlobalAttachment> all;

        if (surat.getCompletionStatus().equals(EntityStatus.APPROVED)
                || surat.getCompletionStatus().equals(EntityStatus.CLOSED)
                || surat.getFormDefinition().getJenisDokumen().getBaseCode().equals(Constants.KODE_REGISTRASI_SURAT)) {
            all = globalAttachmentService.getPublished("t_surat", idSurat);
        } else {
            all = globalAttachmentService.getKonsep("t_surat", idSurat);
        }

        List<GlobalAttachmentDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(t -> {
                    GlobalAttachmentDto dto = new GlobalAttachmentDto(t);
                    dto.setEditLink(fileService.getEditLink(t.getDocId()));
                    dto.setDownloadLink(fileService.getViewLink(t.getDocId(), false));

                    if (t.getDocName().lastIndexOf(".") > 0) {
                        dto.setViewLink(fileService.getViewLink(t.getDocId(), FileUtility.GetFileExtension(t.getDocName())));
                    } else {
                        dto.setViewLink(fileService.getViewLink(t.getDocId()));
                    }

                    return dto;
                })
                .collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getFavorit")
    public Response getFavorit(
            @DefaultValue("") @QueryParam("idJabatan") Long idJabatan,
            @QueryParam("idUser") Long idUser,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {
        HashMap<String, Object> temp  = ngSuratService.getFavoritSurat(idJabatan,idUser,filter, skip, limit,descending, sort);
        List<SuratDto> result = (List<SuratDto>) temp.get("data");

        if (result.size() > 0) {
            List<Long> allID = result.stream().map(SuratDto::getId).collect(Collectors.toList());
            if(idUser == null) {
                interactionService.getByIdSurats(allID).stream().filter(w -> w.getOrganization().getIdOrganization().equals(idJabatan))
                        .forEach(t
                                -> result.stream()
                                .filter(v -> v.getId().equals(t.getSurat().getId()))
                                .findFirst()
                                .ifPresent(suratDto -> suratDto.setInteraction(new SuratInteractionDto(t))));
            }else{
                interactionService.getByIdSurats(allID).stream().filter(w -> w.getOrganization().getIdOrganization().equals(idJabatan) ||
                        (w.getUser()!=null && w.getUser().getId().equals(idUser))).forEach(t
                                -> result.stream()
                                .filter(v -> v.getId().equals(t.getSurat().getId()))
                                .findFirst()
                                .ifPresent(suratDto -> suratDto.setInteraction(new SuratInteractionDto(t))));
            }
        }

        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("arsipkan/{id}")
    public Response arsipkan(
            @NotNull @PathParam("id") Long id,
            @NotNull @QueryParam("idArsip") Long idArsip) throws InternalServerErrorException {
        Surat surat = ngSuratService.find(id);
        FormValue fv = surat.getFormValue().stream().filter(b -> b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                .findFirst().orElse(null);
        try {
            ngSuratService.arsipkan(id, idArsip);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil diarsipkan", fv.getValue()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diarsipkan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
//            notificationService.sendError("Error", String.format("Data gagal diarsipkan %s': %s", idArsip, id));
            notificationService.sendError("Error", String.format("Data '%s' gagal diarsipkan", fv.getValue()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("restoreDraft/{idSurat}")
    public Response restoreDraft(@PathParam("idSurat") long idSurat) {
        try {
            Surat obj = suratService.find(idSurat);
            obj.setIsDeleted(false);
            suratService.edit(obj);
            notificationService.sendInfo("Info", "Dokumen berhasil dikembalikan");

            return Response
                    .ok(new MessageDto("info", "Dokumen berhasil dikembalikan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Restore: %s", Optional.ofNullable(ExceptionUtil.getRealCause(ex)).map(Throwable::getMessage).orElse(ex.getMessage())));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("cancelDraft/{idSurat}")
    public Response cancelDraft(@PathParam("idSurat") long idSurat) {
        try {
            Surat obj = suratService.find(idSurat);

            ngSuratService.batalSurat(obj);
            suratService.edit(obj);

            DokumenHistoryDto history = new DokumenHistoryDto();
            history.setReferenceTable("t_surat");
            history.setReferenceId(idSurat);
            history.setStatus("CANCEL");

            MasterUser usr = userSession.getUserSession().getUser();
            history.setIdOrganisasi(usr.getOrganizationEntity().getIdOrganization());
            history.setIdUser(usr.getId());
            docHistoryService.saveHistory(history);

            notificationService.sendInfo("Info", "Dokumen berhasil dibatalkan");

            return Response
                    .ok(new MessageDto("info", "Dokumen berhasil dibatalkan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal batal dokumen: %s", Optional.ofNullable(ExceptionUtil.getRealCause(ex)).map(Throwable::getMessage).orElse(ex.getMessage())));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("restoreCancel/{idSurat}")
    public Response restoreCancel(@PathParam("idSurat") long idSurat) {
        try {
            Surat obj = suratService.find(idSurat);
            obj.setStatus("DRAFT");
            suratService.edit(obj);
            notificationService.sendInfo("Info", "Dokumen berhasil dikembalikan");

            return Response
                    .ok(new MessageDto("info", "Dokumen berhasil dikembalikan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal mengembalikan dokumen: %s", Optional.ofNullable(ExceptionUtil.getRealCause(ex)).map(Throwable::getMessage).orElse(ex.getMessage())));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("referensi/add/{idSurat}")
    public Response addReferensiSurat(
            @PathParam("idSurat") Long idSurat,
            @QueryParam("idOrganization") Long idOrganization,
            List<ReferensiSuratDto> dtos
    ) {
        try {
            List<DokumenReferensiDto> result = ngSuratService
                    .addRefferences(idSurat, Optional.ofNullable(idOrganization).orElse(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization()), dtos)
                    .stream()
                    .map(DokumenReferensiDto::new)
                    .collect(Collectors.toList());
            notificationService.sendInfo("Info", "Referensi berhasil ditambahkan");

            return Response
                    .ok(result)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Referensi gagal ditambahkan: %s", Optional.ofNullable(ExceptionUtil.getRealCause(ex)).map(Throwable::getMessage).orElse(ex.getMessage())));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("getSuratNotes/byIdSurats")
    public Response getSuratNotesByIdSurats(
            List<Long> idSurats
    ) {
        List<SuratCatatanSekretarisDto> result = suratCatatanSekretarisService.findBySuratIds(idSurats).stream().map(SuratCatatanSekretarisDto::new).collect(Collectors.toList());

        return Response
                .ok(result)
                .status(Response.Status.OK)
                .build();
    }

    @DELETE
    @Path("referensi/remove/{refDetailId}")
    public Response removeReferensiSurat(
            @PathParam("refDetailId") long refDetailId,
            @QueryParam("idSurat") Long idSurat
    ) {
        try {
            ngSuratService.removeRefference(refDetailId,idSurat);
            notificationService.sendInfo("Info", "Referensi berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Referensi berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Referensi gagal dihapus: %s", Optional.ofNullable(ExceptionUtil.getRealCause(ex)).map(Throwable::getMessage).orElse(ex.getMessage())));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("needAttention/draft/count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response needAttentionDraftCount(
            @QueryParam("idOrganization") Long idOrganization
    ) {
        try {
            Long count = suratService.suratDraftNeedAttCount(Optional.ofNullable(idOrganization).orElse(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization()));

            return Response
                    .ok(count)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("needAttention/masuk/count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response needAttentionMasukCount(
            @QueryParam("idOrganization") Long idOrganization
    ) {
        try {
            Long count = suratService.suratMasukNeedAttCount(Optional.ofNullable(idOrganization).orElse(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization()));

            return Response
                    .ok(count)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("needAttention/keluar/count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response needAttentionKeluarCount(
            @QueryParam("idOrganization") Long idOrganization
    ) {
        try {
            Long count = suratService.suratKeluarNeedAttCount(Optional.ofNullable(idOrganization).orElse(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization()));

            return Response
                    .ok(count)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("needAttention/setuju/count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response needAttentionSetujuCount(
            @QueryParam("idOrganization") Long idOrganization
    ) {
        try {
            Long orgId = Optional.ofNullable(idOrganization).orElse(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());
            List<Long> orgIds = masterDelegasiService.getByPejabatIdList(Arrays.asList(orgId))
                    .stream()
                    .map(t -> t.getTo().getIdOrganization())
                    .collect(Collectors.toList());
            orgIds.add(orgId);

            Long count = suratService.suratSetujuNeedAttCount(orgIds);

            return Response
                    .ok(count)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("needAttention/reg/count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response needAttentionRegCount(
            @QueryParam("idOrganization") Long idOrganization
    ) {
        try {
            Long count = suratService.suratRegNeedAttCount(Optional.ofNullable(idOrganization).orElse(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization()));

            return Response
                    .ok(count)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("getReferensi")
    public Response getRefensi(
            @DefaultValue("") @QueryParam("idJabatan") Long idJabatan,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JinqStream<Surat> all = ngSuratService.getReferensi(idJabatan);
        List<SuratDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Surat::getId))
                .map(SuratDto::new)
                .collect(Collectors.toList());

        if (result.size() > 0) {
            List<Long> allID = result.stream().map(SuratDto::getId).collect(Collectors.toList());
            interactionService.getByIdSurats(allID)
                    .forEach(t
                            -> result.stream()
                            .filter(v -> v.getId().equals(t.getSurat().getId()))
                            .findFirst()
                            .ifPresent(suratDto -> suratDto.setInteraction(new SuratInteractionDto(t))));
        }

        Long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("getReferensi2")
    public Response getRefensi2(
            @DefaultValue("") @QueryParam("idJabatan") Long idJabatan,
            @QueryParam("idUser") Long idUser,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {
        HashMap<String, Object> temp  = referensiSuratService.getFiltered(idJabatan,idUser,filter, skip, limit,descending, sort);
        List<SuratDto> result = (List<SuratDto>) temp.get("data");

        if (result.size() > 0) {
            List<Long> allID = result.stream().map(SuratDto::getId).collect(Collectors.toList());
            interactionService.getByIdSurats(allID)
                    .forEach(t
                            -> result.stream()
                            .filter(v -> v.getId().equals(t.getSurat().getId()))
                            .findFirst()
                            .ifPresent(suratDto -> suratDto.setInteraction(new SuratInteractionDto(t))));
        }

        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("updateSurat/{idSurat}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveDraft(
            @Valid @PathParam("idSurat") Long idSurat
    ) {
        try {
            Surat obj = suratService.find(idSurat);
            ngSuratService.updateKonsep(obj);

            return Response
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
