package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.EntityStatus;
import com.qtasnim.eoffice.db.MasterTindakan;
import com.qtasnim.eoffice.db.SuratInteraction;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.GlobalAttachmentService;
import com.qtasnim.eoffice.services.SuratDisposisiService;
import com.qtasnim.eoffice.services.SuratInteractionService;
import com.qtasnim.eoffice.services.SuratService;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("surat")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Surat Interaction Management",
        description = "WS Endpoint untuk menghandle operasi Interaksi Surat"
)
public class SuratInteractionResource {

    @Inject
    private SuratInteractionService suratInteractionService;

    @Inject
    private SuratService suratService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @GET
    @Path("interaction")
    @ApiOperation(
            value = "Get",
            response = SuratInteractionDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get All Surat Interaction")
    public Response getAll( @DefaultValue("0") @QueryParam("skip") int skip,
                            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                            @DefaultValue("") @QueryParam("filter") String filter,
                            @QueryParam("sort") String sort,
                            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<SuratInteractionDto> result = suratInteractionService.getResultList(filter, sort, descending, skip, limit).stream().
                map(SuratInteractionDto::new).collect(Collectors.toList());
        long count = suratInteractionService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("interaction/riwayat/{idSurat}")
    @ApiOperation(
            value = "Get",
            response = RiwayatDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get All Surat Interaction")
    public Response riwayat(
            @PathParam("idSurat") Long idSurat,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit) throws InternalServerErrorException {
        JPAJinqStream<SuratInteraction> riwayat = suratInteractionService.getRiwayat(idSurat);
        List<RiwayatDto> result = riwayat
                .skip(skip)
                .limit(limit)
                .toList()
                .stream()
                .map(RiwayatDto::new)
                .collect(Collectors.toList());
        long count = riwayat.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("interaction/bySuratId/{idSurat}")
    @ApiOperation(
            value = "Get Interaction History by Id Surat",
            response = SuratInteractionDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Get Interaction History by Id Surat")
    public Response getAllBySuratId(@PathParam("idSurat") Long idSurat, @DefaultValue("0") @QueryParam("skip") int skip,
                                    @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                    @DefaultValue("") @QueryParam("filter") String filter,
                                    @QueryParam("sort") String sort,
                                    @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<SuratInteractionDto> result = suratInteractionService.getByIdSurat(idSurat, filter, sort, descending, skip, limit).stream().
                map(SuratInteractionDto::new).collect(Collectors.toList());
        long count = suratInteractionService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("interaction/setStar/{idSurat}")
    @ApiOperation(
            value = "Set as Stared",
            produces = MediaType.APPLICATION_JSON,
            notes = "Set as Stared")
    public Response setStared(
            @NotNull
            @PathParam("idSurat") Long idSurat,
            @DefaultValue(value = "true")
            @QueryParam("value") Boolean value
    ) {
        try{
            suratInteractionService.setStared(idSurat, value);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("interaction/setFinish/{idSurat}")
    @ApiOperation(
            value = "Set as Finished",
            produces = MediaType.APPLICATION_JSON,
            notes = "Set as Finished")
    public Response setFinished(
            @NotNull
            @PathParam("idSurat") Long idSurat,
            @DefaultValue(value = "true")
            @QueryParam("value") Boolean value
    ) {
        try{
            suratInteractionService.setFinished(idSurat, value);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("interaction/setImportant/{idSurat}")
    @ApiOperation(
            value = "Set as Important",
            produces = MediaType.APPLICATION_JSON,
            notes = "Set as Important")
    public Response setImportant(
            @NotNull
            @PathParam("idSurat") Long idSurat,
            @DefaultValue(value = "true")
            @QueryParam("value") Boolean value
    ) {
        try{
            suratInteractionService.setImportant(idSurat, value);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("interaction/setRead/{idSurat}")
    @ApiOperation(
            value = "Set as Read",
            produces = MediaType.APPLICATION_JSON,
            notes = "Set as Read")
    public Response setIsRead(
            @NotNull
            @PathParam("idSurat") Long idSurat
    ) {
        try{
            suratInteractionService.setIsRead(idSurat);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("interaction/addToCal")
    @ApiOperation(
            value = "Add to Calendar",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add to Calendar")
    public Response addToCalendar(@Valid AgendaDto dao) {
        try{
            suratInteractionService.addToCalendar(dao);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }

    @POST
    @Path("action")
    @ApiOperation(
            value = "Aksi Surat",
            produces = MediaType.APPLICATION_JSON,
            notes = "Aksi Surat")
    public Response actionSurat(@Valid ApprovalDto dao) {
        try{
            
            /**
             * action --> 
             *      APPROVE = 1
             *      REJECT = 0
             *      REJECT_TO_PREVIOUS = -1
             */

            SuratDto surat = new SuratDto(suratService.find(dao.getId()));
            if(!Strings.isNullOrEmpty(dao.getNote())) {
                surat.setWorkflowNote(dao.getNote());
            }
            surat.setWorkflowAction(dao.getAction());
            suratService.executeApprovalSurat(surat);
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}
