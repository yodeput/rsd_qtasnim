package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.DokumenHistoryDto;
import com.qtasnim.eoffice.ws.dto.MasterStrukturOrganisasiDto;
import com.qtasnim.eoffice.ws.dto.MasterUserDto;
import com.qtasnim.eoffice.ws.dto.RiwayatDto;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("ng-surat")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class NgSuratInteractionResource {

    @Inject
    private NgSuratInteractionService ngSuratInteractionService;

    @Inject
    private SuratInteractionService suratInteractionService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @Inject
    private DokumenHistoryService docHistoryService;

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private PenandatanganSuratService penandatanganSuratService;

    @GET
    @Path("interaction/riwayat/{idSurat}")
    public Response riwayat(
            @PathParam("idSurat") Long idSurat,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit) throws InternalServerErrorException {
//        JPAJinqStream<SuratInteraction> riwayat = suratInteractionService.getRiwayat(idSurat);
        JPAJinqStream<DokumenHistory> listHistory = docHistoryService.getHistory("t_surat",idSurat);
        List<RiwayatDto> result = listHistory
                .skip(skip)
//                .limit(limit)
                .toList()
                .stream()
                .map(q ->{
                  RiwayatDto r = new RiwayatDto();
                  DateUtil dateUtil = new DateUtil();
                  if(q.getStatus().equals("APPROVE")){
                      PenandatanganSurat ps = penandatanganSuratService.getByOrgSurat(q.getOrganization().getIdOrganization(),q.getReferenceId());
                      if(ps!=null) {
                          if (ps.getOrgAN() != null) {
                              r.setOrganization(new MasterStrukturOrganisasiDto(q.getOrganization()));
                              r.setDate(dateUtil.getCurrentISODate(q.getActionDate()));
                              r.setAction(q.getStatus());
                              r.setUser(new MasterUserDto(q.getUser()));
                              String atasNama = "a.n. " + ps.getOrgAN().getOrganizationName();
                              r.setDelegasiType(atasNama);
                          } else {
                              r.setOrganization(new MasterStrukturOrganisasiDto(q.getOrganization()));
                              r.setDate(dateUtil.getCurrentISODate(q.getActionDate()));
                              r.setAction(q.getStatus());
                              r.setUser(new MasterUserDto(q.getUser()));
                              if (!Strings.isNullOrEmpty(q.getDelegasiType())) {
                                  r.setDelegasiType(q.getDelegasiType());
                              }
                          }
                      }else{
                          r.setOrganization(new MasterStrukturOrganisasiDto(q.getOrganization()));
                          r.setDate(dateUtil.getCurrentISODate(q.getActionDate()));
                          r.setAction(q.getStatus());
                          r.setUser(new MasterUserDto(q.getUser()));
                          if (!Strings.isNullOrEmpty(q.getDelegasiType())) {
                              r.setDelegasiType(q.getDelegasiType());
                          }
                      }
                  }else {
                      r.setOrganization(new MasterStrukturOrganisasiDto(q.getOrganization()));
                      r.setDate(dateUtil.getCurrentISODate(q.getActionDate()));
                      r.setAction(q.getStatus());
                      r.setUser(new MasterUserDto(q.getUser()));
                      if (!Strings.isNullOrEmpty(q.getDelegasiType())) {
                          r.setDelegasiType(q.getDelegasiType());
                      }
                  }
                  return r;
                }).collect(Collectors.toList());
        long count = listHistory.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("interaction/setStar/{idSurat}")
    public Response setStared(
            @NotNull
            @PathParam("idSurat") Long idSurat,
            @DefaultValue(value = "true")
            @QueryParam("value") Boolean value
    ) {
        String action = "Set Starred Flag";

        if (!value) {
            action = "Remove Starred Flag";
        }

        try{
            ngSuratInteractionService.setStared(idSurat, value);
            notificationService.sendInfo("Info", String.format("%s Succeed", action));

            return Response
                    .ok()
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("%s Failed: %s", action, ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("interaction/setFinish/{idSurat}")
    public Response setFinished(
            @NotNull
            @PathParam("idSurat") Long idSurat,
            @DefaultValue(value = "true")
            @QueryParam("value") Boolean value
    ) {
        String action = "Set Finish Flag";

        if (!value) {
            action = "Remove Finish Flag";
        }

        try{
            ngSuratInteractionService.setFinished(idSurat, value);
            notificationService.sendInfo("Info", String.format("%s Succeed", action));

            return Response
                    .ok()
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("%s Failed: %s", action, ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("interaction/setImportant/{idSurat}")
    public Response setImportant(
            @NotNull
            @PathParam("idSurat") Long idSurat,
            @DefaultValue(value = "true")
            @QueryParam("value") Boolean value
    ) {
        String action = "Set Important Flag";

        if (!value) {
            action = "Remove Important Flag";
        }

        try{
            ngSuratInteractionService.setImportant(idSurat, value);
            notificationService.sendInfo("Info", String.format("%s Succeed", action));

            return Response
                    .ok()
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("%s Failed: %s", action, ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("interaction/setRead/{idSurat}")
    public Response setIsRead(
            @NotNull
            @PathParam("idSurat") Long idSurat
    ) {
        try{
            ngSuratInteractionService.setIsRead(idSurat);

            DokumenHistoryDto history = new DokumenHistoryDto();
            history.setReferenceTable("t_surat");
            history.setReferenceId(idSurat);
            history.setStatus("READ");

            MasterUser usr = userSession.getUserSession().getUser();
            history.setIdOrganisasi(usr.getOrganizationEntity().getIdOrganization());
            history.setIdUser(usr.getId());
            docHistoryService.saveHistory(history);

            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}
