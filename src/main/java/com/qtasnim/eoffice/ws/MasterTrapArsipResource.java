package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterTrapArsip;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterTrapArsipService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterTrapArsipDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-lokasi-arsip")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Lokasi Arsip Resource",
        description = "WS Endpoint untuk menghandle operasi Lokasi Arsip"
)
public class MasterTrapArsipResource {
    @Inject
    private MasterTrapArsipService masterTrapArsipService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @Path("trap")
    @ApiOperation(
            value = "Get Data TrapArsip",
            response = MasterTrapArsip[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return TrapArsip list")
    public Response getTrapArsip(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<MasterTrapArsip> all = masterTrapArsipService.getAll();
        List<MasterTrapArsipDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterTrapArsip::getId))
                .map(MasterTrapArsipDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("trap/filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterTrapArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return TrapArsip list")
    public Response getFilteredTrapArsip(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterTrapArsipDto> result = masterTrapArsipService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterTrapArsipDto::new).collect(Collectors.toList());
        long count = masterTrapArsipService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("trap")
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Add TrapArsip")
    public Response saveTrapArsip(@Valid MasterTrapArsipDto dao) {
        try {
            masterTrapArsipService.save(null, dao);
            notificationService.sendInfo("Info", String.format("Data berhasil disimpan", dao.getKode()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal disimpan : %s", dao.getKode(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("trap/{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit TrapArsip")
    public Response editTrapArsip(@NotNull @PathParam("id") Long id, @Valid MasterTrapArsipDto dao) {
        try {
            masterTrapArsipService.save(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil diubah", dao.getKode()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data '%s' gagal diubah: %s", dao.getKode(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("trap/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete TrapArsip")
    public Response deleteTrapArsip(@NotNull @PathParam("id") Long id) {
        try {
            MasterTrapArsip deleted = masterTrapArsipService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil dihapus", deleted.getKode()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus, Data sudah digunakan"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("trap/all")
    @ApiOperation(
            value = "Get All Data",
            response = MasterTrapArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return TrapArsip list")
    public Response getAllTrapArsip(
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterTrapArsipDto> result = masterTrapArsipService.getResultList(filter, sort, descending).stream()
                .map(MasterTrapArsipDto::new).collect(Collectors.toList());
        long count = masterTrapArsipService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}