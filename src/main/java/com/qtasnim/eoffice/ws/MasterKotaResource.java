package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.MasterKota;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterKotaService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterKotaDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-kota")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Kota Resource",
        description = "WS Endpoint untuk menghandle operasi Kota"
)
public class MasterKotaResource {
    @Inject
    private MasterKotaService masterKotaService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterKotaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kota list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterKota> all = masterKotaService.getAll();
        List<MasterKotaDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterKota::getIdKota))
                .map(MasterKotaDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterKotaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Kota list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{
        List<MasterKota> data = masterKotaService.getResultList(filter, sort, descending, skip, limit);
        long count = masterKotaService.count(filter);

        List<MasterKotaDto> result = data.stream().map(q->new MasterKotaDto(q)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Kota")
    public Response saveData(@Valid MasterKotaDto dao) {
        try {
            masterKotaService.saveOrEdit(dao, null);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNamaKota()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNamaKota(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Kota")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterKotaDto dao) {
        try {
            masterKotaService.saveOrEdit(dao, id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNamaKota()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNamaKota(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Kota")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            MasterKota model = masterKotaService.find(id);
            masterKotaService.remove(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getNamaKota()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
