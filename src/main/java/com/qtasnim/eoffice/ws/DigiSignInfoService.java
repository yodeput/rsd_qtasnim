package com.qtasnim.eoffice.ws;


import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.Surat;
import com.qtasnim.eoffice.services.SuratService;
import com.qtasnim.eoffice.ws.dto.SuratDto;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("digi-sign-info")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class DigiSignInfoService {
    @Inject
    private SuratService suratService;

    @GET
    public Response getArea(
            @QueryParam("suratId") Long suratId
    ) throws InternalServerErrorException {
        Surat surat = suratService.findSigned(suratId);

        if (surat != null) {
            return Response.ok(new SuratDto(surat)).build();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
