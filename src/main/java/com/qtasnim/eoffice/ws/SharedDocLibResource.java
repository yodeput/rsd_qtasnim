package com.qtasnim.eoffice.ws;


import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.NotAuthorizedException;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.security.UserNotFoundException;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Seno Anggoro, seno@qtasnim.com
 */
@Path("shared-doclib")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SharedDocLibResource {
    @Inject
    private SharedDocLibService sharedDocLibService;

    @Inject
    private SharedDocLibVendorService sharedDocLibVendorService;

    @Inject
    private FileService fileService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private SuratService suratService;

    @Inject
    private ReferensiSuratDetailService refMailDetailService;

    @GET
    public Response post(
            @QueryParam("token") String token,
            @QueryParam("username") String username,
            @QueryParam("password") String password
    ) {
        if (StringUtils.isNotEmpty(token) && StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) {
            SharedDocLib sharedDocLib = sharedDocLibService.findByAuth(token, username, password);

            if (sharedDocLib != null) {
                SharedDocLibVendor sharedDocLibVendor = sharedDocLib.getSharedDocLibVendor();

                if (sharedDocLibVendor != null && sharedDocLibVendor.getReadDate() == null) {
                    Date now = new Date();
                    sharedDocLibVendor.setReadDate(now);
                    sharedDocLibVendorService.edit(sharedDocLibVendor);
                }

                return Response.ok(new SharedDocLibVendorDto(sharedDocLibVendor)).build();
            } else {
                throw new UserNotFoundException("Username/Password Salah");
            }
        } else {
            throw new UserNotFoundException("Username/Password Salah");
        }
    }

    @GET
    @Path("vendor-history/{suratId}")
    public Response getVendorSharedDocLibHistory(
            @PathParam("suratId") Long suratId
    ) {
        List<SharedDocLibVendorDto> result = sharedDocLibVendorService
                .getBySuratAndReaded(suratId)
                .stream()
                .map(SharedDocLibVendorDto::new)
                .collect(Collectors.toList());

        return Response.ok(result).build();
    }

    @GET
    @Path("getFiles")
    @ApiOperation(
            value = "Get Files",
            response = LampiranDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return File List")
    public Response getAttachmentFile(
            @DefaultValue("") @QueryParam("idSurat") Long idSurat,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        List<LampiranDto> result = new ArrayList<>();
        /*get surat*/
        GlobalAttachment obj = globalAttachmentService.getPublished("t_surat",idSurat)
                .sortedDescendingBy(GlobalAttachment::getId).limit(1).findFirst().orElse(null);
        LampiranDto surat = new LampiranDto();
        if(obj!=null){
            DateUtil dateUtil = new DateUtil();
            surat.setId(obj.getDocId());
            surat.setNamaFile(obj.getDocName());
            surat.setUkuran(obj.getSize());
            surat.setTipe(obj.getMimeType());
            surat.setWaktu(dateUtil.getCurrentISODate(obj.getCreatedDate()));
            surat.setPengunggah(obj.getCreatedBy());
            surat.setDownloadLink(fileService.getViewLink(obj.getDocId(), false));
        }

        result.add(surat);

        /*ambil surat referensi*/
        List<ReferensiSuratDetail> list = refMailDetailService.getBySurat(idSurat).skip(skip).limit(limit).collect(Collectors.toList());
        List<LampiranDto> reference = new ArrayList<>();
        for(ReferensiSuratDetail rsd: list){
           GlobalAttachment q = globalAttachmentService.getAllDataByRef("t_surat", rsd.getReferensiSurat().getSurat().getId())
                    .sortedDescendingBy(GlobalAttachment::getId).limit(1).findFirst().orElse(null);
           if(q!=null){
               LampiranDto s = new LampiranDto();
               DateUtil dateUtil = new DateUtil();
               s.setId(q.getDocId());
               s.setNamaFile(q.getDocName());
               s.setUkuran(q.getSize());
               s.setTipe(q.getMimeType());
               s.setWaktu(dateUtil.getCurrentISODate(q.getCreatedDate()));
               s.setPengunggah(q.getCreatedBy());
               s.setDownloadLink(fileService.getViewLink(q.getDocId(), false));
               reference.add(s);
           }
        }

        result.addAll(reference);

        /*data attachment*/
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getAttachmentDataByRef("t_surat", idSurat);
        List<LampiranDto> attachment = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(q -> {
                    LampiranDto t = new LampiranDto();
                    DateUtil dateUtil = new DateUtil();
                    t.setId(q.getDocId());
                    t.setNamaFile(q.getDocName());
                    t.setUkuran(q.getSize());
                    t.setTipe(q.getMimeType());
                    t.setWaktu(dateUtil.getCurrentISODate(q.getCreatedDate()));
                    t.setPengunggah(q.getCreatedBy());
                    t.setDownloadLink(fileService.getViewLink(q.getDocId(), false));

                    return t;
                }).collect(Collectors.toList());

        result.addAll(attachment);

        return Response
                .ok(result)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}
