package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.ng.FormDefinitionDto;
import com.qtasnim.eoffice.ws.dto.ng.FormFieldDto;
import com.qtasnim.eoffice.InternalServerErrorException;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("form")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Stateless
@LocalBean
public class FormDefinitionAngularResource {
    @Inject
    private FormDefinitionService formDefinitionService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @Inject
    private FormFieldService formFieldService;

    @Inject
    private SuratService suratService;

    @Inject
    private MasterJenisDokumenService masterJenisDokumenService;

    @Inject
    private MasterMetadataService masterMetadataService;

    @GET
    @Path("{idJenisDokumen}")
    public Response get(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @QueryParam("limit") int limit,
            @PathParam("idJenisDokumen") Long idJenisDokumen) {

        FormDefinition activated = formDefinitionService.getActivated(idJenisDokumen);
        JPAJinqStream<FormDefinition> all = formDefinitionService.getHistory(idJenisDokumen);
        Long count = all.count();
        List<FormDefinitionDto> result = all
                .skip(skip)
                .limit(limit)
                .map(t -> {
                    FormDefinitionDto dto = new FormDefinitionDto(t);
                    dto.setActivated(activated != null && t.getId().equals(activated.getId()));

                    return dto;
                })
                .collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    public Response getCountries(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending)throws InternalServerErrorException {
        List<com.qtasnim.eoffice.ws.dto.FormDefinitionDto> result = formDefinitionService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(com.qtasnim.eoffice.ws.dto.FormDefinitionDto::new).collect(Collectors.toList());
        long count = formDefinitionService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("published")
    public Response getPublished(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @QueryParam("limit") int limit) {

        JPAJinqStream<FormDefinition> all = formDefinitionService.getPublished();
        Long count = all.count();
        List<FormDefinitionDto> result = all
                .skip(skip)
                .limit(limit)
                .map(FormDefinitionDto::new)
                .collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("fields/{idJenisDokumen}")
    public Response getFields(
            @PathParam("idJenisDokumen") Long idJenisDokumen,
            @QueryParam("version") String version
    ) {
        FormDefinition activated;

        if (version != null) {
            activated = formDefinitionService.getByVersion(idJenisDokumen, Double.valueOf(version));
        } else {

            activated = formDefinitionService.getActivated(idJenisDokumen);
        }

        List<FormFieldDto> dtos = Optional.ofNullable(activated).orElse(new FormDefinition())
                .getFormFields().stream().map(FormFieldDto::new).collect(Collectors.toList());

        return Response
                .ok(dtos)
                .build();
    }

    @GET
    @Path("fields/latest/{idJenisDokumen}")
    public Response getLatestFields(
            @PathParam("idJenisDokumen") Long idJenisDokumen
    ) {
        FormDefinition activated = formDefinitionService.getLatest(idJenisDokumen);
        List<FormFieldDto> dtos = Optional.ofNullable(activated).orElse(new FormDefinition())
                .getFormFields().stream().map(FormFieldDto::new).collect(Collectors.toList());

        return Response
                .ok(dtos)
                .build();
    }

    @GET
    @Path("fields/by-formdefinition/{idFormDefinition}")
    public Response getFieldsBySurat(
            @PathParam("idFormDefinition") Long idFormDefinition
    ) {
        FormDefinition formDefinition = formDefinitionService.find(idFormDefinition);
        List<FormFieldDto> dtos = Optional.of(formDefinition)
                .orElse(new FormDefinition())
                .getFormFields()
                .stream()
                .map(FormFieldDto::new)
                .sorted(Comparator.comparing(FormFieldDto::getSeq))
                .collect(Collectors.toList());

        return Response
                .ok(dtos)
                .build();
    }

    @POST
    @Path("save/{idJenisDokumen}")
    public Response save(
            @PathParam("idJenisDokumen") Long idJenisDokumen,
            FormDefinitionDto dto) {
        MasterJenisDokumen masterJenisDokumen = masterJenisDokumenService.find(idJenisDokumen);

        try {
            FormDefinition activated = formDefinitionService.getLatest(idJenisDokumen);
            double nextVersion = 0.1;

            if (activated != null) {
                String versionString = activated.getVersion().toString();
                Integer leftPart = Integer.valueOf(versionString.substring(0, versionString.lastIndexOf(".")));
                Integer rightPart = Integer.valueOf(versionString.substring(versionString.lastIndexOf(".") + 1));

                rightPart++;

                nextVersion = Double.valueOf(String.format("%s.%s", leftPart, rightPart));
            }

            FormDefinition formDefinition = saveOrSubmit(idJenisDokumen, dto.getFields(), nextVersion);
            notificationService.sendInfo("Info", String.format("Form '%s' berhasil disimpan", masterJenisDokumen.getNamaJenisDokumen()));

            return Response
                    .ok(formDefinition)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save Form '%s': %s", masterJenisDokumen.getNamaJenisDokumen(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("submit/{idJenisDokumen}")
    public Response submit(
            @PathParam("idJenisDokumen") Long idJenisDokumen,
            FormDefinitionDto dto) throws InternalServerErrorException {
        MasterJenisDokumen masterJenisDokumen = masterJenisDokumenService.find(idJenisDokumen);

        try {
            FormDefinition activated = formDefinitionService.getLatest(idJenisDokumen);
            double nextVersion = 1d;

            if (activated != null) {
                nextVersion = Math.floor(activated.getVersion()) + 1;
            }

            DateUtil dateUtil = new DateUtil();
            FormDefinition formDefinition = saveOrSubmit(idJenisDokumen, dto.getFields(), nextVersion, dateUtil.getDateFromISOString(dto.getActivationDate()));
            notificationService.sendInfo("Info", String.format("Form '%s' berhasil diterbitkan", masterJenisDokumen.getNamaJenisDokumen()));

            return Response
                    .ok(formDefinition)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Publish Form '%s': %s", masterJenisDokumen.getNamaJenisDokumen(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private FormDefinition saveOrSubmit(Long idJenisDokumen, List<FormFieldDto> dtos, double nextVersion) {
        return saveOrSubmit(idJenisDokumen, dtos, nextVersion, null);
    }

    private FormDefinition saveOrSubmit(Long idJenisDokumen, List<FormFieldDto> dtos, double nextVersion, Date activationDate) {
        boolean published = nextVersion % 1 == 0;

        if (published) {
            FormDefinition activated = formDefinitionService.getActivated(idJenisDokumen);

            if (activated != null) {
                activated.setIsActive(false);
                formDefinitionService.edit(activated);
            }
        }

        FormDefinition newForm = new FormDefinition();
        MasterJenisDokumen masterJenisDokumen = masterJenisDokumenService.find(idJenisDokumen);
        DateUtil dateUtil = new DateUtil();

        Date startTemp = new Date();
        if(activationDate==null){
            startTemp = dateUtil.getDateTimeStart(dateUtil.getCurrentISODate(new Date()));
        }else{
            startTemp = dateUtil.getDateTimeStart(dateUtil.getCurrentISODate(activationDate));
        }

        newForm.setStart(startTemp);
        newForm.setEnd(dateUtil.getDefValue());
        newForm.setIsActive(published);
        newForm.setName(masterJenisDokumen.getNamaJenisDokumen());
        newForm.setVersion(nextVersion);
        newForm.setJenisDokumen(masterJenisDokumen);
        newForm.setDescription(String.format("%s versi %s", newForm.getName(), newForm.getVersion()));
        formDefinitionService.create(newForm);

        Integer index = 0;
        for (FormFieldDto dto : dtos) {
            FormField formField = new FormField();
            formField.setDefinition(newForm);
            formField.setIsActive(true);
            formField.setDefaultValue(dto.getDefaultValue());
            formField.setSeq(index);
            formField.setIsRequired(dto.getRequired());
            formField.setIsDisabled(dto.getDisabled());

            MasterMetadata masterMetadata = new MasterMetadata();
            masterMetadata.setNama(dto.getName());
            masterMetadata.setLabel(dto.getLabel());
            masterMetadata.setStart(newForm.getStart());
            masterMetadata.setEnd(newForm.getEnd());
            masterMetadataService.create(masterMetadata);

            formField.setMetadata(masterMetadata);

            switch (dto.getType()) {
                case "input": {
                    switch (dto.getInputType()) {
                        case "password": {
                            formField.setType(dto.getInputType());
                            break;
                        }
                        case "text": {
                            formField.setType(dto.getInputType());
                            break;
                        }
                        case "email": {
                            formField.setType(dto.getInputType());
                            break;
                        }
                    }
                    break;
                }
                default: {
                    formField.setType(dto.getType());
                    formField.setData(dto.getOtherData());
                    break;
                }
            }

            formFieldService.create(formField);

            index++;
        }

        return newForm;
    }
}
