package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.MasterEmailContent;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterEmailContentService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterEmailContentDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-email-content")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
public class MasterEmailContentResource {
    @Inject
    private MasterEmailContentService service;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterEmailContent> all = service.getAll();
        List<MasterEmailContentDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterEmailContent::getId))
                .map(MasterEmailContentDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    public Response saveData(MasterEmailContentDto dao) {
        try {

            MasterEmailContent model = new MasterEmailContent();

            model.setType(dao.getType());
            model.setBody(dao.getBody());
            model.setSubject(dao.getSubject());
            model.setCreatedBy(dao.getCreatedBy());
            model.setCreatedDate(new Date());

            service.create(model);
            notificationService.sendInfo("Info", "Data berhasil disimpan");

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    public Response editData(@PathParam("id") Long id, MasterEmailContentDto dao) {
        try {

            MasterEmailContent model = service.find(id);

            model.setType(dao.getType());
            model.setBody(dao.getBody());
            model.setSubject(dao.getSubject());
            model.setModifiedBy(dao.getModifiedBy());
            model.setModifiedDate(new Date());

            service.edit(model);
            notificationService.sendInfo("Info", "Data berhasil diubah");

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteData(@PathParam("id") Long id) {
        try {

            MasterEmailContent model = service.find(id);

            service.remove(model);
            notificationService.sendInfo("Info", "Data berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}