package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.cmis.CMISProviderException;
import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.db.Khazanah;
import com.qtasnim.eoffice.db.KhazanahActionHistory;
import com.qtasnim.eoffice.db.MasterKlasifikasiKhazanah;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.StringUtil;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaQuery;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("khazanah")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Khazanah Resource",
        description = "WS Endpoint untuk menghandle operasi Khazanah"
)
public class KhazanahResource {

    @Inject
    private KhazanahService khazanahService;

    @Inject
    private MasterKlasifikasiKhazanahService masterKlasifikasiKhazanahService;

    @Inject
    private KhazanahActionHistoryService historyService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private FileService fileService;

    @Inject
    private NotificationService notificationService;

    @Inject
    private Logger logger;

    @GET
    @ApiOperation(
            value = "Get All",
            response = KhazanahDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Khazanah list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<Khazanah> all = khazanahService.getAll();
        List<KhazanahDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(Khazanah::getId))
                .map(KhazanahDto::new).collect(Collectors.toList());
        long count = all.count();

        khazanahService.setFiles(result);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = KhazanahDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Khazanah list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {

        List<Khazanah> data = khazanahService.getResultList(filter, sort, descending, skip, limit);
        long count = khazanahService.count(filter);

        List<KhazanahDto> result = data.stream().map(q -> new KhazanahDto(q)).collect(Collectors.toList());

        khazanahService.setFiles(result);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("list")
    @ApiOperation(
            value = "Get Data By Tipe Khazanah",
            response = KhazanahListDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Khazanah list By Tipe")
    public Response getByParentId(@DefaultValue("0") @QueryParam("skip") int skip,
                                  @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                  @QueryParam("tipe") String tipe, @QueryParam("idKlasifikasi") Long idKlasifikasi) {

        JPAJinqStream<Khazanah> all = khazanahService.getByTipeAndKlasifikasi(tipe, idKlasifikasi);
        List<KhazanahDto> result = all
                .skip(skip)
                .limit(limit)
                .map(KhazanahDto::new).collect(Collectors.toList());
        long count = all.count();

        khazanahService.setFiles(result);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("parents")
    @ApiOperation(
            value = "Get Klasifikasi By Tipe",
            response = KhazanahListDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List Klasifikasi By Tipe")
    public Response getList(@DefaultValue("0") @QueryParam("skip") int skip,
                            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                            @QueryParam("tipe") String tipe) {

        JPAJinqStream<MasterKlasifikasiKhazanah> all = masterKlasifikasiKhazanahService.getByParentName(tipe, null, "nama", false);
        List<KhazanahListDto> result = all
                .skip(skip)
                .limit(limit)
                .map(KhazanahListDto::new).collect(Collectors.toList());

        long count = all.count();

//        khazanahService.setCount(result);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("list")
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Paging",
            response = KhazanahListDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Method untuk paging Khazanah")
    public Response paging(
            @FormDataParam("tipe") String tipe,
            @FormDataParam("filter") String filter,
            @FormDataParam("pagingDto") List<KhazanahPageDto> pagingDto) throws InternalServerErrorException {
        try {
//            boolean isNumeric = StringUtils.isNumeric(filter);
//            Short year = isNumeric ? Short.valueOf(filter) : null;
//            List<KhazanahListDto> result = khazanahService.getByTipeKhazanah(tipe, filter, year, pagingDto);
            List<KhazanahListDto> result = khazanahService.getByTipeKhazanah(tipe, filter, pagingDto);

            return Response
                    .ok(result)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Save",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Save Khazanah")
    public Response saveData(
            @Valid @FormDataParam("dto") KhazanahDto dto,
            @FormDataParam("attachmentsDto") List<KhazanahAttachmentDto> attachmentsDto,
            @ApiParam(hidden = true) @FormDataParam("files") List<FormDataBodyPart> bodyParts) throws InternalServerErrorException {
        try {
            khazanahService.saveOrEdit(dto, null, attachmentsDto, bodyParts, null);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dto.getJudul()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("'%s' gagal disimpan", dto.getJudul()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Khazanah")
    public Response editData(
            @NotNull @PathParam("id") Long id,
            @Valid @FormDataParam("dto") KhazanahDto dto,
            @FormDataParam("attachmentsDto") List<KhazanahAttachmentDto> attachmentsDto,
            @ApiParam(hidden = true) @FormDataParam("files") List<FormDataBodyPart> bodyParts,
            @FormDataParam("deletedAttachmentIds") List<Long> deletedAttachmentIds) throws InternalServerErrorException {
        try {
            khazanahService.saveOrEdit(dto, id, attachmentsDto, bodyParts, deletedAttachmentIds);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dto.getJudul()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("'%s' gagal disimpan", dto.getJudul()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Khazanah")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            khazanahService.delete(id);
            notificationService.sendInfo("Info", "Data berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path(value = "upload/{idKhazanah}")
    @Secured
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Upload",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response upload(
            @PathParam("idKhazanah") Long idKhazanah,
            @FormDataParam(value = "file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value = "file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body
    ) throws IOException {
        try {
            khazanahService.uploadAttachment(idKhazanah, inputStream, fileDisposition, body);
            return Response.ok().build();
        } catch (Exception ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }

    @GET
    @Path("getDataDoc")
    @ApiOperation(
            value = "Get Khazanah Files",
            response = GlobalAttachmentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Khazanah File List")
    public Response getDocAttachment(
            @DefaultValue("") @QueryParam("idKhazanah") Long idKhazanah,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getAttachmentDataByRef("t_khazanah", idKhazanah);
        List<GlobalAttachmentDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(GlobalAttachmentDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("preview/{idDoc}")
    @ApiOperation(
            value = "Get Khazanah Dokumen Preview",
            response = GlobalAttachmentDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return SuratDocLib with Preview Link")
    public Response getDocAttachment(@PathParam("idDoc") @NotNull Long idDoc) throws CMISProviderException {
        AttachmentPreviewDto result = globalAttachmentService.getPreviewDto(idDoc);

        return Response.ok(result).build();
    }

    @GET
    @Path("downloadFile")
    @ApiOperation(
            value = "Download File",
            notes = "Return File from Alfresco")
    public Response getDownloadFile(
            @DefaultValue("") @QueryParam("idDoc") String idDoc) {
        GlobalAttachmentDto obj = globalAttachmentService.findByDocId(idDoc);
        StreamingOutput fileStream = new StreamingOutput() {
            @Override
            public void write(java.io.OutputStream output) throws IOException, WebApplicationException {
                try {
                    byte[] download = fileService.download(idDoc);
                    output.write(download);
                    output.flush();
                } catch (Exception e) {
                    throw new WebApplicationException("File Not Found !!");
                }
            }
        };
        return Response
                .ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
                .header("content-disposition", "attachment; filename = " + obj.getDocName() + "." + obj.getMimeType())
                .build();
    }

    @POST
    @Path("view")
    @ApiOperation(
            value = "Update Khazanah History",
            produces = MediaType.APPLICATION_JSON)
    public Response viewedHistory(@Valid KhazanahActionHistoryDto dao) {
        try {
            KhazanahActionHistory history =
                    historyService.getByKhazanahAndUser(dao.getKhazanahId(), dao.getUserId());
            if (history == null) {
                historyService.saveOrEdit(null, dao);
            } else {
                if (!history.getIsViewed()) {
                    historyService.setViewed(history.getId());
                }
            }

            return Response
                    .ok(new MessageDto("info", "View Khazanah berhasil."))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Unable to View '%s': %s", dao.getKhazanah().getJudul(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("download")
    @ApiOperation(
            value = "Update Khazanah History",
            produces = MediaType.APPLICATION_JSON)
    public Response downloadedHistory(@Valid KhazanahActionHistoryDto dao) {
        try {
            KhazanahActionHistory history =
                    historyService.getByKhazanahAndUser(dao.getKhazanahId(), dao.getUserId());
            if (history == null) {
                historyService.saveOrEdit(null, dao);
            } else {
                if (!history.getIsDownloaded()) {
                    historyService.setDownloaded(history.getId());
                }
            }

            return Response
                    .ok(new MessageDto("info", "Download Khazanah berhasil."))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            notificationService.sendError("Error", String.format("Unable to Download '%s': %s", dao.getKhazanah().getJudul(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("history/{id}")
    @ApiOperation(
            value = "Get Khazanah History Data",
            response = KhazanahActionHistoryDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return History Khazanah list")
    public Response getHistory(@PathParam("id") @NotNull Long idKhazanah) throws InternalServerErrorException {

        List<KhazanahActionHistoryDto> result = khazanahService.getHistoryByKhazanah(idKhazanah);
        long count = result.size();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}
