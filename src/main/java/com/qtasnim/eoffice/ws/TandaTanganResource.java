package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterUser;
import com.qtasnim.eoffice.db.TandaTangan;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterUserService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.services.TandaTanganService;
import com.qtasnim.eoffice.ws.dto.MasterUserDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import com.qtasnim.eoffice.ws.dto.TandaTanganDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Path("tandatangan")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Tanda Tangan Resource",
        description = "WS Endpoint untuk menghandle pengaturan Tanda Tangan"
)
public class TandaTanganResource {
    @Inject
    private TandaTanganService tandaTanganService;

    @Inject
    private MasterUserService masterUserService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = TandaTanganDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return TandaTangan list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<TandaTangan> all = tandaTanganService.getAll();
        List<TandaTanganDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(TandaTangan::getId))
                .map(TandaTanganDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = TandaTanganDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return TandaTangan list Filtered")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @DefaultValue("") @QueryParam("user") String user,
                                @DefaultValue("") @QueryParam("start") String start,
                                @DefaultValue("") @QueryParam("end") String end,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{

        List<TandaTanganDto> result = tandaTanganService.getResultList(filter, sort, descending, skip, limit)
                .stream()
                .map(TandaTanganDto::new)
                .collect(Collectors.toList());
        long count = tandaTanganService.count(filter);

//        HashMap<String,Object> temp = tandaTanganService.getFiltered(filter,user,start,end,skip,limit,descending,sort);
//        List<TandaTanganDto> result = (List<TandaTanganDto>) temp.get("data");
//        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Tanda tangan")
    public Response saveData(TandaTanganDto dao) {
        try {
            tandaTanganService.saveOrEdit(dao, null);
            notificationService.sendInfo("Info", "Data berhasil disimpan");

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            if(Strings.isNullOrEmpty(ex.getMessage())) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Save: %s", ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }else{
                return Response.status(Response.Status.GONE).entity(ex.getMessage()).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit TandaTangan")
    public Response editData(@PathParam("id") Long id, TandaTanganDto dao) {
        try {
            tandaTanganService.saveOrEdit(dao, id);

            notificationService.sendInfo("Info", "Data berhasil diubah");

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit: %s", ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete TandaTangan")
    public Response deleteData(@PathParam("id") Long id) {
        try {
            TandaTangan model = tandaTanganService.find(id);
            model.setIsDeleted(true);
            tandaTanganService.edit(model);
            notificationService.sendInfo("Info", "Data berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("ListUser")
    @ApiOperation(
            value = "Get User List",
            response = MasterUserDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return user list")
    public Response getUsers(@DefaultValue("0") @QueryParam("skip") int skip,
                             @DefaultValue("10") @Max(50) @QueryParam("limit") int limit){
        JPAJinqStream<MasterUser> all = masterUserService.getAllValid();
        List<MasterUserDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterUser::getId))
                .map(MasterUserDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("search")
    @ApiOperation(
            value = "Get Filtered Data",
            response = TandaTanganDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return TandaTangan list Filtered")
    public Response getSearched(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @DefaultValue("") @QueryParam("user") String user,
                                @DefaultValue("") @QueryParam("start") String start,
                                @DefaultValue("") @QueryParam("end") String end,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{

        HashMap<String,Object> temp = tandaTanganService.getFiltered(filter,user,start,end,skip,limit,descending,sort);
        List<TandaTanganDto> result = (List<TandaTanganDto>) temp.get("data");
        long count = (long) temp.get("length");

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}
