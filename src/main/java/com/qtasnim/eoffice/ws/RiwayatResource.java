package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaQuery;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("riwayat")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Riwayat Dokumen Resource",
        description = "WS Endpoint untuk menghandle operasi Riwayat Dokumen"
)
public class RiwayatResource {
    @Inject
    private RiwayatService riwayatService;
    @Inject
    private SuratService suratService;
    @Inject
    private PermohonanDokumenService permohonanDokumenService;
    @Inject
    private NgSuratService ngSuratService;
    @Inject
    private NotificationService notificationService;
    @Inject
    private ProcessTaskService processTaskService;
    @Inject
    private MasterWorkflowProviderService masterWorkflowProviderService;

    @Inject
    private Logger logger;

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = SuratDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Todo list")
    public Response getFilteredData(
            @QueryParam("idOrganisasi") Long idOrganisasi,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @DefaultValue("") @QueryParam("jenisDokumen") String jenisDokumen,
            @QueryParam("sort") String sort,
            @DefaultValue("true") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        CriteriaQuery<Surat> rawQuery = riwayatService.getAllQuery(idOrganisasi, filter, sort, descending, jenisDokumen);
        List<SuratDto> result = riwayatService.execCriteriaQuery(rawQuery, skip, limit)
                .stream().map(SuratDto::new)
                .collect(Collectors.toList());
        long count = riwayatService.countCriteriaQuery(rawQuery);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("cetak-riwayat")
    @ApiOperation(
            value = "Get Converted Pdf Dokumen & Lampiran",
            response = ConvertedPdfDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return ConvertedPdfDto[]")
    public Response getConvertedPdf(
            @DefaultValue("0") @NotNull  @QueryParam("idSurat") Long idSurat) throws InternalServerErrorException {

        try {

            List<ConvertedPdfDto> result = riwayatService.convertRiwayatDokumen(idSurat);
            return Response
                .ok(result)
//                .header("Content-Disposition", "attachment;\"" + "namafile\"")
                .build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}
