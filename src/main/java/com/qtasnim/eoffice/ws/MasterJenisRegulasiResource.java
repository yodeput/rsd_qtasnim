package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.MasterJenisRegulasi;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterJenisRegulasiService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterJenisRegulasiDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-jenis-regulasi")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Jenis Regulasi Resource",
        description = "WS Endpoint untuk menghandle operasi Jenis Regulasi"
)
public class MasterJenisRegulasiResource {
    @Inject
    private MasterJenisRegulasiService masterJenisRegulasiService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterJenisRegulasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Jenis Regulasi list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterJenisRegulasi> all = masterJenisRegulasiService.getAll();
        List<MasterJenisRegulasiDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterJenisRegulasi::getIdJenisRegulasi))
                .map(MasterJenisRegulasiDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterJenisRegulasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Jenis Regulasi list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{
        List<MasterJenisRegulasi> data = masterJenisRegulasiService.getResultList(filter, sort, descending, skip, limit);
        long count = masterJenisRegulasiService.count(filter);

        List<MasterJenisRegulasiDto> result = data.stream().map(q->new MasterJenisRegulasiDto(q)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Jenis Regulasi")
    public Response saveData(@Valid MasterJenisRegulasiDto dao) {
        try {
            masterJenisRegulasiService.saveOrEdit(dao, null);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNamaJenisRegulasi()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNamaJenisRegulasi(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Jenis Regulasi")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterJenisRegulasiDto dao) {
        try {
            masterJenisRegulasiService.saveOrEdit(dao, id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNamaJenisRegulasi()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNamaJenisRegulasi(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Jenis Regulasi")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            MasterJenisRegulasi model = masterJenisRegulasiService.find(id);
            masterJenisRegulasiService.remove(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getNamaJenisRegulasi()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
