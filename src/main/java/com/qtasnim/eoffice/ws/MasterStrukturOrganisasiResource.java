package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterNonStruktural;
import com.qtasnim.eoffice.db.MasterStrukturOrganisasi;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.FieldErrorDto;
import com.qtasnim.eoffice.ws.dto.MasterStrukturOrganisasiDto;
import com.qtasnim.eoffice.ws.dto.MasterUserDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.*;

import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;

@Path("master-struktur-organisasi")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Struktur Organisasi",
        description = "WS Endpoint untuk mendapatkan data Struktur Organisasi"
)
public class MasterStrukturOrganisasiResource {

    @Inject
    private MasterStrukturOrganisasiService masterStrukturOrganisasiService;

    @Inject
    private KontakFavoritService kontakFavoritService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Inject
    private MasterNonStrukturalService nonStrukturalService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterStrukturOrganisasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Struktur Organisasi treeList")
    public Response get(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterStrukturOrganisasi> all = masterStrukturOrganisasiService.getAllValid();
        List<MasterStrukturOrganisasiDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterStrukturOrganisasi::getIdOrganization))
                .map(MasterStrukturOrganisasiDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter")
    @ApiOperation(
            value = "Get Data Filtered",
            response = MasterStrukturOrganisasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Struktur Organisasi treeList Filtered")
    public Response getDataFiltered(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterStrukturOrganisasiDto> result = masterStrukturOrganisasiService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterStrukturOrganisasiDto::new)
                .collect(Collectors.toList());
        long count = masterStrukturOrganisasiService.count(filter);

        /* Attach Non Struktural*/
        result.forEach(a->{
            MasterNonStruktural non = nonStrukturalService.getByOrg(a.getOrganizationId());
            if(non!=null){
                if(non.getUser()!=null) {
                    a.setUser(new MasterUserDto(non.getUser()));
                }
            }
        });

        List<MasterStrukturOrganisasiDto> sortedList = new ArrayList<>();

        /*cek apakah dalam data yang di ambil ada organisasi yang bersifat posisi*/
        if(result.stream().anyMatch(a->a.getIsPosisi())){

            /*tampung organisasi yang bertipe posisi kedalam list*/
            List<MasterStrukturOrganisasiDto> posisiList = result.stream().filter(a->a.getIsPosisi().booleanValue()==true).collect(Collectors.toList());
            if(!posisiList.isEmpty()){

                /*tampung organisasi yang bertipe posisi dan chief*/
                List<MasterStrukturOrganisasiDto> chiefSort = posisiList.stream()
                        .filter(b->b.getIsChief().booleanValue()==true).sorted(Comparator.comparing(MasterStrukturOrganisasiDto::getClazz)
                                .thenComparing(MasterStrukturOrganisasiDto::getGrade)
                                .thenComparing(MasterStrukturOrganisasiDto::getOrganizationName))
                        .collect(Collectors.toList());

                /*add chief kedalam list hasil*/
                for (MasterStrukturOrganisasiDto cs:chiefSort){
                    sortedList.add(cs);

                    /*hapus data chief dari data pencarian*/
                    result.removeIf(c->c.getOrganizationId().equals(cs.getOrganizationId()));
                    posisiList.removeIf(d->d.getOrganizationId().equals(cs.getOrganizationId()));
                }

                /*ketika posisi belum semua dimasukkan kedalam list hasil*/
                if(!posisiList.isEmpty()){
                    List<MasterStrukturOrganisasiDto> posisiSort = posisiList.stream().filter(q->q.getClazz()!=null).sorted(Comparator
                            .comparing(MasterStrukturOrganisasiDto::getGrade)
                            .thenComparing(MasterStrukturOrganisasiDto::getClazz).thenComparing(MasterStrukturOrganisasiDto::getOrganizationName)
                    ).collect(Collectors.toList());
                    for(MasterStrukturOrganisasiDto ps:posisiSort){
                        sortedList.add(ps);
                        result.removeIf(e->e.getOrganizationId().equals(ps.getOrganizationId()));
                        posisiList.removeIf(e->e.getOrganizationId().equals(ps.getOrganizationId()));
                    }

                    if(!posisiList.isEmpty()){
                        List<MasterStrukturOrganisasiDto> posisiSort2 = posisiList.stream().sorted(Comparator
                                .comparing(MasterStrukturOrganisasiDto::getOrganizationCode)).collect(Collectors.toList());
                        for(MasterStrukturOrganisasiDto ps:posisiSort2){
                            sortedList.add(ps);
                            result.removeIf(e->e.getOrganizationId().equals(ps.getOrganizationId()));
                            posisiList.removeIf(e->e.getOrganizationId().equals(ps.getOrganizationId()));
                        }
                    }
                }

                if(!result.isEmpty()){
                    List<MasterStrukturOrganisasiDto> exp = new ArrayList<>();
                    List<String> tmpName = result.stream().filter(a->a.getIsPosisi().booleanValue() == false).map(m->m.getOrganizationName()).collect(Collectors.toList());
                    List<MasterStrukturOrganisasiDto>sameName = new ArrayList<>();
                    String delName;
                    for(String nama:tmpName){
                        int size = result.stream().filter(f->f.getIsPosisi().booleanValue() == false &&
                                f.getOrganizationName().equals(nama)).collect(Collectors.toList()).size();
                        if(size>1){
                            sameName = result.stream().filter(f->f.getIsPosisi().booleanValue() == false &&
                                    f.getOrganizationName().equals(nama)).collect(Collectors.toList());
                        }
                    }

                    if(!sameName.isEmpty()){
                        exp = result.stream().filter(d->d.getIsPosisi().booleanValue()==false).sorted(Comparator.comparing
                                (MasterStrukturOrganisasiDto::getSingkatan)).collect(Collectors.toList());
                        int j=0;
                        sameName = sameName.stream().sorted(Comparator.comparing(
                                MasterStrukturOrganisasiDto::getOrganizationCode)).collect(Collectors.toList());

                        for(int i = 0;i< exp.size();i++){
                            if(exp.get(i).getOrganizationName().equals(sameName.get(j).getOrganizationName())){
                               exp.set(i,sameName.get(j));
                               j++;
                            }
                        }

                    }else{
                        exp = result.stream().filter(d->d.getIsPosisi().booleanValue()==false).collect(Collectors.toList());
                    }
                    sortedList.addAll(exp);
                }
            }
        }else{
           sortedList.addAll(result);
        }

        getStarredContact(sortedList);

        return Response
                .ok(sortedList)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter2")
    @ApiOperation(
            value = "Get Data Filtered",
            response = MasterStrukturOrganisasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Struktur Organisasi treeList Filtered")
    public Response getDataFiltered2(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterStrukturOrganisasiDto> result = masterStrukturOrganisasiService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterStrukturOrganisasiDto::new)
                .collect(Collectors.toList());
        long count = masterStrukturOrganisasiService.count(filter);


        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/parent")
    @ApiOperation(
            value = "Get Data By Parent",
            response = MasterStrukturOrganisasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Struktur Organisasi treeList By Parent")
    public Response getDataByParent(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @QueryParam("idParent") Long idParent,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {
        JPAJinqStream<MasterStrukturOrganisasi> all = masterStrukturOrganisasiService.getByParent(idParent, sort, descending);
        List<MasterStrukturOrganisasiDto> result = all
                .skip(skip)
                .limit(limit)
                .map(MasterStrukturOrganisasiDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/organizationList")
    @ApiOperation(
        value = "Get List Organisasi By List IdUser",
        response = MasterStrukturOrganisasiDto[].class,
        produces = MediaType.APPLICATION_JSON,
        notes = "Return List Organisasi By List IdUser")
    public Response getOrganisasiById(@DefaultValue("") @QueryParam("userIds") String userIds) {
        String[] splited = userIds.split(",");
        List<Long> parsed = Arrays.stream(splited).map(Long::parseLong).collect(Collectors.toList());
        List<MasterStrukturOrganisasiDto> list = new ArrayList<>();
        parsed.forEach(id -> {
            MasterStrukturOrganisasiDto dto = masterStrukturOrganisasiService.getByUserId(id);
            if (dto != null) list.add(dto);
        });
        return Response
                .ok(list)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/organization/{idUser}")
    @ApiOperation(
            value = "Get Data By Organization Id",
            response = MasterStrukturOrganisasiDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return User By Organization Id")
    public Response getUser(@PathParam("idUser") Long idUser) {
        MasterStrukturOrganisasiDto user = masterStrukturOrganisasiService.getByUserId(idUser);
        return Response
                .ok(user)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/get-organisasi/{id}")
    @ApiOperation(
            value = "Get Data By Organization Id",
            response = MasterStrukturOrganisasiDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Organisasi By Organization Id")
    public Response getOrganisasi(@PathParam("id") Long id) {
        MasterStrukturOrganisasiDto org = new MasterStrukturOrganisasiDto(masterStrukturOrganisasiService.getByIdOrganisasi(id));
        return Response.ok(org).build();
    }

    @POST
    public Response saveDivision(@Valid MasterStrukturOrganisasiDto dao) {
        try {
            masterStrukturOrganisasiService.save(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getOrganizationName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getOrganizationName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    public Response editDivision(@NotNull @PathParam("id") Long id, @Valid MasterStrukturOrganisasiDto dao) {
        try {
            masterStrukturOrganisasiService.save(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getOrganizationName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getOrganizationName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteDivision(@NotNull @PathParam("id") Long id) {

        try {
            MasterStrukturOrganisasi deleted = masterStrukturOrganisasiService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getOrganizationName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private void getStarredContact (List<MasterStrukturOrganisasiDto> result) {
        result.forEach(q -> {
            boolean isFavorit = kontakFavoritService.getIsFavoritBy(q.getOrganizationId());
            q.setIsFavorite(isFavorit);
        });
    }

    private MessageDto prepareMessage(String field) {
        String msg;
        switch (field) {
            case "nama":
                msg = "Nama";
                break;
            case "kode":
                msg = "Kode";
                break;
            default:
                msg = "Nama dan Kode";
                break;
        }
        MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();

        FieldErrorDto fieldErrorDto = new FieldErrorDto();
        fieldErrorDto.setField(field);
        fieldErrorDto.setMessage(msg + " Organisasi sudah di gunakan.");
        fieldErrorDtos.add(fieldErrorDto);

        messageDto.setFields(fieldErrorDtos);

        return messageDto;
    }
}
