package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.*;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.security.ISessionContext;
import com.qtasnim.eoffice.services.MasterDelegasiService;
import com.qtasnim.eoffice.services.ProcessInstanceService;
import com.qtasnim.eoffice.services.ProcessTaskService;
import com.qtasnim.eoffice.services.SuratService;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.DokumenDto;
import com.qtasnim.eoffice.ws.dto.PengirimDto;
import com.qtasnim.eoffice.ws.dto.ProcessTaskDto;
import com.qtasnim.eoffice.ws.dto.TaskListDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("beranda")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Dashboard Resource",
        description = "WS Endpoint untuk menghandle operasi Dashboard"
)
public class BerandaResource {

    @Inject
    @ISessionContext
    private Session userSession;

    @Inject
    private MasterDelegasiService delegasiService;

    @Inject
    private ProcessTaskService processTaskService;

    @Inject
    private ProcessInstanceService instanceService;

    @Inject
    private SuratService suratService;

    @GET
    @Path("task")
    @ApiOperation(
            value = "Get Filtered Data",
            response = ProcessTask[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return list peminjaman Dokumen Filtered")
    public Response getTask(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        Long idOrg = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();


        JPAJinqStream<ProcessTask> all = processTaskService.getPendingTaskByOrgId(idOrg);

//        List<Long> ids = processTasks.stream().map(q -> new Long(q.getProcessInstance().getRecordRefId())).collect(Collectors.toList());

//        if (ids.isEmpty()) {
//            return Response.ok(new ArrayList<>())
//                    .header("X-Total-Count", count)
//                    .header("Access-Control-Expose-Headers", "X-Total-Count").build();
//        }

        List<ProcessTaskDto> result =all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(ProcessTask::getId))
                .map(ProcessTaskDto::new).collect(Collectors.toList());

        long count = all.count();

        return Response.ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count").build();
    }


    @GET
    @Path("tasklist")
    @ApiOperation(
            value = "Get Filtered Data",
            response = ProcessTask[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Task List")
    public Response getTaskList(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        List<Long> idOrg = new ArrayList<>();
        idOrg.add(userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization());
        List<MasterDelegasi> delegasiList = delegasiService.getByToNoTipe();
        for(MasterDelegasi delegasi:delegasiList){
            idOrg.add(delegasi.getFrom().getIdOrganization());
        }

        JPAJinqStream<ProcessTask> all=null;
        for(Long i:idOrg){
            all = (processTaskService.getPendingTaskByOrgId(i));
        }

        List<TaskListDto> result = all.skip(skip).limit(limit)
                .sorted(Comparator.comparing(ProcessTask::getAssignedDate).reversed())
                .map(q->{

                    TaskListDto obj = new TaskListDto();
                    Long idSurat = Long.parseLong(q.getProcessInstance().getRecordRefId());
                    Surat surat = suratService.find(idSurat);
                    obj.setNoDokumen(surat.getNoDokumen());
                    FormValue fv = surat.getFormValue().stream().filter(b->b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                            .findFirst().orElse(null);
                    if(fv!=null) {
                        obj.setPerihal(fv.getValue());
                    }else{
                        obj.setPerihal(null);
                    }
                    List<PengirimDto> pObj = surat.getPenandatanganSurat().stream().map(b -> {
                        PengirimDto pengirim = new PengirimDto();
                        pengirim.setNipp(b.getOrganization().getUser().getEmployeeId());
                        pengirim.setJabatan(b.getOrganization().getOrganizationName());
                        pengirim.setNama(b.getOrganization().getUser().getNameFront()+" "+b.getOrganization().getUser().getNameMiddleLast());
                        return pengirim;
                    }).collect(Collectors.toList());
                    obj.setPengirim(pObj);
                    obj.setAksi(q.getTaskName());
                    //obj.setUnitPengirim(surat.getPenandatanganSurat().stream().map(a->a.getUser().getKorsa().getNama()).collect(Collectors.toList()));
                    DateUtil dateUtil = new DateUtil();
                    if(q.getAssignedDate()!=null){
                        obj.setTglDiterima(dateUtil.getCurrentISODate(q.getAssignedDate()));
                    }
                    if(surat.getSubmittedDate()!=null) {
                        obj.setTglDikirim(dateUtil.getCurrentISODate(surat.getSubmittedDate()));
                    }

                    switch (q.getProcessInstance().getRelatedEntity()) {

                        case "com.qtasnim.eoffice.db.Surat": {

                            MasterJenisDokumen jenisDokumen = surat.getFormDefinition().getJenisDokumen();
                            String jenisDok = jenisDokumen.getKodeJenisDokumen()+" "+ jenisDokumen.getNamaJenisDokumen();

                            switch (jenisDokumen.getKodeJenisDokumen()) {

                                case "ND": {

                                    String path = "api/surat/getMetadataSurat?idSurat=" + idSurat;
                                    obj.setTaskClassification("KORESPONDENSI");
                                    obj.setJenisDokumen(jenisDok);
                                    obj.setPath(path);
                                    break;
                                    
                                }

                                case "SME": {

                                    String path = "api/surat/getMetadataSurat?idSurat=" + idSurat;
                                    obj.setTaskClassification("DOKUMEN_MASUK");
                                    obj.setJenisDokumen(jenisDok);
                                    obj.setPath(path);
                                    break;
                                    
                                }

                            }
                            
                        }

                        // case "com.qtasnim.eoffice.db.PermohonanDokumen": {

                        //     obj.setTaskClassification("LAYANAN_DOKUMEN");
                        //     obj.setJenisDokumen(jenisDok);
                        //     obj.setPath(path);

                        // }

                    }
                    
                    obj.setId(idSurat);

                    return obj;

                }).collect(Collectors.toList());


        long count = all.count();

        return Response.ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count").build();
    }


    @GET
    @Path("perluPersetujuan")
    @ApiOperation(
            value = "Get List Draft Perlu Perstujuan",
            response = ProcessTask[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List Draft Perlu Perstujuan")
    public Response getPerluPersetujuan(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        Long idOrg = userSession.getUserSession().getUser().getOrganizationEntity().getIdOrganization();


        JPAJinqStream<ProcessTask> all = processTaskService.getPendingTaskByOrgId(idOrg);

        List<DokumenDto> result =all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(ProcessTask::getId))
                .map(q-> {
                    DokumenDto obj = new DokumenDto();
                    ProcessInstance pi = q.getProcessInstance();
                    Surat surat = pi.getSurat();
                    DateUtil dateUtil = new DateUtil();
                    obj.setId(q.getId());
                    obj.setTanggal(dateUtil.getCurrentISODate(surat.getCreatedDate()));
                    FormValue fv = surat.getFormValue().stream().filter(b->b.getFormField().getMetadata().getNama().toLowerCase().contains("perihal"))
                            .findFirst().orElse(null);
                    if(fv!=null){
                        obj.setPerihal(fv.getValue());
                    }
                    List<PengirimDto> pObj = surat.getPenandatanganSurat().stream().map(b -> {
                        PengirimDto pengirim = new PengirimDto();
                        pengirim.setJabatan(b.getOrganization().getOrganizationName());
                        pengirim.setNama(b.getOrganization().getUser().getNameFront()+" "+b.getOrganization().getUser().getNameMiddleLast());
                        return pengirim;
                    }).collect(Collectors.toList());
                    obj.setPengirim(pObj);
//                    obj.setPengirim(surat.getPenandatanganSurat().stream().map(b->b.getOrganization().getOrganizationName()).collect(Collectors.toList()));
                    return obj;
                }).collect(Collectors.toList());

        long count = all.count();

        return Response.ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count").build();
    }
}
