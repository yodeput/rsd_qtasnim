package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.GlobalAttachment;
import com.qtasnim.eoffice.db.Regulasi;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.util.ExceptionUtil;
import com.qtasnim.eoffice.ws.dto.*;
import com.qtasnim.eoffice.ws.dto.RegulasiDto;
import io.swagger.annotations.*;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("regulasi")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Regulasi Resource",
        description = "WS Endpoint untuk menghandle operasi Regulasi"
)
public class RegulasiResource {
    @Inject
    private RegulasiService regulasiService;

    @Inject
    private FileService fileService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @Inject
    private GlobalAttachmentService globalAttachmentService;
    
    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Data",
            response = RegulasiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Regulasi list")
    public Response getFilter(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {
        List<RegulasiDto> result = regulasiService.getResultList(filter, sort, descending, skip, limit)
                .stream()
                .map(RegulasiDto::new).collect(Collectors.toList());
        long count = regulasiService.count(filter);

        return Response
            .ok(result)
            .header("X-Total-Count", count)
            .header("Access-Control-Expose-Headers", "X-Total-Count")
            .build();
    }

//    @POST
//    @ApiOperation(
//            value = "Add",
//            produces = MediaType.APPLICATION_JSON,
//            notes = "Add Regulasi")
//    public Response saveRegulasi( RegulasiDto dao) {
//        try{
//            regulasiService.save(null, dao);
//            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNamaRegulasi()));
//
//            return Response
//                    .ok(new MessageDto("info", "Data berhasil disimpan"))
//                    .status(Response.Status.OK)
//                    .build();
//        } catch (Exception ex) {
//            logger.error(null, ex);
//            //notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNama(), ex.getMessage()));
//            notificationService.sendError("Error", String.format("'%s' gagal disimpan", dao.getNamaRegulasi()));
//            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
//        }
//    }

//    **************************** ADD + FILE *****************************
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Add",
    response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Regulasi")
    public Response saveRegulasi(@Valid @FormDataParam("dto") RegulasiDto dao,
    @FormDataParam("file") List<FormDataBodyPart> body) {
        try{
            regulasiService.saveOrEdit(null, dao,  body, null);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNamaRegulasi()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNama(), ex.getMessage()));
            notificationService.sendError("Error", String.format("'%s' gagal disimpan", dao.getNamaRegulasi()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    //    **************************** EDIT + FILE *****************************
    @POST
    @Path("{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Edit",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Edit Regulasi")
    public Response editReport(@NotNull @PathParam("id") Long id, @Valid @FormDataParam("dto") RegulasiDto dao,
                               @FormDataParam("file") List<FormDataBodyPart> body, @FormDataParam("deletedFilesId") List<String> deletedFilesId) {

        try {
//            service.save(id, dao);
//            service.uploadAttachment(id, body);
//            service.deleteAttachment(id, deletedFilesId);

            regulasiService.saveOrEdit(id, dao, body, deletedFilesId);
            notificationService.sendInfo("Info", "Laporan berhasil diubah");
            return Response.ok().status(Response.Status.OK).build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", "Laporan gagal diubah");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    
//    @POST
//    @Path("{id}")
//    @ApiOperation(
//            value = "Edit",
//            produces = MediaType.APPLICATION_JSON,
//            notes = "Edit Regulasi")
//    public Response editRegulasi(@NotNull @PathParam("id") Long id, @Valid RegulasiDto dao) {
//
//        try{
//            regulasiService.save(id, dao);
//            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNamaRegulasi()));
//
//            return Response
//                    .ok(new MessageDto("info", "Data berhasil diubah"))
//                    .status(Response.Status.OK)
//                    .build();
//        } catch (Exception ex) {
//            logger.error(null, ex);
//            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNamaRegulasi(), ex.getMessage()));
//            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
//        }
//    }

    
    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Regulasi")
    public Response deleteRegulasi(@NotNull @PathParam("id") Long id) {

        try{
            Regulasi deleted = regulasiService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getNamaRegulasi()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path(value = "file/upload/{idRegulasi}")
    @Secured
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Upload File Regulasi",
            produces = MediaType.APPLICATION_JSON)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", required = true, dataType = "java.io.File", paramType = "form")
    })
    public Response upload(
            @PathParam("idRegulasi") Long idRegulasi,
            @FormDataParam(value = "file") InputStream inputStream,
            @ApiParam(hidden = true) @FormDataParam(value = "file") FormDataContentDisposition fileDisposition,
            @FormDataParam("file") FormDataBodyPart body
    ) {
        try {
            regulasiService.uploadAttachment(idRegulasi, inputStream, fileDisposition, body);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil diunggah", fileDisposition.getFileName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil diunggah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Gagal diunggah '%s': %s", fileDisposition.getFileName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("file/get/{idRegulasi}")
    @ApiOperation(
            value = "Get Files Regulasi",
            response = GlobalAttachmentDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Arsip File List")
    public Response getArsipFile(
            @PathParam("idRegulasi") Long idRegulasi,
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {
        JPAJinqStream<GlobalAttachment> all = globalAttachmentService.getAllDataByRef("t_regulasi", idRegulasi);
        List<LampiranDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(GlobalAttachment::getId))
                .map(q -> {
                    LampiranDto obj = new LampiranDto();
                    DateUtil dateUtil = new DateUtil();
                    obj.setId(q.getDocId());
                    obj.setNamaFile(q.getDocName());
                    obj.setUkuran(q.getSize());
                    obj.setTipe(q.getMimeType());
                    obj.setWaktu(dateUtil.getCurrentISODate(q.getCreatedDate()));
                    obj.setPengunggah(q.getCreatedBy());
                    obj.setDownloadLink(fileService.getViewLink(q.getDocId(), false));

                    return obj;
                }).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @DELETE
    @Path("file/delete/{idFile}")
    @ApiOperation(
            value = "Delete File Regulasi",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON)
    public Response deleteFile(
            @NotNull @PathParam("idFile") Long idFile) {
        try {
            GlobalAttachment deleted = regulasiService.deleteFile(idFile);
            notificationService.sendInfo("Info", String.format("File '%s' berhasil dihapus", deleted.getDocName()));

            return Response
                    .ok(new MessageDto("info", "File berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete File: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("File gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
//    DELETE ATTACHMENT DARI SURAT RESOURCE ************************
    @DELETE
    @Path("deleteAttachment/{idRegulasi}")
    public Response deleteAttachment(
            @PathParam("idRegulasi") long idRegulasi,
            @QueryParam("docId") String docId
    ) {
        try {
            regulasiService.deleteAttachment(idRegulasi, docId);
            notificationService.sendInfo("Info", "Lampiran berhasil dihapus");

            return Response
                    .ok(new MessageDto("info", "Lampiran berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Lampiran gagal dihapus: %s", ExceptionUtil.getRealCause(ex).getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

//    IS STARRRED DARI SURAT INTERACTION HISTORY ************************
    @POST
    @Path("setStar/{idRegulasi}")
    @ApiOperation(
            value = "Set as Stared",
            produces = MediaType.APPLICATION_JSON,
            notes = "Set as Stared")
    public Response setStared(
            @NotNull @PathParam("idRegulasi") Long idRegulasi,
            @DefaultValue(value = "true")
            @QueryParam("value") Boolean value) {

        String action = "Set Starred Flag";

        if (!value) {
            action = "Remove Starred Flag";
        }

        try{
            regulasiService.setStared(idRegulasi,value);
            notificationService.sendInfo("Info", String.format("%s Succeed", action));
            return Response.status(Response.Status.OK).build();
        }catch (Exception ex){
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("%s Failed: %s", action, ex.getMessage()));
            return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
    }
}