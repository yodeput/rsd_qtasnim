package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterKlasifikasiMasalah;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterKlasifikasiMasalahService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.util.DateUtil;
import com.qtasnim.eoffice.ws.dto.FieldErrorDto;
import com.qtasnim.eoffice.ws.dto.MasterKlasifikasiMasalahDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang.StringUtils;

@Path("master-klasifikasi-masalah")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Klasifikasi Masalah Resource",
        description = "WS Endpoint untuk menghandle operasi Klasifikasi Masalah"
)
public class MasterKlasifikasiMasalahResource {

    @Inject
    private MasterKlasifikasiMasalahService masterKlasifikasiMasalahService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterKlasifikasiMasalahDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Klasifikasi Masalah list")
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterKlasifikasiMasalah> all = masterKlasifikasiMasalahService.getAll();
        List<MasterKlasifikasiMasalahDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterKlasifikasiMasalah::getIdKlasifikasiMasalah))
                .map(MasterKlasifikasiMasalahDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterKlasifikasiMasalahDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Klasifikasi Masalah list")
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterKlasifikasiMasalah> data = masterKlasifikasiMasalahService.getResultList(filter, sort, descending, skip, limit);
        long count = masterKlasifikasiMasalahService.count(filter);

        List<MasterKlasifikasiMasalahDto> result = data.stream().map(q -> {
            MasterKlasifikasiMasalahDto rs = new MasterKlasifikasiMasalahDto(q);
            boolean noChild = masterKlasifikasiMasalahService.checkChilds(q.getIdKlasifikasiMasalah());
            rs.setHasChild(noChild);
            return rs;
        }).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("parent")
    @ApiOperation(
            value = "Get Data By Parent",
            response = MasterKlasifikasiMasalahDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Klasifikasi Masalah list By Parent")
    public Response getByParentId(@DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @QueryParam("idParent") Long idParent,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) {

        JPAJinqStream<MasterKlasifikasiMasalah> all = masterKlasifikasiMasalahService.getByParentId(idParent, sort, descending);
        List<MasterKlasifikasiMasalahDto> result = all
                .skip(skip)
                .limit(limit)
                .map(MasterKlasifikasiMasalahDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Klasifikasi Masalah")
    public Response saveData(@Valid MasterKlasifikasiMasalahDto dto) throws InternalServerErrorException {

        List<MasterKlasifikasiMasalah> list = masterKlasifikasiMasalahService.findAll().stream().filter(q->q.getCompanyCode().getId().equals(dto.getCompanyId())).collect(Collectors.toList());
        String hasil = "";
        if(!list.isEmpty()) {
            hasil = this.validate(null, dto.getNamaKlasifikasiMasalah(), dto.getKodeKlasifikasiMasalah(), dto.getParent(), list);
        }

        if (!StringUtils.isEmpty(hasil)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {

            try {
                masterKlasifikasiMasalahService.saveOrEdit(dto, null);
                notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dto.getNamaKlasifikasiMasalah()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil disimpan"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dto.getNamaKlasifikasiMasalah(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Klasifikasi Masalah")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterKlasifikasiMasalahDto dto) throws InternalServerErrorException {

        List<MasterKlasifikasiMasalah> list = masterKlasifikasiMasalahService.findAll().stream().filter(q->q.getCompanyCode().getId().equals(dto.getCompanyId())).collect(Collectors.toList());
        String hasil="";
        if(!list.isEmpty()) {
            hasil = this.validate(id, dto.getNamaKlasifikasiMasalah(), dto.getKodeKlasifikasiMasalah(), dto.getParent(), list);
        }

        if (!StringUtils.isEmpty(hasil)) {
            return Response.status(Response.Status.BAD_REQUEST).entity(prepareMessage(hasil)).build();
        } else {

            try {
                masterKlasifikasiMasalahService.saveOrEdit(dto, id);
                notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dto.getNamaKlasifikasiMasalah()));

                return Response
                        .ok(new MessageDto("info", "Data berhasil diubah"))
                        .status(Response.Status.OK)
                        .build();
            } catch (Exception ex) {
                logger.error(null, ex);
                notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dto.getNamaKlasifikasiMasalah(), ex.getMessage()));
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            response = MessageDto.class,
            notes = "Delete Klasifikasi Masalah")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            MasterKlasifikasiMasalah model = masterKlasifikasiMasalahService.find(id);
            masterKlasifikasiMasalahService.deleteData(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getNamaKlasifikasiMasalah()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            if (Strings.isNullOrEmpty(ex.getMessage())) {
                logger.error(null, ex);
                //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
                notificationService.sendError("Error", String.format("Data gagal dihapus"));

                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } else {
                logger.error(null, ex);
                //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
                notificationService.sendError("Error", String.format("Data gagal dihapus"));

                return Response.status(Response.Status.GONE).entity(new MessageDto("Error", ex.getMessage())).build();
            }
        }
    }

    private String validate(Long id, String nama, String kode, MasterKlasifikasiMasalahDto parent, List<MasterKlasifikasiMasalah> list) {
        List<MasterKlasifikasiMasalah> dataList;
        
        if (parent == null) {
            dataList = list.stream().filter(t -> t.getParent() == null).collect(Collectors.toList());
        } else {
            dataList = list.stream().filter(t -> t.getParent() != null && Objects.equals(t.getParent().getIdKlasifikasiMasalah(), parent.getIdKlasifikasiMasalah())).collect(Collectors.toList());
        }
        
        if (id != null) {
            dataList = dataList.stream().filter(q -> !Objects.equals(q.getIdKlasifikasiMasalah(), id)).collect(Collectors.toList());
        }
        
        if (dataList != null) {
            boolean notValidNama = dataList.stream().anyMatch(t -> nama.equals(t.getNamaKlasifikasiMasalah()));
            boolean notValidKode = dataList.stream().anyMatch(t -> kode.equals(t.getKodeKlasifikasiMasalah()));
            boolean notValidAll = dataList.stream().anyMatch(t -> nama.equals(t.getNamaKlasifikasiMasalah()) && kode.equals(t.getKodeKlasifikasiMasalah()));
            return notValidAll ? "all" : notValidKode ? "kode" : notValidNama ? "nama" : "" ;
        }
        
        return "";
    }

    private MessageDto prepareMessage(String field) {
        String msg;
        switch (field) {
            case "nama":
                msg = "Nama";
                break;
            case "kode":
                msg = "Kode";
                break;
            default:
                msg = "Nama dan Kode";
                break;
        }
        MessageDto messageDto = new MessageDto("error", "Constraint validation failed");
        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>();

        FieldErrorDto fieldErrorDto = new FieldErrorDto();
        fieldErrorDto.setField(field);
        fieldErrorDto.setMessage(msg + " Klasifikasi Dokumen sudah di gunakan.");
        fieldErrorDtos.add(fieldErrorDto);

        messageDto.setFields(fieldErrorDtos);

        return messageDto;
    }
}
