package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterKontakHelpdesk;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterKontakHelpdeskService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterKontakHelpdeskDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-kontak-helpdesk")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Kontak Helpdesk Resource",
        description = "WS Endpoint untuk menghandle operasi Master Kontak Helpdesk"
)
public class MasterKontakHelpdeskResource {
    @Inject
    private MasterKontakHelpdeskService kontakHelpdeskService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = MasterKontakHelpdeskDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Tentang Kami list")
    public Response getData(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit) throws InternalServerErrorException {
        JPAJinqStream<MasterKontakHelpdesk> all = kontakHelpdeskService.getAll();
        List<MasterKontakHelpdeskDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterKontakHelpdesk::getId))
                .map(MasterKontakHelpdeskDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter")
    @ApiOperation(
            value = "Get Data",
            response = MasterKontakHelpdeskDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Tentang Kami list Filtered")
    public Response getFiltered(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterKontakHelpdeskDto> result = kontakHelpdeskService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterKontakHelpdeskDto::new)
                .collect(Collectors.toList());
        long count = kontakHelpdeskService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Tentang Kami")
    public Response saveData(@Valid MasterKontakHelpdeskDto dao) {

        try {
            kontakHelpdeskService.saveOrEdit(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getToka()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getToka(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Tindakan")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterKontakHelpdeskDto dao) {
        try {
            kontakHelpdeskService.saveOrEdit(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getToka()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getToka(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Tentang Kami")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            MasterKontakHelpdesk deleted = kontakHelpdeskService.deleteData(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getToka()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}