package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.db.MasterLokasiKerja;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterLokasiKerjaService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterLokasiKerjaDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-lokasi-kerja")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Lokasi Kerja Resource",
        description = "WS Endpoint untuk menghandle CRUD Lokasi Kerja"
)
public class MasterLokasiKerjaResource {
    @Inject
    private MasterLokasiKerjaService service;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @GET
    @ApiOperation(
            value = "Get All",
            response = MasterLokasiKerjaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List Lokasi Kerja"
    )
    public Response getAll(@DefaultValue("0") @QueryParam("skip") int skip,
                           @DefaultValue("10") @QueryParam("limit") int limit) {

        JPAJinqStream<MasterLokasiKerja> all = service.getAll();
        List<MasterLokasiKerjaDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterLokasiKerja::getIdLoker))
                .map(MasterLokasiKerjaDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered",
            response = MasterLokasiKerjaDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return List Lokasi Kerja yang telah di-filter"
    )
    public Response getFiltered(@DefaultValue("0") @QueryParam("skip") int skip,
                                @DefaultValue("10") @QueryParam("limit") int limit,
                                @DefaultValue("") @QueryParam("filter") String filter,
                                @QueryParam("sort") String sort,
                                @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException{
        List<MasterLokasiKerja> data = service.getResultList(filter, sort, descending, skip, limit);
        long count = service.count(filter);

        List<MasterLokasiKerjaDto> result = data.stream().map(q->new MasterLokasiKerjaDto(q)).collect(Collectors.toList());

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Save",
            response = MessageDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Save Lokasi Kerja Baru")
    public Response saveData(@Valid MasterLokasiKerjaDto dao) {
        try{
            service.saveOrEdit(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getNamaLoker()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getNamaLoker(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Lokasi Kerja Terpilih")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterLokasiKerjaDto dao) {
        try{
            service.saveOrEdit(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getNamaLoker()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getNamaLoker(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Lokasi Kerja Terpilih")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try{
            MasterLokasiKerja model = service.find(id);
            service.remove(model);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", model.getNamaLoker()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
