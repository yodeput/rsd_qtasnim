package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.CompanyCode;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.CompanyCodeService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.CompanyCodeDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("company-code")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Company Code Resource",
        description = "WS Endpoint untuk menghandle operasi Company Code"
)
public class CompanyCodeResource {
    @Inject
    private CompanyCodeService companyCodeService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data Area",
            response = CompanyCode[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Area list")
    public Response getArea(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<CompanyCode> all = companyCodeService.getAll();
        List<CompanyCodeDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(CompanyCode::getId))
                .map(CompanyCodeDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }


    @GET
    @Path("filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = CompanyCode[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Division list")
    public Response getFilteredDivision(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<CompanyCodeDto> result = companyCodeService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(CompanyCodeDto::new).collect(Collectors.toList());
        long count = companyCodeService.count(filter);

        return Response
            .ok(result)
            .header("X-Total-Count", count)
            .header("Access-Control-Expose-Headers", "X-Total-Count")
            .build();
    }

    @GET
    @Path("getCompanyCodeByUrl/{url}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Company Code")
    public Response getCompanyCodeByUrl(@NotNull @PathParam("url") String url) {

        List<CompanyCodeDto> result = companyCodeService.getCompanyCodeByUrl(url).map(CompanyCodeDto::new).collect(Collectors.toList());
        return Response
                .ok(result)
                .build();
    }


    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Company Code")
    public Response saveDivision(@Valid CompanyCodeDto dao) {

        try{
            companyCodeService.save(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getName(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Company Code")
    public Response editDivision(@NotNull @PathParam("id") Long id, @Valid CompanyCodeDto dao) {

        try{
            companyCodeService.save(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
//notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getName(), ex.getMessage()));
            notificationService.sendError("Error", String.format("'%s' gagal diubah", dao.getName()));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Company Code")
    public Response deleteDivision(@NotNull @PathParam("id") Long id) {

        try{
            CompanyCode deleted = companyCodeService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getName()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            //notificationService.sendError("Error", String.format("Unable to delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}