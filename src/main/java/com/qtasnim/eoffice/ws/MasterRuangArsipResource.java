package com.qtasnim.eoffice.ws;

import com.google.common.base.Strings;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterRuangArsip;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.*;
import com.qtasnim.eoffice.ws.dto.GlobalAttachmentDto;
import com.qtasnim.eoffice.ws.dto.MasterRuangArsipDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jinq.jpa.JPAJinqStream;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-lokasi-arsip")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Lokasi Arsip Resource",
        description = "WS Endpoint untuk menghandle operasi Lokasi Arsip"
)
public class MasterRuangArsipResource {
    @Inject
    private MasterRuangArsipService masterRuangArsipService;

    @Inject
    private GlobalAttachmentService globalAttachmentService;

    @Inject
    private FileService fileService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @Path("ruang")
    @ApiOperation(
            value = "Get Data RuangArsip",
            response = MasterRuangArsip[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return RuangArsip list")
    public Response getRuangArsip(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("sort") String sort) throws InternalServerErrorException {
        JPAJinqStream<MasterRuangArsip> all = masterRuangArsipService.getAll();
        List<MasterRuangArsipDto> result = all
                .skip(skip)
                .limit(limit)
                .sorted(Comparator.comparing(MasterRuangArsip::getId))
                .map(MasterRuangArsipDto::new).collect(Collectors.toList());
        long count = all.count();
        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("ruang/filter")
    @ApiOperation(
            value = "Get Filtered Data",
            response = MasterRuangArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return RuangArsip list")
    public Response getFilteredRuangArsip(
            @DefaultValue("0") @QueryParam("skip") int skip,
            @DefaultValue("10") @Max(50) @QueryParam("limit") int limit,
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterRuangArsipDto> result = masterRuangArsipService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterRuangArsipDto::new).collect(Collectors.toList());
        long count = masterRuangArsipService.count(filter);

        result.forEach(a->{
            if(!Strings.isNullOrEmpty(a.getFoto())){
                String[] split= a.getFoto().split(";");
                if(split.length>0){
                    String urls ="";
                    for(String inc:split){
                        GlobalAttachmentDto attachmentDto = globalAttachmentService.findByDocId(inc);
                        if (attachmentDto != null) {
                            String url = fileService.getViewLink(attachmentDto.getDocId(), false);
                            if(!Strings.isNullOrEmpty(url)) {
                                if(Strings.isNullOrEmpty(urls)){
                                    urls = url;
                                }else{
                                    urls = urls+";"+url;
                                }
                            }
                        }
                    }
                    a.setFoto(urls);
                }else{
                    a.setFoto("");
                }
            }
        });

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @Path("ruang")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Add",
            response = MessageDto.class,
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Add RuangArsip")
    public Response saveRuangArsip(
            @Valid @FormDataParam("dto") MasterRuangArsipDto dao,
            @FormDataParam("file") List<FormDataBodyPart> body) {
        try {
            MasterRuangArsipDto ruangArsipDto = masterRuangArsipService.save(null, dao);
            masterRuangArsipService.uploadAttachment2(ruangArsipDto.getId(),body);
            notificationService.sendInfo("Info", String.format("Data berhasil disimpan", dao.getLokasi()));

            return Response
                    .ok(ruangArsipDto)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save : %s", dao.getLokasi(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("ruang/{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Edit",
            produces = MediaType.MULTIPART_FORM_DATA,
            notes = "Edit RuangArsip")
    public Response editRuangArsip(
            @NotNull @PathParam("id") Long id,
            @Valid @FormDataParam("dto") MasterRuangArsipDto dao,
            @FormDataParam("file") List<FormDataBodyPart> body) {
        try {
            MasterRuangArsipDto ruangArsipDto = masterRuangArsipService.save(id, dao);
            masterRuangArsipService.uploadAttachment2(ruangArsipDto.getId(),body);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil diubah", dao.getKode()));

            return Response
                    .ok(ruangArsipDto)
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getKode(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("ruang/{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete RuangArsip")
    public Response deleteRuangArsip(@NotNull @PathParam("id") Long id) {
        try {
            MasterRuangArsip deleted = masterRuangArsipService.delete(id);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil dihapus", deleted.getKode()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus, Data sudah digunakan"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("ruang/{id}")
    @ApiOperation(
            value = "Get Ruang Arsip Data",
            response = MasterRuangArsipDto.class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Ruang Arsip object")
    public Response getRuangArsipById(@NotNull @PathParam("id") Long id) throws InternalServerErrorException {
        MasterRuangArsip ruangArsip = masterRuangArsipService.getData(id);
        MasterRuangArsipDto result = new MasterRuangArsipDto();
        if(ruangArsip!=null){
            result = new MasterRuangArsipDto(ruangArsip);
            if(!Strings.isNullOrEmpty(result.getFoto())){
                String[] split= result.getFoto().split(";");
                if(split.length>0){
                    List<String> urls = new ArrayList<>();
                    for(String inc:split){
                        GlobalAttachmentDto attachmentDto = globalAttachmentService.findByDocId(inc);
                        if (attachmentDto != null) {
                            String url = fileService.getViewLink(attachmentDto.getDocId(), false);
                            if(!Strings.isNullOrEmpty(url)) {
                                urls.add(url);
                            }
                        }
                    }
                    result.setFoto(urls.toString());
                }else{
                    result.setFoto("");
                }
            }
        }

        return Response
                .ok(result)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("ruang/all")
    @ApiOperation(
            value = "Get All Data",
            response = MasterRuangArsipDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return RuangArsip list")
    public Response getAllRuangArsip(
            @DefaultValue("") @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterRuangArsipDto> result = masterRuangArsipService.getResultList(filter, sort, descending).stream()
                .map(MasterRuangArsipDto::new).collect(Collectors.toList());
        long count = masterRuangArsipService.count(filter);

        result.forEach(a->{
            if(!Strings.isNullOrEmpty(a.getFoto())){
                String[] split= a.getFoto().split(";");
                if(split.length>0){
                    String urls ="";
                    for(String inc:split){
                        GlobalAttachmentDto attachmentDto = globalAttachmentService.findByDocId(inc);
                        if (attachmentDto != null) {
                            String url = fileService.getViewLink(attachmentDto.getDocId(), false);
                            if(!Strings.isNullOrEmpty(url)) {
                                if(Strings.isNullOrEmpty(urls)){
                                    urls = url;
                                }else{
                                    urls = urls+";"+url;
                                }
                            }
                        }
                    }
                    a.setFoto(urls);
                }else{
                    a.setFoto("");
                }
            }
        });

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }
}