package com.qtasnim.eoffice.ws;

import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.db.MasterTentangKami;
import com.qtasnim.eoffice.db.MasterTindakan;
import com.qtasnim.eoffice.security.Secured;
import com.qtasnim.eoffice.services.MasterTentangKamiService;
import com.qtasnim.eoffice.services.MasterTindakanService;
import com.qtasnim.eoffice.services.NotificationService;
import com.qtasnim.eoffice.ws.dto.MasterTentangKamiDto;
import com.qtasnim.eoffice.ws.dto.MasterTindakanDto;
import com.qtasnim.eoffice.ws.dto.MessageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jinq.jpa.JPAJinqStream;
import com.qtasnim.eoffice.services.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Path("master-tentang-kami")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured
@Api(
        value = "Master Tentang Kami Resource",
        description = "WS Endpoint untuk menghandle operasi Master Tentang Kami"
)
public class MasterTentangKamiResource {
    @Inject
    private MasterTentangKamiService tentangKamiService;

    @Inject
    private Logger logger;

    @Inject
    private NotificationService notificationService;

    @Context
    private UriInfo uriInfo;

    @GET
    @ApiOperation(
            value = "Get Data",
            response = MasterTentangKamiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Tentang Kami list")
    public Response getData() throws InternalServerErrorException {
        JPAJinqStream<MasterTentangKami> all = tentangKamiService.getAll();
        List<MasterTentangKamiDto> result = all
                .sorted(Comparator.comparing(MasterTentangKami::getSort))
                .map(MasterTentangKamiDto::new).collect(Collectors.toList());
        long count = all.count();

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @GET
    @Path("/filter")
    @ApiOperation(
            value = "Get Data",
            response = MasterTentangKamiDto[].class,
            produces = MediaType.APPLICATION_JSON,
            notes = "Return Tentang Kami list Filtered")
    public Response getFiltered(
            @DefaultValue("0")
            @QueryParam("skip") int skip,
            @DefaultValue("10")
            @Max(50)
            @QueryParam("limit") int limit,
            @DefaultValue("")
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @DefaultValue("false") @QueryParam("descending") boolean descending) throws InternalServerErrorException {
        List<MasterTentangKamiDto> result = tentangKamiService.getResultList(filter, sort, descending, skip, limit).stream()
                .map(MasterTentangKamiDto::new)
                .collect(Collectors.toList());
        long count = tentangKamiService.count(filter);

        return Response
                .ok(result)
                .header("X-Total-Count", count)
                .header("Access-Control-Expose-Headers", "X-Total-Count")
                .build();
    }

    @POST
    @ApiOperation(
            value = "Add",
            produces = MediaType.APPLICATION_JSON,
            notes = "Add Tentang Kami")
    public Response saveData(@Valid MasterTentangKamiDto dao) {

        try {
            tentangKamiService.saveOrEdit(null, dao);
            notificationService.sendInfo("Info", String.format("Data '%s' berhasil disimpan", dao.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil disimpan"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Save '%s': %s", dao.getPerihal(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("{id}")
    @ApiOperation(
            value = "Edit",
            produces = MediaType.APPLICATION_JSON,
            notes = "Edit Tindakan")
    public Response editData(@NotNull @PathParam("id") Long id, @Valid MasterTentangKamiDto dao) {
        try {
            tentangKamiService.saveOrEdit(id, dao);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil diubah", dao.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil diubah"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            notificationService.sendError("Error", String.format("Unable to Edit '%s': %s", dao.getPerihal(), ex.getMessage()));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(
            value = "Delete",
            produces = MediaType.APPLICATION_JSON,
            notes = "Delete Tentang Kami")
    public Response deleteData(@NotNull @PathParam("id") Long id) {
        try {
            MasterTentangKami deleted = tentangKamiService.deleteData(id);
            notificationService.sendInfo("Info", String.format("Data '%s'  berhasil dihapus", deleted.getPerihal()));

            return Response
                    .ok(new MessageDto("info", "Data berhasil dihapus"))
                    .status(Response.Status.OK)
                    .build();
        } catch (Exception ex) {
            logger.error(null, ex);
            //notificationService.sendError("Error", String.format("Unable to Delete: %s", ex.getMessage()));
            notificationService.sendError("Error", String.format("Data gagal dihapus"));

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}