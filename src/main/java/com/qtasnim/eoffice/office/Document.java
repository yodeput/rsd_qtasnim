package com.qtasnim.eoffice.office;

import lombok.Data;

@Data
public class Document {
    private String title;
    private String url;
    private String fileType;
    private String key;
}
