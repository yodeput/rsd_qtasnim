package com.qtasnim.eoffice.office;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class OnlyOfficeService {
    public FileModelFactory getConfigFactory(){
        return new FileModelFactory();
    }
}
