package com.qtasnim.eoffice.office;

import lombok.Data;

@Data
public class FileModel {
    private String type = "desktop";
    private String documentType;
    private Document document;
    private EditorConfig editorConfig;
    private String token;
}
