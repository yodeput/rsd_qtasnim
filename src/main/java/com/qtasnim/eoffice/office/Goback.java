package com.qtasnim.eoffice.office;

import lombok.Data;

@Data
public class Goback {
    private String url;
}
