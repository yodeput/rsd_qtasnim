package com.qtasnim.eoffice.office;

import com.qtasnim.eoffice.servlets.demo.dao.FileModel;
import lombok.Data;

@Data
public class EditorConfig {
    private String mode = "edit";
    private String callbackUrl;
    private User user;
    private Customization customization;
    private Embedded embedded;

    public EditorConfig() {
        user = new User();
        customization = new Customization();
    }

    public void initDesktop(String url)
    {
        embedded = new Embedded();
        embedded.setSaveUrl(url);
        embedded.setEmbedUrl(url);
        embedded.setShareUrl(url);
        embedded.setToolbarDocked("top");
    }
}
