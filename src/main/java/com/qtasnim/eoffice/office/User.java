package com.qtasnim.eoffice.office;

import lombok.Data;

@Data
public class User {
    private String id;
    private String name;
}
