package com.qtasnim.eoffice.office;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.qtasnim.eoffice.InternalServerErrorException;
import com.qtasnim.eoffice.config.ApplicationConfig;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.net.URLEncoder;
import java.util.Optional;
import java.util.UUID;

public class FileModelFactory {
    private FileModel fileModel;

    public FileModelFactory init(String fileName, String version, String mode, ApplicationConfig applicationConfig, String token, String idJenisDokumen) {
        try {
            String id = FileUtility.GetFileNameWithoutExtension(fileName);
            fileModel = new FileModel();
            fileModel.setDocumentType(FileUtility.GetFileType(fileName).toString().toLowerCase());

            Document document = new Document();
            document.setTitle(fileName);
            document.setUrl(String.format("%s/Download?id=%s&version=%s", applicationConfig.getBaseUrl(), id, version));
            document.setFileType(FileUtility.GetFileExtension(fileName).replace(".", ""));

            if (StringUtils.isNotEmpty(mode) && mode.equals("view")) { //jika view, ambil versi terakhir dari alfresco, bukan dari last cache onlyoffice
                String random = UUID.randomUUID().toString();
                document.setKey(random);
                document.setUrl(String.format("%s&random=%s", document.getUrl(), random));
            } else {
                document.setKey(Optional.ofNullable(version).map(t -> generateRevisionId(String.format("%s|%s", fileName, version))).orElse(fileName));
            }

            fileModel.setDocument(document);

            EditorConfig editorConfig = new EditorConfig();
            editorConfig.setCallbackUrl(String.format("%s/Upload?id=%s&token=%s&idJenisDokumen=%s", applicationConfig.getBaseUrl(), id, Optional.ofNullable(token).orElse(""), Optional.ofNullable(idJenisDokumen).orElse("")));
            editorConfig.getUser().setId("test");
            editorConfig.getCustomization().getGoback().setUrl(applicationConfig.getBaseUrl());
            editorConfig.getCustomization().setAutosave(true);
            editorConfig.getCustomization().setForcesave(true);
            fileModel.setEditorConfig(editorConfig);

            return this;
        } catch (Exception e) {
            throw new InternalServerErrorException(null, e);
        }
    }

    public FileModelFactory initDesktop() {
        fileModel.setType("embedded");
        fileModel.getEditorConfig().initDesktop(fileModel.getDocument().getUrl());

        return this;
    }

    public FileModelFactory setMode(String mode) {
        fileModel.getEditorConfig().setMode(mode);

        return this;
    }

    public String build() {
        try {
            return getObjectMapper().writeValueAsString(fileModel);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return objectMapper;
    }

    private String generateRevisionId(String expectedKey)
    {
        if (expectedKey.length() > 20)
            expectedKey = Integer.toString(expectedKey.hashCode());

        String key = expectedKey.replace("[^0-9-.a-zA-Z_=]", "_");

        return key.substring(0, Math.min(key.length(), 20));
    }
}
