package com.qtasnim.eoffice.office;

import lombok.Data;

@Data
public class Embedded {
    private String saveUrl;
    private String embedUrl;
    private String shareUrl;
    private String toolbarDocked;
}
