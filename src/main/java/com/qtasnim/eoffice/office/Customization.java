package com.qtasnim.eoffice.office;

import lombok.Data;

@Data
public class Customization {
    private Boolean autosave;
    private Boolean forcesave;
    private Goback goback;

    public Customization()
    {
        goback = new Goback();
    }
}
