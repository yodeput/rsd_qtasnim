package com.qtasnim.eoffice;

public class InternalServerErrorException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    private final String exceptionCode;

    public InternalServerErrorException(String message) {
        super(message);
        exceptionCode = null;
    }

    public InternalServerErrorException(String message, Throwable ex) {
        super(message, ex);
        exceptionCode = null;
    }

    public InternalServerErrorException(String message, String exceptionCode) {
        super(message);
        this.exceptionCode = exceptionCode;
    }

    public InternalServerErrorException(String message, String exceptionCode, Exception e) {
        super(message, e);
        this.exceptionCode = exceptionCode;
    }

    public String getExceptionCode() {
        return this.exceptionCode;
    }
}
