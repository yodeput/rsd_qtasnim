package id.kai.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

@Data
public class LoginSuccessDto {
    private String status;
    @JsonSerialize(using = ToStringSerializer.class)
    private String message;
    private String token;
    private String nama;
    private String nipp;
    private String jabatan;
    @JsonProperty("last_passworddate")
    private String lastPassworddate;
    @JsonProperty("cost_center")
    private String costCenter;
    @JsonProperty("lokasi_kerja")
    private String lokasiKerja;
    @JsonProperty("profile_URL")
    private String profileUrl;
    private List<String> roleName;
}
