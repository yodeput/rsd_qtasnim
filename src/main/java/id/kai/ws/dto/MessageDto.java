package id.kai.ws.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MessageDto {
    private List<String> password;

    public MessageDto() {
        password = new ArrayList<>();
    }
}
