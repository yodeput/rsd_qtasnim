package id.kai.ws.dto;

import com.qtasnim.eoffice.ws.dto.ResetDto;
import lombok.Data;

@Data
public class KAIResetPasswordDto {
    private Integer status;
    private String name;
    private KAIResetPasswordDetailDto message;

    public KAIResetPasswordDto(){}
}
