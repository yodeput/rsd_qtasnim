package id.kai.ws.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ITWSEmployeeSearchDto {
    private Integer status;
    private String name;
    private String message;
    private List<ITWSEmployeeDto> data;

    public ITWSEmployeeSearchDto() {
        data = new ArrayList<>();
    }
}
