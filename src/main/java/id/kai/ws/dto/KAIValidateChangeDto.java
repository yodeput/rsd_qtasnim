package id.kai.ws.dto;

import lombok.Data;

@Data
public class KAIValidateChangeDto {
    private Integer status;
    private String name;
    private String message;
    private String signature;

    public KAIValidateChangeDto(){}
}
