package id.kai.ws.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HrisOrgSearchDto {
    private Integer status;
    private String name;
    private String message;
    private List<HrisOrgDto> data;

    public HrisOrgSearchDto() {
        data = new ArrayList<>();
    }
}
