package id.kai.ws.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HrisPosisiSearchDto {
    private Integer status;
    private String name;
    private String message;
    private List<HrisPosisiDto> data;

    public HrisPosisiSearchDto() {
        data = new ArrayList<>();
    }
}
