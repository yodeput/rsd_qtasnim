package id.kai.ws.dto;

import lombok.Data;

@Data
public class HrisOrgDto {
    private Integer level;
    private String orgeh;
    private String ouabv;
    private String outxt;
    private String parnt;
    private String persa;
    private boolean isPosisi;
    private boolean isChief;
    private String clazz;
    private String grade;
    private String korsa;
}
