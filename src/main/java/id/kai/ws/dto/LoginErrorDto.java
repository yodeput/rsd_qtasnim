package id.kai.ws.dto;

import lombok.Data;

@Data
public class LoginErrorDto {
    private String status;
    private String name;
    private MessageDto message;
}
