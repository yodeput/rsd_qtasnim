package id.kai.ws.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HrisEmployeeCountDto {
    private Integer status;
    private String name;
    private String message;
    private String data;

    public HrisEmployeeCountDto() {

    }
}
