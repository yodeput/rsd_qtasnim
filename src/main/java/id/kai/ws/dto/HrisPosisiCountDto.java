package id.kai.ws.dto;

import lombok.Data;

@Data
public class HrisPosisiCountDto {
    private Integer status;
    private String name;
    private String message;
    private String count;
}
