package id.kai.ws.dto;

import lombok.Data;

@Data
public class KAIResetPasswordDetailDto {
    private String code;
    private Boolean is;

    public KAIResetPasswordDetailDto(){}
}
