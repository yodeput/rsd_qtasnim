package id.kai.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class LoginResponseDto {
    private String status;
    @JsonProperty("profile_URL")
    private String profileUrl;
}
