package id.kai.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class HrisPosisiDto {
    private String bgdat;
    private String chief;
    @JsonProperty("class")
    private String clazz;
    private String endat;
    private String grade;
    private String jobAbrv;
    private String jobId;
    private String jobTxt;
    private String lastupdate;
    private String lastUpdate;
    private String obdat;
    private String orgeh;
    private String plans;
    private String psabv;
    private String pstxt;
    private String status;
    private String stsob;
    private String trfgb;
    private String korsa;
}
