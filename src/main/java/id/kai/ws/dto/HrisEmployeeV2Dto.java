package id.kai.ws.dto;

import lombok.Data;

@Data
public class HrisEmployeeV2Dto {
    private String agama;
    private String alamat;
    private String busArea;
    private String companyCode;
    private String companyText;
    private String direktorat;
    private String email;
    private String emailKorporate;
    private String esgText;
    private String gelar;
    private String gender;
    private String golongan;
    private String hpPribadi;
    private String idEsg;
    private String idJenisPegawai;
    private String idJobFam;
    private String idKorsa;
    private String idPersa;
    private String idPosisi;
    private String idSubjobFam;
    private String idTipeIdentitas;
    private String idUnit;
    private String jenisPegawai;
    private String job;
    private String kota;
    private String nama;
    private String nipp;
    private String noIdentitas;
    private String npwp;
    private String pangkat;
    private String pangkatText;
    private String persa;
    private String ruang;
    private String tempatLahir;
    private String textBusArea;
    private String textJobFam;
    private String textKorsa;
    private String textPosisi;
    private String textSubjobFam;
    private String textUnit;
    private String tglLahir;
    private String tipeIdentitas;
    private String tmtKerja;
    private String tmtMutasi;
    private String tmtPensiun;
}
