package id.kai.ws.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HrisEmployeeSearchDto {
    private Integer status;
    private String name;
    private String message;
    private List<HrisEmployeeDto> data;

    public HrisEmployeeSearchDto() {
        data = new ArrayList<>();
    }
}
