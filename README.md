# Rail Document System
## Internal API Documentation


##### JAX-RS Constraint Validation
dengan Entity/JSON
````java
  @Data      
  @EnableISODateAfterValidator
  public class DemoEntity {
    @NotNull(message = "id cannot be null")
    private Integer id;
  
    @NotNull(message = "message cannot be null")
    @Size(min = 1, max = 10, message = "message length range should be {min} to {max}")
    private String message;
  
    @Email(message = "email should be valid")
    private String email;
  
    @NotNull(message = "isoDate cannot be null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "isoDate should be valid format: {format}")
    private String isoDate;

    @NotNull(message = "fromDate cannot be null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "fromDate should be valid format: {format}")
    private String fromDate;

    @NotNull(message = "toDate cannot be null")
    @ISODate(format = DateUtil.DATE_FORMAT, message = "toDate should be valid format: {format}")
    @ISODateAfter(field = "fromDate", message = "toDate should be after fromDate")
    private String toDate;
  
    ...
  }

  ...

  @POST
  public Response post(
          @Valid DemoEntity demoEntity
  ) {
    ...
  }
````

Ketika error invalid constraint, return ke front end menjadi seperti ini:

````json
{
  "fields": [
    {
      "field": "isoDate",
      "message": "isoDate should be valid format: yyyy-MM-dd'T'HH:mm:ss.SSSX"
    },
    {
      "field": "message",
      "message": "message length range should be 1 to 10"
    },
    {
      "field": "id",
      "message": "id cannot be null"
    },
    {
      "field": "email",
      "message": "email should be valid"
    },
    {
      "field": "toDate",
      "message": "toDate should be after fromDate"
     }
  ],
  "message": "Constraint validation failed",
  "status": "error"
}
````

##### Notification
```java
import com.qtasnim.eoffice.NotificationType;
import com.qtasnim.eoffice.services.NotificationService;

...

@Inject
private NotificationService notificationService;

...

//notifikasi ke front end
notificationService.sendNotification(user, NotificationType.INFO, "Title", "Body");

//notifikasi ke front end + email
notificationService.sendNotification(user, NotificationType.INFO, "Title", "Body", true);
```

##### File Operation
```java
import com.qtasnim.eoffice.services.FileService;

...

@Inject
private FileService fileService;

...

/** UPLOAD **/
String fileName = ...;
byte[] documentContent = ...;
String id = fileService.upload(fileName, document); //return id string disimpan ke table, digunakan untuk download atau delete

/** DOWNLOAD **/
byte[] documentContent = fileService.download(id);

/** DELETE **/
fileService.delete(id);

/** UPDATE **/
byte[] newDocumentContent = ...;
fileService.update(id, newDocumentContent);

/** GENERATE EDIT DOCUMENT LINK **/
String link = fileService.getEditLink(id);
```

#### Digital Signature
```java
import com.qtasnim.eoffice.services.DigitalSignatureService;

...

@Inject
private DigitalSignatureService digitalSignatureService;

...

byte[] pdfContent = ...;
byte[] signedPdfContent = digitalSignatureService.sign(pdfContent);

```

#### Sample git merge

```bash
git diff master..rollback -- "src/main/java/com/qtasnim/eoffice/db" "src/main/resources" > D:\2.db-from-ph.patch

git diff 3407a79e..a40aa7ad > D:\3.merge-to-latest.patch
```
